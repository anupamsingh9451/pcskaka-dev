<!-- BEGIN #footer -->
		<div id="footer" class="footer">
			<!-- BEGIN container -->
			<div class="container">
				<!-- BEGIN row -->
				<div class="row">
					<!-- BEGIN col-3 -->
					<div class="col-md-4">
						<h4 class="footer-header">ABOUT US</h4>
						<p>
							PCS KAKA is an online digital platform to cater the needs of pcs exam aspirants. PCSKAKA is a team of highly motivated individuals from different backgrounds. Our team consists of PCS officers, MBAs, Engineers. Our team strives hard to provide aspirants quality content with the relevance of the subject required for PCS exam.
						</p></br>
						<p>
						 Start building your career with us today!
						</p>
					</div>
					<!-- END col-3 -->
					<!-- BEGIN col-3 -->
					<div class="col-md-4">
						<h4 class="footer-header">RELATED LINKS</h4>
						<ul class="fa-ul">
							<li><i class="fa fa-li fa-angle-right"></i> <a href="<?php echo base_url('site/terms');?>">Terms of Use</a></li>
							<li><i class="fa fa-li fa-angle-right"></i> <a href="<?php echo base_url('site/contact');?>">Contact Us</a></li>
						    <li><i class="fa fa-li fa-angle-right"></i> <a href="<?php echo base_url('site/careers');?>">Careers</a></li> 
							<li><i class="fa fa-li fa-angle-right"></i> <a href="<?php echo base_url('site/payment');?>">Payment Method</a></li>
							<li><i class="fa fa-li fa-angle-right"></i> <a href="<?php echo base_url('site/sales_refund');?>">Sales & Refund</a></li>
							<li><i class="fa fa-li fa-angle-right"></i> <a href="<?php echo base_url('site/privacypolicy');?>">Privacy & Policy</a></li>
						</ul>
					</div>
					<div class="col-md-4">
						<h4 class="footer-header">OUR CONTACT</h4>
						<address>
							<strong>PCSKAKA, Inc.</strong><br />							 
							<abbr title="Phone">Phone :</abbr> 9452736000<br />		
							<abbr title="Email">Email : </abbr> <a href="javascript:void(0);">support@pcskaka.com</a><br />
						</address>
					</div>
					<!-- END col-3 -->
				</div>
				<!-- END row -->
			</div>
			<!-- END container -->
		</div>
		<!-- END #footer -->
		
		<!-- BEGIN #footer-copyright -->
		<div id="footer-copyright" class="footer-copyright">
			<!-- BEGIN container -->
			<div class="container">
				<div class="payment-method">
					<img src="<?php echo base_url(); ?>assets/frontendasset/img/payment/payment-method.png" alt="" />
				</div>
				<div class="copyright">
					Copyright &copy; 2019 PCSKAKA. All rights reserved.
				</div>
			</div>
			<!-- END container -->
		</div>
		<!-- END #footer-copyright -->
	</div>
	<!-- END #page-container -->

	<!-- begin theme-panel -->
	<div class="theme-panel hide">
		<a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
		<div class="theme-panel-content">
			<ul class="theme-list clearfix">
				<li><a href="javascript:;" class="bg-red" data-theme="red" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/red.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-pink" data-theme="pink" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/pink.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Pink" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-orange" data-theme="orange" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/orange.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-yellow" data-theme="yellow" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/yellow.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Yellow" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-lime" data-theme="lime" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/lime.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Lime" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-green" data-theme="green" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/green.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Green" data-original-title="" title="">&nbsp;</a></li>
				<li class="active"><a href="javascript:;" class="bg-teal" data-theme="default" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/default.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-aqua" data-theme="aqua" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/aqua.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Aqua" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-blue" data-theme="blue" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/blue.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-purple" data-theme="purple" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/purple.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-indigo" data-theme="indigo" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/indigo.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Indigo" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-black" data-theme="black" data-theme-file="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/black.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black" data-original-title="" title="">&nbsp;</a></li>
			</ul>
		</div>
	</div>
	<!-- end theme-panel -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/frontendasset/plugins/jquery/jquery-3.3.1.min.js"></script>	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<script src="<?php echo base_url(); ?>assets/frontendasset/js/slick.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/frontendasset/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>	<!--[if lt IE 9]>
	
	
	<![endif]-->
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/frontendasset/plugins/js-cookie/js.cookie.js"></script>
	<script src="<?php echo base_url(); ?>assets/frontendasset/plugins/paroller/jquery.paroller.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/frontendasset/js/e-commerce/apps.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	<script src="<?php echo base_url('assets/siteasset/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?php echo base_url('assets/siteasset/datatables/js/dataTables.bootstrap.js')?>"></script>
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets/frontendasset/plugins/highlight/highlight.common.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<!-- ================== END PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets/studentassets/js/studentget.js"></script>
	<script src="<?php echo base_url(); ?>assets/studentassets/js/studentpost.js"></script>
	<script src="<?php echo base_url(); ?>assets/siteasset/js/sitepost.js"></script>
	<script src="<?php echo base_url(); ?>assets/siteasset/js/sitevalidate.js"></script>
	<script>
	    
    $('document').ready(function(){
            App.init();			
    		AOS.init();	
    		$('.studentloginreg').click(function(){				
        		$('#exampleModalCenter').modal('show');	
    		});	
        });
	</script>
	<script>
	    var width=$(window).width();
        if(width>768){
            $('.slider-test-series').slick({			
    	        slidesToShow: 4,			
    	        slidesToScroll: 4,			
    	        dots: false,			
    	        infinite: true,			
    	        autoplay: true,			
    	        autoplaySpeed: 3000,			
    	        cssEase: 'linear'	
    	   });
    	   $('.slider-testimonials').slick({			
    	        slidesToShow: 3,			
    	        slidesToScroll: 3,			
    	        dots: false,			
    	        arrows : true,
    	        infinite: true,			
    	        autoplay: true,			
    	        autoplaySpeed: 3000,			
    	        cssEase: 'linear'	
    	   });
        }else{
            $('.slider-test-series').slick({			
    	        slidesToShow: 1,			
    	        slidesToScroll: 1,			
    	        dots: false,			
    	        infinite: true,			
    	        autoplay: true,			
    	        autoplaySpeed: 3000,			
    	        cssEase: 'linear'	
    	   });
    	   $('.slider-testimonials').slick({			
    	        slidesToShow: 1,			
    	        slidesToScroll: 1,			
    	        dots: false,			
    	        arrows : true,		
    	        infinite: true,			
    	        autoplay: true,			
    	        autoplaySpeed: 3000,			
    	        cssEase: 'linear'	
    	   });
        }
	    	
	</script>

</body>
</html>