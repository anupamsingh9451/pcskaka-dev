<link href="<?php echo base_url(); ?>assets/siteasset/css/library/library.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>KAKA's LIBRARY</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
			<h2>What is needed to become IAS/PCS? </h2>
			<p class="text-left"><b>If you are reading this, then you are well aware of pattern and syllabus of our civil services. Syllabus is humongous and questions to cover are in thousands. Aspirants study night and day but still unable answer few questions in the prelims examination and hence miss the cut off. They think that the question has fell from sky. But in reality that is not the case, the basic reason is lack of practice of what you have read. When you don’t get much practice needed for the topics that you have covered, it is natural to forget names and dates. But these names and places are very much core of any prelims examinations. Hence, the need of practicing the topics in a prelims way.
			<br> <br>
			I have seen many aspirants, who have a good knowledge of subject and can write a well versed mains answer but are unable to clear prelims. Puzzled? That’s Ok. Let’s take example of 2015 IAS topper, she scored just the cut off marks of a reserved category and then went ahead to write well versed and articulated answers in mains to secure THE FIRST RANK. Does that ring a bell? 
			<br> <br>
			You should ask the following questions to yourself: 1) What could have been the scenario if she has scored one mark less or if she would have not been from reserved category? 2) What went wrong in prelims, if she had topped the examination in mains (and obviously interview).
			<br> <br>
			PCA Kaka is designed to overcome these challenges faced in prelims examination.
			<br> <br>
			<h2>What is Kaka’s Library?</h2>
			<p class="text-left"><b>Answer is simple. Thousands of questions covered from hundreds of books available in one platform 24x7.
			<br> <br>
				
			<h2>Why Kaka’s Library?</h2>
			<p class="text-left"><b>You read a topic, you practice few questions and you think you are good to go but in reality you are not. What happens when on the D-Day you get a question from the same topic of which you have no idea. This is where Kaka’s library comes in to picture. Kaka’s library has all the possible questions available on this planet. And our library is organic, its evolving day by day. It is highly unlikely that you miss the cutoff if you follow Kaka’s guidance.
			<br> <br>
			
			<h2>How to use Kaka’s Library?</h2>
			<p class="text-left"><b>Kaka’s library is designed in a modular way. It has all the subjects of CSE preparation. Each subject is divided into sub sections. E.g., History is divided into Ancient, Medieval and Modern. One can choose a section and start his/her practice of that section.
			<br> <br>
			
			<p class="text-left"><b>We are working to provide you seamless integration for all the subjects required for CSE prepartion.<br>
			We will be online shortly.
			
			<br> <br>
			To contact us in the meantime please email:<br>
			<a href="mailto:support@pcskaka.com">support@pcskaka.com</a></b></p>
		</div>
				
	</div>
		
	
</div>
