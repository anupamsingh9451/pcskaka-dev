<link href="<?php echo base_url(); ?>assets/siteasset/css/ias.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>IAS</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
			<h2>What we do</h2>
			<p><b>PCS KAKA</b> is a digital library designed to cater needs of civil service aspirants. It has all the subjects required for the preparation of UP PCS examination. Our library has more than 20,000 questions covering various topics from all the books available in market. Our Library covers the following subjects from UP PCS exam’s perspective: History, Geography, Polity, Economics, Environment, Science, Current affairs of state and national importance. This library is completely digital. Available 24x7. </p>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Comparative Analysis :</b> Get your rank after completing a practice test. See where you stand in this competitive world.</span>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Immediate Result :</b> The moment you submit the practice test; answers will be displayed with questions. See where you went wrong.</span>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Unlimited attempts: </b> You can take any practice test unlimited number of times. Improve your score and hence your memorizing power of facts and figures.</span>
		</div>			
	</div>
		
	<div class="pcs-info ">
		<div class="container">
			<div class="section-pcs-info m-b-30">
				<h2>What is IAS or What is UPSC? </h2>
				<p>IAS is the most prestigious examination of India. It is conducted by UPSC – Union Public Service Commission - <a href="https://upsc.gov.in">https://upsc.gov.in.</a>
					In order to succeed, it is important to understand the syllabus of the examination so that aspirant does not waste their time and energy in unnecessary activities. There is a common UPSC syllabus pattern for services such as the Indian Administrative Service, Indian Foreign Service, Indian Police Service, Indian Revenue Service, among others.
				</p>
				<p><b>Selection Process:</b> IAS/IPS examination conducted by UPSC is based on the following pattern of prelims exmaination. Once qualified, aspirant appaears for main examination and viva-voce:-</p>
				<div class="row m-t-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">Exam Type</th>
										<th class="textDarkBlue border-0">Educational Criteria</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td>Preliminary Examination</td>
										<td>Objective Type & Multiple choices</td>
									</tr>
									<tr>
										<td>Main Examination</td>
										<td>Conventional Type, i.e. Written examination</td>
									</tr>
									<tr>
										<td>Viva- Voice</td>
										<td>Personality Test</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="pre-exams">
				<div class="heading">
					<h2>Preliminary Examination:-</h2>
					<p style="font-weight:500;">The Civil Services Examination will consist of two successive stages (i) Civil Services (Preliminary) Examination (Objective type) for the selection of candidates for the Main Examination and Civil Services (Main) Examination (Written and Interview) for the selection of candidates for the various Services and posts.</p>
				</div>
				<div class="row mt-5 m-b-20">
					<div class="col-sm-12">
						<div class="border rounded lightbluebackground p-2">
							<h3 class="mainHeading text-center pt-2 pb-2">Preliminary Examination(objective Type & multiple choice)</h3>
							<div class="row">
								<div class="col-sm-12 col-12 pb-3">
									<div class="table-responsive pr-2 pl-2 pb-1 pt-1">
										<table class="table mb-0">
											<thead>
												<tr class="border-top"><th class="textDarkBlue border-0">Exam</th>
													<th class="textDarkBlue border-0">No. of Questions</th>
													<th class="textDarkBlue border-0">Total Marks</th>
													<th class="textDarkBlue border-0">Timing</th>
												</tr>
											</thead>
											<tbody class="textGray">
												<tr class="border-bottom border-top">
													<td class="text-truncate">Paper-I</td>
													<td class="text-truncate">100</td>
													<td class="text-truncate">200</td>
													<td class="text-truncate">9.30 to 11.30 A.M. </td>
												</tr>
												<tr class="border-bottom border-top">
													<td class="text-truncate">Paper-II</td>
													<td class="text-truncate">80</td>
													<td class="text-truncate">200</td>
													<td class="text-truncate">2.30 to 4.30 P.M.</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<p style="font-weight:500"><b style="color:red;">Note:-</b><br><b> (1) Paper-II</b> of the Preliminary Examination will be a qualifying paper with minimum qualifying marks fixed at 33%.<br> 
					<b>(2)</b> It is mandatory for the Candidates to appear in both the papers of Preliminary Examination for the purpose of evaluation. Therefore, a candidate will be disqualified in case he does not appear in both in papers.<br> 
					<b>(3)</b> The merit of the Candidates will be determined on the basis of marks obtained in Paper-I of the Preliminary Examination.
				</p>

				<div class="row m-t-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">Prelims Syllabus</th>
										<th class="textDarkBlue border-0">Duration</th>
										<th class="textDarkBlue border-0">Total Marks</th>
										<th class="textDarkBlue border-0">Total Questions</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td>GS-I</td>
										<td>Two hours</td>
										<td>200</td>
										<td>150</td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row m-t-30 m-b-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">UPSC Prelims syllabus - GS-1</th>
										<th class="textDarkBlue border-0">PCS KAKA Library</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"> Current events of national and international importance.</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">On a given date Kaka’s libraray has more than <b>1500 questions</b> to practice from. Library is divided into monthly current affairs by Month such as Jan, Feb, etc. On a given day you have access to last 15 months current affairs.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"> History of India and Indian National Movemen.</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Kaka’s Library contains more than <b>4000 question</b> spanning from ancient India to Modern India. Also containing questions from Art and Culture. History library is divided in to subsections such as Ancient India, Medieval India, Modern India and Art and culture.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Indian Polity and Governance-Constitution, Political System, Panchayati Raj, Public Policy,Rights Issues,</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Indian Polity section contains more than <b>2500 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Indian and World Geography-Physical, Social, Economic Geography of India and the World.</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Geography section of Kaka’s library contains more than <b>3000 questions</b>. Geography has been divided into subsections such as Physical Geography, Indian Geography and World Geography.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Economic and Social Development-Sustainable Development, Poverty, Inclusion, Demographics, Social Sector Initiatives. etc</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Economics section contains more than <b>2000 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">General issues on Environmental ecology, Bio-diversity and Climate Change - that do not require subject specialization.</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Environment section contains more than <b>2500 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">General Science</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Science section contains more than <b>2500 questions</b> to practice from. Science has been divided into subsections such as Physics, Chemistry and Biology.
											Aspirant can study and test his knowledge by choosing any subsection to attempt the questions.
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="section-pcs-info m-b-30">
					<h2 class="text-center" style="font-size: 25px;"><ins>Paper-II General Studies-II Duration: Two hours Marks - 200</ins></h2>
				
					<div class="text m-t-20">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Comprehension</b></span>
					</div>
					<div class="text">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Interpersonal skills including communication skills </b></span>
					</div>
					<div class="text">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Logical reasoning and analytical ability </b></span>
					</div>
						<div class="text m-t-20">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Decision-making and problem solving</b></span>
					</div>
					<div class="text">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>General mental ability </b></span>
					</div>
					<div class="text">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Basic numeracy (numbers and their relations, orders of magnitude, etc.) (Class X level), Data interpretation (charts, graphs, tables, data sufficiency etc. – Class X level).</b></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
