<link href="<?php echo base_url(); ?>assets/siteasset/css/index.css" rel="stylesheet" />
<!-- BEGIN #slider -->
	<div id="slider" class="section-container p-0 bg-black-darker">		<!-- BEGIN carousel -->
		<div id="main-carousel" class="carousel slide" data-ride="carousel">
			<!-- BEGIN carousel-inner -->
			<div class="carousel-inner">
				<!-- BEGIN item -->
				<div class="carousel-item active cursor_pointer" onclick="location.href='<?php echo base_url(); ?>site/testseries'"  style="background-image: url(<?php echo base_url(); ?>assets/frontendasset/img/slider/Slider1.png);"></div>
				<!-- END item -->
				<!-- BEGIN item -->
				<div class="carousel-item cursor_pointer" onclick="location.href='<?php echo base_url(); ?>site/library'"  style="background-image: url(<?php echo base_url(); ?>assets/frontendasset/img/slider/slider-10-cover.jpeg);">
				</div>
			</div>
			<!-- END carousel-inner -->
			<a class="carousel-control-prev" href="#main-carousel" data-slide="prev"> 
			    <i class="fa fa-angle-left"></i> 
			</a>
			<a class="carousel-control-next" href="#main-carousel" data-slide="next"> 
				<i class="fa fa-angle-right"></i> 
			</a>
		</div>
		<!-- END carousel -->
	</div>
	
		<div class="w3ls-titles text-center mb-5 mt-2">		
        		<h1 class="title mt-10"><span class="hdng">Our Aim!</span></h1>			
        		<p class="mt-3 mx-auto" align="justify">Digital revolution in civil services preparation. Team PCSKAKA wants to help the person in remotest of village of India aspiring to become civil servant. FREE enrolment for those who cant afford coaching fees. For more info write to us at:<br><b><b> free-enrollment@pcskaka.com</b></p>	
    	    </div>
    	    
<div class="text-info margintp_btm section_padding section-info">			
	<div class="container">		
		<h3 class="text-center heading">Why PCS KAKA for any prelims examination or multiple choice question paper? </h3>		
		<p class="clr_wt" align="justify">Reason is simple, we have every available question which examiner can put in your objective test paper. We have a mammoth library of whopping 20,000+ plus questions available online, 24X7.
In this fast competitive world, every second is important. So, KAKA does not want you to waste your time in buying books, Question Banks, previous year papers, Current Affair Questions, State GK questions, etc. We have made all this available to you on a click of button.
The strategy is simple, buy standard books (books required for CSE is given in subject page, subject wise), read The Hindu daily and attempt questions from our library and focus on your mains examination. Remember, every question is important, no question is lower in value than the other.
</p>
	</div>
</div>

	<div class="w3ls-titles text-center mb-5">		
        		<h2 class="title"><span class="hdng">Subjects in Kaka's Library</span></h2>			
        		<p class="mt-3 mx-auto" align="justify">Following are the subjects which our library has. These are a must read for any aspirant appearing in CSE. </p>	
    	    </div>
    	    
    	    
    <section class="blog_w3ls" style="width:96%;">    
        <div class="container">		 		
            <div class="row py-2">            
                <div class="col-lg-4 col-md-4 pull-up">				
                    <div class="card sub-min-height">					
                        <div class="card-body text-center">						
                            <img src="<?php echo base_url('assets\frontendasset\img\Books_Pics\History\history.png');?>" alt="history for PCS and IAS" class="img-fluid height-250">						
                            <h5 class="blog-title card-title mt-3 mb-3 " >                            
                                <a href="<?php echo base_url('site/history');?>">History</a>                        
                            </h5>						
                            <p class="text-justify">Our History section is divided into Ancient, Art & Culture, Medieval, Modern and World History. 2000+ questions compiled from various books such as NCERT, Jain and Mathur, Norman Lowe, Ramchandra Guha, Bipan Chandra , Nitin Singhania, RS Sharma, A.L. Basham</p>
                        </div>                        				
                    </div>			
                </div>	
                <div class="col-lg-4 col-md-4 pull-up " >				
                    <div class="card sub-min-height">					
                        <div class="card-body text-center">						
                            <img src="<?php echo base_url('assets\frontendasset\img\Books_Pics\Geography\geography.png');?>" alt="geography for PCS and IAS" class="img-fluid height-250">						
                            <h5 class="blog-title card-title mt-3 mb-3">                            
                                <a href="<?php echo base_url('site/geography');?>">Geography</a>                        
                            </h5>
                            <p class="text-justify">Our Library contains 2000+ questions from geography divided into Physical, Indian and World Geography. Questions are taken from all prominent books including NCERT. Others include Majid Hussain, Goh Cheng Leong, D R Khuller.
                            </p> 
                        </div>                        				
                    </div>			
                </div>			
                <div class="col-lg-4 col-md-4 pull-up">				
                    <div class="card sub-min-height">					
                        <div class="card-body text-center">						
                            <img src="<?php echo base_url('assets\frontendasset\img\Books_Pics\Environment\Environment.png');?>" alt="Environment for PCS and IAS" class="img-fluid height-250">					
                            <h5 class="blog-title card-title mt-3 mb-3">                           
                                <a href="<?php echo base_url('site/environment');?>">Environment</a>                        
                            </h5>					
                            <p class="text-justify">Environment section of our library contains 2000+ questions. Special emphasis is given to questions arising from international summits and issues. Not a single prominent author's question is left behind.</p>  
                        </div>                
                    </div>			
                </div>		
                
                  
            </div>
			<div class="row">
			    <div class="col-lg-4 col-md-4 blog-3 mt-md-0 mt-5 pull-up">	
                    <div class="card sub-min-height">		
                        <div class="card-body text-center">			
                            <img src="<?php echo base_url('assets\frontendasset\img\Books_Pics\Economics\economics.png');?>" alt="Economics for PCS and IAS" class="img-fluid height-250">
                            <h5 class="blog-title card-title mt-3 mb-3">					
                                <a href="<?php echo base_url('site/economics');?>">Economics</a>					
                            </h5>				
                            <p class="text-justify">We have 2000+ questions of Economics for CSE. We have tried to include all possible questions such as NCERT and other prominent writers such as Ramesh Singh, Datt and Sundaram and others.</p>			
                        </div>					
                    </div>		
                </div>
				<div class="col-lg-4 col-md-4 blog-3 mt-md-0 mt-5 pull-up">	
                    <div class="card sub-min-height">		
                        <div class="card-body text-center">			
                            <img src="<?php echo base_url('assets\frontendasset\img\Books_Pics\Science\science.png');?>" alt="Science for PCS and IAS" class="img-fluid height-250">
                            <h5 class="blog-title card-title mt-3 mb-3">					
                                <a href="<?php echo base_url('site/science');?>">Science</a>					
                            </h5>				
                            <p class="text-justify">Science section is divided into Physics, Chemistry and Biology having 2000+ questions. Questions have been taken from prominent books especially NCERT and Lucent.</p>			
                        </div>					
                    </div>		
                </div>
                <div class="col-lg-4 col-md-4 blog-3 mt-md-0 mt-5 pull-up">	
                    <div class="card sub-min-height">		
                        <div class="card-body text-center">			
                            <img src="<?php echo base_url('assets\frontendasset\img\Books_Pics\Polity\polity.png');?>" alt="Polity for PCS and IAS" class="img-fluid height-250">
                            <h5 class="blog-title card-title mt-3 mb-3">					
                                <a href="<?php echo base_url('site/polity');?>">POLITY</a>					
                            </h5>				
                            <p class="text-justify">Polity section of our library contains 2000+ questions specially designed for Civil Services Examination. It has questions ranging from NCERT to DD Basu to M Laxminkanth.</p>			
                        </div>					
                    </div>		
                </div>
			</div>
        </div>
    </section>
	
    <section class="blog_w3ls py-lg-5 mb-5" style="width:96%;">	
        <div class="container-fluid">
    		<div class="w3ls-titles text-center mb-5">		
        		<h3 class="title"><span class="hdng">Testimonials</span></h3>			
        		<p class="mt-3 mx-auto" align="justify">Team PCSKAKA thanks the following aspirants who were kind enough to share their thoughts about us. See what they have to say about us.</p>	
    	    </div>	
    	    	<div class="row py-5 slider slider-testimonials">            
					<div class="col-lg-3 col-md-3 pull-up">				
						<div class="card">		
							<div class="card-body text-center">			
								<img src="<?php echo base_url('assets/frontendasset/img/anjana.jpeg');?>" alt="Anjana Jha" class="img-fluid height-250">
								<h5 class="blog-title card-title mt-3 mb-3">					
									<h3 class="title">Anjana Jha</h3>
									<span class="post">I am working professional and dont get time for gathering books and data. I thank KAKA for this digital initiative. Now i can prepare for my psc exam from my home.</span>					
								</h5>				
								<ul class="social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-instagram"></i></a></li>
									<li><a href="#"><i class="fab fa-skype"></i></a></li>
								</ul>
							</div>					
						</div>			
					</div>	
					<div class="col-lg-3 col-md-3 pull-up">				
						<div class="card">		
							<div class="card-body text-center">			
								<img src="<?php echo base_url('assets/frontendasset/img/mannu.jpg');?>" alt="Manvendra" class="img-fluid height-250">
								<h5 class="blog-title card-title mt-3 mb-3">					
									<h3 class="title">Manvendra Singh</h3>
									<span class="post">I thank team PCSKAKA for their guidance throughout my UPPSC preparation due to which i was able to clear prelims 2018. Now that KAKA is digital, i wish them best of luck.</span>					
								</h5>				
								<ul class="social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-instagram"></i></a></li>
									<li><a href="#"><i class="fab fa-skype"></i></a></li>
								</ul>
							</div>					
						</div>		
					</div>			
					<div class="col-lg-3 col-md-3 pull-up">				
						<div class="card">		
							<div class="card-body text-center">			
								<img src="<?php echo base_url('assets/frontendasset/img/akhand.jpeg');?>" alt="Akhand" class="img-fluid height-250">
								<h5 class="blog-title card-title mt-3 mb-3">					
									<h3 class="title">Akhand Pratap Singh</h3>
									<span class="post">PCS J prelims is hardest nut to crack. I thank PCSKAKA for this digital initiative. This helped me for preparation while i was attending court.</span>					
								</h5>				
								<ul class="social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-instagram"></i></a></li>
									<li><a href="#"><i class="fab fa-skype"></i></a></li>
								</ul>
							</div>					
						</div>			
					</div>		
					
					<div class="col-lg-3 col-md-3 blog-3 mt-md-0 mt-5 pull-up">	
						<div class="card">		
							<div class="card-body text-center">			
								<img src="<?php echo base_url('assets/frontendasset/img/Sanoj.jpg');?>" alt="Sanoj" class="img-fluid height-250">
								<h5 class="blog-title card-title mt-3 mb-3">					
									<h3 class="title">Sanoj Kashyap</h3>
									<span class="post">Time is everything. Thank you KAKA for this wonderfull digital initiative. Now I dont need to go anywhere. I work in ofc and after ofc hours can peacefully study at home by just browsing your site.</span>					
								</h5>				
								<ul class="social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-instagram"></i></a></li>
									<li><a href="#"><i class="fab fa-skype"></i></a></li>
								</ul>
							</div>					
						</div>		
					</div>  
            </div>
    	</div>
    </section>

    <section class="service_part section_bg_2 section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="single_service_part">
                        <div class="single_service_part_iner">
                            <span class="fa fa-users"></span>
							<p class="counter">5231</p>
							<h3>Students Enrolled</h3>
                           
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_service_part">
                        <div class="single_service_part_iner">
                            <span class="fa fa-envelope"></span>
                            <p class="counter">4153</p>
							<h3>Test Takers</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_service_part">
                        <div class="single_service_part_iner">
                            <span class="fa fa-briefcase"></span>
							<p class="counter">125308</p>
							<h3>Visitors</h3>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_service_part">
                        <div class="single_service_part_iner">
                            <span class="fa fa-comments"></span>
                            
							 <p class="counter">15356</p>
							<h3>Questions Answered</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- service_part part end-->
<div class="testimonials align-w3 py-5" style="padding-bottom:5em!important;display:none;">        
    <div class="container">            
        <div class="wthree_pvt_title text-center mb-1"style="padding-bottom: 30px;">               
            <h3 class="title"><span class="hdng">Comments</span></h3>                           
        </div>            
        <div class="carousel-inner">                
            <div class="row">                    
                <div class="col-lg-6">                        
                    <div class="testimonials_grid " data-aos="flip-down" data-aos-duration="500" >                            
                        <div class="testi-text text-center">                                
                            <p><span class="fa fa-quote-left text-center"></span>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet<span class="fa fa-quote-right"></span></p>                            
                        </div> 
                        <div class="d-flex align-items-center justify-content-center">     
                            <div class="testi-desc">             
                                <span class="fa fa-user text-center"></span>       
                                <h5 class="text-center">Aliquyam</h5>                
                                <p class="text-center">Member</p>            
                            </div>                           
                        </div>                     
                    </div>                
                </div>                  
                <div class="col-lg-6">         
                    <div class="testimonials_grid mt-lg-0 mt-4 " data-aos="flip-down" data-aos-duration="500">   
                        <div class="testi-text text-center">             
                            <p><span class="fa fa-quote-left text-center"></span>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet<span class="fa fa-quote-right"></span></p>      
                        </div>          
                        <div class="d-flex align-items-center justify-content-center">   
                            <div class="testi-desc">         
                                <span class="fa fa-user text-center"></span>      
                                <h5  class="text-center">Aliquyam</h5>            
                                <p  class="text-center">Member</p>                  
                            </div>                          
                        </div>                     
                    </div>                  
                </div>               
            </div>         
        </div>      
    </div>  
</div>
<section class="bg-light testimonials align-w3 py-5 aos-init aos-animate" id="contact-section" style="padding-bottom:0!important;"data-aos="fade">
      <div class="container">
        <div class="w3ls-titles text-center">		
        		<h3 class="title"><span class="hdng">Get In Touch</span></h3>			
        		<p class="mt-3 mx-auto">We want to hear from you!</p>	
    	</div>
        <div class="row">
          <div class="col-md-12 mb-5">        
            <form  class="" id="contactform" method="post">
			    <div class="row">
    		       <div class="col-md-6">
    					<div class="row form-group">
    						<div class="col-md-6 mb-3 mb-md-0">
    						  <label class="text-black" for="fname">Name <span class="text-danger">*</span></label>
    						  <input type="text" id="fname" name="username" class="form-control" required>
    						</div>
    						<div class="col-md-6  mb-3 mb-md-0">
    						  <label class="text-black" for="email">Email <span class="text-danger">*</span></label> 
    						  <input type="email" id="email" name="email" class="form-control" required>
    						</div>
    					</div>
    					<div class="row form-group">
    						<div class="col-md-6 mb-3 mb-md-0">
    						    <label class="text-black" for="mobileno">Mobile No. </label>
    						    <input type="text" id="mobileno" name="mobileno" class="form-control input_num">
    						</div>
    						<div class="col-md-6  mb-3 mb-md-0">
    						    <label class="text-black" for="subject">Subject <span class="text-danger">*</span></label> 
						        <input type="subject" name="subject" id="subject" class="form-control" required>
    						</div>
    					</div>
						
				    </div>               
                    <div class="col-md-6">
        				<label class="text-black" for="message">Message <span class="text-danger">*</span></label> 
                        <textarea name="message" id="message" name="message" cols="30" rows="5" class="form-control" placeholder="Write your notes or questions here..." required></textarea>                               
                    </div>
		    	</div>
				<div class="row form-group" style="float: right;margin-top: 2px;">
                    <div class="col-auto my-1">
    				    <button type="submit" name="contactform" class="btn btn-md btn-primary float-right contactform">Send Message</button>
    				</div>
				</div>
            </form>
          </div>
          
        </div>
      </div>
      
      <div class="text-info section_padding section-info">			
        <div class="container">		
    		<h3 class="text-left heading">Following are state psc exams that we cover </h3>		
    		<p class="clr_wt">1. Online test series for UPPSC </p>
    		<p class="clr_wt">2. Online test series for MPPSC </p>
    		<p class="clr_wt">3. Online test series for MPSC </p>
    		<p class="clr_wt">4. Online test series for CGPSC </p>
    		<p class="clr_wt">5. Online test series for RAS </p>
    		<p class="clr_wt">6. Online test series for BPSC </p>
    		<p class="clr_wt">7. Online test series for JPSC </p>
    		<p class="clr_wt">8. Online test series for GPSC </p>
    	
    		<p class="clr_wt">Aspirants are advised to keep visiting our test series page to get updated information on ongoing and upcoming test series. Alternately, you can register
    		with us and get updated via email for our test series programmes. Registration is completely free.</p>
    		
    	</div>
    </div>
    </section>		
