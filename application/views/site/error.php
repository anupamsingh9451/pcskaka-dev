<style>
    #page-container{
        background-color: #f1f3f6;
    }
    .error-fontsize{
        font-size: 95px;
    }
</style>
<div class="container" style="padding:50px 0;">
    <div class="section" style="padding-bottom:0; background-color: #FFF;">
        <div class="row" style="margin-left: 10%;">
            <div class="col l6 m6 s12" style="padding-left: 20px;">
                <div style="color:#EEE;" class="error-fontsize"><b>Oops!</b></div>
                <div class="error-width" style="color: #333;font-size: 17px;"><p>It’s looking like you may have taken a wrong turn. Don’t worry... it happens to the best of us.</p></div>
                <div style="color:#333; font-size:16px; margin-top:40px;height: 185px;">
                    <p>Looks like the page you requested does not exist or has moved. We suggest you check if the URL is correct.</p>
                    <p>We can help. Call us on +91 - 9452736000</p>
                    <p>Take me back to <a href="/">PCSKAKA Homepage</a></p>
                </div>
            </div>
            <div class="col l6 m6 s12">
                <div><img class="responsive-img" src="<?php echo base_url('assets/frontendasset/img/cartoon-astronaut.jpg');?>" alt="404 lost astronaut cartoon"></div>
            </div>
        </div>   
    </div>
</div>