<link href="<?php echo base_url(); ?>assets/siteasset/css/salesrefund/salesrefund.css" rel="stylesheet" />
<main>
  <h1  style="margin-top: 2%;">Sales & Refund Policy</h1>
  <p>We have a “no questions asked refund policy” which entitles all our customers to return the product/services if due to some reason they are not satisfied with the quality or content of the product/services. All refund related terms and conditions are stated at the time of buying a product/service. If a person buys a product that means he/she agrees with the terms. We will ivestigate the matter and look if the person seeking refund qualifies for it. Then we issue a refund which will be credited to your account within 7 working days. Subject to clearance by PayUMoney, our payment partner.

<br>
<br>
Please note the following points for refund:<br>
1. If a product/service is non-refundale, no refund will be processed.<br>
2. In case of a refundable product, we will check if all the criterions are met and then process the refund.<br>
3. All the test series are non refundable, hence no refund will be processed in case of test series subscription/payment..
</p>
<br>
<br>
<p align="left" style="width: 100%;">pcakaka.com reserves the right to deny the refund, if:<br>
 1. User does not comply with terms and conditions of our website<br>
 2. User is found indulged in copying content or trying access the content in any unlawfull manner<br>
 3. Any cheating, forgery or any mischievous act is found on user's part<br>
 4. Any other reason found at that point of time which is not acceptable</p>

  <br>
  <h3 class="p">To contact us for any clarifications please email to:</h3>
 
  <a href="mailto:support@pcskaka.com" class="a">support@pcskaka.com</a>
</main>