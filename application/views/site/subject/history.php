<link href="<?php echo base_url(); ?>assets/siteasset/css/subject/history.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>HISTORY</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
		
			<p><b>Civil services examination syllabus for History is vast compared to other subjects. As it covers data from Ancient History to Medieval History to Modern History and upto India after Independence. So basically, it’s everything comprising nearly 5000 years of Historical data.</b><br><br>
                <p>Let’s try to divide History into its respective sections:</p>
                <br>
                <p><h6>Ancient History: </h6>This section of History starts from very early stages of civilization, it includes Pre-Historic period, Harappan Culture, Vedic Period up to Gupta’s and HarshVardhan Period. While studying this section aspirant needs to pay a special attention to this period’s art and culture. For this reason, PCSKAKA has created a new section called art and culture, which gives aspirants questions specific to art and Culture.</p><br>

                <p><h6>Medieval History:</h6> This is the period which started from the existence of Delhi Sultanate and lasted up to Mughals. This period also contains details about Vijayanagara Kingdom. Aspirant need to study about the Sufi and Bhakti Movements of this period as well.</p><br>

                <p><h6>Modern India:</h6> This period started with arrival of Britishers into India. The period when tyrannical East India company established its roots in India. The start of Indian freedom movement. The formation of congress. Arrival of Gandhi and many more events which took place primarily between 1857 and 1947.<br><br>
                World History: Even though we try categorize events that happened during above mentioned period contemporary to India into World History, its not that simple. We need to holistically study the events of the world similar in manner to Indian History.</p><br>

                <h6>Following books are required for History syllabus preparation for prelims and mains perspective:</h6>
                <ol><li>History of Modern World – Jain and Mathur for World History</li>
                    <li>Mastering Modern World History-Norman Lowe for World History</li>
                    <li>India’s Struggle for Independence – Bipan Chandra for Modern History</li>
                    <li>India After Gandhi – Ramchandra Guha for Modern History</li>
                    <li>History Of Modern India – Bipan Chandra for Modern History</li>
                    <li>Indian Art and Culture – Nitin Singhania</li>
                    <li>India’s Ancient Past- RS Sharma for Ancient History</li>
                    <li>The Wonder That Was India – A.L. Basham for Ancient History</li>
                    <li>From Plassey to Partition for Modern History</li>
                    <li>NCERT books from class 6th to 10th </li>
                </ol>
                The detailed syllabus as prescribed by UPSC for history is presented below. It is advised that an Aspirant should understand this syllabus.<br>

                <ol><li>Sources Archaeological sources : Exploration, excavation, epigraphy, numismatics, monuments. Literary sources: Indigenous: Primary and secondary; poetry, scientific literature, literature, literature in regional languages, religious literature. Foreign account: Greek, Chinese and Arab writers.</li>
                    <li> Pre-history and Proto-history : Geographical factors; hunting and gathering (paleolithic and mesolithic); Beginning of agriculture (neolithic and chalcolithic). </li>
                    <li> Indus Valley Civilization : Origin, date, extent, characteristics-decline, survival and significance, art and architecture. </li>
                    <li> Megalithic Cultures : Distribution of pastoral and farming cultures outside the Indus, Development of community life, Settlements, Development of agriculture, Crafts, Pottery, and Iron industry. </li>
                    <li> Aryans and Vedic Period : Expansions of Aryans in India : Vedic Period: Religious and philosophic literature; Transformation from Rig Vedic period to the later Vedic period; Political, social and economical life; Significance of the Vedic Age; Evolution of Monarchy and Varna system.</li>
                     <li> Period of Mahajanapadas : Formation of States (Mahajanapada): Republics and monarchies; Rise of urban centres; Trade routes; Economic growth; Introduction of coinage; Spread of Jainism and Buddism; Rise of Magadha and Nandas. Iranian and Mecedonian invasions and their impact. </li>
                     <li> Mauryan Empire : Foundation of the Mauryan Empire, Chandragupta, Kautilya and Arthashastra; Ashoka; Concept of Dharma; Edicts; Polity, Administration, Economy; Art, architecture and sculpture; External contacts; Religion; Spread of religion; Literature. Disintegration of the empire; sungas and Kanvas. </li><li> Post-Mauryan Period (Indo-Greeks, Sakas, Kushanas, Western Kshatrapas) : Contact with outside world; growth of urban centres, economy, coinage, development of religions, Mahayana, social conditions, art, architecture, culture, literature and science. </li>
                     <li> Early State and Society in Eastern India, Deccan and South India: Kharavela, The Satavahanas, Tamil States of the Sangam Age; Administration, Economy, land grants, coinage, trade guilds and urban centres; Buddhist centres; Sangam literature and culture; Art and architecture. </li>
                    <li> Guptas, Vakatakas and Vardhanas: Polity and administration, Economic conditions, Coinage of the Guptas, Land grants, Decline of urban centres, Indian feudalism, Caste system, Position of women, Education and educational institutions; Nalanda, Vikramshila and Vallabhi, Literature, scientific literature, art and architecture. </li>
                    <li> Regional States during Gupta Era: The Kadambas, Pallavas, Chalukyas of Badami; Polity and Administration, Trade guilds, Literature; growth of Vaishnava and Saiva religions. Tamil Bhakit movement, Shankaracharya; Vedanta; Institutions of temple and temple architecture; Palas, Senas, Rashtrakutas, Paramaras, Polity and administration; Cultural aspects. Arab conquest of Sind; Alberuni, The Chaluky as of Kalyana, Cholas, Hoysalas, Pandyas; Polity and Administration; Local Government; Growth of art and architecture, religious sects, Institution of temple and Mathas, Agraharas, education and literature, economy and society.</li>
<li> Themes in Early Indian Cultural History: Languages and texts, major stages in the evolution of art and architecture, major philosophical thinkers and schools, ideas in Science and Mathematics.</li> 
<li> Early Medieval India, 750-1200: — Polity: Major political developments in Northern India and the peninsula, origin and the rise of Rajputs. — The Cholas: administration, village economy and society “Indian Feudalism”. — Agrarian economy and urban settlements. — Trade and commerce. — Society: the status of the Brahman and the new social order. — Condition of women. — Indian science and technology. </li>
<li> Cultural Traditions in India, 750-1200: — Philosophy: Skankaracharya and Vedanta, Ramanuja and Vishishtadvaita, Madhva and Brahma-Mimansa. — Religion: Forms and features of religion, Tamil devotional cult, growth of Bhakti, Islam and its arrival in India, Sufism. — Literature: Literature in Sanskrit, growth of Tamil literature, literature in the newly developing languages, Kalhan's Rajtarangini, Alberuni's India. — Art and Architecture: Temple architecture, sculpture, painting.</li> 
<li> The Thirteenth Century: — Establishment of the Delhi Sultanate: The Ghurian invasions - factors behind Ghurian success. — Economic, Social and cultural consequences. — Foundation of Delhi Sultanate and early Turkish Sultans. — Consolidation: The rule of Iltutmish and Balban.</li> 
<li> The Fourteenth Century: — “The Khalji Revolution”. — Alauddin Khalji: Conquests and territorial expansion, agrarian and economic measure. — Muhammad Tughluq: Major projects, agrarian measures, bureaucracy of Muhammad Tughluq. — Firuz Tugluq: Agrarian measures, achievements in civil engineering and public works, decline of the Sultanate, foreign contacts and Ibn Battuta's account.</li>
<li>Society, Culture and Economy in the Thirteenth and Fourteenth Centuries: — Society: composition of rural society, ruling classes, town dwellers, women, religious classes, caste and slavery under the Sultanate, Bhakti movement, Sufi movement. — Culture: Persian literature, literature in the regional languages of North India, literaute in the languages of South India, Sultanate architecture and new structural forms, painting, evolution of a composite culture. — Economy: Agricultural Production, rise of urban economy and non-agricultural production, trade and commerce.</li> 
<li> The Fifteenth and Early Sixteenth Century-Political Developments and Economy: — Rise of Provincial Dynasties : Bengal, Kashmir (Zainul Abedin), Gujarat. — Malwa, Bahmanids. — The Vijayanagara Empire. — Lodis. — Mughal Empire, first phase : Babur, Humayun. — The Sur Empire : Sher Shah’s administration. — Portuguese colonial enterprise, Bhakti and Sufi Movements. </li>
<li> The Fifteenth and Early Sixteenth Century- Society and culture: — Regional cultures specificities. — Literary traditions. — Provincial architectural. — Society, culture, literature and the arts in Vijayanagara Empire. </li>
<li> Akbar: — Conquests and consolidation of empire. — Establishment of jagir and mansab systems. — Rajput policy. — Evolution of religious and social outlook. Theory of Sulh-i-kul and religious policy. — Court patronage of art and technology. </li>
<li> Mughal Empire in the Seventeenth Century: — Major administrative policies of Jahangir, Shahjahan and Aurangzeb. — The Empire and the Zamindars. — Religious policies of Jahangir, Shahjahan and Aurangzeb. — Nature of the Mughal State. — Late Seventeenth Century crisis and the revolts. — The Ahom kingdom. — Shivaji and the early Maratha Kingdom. </li>
<li> Economy and society, in the 16th and 17th Centuries: Population Agricultural and craft production. — Towns, commerce with Europe through Dutch, English and French companies : a trade revolution. — Indian mercantile classes. Banking, insurance and credit systems. — Conditions of peasants, Condition of Women. — Evolution of the Sikh community and the Khalsa Panth.</li>
<li> Culture during Mughal Empire: — Persian histories and other literature. — Hindi and religious literatures. — Mughal architecture. — Mughal painting. — Provincial architecture and painting. — Classical music. — Science and technology. </li>
<li> The Eighteenth Century: — Factors for the decline of the Mughal Empire. — The regional principalities: Nizam’s Deccan, Bengal, Awadh. — Maratha ascendancy under the Peshwas. — The Maratha fiscal and financial system. — Emergence of Afghan power Battle of Panipat, 1761. — State of, political, cultural and economic, on eve of the British conquest</li>
<li> European Penetration into India: The Early European Settlements; The Portuguese and the Dutch; The English and the French East India Companies; Their struggle for supremacy; Carnatic Wars; Bengal-The conflict between the English and the Nawabs of Bengal; Siraj and the English; The Battle of Plassey; Significance of Plassey. </li>
<li> British Expansion in India: Bengal-Mir Jafar and Mir Kasim; The Battle of Buxar; Mysore; The Marathas; The three Anglo-Maratha Wars; The Punjab. </li>
<li> Early Structure of the British Raj: The Early administrative structure; From diarchy to direct contol; The Regulating Act (1773); The Pitt's India Act (1784); The Charter Act (1833); The Voice of free trade and the changing character of British colonial rule; The English utilitarian and India. </li>
<li> Economic Impact of British Colonial Rule: (a) Land revenue settlements in British India; The Permanent Settlement; Ryotwari Settlement; Mahalwari Settlement; Economic impact of the revenue arrangements; Commercialization of agriculture; Rise of landless agrarian labourers; Impoverishment of the rural society. (b) Dislocation of traditional trade and commerce; De-industrialisation; Decline of traditional crafts; Drain of wealth; Economic transformation of India; Railroad and communication network including telegraph and postal services; Famine and poverty in the rural interior; European business enterprise and its limitations.</li>
<li> Social and Cultural Developments: The state of indigenous education, its dislocation; Orientalist-Anglicist controversy, The introduction of western education in India; The rise of press, literature and public opinion; The rise of modern vernacular literature; Progress of Science; Christian missionary activities in India. </li>
<li> Social and Religious Reform Movements in Bengal and Other Areas: Ram Mohan Roy, The Brahmo Movement; Devendranath Tagore; Iswarchandra Vidyasagar; The Young Bengal Movement; Dayanada Saraswati; The social reform movements in India including Sati, widow remarriage, child marriage etc.; The contribution of Indian renaissance to the growth of modern India; Islamic revivalism-the Feraizi and Wahabi Movements. </li>
<li> Indian Response to British Rule: Peasant movement and tribal uprisings in the 18th and 19th centuries including the Rangpur Dhing (1783), the Kol Rebellion (1832), the Mopla Rebellion in Malabar (1841-1920), the Santal Hul (1855), Indigo Rebellion (1859-60), Deccan Uprising (1875) and the Munda Ulgulan (1899-1900); The Great Revolt of 1857 —Origin, character, casuses of failure, the consequences; The shift in the character of peasant uprisings in the post-1857 period; the peasant movements of the 1920s and 1930s. </li>
<li> Factors leading to the birth of Indian Nationalism; Politics of Association; The Foundation of the Indian National Congress; The Safety-valve thesis relating to the birth of the Congress; Programme and objectives of Early Congress; the social composition of early Congress leadership; the Moderates and Extremists; The Partition of Bengal (1905); The Swadeshi Movement in Bengal; the economic and political aspects of Swadeshi Movement; The beginning of revolutionary extremism in India. </li>
<li> Rise of Gandhi; Character of Gandhian nationalism; Gandhi's popular appeal; Rowlatt Satyagraha; the Khilafat Movement; the Non-cooperation Movement; National politics from the end of the Non-cooperation movement to the beginning of the Civil Disobedience Movement; the two phases of the Civil Disobedience Movement; Simon Commission; The Nehru Report; the Round Table Conferences; Nationalism and the Peasant Movements; Nationalism and Working class movements; Women and Indian youth and students in Indian politics (1885-1947); the election of 1937 and the formation of ministries; Cripps Mission; the Quit India Movement; the Wavell Plan; The Cabinet Mission. </li>
<li> Constitutional Developments in the Colonial India between 1858 and 1935. </li>
<li> Other strands in the National Movement. The Revolutionaries: Bengal, the Punjab, Maharashtra, U.P. the Madras Presidency, Outside India. The Left; The Left within the Congress: Jawaharlal Nehru, Subhas Chandra Bose, the Congress Socialist Party; the Communist Party of India, other left parties.</li>
<li> Politics of Separatism; the Muslim League; the Hindu Mahasabha; Communalism and the politics of partition; Transfer of power; Independence.</li> 
<li> Consolidation as a Nation; Nehru's Foreign Policy; India and her neighbours (1947-1964); The linguistic reorganisation of States (1935-1947); Regionalism and regional inequality; Integration of Princely States; Princes in electoral politics; the Question of National Language. </li>
<li> Caste and Ethnicity after 1947; Backward Castes and Tribes in post-colonial electoral politics; Dalit movements. </li>
<li> Economic development and political change; Land reforms; the politics of planning and rural reconstruction; Ecology and environmental policy in post-colonial India; Progress of Science.</li>
<li> Enlightenment and Modern ideas: (i) Major Ideas of Enlightenment : Kant, Rousseau. (ii) Spread of Enlightenment in the colonies. (iii) Rise of socialist ideas (up to Marx); spread of Marxian Socialism. </li>
<li> Origins of Modern Politics : (i) European States System. (ii) American Revolution and the Constitution. (iii) French Revolution and Aftermath, 1789-1815. (iv) American Civil War with reference to Abraham Lincoln and the abolition of slavery. (v) British Democratic politics, 1815-1850 : Parliamentary Reformers, Free Traders, Chartists. </li>
<li> Industrialization : (i) English Industrial Revolution : Causes and Impact on Society. (ii) Industrialization in other countries : USA, Germany, Russia, Japan. (iii) Industrialization and Globalization. </li>
<li> Nation-State System : (i) Rise of Nationalism in 19th century. (ii) Nationalism : State-building in Germany and Italy. (iii) Disintegration of Empires in the face of the emergence of nationalities across the World.</li> 
<li> Imperialism and Colonialism : (i) South and South-East Asia. (ii) Latin America and South Africa. (iii) Australia. (iv) Imperialism and free trade: Rise of neo-imperialism. </li>
<li> Revolution and Counter-Revolution : (i) 19th Century European revolutions. (ii) The Russian Revolution of 1917-1921. (iii) Fascist Counter-Revolution, Italy and Germany. (iv) The Chinese Revolution of 1949.</li>
<li> World Wars : (i) 1st and 2nd World Wars as Total Wars : Societal implications. (ii) World War I : Causes and Consequences. (iii) World War II : Causes and Consequences. </li>
<li> The World after World War II: (i) Emergence of Two power blocs. (ii) Emergence of Third World and non-alignment. (iii) UNO and the global disputes. </li>
<li> Liberation from Colonial Rule : (i) Latin America-Bolivar. (ii) Arab World-Egypt. (iii) Africa-Apartheid to Democracy. (iv)South-East Asia-Vietnam. </li>
<li> Decolonization and Underdevelopment : (i) Factors constraining Development ; Latin America, Africa. </li>
<li> Unification of Europe : (i) Post War Foundations ; NATO and European Community. (ii) Consolidation and Expansion of European Community (iii) European Union. </li>
<li> Disintegration of Soviet Union and the Rise of the Unipolar World : (i) Factors leading to the collapse of Soviet Communism and Soviet Union, 1985-1991. (ii) Political Changes in East Europe 1989-2001. (iii) End of the Cold War and US Ascendancy in the World as the lone superpower.</li>


                </ol>

			<p>If you need further help, please email us at:</p><br>
			<a href="mailto:support@pcskaka.com">support@pcskaka.com</a></b></p>
		</div>
				
	</div>
		
	
</div>
