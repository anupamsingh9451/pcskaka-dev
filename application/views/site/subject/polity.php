<link href="<?php echo base_url(); ?>assets/siteasset/css/subject/polity.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>POLITY</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
		
			<p>The system of government called polity is midway between democracy and oligarchy. The subject deals with the functioning of the government in the country. This gives an understanding of the legislative, executive, judicial systems and various constitutional authorities in the country.<br>
            Compared to previous year, Polity is moving towards governance and public administration type questions eg- Chief Secretary, Gram Nyayalayas.
            <br><br>

            <h5>Books required for Polity Syllabus for prelims and mains:</h5>
           <ol> 
            <li>Indian Polity for Civil Services Examinations – M. Laxmikanth</li>
            <li>Important Judgments that Transformed India – Alex Andrews George</li>
            <li>Introduction to the Constitution of India – Durga Das Basu</li>
            <li>Our Constitution Paperback – Subhash C. Kashyap</li>
            <li>Our Parliament – Subhash Kashyap</li></ol>

            <p>Following areas are to covered from prelims exam perspective:</p>
            <br>
            <h5>Preamble</h5>
            <ol>
                <li>Features of preamble</li>
                <li>42nd Amendment</li>
                <li>Swaran Singh committee</li>
            </ol>

            <h5>Schedules</h5>
            <ol><li>Basic idea about 12 schedules</li>
                <h5>Constitution of India</h5>
                <li>Basic idea about All articles</li>
                <li>Historical Background</li>
                <li>Drafting committee and making of the Constitution</li>
                <li>Influence of other constitutions</li>
                <li>Its salient features</li>
                <li>Union and its Territory
                    <ul><li> Basic idea about Article 1-4</li>
                        <li> State reorganization and different Commissions</li>
                        <li> Federal nature</li>
                        <li> Recent issues</li>
                    </ul>
                </li>
            </ol>
            <ul><li><p> Citizenship</p></li>
                <ol><li>Basic idea about Article 5-11</li>
                    <li>PIO, NRI, OCI and Pravasi Bharathiya Divas</li>
                    <li>Privileges available for Indian citizens and foreigners</li>
                    <li>Citizenship Amendment Act 2016</li>
                    <li>New policies, schemes and recent changes in voting.</li>
                </ol>
                <li> <p>Fundamental Rights (FR)</p></li>
                <ol><li>Basic idea about Article 12-35</li>
                    <li>A thorough understanding about Articles 14- 30 and Art. 32</li>
                    <li>Rights and privileges available to citizens of India only and both to citizens and foreigners</li>
                    <li>44th amendment act</li>
                    <li>Different types of Writs</li>
                    <li>Enforcement and Exceptional cases with regard to FR’s</li>
                    <li>RTE and recent issues related to FR</li>
                </ol>
                <li><p> Fundamental Duties(FD)</p></li>
                <ol><li>Article 51A</li>
                    <li>Difference between FR and FD</li>
                    <li>Significance and Criticism</li>
                    <li>Enforcement of FD’s</li>
                    <li>Recent issues about FD</li>
                </ol>
                <li><p>Directive Principles of State Policy (DPSP)</p></li>
                <ol><li>Basic idea about Article and Article 36-51 and Article 368</li>
                    <li>Sources and key features of DPSP</li>
                    <li>Classification of DPSP</li>
                    <li>Comparison/ conflicts between Fundamental Rights and Directive Principles</li>
                    <li>Keshavananda Bharathi, Minerva Mills, Golaknath Case, Maneka Gandhi case.</li>
                    <li>Important Amendments- 42nd Amendment, 44th Amendment, and 97th amendment</li>
                </ol>
                <li><p> Union </p></li>
                <ol><li>Basic idea about Article 52-73</li>
                    <li>Qualification and Election</li>
                    <li>Function and Powers- (Executive, Legislative, Financial, Judicial, Diplomatic, Military and Emergency Powers)</li>
                    <li>Resignation and impeachment</li>
                    <li>Role and responsibilities and relationship with Prime minister, Council of Minister, Cabinet ministers.</li>
                    <li>Prime minister and council of minister- Basic idea about Article 74-75</li>
                    <li>Powers and Functions</li>
                    <li>Council of ministers</li>
                    <li>Resignation and Removal</li>
                    <li>Attorney general</li>
                </ol>
                <li><p> Parliament</p></li>
                <ol><li>Basic idea about Article related</li>
                    <li>Role and functions of the Parliament</li>
                    <li>Sessions, Motions, Parliamentary procedure – Summoning, Prorogation, Joint sitting</li>
                    <li>Parliamentary proceedings like Question Hour, Zero Hour, and Adjournment Motion etc.</li>
                    <li>Lok Sabha and Rajya Sabha,</li>
                    <li>Special powers of Rajya Sabha</li>
                    <li>Anti defection law and 10th schedule</li>
                    <li>Parliamentary Privileges</li>
                    <li>Bill and law making procedure</li>
                    <li>Budget, funds and its summary</li>
                    <li>Parliamentary Committees</li>
                </ol>
                <li><p>Judiciary</p></li>
                <ol><li>Basic idea about Article related to the judiciary.</li>
                    <li>Powers of Supreme court and high court</li>
                    <li>Qualification and appointment</li>
                    <li>Removal procedure</li>
                    <li>Recent controversy, verdicts, and constitutional provisions.</li>
                </ol>
                <li><p>State Government- State Executive</p></li>
                <ol><li>Governor- appointment, removal and special powers.</li>
                    <li>Executive, Legislative, Financial, Judicial powers and discretionary of governor</li>
                    <li>7th constitutional amendment</li>
                    <li>Chief minister and council of ministers</li>
                    <li>Power of chief minister</li>
                </ol>
                <li><p> State Legislature</p></li>
                <ol><li>State legislature compared to the Parliament with regard to composition,  powers, and functions.</li>
                    <li>Bicameral legislatures</li>
                    <li>Creation and abolition of the Legislative councils</li>
                </ol>
                <li><p>Administration of Union Territories (UT)</p></li>
                <ol><li>Special provision for Delhi</li>
                    <li>Administration and jurisdiction in UT’s</li>
                </ol>
                <li><p>Administration of Special Areas</p></li>
                <ol><li>Basic idea about 5thSchedule 6th Schedule</li>
                    <li>Recent issues related to Administration of Special Areas</li>
                    <li>Special provision for Jammu and Kashmir-Article 370</li>
                    <li>Difference between constitutional provisions related to Jammu and Kashmir</li>
                </ol>
                <li><p>Emergency Provisions</p></li>
                <ol><li>National emergency- Article 352</li>
                    <li>President’s rule or State emergency- Article 356</li>
                    <li>Financial emergency- Article 360</li>
                    <li>44th amendment act</li>
                    <li>Effects and implications of emergency</li>
                    <li>Role of President in emergency time</li>
                    <li>The State of FR, Lok sabha, and Rajya sabha</li>
                    <li>Revoking emergency</li>
                </ol>
                <li><p>State- centre and interstate relations</p></li>
                <ol><li>Basic idea about Articles 262 and 263</li>
                    <li>Composition and functions of Interstate council and Zonal council</li>
                    <li>Inter-State trade and Commerce</li>
                    <li>Recent disputes between states, controversies etc</li>
                    <li>New policies or schemes which impact interstate relations</li>
                </ol>
                <li><p>Panchayati raj and municipalities</p></li>
                <ol><li>Elections, auditing, powers and authority of panchayats</li>
                    <li>3 tier structure</li>
                    <li>73rd Amendment Act and 74th Amendment Act</li>
                    <li>Relation with FR and DPSP</li>
                    <li>Schemes introduced</li>
                    <li>Metropolitan planning committee and urban development</li>
                    <li>Reservation</li>
                </ol>
                <li><p> Constitution Bodies</p></li>
                <ol><li>Election Commission</li>
                    <li>UPSC</li>
                    <li>SPSC</li>
                    <li>JPSC</li>
                    <li>Finance Commission</li>
                    <li>National Commission for SCs and ST’s,</li>
                    <li>Composition, Powers and functions, Removal of the Constitutional bodies</li>
                </ol>
                <li><p>Non-Constitutional Bodies</p></li>
                <ol><li>Basic idea about Composition, Functions, Working of the Non-Constitutional bodies such as National Human Rights Commission, Central Information Commission, Central Vigilance Commission, Central Bureau of Investigation, State Human Rights Commission, State Information Commission etc</li>
                </ol>
                <li><p>Tribunals</p></li>
                <ol><li>Basic idea about Article 323A and tribunals under Article 323B</li>
                    <li>Recent controversial issues related to tribunals</li>
                    <li>Different tribunals and importance</li>
                </ol>
                <li><p>Special Provisions for SC’s, ST’s, Backward Classes, Minorities and Anglo-Indians</p></li>
                <ol><li>Privileges and right issued to SC’s, ST’s, Backward Classes, Minorities and Anglo-Indians</li>
                    <li>Issues related to vulnerable sections like women, child, SC’s, ST’s, Backward Classes, Minorities and Anglo-Indians</li>
                </ol>
                <li><p> Current affairs</p></li>
                <ol><li>Recent issues related to above-mentioned categories</li>
                    <li>Important schemes, programs, missions, laws, and policies launched by the government.</li>
                    <li>Recent Government Bills and Governance- actions</li>
                </ol>
            </ul>
			<p>If you need further help, please email us at:</p><br>
			<a href="mailto:support@pcskaka.com">support@pcskaka.com</a></b></p>
		</div>
				
	</div>
		
	
</div>
