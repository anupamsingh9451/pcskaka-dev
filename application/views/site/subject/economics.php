<link href="<?php echo base_url(); ?>assets/siteasset/css/subject/economics.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>ECONOMICS</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
		    <p>
			Economic and Social Development: sustainable development, poverty, inclusion, demographics, social sector initiatives, etc.
            Here, an Aspirant need to focus on economic growth and development, budget, balance of payments, poverty, banking, finance and other related issues. Population composition and related characteristics, social sector initiatives related to education, health and sanitation, and international financial institutions.
            Books required for Economics syllabus for prelims and mains:</p>
            <ol>
                <li>Indian Economy by Ramesh Singh</li>
                <li>Indian Economy Key Concepts by Sankarganesh K</li>
                <li>The Indian Economy by Sanjiv Verma</li>
                <li>Datt and Sundaram’s Indian Economy</li>
                <li>Indian Economy by Uma Kapila</li>
                <li>NCERT from 6th to 10th </li>
            </ol>
            <p>For prelims point of view, aspirant need to pay special attention to the following fields of study:</p>
            <ul>
                <li>Economic growth and development – basic concept and definition of economy and economics, uses and transfer of resources, distributive effects, macro and micro economic policy, micro-macro balance, distributive impact of economic policies, development versus growth, determinant of growth and development, concepts such as HPI/MPI, HDI, PQLI, GEM, GDI/GII, TAI, Green index, sustainable development, India’s ranking in the various indices.</li>
                <li>Poverty – definitions, causes, distribution-deprivation, income versus calories, measurement of poverty, status of poverty, eradication programmes, poverty and resource policy, tribal rights and issues, livelihood mission.</li>
                <li>Inclusion – definition, relevance, types, financial inclusion, recent initiatives.</li>
                <li>Demographics – census data, populations by gender, by state, by age group, socio-economic status, caste, religion, literacy levels, etc. Trends in human development – interstate comparison, etc.</li>
                <li>Fiscal policy – definition, component, receipts, revenue and capital account, tax revenue, expenditure, budget.</li>
             </ul>
		</div>
				<p>If you need further help, please email us at:</p><br>
			<a href="mailto:support@pcskaka.com">support@pcskaka.com</a></b></p>	
	</div>
</div>
