<link href="<?php echo base_url(); ?>assets/siteasset/css/subject/geography.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>GEOGRAPHY</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
	
			<p><b>Geography is a field of science devoted to the study of the lands, features, inhabitants, and phenomena of the Earth and planets. Geography is an all-encompassing discipline that seeks an understanding of Earth and its human and natural complexities—not merely where objects are, but also how they have changed and come to be.</b><br><br>

                For our prelims exam perspective, we will divide Geography into following sections:<br>
                <ol><li><font style="font-weight: bold;">Physical Geography:</font> It is the branch of geography dealing with natural features. It deals with the study of processes and patterns in the natural environment such as  like the atmosphere, hydrosphere, biosphere, and geosphere, It involves deeper understanding of concepts and phenomenon rather than just memorizing facts.</li>
                <li><font style="font-weight: bold;">Indian Geography:</font> This section involves more focus on the Indian Geographical patterns and concepts of geography on making and breaking of Indian subcontinent. Special emphasis is required on facts and figures related to Indian boundaries.</li>
                <li><font style="font-weight: bold;">World Geography:</font> Study of physical geography with respect to rest of the world excluding India can be considered as the subject matter of this section. This requires both approaches explained in above 2 sections, ie. Understanding concepts, phenomenon, principles, etc along with memorizing facts and figures about rest of the world.</li>
            </ol>

            <h6>Books required for Geography syllabus for prelims and mains:</h6>
            <ol><li>Geography of India by Majid Hussain</li>
                <li>World Geography by Majid Hussain</li>
                <li>Certificate of Physical and Human Geography by Goh Cheng Leong</li>
                <li>Physical, Human and Economic Geography for Civil Services Examination by D R Khuller</li>
            </ol><br>
            Let’s holistically present the syllabus as described by UPSC:
            <ol><li>Geomorphology : Factors controlling landform development; endogenetic and exogenetic forces; Origin and evolution of the earth’s crusts; Fundamentals of geomagnetism; Physical conditions of the earth’s interior; Geosynclines; Continental drift; Isostasy; Plate tectonics; Recent views on mountain building; Volcanicity; Earthquakes and Tsunamis; Concepts of geomorphic cycles and Land scape development; Denudation chronology; Channel morphology; Erosion surfaces; Slope development; Applied Geomorphology; Geomorphology, economic geology and environment.</li>
<li>Climatology : Temperature and pressure belts of the world; Heat budget of the earth; Atmospheric circulation; Atmospheric stability and instability. Planetary and local winds; Monsoons and jet streams; Air masses and fronto; Temperate and tropical cyclones; Types and distribution of precipitation; Weather and Climate; Koppen’s Thornthwaite’s and Trewar Tha’s classification of world climate; Hydrological cycle; Global climatic change, and role and response of man in climatic changes Applied climatology and Urban climate.</li>
<li>Oceanography : Bottom topography of the Atlantic, Indian and Pacific Oceans; Temperature and salinity of the oceans; Heat and salt budgets, Ocean deposits; Waves, currents and tides; Marine resources; biotic, mineral and energy resources; Coral reefs coral bleaching; Sea-level changes; Law of the sea and marine pollution. </li>
<li>Biogeography : Genesis of soils; Classification and distribution of soils; Soil profile; Soil erosion, Degrada-tion and conservation; Factors influencing world distribution of plants and animals; Problems of deforestation and conservation measures; Social forestry, agro-forestry; Wild life; Major gene pool centres.</li>
<li>Environmental Geography : Principle ecology; Human ecological adaptations; Influence of man on ecology and environment; Global and regional ecological changes and imbalances; Ecosystem their management and conservation; Environmental degradation, management and conservation; Biodiversity and sustainable development; Environmental policy; Environmental hazards and remedial measures; Environmental education and legislation</li>
<li>Perspectives in Human Geography : Areal differentiation; Regional synthesis; Dichotomy and dualism; Environmentalism; Quantitative revolution and locational analysis; Radical, behavioural, human and welfare approaches; Languages, religions and secularisation; Cultural regions of the world; Human development index. </li>
<li>Economic Geography : World economic development: measurement and problems; World resources and their distribution; Energy crisis; the limits to growth; World agriculture: typology of agricultural regions; Agricultural inputs and productivity; Food and nutritions problems; Food security; famine: causes, effects and remedies; World industries: location patterns and problems; Patterns of world trade.</li>
<li>Population and Settlement Geography : Growth and distribution of world population; Demographic attributes; Causes and consequences of migration; Concepts of over-under-and optimum population; Population theories, world population problems and policies, Social well-being and quality of life; Population as social capital. Types and patterns of rural settlements; Environmental issues in rural settlements; Hierarchy of urban settlements; Urban morphology; Concept of primate city and rank-size rule; Functional classification of towns; Sphere of urban influence; Rural-urban fringe; Satellite towns; Problems and remedies of urbanization; Sustainable development of cities.</li>
<li>Concept of a region; Types of regions and methods of regionalisation; Growth centres and growth poles; Regional imbalances; Regional development strategies; Environmental issues in regional planning; Planning for sustainable development.</li>
<li>Models, Theories and Laws in Human Geography : System analysis in Human geography; Malthusian, Marxian and demographic transition models; Central Place theories of Christaller and Losch; Perroux and Boudeville; Von Thunen’s model of agricultural location; Weber’s model of industrial location; Ostov’s model of stages of growth. Heart-land and Rimland theories; Laws of international boundaries and frontiers.</li>
<li>Physical Setting : Space relationship of India with neighbouring countries; Structure and relief; Drainage system and watersheds; Physiographic regions; Mechanism of Indian monsoons and rainfall patterns; Tropical cyclones and western disturbances; Floods and droughts; Climatic regions; Natural vegetation, Soil types and their distributions.</li>
<li>Resources : Land, surface and ground water, energy, minerals, biotic and marine resources, Forest and wild life resources and their conservation; Energy crisis.</li>
<li>Agriculture : Infrastructure: irrigation, seeds, fertilizers, power; Institutional factors; land holdings, land tenure and land reforms; Cropping pattern, agricultural productivity, agricultural intensity, crop combination, land capability; Agro and social-forestry; Green revolution and its socio-economic and ecological implications; Significance of dry farming; Livestock resources and white revolution; Aqua-culture; Sericulture, Agriculture and poultry; Agricultural regionalisation; Agro-climatic zones; Agro-ecological regions</li>
<li>Industry : Evolution of industries; Locational factors of cotton, jute, textile, iron and steel, aluminium, fertiliser, paper, chemical and pharmaceutical, automobile, cottage and ago-based industries; Industrial houses and complexes including public sector underkings; Industrial regionalisation; New industrial policy;</li>
<li>Transport, Communication and Trade : Road, railway, waterway, airway and pipeline net works and their complementary roles in regional development; Growing importance of ports on national and foreign trade; Trade balance; Trade Policy;Export processing zones; Developments in communication and information technology and their impacts on economy and society; Indian space programme.</li>
<li>Cultural Setting : Historical Perspective of Indian Society; Racial linguistic and ethnic diversities; religious minorities; Major tribes, tribal areas and their problems; Cultural regions; Growth, distribution and density of population; Demographic attributes: sex-ratio, age structure, literacy rate, work-force, dependency ratio, longevity; migration (inter-regional, interaregional and international) and associated problems; Population problems and policies; Health indicators.</li>
<li>Settlements : Types, patterns and morphology of rural settlements; Urban developments; Morphology of Indian cities; Functional classification of Indian cities; Conurbations and metropolitan regions; Urban sprawl; Slums and asssociated problems; Town planning; Problems of urbanisation and remedies.</li>
<li>Regional Development and Planning: Experience of regional planning in India; Five Year Plans; Integrated rural development programmes; Panchayati Raj and decentralised planning; Command area development; Watershed management; Planning for backward area, desert, drought-prone, hill tribal area development; Multi-level planning; Regional planning and development of island territories.</li>
<li>Political Aspects : Geographical basis of Indian federalism; State reorganisation; Emergence of new states; Regional consciousness and inter-state issues; International boundary of India and related issues; Cross-border terrorism; India’s role in world affairs; Geopolitics of South Asia and Indian Ocean realm.</li>
<li>Contemporary Issues : Ecological issues: Environmental hazards: landslides, earthquakes, Tsunamis, floods and droughts, epidemics; Issues related to environmental pollution; Changes in patterns of land use; Principles of environmental impact assessment and environmental management; Population explosion and food security; Environmental degradation; Deforestation, desertification and soil erosion; Problems of agrarian and industrial unrest; Regional disparities in economic development; Concept of sustainable growth and development; Environmental awareness; Linkage of rivers; Globalisation and Indian economy.</li>
</li></ol>
	<p>If you need further help, please email us at:</p><br>
			<a href="mailto:support@pcskaka.com">support@pcskaka.com</a></b></p>
		</div>
				
	</div>
		
	
</div>
