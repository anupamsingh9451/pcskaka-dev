<link href="<?php echo base_url(); ?>assets/siteasset/css/subject/environment.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>ENVIRONMENT</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
		
			<p>CSE syllabus says: General issues on<b>Environmental Ecology, Biodiversity and Climate Change–</b> that do not require subject specialization<br>

                Aspirants worry about these topics as they are not as ‘concrete’ as other topics like History or Indian Economy. The study material is not as ‘time-tested’ as in other subjects. Also, there is a very high correlation to current affairs in these sections. So, you have to be very alert to any relevant news. Additionally, these topics are constantly in the news these days due to concerns about the environment, climate change, etc. assuming more priority with governments and world bodies.<br>
                <br>
            Books required for Environment syllabus for prelims and mains:
            <ol><li>Environment by Shankar IAS Academy</li>
                <li>Environment And Ecology – A Complete Guide – by R. Rajagopalan (Lexis Nexis)</li>
                <li>Environment for Civil Services Prelims and Mains – by Khuller and Rao (McGraw Hill)</li>
                <li>Environment and Ecology – Biodiversity, Climate Change and Disaster Management – Majid Husain (Access Publishers)</li>
                <li>Efforts Towards Green India – Environment & Ecology by Arihant Experts</li>
            </ol>
            The following topics is a MUST for prelims examination:
            <ol><li>Biodiversity – importance for human survival, International Union for Conservation of Nature and Natural Resources, biodiversity hot spots, Red Data Book.
                <li>General topics from environmental ecology – terms such as ecotone, ecosystems, effects of environmental degradation, food chain, etc.</li>
                <li>Pollution – acid rain, smog, greenhouse gases, algal bloom, ozone layer.</li>
                <li>Sustainable Development – renewable energy sources, bio-fertilizers, bio-pesticides, biomass gasification.</li>
                <li>Conservation – conservation of natural resources, wildlife conservation, national parks, wetlands, biosphere reserves.</li>
                <li>Climate change – Kyoto Protocol, Montreal Protocol, climate change summits held.</li>
                <li>Ecologically sensitive areas – Western Ghats, Himalayas.</li>
                <li>Laws, regulatory bodies and policies at national and international level – Environment Protection Act, National Biodiversity Authority, Forest Right Act, protocols and summits like Nagoya Protocol, Cartagena Protocol and Lima Conference, etc.</li>
                <li>UNEP, FAO, UNESCO, etc.</li>
                <li>Ramsar Convention, Montreux Record, the three Rio Conventions—Convention on Biological Diversity (CoB), United Nations Framework Convention on Climate Change (UNFCCC) and United Nations Convention to Combat Desertification – from the 1992 Earth Summit, etc.</li>
                <li>Carbon trading and leakage issues, Kyoto mechanism, emission trading, etc.</li>
</li></ol>

				<p>If you need further help, please email us at:</p><br>
			<a href="mailto:support@pcskaka.com">support@pcskaka.com</a></b></p>
		</div>
				
	</div>
		
	
</div>
