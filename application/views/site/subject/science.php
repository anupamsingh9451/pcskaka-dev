<link href="<?php echo base_url(); ?>assets/siteasset/css/subject/science.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>SCIENCE</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
		
			<p><b>General Science can be classified into three:</b><br>
                <h6></h6>
                <ol><li>Biology</li>
                    <li>Chemistry</li>
                    <li>Physics</li>
                </ol>
                There is no specific defined syllabus for Science. Aspirant need to study books of NCERT from 6th to 12th standard with special focus up to class 10th. <br>
                Books required for Science syllabus for prelims and mains:

                <ol><li>General Science: Lucent’s</li>
                    <li>Physics: NCERT books from class 6th to 12th</li>
                    <li>Chemistry: NCERT books from class 6th to 12th</li>
                    <li>Biology: NCERT books from class 6th to 12th</li>
                </ol>

                Let’s try to break the syllabus in smaller parts for better understanding of the scope of study:
                <ol>
                    <li>Universe – Big Bang, Redshift, Blueshift</li>
                    <li>Star Formation – Stellar Evolution, Life Cycle of A Star</li>
                    <li>Solar System Formation – Nebular Theory of Laplace</li>
                    <li>Solar System – Planets, Inner Planets, Outer Planets</li>
                    <li>Sun – Internal Structure, Atmosphere</li>
                    <li>Nuclear Fission, Nuclear Reactor Types</li>
                    <li>India’s Three-Stage Nuclear Power Programme</li>
                    <li>Cell Organelles – Plant Cell vs. Animal Cell</li>
                    <li>Carbohydrates – Monosaccharides, Polysaccharides</li>
                    <li>Proteins – Amino Acids, Enzymes</li>
                    <li>Vitamins and Minerals – Deficiency Diseases</li>
                    <li>Fats – Healthy Fats and Unhealthy Fats</li>
                    <li>Animal Tissues – Epithelium, Connective Tissues</li>
                    <li>Human Digestive System – Digestive Glands</li>
                    <li>Respiratory System – NCERT General Science</li>
                    <li>Endocrine Glands and Hormones</li>
                    <li>Human Neural System – Human Brain</li>
                    <li>Muscular and Skeletal System</li>
                    <li>Nucleic acids – DNA and RNA, Recombinant DNA</li>
                    <li>Mitosis – Cell Cycle, Cell Division, Meiosis – Mitosis – Meiosis Comparison</li>
                    <li>Inheritance – Mendel’s Laws of Inheritance, Chromosomal Theory, Human Genome Project</li>
                    <li>Sex Determination – Genetic Disorders</li>
                    <li>Diseases Caused by Microorganisms</li>
                    <li>Microbes in Human Welfare – Useful Microbes</li>
                    <li>Immunity – Human Immune System</li>
                    <li>AIDS, Cancer – causes</li>
                    <li>Drugs and Alcohol Abuse</li>
                    <li>Diseases – Acute, Chronic, Communicable Diseases</li>
                    <li>Blood – Blood Groups – Formed Elements</li>
                    <li>Circulatory System, Double Circulation</li>
                    <li>Excretory System – Kidney, Urine Formation</li>
                    <li>Origin and Evolution of Life on Earth</li>
                    <li>Biological Classification</li>
                    <li>Five Kingdom Classifications of Plants and Animals</li>
                    <li>Plant Parts and Their Functions</li>
                    <li>Plant Kingdom – Thallophytes (Algae), Bryophytes, Pteridophytes</li>
                    <li>Plants with Seeds – Gymnosperms and Angiosperms</li>
                    <li>Plant Tissue – Meristematic – Simple, Complex Permanent Tissue</li>
                    <li>Plant Nutrition – Photosynthesis, Nitrogen Cycle, Fixation</li>
                    <li>Sexual and Asexual Reproduction in Plants</li>
                    <li>Classification of Animal Kingdom (Animalia)</li>
                    <li>Classification of Vertebrata (Phylum Chordata)</li>
                    <li>Human Reproductive System</li>
                    <li>Biotechnology – Genetic Engineering – Processes and Applications</li>
                    <li>Atomic Theory – Structure of an Atom</li>
                    </li>
                </ol>

                A special reference to reading of THE HINDU is required. Try to make notes of Science and Technology section of this newspapers.

			<p>If you need further help, please email us at:</p><br>
			<a href="mailto:support@pcskaka.com">support@pcskaka.com</a></b></p>
		</div>
				
	</div>
		
	
</div>
