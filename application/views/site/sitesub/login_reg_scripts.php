<script>
    $('document').ready(function(){
        $('body').on('blur','.regemail', function () {
            var email = $('.regemail').val();
            var emailcnfrm = $('.emailcnfrm').val();
            if(isEmail($(this).val())){
                $('.regemail').css({ "border": "none"});
                $('.btn-register').attr("disabled", false);
                if(emailcnfrm!=''){
                    if (email == emailcnfrm && isEmail(email)  && isEmail(emailcnfrm)){
                        $('.regemail').css({ "border": "none"});
                        $('.emailcnfrm').css({ "border": "none"});
                        $('.btn-register').attr("disabled", false);
                    }else{
                        $('.emailcnfrm').css({ "border": "1px solid #d88080"});
                        $('.btn-register').attr("disabled", 'disabled');
                    }
                }
            }else{
                if($(this).val()!=''){
                    $('.regemail').css({ "border": "1px solid #d88080"});
                    $('.btn-register').attr("disabled", "disabled");
                }else{
                    $('.regemail').css({ "border": "none"});
                    $('.btn-register').attr("disabled", false);
                }
                
            }
        });
        $('body').on('blur','.emailcnfrm', function () {
            var email = $('.regemail').val();
            var emailcnfrm = $('.emailcnfrm').val();
            if(isEmail($(this).val()) && $(this).val()!=''){
                $('.emailcnfrm').css({ "border": "none"});
                $('.btn-register').attr("disabled", false);
                if(email!=''){
                    if (email == emailcnfrm && isEmail(email) && isEmail(emailcnfrm)){
                        $('.regemail').css({ "border": "none"});
                        $('.emailcnfrm').css({ "border": "none"});
                        $('.btn-register').attr("disabled", false);
                    }else{
                        
                        $('.emailcnfrm').css({ "border": "1px solid #d88080"});
                        $('.btn-register').attr("disabled", "disabled");
                    }
                }
            }else{
                if($(this).val()!=''){
                    $('.emailcnfrm').css({ "border": "1px solid #d88080"});
                    $('.btn-register').attr("disabled", "disabled");
                }else{
                    $('.emailcnfrm').css({ "border": "none"});
                    $('.btn-register').attr("disabled", false);
                }
            }
        });
        
        
        $('body').on('keyup','.regpassword, .cnfrmpassword', function () {
            var cnfrmpass = $('.cnfrmpassword').val();
            var password = $('.regpassword').val();
            if(password!=''){
                if(password!='' && cnfrmpass!=''){
                    if (password != cnfrmpass){
                        $('.cnfrmpassword').css({ "border": "1px solid #d88080"});
                        $('.btn-register').attr("disabled", "disabled")
                    }
                    if (password == cnfrmpass){
                        $('.regpassword').css({ "border": "none"});
                        $('.cnfrmpassword').css({ "border": "none"});
                        $('.btn-register').attr("disabled", false);
                    }
                }
            }else{
                if(cnfrmpass!=''){
                    $('.cnfrmpassword').css({ "border": "1px solid #d88080"});
                    $('.btn-register').attr("disabled", "disabled");
                }
                
            }
            
        });
        $('body').on('keyup','.otp', function () {
            var otp = $(this).val();
            if(otp!=''){
                 $('.otp_error').css({ "display": "none"});
            }else{
                 $('.otp_error').css({ "display": "block"});
            }
            
        });
        $('body').on('keyup','.regpasswordforgot, .cnfrmpasswordforgot', function () {
            var cnfrmpass = $('.cnfrmpasswordforgot').val();
            var password = $('.regpasswordforgot').val();
            if(password!=''){
                if(password!='' && cnfrmpass!=''){
                    if (password != cnfrmpass){
                        $('.cnfrmpasswordforgot').css({ "border": "1px solid #d88080"});
                        $('.btn-forgot-confirmpas').attr("disabled", "disabled")
                    }
                    if (password == cnfrmpass){
                        $('.regpasswordforgot').css({ "border": "none"});
                        $('.cnfrmpasswordforgot').css({ "border": "none"});
                        $('.btn-forgot-confirmpas').attr("disabled", false);
                    }
                }
            }else{
                if(cnfrmpass!=''){
                    $('.cnfrmpasswordforgot').css({ "border": "1px solid #d88080"});
                    $('.btn-forgot-confirmpas').attr("disabled", "disabled");
                }
                
            }
            
        });
        $('body').on('keyup','.name', function () {
            if($(this).val()!=''){
                $("#student_reg").find('.name').removeClass('redborder');
            }
        });
        $('body').on('keyup','.regemail', function () {
            if($(this).val()!=''){
                $("#student_reg").find('.regemail').removeClass('redborder');
            }
        });
        $('body').on('keyup','.emailcnfrm', function () {
            if($(this).val()!=''){
                $("#student_reg").find('.emailcnfrm').removeClass('redborder');
            }
        });
        $('body').on('keyup','.regpassword', function () {
            if($(this).val()!=''){
                $("#student_reg").find('.regpassword').removeClass('redborder');
            }
        });
        $('body').on('keyup','.cnfrmpassword', function () {
            if($(this).val()!=''){
                $("#student_reg").find('.cnfrmpassword').removeClass('redborder');
            }
        });
        $('body').on('keyup','.mobile', function () {
            if($(this).val()!=''){
                $("#student_reg").find('.mobile').removeClass('redborder');
            }
        });
        
    });
    function shakeModal(msg){
        $('#loginModal .modal-dialog').addClass('shake');
                 $('.error').addClass('alert alert-danger').html(msg);
                 $('input[type="password"]').val('');
                 setTimeout( function(){ 
                    $('#loginModal .modal-dialog').removeClass('shake'); 
        }, 1000 ); 
    }
    function showRegisterForm(){
        
        $(".loginBox").find('.login_error').html('');
        // $("#student_login").trigger('reset');
        // $("#submit_otp").trigger('reset');
        $('.loginBox').fadeOut('fast',function(){
            $('.registerBox').fadeIn('fast');
            $('.login-footer').fadeOut('fast',function(){
                $('.register-footer').fadeIn('fast');
            });
            $('.form_title').html('Create your new Account');
        }); 
        $('.error').removeClass('alert alert-danger').html('');
           
    }
    function showLoginForm(){
        // $("#student_reg").trigger('reset');
        // $("#submit_otp").trigger('reset');
        $('#exampleModalCenter .registerBox').fadeOut('fast',function(){
            $('.loginBox').fadeIn('fast');
            $('.register-footer').fadeOut('fast',function(){
                $('.login-footer').fadeIn('fast');    
            });
            
            $('.form_title').html('Login with');
        });
        $('.otpBox').fadeOut('fast');   
        $('.forgotpassBox').fadeOut('fast');   
        $('.forgot_confirmpass_Box').fadeOut('fast');   
        $('.emailsentmsg').css({ "display": "none"});
        $('.otpresendtmsg').css({ "display": "none"});
        $('.otp-footer').css({ "display": "none"});
        $('.forgotpass-footer').css({ "display": "none"});
        $('.social').css({ "display": "block"});
        $('.division').css({ "display": "block"});
        $('.error').removeClass('alert alert-danger').html(''); 
    }
    function showOtpForm(){
        // $("#student_reg").trigger('reset');
        // $("#student_login").trigger('reset');
        $('#exampleModalCenter .registerBox').fadeOut('fast',function(){
            $('.otpBox').fadeIn('fast');
            $('.register-footer').fadeOut('fast',function(){
                $('.otp-footer').fadeIn('fast');    
            });
            
            $('.form_title').html('OTP Verification');
        });       
         $('.error').removeClass('alert alert-danger').html(''); 
         $('.emailsentmsg').css({ "display": "block"});
         $('.otpresendtmsg').css({ "display": "none"});
         $('.otp-footer').css({ "display": "block"});
         $('.social').css({ "display": "none"});
         $('.division').css({ "display": "none"});
         $('.registerBox').css({ "display": "none"});
    }
    function showforgotpassForm(){
        $('#exampleModalCenter .loginBox').fadeOut('fast',function(){
            $('.forgotpassBox').fadeIn('fast');
            $('.login-footer').fadeOut('fast',function(){
                $('.forgotpass-footer').fadeIn('fast');    
            });
            $('.form_title').html('Reset Password');
        });       
         $('.error').removeClass('alert alert-danger').html(''); 
         $('.social').css({ "display": "none"});
         $('.division').css({ "display": "none"});
    }
    function showOtpFormforgotpass(){
        $('#exampleModalCenter .forgotpassBox').fadeOut('fast',function(){
            $('.otpBoxforgotpass').fadeIn('fast');
            $('.forgotpass-footer').fadeOut('fast',function(){
                $('.otp-footer').fadeIn('fast');    
            });
            
            $('.form_title').html('OTP Verification');
        });       
         $('.error').removeClass('alert alert-danger').html(''); 
         $('.emailsentmsg').css({ "display": "block"});
         $('.otpresendtmsg').css({ "display": "none"});
         $('.social').css({ "display": "none"});
         $('.division').css({ "display": "none"});
    }
    function showForgotConfirmPassForm(){
        $('#exampleModalCenter .otpBoxforgotpass').fadeOut('fast',function(){
            $('.forgot_confirmpass_Box').fadeIn('fast');
            $('.otp-footer').fadeOut('fast',function(){
                // $('.otp-footer').fadeIn('fast');    
            });
            
            $('.form_title').html('Reset Password');
        });       
         $('.error').removeClass('alert alert-danger').html(''); 
         $('.emailsentmsg').css({ "display": "none"});
         $('.otpresendtmsg').css({ "display": "none"});
         $('.social').css({ "display": "none"});
         $('.division').css({ "display": "none"});
    }
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    
</script>
