<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from seantheme.com/color-admin-v4.3/frontend/e-commerce/index_inverse_header.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Mar 2019 06:24:53 GMT -->
<head>
	<meta charset="utf-8" />
	<title>PCSKAKA</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/plugins/font-awesome/css/all.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/default.css" id="theme" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/slick.css" rel="stylesheet"/>
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/animate.css" rel="stylesheet"/>
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/default.css" rel="stylesheet"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		 <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/frontendasset/plugins/pace/pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	<script>
		var api_loc="http://localhost:3000/";
		var base_loc="<?php echo base_url(); ?>";
		var clientserv="Alam";
        var apikey="eduweb";
	</script>
	<style>

		section.stat_w3l {
			background: url("<?php echo base_url('assets/frontendasset/img/bg/hero_1.jpg');?>") no-repeat;
			background-size: cover;
			-webkit-background-size: cover;
			
			background-attachment: fixed;
		}
		.pr-4{
			padding-right: 2rem!important;
		}
		.modal-header {
			padding: 0 15px;
			border-bottom:none!important; 
		}
		button.close {
			font-size: 35px;
		}
		.modal-header .close {
			padding: 1rem 1rem 0;
			margin: -1rem -1rem -1rem auto;
		}
		
		input.form-control {
			
			border-top: none;
			border-left: none;
			border-right: none;
			border-radius: 0;
		}
		.center {
			left: 15%;
		}
		.pull-up:hover {		
    -webkit-transform: translateY(-4px) scale(1.02);		
    -moz-transform: translateY(-4px) scale(1.02);		
    -ms-transform: translateY(-4px) scale(1.02);		
    -o-transform: translateY(-4px) scale(1.02);		
    transform: translateY(-4px) scale(1.02);		
    -webkit-box-shadow: 0 14px 24px rgba(62,57,107,.2);		
    box-shadow: 0 14px 24px rgba(62,57,107,.2);		
    z-index: 999;	
}	
a{		
    text-decoration:none;	
}

@media only screen and (max-width: 400px) {
  .hero {
        position: absolute;
        top: 44%;
        left: 55%;
        z-index: 3;
        color: #3b0001;
        text-align: center;
        text-transform: uppercase;
        text-shadow: 1px 1px 0 rgba(0,0,0,.75);
          -webkit-transform: translate3d(-50%,-50%,0);
             -moz-transform: translate3d(-50%,-50%,0);
              -ms-transform: translate3d(-50%,-50%,0);
               -o-transform: translate3d(-50%,-50%,0);
                  transform: translate3d(-50%,-50%,0);
    }
    .hero h1 {
        font-size: 1.7em;    
        font-weight: bold;
        margin: 0;
        padding: 0;
    }
}
@media screen and (min-width: 400px) 
  and (max-width: 992px){
    .hero {
        position: absolute;
        top: 60%;
        left: 61%;
        z-index: 3;
        color: #3b0001;
        text-align: center;
        text-transform: uppercase;
        text-shadow: 1px 1px 0 rgba(0,0,0,.75);
          -webkit-transform: translate3d(-50%,-50%,0);
             -moz-transform: translate3d(-50%,-50%,0);
              -ms-transform: translate3d(-50%,-50%,0);
               -o-transform: translate3d(-50%,-50%,0);
                  transform: translate3d(-50%,-50%,0);
        }
        .hero h1 {
        font-size: 4.7em;    
        font-weight: bold;
        margin: 0;
        padding: 0;
        }
}
@media screen and (min-width: 992px) {
  .hero {
        position: absolute;
        top: 60%;
        left: 51%;
        z-index: 3;
        color: #3b0001;
        text-align: center;
        text-transform: uppercase;
        text-shadow: 1px 1px 0 rgba(0,0,0,.75);
          -webkit-transform: translate3d(-50%,-50%,0);
             -moz-transform: translate3d(-50%,-50%,0);
              -ms-transform: translate3d(-50%,-50%,0);
               -o-transform: translate3d(-50%,-50%,0);
                  transform: translate3d(-50%,-50%,0);
        }
        .hero h1 {
        font-size: 4.7em;    
        font-weight: bold;
        margin: 0;
        padding: 0;
        }
}
h3.title {
    display: block;
    font-size: 30px;
    font-weight: 700;
    color: blueviolet;
    margin: 0 0 10px;
}
span.post {
    display: block;
    font-size: 15px;
    color: brown;
    margin-bottom: 10px;
    text-transform: capitalize;
}
ul.social {
    padding: 0;
    margin: 0;
    list-style: none;
}
.social li {
    display: inline-block;
}
.social li a {
    display: block;
    width: 35px;
    height: 35px;
    background: #fff;
    border-radius: 50%;
    font-size: 17px;
    color: #1b1462;
    line-height: 35px;
    margin-right: 5px;
    transition: all .4s ease-in-out 0s;
}
.p-5 {
    padding: 3rem!important;
}

	</style>	
</head>
<body>
	<!-- BEGIN #page-container -->
	<div id="page-container" class="fade">
		<!-- BEGIN #top-nav -->
		<div id="top-nav" class="top-nav">
			<!-- BEGIN container -->
			<div class="container">
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="dropdown dropdown-hover">
							<a href="#" data-toggle="dropdown">English <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="#">English</a></li>
								<li><a class="dropdown-item" href="#">Hindi</a></li>
								
							</ul>
						</li>
						<li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i><span class="ml-1">090266 01666</span></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="javascript:void(0);"><i class="fab fa-facebook-f f-s-14"></i></a></li>
						<li><a href="javascript:void(0);"><i class="fab fa-twitter f-s-14"></i></a></li>
						<li><a href="javascript:void(0);"><i class="fab fa-instagram f-s-14"></i></a></li>
						<li><a href="javascript:void(0);"><i class="fab fa-dribbble f-s-14"></i></a></li>
						<li><a href="javascript:void(0);"><i class="fab fa-google f-s-14"></i></a></li>
					</ul>
				</div>
			</div>
			<!-- END container -->
		</div>
		<!-- END #top-nav -->		
		<!-- BEGIN #header -->
		<div id="header" class="header header-inverse">
			<!-- BEGIN container -->
			<div class="container">
				<!-- BEGIN header-container -->
				<div class="header-container">
					<!-- BEGIN navbar-header -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="header-logo">
							<a href="<?php echo base_url();?>">
								<span class="brand"></span>
								<span>PCS</span>KAKA
								<small>Start your learning</small>
							</a>
						</div>
					</div>
					<!-- END navbar-header -->
					<!-- BEGIN header-nav -->
					<div class="header-nav">
						<div class=" collapse navbar-collapse" id="navbar-collapse">
							<ul class="nav">
								<li><a href="<?php echo base_url();?>">HOME</a></li>
								<li><a href="<?php echo base_url();?>">IAS</a></li>
								<li class="dropdown dropdown-full-width dropdown-hover">
									<a href="#" data-toggle="dropdown">
										PCS 
										<b class="caret"></b>
										<span class="arrow top"></span>
									</a>
									<!-- BEGIN dropdown-menu -->
									<div class="dropdown-menu p-0">
										<!-- BEGIN dropdown-menu-container -->
										<div class="dropdown-menu-container">
										
											<!-- BEGIN dropdown-menu-content -->
											<div class="dropdown-menu-content">
												<h4 class="title">All Cities</h4>
												<div class="row">
													<div class="col-md-3">
														<ul class="dropdown-menu-list">					
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Delhi</a></li>
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Bengaluru</a></li>
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Mumbai</a></li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="dropdown-menu-list">					
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Kolkata</a></li>
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Pune</a></li>
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Chennai</a></li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="dropdown-menu-list">					
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Chandigarh</a></li>
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Haryana</a></li>
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Indore</a></li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="dropdown-menu-list">					
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Gurgaon</a></li>
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Noida</a></li>
															<li><a href="product_detail.html"><i class="fa fa-fw fa-angle-right text-muted"></i>Lucknow</a></li>
														</ul>
													</div>
												</div>
											
											</div>
											<!-- END dropdown-menu-content -->
										</div>
										<!-- END dropdown-menu-container -->
									</div>
									<!-- END dropdown-menu -->
								</li>
								<li class="dropdown dropdown-hover " id="nav-menu">
									<a href="#" data-toggle="dropdown">
										HISTORY 
										<b class="caret"></b>
										<span class="arrow top"></span>
									</a>
									<div class="dropdown-menu sub-nav">
										<a class="dropdown-item dropmenu" href="javascript:void(0);">Ancient History</a>										
										<a class="dropdown-item dropmenu" href="javascript:void(0);">Medieval History</a>										
										<a class="dropdown-item dropmenu" href="javascript:void(0);">Modern History</a>										
										<a class="dropdown-item dropmenu" href="javascript:void(0);">FULL</a>										
																			
									</div>
								</li>
								<li><a href="product.html">CURRENT AFFAIRS</a></li>
								<li><a href="product.html">LIBRARY</a></li>															
								<li><a href="product.html">TEST SERIES</a></li>
							</ul>
						</div>
					</div>
					<!-- END header-nav -->
					<div class="header-nav">
						<ul class="nav pull-right">	
							<li>
								<a href="<?php echo base_url(); ?>admin">
									<img src="<?php echo base_url(); ?>assets/frontendasset/img/user/user-1.jpg" class="user-img" alt="" /> 
									<span class="d-none d-xl-inline">Login</span>
								</a>
							</li>	
						</ul>
					</div>
				</div>
				<!-- END header-container -->
			</div>
			<!-- END container -->
		</div>
		<!-- END #header -->
		
	</div>



<!-- BEGIN #slider -->
	<div id="slider" class="section-container p-0 bg-black-darker">		<!-- BEGIN carousel -->
		<div id="main-carousel" class="carousel slide" data-ride="carousel">
			<!-- BEGIN carousel-inner -->
			<div class="carousel-inner">
				<!-- BEGIN item -->
				<div class="carousel-item active" data-paroller="true" data-paroller-factor="0.3" data-paroller-factor-sm="0.01" data-paroller-factor-xs="0.01" style="background: url(<?php echo base_url(); ?>assets/frontendasset/img/slider/slider-9-cover.jpg) center 0 / cover no-repeat;"></div>
				<!-- END item -->
				<!-- BEGIN item -->
				<div class="carousel-item" data-paroller="true" data-paroller-factor="-0.3" data-paroller-factor-sm="0.01" data-paroller-factor-xs="0.01" style="background: url(<?php echo base_url(); ?>assets/frontendasset/img/slider/slider-8-cover.jpg) center 0 / cover no-repeat;">
				    <div class="hero">
                        <hgroup>
                            <h1>Best Digital Library for PCS Preparation</h1>
                        </hgroup>
                    </div>
				</div>
			</div>
			<!-- END carousel-inner -->
			<a class="carousel-control-prev" href="#main-carousel" data-slide="prev"> 
			    <i class="fa fa-angle-left"></i> 
			</a>
			<a class="carousel-control-next" href="#main-carousel" data-slide="next"> 
				<i class="fa fa-angle-right"></i> 
			</a>
		</div>
		<!-- END carousel -->
	</div>
<div class="section_padding section-info w3ls-titles" data-aos="zoom-in-up" data-aos-duration="2000" style="padding-bottom:0!important;" >			
	<div class="text-info">		
		<h3 class="text-center heading" >Marketing test to encourage enrollment</h3>		
	</div>
</div>

	
    <section class="blog_w3ls py-lg-5 mb-5" style="width:96%;">	
        <div class="container-fluid">
    		<div class="w3ls-titles text-center mb-5">		
        		<h3 class="title"><span class="hdng">Testimonials</span></h3>			
        		<p class="mt-3 mx-auto">The Goal of Education.Connection stimulated estimating excellence an to impression.</p>	
    	    </div>	
    	    	<div class="row py-5 slider slider-testimonials">            
					<div class="col-lg-3 col-md-3 pull-up" data-aos="flip-left" data-aos-duration="2000" >				
						<div class="card">		
							<div class="card-body text-center">			
								<img src="<?php echo base_url('assets/frontendasset/img/person_1.jpg');?>" alt="" class="img-fluid height-250">
								<h5 class="blog-title card-title mt-3 mb-3">					
									<h3 class="title">Williamson</h3>
									<span class="post">role in detail</span>					
								</h5>				
								<ul class="social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-instagram"></i></a></li>
									<li><a href="#"><i class="fab fa-skype"></i></a></li>
								</ul>
							</div>					
						</div>			
					</div>	
					<div class="col-lg-3 col-md-3 pull-up"  data-aos="flip-left" data-aos-duration="2000" >				
						<div class="card">		
							<div class="card-body text-center">			
								<img src="<?php echo base_url('assets/frontendasset/img/person_6.jpg');?>" alt="" class="img-fluid height-250">
								<h5 class="blog-title card-title mt-3 mb-3">					
									<h3 class="title">Kristiana</h3>
									<span class="post">role in detail</span>					
								</h5>				
								<ul class="social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-instagram"></i></a></li>
									<li><a href="#"><i class="fab fa-skype"></i></a></li>
								</ul>
							</div>					
						</div>		
					</div>			
					<div class="col-lg-3 col-md-3 pull-up" data-aos="flip-left" data-aos-duration="2000" >				
						<div class="card">		
							<div class="card-body text-center">			
								<img src="<?php echo base_url('assets/frontendasset/img/person_3.jpg');?>" alt="" class="img-fluid height-250">
								<h5 class="blog-title card-title mt-3 mb-3">					
									<h3 class="title">Micheal</h3>
									<span class="post">role in detail</span>					
								</h5>				
								<ul class="social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-instagram"></i></a></li>
									<li><a href="#"><i class="fab fa-skype"></i></a></li>
								</ul>
							</div>					
						</div>			
					</div>		
					<div class="col-lg-3 col-md-3 blog-3 mt-md-0 mt-5 pull-up" data-aos="flip-right" data-aos-duration="2000" >	
						<div class="card">		
							<div class="card-body text-center">			
								<img src="<?php echo base_url('assets/frontendasset/img/person_2.jpg');?>" alt="" class="img-fluid height-250">
								<h5 class="blog-title card-title mt-3 mb-3">					
									<h3 class="title">Rogert</h3>
									<span class="post">role in detail</span>					
								</h5>				
								<ul class="social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-instagram"></i></a></li>
									<li><a href="#"><i class="fab fa-skype"></i></a></li>
								</ul>
							</div>					
						</div>		
					</div>
					<div class="col-lg-3 col-md-3 blog-3 mt-md-0 mt-5 pull-up" data-aos="flip-right" data-aos-duration="2000" >	
						<div class="card">		
							<div class="card-body text-center">			
								<img src="<?php echo base_url('assets/frontendasset/img/person_4.jpg');?>" alt="" class="img-fluid height-250">
								<h5 class="blog-title card-title mt-3 mb-3">					
									<h3 class="title">Jhones Bose</h3>
									<span class="post">role in detail</span>					
								</h5>				
								<ul class="social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-instagram"></i></a></li>
									<li><a href="#"><i class="fab fa-skype"></i></a></li>
								</ul>
							</div>					
						</div>		
					</div>  
            </div>
    	</div>
    </section>
<!--    <section class="stat_w3l py-5">	-->
<!--        <div class="container">	-->
<!--            <div class="w3ls-titles text-center mb-5">	-->
<!--                <h3 class="title text-white"><span class="hdng">Services </span>Fact</h3>	-->
<!--            </div>	-->
<!--            <div class="row py-lg-5 stats-con">      -->
<!--                <div class="col-lg-3 col-md-6 stats_info counter_grid" data-aos="zoom-out-right" data-aos-duration="2000">	-->
<!--                    <div class="stats_info1">				-->
<!--                        <span class="fa fa-users"></span>			-->
<!--                        <p class="counter">25,000</p>				-->
<!--                        <h4>Students Enrolled</h4>			-->
<!--                    </div>          \-->
<!--                </div>		-->
<!--                <div class="col-lg-3 col-md-6 stats_info counter_grid" data-aos="zoom-out-left" data-aos-duration="2000">	-->
<!--                    <div class="stats_info1">			-->
<!--                        <span class="fa fa-envelope"></span>	-->
<!--                        <p class="counter">120</p>				-->
<!--                        <h4>Test Takers</h4>		-->
<!--                    </div>	-->
<!--                </div>		-->
<!--                <div class="col-lg-3 col-md-6 stats_info counter_grid1" data-aos="zoom-out-up" data-aos-duration="2000">-->
<!--                    <div class="stats_info1">		-->
<!--                    <span class="fa fa-briefcase"></span>	-->
<!--                    <p class="counter">486</p>		-->
<!--                    <h4>Visitors</h4>			-->
<!--                </div>		-->
<!--            </div>		-->
<!--            <div class="col-lg-3 col-md-6 stats_info counter_grid2" data-aos="zoom-out-down" data-aos-duration="2000">	-->
<!--                <div class="stats_info1">	-->
<!--                    <span class="fa fa-comments"></span>-->
<!--                    <p class="counter">6,812</p>			-->
<!--                    <h4>Questions Answered</h4>			-->
<!--                </div>		-->
<!--            </div>     -->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
<!-- service_part part start-->
    <section class="service_part section_bg_2 section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="single_service_part">
                        <div class="single_service_part_iner">
                            <span class="fa fa-users"></span>
							<p class="counter">5231</p>
							<h3>Students Enrolled</h3>
                           
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_service_part">
                        <div class="single_service_part_iner">
                            <span class="fa fa-envelope"></span>
                            <p class="counter">4153</p>
							<h3>Test Takers</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_service_part">
                        <div class="single_service_part_iner">
                            <span class="fa fa-briefcase"></span>
							<p class="counter">125308</p>
							<h3>Visitors</h3>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_service_part">
                        <div class="single_service_part_iner">
                            <span class="fa fa-comments"></span>
                            
							 <p class="counter">15356</p>
							<h3>Questions Answered</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- service_part part end-->
<div class="testimonials align-w3 py-5" style="padding-bottom:5em!important;">        
    <div class="container">            
        <div class="wthree_pvt_title text-center mb-1"style="padding-bottom: 30px;">               
            <h3 class="title"><span class="hdng">Comments</span></h3>                           
        </div>            
        <div class="carousel-inner">                
            <div class="row">                    
                <div class="col-lg-6">                        
                    <div class="testimonials_grid " data-aos="flip-down" data-aos-duration="500" >                            
                        <div class="testi-text text-center">                                
                            <p><span class="fa fa-quote-left text-center"></span>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet<span class="fa fa-quote-right"></span></p>                            
                        </div> 
                        <div class="d-flex align-items-center justify-content-center">     
                            <div class="testi-desc">             
                                <span class="fa fa-user text-center"></span>       
                                <h5 class="text-center">Aliquyam</h5>                
                                <p class="text-center">Member</p>            
                            </div>                           
                        </div>                     
                    </div>                
                </div>                  
                <div class="col-lg-6">         
                    <div class="testimonials_grid mt-lg-0 mt-4 " data-aos="flip-down" data-aos-duration="500">   
                        <div class="testi-text text-center">             
                            <p><span class="fa fa-quote-left text-center"></span>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet<span class="fa fa-quote-right"></span></p>      
                        </div>          
                        <div class="d-flex align-items-center justify-content-center">   
                            <div class="testi-desc">         
                                <span class="fa fa-user text-center"></span>      
                                <h5  class="text-center">Aliquyam</h5>            
                                <p  class="text-center">Member</p>                  
                            </div>                          
                        </div>                     
                    </div>                  
                </div>               
            </div>         
        </div>      
    </div>  
</div>
<section class="bg-light testimonials align-w3 py-5 aos-init aos-animate" id="contact-section" style="padding-bottom:0!important;"data-aos="fade">
      <div class="container">
        <div class="w3ls-titles text-center">		
        		<h3 class="title"><span class="hdng">Get In Touch</span></h3>			
        		<p class="mt-3 mx-auto">We want to hear from you!</p>	
    	</div>
        <div class="row">
          <div class="col-md-12 mb-5">        
            <form action="#" class="p-5 bg-white">
			<div class="row">
				<div class="col-md-6">
					<div class="row form-group">
						<div class="col-md-6 mb-3 mb-md-0">
						  <label class="text-black" for="fname">Name</label>
						  <input type="text" id="fname" class="form-control">
						</div>
						<div class="col-md-6  mb-3 mb-md-0">
						  <label class="text-black" for="email">Email</label> 
						  <input type="email" id="email" class="form-control">
						  
						</div>
					</div>
						<label class="text-black" for="subject">Subject</label> 
							<input type="subject" id="subject" class="form-control">
				</div>               
                <div class="col-md-6">
				<label class="text-black" for="message">Message</label> 
                  <textarea name="message" id="message" cols="30" rows="5" class="form-control" placeholder="Write your notes or questions here..."></textarea>                               
                </div>
			</div>
				<div class="row form-group" style="float: right;margin-top: 2px;">
                <div class="col-auto my-1">
				  <button type="submit" class="btn btn-md btn-primary float-right">Send Message</button>
				</div>
				</div>

  
            </form>
          </div>
          
        </div>
      </div>
    </section>		