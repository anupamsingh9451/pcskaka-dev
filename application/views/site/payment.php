<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header"><b>Safe and Secure payment by PayU</b></h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->
				<!-- BEGIN #product -->
		<div id="product" class="section-container p-t-20">
			<!-- BEGIN container -->
			<div class="container">
			
				<!-- BEGIN row -->
				<div class="row row-space-30">
					<div class="col-md-12">
					<h3 class="text-center" style="margin-top: 2%;">Our software is pre-integrated with leading payment gateway of India.</h3>	
					<p>Users data is of utmost priority to us. We use the secured payment gateway provided by PAYUMONEY. So that you can proceed with your payments with peace of mind.</p>
						<section>
            <div class="layout-container l-flex l-flex-vcenter l-spacing l-column-rwd">

                <div class="l-box-5">
                    
					<div class="row">
						<div class="col-md-6">
							<figure class="rwd-hide-full rwd-hide-medium">
								<span>
									<picture>
										
										<img src="<?php echo base_url('assets/frontendasset/img/svg/payu.png');?>" alt="Payment Methods" style="max-width: 100%;">
									</picture>
								</span>
							</figure>
						</div>
						<div class="col-md-6">
							<h2 style="margin-bottom:1em; margin-top: 88px;">
                        PayU - Payment Gateway                    </h2>
							<ul class="list less-margin" style="font-size:1.2em">
								<li style="list-style: none">
									Multiple Payment options.                        </li>

								<li style="list-style: none">
									Secured payment process.                        </li>

							
                                 	<li style="list-style: none">   Credit Card – VISA / MASTER  </li>
                                	<li style="list-style: none">   Internet Banking – All major Banks supported  </li>
                                	<li style="list-style: none">   Debit Card –  All major Banks supported  </li>
                                 	<li style="list-style: none">   BHIM / UPI  </li>
                                 	<li style="list-style: none">   Payment wallets – All major payment wallets supported                    </li>

								<li style="list-style: none">
									Mobile optimized payment page.                        </li>
							</ul>
						</div>
					</div>
                </div>
            </div>
        </section>
						
					</div>
				</div>
			</div>
			<!-- END row -->
		</div>
		<!-- END #product -->