<link href="<?php echo base_url(); ?>assets/siteasset/css/pcs/ras.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>RAS</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
			<h2>What we do</h2>
			<p><b>PCS KAKA</b> is a digital library designed to cater needs of state civil service aspirants. It has all the subjects required for the preparation of UP PCS examination. Our library has more than 20,000 questions covering various topics from all the books available in market. Our Library covers the following subjects from UP PCS exam’s perspective: History, Geography, Polity, Economics, Environment, Science, Current affairs of state and national importance. This library is completely digital. Available 24x7. </p>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Comparative Analysis :</b> Get your rank after completing a practice test. See where you stand in this competitive world.</span>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Immediate Result :</b> The moment you submit the practice test; answers will be displayed with questions. See where you went wrong.</span>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Unlimited attempts: </b> You can take any practice test unlimited number of times. Improve your score and hence your memorizing power of facts and figures.</span>
		</div>			
	</div>
		
	<div class="pcs-info ">
		<div class="container">
			<div class="section-pcs-info m-b-30">
				<h2>What is RAS? </h2>
				<p>Rajasthan Public Service commission (RPSC) conducts exam for the recruitment to the posts in Rajasthan Civil Services for the post of  assistant collector, Sub-Divisional magistrate, Additional district collector, Additional Divisional Commissioner, Deputy Inspector General Stamp and Registration, Special Assistant to State Minister, Commissioner to Municipal Corporation, Additional Chief Executive Officer to Zila Parishad, Chief Executive Officer to Zila Parishad, District Supply Officer, Secretary to Urban Improvement Trust, Registrar to State University, District Excise Officer, Member of Board of Revenue, Deputy and additional Commissioner to Colonization.  <a href="https://rpsc.rajasthan.gov.in">https://rpsc.rajasthan.gov.in.</a>
				</p>
				<p><b>Selection Process:</b> The competitive exam comprises of one Preliminary exam (one paper of Objective Type) of200 marks, Main Exam (4 Papers for 800 marks Written Type) and The interview of 100 marks</p>
				<div class="row m-t-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">Exam Type</th>
										<th class="textDarkBlue border-0">Educational Criteria</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td>Preliminary Examination</td>
										<td>Objective Type & Multiple choices</td>
									</tr>
									<tr>
										<td>Main Examination</td>
										<td>Conventional Type, i.e. Written examination</td>
									</tr>
									<tr>
										<td>Viva- Voice</td>
										<td>Personality Test</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="pre-exams">
				<div class="heading">
					<h2>Preliminary Examination:-</h2>
					<p style="font-weight:500;">The Preliminary Examination will consist of one paper on the subject specified below, which will be of objective type and carry a maximum of 200 marks. There will be Negative Marking – 1/3 part of mark(s) of each question will be deducted for each wrong answer. The examination is meant to serve as a screening test only. The Standard of the paper will be that of a Bachelor’s Degree Level.</p>
				</div>
				<div class="row mt-5 m-b-20">
					<div class="col-sm-12">
						<div class="border rounded lightbluebackground p-2">
							<h3 class="mainHeading text-center pt-2 pb-2">Preliminary Examination(objective Type & multiple choice)</h3>
							<div class="row">
								<div class="col-sm-12 col-12 pb-3">
									<div class="table-responsive pr-2 pl-2 pb-1 pt-1">
										<table class="table mb-0">
											<thead>
												<tr class="border-top"><th class="textDarkBlue border-0">Exam</th>
													<th class="textDarkBlue border-0">No. of Questions</th>
													<th class="textDarkBlue border-0">Total Marks</th>
													<th class="textDarkBlue border-0">Timing</th>
												</tr>
											</thead>
											<tbody class="textGray">
												<tr class="border-bottom border-top">
													<td class="text-truncate">Paper-I</td>
													<td class="text-truncate">150</td>
													<td class="text-truncate">200</td>
													<td class="text-truncate">9.30 to 11.30 A.M. </td>
												</tr>
											
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row m-t-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">Prelims Syllabus</th>
										<th class="textDarkBlue border-0">Duration</th>
										<th class="textDarkBlue border-0">Total Marks</th>
										<th class="textDarkBlue border-0">Total Questions</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td>GS-I</td>
										<td>Two hours</td>
										<td>200</td>
										<td>150</td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row m-t-30 m-b-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">RAS Prelims syllabus</th>
										<th class="textDarkBlue border-0">PCS KAKA Library</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Major Current Events and Issues of State (Rajasthan), National and International Importance  Persons and Places in recent news,  Games and Sports related Activities.</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">On a given date Kaka’s libraray has more than <b>1500 questions</b> to practice from. Library is divided into monthly current affairs by Month such as Jan, Feb, etc. On a given day you have access to last 15 months current affairs.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">1.	Ancient & Medieval Period: Salient features and Major Landmarks of Ancient and Medieval India.  Art, Culture, Literature and Architecture. Major Dynasties, Their Administrative System. Socio-Economic Conditions, Prominent Movements.
2.	Modern Period: Modern Indian history (from about the middle of the eighteenth century until the present)- significant events, personalities and issues.  The Freedom Struggle & Indian National Movement- its various stages and important contributors and contributions from different parts of the country.  Social and religious reform movements in the 19th and 20th century. Post-independence consolidation and reorganization within the country.
 </td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Kaka’s Library contains more than <b>4000 question</b> spanning from ancient India to Modern India. Also containing questions from Art and Culture. History library is divided in to subsections such as Ancient India, Medieval India, Modern India and Art and culture.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Indian Constitution, Political System & Governance:
1.	Constitutional Development & Indian Constitution: Government of India Acts: 1919 and 1935, Constituent Assembly, Nature of Indian Constitution; Preamble, Fundamental Rights, Directive Principles of State, Fundamental Duties, Federal Structure, Constitutional Amendments, Emergency Provisions, Public Interest Litigation (P.I.L.) and Judicial Review.
2.	Indian Political System and Governance: Nature of Indian State, Democracy in India, Reorganization of States, Coalition Governments, Political Parties, National Integration.  Union and State Executive; Union and State Legislative, Judiciary.  President, Parliament, Supreme Court, Election Commission, Comptroller and Auditor General, Planning Commission, National Development Council, Central Vigilance Commission (CVC), Central Information Commission , Lokpal, National Human Rights Commission (NHRC).  Local Self Government & Panchayati Raj.
3.	Public Policy & Rights: National Public Policy as a welfare state.  Various Legal Rights and Citizen Charter.
4.	Political and Administrative System of Rajasthan: Governor, Chief Minister, State Assembly, High Court, Rajasthan Public Service Commission, District Administration, State Human Rights Commission, Lokayukt, State Election Commission, State Information Commission.  Public Policy, Legal Rights and Citizen Charter.
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Indian Polity section contains more than <b>2500 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">1.	World Geography: Broad Physical features.  Environmental and Ecological Issues.  Wildlife and Bio-diversity.  International Waterways.  Major Industrial Regions.
2.	Geography of India: Broad physical features and Major physiographic divisions.  Agriculture and Agro based Activities.  Minerals – Iron, Manganese, Coal, Oil & Gas, Atomic minerals.  Major Industries and Industrial development. Transportation– major transport corridors.  Natural Resources. Environmental Problems and Ecological Issues
3.	Geography of Rajasthan: Broad physical features and Major physiographic divisions.  Natural Resource of Rajasthan. Climate, Natural Vegetation, Forests, Wildlife and Bio-diversity, Major irrigation projects, Mines and Minerals,  Population,  Major Industries and Potential for Industrial Development.
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Geography section of Kaka’s library contains more than <b>3000 questions</b>. Geography has been divided into subsections such as Physical Geography, Indian Geography and World Geography.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Economic Concepts and Indian Economy: 
1.	Basic Concepts of Economics: Basic Knowledge of Budgeting, Banking, Public Finance, National Income, Growth and Development  Accounting- Concept, Tools and Uses in Administration, Stock Exchange and Share Market, Fiscal and Monetary Policies, Subsidies, Public Distribution System, e-Commerce, Inflation- Concept, Impact and Control Mechanism.
2.	Economic Development & Planning: 5 Year Plans - Objectives, Strategies and Achievements.  Major Sectors of Economy- Agriculture, Industry, Service and Trade- Current Status, Issues & initiatives.  Major Economic Problems and Government Initiatives. Economic Reforms and Liberalization.
3.	Human Resource and Economic Development: Human Development Index Poverty and Unemployment:- Concept, Types, Causes, Remedies and Current Flagship Schemes, Provisions for Weaker Sections.
4.	Economy Of Rajasthan: Macro overview of Economy.  Major Agricultural, Industrial and Service Sector Issues. Growth, Development and Planning.  Infrastructure & Resources. Major Development Projects.  Programmes and Schemes- Government Welfare Schemes for SC/ST/Backward Class/Minorities/Disabled Persons, Destitute, Women, Children, Old Age People, Farmers & Labourers.

</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Economics section contains more than <b>2000 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Environmental and Ecological Changes and its Impacts. Biodiversity, Biotechnology and Genetic Engineering. Agriculture, Horticulture, Forestry and Animal Husbandry with special reference to Rajasthan.   </td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Environment section contains more than <b>2500 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Basics of Everyday Science.  Electronics, Computers, Information and Communication Technology. Space Technology including Satellites. Defence Technology. Nanotechnology. Human body, Food and Nutrition, Health care. Development of Science and Technology in Rajasthan.</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Science section contains more than <b>2500 questions</b> to practice from. Science has been divided into subsections such as Physics, Chemistry and Biology.
											Aspirant can study and test his knowledge by choosing any subsection to attempt the questions.
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
						
			</div>
		</div>
	</div>
</div>
