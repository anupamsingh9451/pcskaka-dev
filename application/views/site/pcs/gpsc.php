<link href="<?php echo base_url(); ?>assets/siteasset/css/pcs/gpsc.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>GPSC</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
			<h2>What we do</h2>
			<p><b>PCS KAKA</b> is a digital library designed to cater needs of state civil service aspirants. It has all the subjects required for the preparation of UP PCS examination. Our library has more than 20,000 questions covering various topics from all the books available in market. Our Library covers the following subjects from UP PCS exam’s perspective: History, Geography, Polity, Economics, Environment, Science, Current affairs of state and national importance. This library is completely digital. Available 24x7. </p>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Comparative Analysis :</b> Get your rank after completing a practice test. See where you stand in this competitive world.</span>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Immediate Result :</b> The moment you submit the practice test; answers will be displayed with questions. See where you went wrong.</span>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Unlimited attempts: </b> You can take any practice test unlimited number of times. Improve your score and hence your memorizing power of facts and figures.</span>
		</div>			
	</div>
		
	<div class="pcs-info ">
		<div class="container">
			<div class="section-pcs-info m-b-30">
				<h2>What is GPSC? </h2>
				<p>Gujarat Public Service commission (GPSC) conducts exam for the recruitment to the posts in Gujarat Civil Services, Class-I and Class-II i.e. Deputy superintendent of police, District registrar of Co-Op. Society,  Mamlatdar, Section officer –Sachivalaya, Section officer-Vidhansabha, Section officer-GPSC, Taluka Development Officer, Labour Officer, Assistant District registrar of Co-Op. Society, District Inspector of land record. The posts are advertised by the commission in the Gujarat Rojgar Samachar and newspapers. -  <a href="https://gpsc.gujarat.gov.in">https://gpsc.gujarat.gov.in</a>
				To participate in GPSC exam, candidate should have graduate in any discipline from recognized university and must have completed 21 years and maximum of 28 years.
				</p>
				<p><b>Selection Process:</b> The competitive exam comprises of Preliminary exam (Objective Type) of400 marks, Main Exam (Written Type) of 900 marks and The interview of 100 marks:-</p>
				<div class="row m-t-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">Exam Type</th>
										<th class="textDarkBlue border-0">Educational Criteria</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td>Preliminary Examination</td>
										<td>Objective Type & Multiple choices</td>
									</tr>
									<tr>
										<td>Main Examination</td>
										<td>Conventional Type, i.e. Written examination</td>
									</tr>
									<tr>
										<td>Viva- Voice</td>
										<td>Personality Test</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="pre-exams">
				<div class="heading">
					<h2>Preliminary Examination:-</h2>
					<p style="font-weight:500;">The Preliminary examination for the GPSC will consist of two compulsory papers (Objective Type) of 200 Marks and 3 hours each.</p>
				</div>
				<div class="row mt-5 m-b-20">
					<div class="col-sm-12">
						<div class="border rounded lightbluebackground p-2">
							<h3 class="mainHeading text-center pt-2 pb-2">Preliminary Examination(objective Type & multiple choice)</h3>
							<div class="row">
								<div class="col-sm-12 col-12 pb-3">
									<div class="table-responsive pr-2 pl-2 pb-1 pt-1">
										<table class="table mb-0">
											<thead>
												<tr class="border-top"><th class="textDarkBlue border-0">Exam</th>
													<th class="textDarkBlue border-0">No. of Questions</th>
													<th class="textDarkBlue border-0">Total Marks</th>
													<th class="textDarkBlue border-0">Time</th>
												</tr>
											</thead>
											<tbody class="textGray">
												<tr class="border-bottom border-top">
													<td class="text-truncate">Paper-I</td>
													<td class="text-truncate">200</td>
													<td class="text-truncate">200</td>
													<td class="text-truncate">3 Hours </td>
												</tr>
												<tr class="border-bottom border-top">
													<td class="text-truncate">Paper-II</td>
													<td class="text-truncate">200</td>
													<td class="text-truncate">200</td>
													<td class="text-truncate">3 Hours</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row m-t-30 m-b-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">GPSC Prelims syllabus: Paper I</th>
										<th class="textDarkBlue border-0">PCS KAKA Library</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">History: 
1.Indus valley Civilisation: Features, Sites, Society, Cultural History, Art and Religion. Indus Valley Civilisation and Gujarat. 
2.Vedic age- Jainism and Buddhism. 
3.Foreign invasions on India and their impact. 
4.The Mauryan and the Gupta Empire- their administration- social, religious and economic conditions-art, architecture, literature, science and technology. 
5.Kanishka , Harsha and South Indian Dynasties. 
6. The Delhi Sultanate, Vijaynagar Empire and the Mughal Empire. 
7.The Bhakti Movement and Sufism. 
8.The Chavada, Solanki and Vaghela Dynasties of Gujarat- their rulers - administration, economy, society, religion, literature, arts and architecture. 
9.Independent Sultanate of Gujarat – Sultan Ahmedashah I, Mahmud Begda and Bahadurshah. 
10.Gujarat under the Mughals and the Marathas, Gaekwad’s rule in Baroda- Walker’s Settlement. 
11.The European Trading companies in India- their struggle for supremacy- with special reference to Bengal, Mysore, Marathas and Hyderabad. 
12. Governor-Generals and Viceroys. 
13.Indian war of Independence of 1857 - Origin, nature, causes, consequences and significance with special reference to Gujarat. 
14.Religious and social Reform Movements in 19th Century in India and Gujarat. 
15.India’s Freedom Movement, Revolutionaries in India and abroad. 
16.Reform measures by the Princely States of Saurashtra, Kutchh and Gujarat with special reference to Sayajirao Gaekwad III of Baroda, Bhagwatsinhji of Gondal, Waghji-II of Morbi, Bhavsinhji II of Bhavnagar, Lakhajiraj of Rajkot and Ranjitsinh of Nawanagar. 
17.Mahatma Gandhi, his thoughts, principles and philosophy. Important Satyagrahas with special reference to Satyagrahas of Gujarat - Kheda, Borsad, Bardoli, Dharasana, Dholera, Rajkot and Limbadi. 
18.The Role of Sardar Patel in freedom movement and post independence consolidation. 
19.Dr. B.R. Ambedkar, his life and contribution to making of Indian Constitution. 
20.India after Independence - Reorganization of the States within the country, Maha Gujarat Movement, Major events. 
Cultural Heritage:
1. Cultural Heritage of India and Gujarat: Art forms, Literature, Sculpture and Architecture. 
2. Indian saint tradition and its impact on psyche of people. 
3. Indian life style, Fairs, Festivals, Food, Costumes and Traditions. 
4. Indian Music and its importance. 
5. Gujarat’s Museums, Activities of Libraries, Cultural-Religious and Literary importance. 
6. Language and dialects of Gujarat. 
7. Gujarati Theatre: Drama, songs and different groups. 
8. Life of Adivasi (Tribes): Festivals, Fair, Costumes, Rituals etc. 
9. Gujarati Literature: Modes, Streams, Litterateurs and Literary Organizations. 10. Pilgrimage and Tourist Places of Gujarat.
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Kaka’s Library contains more than <b>4000 question</b> spanning from ancient India to Modern India. Also containing questions from Art and Culture. History library is divided in to subsections such as Ancient India, Medieval India, Modern India and Art and culture.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Constitution, Polity, Social Justice and International relations:
1.Indian Constitution : Evolution, features, Preamble, Fundamental Rights, Fundamental Duties, Directive Principles of State Policy, Amendments, Significant Provisions and Basic Structure. 
2.Functions and Responsibilities of the Union and the States, Parliament and State Legislatures: Structure, Function, Power and Privileges. Issues and challenges pertaining to Federal Structure: Devolution of Power and Finances up to local levels and Challenges therein. 
3.Constitutional Authorities: Powers, Functions and Responsibilities. 
4.Panchayati Raj. 
5.Public Policy and Governance. 
6.Impact of Liberalization, Privatization and Globalization on Governance. 7.Statutory, Regulatory and Quasi-judicial bodies. 
8.Rights Issues (Human rights, Women rights, SC/ST rights, Child rights) etc. 9.India’s Foreign Policy – International Relations – Important Institutions, Agencies and Fora, their structure and mandate. 
10.Important Policies and Programmes of Central and State Governments.
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Indian Polity section contains more than <b>2500 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">General Mental Ability
1.Logical Reasoning and Analytical Ability. 
2.Number Series, Coding –Decoding. 
3.Problems related to Relations. 
4.Shapes and their Sub-sections, Venn Diagram. 
5.Problems based on Clocks, Calendar and Age. 
6.Number system and order of Magnitude. 
7.Linear Equations - in one or two Variables. 
8.Ratio, proportion and variation. 
9.Average of mean, median, mode – including weighted mean. 
10.Power and exponent, Square, Square Root, Cube Root, H.C.F. and L.C.M. 11.Percentage, Simple and Compound Interest, Profit and loss. 
12.Time and Work, Time and Distance, Speed and Distance. 
13.Area and Perimeter of Simple Geometrical Shapes, Volume and Surface Area of Sphere, Cone, Cylinder, cubes and Cuboids. 
14.Lines, angels and common geometrical figures – properties of transverse of parallel lines, properties of related to measure of sides of a triangles, Pythagoras theorem, quadrilateral, rectangle, parallelogram, and rhombus. 
15.Introduction to algebra – BODMAS, simplification of weird symbols. 16.Data interpretation, Data Analysis, Data sufficiency, Probability.
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
							<div class="row m-t-30 m-b-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">GPSC Prelims syllabus: Paper II</th>
										<th class="textDarkBlue border-0">PCS KAKA Library</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">On Current Events of National and International Importance, candidates will be expected to have a thorough knowledge about them. </td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">On a given date Kaka’s libraray has more than <b>1500 questions</b> to practice from. Library is divided into monthly current affairs by Month such as Jan, Feb, etc. On a given day you have access to last 15 months current affairs.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Geography:
1. General Geography: Earth in Solar system, Motion of the Earth, Concept of time, Season, Internal Structure of the Earth, Major landforms and their features. Atmosphere-structure and composition, elements and factors of Climate, Airmasses and Fronts, atmospheric disturbances, climate change. Oceans: Physical, chemical and biological characteristics, Hydrological Disasters, Marine and Continental resources. 
2. Physical: World, India and Gujarat : Major physical divisions, Earthquakes, landslides, Natural drainage, climatic changes and regions, Monsoon, Natural Vegetation, Parks and Sanctuaries, Major Soil types, Rocks and Minerals. 
3. Social: World, India and Gujarat : distribution, density, growth, Sex-ratio, Literacy, Occupational Structure, SC and ST Population, Rural-Urban components, Racial, tribal, religious and linguistic groups, urbanization, migration and metropolitan regions. 
4. Economic: World, India and Gujarat: Major sectors of economy, Agriculture, Industry and Services, their salient features. Basic Industries-Agro, mineral, forest, fuel and manpower based Industries, Transport and Trade, Pattern and Issues.
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Geography section of Kaka’s library contains more than <b>3000 questions</b>. Geography has been divided into subsections such as Physical Geography, Indian Geography and World Geography.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Indian Economy and Planning:
1.Indian Economy on the eve of Independence, Emergence and development of planning exercise in India – historical debates, plan models and shift in focus over time. Indian economy in post reform period: New Economic Reforms, NITI Aayog : aims, constitution and functions. 
2.Agriculture sector: Institutional Structure- Land Reforms in India; Technological change in agriculture- Major Crops and Cropping Patterns in various parts of the country, Irrigation, pricing of agricultural inputs and outputs; Terms of trade between agriculture and industry; Agricultural Finance Policy; Agricultural Marketing and Warehousing; Issues in Food Security and Public Distribution System, Green Revolution, policies for sustainable agriculture and organic farming. 
3.Industrial policy; Public Sector enterprises and their performance; Privatization and disinvestments debate; Growth and pattern of industrialization; Small-scale sector; Productivity in industrial sector; Special Economic Zones (SEZ) and industrialization, foreign investment and competition policy. 
4.Infrastructure in Indian Economy: Meaning and Importance of Infrastructure - Water supply and Sanitation - Energy and Power - Science and Technology - Rural and Urban Infrastructure:Ports, Roads, Airports, Railway, Telecommunication. Social Impact Assessment. 
5.Trends and patterns in structure of population over time – growth rate, gender, rural-urban migration, literacy, regional; Structure and trends of Poverty and Inequality; Unemployment – trends, structure and National Rural Employment Policies. Indicators of development-Physical Quality of Life Index, Human Development Index, Human Poverty Index, Gender Development Index, National Happiness Index. 
6.Indian Public finance; Indian tax system, public expenditure, public debt, deficit and subsidies in the Indian economy. Center-State financial relation. Recent fiscal and monetary policy issues and their impact, GST: Concept and Implications. 7.Trend, composition, structure and direction of India’s Foreign Trade. India’s Balance of payments Situation in post reforms period. 8.Gujarat economy-An overview; social sector in Gujarat- Education, Health, and Nutrition. Gujarat Economy in relation to India and major states in recent decades,major problems in agriculture, forest, water resources, mining, industry and service Sector. Development policies for economic and social infrastructure – An appraisal.
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Economics section contains more than <b>2000 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Environmental Science: Issues and concerns related to environment; Its legal aspects, policies and treaties for the protection of environment at the national and the international level; Biodiversity- its importance and concerns; Climate Change, International Initiatives (Policies, Protocols) and India’s commitment; Forest and Wildlife - Legal framework for Forest and Wildlife Conservation in India; Environmental Hazards, pollution, carbon emission, Global warming. National Action plans on Climate Change and Disaster management. Biotechnology andNanotechnology; Nature, Scope and application, Ethical, Social, and Legal issues, Government Policies. Genetic Engineering; Issues related to it and its impact on human life. Health & Environment.</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Environment section contains more than <b>2500 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Science and Technology:
1.	Science and Technology: Nature and Scope of Science & Technology; Relevance of Science & Technology to the day to day life; National Policy on Science, Technology and Innovation; Institutes and Organization in India promoting integration of Science, Technology and Innovation, their activities and contribution; Contribution of Prominent Indian Scientists. 
2.	Information and Communication Technology (ICT): Nature and Scope of ICT; ICT in day to day life; ICT and Industry; ICT and Governance - Various government schemes promoting use of ICT, E-Governance programmes and services; Netiquettes; Cyber Security Concerns - National Cyber Crime Policy.
3.	Technology in Space & Defence: Evolution of Indian Space Programme; Indian Space Research Organization (ISRO) – it’s activities and achievements; Various Satellite Programmes – Satellites for Telecommunication, Indian Regional Navigation Satellite System (IRNSS), Indian Remote Sensing (IRS) Satellites; Satellites for defence, Eduset or Satellites for academic purposes; Defence Research and Development Organization (DRDO)- vision, mission and activities.
4.	Energy Requirement and Efficiency: India’s existing energy needs and deficit; India’s Energy Resources and Dependence, Energy policy of India - Government Policies and Programmes. 
5.	Nuclear Policy of India and its commitment to the world: India’s Nuclear power programme; India’s nuclear cooperation with other countries; Nuclear Suppliers Group (NSG) and India; Nuclear weapons policy of India; Draft Nuclear Doctrine of India, Nuclear Non-Proliferation Treaty (NPT), Comprehensive Test Ban Treaty (CTBT), Fissile Material Cut off Treaty (FMCT), Conference on Disarmament (CD); Nuclear Security Summit (NSS) and India.
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Science section contains more than <b>2500 questions</b> to practice from. Science has been divided into subsections such as Physics, Chemistry and Biology.
											Aspirant can study and test his knowledge by choosing any subsection to attempt the questions.
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
