<link href="<?php echo base_url(); ?>assets/siteasset/css/pcs/jpsc.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>JPSC</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
			<h2>What we do</h2>
			<p><b>PCS KAKA</b> is a digital library designed to cater needs of state civil service aspirants. It has all the subjects required for the preparation of UP PCS examination. Our library has more than 20,000 questions covering various topics from all the books available in market. Our Library covers the following subjects from UP PCS exam’s perspective: History, Geography, Polity, Economics, Environment, Science, Current affairs of state and national importance. This library is completely digital. Available 24x7. </p>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Comparative Analysis :</b> Get your rank after completing a practice test. See where you stand in this competitive world.</span>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Immediate Result :</b> The moment you submit the practice test; answers will be displayed with questions. See where you went wrong.</span>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Unlimited attempts: </b> You can take any practice test unlimited number of times. Improve your score and hence your memorizing power of facts and figures.</span>
		</div>			
	</div>
		
	<div class="pcs-info ">
		<div class="container">
			<div class="section-pcs-info m-b-30">
				<h2>What is JPSC? </h2>
				<p>Jharkhand Public Service commission (JPSC) conducts exam for the recruitment to the posts in Jharkhand Civil Services, i.e. Deputy superintendent of police, Block development officer, District Panchayati Raj Officer, District welfare officer, Sub-divisional officer, Additional collector, Deputy development commissioner, deputy commissioner. - <a href="https://jpsc.gov.in">https://jpsc.gov.in.</a>
				To participate in JPSC exam, candidate should have graduate in any discipline from recognized university.
				</p>
				<p><b>Selection Process:</b> The competitive exam comprises of Preliminary exam (Objective Type) of 400 marks, Main Exam (Written Type) of 1050 marks and The interview of 100 marks</p>
				<div class="row m-t-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">Exam Type</th>
										<th class="textDarkBlue border-0">Educational Criteria</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td>Preliminary Examination</td>
										<td>Objective Type & Multiple choices</td>
									</tr>
									<tr>
										<td>Main Examination</td>
										<td>Conventional Type, i.e. Written examination</td>
									</tr>
									<tr>
										<td>Viva- Voice</td>
										<td>Personality Test</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="pre-exams">
				<div class="heading">
					<h2>Preliminary Examination:-</h2>
					<p style="font-weight:500;">The Preliminary examination for the JPSC will consist of two compulsory papers (Objective Type) of 200 Marks and 2 hours each.</p>
				</div>
				<div class="row mt-5 m-b-20">
					<div class="col-sm-12">
						<div class="border rounded lightbluebackground p-2">
							<h3 class="mainHeading text-center pt-2 pb-2">Preliminary Examination(objective Type & multiple choice)</h3>
							<div class="row">
								<div class="col-sm-12 col-12 pb-3">
									<div class="table-responsive pr-2 pl-2 pb-1 pt-1">
										<table class="table mb-0">
											<thead>
												<tr class="border-top"><th class="textDarkBlue border-0">Exam</th>
													<th class="textDarkBlue border-0">No. of Questions</th>
													<th class="textDarkBlue border-0">Total Marks</th>
													<th class="textDarkBlue border-0">Timing</th>
												</tr>
											</thead>
											<tbody class="textGray">
												<tr class="border-bottom border-top">
													<td class="text-truncate">Paper-I</td>
													<td class="text-truncate">100</td>
													<td class="text-truncate">200</td>
													<td class="text-truncate">9.30 to 11.30 A.M. </td>
												</tr>
												<tr class="border-bottom border-top">
													<td class="text-truncate">Paper-II</td>
													<td class="text-truncate">100</td>
													<td class="text-truncate">200</td>
													<td class="text-truncate">2.30 to 4.30 P.M.</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row m-t-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">Prelims Syllabus</th>
										<th class="textDarkBlue border-0">Duration</th>
										<th class="textDarkBlue border-0">Total Marks</th>
										<th class="textDarkBlue border-0">Total Questions</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td>GS-I</td>
										<td>Two hours</td>
										<td>200</td>
										<td>100</td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row m-t-30 m-b-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">JPSC Prelims syllabus</th>
										<th class="textDarkBlue border-0">PCS KAKA Library</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">National and International Current Events ( 15 Questions)</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">On a given date Kaka’s libraray has more than <b>1500 questions</b> to practice from. Library is divided into monthly current affairs by Month such as Jan, Feb, etc. On a given day you have access to last 15 months current affairs.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">1. Ancient India (5 Questions): Indus valley Civilisation, Vedic age and Foreign invasions on India and their impact
2.	Medieval India (5 Questions): The Mauryan and the Gupta Empire, Kanishka , Harsha and South Indian Dynasties, The Delhi Sultanate, Vijaynagar Empire and the Mughal Empire, The Bhakti Movement and Sufism, The European Trading companies in India
3.	Modern India (5 Queations) : Governor-Generals and Viceroys, Indian war of Independence of 1857, Religious and social Reform Movements in 19th Century in India, India’s Freedom Movement, Revolutionaries in India and abroad, Mahatma Gandhi, his thoughts, principles and philosophy, India after Independence - Reorganization of the States within the country. Jharkhand Specific Questions( general Awareness of its history, society, culture and heritage) 10 Questions
 </td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Kaka’s Library contains more than <b>4000 question</b> spanning from ancient India to Modern India. Also containing questions from Art and Culture. History library is divided in to subsections such as Ancient India, Medieval India, Modern India and Art and culture.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"><b>Indian Polity and Governance -</b> Indian Constitution (4 Questions) : Evolution, features, Preamble, Fundamental Rights, Fundamental Duties, Directive Principles of State Policy, Amendments, Significant Provisions and Basic Structure. 
2. Public Administration and good governance (4 questions) 
3.De-centralization: Panchayats and municipalities(2 questions)
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Indian Polity section contains more than <b>2500 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Geography Of India: 
1. General Geography (3 Questions): Earth in Solar system, Motion of the Earth, Concept of time, Season, Internal Structure of the Earth, Major landforms and their features. Atmosphere-structure and composition, elements and factors of Climate, Airmasses and Fronts, atmospheric disturbances, climate change. Oceans: Physical, chemical and biological characteristics, Hydrological Disasters, Marine and Continental resources. 
2. Physical (3 Questions): World, India and Gujarat : Major physical divisions, Earthquakes, landslides, Natural drainage, climatic changes and regions, Monsoon, Natural Vegetation, Parks and Sanctuaries, Major Soil types, Rocks and Minerals. 
3. Social (2 Questions) : World, India and Gujarat : distribution, density, growth, Sex-ratio, Literacy, Occupational Structure, SC and ST Population, Rural-Urban components, Racial, tribal, religious and linguistic groups, urbanization, migration and metropolitan regions. 
4. Economic (2 Questions): World, India and Gujarat: Major sectors of economy, Agriculture, Industry and Services, their salient features. Basic Industries-Agro, mineral, forest, fuel and manpower based Industries, Transport and Trade, Pattern and Issues.
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Geography section of Kaka’s library contains more than <b>3000 questions</b>. Geography has been divided into subsections such as Physical Geography, Indian Geography and World Geography.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"><b>Economic and Social Development - </b>1.	Basic features of Indian Economy (4 queations)
2.	Sustainable development and economic issues (4 questions)
3.	Economic reforms and globalization( 2 questions). World, India and Gujarat: Major sectors of economy, Agriculture, Industry and Services, their salient features. Basic Industries-Agro, mineral, forest, fuel and manpower based Industries, Transport and Trade, Pattern and Issues.
 </td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Economics section contains more than <b>2000 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">General Question of Misc Nature (15 Questions)
Human rights, environment protection, Bio-diversity & Climate change, Urbanization, Sports, Disaster Management, Poverty and Un-employment, awards and UN and Other International agencies.
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Environment section contains more than <b>2500 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"><b>General Science: - </b>1.	General Science (6 Questions): 
2.	Agriculture and technology Development (6 questions)
3.	Information and communication technology (3 questions)
</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Science section contains more than <b>2500 questions</b> to practice from. Science has been divided into subsections such as Physics, Chemistry and Biology.
											Aspirant can study and test his knowledge by choosing any subsection to attempt the questions.
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="section-pcs-info m-b-30">
					<h2 class="text-center" style="font-size: 25px;"><ins>Paper-II General Studies-II Duration: Two hours Marks - 200</ins></h2>
				</div>
				<div class="section-pcs-info m-b-30">
					<div class="row">
						<div class="col-md-6 box" style="border-right:none;border-bottom:none;">
							<p><b>1. History of Jharkhand:: - </b></p>
							<ol>
								<li>Munda Administration (1 Ques)</li>
								<li>Nagvanshi Administration (1 Ques)</li>
								<li>Padaha Council Administration (1 Ques) </li>
								<li>Manjhi Pargana Administration (1 Ques) </li>
								<li>Munda manki Administration (1 Ques) </li>
								<li>Dhoklo Sohor Administration (1 Ques)</li>
								<li>Ethinic Panchayat Administration (2 Ques)</li>
							</ol>
						</div>
						<div class="col-md-6 box" style="border-bottom:none;">
							<p><b>2. Jharkhand Movement: - </b></p>
							<ol>
								<li>House of Movement (1 ques)</li>
								<li>Tribes of Jharkhand (1 ques)</li>
                                <li>Freedom Fighter of Jharkhand (1 ques)</li>
                                <li>Vibhuti  of Jharkhand (2 ques)</li>
                                <li>Jharkhand Movement and formation of state(2 ques)</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 box" style="border-right:none;">
						
							<ol>
								<li>Social condition of Jharkhand (1 ques) </li>
								<li>Cultural condition of Jharkhand (1 ques)</li>
				                <li>Political condition of Jharkhand (1 ques)</li>
				                <li>Economic condition of Jharkhand (1 ques)</li>
				                <li>Religious condition of Jharkhand (1 ques)</li>
				                <li>Folk Literature (1 Ques)</li>
				                <li>Traditional Art and Folk Dance (1 Ques)</li>
				                <li>Important Places (1 Ques)</li>
				                <li>Folk Music and Instrument(1 Ques)</li>
				                <li>Tribes (1 Ques)</li>
							</ol>
						</div>
						<div class="col-md-6 box " >
						
							<ol>
								<li>Literature and writer ( 5 Questions) </li>
                                <li>Important Education Institution (3 ques)</li>
								<li>Sports(5 Ques) </li>
				                <li>Land Law: a) CNT(5 ques),  b) SPT (5 Ques) c)  others (2 ques)</li>
				                <li>Geography of the state (10 questions) Forest, River, Mountain, mines, and minerals</li>
				                <li>Industrial Policy, Migration, and rehabilitation (6 ques)</li>
				                <li>Important Industry name, place and development (5 ques)</li>
				                <li>Important plan of state and sub-plan (5 ques)</li>
				                <li>Forest management and wild life conservation program (5 ques)</li>
				                <li>Jharkhand environment changes, mitigation and adaptation (7 ques)</li>
				                <li>Disaster management (5 ques)</li>
				                <li>Current Affairs of State (7 ques)</li>
							</ol>
						</div>
					</div>
				</div>
						
			</div>
		</div>
	</div>
</div>
