<link href="<?php echo base_url(); ?>assets/siteasset/css/pcs/ukpsc.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>UKPSC</b></h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->
<div id="product" class="section-container p-t-20">
	<!-- BEGIN container -->
	<div class="container m-b-40">
		<div class="section-header">
			<h2>What we do</h2>
			<p><b>PCS KAKA</b> is a digital library designed to cater needs of state civil service aspirants. It has all the subjects required for the preparation of UP PCS examination. Our library has more than 20,000 questions covering various topics from all the books available in market. Our Library covers the following subjects from UP PCS exam’s perspective: History, Geography, Polity, Economics, Environment, Science, Current affairs of state and national importance. This library is completely digital. Available 24x7. </p>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Comparative Analysis :</b> Get your rank after completing a practice test. See where you stand in this competitive world.</span>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Immediate Result :</b> The moment you submit the practice test; answers will be displayed with questions. See where you went wrong.</span>
		</div>
		<div class="text">
			<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Unlimited attempts: </b> You can take any practice test unlimited number of times. Improve your score and hence your memorizing power of facts and figures.</span>
		</div>			
	</div>
		
	<div class="pcs-info ">
		<div class="container">
			<div class="section-pcs-info m-b-30">
				<h2>What is UP PCS or What is UP PSC? </h2>
				<p>UP PCS stands for Uttar Pradesh Provincial Civil Services Examination. This exam is conducted by Uttar Pradesh Public Service Commission, Prayagraj -  <a href="http://uppsc.up.nic.in.">http://uppsc.up.nic.in.</a>
					PCS officers hold various posts at sub-divisional, district, divisional and state level from conducting revenue administration and maintenance of law and order.
				</p>
				<p><b>Selection Process:</b> UPPSC PCS 2019 selection for PCS (Combined State/Upper Subordinate Services), Assistant Conservator of Forest (ACF) and Range Forest Officer (RFO) will be based on:-</p>
				<div class="row m-t-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">Exam Type</th>
										<th class="textDarkBlue border-0">Educational Criteria</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td>Preliminary Examination</td>
										<td>Objective Type & Multiple choices</td>
									</tr>
									<tr>
										<td>Main Examination</td>
										<td>Conventional Type, i.e. Written examination</td>
									</tr>
									<tr>
										<td>Viva- Voice</td>
										<td>Personality Test</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="pre-exams">
				<div class="heading">
					<h2>Preliminary Examination:-</h2>
					<p style="font-weight:500;">The Preliminary examination for the Combined State / Upper Subordinate Services (General Recruitment / Physically Handicapped – Backlog / Special Recruitment) Examination and Assistant Conservator of Forest / Range Forest Officer Services Examination will consist of two compulsory papers of which answer sheet be on OMR sheets.</p>
				</div>
				<div class="row mt-5 m-b-20">
					<div class="col-sm-12">
						<div class="border rounded lightbluebackground p-2">
							<h3 class="mainHeading text-center pt-2 pb-2">Preliminary Examination(objective Type & multiple choice)</h3>
							<div class="row">
								<div class="col-sm-12 col-12 pb-3">
									<div class="table-responsive pr-2 pl-2 pb-1 pt-1">
										<table class="table mb-0">
											<thead>
												<tr class="border-top"><th class="textDarkBlue border-0">Exam</th>
													<th class="textDarkBlue border-0">No. of Questions</th>
													<th class="textDarkBlue border-0">Total Marks</th>
													<th class="textDarkBlue border-0">Timing</th>
												</tr>
											</thead>
											<tbody class="textGray">
												<tr class="border-bottom border-top">
													<td class="text-truncate">Paper-I</td>
													<td class="text-truncate">100-150</td>
													<td class="text-truncate">200</td>
													<td class="text-truncate">9.30 to 11.30 A.M. </td>
												</tr>
												<tr class="border-bottom border-top">
													<td class="text-truncate">Paper-II</td>
													<td class="text-truncate">100-150</td>
													<td class="text-truncate">200</td>
													<td class="text-truncate">2.30 to 4.30 P.M.</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<p style="font-weight:500"><b style="color:red;">Note:-</b><br><b> (1) Paper-II</b> of the Preliminary Examination will be a qualifying paper with minimum qualifying marks fixed at 33%.<br> 
					<b>(2)</b> It is mandatory for the Candidates to appear in both the papers of Preliminary Examination for the purpose of evaluation. Therefore, a candidate will be disqualified in case he does not appear in both in papers.<br> 
					<b>(3)</b> The merit of the Candidates will be determined on the basis of marks obtained in Paper-I of the Preliminary Examination.
				</p>

				<div class="row m-t-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">Prelims Syllabus</th>
										<th class="textDarkBlue border-0">Duration</th>
										<th class="textDarkBlue border-0">Total Marks</th>
										<th class="textDarkBlue border-0">Total Questions</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td>GS-I</td>
										<td>Two hours</td>
										<td>200</td>
										<td>150</td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row m-t-30 m-b-30">
					<div class="col-sm-12">
						<div class="table-responsive border rounded lightbluebackground pr-2 pl-2 pb-1 pt-1">
							<table class="table mb-0">
								<thead class="lightBlueBgColor">
									<tr>
										<th class="textDarkBlue border-0">UP PCS Prelims syllabus</th>
										<th class="textDarkBlue border-0">PCS KAKA Library</th>
									</tr>
								</thead>
								<tbody class="textGray">
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"><b>Current events of national and international Importance: -</b> On Current Events of National and International Importance, candidates will be expected to have knowledge about them. 	On a given date Kaka’s libraray has more than 1500 questions to practice from. Library is divided into monthly current affairs by Month such as Jan, Feb, etc. On a given day you have access to last 15 months current affairs.</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">On a given date Kaka’s libraray has more than <b>1500 questions</b> to practice from. Library is divided into monthly current affairs by Month such as Jan, Feb, etc. On a given day you have access to last 15 months current affairs.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"><b>History of India & Indian National Movement: -</b> In History emphasis should be on broad understanding social, economic and political aspects of Indian History. In the Indian National Movement, the candidates are expected to have synoptic view of nature and character of the freedom movement, growth of nationalism and attainment of Independence. </td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Kaka’s Library contains more than <b>4000 question</b> spanning from ancient India to Modern India. Also containing questions from Art and Culture. History library is divided in to subsections such as Ancient India, Medieval India, Modern India and Art and culture.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"><b>Indian Polity and Governance -</b> Constitution, Political System, Panchayati Raj, Public Policy, Rights Issues, etc.: - In Indian Polity, Economic and Culture, questions will test knowledge of country's political system including Panchayati Raj and Community Development, broad features of Economic policy in India and Indian Culture. </td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Indian Polity section contains more than <b>2500 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"><b>India and World Geography - </b> Physical, Social, Economic geography of India and the World: In World Geography only general understanding of the subject will be expected. Questions on the Geography of India will relate to Physical, Social & Economic Geography of India. </td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Geography section of Kaka’s library contains more than <b>3000 questions</b>. Geography has been divided into subsections such as Physical Geography, Indian Geography and World Geography.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"><b>Economic and Social Development - </b>Sustainable Development, Poverty, Inclusion, Demographics, Social Sector Initiatives, etc.: - The candidates will be tested with respect to problems and relationship between Population, Environment and Urbanization. </td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Economics section contains more than <b>2000 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"><b>General Issues on Environmental ecology, Bio-diversity and Climate Change - </b>that do not require subject specialization, General awareness of the subject is expected from candidates.  </td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Environment section contains more than <b>2500 questions</b> to practice from.</td>
									</tr>
									<tr>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;"><b>General Science: - </b>Questions on General Science will cover general appreciation and understanding of Science including matters of every day observation and experience, as may be expected of a well-educated person, who has not made a special study of any scientific discipline.</td>
										<td style="width:50%;line-height: 1.7em;border-right: 1px solid #dee2e6;">Science section contains more than <b>2500 questions</b> to practice from. Science has been divided into subsections such as Physics, Chemistry and Biology.
											Aspirant can study and test his knowledge by choosing any subsection to attempt the questions.
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="section-pcs-info m-b-30">
					<h2 class="text-center" style="font-size: 25px;"><ins>Paper-II General Studies-II Duration: Two hours Marks - 200</ins></h2>
					<p class="text-center">Comprehension. Interpersonal skills including communication skills. Logical reasoning and analytical ability. Decision making and problem solving. General mental ability. </p>
					<div class="text m-t-20">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>Elementary Mathematics up to Class X Level-</b>Elementary Mathematics up to Class X Level-</span>
					</div>
					<div class="text">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>General English up to Class X level. </b></span>
					</div>
					<div class="text">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp;&nbsp;<span><b>General Hindi up to Class X level. </b></span>
					</div>
				</div>
				<div class="section-pcs-info m-b-30">
					<h3 style="font-size: 25px;">Elementary Mathematics (Up to Class X Level) </h3>
					<div class="row">
						<div class="col-md-6 box" style="border-right:none;border-bottom:none;">
							<p><b>1. Arithmetic: - </b></p>
							<ol>
								<li> Number systems: Natural Numbers, Integers, Rational and Irrational numbers,<br> Real numbers, Divisors of an Integer, prime Integers, L.C.M. and H.C.F. of<br> integers and their Interrelationship.</li>
								<li>Average</li>
								<li>Ratio and proportion </li>
								<li>Percentage </li>
								<li>Profit and Loss </li>
								<li>Simple and Compound Interests </li>
								<li>Work and Time </li>
								<li>Speed, Time and Distance. </li>
							</ol>
						</div>
						<div class="col-md-6 box" style="border-bottom:none;">
							<p><b>2. Algebra: - </b></p>
							<ol>
								<li>Factors of polynomials, L.C.M. and H.C.F. of polynomials and their Interrelationship,<br> Remainder theorem, simultaneous linear equations, quadratic equations. </li>
								<li>Set Theory: - Set, null set, subsets and proper subsets of a set, operations (Union,<br> Intersections, difference, symmetric difference) between sets, Venn diagram. </li>

							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 box" style="border-right:none;">
							<p><b>3. Geometry: - </b></p>
							<ol>
								<li> Constructions and theorems regarding triangle, rectangle, square, trapezium and circles, their perimeter and area. </li>
								<li>Volume and surface area of sphere, right circular cylinder, right circular Cone and Cube. </li>
				
							</ol>
						</div>
						<div class="col-md-6 box " >
							<p><b>4. Statistics: -</b></p>
							<ol>
								<li>Collection of data, Classification of data, frequency, frequency distribution, tabulation, cumulative frequency. Representation of data - Bar diagram, Pie chart, histogram, frequency polygon, cumulative frequency curves (ogives), Measures of Central tendency: Arithmetic Mean, Median and Mode. </li>

							</ol>
						</div>
					</div>
				</div>
				<div class="section-pcs-info m-b-30">
					<h3 style="font-size: 25px;">General English up to Class X Level </h3>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<ol>
								<li>Comprehension</li>
								<li>Active Voice and Passive Voice </li>
								<li>Parts of Speech </li>
								<li>Transformation of Sentences </li>
								<li>Direct and Indirect Speech </li>
								<li>Punctuation and Spellings </li>
								<li>Words meanings </li>
								<li>Vocabulary & Usage </li>
								<li>Idioms and Phrases </li>
								<li>Fill in the Blanks </li>

							</ol>
						</div>
					</div>
				</div>					
			</div>
		</div>
	</div>
</div>
