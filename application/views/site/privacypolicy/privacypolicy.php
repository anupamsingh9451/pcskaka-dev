<link href="<?php echo base_url(); ?>assets/siteasset/css/privacypolicy/privacypolicy.css" rel="stylesheet" />
<main>
  <h1 style="margin-top: 2%;">Privacy Policy</h1>
  <h2>Who we are?</h2>
  <p>Our website address is: www.pcskaka.com<br>
We collect your certain details such as email id, ip address, moblile no, url you are coming from , url you are going to ,etc all this is done to give you better user experience and services.
<br>We also collect and store personal information provided by you from time to time on the website. We only collect and use such information from you that we consider necessary for achieving a seamless, efficient and safe experience, customized to your needs.
<br>
</p>
<br>
<h2>What personal data we collect and why we collect it?</h2><br>
<h3>Comments</h3>
<p> When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor’s IP address and browser user agent string to help spam detection. </p>
<br>
<h3>Media</h3>
<p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. </p>
<br>
<h3>Cookies</h3>
<p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for a period as determined by the website owner.
If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data.
</p>
<br>
<h3>How long we retain your data</h3>
<p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.
For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.
</p>
<br>
<h3>What rights you have over your data</h3>
<p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. This service will be provided on “best effort basis” and is not guaranteed. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes. </p>
<br>
<h3>Where we send your data</h3>
<p>Visitor comments may be checked through an automated spam detection service. </p>
<br>

<h3>To contact us for any clarifications please email to:</h3>
  <a href="mailto:support@pcskaka.com" style="margin-bottom: 2%;">support@pcskaka.com</a>
</main>