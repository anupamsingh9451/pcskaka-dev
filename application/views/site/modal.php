<link href="<?php echo base_url(); ?>assets/siteasset/css/modal.css" rel="stylesheet" />
<div class="modal fade login" id="exampleModalCenter">
  <div class="modal-dialog login animated">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <div class="box">
                 <div class="content">
                     <h6 class="text-center form_title"></h6>
                    <div class="social">
                        <a class="circle github" href="#">
                            <i class="fa fa-github fa-fw"></i>
                        </a>
                        <a id="google_login" class="circle google" href="#">
                            <i class="fa fa-google-plus fa-fw"></i>
                        </a>
                        <a id="facebook_login" class="circle facebook" href="#">
                            <i class="fa fa-facebook fa-fw"></i>
                        </a>
                    </div>
                    <div class="division">
                        <div class="line l"></div>
                          <span>or</span>
                        <div class="line r"></div>
                    </div>
                    <div class="emailsentmsg" style="display:none;text-align: center;margin: 2px 0 10px 0;">
                          <span style="color: #005656;font-size: 14px;">OTP has been Sent to Your Email ID</span>
                    </div>
                    <div class="otpresendtmsg" style="display:none;text-align: center;margin: 2px 0 10px 0;">
                          <span style="color: #dc3545;font-size: 14px;">OTP has been Resent to Your Email ID</span>
                    </div>
                    <div class="error"></div>
                    <div class="form loginBox">
                        <span class="login_error" style="display:none;color: red;font-size: 12px;margin-bottom: 6px;"></span>
                        <form id="student_login">
                            <input class="form-control logemail" type="text" placeholder="Email" name="email">
                            <input class="form-control logpassword" type="password" placeholder="Password" name="password">
                            <button class="btn btn-default btn-login" type="Submit">Login</button>
                        </form>
                    </div>
                 </div>
            </div>
            <div class="box">
                <div class="content otpBox" style="display:none;">
                     <div class="form">
                        <form id="submit_otp">
                            <input class="form-control otp" type="text" placeholder="Enter OTP" name="otp">
                            <span class="otp_error" style="display:none;color: red;font-size: 11px;margin-bottom: 6px;">Please Enter OTP First *</span>
                            <span class="otp_resend_msg" style="display:none;color: green;font-size: 11px;margin-bottom: 6px;">OTP has been Resent to your Email Id *</span>
                            <input class="insertid" type="hidden" name="insertid">
                            <button class="btn btn-primary btn-otp" type="Submit" style="height: 43px;padding: 4px;margin-top:5px;">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="content registerBox" style="display:none;">
                     <div class="form">
                        <form id="student_reg">
                            <input class="form-control name" type="text" placeholder="Name" name="name">
                            <input class="form-control regemail" type="text" placeholder="Email" name="email">
                            <input class="form-control emailcnfrm" type="text" placeholder="Repeat Email" name="emailcnfrm">
                            <input class="form-control mobile input_num" type="text" placeholder="Contact" name="mobile">
                            <input class="form-control regpassword" type="password" placeholder="Password" name="password">
                            <input class="form-control cnfrmpassword" type="password" placeholder="Repeat Password" name="password_confirmation">
                            <button class="btn btn-default btn-register" type="Submit">Create account</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="content forgotpassBox" style="display:none;">
                     <div class="form">
                         <span class="forgotpass_error" style="display:none;color: red;font-size: 12px;margin-bottom: 6px;"></span>
                        <form id="forgot_pass_form">
                            <input class="form-control forgotemail" type="text" placeholder="Email" name="email">
                            <button class="btn btn-default btn-forgotpass" type="Submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="content otpBoxforgotpass" style="display:none;">
                     <div class="form">
                        <form id="submit_otp_forgotpass">
                            <input class="form-control otp" type="text" placeholder="Enter OTP" name="otp">
                            <span class="otp_error_forgotpass" style="display:none;color: red;font-size: 11px;margin-bottom: 6px;">Please Enter OTP First *</span>
                            <span class="otp_resend_msg" style="display:none;color: green;font-size: 11px;margin-bottom: 6px;">OTP has been Resent to your Email Id *</span>
                            <input class="insertid" type="hidden" name="insertid">
                            <button class="btn btn-primary btn-otpforgotpass" type="Submit" style="height: 43px;padding: 4px;margin-top:5px;">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="content forgot_confirmpass_Box" style="display:none;">
                     <div class="form">
                         <span class="forgot_confirmpass_error" style="display:none;color: red;font-size: 12px;margin-bottom: 6px;"></span>
                        <form id="forgot_confirmpass_form">
                            <input class="form-control regpasswordforgot" type="password" placeholder="Password" name="password">
                            <input class="insertid" type="hidden" name="insertid">
                            <input class="form-control cnfrmpasswordforgot" type="password" placeholder="Repeat Password" name="password_confirmation">
                            <button class="btn btn-forgot-confirmpas" type="Submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="footer-content login-footer">
                <span>Looking to
                     <a href="javascript: showRegisterForm();">create an account</a>
                </span>
                <br>
                <span>
                     <a href="javascript: showforgotpassForm();">Forgot Password</a>
                </span>
            </div>
            <div class="footer-content register-footer" style="display:none">
                 <span>Already have an account?</span>
                 <a href="javascript: showLoginForm();">Login</a>
            </div>
            <div class="footer-content forgotpass-footer" style="display:none">
                <span>Already have an account?</span>
                 <a href="javascript: showLoginForm();">Login</a>
            </div>
            <div class="footer-content otp-footer" style="display:none">
                 <span>OTP has not Receive ! </span>
                 <a href="javascript:void(0);" id="resendOtp">Resend OTP</a>
            </div>
        </div>
      </div>
  </div>
</div>
