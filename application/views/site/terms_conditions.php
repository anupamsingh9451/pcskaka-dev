<link href="<?php echo base_url(); ?>assets/siteasset/css/terms_conditions.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header"><b>Terms</b> & <b>Conditions</b> </h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->
			<!-- BEGIN #product -->
		<div id="product" class="section-container p-t-20">
			<!-- BEGIN container -->
			<div class="container">
				
				<!-- BEGIN row -->
				<div class="row row-space-30">
					<div class="col-md-12">
						<div class="container" style="width: 100%;">
				<div class="feature">
					<div class="row">
						<div class="col-ms-12">
						
							<div class="row">
								<div class="col-ms-12 col-sm-4" style="text-align: center;">
									<img class="wrap-class" width="100" height="100" alt="Select platform" src="<?php echo base_url('assets/frontendasset/img/svg/platform.svg');?>">								
									<h4>Website, App, Tablet or any other platform</h4>
									<p>Any platform you use to access any type of content of this website (pcskaka.com), you abide by the terms defined by the terms and other rules mentioned in this website.</p>
								</div>
								<div class="col-ms-12 col-sm-4" style="text-align: center;">
									<img class="wrap-class" width="100" height="100" alt="Customize policy" src="<?php echo base_url('assets/frontendasset/img/svg/questionnaire.svg');?>">								
									<h4>Customize policy</h4>
									<p>In order to provide you best content, we regularly modify the content of the site including text, pictures, graphs or any other media. Any user either paid or non-paid does not hold any rigts whatsoever on any type of content.</p>
								</div>
								<div class="col-ms-12 col-sm-4" style="text-align: center;">
									<img class="wrap-class" width="100" height="100" alt="Download and publish" src="<?php echo base_url('assets/frontendasset/img/svg/publish.svg');?>">								
									<h4>Download &amp; publish</h4>
									<p>As a user of this website, pcskaka.com, paid or unpaid, you are not allowed to download, copy or use any other means to store the content of this website in any form. You are not supposed to redistribute or publish or use any other means to reuse of this website in any form.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
						<p class="m-t-30" style="font-weight:500;">By using our site or App or any other mode, you comply by the following terms:-</p>

						<ol class="m-t-10" style="padding-left: 15px;">
							<li><p>The information contained in this website(pcskaka.com) is for general information purposes only. The information is provided by pcskaka.com and its team and while we endeavor to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p></li>
							<li><p>In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arise out of, or in connection with, the use of this website.</p></li>
							<li><p>Through this website you are able to link to other websites which are not under the control of pcskaka.com. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.</p></li>
							<li><p>Every effort is made to keep the website up and running smoothly. However, pcskaka.com takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.</p></li>
						    <br> <br>
						    <p>Team PCSKAKA</p>
                            <p>Email: support@pcskaka.com</p>
                            <p>Website: www.pcskaka.com</p>

						
						</ol>
					</div>
				</div>
			</div>
			<!-- END row -->
			
		</div>
		<!-- END #product -->