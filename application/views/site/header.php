<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
	<title><?php echo $title; ?></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	
	
	<!-- ===============================================dynamic meta tag Start============================================================ -->
	<meta name="description" content="<?php echo $description; ?>"/>
	<link rel="canonical" href="<?php echo $canonical; ?>" />
	<meta property="og:title" content="<?php echo $og_title; ?>" />
	<meta property="og:description" content="<?php echo $og_description; ?>" />
	<meta property="og:url" content="<?php echo $og_url; ?>" />
	<meta property="og:image" content="<?php echo $og_image; ?>" />
	<meta property="og:image:secure_url" content="<?php echo $og_image_secure_url; ?>" />
	<meta name="twitter:card" content="<?php echo $twitter_card; ?>" />
	<meta name="twitter:description" content="<?php echo $twitter_description; ?>" />
	<meta name="twitter:title" content="<?php echo $twitter_title; ?>" />
    <!-- ===============================================dynamic meta tag End============================================================ -->
	
	<meta name="robots" content="index, follow"/>
	
	<meta content="" name="author" />
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-151895981-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-151895981-1');
    </script>
    
    <script data-ad-client="ca-pub-7820286022657349" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js" ></script>
    
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/plugins/font-awesome/css/all.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/default.css" id="theme" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/slick.css" rel="stylesheet"/>
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/animate.css" rel="stylesheet"/>
	<link href="<?php echo base_url(); ?>assets/frontendasset/css/default.css" rel="stylesheet"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<link rel="icon" href="<?php echo base_url(); ?>assets/logo/logo.jpeg">
	<!-- ================== END BASE CSS STYLE ================== -->
	


	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/pace/pace.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery/jquery-3.3.1.min.js"></script>
	<link href="<?php echo base_url(); ?>assets/adminassets/css/main.css" rel="stylesheet" />
	<!-- ================== END BASE JS ================== -->
	<script>
		var api_loc="http://localhost:3000/";
		var base_loc="<?php echo base_url(); ?>";
		var clientserv="Ratan";
        var apikey="pcskaka";
	</script>
	<style>
	    .redborder{
	        border: 1px solid rgb(216, 128, 128) !important;
	    }
	    
	</style>
</head>
<body>
	<!-- BEGIN #page-container -->
	<div id="page-container" class="fade">
		<!-- BEGIN #top-nav -->
		<div id="top-nav" class="top-nav">
			<!-- BEGIN container -->
			<div class="container">
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i><span class="ml-1">+91 9452736000</span></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="https://www.facebook.com/DigitalLibraryForPCS/"><i class="fab fa-facebook-f f-s-14"></i></a></li>
						<li><a href="https://twitter.com/PCSKAKA1"><i class="fab fa-twitter f-s-14"></i></a></li>
						<li><a href="https://www.instagram.com/pcskakadigital"><i class="fab fa-instagram f-s-14"></i></a></li>
						<li><a href="https://www.linkedin.com/in/pcskaka-pcskaka-a47b11198"><i class="fa fa-linkedin f-s-14"></i></a></li>
						<li><a href="https://www.youtube.com/channel/UCpkf0Q0plfSGu2PtadUTFxw"><i class="fa fa-youtube-play f-s-14"></i></a></li>
					</ul>
				</div>
			</div>
			<!-- END container -->
		</div>
		<!-- END #top-nav -->
		<!-- BEGIN #header -->
		<div id="header" class="header header-inverse">
			<!-- BEGIN container -->
			<div class="container">
				<!-- BEGIN header-container -->
				<div class="header-container">
					<!-- BEGIN navbar-header -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="header-logo">
							<a href="<?php echo base_url();?>">
								<span class=""><img src="<?php echo base_url(); ?>assets/logo/pcslogo.png" style="max-height: 80px !important;max-width: 200px !important;important;" alt="PCSKAKA Logo" ></span>
							</a>
						</div>
					</div>
					<!-- END navbar-header -->
					<!-- BEGIN header-nav -->
					<div class="header-nav">
						<div class=" collapse navbar-collapse" id="navbar-collapse">
							<ul class="nav">
								<li><a href="<?php echo base_url();?>">HOME</a></li>
								<li><a href="<?php echo base_url('site/ias');?>">IAS</a></li>
								<li class="dropdown dropdown-full-width dropdown-hover">
									<a href="#" data-toggle="dropdown">
										PCS 
										<b class="caret"></b>
										<span class="arrow top"></span>
									</a>
									<!-- BEGIN dropdown-menu -->
								    <div class="dropdown-menu p-0">
										<!-- BEGIN dropdown-menu-container -->
										<div class="dropdown-menu-container">
										
											<!-- BEGIN dropdown-menu-content -->
											<div class="dropdown-menu-content">
												
												<div class="row">
													<div class="col-md-3">
														<ul class="dropdown-menu-list">					
														<!--		<li><a href="<?php echo base_url('site/appsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Andhra Pradesh (APPSC)</a></li> -->
															<li><a href="<?php echo base_url('site/bpsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Bihar (BPSC)</a></li>
															<li><a href="<?php echo base_url('site/cgpsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Chhattisgarh (CGPSC)</a></li>
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="dropdown-menu-list">					
															<li><a href="<?php echo base_url('site/gpsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Gujarat (GPSC)</a></li>
																<li><a href="<?php echo base_url('site/ras');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Rajasthan (RAS)</a></li>
															<!--	<li><a href="<?php echo base_url('site/hpsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Haryana (HPSC)</a></li> -->
															<!--	<li><a href="<?php echo base_url('site/hpas');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Himachal Pradesh (HPAS)</a></li> -->
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="dropdown-menu-list">					
															<li><a href="<?php echo base_url('site/jpsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Jharkhand (JPSC)</a></li>
															<li><a href="<?php echo base_url('site/uppsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Uttar Pradesh (UPPSC)</a></li>
															<!--	<li><a href="<?php echo base_url('site/kpsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Karnataka (Karnataka PSC)</a></li> -->
															<!--	<li><a href="<?php echo base_url('site/kerala_psc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Kerala (Kerala PSC)</a></li> -->
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="dropdown-menu-list">					
															<li><a href="<?php echo base_url('site/mppsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Madhya Pradesh (MPPSC)</a></li>
															<li><a href="<?php echo base_url('site/mpsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Maharashtra (MPSC)</a></li>
															<!--	<li><a href="<?php echo base_url('site/opsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Odisha (OPSC)</a></li>  -->
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="dropdown-menu-list">					
															<!--	<li><a href="<?php echo base_url('site/ppsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Punjab (PPSC)</a></li>  -->
														
															<!--	<li><a href="<?php echo base_url('site/tnpsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Tamil Nadu (TNPSC)</a></li>  -->
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="dropdown-menu-list">					
															<!--	<li><a href="<?php echo base_url('site/tspsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Telangana (TSPSC)</a></li> -->
															<!--	<li><a href="<?php echo base_url('site/ukpsc');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>Uttarkhand (UKPSC)</a></li> -->
														
														</ul>
													</div>
													<div class="col-md-3">
														<ul class="dropdown-menu-list">					
														<!--		<li><a href="<?php echo base_url('site/pscwb');?>"><i class="fa fa-fw fa-angle-right text-muted"></i>West Bengal (PSCWB)</a></li> -->
														</ul>
													</div>
												</div>
											
											</div>
											<!-- END dropdown-menu-content -->
										</div>
										<!-- END dropdown-menu-container -->
									</div>
									<!-- END dropdown-menu -->
								</li>
								<li class="dropdown dropdown-hover">
									<a href="#" data-toggle="dropdown">
										SUBJECTS 
										<b class="caret"></b>
										<span class="arrow top"></span>
									</a>
									<div class="dropdown-menu sub-nav">
									
										<a class="dropdown-item" href="<?php echo base_url('site/geography');?>">GEOGRAPHY</a>
										<a class="dropdown-item" href="<?php echo base_url('site/history');?>">HISTORY</a>
										<a class="dropdown-item" href="<?php echo base_url('site/polity');?>">POLITY</a>
										<a class="dropdown-item" href="<?php echo base_url('site/science');?>">SCIENCE</a>
										<a class="dropdown-item" href="<?php echo base_url('site/economics');?>">ECONOMICS</a>
                                        <a class="dropdown-item" href="<?php echo base_url('site/environment');?>">ENVIRONMENT</a>
									</div>
								</li>
								<li><a href="<?php echo base_url('site/currentaffairs');?>">CURRENT AFFAIRS</a></li>
								<li><a href="<?php echo base_url('site/library');?>">LIBRARY</a></li>
								<li><a href="<?php echo base_url('site/testseries');?>">TEST SERIES</a></li>
							</ul>
						</div>
					</div>
					<!-- END header-nav -->
					<!-- BEGIN header-nav -->
					<div class="header-nav">
						<ul class="nav pull-right">
							
							<li>
								<a href="javascript:void(0)" class="studentloginreg">
									<img src="<?php echo base_url(); ?>assets/frontendasset/img/user/logon.png" class="user-img" alt="PCSKAKA Login logo" /> 
									<span class="d-none d-xl-inline">Login / Register</span>
								</a>
							</li>
						</ul>
					</div>
					<!-- END header-nav -->
				</div>
				<!-- END header-container -->
			</div>
			<!-- END container -->
		</div>
		<!-- END #header -->