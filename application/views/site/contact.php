	<!-- BEGIN #page-header -->
		<div id="page-header" class="section-container page-header-container bg-black">
			<!-- BEGIN page-header-cover -->
			<div class="page-header-cover">
				<img src="<?php echo base_url('assets/frontendasset/img/cover/cover-12.jpg');?>" alt="" />
			</div>
			<!-- END page-header-cover -->
			<!-- BEGIN container -->
			<div class="container">
				<h1 class="page-header"><b>Contact</b> Us</h1>
			</div>
			<!-- END container -->
		</div>
		<!-- BEGIN #page-header -->
		
		<!-- BEGIN #product -->
		<div id="product" class="section-container p-t-20">
			<!-- BEGIN container -->
			<div class="container">
				<!-- BEGIN breadcrumb -->
				<ul class="breadcrumb m-b-10 f-s-12">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Contact Us</li>
				</ul>
				<!-- END breadcrumb -->
				<!-- BEGIN row -->
				<div class="row row-space-30">
					<!-- BEGIN col-8 -->
					<div class="col-md-8">
						<h4 class="m-t-0">Contact Form</h4>
						<p class="m-b-30 f-s-13">
						We love to hear from our students. Have any kind of question, please drop us a note and we will call you back within 48 hours. Have any suggestions, let us know and we will discuss it with you. Need any help regarding your preparation for CSE, feel free to write to us and we be will calling you ASAP.
						</p>
						<form class="form-horizontal" name="contact_us_form" id="contactform" action="" method="POST">
						    <div class="form-group row">
								<label class="col-form-label col-md-3 text-lg-center">Name <span class="text-danger">*</span></label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="username" required />
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-md-3 text-lg-center">Email <span class="text-danger">*</span></label>
								<div class="col-md-7">
									<input type="text" class="form-control" name="email" required />
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-md-3 text-lg-center">Subject <span class="text-danger">*</span></label>
								<div class="col-md-7">
									<input type="text" class="form-control" name="subject" required />
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-md-3 text-lg-center">Mobile No.</label>
								<div class="col-md-7">
									<input type="text" class="form-control input_num" name="mobileno"  />
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-md-3 text-lg-center">Message <span class="text-danger">*</span></label>
								<div class="col-md-7">
									<textarea class="form-control" rows="10" name="message" required></textarea>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-md-3"></label>
								<div class="col-md-7">
									<button type="submit" name="contactform" class="btn btn-inverse btn-lg contactform">Send Message</button>
								</div>
							</div>
						</form>
					</div>
					<!-- END col-8 -->
					<!-- BEGIN col-4 -->
					<div class="col-md-4">
						<h4 class="m-t-0">Our Contacts</h4>
						<div class="embed-responsive embed-responsive-16by9 m-b-15">
							<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1779.375038134201!2d80.93022900110618!3d26.879680425885116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399bfd8863856989%3A0x871a191a1704b97d!2sUnion%20Bank%20of%20India!5e0!3m2!1sen!2ssg!4v1572716240338!5m2!1sen!2ssg" allowfullscreen></iframe>
						</div>
						<div></div>
				
						<div><b>Email</b></div>
						<p class="m-b-15">
							<a href="mailto:support@pcskaka.com" class="text-inverse">support@pcskaka.com</a><br />
						</p>
						<div><b>Call</b></div>
							<p class="m-b-15">
							<a class="text-inverse">+91 9452736000</a><br />
						</p>
						
						<div class="m-b-5"><b>Social Network</b></div>
						<p class="m-b-15">
							<a href="#" class="btn btn-icon btn-white btn-circle"><i class="fab fa-facebook"></i></a>
							<a href="#" class="btn btn-icon btn-white btn-circle"><i class="fab fa-twitter"></i></a>
							<a href="#" class="btn btn-icon btn-white btn-circle"><i class="fab fa-google-plus"></i></a>
							<a href="#" class="btn btn-icon btn-white btn-circle"><i class="fab fa-instagram"></i></a>
							<a href="#" class="btn btn-icon btn-white btn-circle"><i class="fab fa-dribbble"></i></a>
						</p>
					</div>
					<!-- END col-4 -->
				</div>
				<!-- END row -->
			</div>
			<!-- END row -->
		</div>
		<!-- END #product -->
