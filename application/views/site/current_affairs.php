<!-- ================== END BASE CSS STYLE ================== -->
<link href="<?php echo base_url(); ?>assets/siteasset/css/current_affairs.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>Current Affairs</b> </h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->

<div id="product" class="section-container p-t-5">
	<!-- BEGIN container -->
	<div class="container">
		
	</div>
	
</div>
<div class="col-md-12">
	<div class="row p-10">
		<div class="col-lg-12 ui-sortable">
			<!-- begin panel -->
			<div class="hide">
				Text area with multiple paragraph
			</div>
			<!-- end panel -->
			<div class="panel">
				<!-- begin panel-heading -->
				<?php if($this->input->get('bk')){ ?>
				<?php if (!empty($results)) { ?>
				<div class="panel-heading brdr_btm ui-sortable-handle m-b-15">
					<?php $bknm="";
						if($this->input->get('bk')){
							$bknm=get_bookdt_by_id($this->input->get('bk'))->BOOK_NAME;
						}
					?>
					<h4 class="panel-title">Current Affairs <?php echo $bknm;?></h4>
				</div>
				<div class="panel-body p-0">
					<?php
							$j =1;
							foreach ($results as $data) {
								$ques=$data->QUES;
					$obj = json_decode($ques); ?>
					<div class="row">
						<div class="col-md-12">
							<div class="card" style="border: 1px solid rgba(0,0,0,.2);">
								<div class="card-block">
									<p class="card-text ques_font"> <?php echo $page+$j.". ".strip_tags($obj->QUESTION);?></p>
									<div class="row">
										<div class="col-md-7">
											<div class="qa_progress" style="width:100%;">
												<div class="qa_progress_text">
													<ul>
														<li class="qa_number">A</li>
														<li class="qa_text" style="display: inline-block">
															<p><?php echo $obj->OPTION[0] ; ?></p>
														</li>
													</ul>
												</div>
											</div>
											<div class="qa_progress" style="width:100%;">
												<div class="qa_progress_text">
													<ul>
														<li class="qa_number">B</li>
														<li class="qa_text" style="display: inline-block">
															<p><?php echo $obj->OPTION[1] ; ?></p>
														</li>
													</ul>
												</div>
											</div>
											<div class="qa_progress" style="width:100%;">
												<div class="qa_progress_text">
													<ul>
														<li class="qa_number">C</li>
														<li class="qa_text" style="display: inline-block">
															<p><?php echo $obj->OPTION[2] ; ?></p>
														</li>
													</ul>
												</div>
											</div>
											<div class="qa_progress" style="width:100%;">
												<div class="qa_progress_text">
													<ul>
														<li class="qa_number">D</li>
														<li class="qa_text" style="display: inline-block">
															<p><?php echo $obj->OPTION[3] ; ?></p>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<a class="text-success show-ans"   href="javascript:void(0);" role="button" attr-id="<?php echo $data->QUES_ID; ?>" attr-ans="<?php echo $data->QUES_ANSWERS; ?>">Show Answer</a>
									<div class="collapse opt<?php echo $data->QUES_ID; ?>" id="collapseExample">
										<?php if ($data->QUES_ANSWERS == 1) {
										echo "<span style='font-weight:bold;'>Correct Answer :</span> A [".$obj->OPTION[0].']';
										} elseif ($data->QUES_ANSWERS == 2) {
										echo "<span style='font-weight:bold;'>Correct Answer :</span> B [".$obj->OPTION[1].']';
										} elseif ($data->QUES_ANSWERS == 3) {
										echo "<span style='font-weight:bold;'>Correct Answer :</span> C [".$obj->OPTION[2].']';
										} else {
										echo "<span style='font-weight:bold;'>Correct Answer :</span> D [".$obj->OPTION[3].']';
										} ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php      $j++;    } ?>
					<div class="pagination">
						<p><?php echo $links; ?></p>
					</div>
				</div>
				<?php } ?>
				<?php } ?>
			</div>
		</div>
		
	</div>
	<div class="panel panel-inverse m-b-0">
		<div class="panel-body">
			<div class="row">
				<?php $currentaffairs=get_all_currentaffairs_activebooks();
					if(!empty($currentaffairs)){
						foreach ($currentaffairs as $bk) {
						?>
						<div class="col-md-2 col-6">
							<a href="<?php echo base_url();?>site/currentaffairs?bk=<?php echo $bk->BOOK_ID;?>">
								<div class="alert alert-primary fade show text-center">
									<?php 
										$data=explode(" ",$bk->BOOK_NAME);
										echo "<h4>".ucfirst(strtolower($data[0]))."</h4>".$data[1];
									?>
								</div>
							</a>
						</div>
						<?php	
						}
					}
				?>	
			</div>
		</div>
	</div>
</div>
<!-- =========begin ads div======= -->
<div class="col-md-3">

</div>
<!-- =========end ads div======== -->
<body>
<script>
	$(document).ready(function(){
		$('.opt').hide();
		$('a.show-ans').click(function (e) {
			var id=$(this).attr('attr-id');
			$('div.opt'+id).toggle();
			if ($(this).text() == "Show Answer") {
				$(this).text('Hide Answer')
			}
			else {
				$(this).text('Show Answer')
			}
		});
	});
</script>