<link href="<?php echo base_url(); ?>assets/siteasset/css/testseries.css" rel="stylesheet" />
<!-- BEGIN #page-header -->
<div id="page-header" class="section-container page-header-container bg-black">
	<!-- BEGIN page-header-cover -->
	<div class="page-header-cover">
		<img src="<?php echo base_url('assets/frontendasset/img/cover/slider.jpg');?>" alt="" />
	</div>
	<!-- END page-header-cover -->
	<!-- BEGIN container -->
	<div class="container">
		<h1 class="page-header"><b>Test Series</b> </h1>
	</div>
	<!-- END container -->
</div>
<!-- BEGIN #page-header -->

<div class="test-series">
	<div class="container-fluid" style="margin-top:20px;">
		<div class="row">
			<!-- begin col-3 -->
			<?php if(!empty($tsdt)){ foreach($tsdt as $tsdts){ if(strtotime($tsdts->TS_END_DATE)>strtotime(date('Y-m-d H:i:s'))){ ?>
			<div class="col-lg-3 col-md-6">
				<div class="widget widget-stats bg-gradient-teal <?php if($tsdts->TS_STATUS==0){ echo "cursor_disabled"; } ?>" style="min-height: 267px;">
					<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
						<div class="stats-content">
							<a class="media-right" href="javascript:;" style="display: inline-table;">
								<img src="<?php echo base_url(); ?>assets/themeassets/img/user/upsc-icon-10.jpg" alt="" style="height:60px;width: 65px;" class="media-object rounded-corner">
							</a>
							<?php if($tsdts->TS_STATUS==0){ echo '<span class="blink_me coming_soon"  style="color:red;font-weight:700;font-size:17px;">Coming Soon!</span>'; } ?>
							<div class="stats-number" style="height: 95px;"><div><?php echo $tsdts->TS_NAME; ?></div>  	</div>
							<div class="stats-progress progress m-t-15">
								<div class="progress-bar" style="width: 70.1%;"></div>
							</div>
							<div>
								<a href="<?php if($tsdts->TS_STATUS==1){ echo base_url().'site/testpaper/'.url_encode($tsdts->TS_ID); }else{ echo "javascript:void(0);"; } ?>" class="btn btn-success btn-block m-t-20 f-s-16 <?php if($tsdts->TS_STATUS==0){ echo "cursor_disabled"; } ?>">View</a>
							</div>
						</div>
				</div>
			</div>
			<!-- end col-3 -->
			<?php } } } ?>			
		</div>				
	</div>
</div>
		