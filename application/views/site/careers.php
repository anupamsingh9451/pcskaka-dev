	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="<?php echo base_url('assets/frontendasset/img/cover/cover-15.jpg');?>" alt="" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header"><b>Careers</b></h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->
	
	<div class="page-section cs-page-sec-910813 ">
		    	    	<!-- Container Start -->
	    	<div class="container "> 
						    <div class="row">
				<div class="section-fullwidth col-lg-12 col-md-12 col-sm-12 col-xs-12 "><div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="cs-heading"><h1 style="color: !important; font-size: 28px !important; font-weight:  !important; letter-spacing: 1px !important; line-height: 34px !important; text-align:Center; font-style:normal;">WELCOME ! HERE IS WHAT WE ARE </h1>
				<div class="heading-description" style="color: !important; text-align: Center; font-style:normal;">Every single one of our jobs has some kind of flexibility option - such as telecommuting, a part-time schedule or a flexible or flextime schedule.</div></div></div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				        <div class="cs-simple fancy-box"><div class="row"><div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				        <div class="cs-icon-box left"><div class="cs-media"><figure><a href="#"><i class="icon-basket2" style="color:#666a97 !important;"></i></a></figure></div>
				        <div class="cs-text"><h4><a href="#">Work From Home</a></h4><p>Looking for highly motivated and energetic individuals who can take up the responsibilty and deliver work while working from home. You tell us your quality and we will assign you the work accordingly.</p> </div></div></div>
				        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"><div class="cs-icon-box left"><div class="cs-media"><figure><a href="#"><i class="icon-map2" style="color:#666a97 !important;"></i></a></figure></div>
				        <div class="cs-text"><h4><a href="#">English Data Entry</a></h4><p>We have a lot of quality content but due lesser bandwidth we are unable to put that up on website. We need people who can work with our content team and help them in typing content.</p> </div></div></div>
				        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"><div class="cs-icon-box left"><div class="cs-media"><figure><a href="#"><i class="icon-lightbulb" style="color:#666a97 !important;"></i></a></figure></div>
				        <div class="cs-text"><h4><a href="#">Hindi Content Writer</a></h4><p>We receive thousands of request from aspirants who are from Hindi Background. We are looking for individuals who can translate English into Hindi and vice versa. Hindi typing is added advantage. </p> </div></div></div>
				        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"><div class="cs-icon-box left"><div class="cs-media"><figure><a href="#"><i class="icon-streetsign" style="color:#666a97 !important;"></i></a></figure></div>
				    
				        </div></div></div></div></div></div></div></div></div>
			    	    	</div>
