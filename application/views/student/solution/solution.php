<link href="<?php echo base_url(); ?>assets/studentassets/css/solution/solution.css" rel="stylesheet" />
<div class="main_page_div">
<div class="col-md-12 border-bottom">
    <div class="m-15 m-l-20">
        <h4>
            <div class="time hide">
                  Time Left <span id="defaultCountdown"><?php echo "sefdfd"; ?></span>
            </div>
        </h4>
    </div>
</div>
<div class="container-fluid main_div" style="max-height: 579px;overflow: auto;">
        <input type="hidden" value="<?php echo sizeof($allques); ?>" class="total_no_ques">
<?php $i=1; if (!empty($result)) {
    if (!empty(json_decode($result[0]->STD_RESULT))) {
        $res = json_decode($result[0]->STD_RESULT);
        foreach ($res as $ress) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row questions <?php if ($i!=1) {
            echo "hidez";
        } else {
            "";
        } ?>" attr-gqwts="<?php echo url_encode($tpdt[0]->TP_ID); ?>" attr-gzwtp="<?php echo url_encode($tpdt[0]->TS_ID); ?>" attr-zwdqi="<?php echo $ress->quesid; ?>" attr-srno="<?php echo $i; ?>" >
                <div class="col-md-12 border-bottom">
                    <div class="m-15 m-l-20">
                        <h4>Question-<?php echo $i; ?></h4>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="p-10 border-bottom">
                        <div class="m-b-15">
                            <div class="ques">
                                <p class=""><?php $ques = $this->Studentgetmodel->get_quesdt_by_id(url_decode($ress->quesid)); $quesdt =  json_decode($ques[0]->QUES); echo $quesdt->QUESTION; ?></p>
                            </div>
                            <div class="options p-l-15">
                                <?php $j = 1; foreach ($quesdt->OPTION as $options) { ?>
                                	<div class="myclass <?php if ($ques[0]->QUES_ANSWERS==$j) {
            echo "class_true";
        } else {
            if ($ress->ques_ans==$j) {
                echo "wrong_ans";
            }
        } ?>" >
                                    	<div class="form-check">
                                    		<label class="form-check-label" for="<?php echo url_encode($ques[0]->QUES_ID); ?>_option_<?php echo $j; ?>"><?php echo $options; ?></label>
                                    	</div>
                                    </div>
                            	<?php $j++; } ?>
                            </div>
                           
                        </div>
                	</div>
                	<div class="col-md-12">
                        <div class="m-t-15">
                            <h4>Result</h4>
                        </div>
                        <div class="desc">
                            <p class="fa-2x"><?php if ($ress->ques_ans!=0) {
            if ($ress->ques_ans==$ques[0]->QUES_ANSWERS) {
                echo '<i class="fas fa-check clr-green"></i><span class="m-l-10 clr-green">True</span>';
            } else {
                echo '<i class="fas fa-times clr-red"></i><span class="m-l-10 clr-red">False</span>';
            }
        } else {
            echo "<span style='font-size: 16px;'>Not Attempted</span>";
        } ?></p>
                        </div>
                    </div>
                	 <div class="col-md-12">
                        <div class="m-t-15">
                            <h4>Description</h4>
                        </div>
                        <div class="desc">
                            <p class=""><?php echo $ques[0]->QUES_EXPLANATION; ?></p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
<?php $i++; }
    }
} ?>

		<!-- begin theme-panel -->
		<div class="theme-panel theme-panel-lg">
			<a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
			<div class="theme-panel-content">
			    <!--<div class="profile_details">-->
			    <!--    <div class="profiles">-->
			    <!--        N-->
			    <!--    </div>-->
			    <!--</div>-->
			    <div class="divider"></div>
				<ul class="theme-list clearfix">
					<li class="solved-icon"><span class="label attempted attemptedtotal"><?php echo $tpresult['attempted']; ?></span>&nbsp;Answered&nbsp;&nbsp;</li>
					<li class="solved-icon"><span class="label bookmarked bookmarkedtotal"><?php echo $tpresult['bookmark_both']; ?></span>&nbsp;Marked&nbsp;&nbsp;</li>
					<li class="solved-icon"><span class="label no_visitedtotal"><?php echo $tpresult['not_visited']; ?></span>&nbsp;Not Visited&nbsp;&nbsp;</li>
					<li class="solved-icon"><span class="label attempted bookmarked attemptedmarktotal"><?php echo $tpresult['attempt_boomark']; ?></span>&nbsp;Mark and answered&nbsp;&nbsp;</li>
					<li class="solved-icon"><span class="label skiptotal skipped"><?php echo $tpresult['skipped']; ?></span>&nbsp;Not Answered&nbsp;&nbsp;</li>
				</ul>
				<div class="divider"></div>
				<div class="side-div">
				    <h4>QUESTIONS</h4>
				</div>
				
				<div class="row m-t-10 queslistclass">
    				<ul class="theme-list clearfix ques_no_div">
    				    <?php $j=1; if (!empty($result)) {
    if (!empty(json_decode($result[0]->STD_RESULT))) {
        $res = json_decode($result[0]->STD_RESULT);
        foreach ($res as $ress) { ?>
    					    <li class="ques-icon-li" attr-gqwts="<?php echo url_encode($tpdt[0]->TP_ID); ?>" attr-gzwtp="<?php echo url_encode($tpdt[0]->TS_ID); ?>" attr-zwdqi="<?php echo $ress->quesid; ?>" attr-srno="<?php echo $j; ?>">
    					        <span class="ques-no-label ques-icons  <?php if ($j==1) {
            echo "view activeque ";
        } else {
            "";
        } echo $ress->quesclass; ?>"><?php echo $j; ?></span>
    					   </li>
    					<?php $j++;  }
    }
} ?>
    				</ul>
				</div>
				<!--<div class="divider"></div>-->
				<!--<div class="row">-->
				<!--	<div class="col-md-12">-->
				<!--		<a href="javascript:void(0);" class="btn btn-primary btn-block btn-rounded submit_modal"><b>Submit Test</b></a>-->
				<!--	</div>-->
				<!--</div>-->
			</div>
		</div>
		<!-- end theme-panel -->
		
	
</div>
<div class="container-fluid footer_div">
    <div class="row">
        <div class="col-md-8">
            <div class="mrgncust">
                <button type="button" class="btn btn-success mark_for_review_and_next">previous</button>
                
                <button type="button" class="btn btn-danger save_and_next">next</button>
                <a href="<?php echo base_url(); ?>student/dashboard" class="btn btn-primary"><b>Go to Dashboard</b></a>
            </div>
        </div>
        
        
    </div>
</div>
</div>     		

	
		        