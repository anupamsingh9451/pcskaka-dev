
        
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
		<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/js-cookie/js.cookie.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/theme/default.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/apps.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/studentassets/js/studentget.js"></script>
	<script src="<?php echo base_url(); ?>assets/studentassets/js/studentpost.js"></script>
	<!--<script src="<?php echo base_url(); ?>assets/studentassets/js/studentcommon.js"></script>-->
	<script src="<?php echo base_url(); ?>assets/studentassets/js/studentvalidation.js"></script>
	


	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>
</body>

</html>
