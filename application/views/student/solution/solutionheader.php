<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>PCSKAKA STUDENT</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link rel="icon" href="<?php echo base_url(); ?>assets/logo/logo.jpeg">
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/font-awesome/css/all.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/css/default/style.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/css/default/style-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/css/default/theme/default.css" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/pace/pace.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery/jquery-3.3.1.min.js"></script>

		<!-- ================== END BASE JS ================== -->
	<script>
		var api_loc="http://localhost:3000/";
		var base_loc="<?php echo base_url(); ?>";
		var loginid="<?php echo $this->session->userdata('studentloginid'); ?>";
        var token="<?php echo $this->session->userdata('studenttoken'); ?>";
        var type="<?php echo $this->session->userdata('studenttype'); ?>";
		var clientserv="Ratan";
        var apikey="pcskaka";
	</script>
<style>
    body {
        background: #f6f8f9;
    }
    
    .header{
        box-shadow: 0 4px 4px rgba(0,0,0,.1) !important;
    }
    .media.media-sm .media-object {
        width: 50px !important;
    }
</style>
</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-without-sidebar page-header-fixed">
		<!-- begin #header -->
		<div id="header" class="header navbar-default" style="">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="<?php echo base_url(); ?>student/dashboard" class="navbar-brand"><span class="navbar-logo"></span> <b>PCSKAKA</b> <?php echo $tpdt[0]->TP_NAME; ?></a>
			</div>
			<!-- end navbar-header -->
			<!-- begin header-nav -->
			
			<!-- end header navigation right -->
		</div>
		<!-- end #header -->