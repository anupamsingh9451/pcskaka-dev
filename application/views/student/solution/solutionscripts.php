<script>
$(document).ready(function(){
    $('.theme-panel').addClass('active');
    
    
    var windowHeight = $(window).height();
    var headerHeight = $('#header').height();
    var instruction_footer_height = $('.footer_div').height();
    $('.main_page_div').css("max-height", windowHeight-headerHeight-instruction_footer_height-32);
    $('.main_page_div').css("min-height", windowHeight-headerHeight-instruction_footer_height-32);
    $('.main_page_div').css("overflow", "auto");
    
    
	$('body').on('click','.ques-icon-li',function(){
	   var zwdqi=$(this).attr('attr-zwdqi');
	   var srno=$(this).attr('attr-srno');
	   var prevqueid=$('.ques-icon-li').find('.activeque').parent().attr('attr-zwdqi');
	   if(prevqueid!=zwdqi){
    	   $('.ques-icon-li[attr-zwdqi="'+prevqueid+'"]').find('.ques-icons').removeClass('activeque');
    	   $(this).find('.ques-icons').addClass('activeque');
	   }
	   $('.questions').removeClass('showz');
	   $('.questions').addClass('hidez');
	   $('.questions[attr-zwdqi="'+zwdqi+'"]').addClass('showz');
	   $('.ques-icon-li').find('.ques-icons').removeClass('view');
	   $(this).find('.ques-icons').addClass('view');
	   
	});
	
	$('body').on('click','.mark_for_review_and_next',function(){
	    var activequesid = $('.ques-icon-li').find('.activeque').parent().attr('attr-zwdqi');
	    var srno = parseInt($('.ques-icon-li').find('.activeque').parent().attr('attr-srno'));
    	srno = parseInt(srno-1);
	    $('.ques-icon-li[attr-srno="'+srno+'"]').trigger('click');
	});
	
	$('body').on('click','.save_and_next',function(){
	    var activequesid = $('.ques-icon-li').find('.activeque').parent().attr('attr-zwdqi');
	    var srno = parseInt($('.ques-icon-li').find('.activeque').parent().attr('attr-srno'));
    	srno = parseInt(srno+1);
    	
    	
	    $('.ques-icon-li[attr-srno="'+srno+'"]').trigger('click');
	});

	
	    
        
	
});

</script>