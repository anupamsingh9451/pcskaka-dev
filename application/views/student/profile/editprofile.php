<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">User</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Edit</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Edit Profile</h1>
	<!-- end page-header -->	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class='col-md-6'>
			<!-- begin panel -->
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Edit</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<form id="editprofile">
					    <div class="col-md-12">
					    	<?php
                            foreach ($editdt as $row) {
                                ?>
					    	<input type="hidden" name="studentid" value="<?php echo $row->USER_ID; ?>">
					        <div class="row">
					            
                                <div class="col-md-6">
        							<label class="col-form-label">User First Name </label>
        							<input type="text" class="form-control fname" name="fname" placeholder="Enter First Name" value="<?php echo $row->USER_FNAME; ?>">
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">User Last Name</label>
        							<input type="text" class="form-control lname" name="lname" placeholder="Enter Last Name" value="<?php echo $row->USER_LNAME; ?>">
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">User Father Name </label>
        							<input type="text" class="form-control father" name="father" placeholder="Enter Father Name"value="<?php echo $row->USER_FATHER; ?>" >
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">User Address </label>
        							<input type="text" class="form-control add" name="add" placeholder="Enter Address" value="<?php echo $row->USER_ADDRESS; ?>">
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">User Contact </label>
        							<input type="text" class="form-control contact input_num" name="contact" placeholder="Enter Contact" value="<?php echo $row->USER_CONTACT; ?>">
        						</div>
        						
        						<div class="col-md-6">
        							<label class="col-form-label">Qualification </label>
        							<input type="text" class="form-control quali " name="quali" placeholder="Enter Qualification" value="<?php echo $row->USER_QUALIFICATION; ?>" >
        						</div>        					    
					        </div>
					    <?php
                            }?>
					    </div>
						<div class="form-group row m-b-10 m-t-10">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary edit_profile" name="edit_profile">SAVE</button>
							</div>
						</div>
					</form>
				</div>
			
			</div>
			
		</div>

		
	</div>
</div>

