<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">User</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Edit</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Change Password</h1>
	<!-- end page-header -->	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class='col-md-6'>
			<!-- begin panel -->
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Change Password</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<form id="changepwd">
					    <div class="col-md-12">
					    	
					    	<input type="hidden" name="studentid" value="<?php echo $userid; ?>" >
					        <div class="row">
					            
							<div class="col-md-12">
        							<label class="col-form-label">Old Password</label>
        							<input data-toggle="password" data-placement="after" class="form-control oldpwd" name="oldpwd" type="password" placeholder="Enter old Password" required/>
        							
        					</div>
                                <div class="col-md-12">
        							<label class="col-form-label">New Password</label>
        							<input data-toggle="password" data-placement="after" class="form-control newpwd" name="newpwd" type="password" placeholder="Re-enter Password"  required />
        						</div>
        						<div class="col-md-12">
        							<label class="col-form-label">Confirm Password</label>
        							<input data-toggle="password" data-placement="after" class="form-control cnfrmpwd" name="cnfrmpwd" type="password" placeholder="Re-enter Password"  required  />
        						</div>
					        </div>
					    
					    </div>
						<div class="form-group row m-b-10 m-t-10">
							<div class="col-md-12 m-l-15">
								<button type="submit" class="btn btn-primary changepwd" name="changepwd">SAVE</button>
							</div>
						</div>
					</form>
				</div>
			
			</div>
			
		</div>

		
	</div>
</div>
<script>
	$(document).ready(function() {
    	FormPlugins.init();
    });
    function randomPassword() {
		var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$_+";
		var size = 10;
		var i = 1;
		var ret = ""
		while ( i <= size ) {
			$max = chars.length-1;
			$num = Math.floor(Math.random()*$max);
			$temp = chars.substr($num, 1);
			ret += $temp;
			i++;
		}
		return ret;
	}
</script>
