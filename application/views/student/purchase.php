<?php
function getCallbackUrlsuccess()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/successpayment';
}
function getCallbackUrlfailure()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/failurepayment';
}
?>
<link href="<?php echo base_url(); ?>assets/studentassets/css/purchase/purchase.css" rel="stylesheet" />
<!-- begin #content -->
<div id="content" class="content  hidden-print">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right buyallcartproduct">
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Dashboard </h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		
		<!-- begin col-9 -->
		<div class="col-xl-12">
			<!-- begin panel -->
			<div class="panel panel-default panel-with-tabs" data-sortable-id="ui-unlimited-tabs-2">
				<!-- begin panel-heading -->
				<div class="panel-heading p-0">
					<!-- begin nav-tabs -->
					<div class="tab-overflow">
						<ul class="nav nav-tabs">
							<li class="nav-item prev-button"><a href="javascript:;" data-click="prev-tab" class="nav-link text-primary"><i class="fa fa-arrow-left"></i></a></li>
							<li class="nav-item"><a href="#nav-tab-1" data-toggle="tab" class="nav-link active">Test Series</a></li>
							<li class="nav-item"><a href="#nav-tab-2" data-toggle="tab" class="nav-link">Library</a></li>
							<li class="nav-item next-button"><a href="javascript:;" data-click="next-tab" class="nav-link text-primary"><i class="fa fa-arrow-right"></i></a></li>
						</ul>
					</div>
				</div>
				<!-- end panel-heading -->
				<!-- begin tab-content -->
				<div class="panel-body tab-content">
					<!-- begin tab-pane -->
					<div class="tab-pane fade active show" id="nav-tab-1">
						<h3 class="m-t-10">Test Series</h3>
						<div class="row">
							<?php if (!empty($testseriesdt)) {
    $i=1;
    foreach ($testseriesdt as $testseriesdts) {
        $checkpayment = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 4, $testseriesdts->TS_ID);
        if (strtotime($testseriesdts->TS_START_DATE)<strtotime(date('Y-m-d H:i:s')) && strtotime($testseriesdts->TS_END_DATE)>strtotime(date('Y-m-d H:i:s'))) { ?>
							<div class="col-lg-4 col-md-6 col-xs-12">
								<div class="widget widget-stats bg-gradient-teal <?php if ($testseriesdts->TS_STATUS==0) {
            echo "cursor_disabled";
        } ?>">
									<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
									<div class="stats-content">
										<a class="media-right" href="javascript:;" style="display: inline-table;">
											<img src="<?php echo base_url(); ?>assets/themeassets/img/user/upsc-icon-10.jpg" alt="PCSKAKA-<?php echo $testseriesdts->TS_NAME; ?> LOGO" style="height:60px;width: 65px;" class="media-object rounded-corner">
										</a>
										<?php if ($testseriesdts->TS_STATUS==0) {
            echo '<span class="blink_me coming_soon"  style="color:red;font-weight:900;">Coming Soon!</span>';
        } ?>
										<div class="stats-number"><div><?php echo $testseriesdts->TS_NAME; ?> </div> </div>
										<div class="stats-progress progress m-t-15 ">
											<div class="progress-bar" style="width: 70.1%;"></div>
										</div>
										<?php $discount = $this->Studentgetmodel->get_discount_by_prod_type(4, 2, $testseriesdts->TS_ID);
                                            $total=$discamt=0;
                                            if (!empty($discount)) {
                                                if ($discount[0]->COUPON_MODE==1) {
                                                    $discamt = $discount[0]->COUPON_AMOUNT;
                                                    $discstr = "₹";
                                                } else {
                                                    $discamt = ($testseriesdts->TS_COST*$discount[0]->COUPON_AMOUNT)/100;
                                                    $discstr = "%";
                                                }
                                            }
                                            $totalamt=$testseriesdts->TS_COST-$discamt;
                                        ?>
										<div class="stats-desc f-w-500 f-s-12 m-t-16 m-b-15"><?php $arr = json_decode($testseriesdts->TS_TEST_PAPERS); if (!empty($arr)) {
                                            echo sizeof($arr);
                                        } else {
                                            echo '0';
                                        } ?> Total Test 
											<?php $check = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 4, $testseriesdts->TS_ID); if (empty($check)) { ?>
													<?php if (!empty($discount)) { ?>
														<span class="badge badge-green pull-right m-r-2" style="text-transform: unset;"><?php echo $discount[0]->COUPON_AMOUNT." ".$discstr.' off'; ?> </span>
													<?php } ?>
													<span class="badge badge-info f-s-15 pull-right m-r-2" > Price : <?php if (!empty($discount)) {
                                            $totalamt = $testseriesdts->TS_COST-$discamt;
                                            echo '₹ '.$totalamt.'<span class="disc-through-amt m-l-6">'.$testseriesdts->TS_COST.'</span>';
                                        } else {
                                            echo $testseriesdts->TS_COST;
                                        }  ?></span>
											<?php } else { ?>   
													<span class="badge badge-green pull-right" translate="">Purchased </span>
													<?php if (!empty($discount)) { ?>
														<span class="badge badge-green pull-right m-r-2" style="text-transform: unset;"><?php echo $discount[0]->COUPON_AMOUNT." ".$discstr.' off'; ?></span>
													<?php } ?>
													<span class="badge badge-info f-s-15  m-r-2" > Price : <?php if (!empty($discount)) {
                                            $totalamt = $testseriesdts->TS_COST-$discamt;
                                            echo '₹ '.$totalamt.'<span class="disc-through-amt m-l-6">'.$testseriesdts->TS_COST.'</span>';
                                        } else {
                                            echo $testseriesdts->TS_COST;
                                        }  ?></span>
											<?php }?>
										</div>
									</div>
									<div style="width: 94%;position: absolute;bottom: 22px;">
										<input type="hidden" id="surl" name="surl" value="<?php echo getCallbackUrlsuccess(); ?>" />
										<input type="hidden" id="furl" name="furl" value="<?php echo getCallbackUrlfailure(); ?>" />
										<?php $check = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 4, $testseriesdts->TS_ID); if (empty($check)) { ?>
											<a href="javascript:void(0);"  class="btn btn-primary btn-block m-t-20 f-s-16 <?php if ($testseriesdts->TS_STATUS==1) {
                                            echo "addtocart";
                                        } else {
                                            echo "cursor_disabled";
                                        }  ?>" attr-prodid="<?php echo $testseriesdts->TS_ID; ?>" attr-prodtype="4" attr-prodcost="<?php echo $testseriesdts->TS_COST; ?>"  attr-prodname="<?php echo $testseriesdts->TS_NAME; ?>"  attr-discountamt="<?php echo round($discamt, 2);?>"  attr-totalamt="<?php echo round($totalamt, 2);;?>"><i class="fa fa-plus"></i> ADD TO CART</a>
										<?php } ?>
										<a href="<?php if ($testseriesdts->TS_STATUS==1) {
                                            echo base_url().'student/testpaper/'.$testseriesdts->TS_ID;
                                        } else {
                                            echo "javascript:void(0);";
                                        } ?>" class="btn btn-success btn-block m-t-6 f-s-16 <?php if ($testseriesdts->TS_STATUS==0) {
                                            echo "cursor_disabled";
                                        } ?>" >Go to Test Series</a>
									</div>
								</div>
							</div>
							<?php }
        $i++;
    }
} ?>
						</div>	
					</div>
					<!-- end tab-pane -->
					<!-- begin tab-pane -->
					<div class="tab-pane fade" id="nav-tab-2">
						<h3 class="m-t-10">Library</h3>
						<div class="row">
							<?php if (!empty($library['data'])) {
    $i = 1;
    foreach ($library['data'] as $librarys) {
        $checkpayment = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 1, $librarys->LIB_ID); ?>
							<div class="col-lg-4 col-md-6 col-xs-12">
								<div class="widget widget-stats bg-gradient-teal <?php if ($librarys->LIB_STATUS==0) {
            echo "cursor_disabled";
        } ?>">
									
									<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
									<div class="stats-content">
										<a class="media-right" href="javascript:;" style="display: inline-table;">
											<img src="<?php echo base_url(); ?>assets/themeassets/img/user/library-logo.jpg" alt="PCSKAKA-<?php echo $librarys->LIB_NAME; ?> LOGO" style="height:60px;width: 65px;" class="media-object rounded-corner">
										</a>
										<?php if ($librarys->LIB_STATUS==0) {
            echo '<span class="blink_me coming_soon"  style="color:red;font-weight:900;">Coming Soon!</span>';
        } ?>
										<div class="stats-number"><div><?php echo $librarys->LIB_NAME; ?> </div> </div>
										<div class="stats-progress progress m-t-15 ">
											<div class="progress-bar" style="width: 70.1%;"></div>
										</div>
										<div class="stats-desc f-w-500 f-s-12 m-t-16">
											<?php $discount = $this->Studentgetmodel->get_discount_by_prod_type(1, 2, $librarys->LIB_ID);
        $totalamt=$discamt=0;
        if (!empty($discount)) {
            if ($discount[0]->COUPON_MODE==1) {
                $discamt = $discount[0]->COUPON_AMOUNT;
                $discstr = "₹";
            } else {
                $discamt = ($testseriesdts->TS_COST*$discount[0]->COUPON_AMOUNT)/100;
                $discstr = "%";
            }
        } ?>
											<?php if (empty($checkpayment)) { ?>
												<?php if (!empty($discount)) { ?>
													<span class="badge badge-green pull-right m-r-2" style="text-transform: unset;"><?php echo $discount[0]->COUPON_AMOUNT." ".$discstr.' off'; ?> </span>
												<?php } ?>
												<span class="badge badge-info f-s-15 pull-right m-r-2" > Price : <?php if (!empty($discount)) {
            $totalamt = $librarys->LIB_COST-$discamt;
            echo '₹ '.$totalamt.'<span class="disc-through-amt m-l-6">'.$librarys->LIB_COST.'</span>';
        } else {
            echo $librarys->LIB_COST;
        }  ?></span>
											<?php } else { ?>
												<span class="badge badge-green pull-right" translate="">Purchased </span>
												<?php if (!empty($discount)) { ?>
													<span class="badge badge-green pull-right m-r-2" style="text-transform: unset;"><?php echo $discount[0]->COUPON_AMOUNT." ".$discstr.' off'; ?></span>
												<?php } ?>
												<span class="badge badge-info f-s-15 pull-right m-r-2" > Price : <?php if (!empty($discount)) {
            $totalamt = $librarys->LIB_COST-$discamt;
            echo '₹ '.$totalamt.'<span class="disc-through-amt m-l-6">'.$librarys->LIB_COST.'</span>';
        } else {
            echo $librarys->LIB_COST;
        }  ?></span>
											<?php } ?>
										</div>
									</div>
									<div style="margin-top: 48px !important;">
										<input type="hidden" id="surl" name="surl" value="<?php echo getCallbackUrlsuccess(); ?>" />
										<input type="hidden" id="udf1" name="udf1" value="1" />
										<input type="hidden" id="furl" name="furl" value="<?php echo getCallbackUrlfailure(); ?>" />
										<?php if (empty($checkpayment)) { ?>
											<!-- <a href="javascript:void(0);" class="btn btn-primary btn-block f-s-16 <?php if ($librarys->LIB_STATUS==1) {
            echo "payumoneylib";
        } else {
            echo "cursor_disabled";
        } ?>" attr-libid="<?php echo $librarys->LIB_ID; ?>" style="color: #000 !important;background: #e8f1f1 !important;border-color: #8c8c8c !important;"><i class="fa fa-lock"></i> BUY NOW!</a> -->
											<a href="javascript:void(0);" class="btn btn-primary btn-block f-s-16 <?php if ($librarys->LIB_STATUS==1) {
            echo "addtocart";
        } else {
            echo "cursor_disabled";
        } ?>" attr-prodid="<?php echo $librarys->LIB_ID; ?>" attr-prodtype="1" attr-prodcost="<?php echo $librarys->LIB_COST; ?>"  attr-prodname="<?php echo $librarys->LIB_NAME; ?>"   attr-discountamt="<?php echo $discamt;?>" attr-totalamt="<?php echo $totalamt;?>"><i class="fa fa-plus"></i> ADD TO CART</a>
										<?php } ?>
										<a href="<?php if ($librarys->LIB_STATUS==1) {
            echo base_url().'student/subject/'.$librarys->LIB_ID;
        } else {
            echo "javascript:void(0);";
        } ?>" class="btn btn-success btn-block m-t-6 f-s-16 <?php if ($librarys->LIB_STATUS==0) {
            echo "cursor_disabled";
        } ?>" style="<?php if (!empty($checkpayment)) {
            echo "margin-top:82px;";
        } ?>" >Go to Subject</a>
									</div>
								</div>
							</div>
							<?php $i++;
    }
} ?>
						</div>	
					</div>
					<!-- end tab-pane -->
				</div>
				<!-- end tab-content -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-9 -->
	</div>
	<!-- end row -->
</div>
<!-- end #content -->
<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
<link href="<?php echo base_url(); ?>assets/themeassets/css/default/invoice-print.min.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
<!-- #modal-without-animation -->
<div class="modal " id="modal-prucahseproduct">
	<div class="modal-dialog  modal-xl">
		<div class="modal-content">
			<div class="modal-header hidden-print">
				<h4 class="modal-title">Review Your Order</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body ">
				<!-- begin invoice -->
				<div class="invoice">
					<!-- begin invoice-company -->
					<div class="invoice-company">
						<span class="pull-right hidden-print">
							<a href="javascript:;" class="btn btn-sm btn-white m-b-10"><i class="fa fa-file-pdf t-plus-1 text-danger fa-fw fa-lg"></i> Export as PDF</a>
							<a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-white m-b-10"><i class="fa fa-print t-plus-1 fa-fw fa-lg"></i> Print</a>
						</span>
						PCSKAKA
					</div>
					<!-- end invoice-company -->
					<!-- begin invoice-header -->
					<div class="invoice-header">
						<div class="invoice-from">
							<small>from</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse">PCSKAKA, Inc.</strong><br />
								Street Address<br />
								City, Zip Code<br />
								Phone: +91 945-273-6000<br />
								Email: support@pcskaka.com
							</address>
						</div>
						<div class="invoice-to">
							<small>to</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse"><span class="name"></span></strong><br />
								Street Address<br />
								City, Zip Code<br />
								Phone: <span class="student_mobileno"></span><br />
								Email: <span class="student_name_upper"></span>
							</address>
						</div>
						<div class="invoice-date">
							<small>Invoice / <?php echo date('F');?> period</small>
							<div class="date text-inverse m-t-5"><?php echo date('d-F-Y'); ?></div>
							<div class="invoice-detail">
								#0000123DSS<br />
								Services Product
							</div>
						</div>
					</div>
					<!-- end invoice-header -->
					<!-- begin invoice-content -->
					<div class="invoice-content">
						<!-- begin table-responsive -->
						<div class="table-responsive ">
							<table class="table table-invoice buycarttable">
								<thead>
									<tr>
										<th class="text-nowrap"> PRODUCT DESCRIPTION</th>
										<th class="text-right" width="5%">PRICE</th>
										<th class="text-right" width="5%">DISCOUNT</th>
										<th class="text-right" width="5%">TOTAL</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot></tfoot>
							</table>
						</div>
						<!-- end table-responsive -->
						<!-- begin invoice-price -->
						<div class="invoice-price">
							<div class="invoice-price-left bg-white">
								<div class="invoice-price-row hide">
									<div class="sub-price">
										<small>SUBTOTAL</small>
										<span class="text-inverse subtotal"></span>
									</div>
								</div>
							</div>
							<div class="invoice-price-right totalpay">
								<small>TOTAL</small> <span class="f-w-600"></span>
							</div>
						</div>
						<!-- end invoice-price -->
					</div>
					<!-- end invoice-content -->
					<div class="text-right">
						<a href="javascript:;" class="btn btn-primary checkout">Check out</a>
					</div>
					<!-- begin invoice-note -->
					<div class="invoice-note">
						* Make all cheques payable to [Your Company Name]<br />
						* Payment is due within 30 days<br />
						* If you have any questions concerning this invoice, contact  [Name, Phone Number, Email]
					</div>
					<!-- end invoice-note -->
					<!-- begin invoice-footer -->
					<div class="invoice-footer">
						<p class="text-center m-b-5 f-w-600">
							THANK YOU FOR YOUR BUSINESS
						</p>
						<p class="text-center">
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-globe"></i> matiasgallipoli.com</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-phone-volume"></i> T:016-18192302</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-envelope"></i> rtiemps@gmail.com</span>
						</p>
					</div>
					<!-- end invoice-footer -->
				</div>
				<!-- end invoice -->
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<div class="hide singleproduct">
	<input type="hidden" class="product-id">
	<input type="hidden" class="product-type">
	<input type="hidden" class="couponcode">
</div>