<link href="<?php echo base_url(); ?>assets/studentassets/css/testseries/testseries.css" rel="stylesheet" />
<!-- BOLT Sandbox/test //-->
<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="https://pcskaka.com/developer/assets/themeassets/img/user/user-13.jpg"></script>


<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style-responsive.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/default.css" rel="stylesheet" />
<style type="text/css">
	.checkbox.checkbox-css{
		padding-top: 0px;
	}
	.checkbox.checkbox-css label{
		padding-left: 19px;
	}
	.search-category-list>li{
		padding-left: 18px;
		background: white;
	}
	.widget.widget-stats{
		position: relative;
	    color: #000;
	    padding: 15px;
	    border-radius: 3px;
	    background-image: linear-gradient(rgb(255, 255, 255) 0%, rgba(255, 255, 255, 0.54) 46%, white 100%) !important;
	    min-height: 10px;
	}
	
</style>

<!-- BOLT Production/Live //-->
 <!--<script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="https://pcskaka.com/developer/assets/themeassets/img/user/user-13.jpg"></script>-->

<!-- begin #content --> 
<?php
function getCallbackUrlsuccess()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/successpayment';
}
function getCallbackUrlfailure()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/failurepayment';
}
?>

		<div id="content" class="content p-t-0">
			
			
			<!-- begin page-header -->
			<div class="search-toolbar sticky-top" style="top: 64px">
						<h4 style="font-size: 22px" class="pull-left">Exams / <?php echo $LIB_DETAIL->LIB_NAME; ?></h4>

						<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right buyallcartproduct">
			</ol>
			<!-- end breadcrumb -->
			</div>
			<!-- end page-header -->

			<!--Start Container-->
			<div class="container mt-4">
				<h1 class="page-header f-w-400">Test Series</h1>
				<div class="row">

					<?php foreach ($LIB_DATA as $row) { ?>

					<div class="col-lg-3 col-md-6">
						<div class="card border h-100 shadow" style="border-radius: 18px;">
						  <?php if(!empty($row["TS_IMG"])){?>
						  <img class="card-img-top" style="border-top-right-radius: 18px;border-top-left-radius: 18px;" src="<?php echo $row["TS_IMG"]; ?>" alt="Card image cap">
						  <?php } ?>
						  <div class="card-body">
						    <h5 class="card-title text-truncate width-full"><?php echo $row["TS_NAME"]; ?></h5>
						    <p class="card-text">
						    	<?php echo count($row["free_test_papers"]); ?> Free Test Papers <br>
						    	<?php echo count($row["paid_test_papers"]); ?>  Paid Test Papers <br>
						    	<?php echo count($row["free_quizzes"]); ?> Free Quizzes <br>	
						    	<?php echo count($row["paid_quizzes"]); ?> Paid <br>
						    	<span class="badge f-s-12">₹<?php echo $row["TS_COST"]; ?></span>
						    </p>
						    <div class="row">
						    	<div class="col-lg-6 col-md-12 col-6 d-flex justify-content-center">
						    		<a href="<?php  echo base_url().'student/testpaper/'.$row["TS_ID"]; ?>" class="btn btn-info btn-sm textcard- f-s-10 text-nowrap">VIEW DETAILS</a>
						    	</div>
						    	<div class="col-lg-6 col-md-12 col-6 d-flex justify-content-center">
						    		<?php
                                        $discount = $this->Studentgetmodel->get_discount_by_prod_type(4, 2, $row["TS_ID"]);
                                        $totalamt=$discamt=0;
                                        if (!empty($discount)) {
                                            if ($discount[0]->COUPON_MODE==1) {
                                                $discamt = $discount[0]->COUPON_AMOUNT;
                                                $discstr = "₹";
                                            } else {
                                                $discamt = ($row["TS_COST"]*$discount[0]->COUPON_AMOUNT)/100;
                                                $discstr = "%";
                                            }
                                            $totalamt = $row["TS_COST"] - $discamt;
                                        } else {
                                            $totalamt = $row["TS_COST"];
                                        }

                                    ?>
									<?php
                                        $checkpayment = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 4, $row["TS_ID"]);
                                        if (empty($checkpayment)) {
                                            ?>
									<input type="hidden" id="surl" name="surl" value="<?php echo getCallbackUrlsuccess(); ?>" />
					       			<input type="hidden" id="furl" name="furl" value="<?php echo getCallbackUrlfailure(); ?>" />
									<a 
										href="javascript:void(0);" 
										class="btn btn-warning btn-sm f-s-10 addtocart text-nowrap" 
										attr-prodid="<?php echo $row["TS_ID"]; ?>" 
										attr-prodtype="4" 
										attr-prodcost="<?php echo $row["TS_COST"]; ?>"  
										attr-prodname="<?php echo $row["TS_NAME"]; ?>"   
										attr-discountamt="<?php echo $discamt; ?>" 
										attr-totalamt="<?php echo $totalamt; ?>"
									>
										<i class="fa fa-plus"></i> ADD TO CART
									</a>
								    <?php
                                        } else { ?>
								    	<span class="btn-success btn-sm btn f-s-10">PURCHASED</span>
								    <?php } ?>
						    	</div>
						    </div>
						  </div>
						</div>
					</div>
					<?php } ?>

				</div>
			</div>
			<!--End Container-->

			<!--Start Container-->
			<div class="container mt-4">
				<h1 class="page-header f-w-400">Free Test Papers</h1>
				<div class="row">

				<?php foreach ($LIB_DATA as $ts) {
                                            foreach ($ts["free_test_papers"] as $tp) { ?>

					<div class="col-lg-4 col-md-12 col-12">
    						
    						<div class=" widget  widget-list-item shadow">
								<div class="widget-list-media p-4 p-r-10">
									<img src="<?php echo base_url(); ?>assets/logo/logo.jpeg" alt=""  style="width: 100%;height: 100%;border-radius: 50%;">
								</div>
								<div class="widget-list-content">
									<h4 class="widget-list-title text-truncate width-150">
										<a href="#" class=""><?php echo $tp["TP_NAME"]; ?>
										</a>
									</h4>
									<p class="widget-list-desc">
										<?php echo $tp["TS_QUESTION_NOS"]." Questions"; ?><br>
										<?php echo $tp["TS_MARK"]." Marks"; ?>,
										<?php echo $tp["TS_DURATION"]." Mins"; ?>
									</p>

									
								</div>
								<div class="widget-list-media p-0 p-r-10">
									<!-- <a href="" class="btn btn-info btn-sm text-nowrap">Start Now</a> -->

									<?php if (strtotime(date('Y-m-d H:i:s'))>=strtotime($tp["TP_START_DATE"])) { ?>
        						        <?php $paperattempt = $this->Studentgetmodel->check_attempt_testpaper($tp["TP_ID"]); if (!$paperattempt) { ?>
                                            <a href="<?php echo base_url().'test/solution/'.url_encode($tp["TP_ID"]); ?>" class="btn btn-success m-r-5 m-b-5 btn-sm text-nowrap" > Solution</a>
                                        <?php } else { ?>
                                        
        						            <a href="<?php echo base_url().'test/instruction/'.url_encode($tp["TP_ID"]); ?>" class="btn btn-danger m-r-5 m-b-5 btn-sm text-nowrap" > Start Paper</a>
                                        <?php } ?>
    							    <?php } else { ?>
                                        <a href="javascript:;" class="btn btn-warning f-s-10 btn-sm text-nowrap" style="border-color:#5c5e5f;"><i class="fa fa-lock"></i> Start on <?php echo date('M d', strtotime($tp["TP_START_DATE"])); ?></a>
                                        
                                    <?php } ?>

								</div>

							</div>
    						
    				</div>

    			<?php }
                                        } ?>

				</div>
			</div>
			<!--End Container-->


			<!--Start Container-->
			<div class="container mt-4">
				<h1 class="page-header f-w-400">Paid Test Papers</h1>
				<div class="row">

				<?php foreach ($LIB_DATA as $ts) {
                                            foreach ($ts["paid_test_papers"] as $tp) { ?>

					<div class="col-lg-4 col-md-12 col-12">
    						
    						<div class=" widget  widget-list-item shadow">
								<div class="widget-list-media p-4 p-r-10">
									<img src="<?php echo base_url(); ?>assets/logo/logo.jpeg" alt=""  style="width: 100%;height: 100%;border-radius: 50%;">
								</div>
								<div class="widget-list-content">
									<h4 class="widget-list-title text-truncate width-150">
										<a href="#" class=""><?php echo $tp["TP_NAME"]; ?>
										</a>
									</h4>
									<p class="widget-list-desc">
										<?php echo $tp["TS_QUESTION_NOS"]." Questions"; ?><br>
										<?php echo $tp["TS_MARK"]." Marks"; ?>,
										<?php echo $tp["TS_DURATION"]." Mins"; ?>
									</p>

									
								</div>
								<div class="widget-list-media p-0 p-r-10">
									<?php
                                    date_default_timezone_set('Asia/Kolkata');
                                    $check = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 4, $tp["TS_ID"]);
                                    if (!empty($check) && strtotime(date('Y-m-d H:i:s'))>=strtotime($tp["TP_START_DATE"])) { ?>
        						        <?php $paperattempt = $this->Studentgetmodel->check_attempt_testpaper($tp["TP_ID"]); if (!$paperattempt) { ?>
                                            <a href="<?php echo base_url().'test/solution/'.url_encode($tp["TP_ID"]); ?>" class="btn btn-success m-r-5 m-b-5 btn-sm text-nowrap" > Solution</a>
                                        <?php } else { ?>
                                        
        						            <a href="<?php echo base_url().'test/instruction/'.url_encode($tp["TP_ID"]); ?>" class="btn btn-danger m-r-5 m-b-5 btn-sm text-nowrap" > Start Paper</a>
                                        <?php } ?>
    							    <?php } else { ?>
                                        <a href="javascript:;" class="btn btn-warning f-s-10 btn-sm text-nowrap" style="border-color:#5c5e5f;"><i class="fa fa-lock"></i> Start on <?php echo date('M d', strtotime($tp["TP_START_DATE"])); ?></a>
                                        
                                    <?php } ?>
								</div>

							</div>
    						
    				</div>

    			<?php }
                                        } ?>

				</div>
			</div>
			<!--End Container-->


			<!--Start Container-->
			<div class="container mt-4">
				<h1 class="page-header f-w-400">Free Quizzes</h1>
				<div class="row">

				<?php foreach ($LIB_DATA as $ts) {
                                            foreach ($ts["free_quizzes"] as $tp) { ?>

					<div class="col-lg-4 col-md-12 col-12">
    						
    						<div class=" widget  widget-list-item">
								
								<div class="widget-list-content">
									<h4 class="widget-list-title">
										<a href="#" class=""><?php echo $tp["TP_NAME"]; ?>
										</a>
									</h4>
									<p class="widget-list-desc">
										<?php echo $tp["TS_QUESTION_NOS"]." Questions"; ?><br>
										<?php echo $tp["TS_MARK"]." Marks"; ?>,
										<?php echo $tp["TS_DURATION"]." Mins"; ?>
									</p>

									
								</div>
								<div class="widget-list-media p-0 p-r-10">
									<?php
                                    if (strtotime(date('Y-m-d H:i:s'))>=strtotime($tp["TP_START_DATE"])) { ?>
        						        <?php $paperattempt = $this->Studentgetmodel->check_attempt_testpaper($tp["TP_ID"]); if (!$paperattempt) { ?>
                                            <a href="<?php echo base_url().'test/solution/'.url_encode($tp["TP_ID"]); ?>" class="btn btn-success m-r-5 m-b-5 btn-sm text-nowrap" > Solution</a>
                                        <?php } else { ?>
                                        
        						            <a href="<?php echo base_url().'test/instruction/'.url_encode($tp["TP_ID"]); ?>" class="btn btn-danger m-r-5 m-b-5 btn-sm text-nowrap" > Start Paper</a>
                                        <?php } ?>
    							    <?php } else { ?>
                                        <a href="javascript:;" class="btn btn-warning f-s-10 btn-sm text-nowrap" style="border-color:#5c5e5f;"><i class="fa fa-lock"></i> Start on <?php echo date('M d', strtotime($tp["TP_START_DATE"])); ?></a>
                                        
                                    <?php } ?>
								</div>

							</div>
    						
    				</div>

    			<?php }
                                        } ?>

				</div>
			</div>
			<!--End Container-->


			<!--Start Container-->
			<div class="container mt-4">
				<h1 class="page-header f-w-400">Paid Quizzes</h1>
				<div class="row">

				<?php foreach ($LIB_DATA as $ts) {
                                            foreach ($ts["paid_quizzes"] as $tp) { ?>

					<div class="col-lg-4 col-md-12 col-12">
    						
    						<div class=" widget  widget-list-item">
								<div class="widget-list-media p-0 p-r-10">
									<img src="<?php echo base_url(); ?>assets/themeassets/img/user/upsc-icon-10.jpg" alt="" class="rounded" style="width: 100%;height: 100%;">
								</div>
								<div class="widget-list-content">
									<h4 class="widget-list-title">
										<a href="#" class=""><?php echo $tp["TP_NAME"]; ?>
										</a>
									</h4>
									<p class="widget-list-desc">
										<?php echo $tp["TS_QUESTION_NOS"]." Questions"; ?><br>
										<?php echo $tp["TS_MARK"]." Marks"; ?><br>
										<?php echo $tp["TS_DURATION"]." Mins"; ?>
									</p>

								</div>
								<div class="widget-list-media p-0 p-r-10">
									<?php
                                    date_default_timezone_set('Asia/Kolkata');
                                    $check = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 4, $tp["TS_ID"]);
                                    if (!empty($check) && strtotime(date('Y-m-d H:i:s'))>=strtotime($tp["TP_START_DATE"])) { ?>
        						        <?php $paperattempt = $this->Studentgetmodel->check_attempt_testpaper($tp["TP_ID"]);
                                        if (!$paperattempt) { ?>
                                            <a href="<?php echo base_url().'test/solution/'.url_encode($tp["TP_ID"]); ?>" class="btn btn-success m-r-5 m-b-5 btn-sm text-nowrap" > Solution</a>
                                        <?php } else { ?>
                                        
        						            <a href="<?php echo base_url().'test/instruction/'.url_encode($tp["TP_ID"]); ?>" class="btn btn-danger m-r-5 m-b-5 btn-sm text-nowrap" > Start Paper</a>
                                        <?php } ?>
    							    <?php } else { ?>
                                        <a href="javascript:;" class="btn btn-warning f-s-10 btn-sm text-nowrap" style="border-color:#5c5e5f;"><i class="fa fa-lock"></i> Start on <?php echo date('M d', strtotime($tp["TP_START_DATE"])); ?></a>
                                        
                                    <?php } ?>
								</div>

							</div>
    						
    				</div>

    			<?php }
                                        } ?>

				</div>
			</div>
			<!--End Container-->
			
		</div>
		<!-- end #content -->
		
<!-- APPLY COUPON MODEL -->
<!-- <div class="modal fade" id="applycoupon">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">CHECKOUT</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
			    <div class="col-md-12 checkout_html_div">
			        
			    </div>
			</div>
			<div class="modal-footer">
				<button type="button "class="btn btn-success checkout">Checkout</a>
			</div>
		</div>
	</div>
</div> -->
<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
<link href="<?php echo base_url(); ?>assets/themeassets/css/default/invoice-print.min.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
<!-- #modal-without-animation -->
<div class="modal " id="modal-prucahseproduct">
	<div class="modal-dialog  modal-xl">
		<div class="modal-content">
			<div class="modal-header hidden-print">
				<h4 class="modal-title">Review Your Order</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body ">
				<!-- begin invoice -->
				<div class="invoice">
					<!-- begin invoice-company -->
					<div class="invoice-company">
						<span class="pull-right hidden-print">
							<a href="javascript:;" class="btn btn-sm btn-white m-b-10"><i class="fa fa-file-pdf t-plus-1 text-danger fa-fw fa-lg"></i> Export as PDF</a>
							<a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-white m-b-10"><i class="fa fa-print t-plus-1 fa-fw fa-lg"></i> Print</a>
						</span>
						PCSKAKA
					</div>
					<!-- end invoice-company -->
					<!-- begin invoice-header -->
					<div class="invoice-header">
						<div class="invoice-from">
							<small>from</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse">PCSKAKA, Inc.</strong><br />
								Phone: +91 945-273-6000<br />
								Email: support@pcskaka.com
							</address>
						</div>
						<div class="invoice-to">
							<small>to</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse"><span class="name"></span></strong><br />
								<span class="student_address_html"></span></br>
								Phone: <span class="student_mobileno"></span><br />
								Email: <span class="student_name_upper"></span>
							</address>
						</div>
						<div class="invoice-date">
							<small>Invoice / <?php echo date('F');?> period</small>
							<div class="date text-inverse m-t-5"><?php echo date('d-F-Y'); ?></div>
							<div class="invoice-detail">
								#0000123DSS<br />
								Services Product
							</div>
						</div>
					</div>
					<!-- end invoice-header -->
					<!-- begin invoice-content -->
					<div class="invoice-content">
						<!-- begin table-responsive -->
						<div class="table-responsive ">
							<table class="table table-invoice buycarttable">
								<thead>
									<tr>
										<th class="text-nowrap"> PRODUCT DESCRIPTION</th>
										<th class="text-right" width="5%">PRICE</th>
										<th class="text-right" width="5%">DISCOUNT</th>
										<th class="text-right" width="5%">TOTAL</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot></tfoot>
							</table>
						</div>
						<!-- end table-responsive -->
						<!-- begin invoice-price -->
						<div class="invoice-price">
							<div class="invoice-price-left bg-white">
								<div class="invoice-price-row hide">
									<div class="sub-price">
										<small>SUBTOTAL</small>
										<span class="text-inverse subtotal"></span>
									</div>
								</div>
							</div>
							<div class="invoice-price-right totalpay">
								<small>TOTAL</small> <span class="f-w-600"></span>
							</div>
						</div>
						<!-- end invoice-price -->
					</div>
					<!-- end invoice-content -->
					<div class="text-right">
						<a href="javascript:;" class="btn btn-primary checkout">Pay Now</a>
					</div>
					<!-- begin invoice-note -->
					<div class="invoice-note">
						* Make all cheques payable to [Your Company Name]<br />
						* Payment is due within 30 days<br />
						* If you have any questions concerning this invoice, contact  [Name, Phone Number, Email]
					</div>
					<!-- end invoice-note -->
					<!-- begin invoice-footer -->
					<div class="invoice-footer">
						<p class="text-center m-b-5 f-w-600">
							THANK YOU FOR YOUR BUSINESS
						</p>
						<p class="text-center">
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-globe"></i> matiasgallipoli.com</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-phone-volume"></i> T:016-18192302</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-envelope"></i> rtiemps@gmail.com</span>
						</p>
					</div>
					<!-- end invoice-footer -->
				</div>
				<!-- end invoice -->
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<div class="hide singleproduct">
	<input type="hidden" class="product-id">
	<input type="hidden" class="product-type">
	<input type="hidden" class="couponcode">
</div>

		
		
		