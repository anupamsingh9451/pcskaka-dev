<link href="<?php echo base_url(); ?>assets/studentassets/css/testseries/testseries.css" rel="stylesheet" />
<!-- BOLT Sandbox/test //-->
<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524"
	bolt-logo="https://pcskaka.com/developer/assets/themeassets/img/user/user-13.jpg"></script>


<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style-responsive.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/default.css" rel="stylesheet" />
<style type="text/css">
	.checkbox.checkbox-css {
		padding-top: 0px;
	}

	.checkbox.checkbox-css label {
		padding-left: 19px;
	}

	.search-category-list>li {
		padding-left: 18px;
		background: white;
	}

	.widget.widget-stats {
		position: relative;
		color: #000;
		padding: 15px;
		border-radius: 3px;
		background-image: linear-gradient(rgb(255, 255, 255) 0%, rgba(255, 255, 255, 0.54) 46%, white 100%) !important;
		min-height: 10px;
	}

	.carousel .carousel-inner,
	.carousel .carousel-inner .carousel-item,
	.carousel .carousel-inner .item,
	.slider .carousel {
		min-height: 100px;
	}
	.default-card-img{
		padding:20px 60px;
		max-height:136px;
		max-width:250px;
	}
	@media (min-width: 576px) { 
		#carouselExampleIndicators img {
			height:500px !important;
		}

	}

	@media (max-width: 576px) { 
		#carouselExampleIndicators img {
			height:300px !important;
		}

	}
</style>

<!-- BOLT Production/Live //-->
<!--<script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="https://pcskaka.com/developer/assets/themeassets/img/user/user-13.jpg"></script>-->

<!-- begin #content -->
<?php
function getCallbackUrlsuccess()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/successpayment';
}
function getCallbackUrlfailure()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/failurepayment';
}
?>

<div id="content" class="content p-t-0">


	<!-- begin page-header -->
	<div class="search-toolbar sticky-top" style="top: 64px">
		<h4 style="font-size: 22px" class="pull-left">Courses/<?php echo $NAME?></h4>
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right buyallcartproduct">
		</ol>
		<!-- end breadcrumb -->
	</div>
	<!-- end page-header -->

	<div class="row">
		
		<div class="col-lg-12 col-md-12">
			<div class="container mt-4">
				<div class="row">
					<?php
                        $popular_slider=$this->Studentgetmodel->get_all_suggested_sliders('2');
                        if (!empty($popular_slider)) {
                            ?>
					<div class="col-lg-12">
						<div id="carouselExampleIndicators" class="carousel slide shadow" data-ride="carousel">
							<ol class="carousel-indicators">
							<?php $slider_inc=1;
                            foreach ($popular_slider as $slider) { ?>
								<li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $slider_inc++; ?>" class="<?php if ($slider_inc==1) {
                                echo "active";
                            }?>"></li>
							<?php } ?>
							</ol>
							<div class="carousel-inner">
								<?php $slider_inc=1;
                            foreach ($popular_slider as $slider) { ?>
									<div class="carousel-item <?php if ($slider_inc==1) {
                                echo "active";
                            }?>">
										<img class="d-block w-100 img-responsive" src="<?php echo base_url($slider->SLIDER_IMG); ?>" alt="<?php echo $slider_inc++ ?> slide">
									</div>
								<?php } ?>
							</div>
							<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
								data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
								data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
					<?php
                        } ?>
				</div>
			</div>

			<!--Start Container-->
			<?php if (!empty($allsubjects)) {?>
			<div class="container mt-4">
				<h1 class="page-header f-w-700">Subjects</h1>
				<div class="row">
					<?php
                        $i=1;
                        foreach ($allsubjects as $subjectdt) {
                            $checkpayment = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 2, $subjectdt->SUB_ID);
                            $course = $this->Studentgetmodel->get_course_by_id($subjectdt->COURSE_ID);
                            $discount = $this->Studentgetmodel->get_discount_by_prod_type(2, 2, $subjectdt->SUB_ID);
                            $totalamt=$discamt=0;
                            if (!empty($discount)) {
                                if ($discount[0]->COUPON_MODE==1) {
                                    $discamt = $discount[0]->COUPON_AMOUNT;
                                    $discstr = "₹";
                                } else {
                                    $discamt = ($subjectdt->SUB_COST*$discount[0]->COUPON_AMOUNT)/100;
                                    $discstr = "%";
                                }
                            } ?>
							<div class="col-lg-3 col-md-6 mb-4 course_ts" data-course="<?php if (count($course)>0) {
                                echo $course[0]->COURSE_NAME;
                            } ?>">										
								<div class="card border h-100 shadow" style="border-radius: 18px;">
									<div class="card-body">
										<h4 class="card-title  width-full height-40"><?php echo $subjectdt->SUB_NAME; ?></h4>
										<strong><?php echo $NAME?></strong>
										<p class="card-text">
											<?php $arr = $this->Studentgetmodel->get_booksdt_by_sub_id($subjectdt->SUB_ID);
                            if (!empty($arr)) {
                                echo sizeof($arr);
                            } else {
                                echo '0';
                            } ?> Total books 
											<br>
											<?php if ($subjectdt->SUB_STATUS==0) {
                                echo '<div class="pull-right badge badge-danger discount blink_me coming_soon">Coming Soon!</div>';
                            } else { ?>
												<?php if (!empty($discount)) { ?>
													<div class="discount blink_me coming_soon"><?php echo $discount[0]->COUPON_AMOUNT." ".$discstr.' off'; ?></div>
												<?php } ?>
											<?php } ?>
											<br>
											<?php if (!empty($discount)) {
                                $totalamt = $subjectdt->SUB_COST-$discamt;
                                echo '<div class="badge item-price">₹'.$totalamt.'</div><div class="item-discount-price">'.$subjectdt->SUB_COST.'</div>';
                            } else {
                                echo '<div class="badge item-discount-price">₹'.$subjectdt->SUB_COST.'</div>';
                            } ?>
										</p>
										<div class="row">
											<div class="col-lg-6 col-md-12 col-6 d-flex justify-content-center">
												<!-- <a href="#" class="btn btn-info btn-sm textcard- f-s-10 text-nowrap">VIEW DETAILS</a> -->
												<a href="<?php if ($subjectdt->SUB_STATUS==1) {
                                echo base_url().'student/subject_details/'.$subjectdt->SUB_ID;
                            } else {
                                echo "javascript:void(0);";
                            } ?>" class="btn btn-info btn-sm textcard- f-s-10 text-nowrap <?php if ($subjectdt->SUB_STATUS==0) {
                                echo "cursor_disabled";
                            } ?>">VIEW DETAILS</a>
											</div>
											<div class="col-lg-6 col-md-12 col-6 d-flex justify-content-center">
												<input type="hidden" id="surl" name="surl" value="<?php echo getCallbackUrlsuccess(); ?>" />
												<input type="hidden" id="furl" name="furl" value="<?php echo getCallbackUrlfailure(); ?>" />
												<?php if (empty($checkpayment) && $subjectdt->SUB_STATUS==1) { ?>
												<a 
													href="javascript:void(0);" 
													class="btn btn-warning btn-sm f-s-10 addtocart text-nowrap" 
													attr-prodid="<?php echo $subjectdt->SUB_ID; ?>" 
													attr-prodtype="2" 
													attr-prodcost="<?php echo $subjectdt->SUB_COST; ?>"  
													attr-prodname="<?php echo $subjectdt->SUB_NAME; ?>"   
													attr-discountamt="<?php echo $discamt;?>" 
													attr-totalamt="<?php echo $totalamt;?>"
												>
													<i class="fa fa-plus"></i> ADD TO CART
												</a>
												<?php } elseif ($subjectdt->SUB_STATUS==1) { ?>
														<span class="btn-success btn-sm btn f-s-10">PURCHASED</span>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>	
							</div>	
					<?php
                        $i++;
                        }
                    ?>	
				</div>
			</div>
			<?php } ?>
			<!-- End Container -->
		</div>
	</div>
</div>
<!-- end row -->
</div>
<!-- end #content -->

<!-- APPLY COUPON MODEL -->
<!-- <div class="modal fade" id="applycoupon">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">CHECKOUT</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
			    <div class="col-md-12 checkout_html_div">
			        
			    </div>
			</div>
			<div class="modal-footer">
				<button type="button "class="btn btn-success checkout">Checkout</a>
			</div>
		</div>
	</div>
</div> -->

<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
<link href="<?php echo base_url(); ?>assets/themeassets/css/default/invoice-print.min.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
<!-- #modal-without-animation -->
<div class="modal " id="modal-prucahseproduct">
	<div class="modal-dialog  modal-xl">
		<div class="modal-content">
			<div class="modal-header hidden-print">
				<h4 class="modal-title">Review Your Order</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body ">
				<!-- begin invoice -->
				<div class="invoice">
					<!-- begin invoice-company -->
					<div class="invoice-company">
						<span class="pull-right hidden-print">
							<a href="javascript:;" class="btn btn-sm btn-white m-b-10"><i
									class="fa fa-file-pdf t-plus-1 text-danger fa-fw fa-lg"></i> Export as PDF</a>
							<a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-white m-b-10"><i
									class="fa fa-print t-plus-1 fa-fw fa-lg"></i> Print</a>
						</span>
						PCSKAKA
					</div>
					<!-- end invoice-company -->
					<!-- begin invoice-header -->
					<div class="invoice-header">
						<div class="invoice-from">
							<small>from</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse">PCSKAKA, Inc.</strong><br />
								Phone: +91 945-273-6000<br />
								Email: support@pcskaka.com
							</address>
						</div>
						<div class="invoice-to">
							<small>to</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse"><span class="name"></span></strong><br />
								<span class="student_address_html"></span></br>
								Phone: <span class="student_mobileno"></span><br />
								Email: <span class="student_name_upper"></span>
							</address>
						</div>
						<div class="invoice-date">
							<small>Invoice / <?php echo date('F');?> period</small>
							<div class="date text-inverse m-t-5"><?php echo date('d-F-Y'); ?></div>
							<div class="invoice-detail">
								#0000123DSS<br />
								Services Product
							</div>
						</div>
					</div>
					<!-- end invoice-header -->
					<!-- begin invoice-content -->
					<div class="invoice-content">
						<!-- begin table-responsive -->
						<div class="table-responsive ">
							<table class="table table-invoice buycarttable">
								<thead>
									<tr>
										<th class="text-nowrap"> PRODUCT DESCRIPTION</th>
										<th class="text-right" width="5%">PRICE</th>
										<th class="text-right" width="5%">DISCOUNT</th>
										<th class="text-right" width="5%">TOTAL</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot></tfoot>
							</table>
						</div>
						<!-- end table-responsive -->
						<!-- begin invoice-price -->
						<div class="invoice-price">
							<div class="invoice-price-left bg-white">
								<div class="invoice-price-row hide">
									<div class="sub-price">
										<small>SUBTOTAL</small>
										<span class="text-inverse subtotal"></span>
									</div>
								</div>
							</div>
							<div class="invoice-price-right totalpay">
								<small>TOTAL</small> <span class="f-w-600"></span>
							</div>
						</div>
						<!-- end invoice-price -->
					</div>
					<!-- end invoice-content -->
					<div class="text-right">
						<a href="javascript:;" class="btn btn-primary checkout">Pay Now</a>
					</div>
					<!-- begin invoice-note -->
					<div class="invoice-note">
						* Make all cheques payable to [Your Company Name]<br />
						* Payment is due within 30 days<br />
						* If you have any questions concerning this invoice, contact [Name, Phone Number, Email]
					</div>
					<!-- end invoice-note -->
					<!-- begin invoice-footer -->
					<div class="invoice-footer">
						<p class="text-center m-b-5 f-w-600">
							THANK YOU FOR YOUR BUSINESS
						</p>
						<p class="text-center">
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-globe"></i> matiasgallipoli.com</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-phone-volume"></i> T:016-18192302</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-envelope"></i> rtiemps@gmail.com</span>
						</p>
					</div>
					<!-- end invoice-footer -->
				</div>
				<!-- end invoice -->
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<div class="hide singleproduct">
	<input type="hidden" class="product-id">
	<input type="hidden" class="product-type">
	<input type="hidden" class="couponcode">
</div>
