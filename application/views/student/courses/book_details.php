<link href="<?php echo base_url(); ?>assets/studentassets/css/testseries/testseries.css" rel="stylesheet" />
<!-- BOLT Sandbox/test //-->
<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="https://pcskaka.com/developer/assets/themeassets/img/user/user-13.jpg"></script>


<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style-responsive.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/default.css" rel="stylesheet" />
<style type="text/css">
	.checkbox.checkbox-css{
		padding-top: 0px;
	}
	.checkbox.checkbox-css label{
		padding-left: 19px;
	}
	.search-category-list>li{
		padding-left: 18px;
		background: white;
	}
	.widget.widget-stats{
		position: relative;
	    color: #000;
	    padding: 15px;
	    border-radius: 3px;
	    background-image: linear-gradient(rgb(255, 255, 255) 0%, rgba(255, 255, 255, 0.54) 46%, white 100%) !important;
	    min-height: 10px;
	}
	.carousel .carousel-inner, .carousel .carousel-inner .carousel-item, .carousel .carousel-inner .item, .slider .carousel {
        min-height: 100px;
	}
</style>

<!-- BOLT Production/Live //-->
 <!--<script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="https://pcskaka.com/developer/assets/themeassets/img/user/user-13.jpg"></script>-->

<!-- begin #content --> 
<?php
function getCallbackUrlsuccess()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/successpayment';
}
function getCallbackUrlfailure()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/failurepayment';
}
?>

		<div id="content" class="content p-t-0">
			
			
			<!-- begin page-header -->
			<div class="search-toolbar sticky-top" style="top: 64px">
				<h4 style="font-size: 22px" class="pull-left">Book/<?php echo $NAME; ?></h4>
				<!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right buyallcartproduct">
                </ol>
			    <!-- end breadcrumb -->
			</div>
			<!-- end page-header -->
            
			<div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="container mt-4 p-0">
						<div class="row">
						<?php
                        	$popular_slider=$this->Studentgetmodel->get_all_suggested_sliders('6');
                        	if (!empty($popular_slider)) {
                        ?>
                            <div class="col-lg-12">

                                <div id="carouselExampleIndicators" class="carousel slide shadow" data-ride="carousel">
								    <ol class="carousel-indicators">
									<?php $slider_inc=1;
										foreach ($popular_slider as $slider) { ?>
											<li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $slider_inc++; ?>" class="<?php if ($slider_inc==1) { echo "active";}?>"></li>
									<?php } ?>
									</ol>
									<div class="carousel-inner">
										<?php 
											$slider_inc=1;
											foreach ($popular_slider as $slider) { ?>
											<div class="carousel-item <?php if ($slider_inc==1) { echo "active"; }?>">
												<img class="d-block w-100 img-responsive" src="<?php echo $slider->SLIDER_IMG; ?>" alt="<?php echo $slider_inc++ ?> slide">
											</div>
										<?php } ?>
									</div>
									<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
										<span class="carousel-control-prev-icon" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
										<span class="carousel-control-next-icon" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
                                </div>
                            </div>
						<?php } ?>	
                        </div>
                    </div>
                	<?php if (!empty($all_practicetest)) { ?>
                    <!--Start Container-->
					<div class="conatiner mt-4">
						<h1 class="page-header f-w-700">Practice Tests</h1>
                        <div class="row">
                            <?php
                            $i=1;
                            foreach ($all_practicetest as $testseriesdts) {
                                $checkpayment = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 6, $testseriesdts->TS_ID);
                                if (strtotime($testseriesdts->TS_START_DATE)<strtotime(date('Y-m-d H:i:s')) && strtotime($testseriesdts->TS_END_DATE)>strtotime(date('Y-m-d H:i:s'))) {
                                    $course = $this->Studentgetmodel->get_course_by_id($testseriesdts->COURSE_ID);
                                    $discount = $this->Studentgetmodel->get_discount_by_prod_type(6, 2, $testseriesdts->TS_ID);
                                    $totalamt=$discamt=0;
                                    if (!empty($discount)) {
                                        if ($discount[0]->COUPON_MODE==1) {
                                            $discamt = $discount[0]->COUPON_AMOUNT;
                                            $discstr = "₹";
                                        } else {
                                            $discamt = ($testseriesdts->TS_COST*$discount[0]->COUPON_AMOUNT)/100;
                                            $discstr = "%";
                                        }
                                    } ?>
                                <div class="col-lg-3 col-md-6 mb-4 course_ts" data-course="<?php if (count($course)>0) {
                                        echo $course[0]->COURSE_NAME;
                                    } ?>">
                                    <div class="card border h-100 shadow" style="border-radius: 18px;">
										<?php if(!empty($testseriesdts->TS_IMG)){ ?>
										<img class="card-img-top" style="border-top-right-radius: 18px;border-top-left-radius: 18px;" src="<?php echo $testseriesdts->TS_IMG; ?>" alt="Card image cap">
										<?php } ?>
                                        <div class="card-body">
                                            <h5 class="card-title text-truncate width-full"><?php echo $testseriesdts->TS_NAME; ?></h5>
                                            <p class="card-text">
                                                <?php $arr = json_decode($testseriesdts->TS_TEST_PAPERS);
                                    if (!empty($arr)) {
                                        echo sizeof($arr);
                                    } else {
                                        echo '0';
                                    } ?> Total Test 
                                                <br>
                                                <?php if ($testseriesdts->TS_STATUS==0) {
                                        echo '<div class="discount blink_me coming_soon">Coming Soon!</div>';
                                    } else {
                                        ?>
                                                    <?php if (!empty($discount)) { ?>
                                                        <div class="discount blink_me coming_soon"><?php echo $discount[0]->COUPON_AMOUNT." ".$discstr.' off'; ?></div>
                                                    <?php } ?>
                                                <?php
                                    } ?>
                                                <br>
                                                <?php if (!empty($discount)) {
                                        $totalamt = $testseriesdts->TS_COST-$discamt;
                                        echo '<div class="badge item-price">₹'.$totalamt.'</div><div class="item-discount-price">'.$testseriesdts->TS_COST.'</div>';
                                    } else {
                                        echo '<div class="badge item-discount-price">₹'.$testseriesdts->TS_COST.'</div>';
                                    } ?>
                                            </p>

                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-6 d-flex justify-content-center">
                                                    <!-- <a href="#" class="btn btn-info btn-sm textcard- f-s-10 text-nowrap">VIEW DETAILS</a> -->
                                                    <a href="<?php if ($testseriesdts->TS_STATUS==1) {
                                        echo base_url().'student/ptestpaper/'.$testseriesdts->TS_ID;
                                    } else {
                                        echo "javascript:void(0);";
                                    } ?>" class="btn btn-info btn-sm textcard- f-s-10 text-nowrap <?php if ($testseriesdts->TS_STATUS==0) {
                                        echo "cursor_disabled";
                                    } ?>">VIEW DETAILS</a>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-6 d-flex justify-content-center">
                                                    <input type="hidden" id="surl" name="surl" value="<?php echo getCallbackUrlsuccess(); ?>" />
                                                    <input type="hidden" id="furl" name="furl" value="<?php echo getCallbackUrlfailure(); ?>" />
                                                    <?php if (empty($checkpayment) && $testseriesdts->TS_STATUS==1) { ?>

                                                    <a 
                                                        href="javascript:void(0);" 
                                                        class="btn btn-warning btn-sm f-s-10 addtocart text-nowrap" 
                                                        attr-prodid="<?php echo $testseriesdts->TS_ID; ?>" 
                                                        attr-prodtype="6" 
                                                        attr-prodcost="<?php echo $testseriesdts->TS_COST; ?>"  
                                                        attr-prodname="<?php echo $testseriesdts->TS_NAME; ?>"   
                                                        attr-discountamt="<?php echo $discamt;?>" 
                                                        attr-totalamt="<?php echo $totalamt;?>"
                                                    >
                                                        <i class="fa fa-plus"></i> ADD TO CART
                                                    </a>

                                                    <?php } elseif ($testseriesdts->TS_STATUS==1) { ?>
                                                        <span class="btn-success btn-sm btn f-s-10">PURCHASED</span>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                }
                                $i++;
                            }
                       		?>
						</div>	   
                    </div>
                    <!--End Container-->
                    <?php }?>
            	</div>
			</div>	
				
		</div>
		<!-- end #content -->
		
<!-- APPLY COUPON MODEL -->
<!-- <div class="modal fade" id="applycoupon">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">CHECKOUT</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
			    <div class="col-md-12 checkout_html_div">
			        
			    </div>
			</div>
			<div class="modal-footer">
				<button type="button "class="btn btn-success checkout">Checkout</a>
			</div>
		</div>
	</div>
</div> -->

<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
<link href="<?php echo base_url(); ?>assets/themeassets/css/default/invoice-print.min.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
<!-- #modal-without-animation -->
<div class="modal " id="modal-prucahseproduct">
	<div class="modal-dialog  modal-xl">
		<div class="modal-content">
			<div class="modal-header hidden-print">
				<h4 class="modal-title">Review Your Order</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body ">
				<!-- begin invoice -->
				<div class="invoice">
					<!-- begin invoice-company -->
					<div class="invoice-company">
						<span class="pull-right hidden-print">
							<a href="javascript:;" class="btn btn-sm btn-white m-b-10"><i class="fa fa-file-pdf t-plus-1 text-danger fa-fw fa-lg"></i> Export as PDF</a>
							<a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-white m-b-10"><i class="fa fa-print t-plus-1 fa-fw fa-lg"></i> Print</a>
						</span>
						PCSKAKA
					</div>
					<!-- end invoice-company -->
					<!-- begin invoice-header -->
					<div class="invoice-header">
						<div class="invoice-from">
							<small>from</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse">PCSKAKA, Inc.</strong><br />
								Phone: +91 945-273-6000<br />
								Email: support@pcskaka.com
							</address>
						</div>
						<div class="invoice-to">
							<small>to</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse"><span class="name"></span></strong><br />
								<span class="student_address_html"></span></br>
								Phone: <span class="student_mobileno"></span><br />
								Email: <span class="student_name_upper"></span>
							</address>
						</div>
						<div class="invoice-date">
							<small>Invoice / <?php echo date('F');?> period</small>
							<div class="date text-inverse m-t-5"><?php echo date('d-F-Y'); ?></div>
							<div class="invoice-detail">
								#0000123DSS<br />
								Services Product
							</div>
						</div>
					</div>
					<!-- end invoice-header -->
					<!-- begin invoice-content -->
					<div class="invoice-content">
						<!-- begin table-responsive -->
						<div class="table-responsive ">
							<table class="table table-invoice buycarttable">
								<thead>
									<tr>
										<th class="text-nowrap"> PRODUCT DESCRIPTION</th>
										<th class="text-right" width="5%">PRICE</th>
										<th class="text-right" width="5%">DISCOUNT</th>
										<th class="text-right" width="5%">TOTAL</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot></tfoot>
							</table>
						</div>
						<!-- end table-responsive -->
						<!-- begin invoice-price -->
						<div class="invoice-price">
							<div class="invoice-price-left bg-white">
								<div class="invoice-price-row hide">
									<div class="sub-price">
										<small>SUBTOTAL</small>
										<span class="text-inverse subtotal"></span>
									</div>
								</div>
							</div>
							<div class="invoice-price-right totalpay">
								<small>TOTAL</small> <span class="f-w-600"></span>
							</div>
						</div>
						<!-- end invoice-price -->
					</div>
					<!-- end invoice-content -->
					<div class="text-right">
						<a href="javascript:;" class="btn btn-primary checkout">Pay Now</a>
					</div>
					<!-- begin invoice-note -->
					<div class="invoice-note">
						* Make all cheques payable to [Your Company Name]<br />
						* Payment is due within 30 days<br />
						* If you have any questions concerning this invoice, contact  [Name, Phone Number, Email]
					</div>
					<!-- end invoice-note -->
					<!-- begin invoice-footer -->
					<div class="invoice-footer">
						<p class="text-center m-b-5 f-w-600">
							THANK YOU FOR YOUR BUSINESS
						</p>
						<p class="text-center">
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-globe"></i> matiasgallipoli.com</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-phone-volume"></i> T:016-18192302</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-envelope"></i> rtiemps@gmail.com</span>
						</p>
					</div>
					<!-- end invoice-footer -->
				</div>
				<!-- end invoice -->
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<div class="hide singleproduct">
	<input type="hidden" class="product-id">
	<input type="hidden" class="product-type">
	<input type="hidden" class="couponcode">
</div>
		
		
		