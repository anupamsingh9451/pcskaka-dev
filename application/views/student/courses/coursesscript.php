<script>
$( document ).ready(function() {
/*    $('body').on('click','.check_prod', function (){
        var check = 0;
        $(".check_prod").each(function() {
            if($(this).is(":checked")){
                check = 1;
            }
        });
        if(check==1){
            $('.breadcrumb').removeClass('hide');;
        }else{
            $('.breadcrumb').addClass('hide');;
        }
    });
    $('body').on('click','.buy_checked_prod', function (){
        var prod_array = new Array();
        $(".check_prod").each(function() {
            if($(this).is(":checked")){
                prod_array.push($(this).val());
            }
        });
        var productid = prod_array.join(',');
        var prod_type = 4;
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'studentgetajax/get_checkout_html',
    		data:'productid=' + productid + '&prod_type='+prod_type,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.checkout_html_div').html(msg);
    			$('#applycoupon').modal('show');
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
    	});
    });
    $('body').on('click','.payumoneyts', function (){
        var tsid = $(this).attr('attr-tsid'); 
        var prod_type = 4;
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'studentgetajax/get_checkout_html',
    		data:'productid=' + tsid + '&prod_type='+prod_type,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.checkout_html_div').html(msg);
    			$('#applycoupon').modal('show');
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
    	});
    });
    $('body').on('click','.checkout', function (){
        var tsid = $('.tsid').val(); 
        var promoid = $('.promo_id').val(); 
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'studentgetajax/get_payumoney_details_new',
    		data: 'prod_type='+4+'&productid='+tsid+'&promoid='+promoid ,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    		    console.log(msg);
    		    if(msg.status==200 && msg.message=="ok"){
    		        var data = msg.data;
    		        launchBOLTS(data.key,data.txnid,data.hash,data.amount,data.fname,data.email,data.phone,data.pinfo,data.udf1,data.udf2,data.udf3,data.udf5);
    		    }else{
    		        GritterNotification("Error",'Payment Already Done!',"my-sticky-class");
    		    }
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
    });
    $('body').on('click','.apply_promo', function (){
        var tsid = $('.tsid').val(); 
        var promo_input = $('.promo_input').val(); 
        var prod_type = 4;
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'studentgetajax/check_promo_validity',
    		data:'productid=' + tsid+'&promo_input='+promo_input+'&prod_type='+prod_type+'&coupon_type='+2,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    		    if(msg.message=="ok"){
    		       $('.final_amount').html("₹ "+msg.FINAL_AMT);
    		       $('.promo_amt').html("- "+msg.PROMO_AMT);
    		        var str = '<div class="alert alert-success fade show m-b-0 f-s-14"><strong>Success ! </strong> Promo Code Applied Successfully!</div>';
    		        $('.coupon_validity_msg').html(str);
    		        $('.promo_id').val(msg.PROMO_ID);
    		        
    		        $('.final_amount_div').removeClass('hide');
    		       $('.promo_amt_div').removeClass('hide');
    		    }else{
    		        var str = '<div class="alert alert-danger fade show m-b-0 f-s-14"><strong>Not Valid ! </strong> Promo Code Not Valid!</div>';
    		        $('.coupon_validity_msg').html(str);
    		        
    		        $('.promo_id').val('');
    		        $('.final_amount_div').addClass('hide');
    		       $('.promo_amt_div').addClass('hide');
    		    }
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
    	});
    });
*/
    $(".select_course_category").on('click',function(){
        $('.select_course_category').removeClass('bg-grey');
        $(this).addClass('bg-grey');
        var text = $(this).find('a').text();
		$('.course_cc').removeClass('d-none');
        $.each($(".course_cc"),function(index,value){
            if( text == 'All'){
                $('.course_cc').removeClass('d-none');
            }else if( text != $(value).attr("data-course") ){
                $(value).addClass('d-none');
            }else{
                //
            }
        });
    });
});

/*

function launchBOLTS(keys,txnsid,hashes,amt,fname,emails,phones,pinfo,udf1,udf2,udf3,udf5)
{
	bolt.launch({
	    key: keys,
    	txnid:txnsid, 
    	hash: hashes,
    	amount:amt,
    	firstname:fname,
    	email: emails,
    	phone: phones,
    	productinfo: pinfo,
    	udf5: udf5,
    	udf1: udf1,
    	udf2: udf2,
    	udf3: udf3,
    	surl : $('#surl').val(),
    	furl: $('#furl').val(),
    	mode: 'dropout'
    },{
        responseHandler: function(BOLT){
            if(BOLT.response.txnStatus != 'CANCEL')
        	{
        		//Salt is passd here for demo purpose only. For practical use keep salt at server side only.
        		var fr = '<form action=\"'+$('#surl').val()+'\" method=\"post\">' +
        		'<input type=\"hidden\" name=\"key\" value=\"'+BOLT.response.key+'\" />' +
        		'<input type=\"hidden\" name=\"salt\" value=\"'+$('#salt').val()+'\" />' +
        		'<input type=\"hidden\" name=\"txnid\" value=\"'+BOLT.response.txnid+'\" />' +
        		'<input type=\"hidden\" name=\"amount\" value=\"'+BOLT.response.amount+'\" />' +
        		'<input type=\"hidden\" name=\"productinfo\" value=\"'+BOLT.response.productinfo+'\" />' +
        		'<input type=\"hidden\" name=\"firstname\" value=\"'+BOLT.response.firstname+'\" />' +
        		'<input type=\"hidden\" name=\"email\" value=\"'+BOLT.response.email+'\" />' +
        		'<input type=\"hidden\" name=\"udf5\" value=\"'+BOLT.response.udf5+'\" />' +
        		'<input type=\"hidden\" name=\"udf1\" value=\"'+BOLT.response.udf1+'\" />' +
        		'<input type=\"hidden\" name=\"udf2\" value=\"'+BOLT.response.udf2+'\" />' +
        		'<input type=\"hidden\" name=\"udf3\" value=\"'+BOLT.response.udf3+'\" />' +
        		'<input type=\"hidden\" name=\"mihpayid\" value=\"'+BOLT.response.mihpayid+'\" />' +
        		'<input type=\"hidden\" name=\"status\" value=\"'+BOLT.response.status+'\" />' +
        		'<input type=\"hidden\" name=\"hash\" value=\"'+BOLT.response.hash+'\" />' +
        		'</form>';
        		var form = jQuery(fr);
        		jQuery('body').append(form);								
        		form.submit();
        	}
        },
    	catchException: function(BOLT){
     		alert( BOLT.message );
    	}
    });
}*/
</script>

<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="https://pcskaka.com/developer/assets/themeassets/img/user/user-13.jpg"></script>
<script src="<?php echo base_url();?>assets/studentassets/js/studentpurchase.js"> </script>