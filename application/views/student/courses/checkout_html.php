<link href="<?php echo base_url(); ?>assets/studentassets/css/testseries/checkout_html.css" rel="stylesheet" />
<div class="row">
    <div class="col-md-12">
        <div class="items">
            <div style="height: 44px;padding: 8px 0 8px 14px;background-color: #f1f1f1;">
               <span class="f-s-18">Review Your Order</span>
           </div>
           <div class="p-t-15 m-b-15">
               <?php $total = 0; $tsidarr = array(); if (!empty($tsdt)) {
    foreach ($tsdt as $tsdts) { ?>
                <?php $discount = $this->Studentgetmodel->get_discount_by_prod_type(4, 2, $tsdts->TS_ID); $discamt=0;
                    if (!empty($discount)) {
                        if ($discount[0]->COUPON_MODE==1) {
                            $discamt = $discount[0]->COUPON_AMOUNT;
                            $discstr = "₹";
                        } else {
                            $discamt = ($tsdts->TS_COST*$discount[0]->COUPON_AMOUNT)/100;
                            $discstr = "%";
                        }
                    }
                    $tsidarr[] = $tsdts->TS_ID;
                ?>
                   <div class="m-l-30">
                      <div class="row">
                            <div class="col-md-12">
                                <h3 class="f-s-20"><?php echo $tsdts->TS_NAME; ?></h3>
                            </div>
                      </div>
                       <div class="row m-l-30">
                           <table style="width:100%;">
                               <tr>
                                   <td><span class="f-s-20 text-black-darker">Price</span></td>
                                   <td><h4 style="color:red;">₹ <?php echo $tsdts->TS_COST; ?></h4></td>
                               </tr>
                               <tr>
                                   <td><span class="f-s-19 text-black-darker">Discount</span></td>
                                   <td><h4>₹ <?php echo $discamt; ?></h4></td>
                               </tr>
                               <tr>
                                   <td><span class="f-s-19 text-black-darker">Final Price</span></td>
                                   <td><h4 style="color:green;">₹ <?php echo $tsdts->TS_COST-$discamt; ?></h4></td>
                               </tr>
                           </table>
                       </div>
                   </div>
                   <hr style="border-top: 1px solid #f1f3f5;">
               <?php $total += $tsdts->TS_COST-$discamt; }
} ?>
           </div>
         </div>
    </div>
    <div class="col-md-12 m-t-10">
        <input type="hidden" class="tsid" value="<?php echo implode(',', $tsidarr); ?>">
        <div class="items">
            <div style="height: 44px;padding: 8px 0 8px 14px;background-color: #f1f1f1;">
               <span class="f-s-18">Apply Promo</span>
           </div>
           <div class="p-t-15 p-l-30 p-r-5  m-b-15">
               <div class="input-group m-b-10">
                   <div class="col-md-12">
                       <div class="row">
                           <div class="col-md-12">
                               <div class="m-b-10 coupon_validity_msg">
        							
        						</div>
                           </div>
                       </div>
                       <div class="row">
                           <div class="col-md-8 m-t-2 m-b-2">
                               <input type="text" class="form-control promo_input" name="promo_input">
                           </div>
                           <div class="col-md-4 text-center m-t-2 m-b-2">
                               <button type="button" class="btn btn-primary m-r-5 m-b-5 apply_promo">Submit</button>
                           </div>
                       </div>
                   </div>
				</div>
           </div>
         </div>
    </div>
    <div class="col-md-12 m-t-10">
        <div class="col-md-8 pull-right">
            <div>
                <table>
                    <tr>
                        <td><h4>Total : </h4></td>
                        <td><h3><span style="color:red;">₹ <?php echo $total; ?></span></h3></td>
                    </tr>
                    <tr class="promo_amt_div hide">
                        <td><h4>Promo Amount : </h4></td>
                        <td><h3><span class="promo_amt" style="color:green;"></span></h3></td>
                    </tr>
                    <tr class="final_amount_div hide">
                        <td><h4>Final Amount : </h4></td>
                        <td><h3><span class="final_amount" style="color:red; "></span></h3></td>
                    </tr>
                    <input type="hidden"  class="promo_id" value="">
                </table>
            </div>
        </div>
    </div>
</div>