<script>
$(document).ready(function(){
   


	$('body').on('click','.ques-icon-li',function(){
	   var zwdqi=$(this).attr('attr-zwdqi');
	   var srno=$(this).attr('attr-srno');
	   var prevqueid=$('.ques-icon-li').find('.activeque').parent().attr('attr-zwdqi');
	   if(prevqueid!=zwdqi){
    	   $('.ques-icon-li[attr-zwdqi="'+prevqueid+'"]').find('.ques-icons').removeClass('activeque');

    	   $(this).find('.ques-icons').addClass('activeque');
	   }
	   $('.questions').removeClass('showz');
	   $('.questions').addClass('hidez');
	   $('.questions[attr-zwdqi="'+zwdqi+'"]').addClass('showz');
	   $('.ques-icon-li').find('.ques-icons').removeClass('view');
	   $(this).find('.ques-icons').addClass('view');
	   addiconclass(zwdqi,prevqueid);
	   fullcalc(1);
	   
	});
	
	$('body').on('click','.mark_for_review_and_next',function(){
	    var activequesid = $('.ques-icon-li').find('.activeque').parent().attr('attr-zwdqi');
	    var srno = parseInt($('.ques-icon-li').find('.activeque').parent().attr('attr-srno'));
	    srno = parseInt(srno+1);
	    var checked = 0;
	    $('.ques-icon-li').find('.activeque').addClass('bookmarked');
	    
	    $('.questions[attr-zwdqi="'+activequesid+'"] .form-check-input').each(function(){
            if($(this).prop('checked')){
                checked = checked+1;
            }
        });
        removeallclass(activequesid);
	    if(checked==1){
    	    $('.ques-icon-li').find('.activeque').addClass('attempted bookmarked');
	    }else{
	        $('.ques-icon-li').find('.activeque').addClass('bookmarked');
	    }
	    
	    if($('.total_no_ques').val()==srno-1){
	        fullcalc(1);
	        update_cookie(1);
	    }else{
	        $('.ques-icon-li[attr-srno="'+srno+'"]').trigger('click');
	    }
	    
	});
	
	$('body').on('click','.save_and_next',function(){
	    var activequesid = $('.ques-icon-li').find('.activeque').parent().attr('attr-zwdqi');
	    var checked = 0;
	    var srno = parseInt($('.ques-icon-li').find('.activeque').parent().attr('attr-srno'));
    	srno = parseInt(srno+1);
	    $('.questions[attr-zwdqi="'+activequesid+'"] .form-check-input').each(function(){
            if($(this).prop('checked')){
                checked = checked+1;
            }
        });
        removeallclass(activequesid)
	    if(checked==1){
    	    $('.ques-icon-li').find('.activeque').addClass('attempted');
	    }else{
	        $('.ques-icon-li').find('.activeque').addClass('skipped');
	    }
	    if($('.total_no_ques').val()==srno-1){
	        fullcalc(1);
	        update_cookie(1);
	    }else{
	        $('.ques-icon-li[attr-srno="'+srno+'"]').trigger('click');
	    }
	    
    	   
	});
	$('body').on('click','.clear_response',function(){
	    var activequesid = $('.ques-icon-li').find('.activeque').parent().attr('attr-zwdqi');
	    $('.questions[attr-zwdqi="'+activequesid+'"] .form-check-input').prop('checked', false);
	    if($('.ques-icon-li[attr-zwdqi="'+activequesid+'"]').find('.ques-icons').hasClass('attempted')){
            $('.ques-icon-li[attr-zwdqi="'+activequesid+'"]').find('.ques-icons').removeClass('attempted');
            if($('.ques-icon-li[attr-zwdqi="'+activequesid+'"]').find('.ques-icons').hasClass('bookmarked')){
                
            }else{
                $('.ques-icon-li[attr-zwdqi="'+activequesid+'"]').find('.ques-icons').addClass('skipped');
            }
            
        }
        fullcalc(1);
	});
	$('body').on('click','.submit_testpaper',function(){
	     $('.submit_testpaper').attr('style','display:none !important');
		 $('.submit_testpapernewbtn').attr('style','display:block !important');
	     update_cookie(2);
	     submit_test_paper_modal();
        
	});
	$('body').on('click','.submit_modal',function(){
	    $('#submit_tp_modal').modal('show');
	    $('.m_attempted').html($('.attemptedtotal').html());
	    $('.m_unattempted').html($('.no_visitedtotal').html());
	    $('.m_marked').html($('.bookmarkedtotal').html());
        
	});
	
	    
        
	
});
function addiconclass(zwdqi,prevqueid){
    if($('.ques-icon-li[attr-zwdqi="'+prevqueid+'"]').find('.ques-icons').hasClass('bookmarked')){
        
    }else if($('.ques-icon-li[attr-zwdqi="'+prevqueid+'"]').find('.ques-icons').hasClass('attempted')){
        
    }else if($('.ques-icon-li[attr-zwdqi="'+prevqueid+'"]').find('.ques-icons').hasClass('skipped')){
        
    }else{
        $('.ques-icon-li[attr-zwdqi="'+prevqueid+'"]').find('.ques-icons').addClass('skipped');
    }
    
}
function removeallclass(activequesid){
     $('.ques-icon-li[attr-zwdqi="'+activequesid+'"]').find('.ques-icons').removeClass('attempted bookmarked');
     $('.ques-icon-li[attr-zwdqi="'+activequesid+'"]').find('.ques-icons').removeClass('bookmarked');
     $('.ques-icon-li[attr-zwdqi="'+activequesid+'"]').find('.ques-icons').removeClass('skipped');
     $('.ques-icon-li[attr-zwdqi="'+activequesid+'"]').find('.ques-icons').removeClass('attempted');
}
function fullcalc(val){
    get_not_answered_ques_no();

    var bookmarked=1;
    $('.queslistclass .bookmarked').each(function(){
        bookmarked++;
    });
    bookmarked=bookmarked-1;
    $('.bookmarkedtotal').html(bookmarked);
    
    var attempted=1;
    $('.queslistclass .attempted').each(function(){
        attempted++;
    });
    
    attempted=attempted-1;
    $('.attemptedtotal').html(attempted);
    
    var mark_answer=1;
    $('.queslistclass .attempted').each(function(){
        if($(this).hasClass('bookmarked')){
            mark_answer++;
        }
    });
    mark_answer = mark_answer-1;
    $('.attemptedmarktotal').html(mark_answer);
    
    var no_visited=1;
    $('.queslistclass .ques-icons').each(function(){
        if(!$(this).hasClass("bookmarked") && !$(this).hasClass("skipped") && !$(this).hasClass("attempted"))
        no_visited++;
    });
    no_visited=no_visited-1;
    $('.no_visitedtotal').html(no_visited);
    if(val==1){
        update_cookie(1);
    }
}
function get_not_answered_ques_no(){
    var skip=1;
    $('.queslistclass .skipped').each(function(){
        skip++;
    });
    
    $('.queslistclass .bookmarked').each(function(){
        if(!$(this).hasClass('attempted')){
            skip++;
        }
        
    });
    skip=skip-1;
    $('.skiptotal').html(skip);
}
function update_cookie(checkz){
    var loop=1;
    var str = '';
    $('.ques-icon-li').each(function(){
        var quesid = $(this).attr('attr-zwdqi');
        if($(this).find('.ques-icons').hasClass('skipped')){
            var quesclass = "skipped";
        }else if($(this).find('.ques-icons').hasClass('attempted')){
            if(!$(this).find('.ques-icons').hasClass('bookmarked')){
                var quesclass = "attempted";
            }else{
                var quesclass = "attempted bookmarked";
            }
        }else if($(this).find('.ques-icons').hasClass('bookmarked')){
            if(!$(this).find('.ques-icons').hasClass('attempted')){
                var quesclass = "bookmarked";
            }else{
                var quesclass = "attempted bookmarked";
            }
        }else{
            var quesclass = "";
        }
        var options = 1;
        var ques_ans = 0;
        $('.questions[attr-zwdqi="'+quesid+'"] .form-check-input').each(function(){
            if($(this).prop('checked')){
                ques_ans = options;
            }
            options++;
        });
        
        str += "&quesid_"+loop+"="+quesid+"&quesclass_"+loop+"="+quesclass+"&ques_ans_"+loop+"="+ques_ans;
        loop++;
    });
    
    loop = loop-1;
    
    var datastring  = "uri="+$('.uri').val()+"&total="+loop+str;
    
    var oldtmsp = $('.timestamp').val();
    if($('.timestamp').val()==''){
        var oldtmsp = 0;
    }
    var currenttmsp = $.now();
    var nettmsp = currenttmsp-oldtmsp;
    if(nettmsp>10000 || checkz==2)
    {
        
        $('.timestamp').val($.now());
        if(checkz==2){
            $.ajax({
        		type: 'POST',
        		url: base_loc + 'test/set_cookies',
        		data:datastring,
        		async:false,
        		headers: {
        			'Client-Service': clientserv,
        			'Auth-Key': apikey,
        			'User-ID': loginid,
        			'Authorization': token,
        			'type': type
        		},
        
        		success: function (msg) {
        
        		},
        		error: function (msg) {
        			if (msg.responseJSON['status'] == 303) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 401) {
        				location.href = base_loc; 
        			}
        			if (msg.responseJSON['status'] == 400) {
        				location.href = base_loc; 
        			}
        		}
        	});
        }else{
            $.ajax({
        		type: 'POST',
        		url: base_loc + 'test/set_cookies',
        		data:datastring,
        		headers: {
        			'Client-Service': clientserv,
        			'Auth-Key': apikey,
        			'User-ID': loginid,
        			'Authorization': token,
        			'type': type
        		},
        
        		success: function (msg) {
        
        		},
        		error: function (msg) {
        			if (msg.responseJSON['status'] == 303) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 401) {
        				location.href = base_loc; 
        			}
        			if (msg.responseJSON['status'] == 400) {
        				location.href = base_loc; 
        			}
        		}
        	});
        }
        
    }
}
function submit_test_paper_modal(){
    // localStorage.clear();
    clearInterval(interval);
    $('.time').addClass('hidez');
    var timer = $('.Timer').val();
    urlpath=$(location).attr("href");
    localStorage.removeItem("duration_"+$('.uri').val());
	<?php 
		$pagepath=explode("/",current_url());
		$submitpage="";
		if (in_array("pt_instruction", $pagepath)){
			$submitpage='submit_ptpaper';
		}
		else{
			$submitpage='submittestpaper';
		}
	?>
    $.ajax({
		type: 'POST',
		url: base_loc + 'test/<?php echo $submitpage; ?>',
		data:'tpid=' + $('.uri').val()+"&timer="+timer,
		async:false,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		beforeSend: function () {
			$('.submit_testpaperbtn').attr("disabled", "disabled");
			$('.submit_testpaperbtn').html("Please Wait...");
		},
		success: function (msg) {
			
			$('.submit_testpaperbtn').html("Yes");
			if(msg.status==200 && msg.message=='ok'){
			    localStorage.removeItem("duration_"+$('.uri').val());
			    location.href = base_loc+'test/analysis/'+msg.data+'';
			}
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc; 
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc; 
			}
		}
	});
}
</script>