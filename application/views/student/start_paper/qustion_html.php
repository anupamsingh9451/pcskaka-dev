<input type="hidden" value="<?php echo sizeof($allques); ?>" class="total_no_ques">
<?php $i=1; if (!empty($allques)) {
    foreach ($allques as $allquess) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row questions <?php if ($i!=1) {
        echo "hidez";
    } else {
        "";
    } ?>" attr-gqwts="<?php echo url_encode($tpdt[0]->TP_ID); ?>" attr-gzwtp="<?php echo url_encode($tpdt[0]->TS_ID); ?>" attr-zwdqi="<?php echo url_encode($allquess->QUES_ID); ?>" attr-srno="<?php echo $i; ?>" >
                <div class="col-md-12 border-bottom">
                    <div class="m-15 m-l-20">
                        <h4>Question-<?php echo $i; ?></h4>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="p-10">
                        <div class="m-b-15">
                            <div class="ques">
                                <p class=""><?php $ques = json_decode($allquess->QUES); echo $ques->QUESTION; ?></p>
                            </div>
                            <div class="options p-l-15">
                                <?php $j = 1; foreach ($ques->OPTION as $options) { ?>
                                	<div class="form-check">
                                		<input class="form-check-input" type="radio" name="<?php echo url_encode($allquess->QUES_ID).'_option_'; ?>" id="<?php echo url_encode($allquess->QUES_ID); ?>_option_<?php echo $j; ?>" value="<?php echo $options; ?>" attr-opt-srno="<?php echo $j; ?>" >
                                		<label class="form-check-label" for="<?php echo url_encode($allquess->QUES_ID); ?>_option_<?php echo $j; ?>"><?php echo $options; ?></label>
                                	</div>
                            	<?php $j++; } ?>
                            </div>
                        </div>
                	</div>
                	
                </div>
            </div>
        </div>
    </div>
    
<?php $i++; }
} ?>

		<!-- begin theme-panel -->
		<div class="theme-panel theme-panel-lg">
			<a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
			<div class="theme-panel-content">
			    <div class="profile_details">
			        <div class="profiles">
			            N
			        </div>
			    </div>
			    <div class="divider"></div>
				<ul class="theme-list clearfix">
					<li class="solved-icon"><span class="label attempted attemptedtotal">0</span>&nbsp;Answered&nbsp;&nbsp;</li>
					<li class="solved-icon"><span class="label bookmarked bookmarkedtotal">0</span>&nbsp;Marked&nbsp;&nbsp;</li>
					<li class="solved-icon"><span class="label no_visitedtotal"><?php echo sizeof($allques); ?></span>&nbsp;Not Visited&nbsp;&nbsp;</li>
					<li class="solved-icon"><span class="label attempted bookmarked attemptedmarktotal">0</span>&nbsp;Mark and answered&nbsp;&nbsp;</li>
					<li class="solved-icon"><span class="label skiptotal skipped">0</span>&nbsp;Not Answered&nbsp;&nbsp;</li>
				</ul>
				<div class="divider"></div>
				<div class="side-div">
				    <h4>QUESTIONS</h4>
				</div>
				
				<div class="row m-t-10 queslistclass">
    				<ul class="theme-list clearfix ques_no_div">
    				    <?php $j=1; if (!empty($allques)) {
    foreach ($allques as $allquess) { ?>
    					    <li class="ques-icon-li" attr-gqwts="<?php echo url_encode($tpdt[0]->TP_ID); ?>" attr-gzwtp="<?php echo url_encode($tpdt[0]->TS_ID); ?>" attr-zwdqi="<?php echo url_encode($allquess->QUES_ID); ?>" attr-srno="<?php echo $j; ?>">
    					        <span class="ques-no-label ques-icons <?php if ($j==1) {
        echo "view activeque";
    } else {
        "";
    } ?>"><?php echo $j; ?></span>
    					   </li>
    					<?php $j++;  }
} ?>
    				</ul>
					
				</div>
				<div class="divider"></div>
				<div class="row">
					<div class="col-md-12">
						<a href="javascript:void(0);" class="btn btn-primary btn-block btn-rounded submit_modal"><b>Submit Test</b></a>
					</div>
				</div>
			</div>
		</div>
		<!-- end theme-panel -->
		<div class="modal fade show" id="submit_tp_modal" aria-modal="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
					    <h4 class="modal-title">Summery of Test Paper</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						<div class='row p-12'>
						    <div class="col-md-6">
						        <div class="stats-desc f-w-500 f-s-14 m-0"><span><i class="fas fa-clock fa-fw" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Time Left</span></div>
						    </div>
						    <div class="col-md-6">
						        <span id="defaultCountdown1"></span>
						    </div>
						</div>
						<div class='row p-12'>
						    <div class="col-md-6">
						        <div class="stats-desc f-w-500 f-s-14 m-0"><span><i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Attempted</span></div>
						    </div>
						    <div class="col-md-6">
						        <span class="m_attempted"></span>
						    </div>
						</div>
						<div class='row p-12'>
						    <div class="col-md-6">
						        <div class="stats-desc f-w-500 f-s-14 m-0"><span><i class="fas fa-minus-square fa-fw" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Unattempted</span></div>
						    </div>
						    <div class="col-md-6">
						        <span class="m_unattempted"></span>
						    </div>
						</div>
						<div class='row p-12'>
						    <div class="col-md-6">
						        <div class="stats-desc f-w-500 f-s-14 m-0"><span><i class="fas fa-bookmark fa-fw" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Marked</span></div>
						    </div>
						    <div class="col-md-6">
						        <span class="m_marked"></span>
						    </div>
						</div>
					</div>
					<div class="modal-footer">
					    <a href="javascript:;" class="btn btn-success submit_testpaper">Yes</a>
						<a href="javascript:;" class="btn btn-white" data-dismiss="modal">No</a>
					</div>
				</div>
			</div>
		</div>
	