<link href="<?php echo base_url(); ?>assets/studentassets/css/dashboard.css" rel="stylesheet" />
<style type="text/css">
	.widget-stats .stats-desc, .widget-stats .stats-title {
		font-weight: bold;
		color: rgba(255,255,255,1);
		text-shadow: 0.1px 0.1px 1px black;
		text-align: center;
	}
	.widget-stats .stats-number{
		text-align: center;
	}
	table th{
		text-align: center;
		vertical-align: center;
	}
	table, th, td {
	  border: 1px solid black;
	}

</style>
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active"><a href="javascript:;">Dashboard</a></li>
				<!--<li class="breadcrumb-item active">Dashboard v2</li>-->
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Dashboard</h1>
			<!-- end page-header -->
			
			
			<div class="row">
			    <div class="col-lg-3 col-md-6">
					<div class="widget widget-stats bg-gradient-red">
						<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
						<div class="stats-content">
							<div class="stats-title">Courses</div>
							<div class="stats-number"><?php echo $purchase_summary[0]; ?></div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="widget widget-stats bg-gradient-yellow">
						<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
						<div class="stats-content">
							<div class="stats-title">Exams</div>
							<div class="stats-number"><?php echo $purchase_summary[1]; ?></div>
							
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="widget widget-stats bg-gradient-pink">
						<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
						<div class="stats-content">
							<div class="stats-title">Books</div>
							<div class="stats-number"><?php echo $purchase_summary[3]; ?></div>
							
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="widget widget-stats bg-gradient-blue">
						<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
						<div class="stats-content">
							<div class="stats-title">Test Series</div>
							<div class="stats-number"><?php echo $purchase_summary[4]; ?></div>
							
						</div>
					</div>
				</div>
			</div>
			
			<!-- begin row -->
			<div class="row">
			    <div class="col-md-3">


			    		<div class="widget-todolist widget-todolist-rounded m-b-30" data-id="widget">
						<!-- begin widget-todolist-header -->
						<div class="widget-todolist-header">
							<div class="widget-todolist-header-left">
								<h4 class="widget-todolist-header-title">[Course] TEST MCA</h4>
							</div>
							<div class="widget-todolist-header-right">
							</div>
						</div>
						<!-- end widget-todolist-header -->
						<!-- begin widget-todolist-body -->
						<div class="widget-todolist-body">
							
							<!-- begin widget-todolist-item -->
							<div class="widget-todolist-item">
								<div class="widget-todolist-input">
									<i class="fas fa-check text-success"></i>
								</div>
								<div class="widget-todolist-content">
									<h4 class="widget-todolist-title">20+ Exams</h4>
									<p class="widget-todolist-desc"></p>
								</div>
								<div class="widget-todolist-icon">
								</div>
							</div>
							<!-- end widget-todolist-item -->
							<!-- begin widget-todolist-item -->
							<div class="widget-todolist-item">
								<div class="widget-todolist-input">
									<i class="fas fa-check text-success"></i>
								</div>
								<div class="widget-todolist-content">
									<h4 class="widget-todolist-title">20+ Subjects</h4>
									<p class="widget-todolist-desc"></p>
								</div>
								<div class="widget-todolist-icon">
								</div>
							</div>
							<!-- end widget-todolist-item -->
							<!-- begin widget-todolist-item -->
							<div class="widget-todolist-item">
								<div class="widget-todolist-input">
									<i class="fas fa-check text-success"></i>
								</div>
								<div class="widget-todolist-content">
									<h4 class="widget-todolist-title">20+ Test Series</h4>
									<p class="widget-todolist-desc"></p>
								</div>
								<div class="widget-todolist-icon">
								</div>
							</div>
							<!-- end widget-todolist-item -->
							<!-- begin widget-todolist-item -->
							<div class="widget-todolist-item">
								<div class="widget-todolist-input">
									<i class="fas fa-check text-success"></i>
								</div>
								<div class="widget-todolist-content">
									<h4 class="widget-todolist-title">20+ Books</h4>
									<p class="widget-todolist-desc"></p>
								</div>
								<div class="widget-todolist-icon">
								</div>
							</div>
							<!-- end widget-todolist-item -->
							<!-- begin widget-todolist-item -->
							<div class="widget-todolist-item">
								<div class="widget-todolist-input">
								</div>
								<div class="widget-todolist-content">
									<span style="font-size: 15px; color: green;font-weight: bold;">₹ 1000/-</span> 
									
								</div>
								<div class="widget-todolist-content">
									<button class="btn btn-success">Buy Now</button>
								</div>
							</div>
							<!-- end widget-todolist-item -->
						</div>
						<!-- end widget-todolist-body -->
					</div>
					<!-- end widget-todolist -->
				</div>


				<div class="col-md-3">


			    		<div class="widget-todolist widget-todolist-rounded m-b-30" data-id="widget">
						<!-- begin widget-todolist-header -->
						<div class="widget-todolist-header">
							<div class="widget-todolist-header-left">
								<h4 class="widget-todolist-header-title">[Course] TEST MCA</h4>
							</div>
							<div class="widget-todolist-header-right">
							</div>
						</div>
						<!-- end widget-todolist-header -->
						<!-- begin widget-todolist-body -->
						<div class="widget-todolist-body">
							
							<!-- begin widget-todolist-item -->
							<div class="widget-todolist-item">
								<div class="widget-todolist-input">
									<i class="fas fa-check text-success"></i>
								</div>
								<div class="widget-todolist-content">
									<h4 class="widget-todolist-title">20+ Exams</h4>
									<p class="widget-todolist-desc"></p>
								</div>
								<div class="widget-todolist-icon">
								</div>
							</div>
							<!-- end widget-todolist-item -->
							<!-- begin widget-todolist-item -->
							<div class="widget-todolist-item">
								<div class="widget-todolist-input">
									<i class="fas fa-check text-success"></i>
								</div>
								<div class="widget-todolist-content">
									<h4 class="widget-todolist-title">20+ Subjects</h4>
									<p class="widget-todolist-desc"></p>
								</div>
								<div class="widget-todolist-icon">
								</div>
							</div>
							<!-- end widget-todolist-item -->
							<!-- begin widget-todolist-item -->
							<div class="widget-todolist-item">
								<div class="widget-todolist-input">
									<i class="fas fa-check text-success"></i>
								</div>
								<div class="widget-todolist-content">
									<h4 class="widget-todolist-title">20+ Test Series</h4>
									<p class="widget-todolist-desc"></p>
								</div>
								<div class="widget-todolist-icon">
								</div>
							</div>
							<!-- end widget-todolist-item -->
							<!-- begin widget-todolist-item -->
							<div class="widget-todolist-item">
								<div class="widget-todolist-input">
									<i class="fas fa-check text-success"></i>
								</div>
								<div class="widget-todolist-content">
									<h4 class="widget-todolist-title">20+ Books</h4>
									<p class="widget-todolist-desc"></p>
								</div>
								<div class="widget-todolist-icon">
								</div>
							</div>
							<!-- end widget-todolist-item -->
							<!-- begin widget-todolist-item -->
							<div class="widget-todolist-item">
								<div class="widget-todolist-input">

								</div>
								<div class="widget-todolist-content">
									<span style="font-size: 15px; color: green;font-weight: bold;">₹ 1000/-</span> 
									
								</div>
								<div class="widget-todolist-content">
									<button class="btn btn-success">Buy Now</button>
								</div>
							</div>
							<!-- end widget-todolist-item -->
						</div>
						<!-- end widget-todolist-body -->
					</div>
					<!-- end widget-todolist -->
				</div>






				<div class="col-md-6">
				    <div class="panel panel-inverse" data-sortable-id="table-basic-9">
						<!-- begin panel-heading -->
						<div class="panel-heading ui-sortable-handle">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Attendance</h4>
						</div>
						<!-- end panel-heading -->
						<!-- begin panel-body -->
						<div class="panel-body">
							<div class="chats" data-scrollbar="true" data-height="225px">
								<div class="left">
									<a href="javascript:;" class="image"><img alt="" src="<?php echo base_url(); ?>assets/themeassets/img/user/user-15.png" /></a>
									<div class="message" style="font-size: 15px;">
										You need to attend more test paper. You are missing out on crucial test papers that can help you score better.
									</div>
								</div>
								
								<div class="left">
									<span class="date-time"></span>
									<a href="javascript:;" class="name2"></a>
									<div class="message">

										<div class="widget-chart-info">
											<h4 class="widget-chart-info-title name"></h4>
											<p class="widget-chart-info-desc" style="font-size: 15px;">You Have Attended <?php echo $attendance.'/'.$total; ?> Test Papers.</p>
											<div class="widget-chart-info-progress">
												<b>Attendance Percentage</b>
												<span class="pull-right"><?php if ($total!=0) {
    echo ceil(($attendance*100)/$total);
} else {
    echo 0;
} ?>%</span>
											</div>
											<div class="progress progress-sm">
												<div class="progress-bar progress-bar-striped progress-bar-animated rounded-corner" style="width:<?php if ($total!=0) {
    echo ceil(($attendance*100)/$total);
} else {
    echo 0;
} ?>%;"></div>
											</div>
										</div>


										
									</div>
									
								</div>
							</div>
						</div>
						<!-- end panel-body -->
						
					</div>
				</div>
			</div>
			<!-- end row -->


			<!-- begin row -->
			<div class="row">
			    <div class="col-md-12">
				    <div class="panel panel-inverse" data-sortable-id="table-basic-9">
						<!-- begin panel-heading -->
						<div class="panel-heading ui-sortable-handle">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Test Paper</h4>
						</div>
						<!-- end panel-heading -->
						<!-- begin panel-body -->
						<div class="panel-body">

							<div class="row">
							    <div class="col-lg-4 col-md-6">
									<div class="widget widget-stats bg-gradient-purple">
										<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
										<div class="stats-content">
											<div class="stats-title">Total</div>
											<div class="stats-number"><?php echo $total; ?></div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="widget widget-stats bg-gradient-teal">
										<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
										<div class="stats-content">
											<div class="stats-title">Attempted</div>
											<div class="stats-number"><?php echo $attempted; ?></div>
											
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="widget widget-stats bg-gradient-pink">
										<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
										<div class="stats-content">
											<div class="stats-title">Last Performance</div>
											<div class="stats-number">Excellent</div>
											
										</div>
									</div>
								</div>
							</div>
							<!-- begin table-responsive -->
							<div >
								<table  id="data-table-responsive" class="table table-striped table-bordered">
									<thead>
										
										
										<tr>
											<th>Sno</th>
											<th style="background: lightgray;">Test Series</th>
											<th style="background: lightgray;">Test Paper</th>
											<th style="background: lightgray;">Status</th>
											<th style="background: lightgray;">Marks Out Of</th>
											<th style="background: lightgray;">Accuracy</th>
											<th style="background: lightgray;">Rank</th>
										</tr>
									</thead>
									<tbody>
									   <?php $i=0; foreach ($data as $row): ?>
									   	
									   		<?php if (!empty($row['RESULT'])):  foreach ($row["RESULT"] as $row2): ?>
									   		<tr>
									   			<td><?php echo ++$i;?></td>
									   		    <td><?php echo $row['TS_NAME']; ?></td>
									   		    <td><?php echo $row['TP_NAME']; ?></td>
									   			<td><?php echo 'Attempted'; ?></td>
										   		<td><?php echo $row2['obtained_mark'].'/'.$row2['max_mark']; ?></td>
										   		<td><?php echo $row2['number_of_true_ques'].'/'.$row2['total_ques']; ?></td>
										   		<td><?php echo $row2['RANK']; ?></td>
										   	</tr>
									   		<?php endforeach; else: ?>
									   		<tr>
									   		    <td><?php echo ++$i;?></td>
									   		    <td><?php echo $row['TS_NAME']; ?></td>
									   		    <td><?php echo $row['TP_NAME']; ?></td>
									   			<td><?php echo 'UnAttempted'; ?></td>
										   		<td><?php echo '0/'.$row['MAX_MARK']; ?></td>
										   		<td><?php  ?></td>
										   		<td><?php  ?></td>
										   	</tr>
									   		<?php endif;?>
									   <?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<!-- end table-responsive -->
						</div>
						<!-- end panel-body -->
						
					</div>
				</div>
			</div>
			<!-- end row -->
			
		
		</div>
		<!-- end #content -->
		
<script type="text/javascript">
	$(document).ready(function() {
			$('#data-table-responsive').DataTable( {
			    responsive: true
			} );
	});
</script>
