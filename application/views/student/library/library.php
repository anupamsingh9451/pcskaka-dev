<link href="<?php echo base_url(); ?>assets/studentassets/css/testseries/testseries.css" rel="stylesheet" />
<!-- BOLT Sandbox/test //-->
<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="https://pcskaka.com/developer/assets/themeassets/img/user/user-13.jpg"></script>


<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style-responsive.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/default.css" rel="stylesheet" />
<style type="text/css">
	.checkbox.checkbox-css{
		padding-top: 0px;
	}
	.checkbox.checkbox-css label{
		padding-left: 19px;
	}
	.search-category-list>li{
		padding-left: 18px;
		background: white;
	}
	.widget.widget-stats{
		position: relative;
	    color: #000;
	    padding: 15px;
	    border-radius: 3px;
	    background-image: linear-gradient(rgb(255, 255, 255) 0%, rgba(255, 255, 255, 0.54) 46%, white 100%) !important;
	    min-height: 10px;
	}
	.widget-list-item .widget-list-media {
	    width: 35%;
	    height: 100%;
	}
 
	.carousel .carousel-inner, .carousel .carousel-inner .carousel-item, .carousel .carousel-inner .item, .slider .carousel {
        min-height: 100px;
	}
	@media (min-width: 576px) {
	    .card-columns {
	        column-count: 1;
	    }
	}

	@media (min-width: 768px) {
	    .card-columns {
	        column-count: 2;
	    }
	}

	@media (min-width: 992px) {
	    .card-columns {
	        column-count: 3;
	    }
	}

	@media (min-width: 1200px) {
	    .card-columns {
	        column-count: 3;
	    }
	}
	@media (min-width: 576px) { 
		#carouselExampleIndicators img {
			height:500px !important;
		}

	}

	@media (max-width: 576px) { 
		#carouselExampleIndicators img {
			height:300px !important;
		}

	}
</style>

<!-- BOLT Production/Live //-->
 <!--<script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="https://pcskaka.com/developer/assets/themeassets/img/user/user-13.jpg"></script>-->

<!-- begin #content --> 
<?php
function getCallbackUrlsuccess()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/successpayment';
}
function getCallbackUrlfailure()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/failurepayment';
}
?>

		<div id="content" class="content p-t-0">
			
			
			<!-- begin page-header -->
			<div class="search-toolbar sticky-top" style="top: 64px">
						<h4 style="font-size: 22px" class="pull-left">Exams</h4>
						<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right buyallcartproduct">
			</ol>
			<!-- end breadcrumb -->
			</div>
			<!-- end page-header -->

							
            
			<div class="row">
					<div class="col-lg-12 col-md-12">


										<div class="container mt-4">
								<div class="row">
									<div class="col-lg-12">
										<?php $popular_slider=$this->Studentgetmodel->get_all_suggested_sliders('4'); if (!empty($popular_slider)) {?>
                                        <div id="carouselExampleIndicators" class="carousel slide shadow" data-ride="carousel">
										  	<ol class="carousel-indicators">
										  	<?php $slider_inc=1; foreach ($popular_slider as $slider) { ?>
												<li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $slider_inc++; ?>" class="<?php if ($slider_inc==1) { echo "active"; }?>"></li>
											<?php } ?>
										  	</ol>
										  	<div class="carousel-inner">
										  	<?php $slider_inc=1; foreach ($popular_slider as $slider) { ?>
												<div class="carousel-item <?php if ($slider_inc==1) { echo "active"; }?>">
													<img class="d-block w-100 img-responsive " src="<?php echo base_url($slider->SLIDER_IMG); ?>" alt="<?php echo $slider_inc++ ?> slide">
												</div>
											<?php } ?>
										  	</div>
										  	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
												<span class="carousel-control-prev-icon" aria-hidden="true"></span>
												<span class="sr-only">Previous</span>
										  	</a>
										  	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
												<span class="carousel-control-next-icon" aria-hidden="true"></span>
												<span class="sr-only">Next</span>
										  	</a>
										</div>
										<?php }?>
                                    </div>
                                </div>
                                </div>


							<!--Start Container-->
							<div class="container mt-4">
								<h1 class="page-header f-w-700">Upcoming and Popular Exams</h1>
								<div class="row">
								<?php foreach ($upcoming_and_popular_exams as $exam): ?>
									<div class="col-lg-4 col-md-6 col-sm-4 col-xs-4">
			        						
			        						<div class=" widget  widget-list-item shadow" style="border-radius: 20px">
												<div class="widget-list-media p-6 p-r-10">
													<?php if(!empty($exam->LIB_IMG)){?>
														<img src="<?php echo base_url($exam->LIB_IMG); ?>" alt=""  style="width: 100px!important;height: 100px!important;border-radius: 25px;">
													<?php }else{?>
														<img src="<?php echo base_url(); ?>assets/themeassets/img/user/upsc-icon-10.jpg" alt=""  style="width: 100%;height: 100%;border-radius: 25px;">
													<?php }?>
												</div>
												<div class="widget-list-content">
													<h4 class="widget-list-title">
														<a href="<?php echo base_url('student/library_detail/'.$exam->LIB_ID); ?>" class=""><?php echo $exam->LIB_NAME; ?>
														</a>
													</h4>
													<p class="widget-list-desc">

														<?php
                                                        $library_data = [
                                                            "free_test_papers" => 0,
                                                            "paid_test_papers" => 0,
                                                            "free_quizzes"     => 0,
                                                            "paid_quizzes"     => 0
                                                        ];
                                                        $test_series_count = 0;
                                                        foreach ($this->Studentgetmodel->get_active_testseries_by_lib_id($exam->LIB_ID) as $row) {
                                                            $test_series_count += 1;
                                                            $test_papers = $this->Studentgetmodel->get_test_papers_by_tsid($row->TS_ID);
                                                            if (!empty($test_papers)) {
                                                                foreach ($test_papers as $test_paper) {
                                                                    if ($test_paper->TP_STATUS==1) {
                                                                        if ($test_paper->TP_PAY_STATUS==1) {  //Free
                                                                            if ($test_paper->TP_QUIZ == 1) { //Quiz
                                                                                $library_data["free_quizzes"] += 1;
                                                                            } else { //paper
                                                                                $library_data["free_test_papers"] += 1;
                                                                            }
                                                                        } else {   //Non Free
                                                                            if ($test_paper->TP_QUIZ == 1) { //Quiz
                                                                                $library_data["paid_quizzes"] += 1;
                                                                            } else { //paper
                                                                                $library_data["paid_test_papers"] += 1;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>

														Test Series <?php echo $test_series_count; ?> (Paid) <br>
														Test Papers <?php echo $library_data["paid_test_papers"]; ?> (Paid) <?php echo $library_data["free_test_papers"]; ?> (Free) <br>
														Quizzes <?php echo $library_data["paid_quizzes"]; ?> (Paid) <?php echo $library_data["free_quizzes"]; ?> (Free) <br>
														<br>
														<?php
                                                            $discount = $this->Studentgetmodel->get_discount_by_prod_type(1, 2, $exam->LIB_ID);
                                                            $totalamt=$discamt=0;
                                                            if (!empty($discount)) {
                                                                if ($discount[0]->COUPON_MODE==1) {
                                                                    $discamt = $discount[0]->COUPON_AMOUNT;
                                                                    $discstr = "₹";
                                                                } else {
                                                                    $discamt = ($exam->LIB_COST*$discount[0]->COUPON_AMOUNT)/100;
                                                                    $discstr = "%";
                                                                }
                                                                $totalamt = $exam->LIB_COST - $discamt;
                                                            } else {
                                                                $totalamt = $exam->LIB_COST;
                                                            }

                                                        ?>
														<span class="badge f-s-12">₹<?php echo $exam->LIB_COST; ?></span>
														<?php
                                                            $checkpayment = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 1, $exam->LIB_ID);
                                                            if (empty($checkpayment)) {
                                                                ?>
														<a 
															href="javascript:void(0);" 
															class="btn btn-warning btn-xs f-s-8 addtocart" 
															attr-prodid="<?php echo $exam->LIB_ID; ?>" 
															attr-prodtype="1" 
															attr-prodcost="<?php echo $exam->LIB_CONTENT_AMT; ?>"  
															attr-prodname="<?php echo $exam->LIB_NAME; ?>"   
															attr-discountamt="<?php echo $exam->LIB_CONTENT_DISC + $discamt; ?>" 
															attr-totalamt="<?php echo $totalamt; ?>"
														>
															<i class="fa fa-plus"></i> ADD TO CART
														</a>
													    <?php
                                                            } else { ?>
													    	<span class="badge badge-success f-s-12">Purchased</span>
													    <?php } ?>
														
													</p>
												</div>
											</div>
			        						
			        				</div>
			        			<?php endforeach; ?>

								</div>
							</div>
							<!--End Container-->

							<!--Start Container-->
							<div class="container mt-4">
								<h1 class="page-header f-w-700">Explore All Exams</h1>
								<!-- begin nav-pills -->
								<div class="panel panel-default panel-with-tabs shadow" data-sortable-id="ui-unlimited-tabs-1">
								<!-- begin panel-heading -->
								<div class="panel-heading p-0">
									<!-- begin nav-tabs -->
									<div class="tab-overflow">
										<ul class="nav nav-tabs">
											<li class="nav-item prev-button "><a href="javascript:;" data-click="prev-tab" class="nav-link text-success"><i class="fa fa-arrow-left"></i></a></li>

											<?php foreach ($this->Studentgetmodel->get_course_category() as $index => $cc_row) { ?>
											<li class="nav-items">
												<a href="<?php echo "#default-tab-".$cc_row->CC_ID; ?>" data-toggle="tab" class="nav-link <?php if ($index==0) {
                                                                echo 'active';
                                                            } ?>">
													<span class="d-sm-none"><?php echo $cc_row->CC_NAME; ?></span>
													<span class="d-sm-block d-none"><?php echo $cc_row->CC_NAME; ?></span>
												</a>
											</li>
											<?php } ?>
											<li class="nav-item next-button" style=""><a href="javascript:;" data-click="next-tab" class="nav-link text-success"><i class="fa fa-arrow-right"></i></a></li>
										</ul>
									</div>
								</div>
					<!-- end nav-tabs -->
					<!-- begin tab-content -->
					<div class="tab-content">
						<!-- begin tab-pane -->
						
					<?php foreach ($this->Studentgetmodel->get_course_category() as $index => $cc_row) { ?>
					<div class="tab-pane fade <?php if ($index==0) {
                                                                echo 'active';
                                                            } ?> show" id="<?php echo "default-tab-".$cc_row->CC_ID; ?>">
							<div class="card-columns">
								
									<?php foreach ($this->Studentgetmodel->get_course_by_cc_id($cc_row->CC_ID) as $course_row) { ?>
										<div class="card">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-12">
											<a href="" class="list-group-item no-border d-flex  align-items-center f-w-700 text-black "><?php echo $course_row->COURSE_NAME; ?> 
											<i class="fa fa-angle-right ml-2"></i>
											</a>

										</div>
										<?php foreach ($this->Studentgetmodel->get_library_by_course_id($course_row->COURSE_ID) as $lib_row) { ?>
											<div class="col-lg-12 col-md-12 col-12">

												

												<div class=" widget  widget-list-item shadow" style="border-radius: 20px;">
													<div class="widget-list-media p-6 p-r-10">
														<?php if(!empty($lib_row->LIB_IMG)){?>
															<img src="<?php echo base_url($exam->LIB_IMG); ?>" alt=""  style="width: 100px!important;height: 100!important;border-radius: 25px;">
														<?php }else{?>
															<img src="<?php echo base_url(); ?>assets/themeassets/img/user/upsc-icon-10.jpg" alt=""  style="width: 100%;height: 100%;border-radius: 25px;">
														<?php }?>
												   	</div>
													<div class="widget-list-content">
														<h4 class="widget-list-title">
															<a href="<?php echo base_url('student/library_detail/'.$lib_row->LIB_ID); ?>" class=""><?php echo $lib_row->LIB_NAME; ?>
															</a>
														</h4>
														<p class="widget-list-desc">

														<?php
                                                        $library_data = [
                                                            "free_test_papers" => 0,
                                                            "paid_test_papers" => 0,
                                                            "free_quizzes"     => 0,
                                                            "paid_quizzes"     => 0
                                                        ];
                                                        $test_series_count = 0;
                                                        foreach ($this->Studentgetmodel->get_active_testseries_by_lib_id($lib_row->LIB_ID) as $row) {
                                                            $test_series_count += 1;
                                                            $test_papers = $this->Studentgetmodel->get_test_papers_by_tsid($row->TS_ID);
                                                            if (!empty($test_papers)) {
                                                                foreach ($test_papers as $test_paper) {
                                                                    if ($test_paper->TP_STATUS==1) {
                                                                        if ($test_paper->TP_PAY_STATUS==1) {  //Free
                                                                            if ($test_paper->TP_QUIZ == 1) { //Quiz
                                                                                $library_data["free_quizzes"] += 1;
                                                                            } else { //paper
                                                                                $library_data["free_test_papers"] += 1;
                                                                            }
                                                                        } else {   //Non Free
                                                                            if ($test_paper->TP_QUIZ == 1) { //Quiz
                                                                                $library_data["paid_quizzes"] += 1;
                                                                            } else { //paper
                                                                                $library_data["paid_test_papers"] += 1;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>

														Test Series <?php echo $test_series_count; ?> (Paid) <br>
														Test Papers <?php echo $library_data["paid_test_papers"]; ?> (Paid) <?php echo $library_data["free_test_papers"]; ?> (Free) <br>
														Quizzes <?php echo $library_data["paid_quizzes"]; ?> (Paid) <?php echo $library_data["free_quizzes"]; ?> (Free) <br>
															<br>
															<?php
                                                                $discount = $this->Studentgetmodel->get_discount_by_prod_type(1, 2, $lib_row->LIB_ID);
                                                                $totalamt=$discamt=0;
                                                                if (!empty($discount)) {
                                                                    if ($discount[0]->COUPON_MODE==1) {
                                                                        $discamt = $discount[0]->COUPON_AMOUNT;
                                                                        $discstr = "₹";
                                                                    } else {
                                                                        $discamt = ($lib_row->LIB_COST*$discount[0]->COUPON_AMOUNT)/100;
                                                                        $discstr = "%";
                                                                    }
                                                                    $totalamt = $lib_row->LIB_COST - $discamt;
                                                                } else {
                                                                    $totalamt = $lib_row->LIB_COST;
                                                                }

                                                            ?>
															<span class="badge f-s-12">₹<?php echo $lib_row->LIB_COST; ?></span>
															<?php
                                                                $checkpayment = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 1, $lib_row->LIB_ID);
                                                                if (empty($checkpayment)) {
                                                                    ?>
										<input type="hidden" id="surl" name="surl" value="<?php echo getCallbackUrlsuccess(); ?>" />
				       				    <input type="hidden" id="furl" name="furl" value="<?php echo getCallbackUrlfailure(); ?>" />
															<a 
																href="javascript:void(0);" 
																class="btn btn-warning btn-xs f-s-8 addtocart" 
																attr-prodid="<?php echo $lib_row->LIB_ID; ?>" 
																attr-prodtype="1" 
																attr-prodcost="<?php echo $lib_row->LIB_CONTENT_AMT; ?>"  
																attr-prodname="<?php echo $lib_row->LIB_NAME; ?>"   
																attr-discountamt="<?php echo $lib_row->LIB_CONTENT_DISC + $discamt; ?>" 
																attr-totalamt="<?php echo $totalamt; ?>"
															>
																<i class="fa fa-plus"></i> ADD TO CART
															</a>
														    <?php
                                                                } else { ?>
														    	<span class="badge badge-success f-s-12">Purchased</span>
														    <?php } ?>
															
														</p>
													</div>
												</div>

											</div>
										<?php }?>
										 </div>
								</div>
									<?php } ?>
								   
							</div>
					</div>
					<?php } ?>
							
						<!-- end tab-pane -->
					</div>
					<!-- end tab-content -->
								</div>
					
							</div>
							<!--End Container-->

														<!--Start Container-->
							<div class="container mt-4">
								<h1 class="page-header f-w-700">Popular Courses</h1>
									<div class="row">
					<?php $i=0; foreach ($popularcourses as $course) { ?>

									<div class="col-lg-3 col-md-6 col-12">

										<div class="card border h-100 shadow" style="border-radius: 18px;">
										<?php if(!empty($course['IMAGE'])){?>
											<img class="card-img-top" style="border-top-right-radius: 18px;border-top-left-radius: 18px;" src="<?php echo base_url($course['IMAGE']); ?>" alt="Card image cap">
										<?php }?>
										  <div class="card-body">
										    <h5 class="card-title text-truncate width-full"><?php echo $course["NAME"]; ?></h5>
										    <p class="card-text">
										    	<?php foreach ($course["CONTENT_SUMMARY"] as $index => $row) { ?>
															<?php echo $row["count"]." ".$row["name"].'<br> '; ?>
													<?php }?>
										    	<?php if ($course["STATUS"]==0) {
                                                                    echo '<div class="discount blink_me coming_soon">Coming Soon!</div>';
                                                                } ?>
										    </p>
										  </div>
										</div>



									
									</div>
							<?php  } ?>
										</div>
							</div>
							<!--End Container-->

							<!--Start Container-->
							<div class="container mt-4">
								<h1 class="page-header f-w-700">Exams <?php echo date('Y'); ?> Calendar</h1>
								<div class="row">
									<div class="col-lg-12">
										
															<!-- begin #accordion -->
					<div id="accordion" class="card-accordion shadow">
						

						<?php $c=1; foreach ($current_year_exams as $month_year => $arr) { ?>
						<!-- begin card -->
						<div class="card">
							<div class="card-header text-white bg-secondary pointer-cursor" data-toggle="collapse" data-target="<?php echo "#collapse$c"; ?>">
								<?php echo $month_year; ?> <span class="badge badge-secondary"><?php echo count($arr); ?> Exams</span>
								<i class="fas fa-angle-down fa-lg pull-right"></i>
							</div>
							<div id="<?php echo "collapse$c"; ?>" class="collapse <?php echo $month_year==date("F Y")?'show':''; ?>" data-parent="#accordion">
								<div class="card-body" style="background: #f0f3f5;">
									<div class="row">
									<?php foreach ($arr as $exam) { ?>
										<div class="col-lg-4 col-md-6 col-12">

											<div class=" widget  widget-list-item shadow" style="border-radius: 20px;">
												<div class="widget-list-media p-6 p-r-10">
													<?php if(!empty($exam->LIB_IMG)){?>
														<img src="<?php echo base_url($exam->LIB_IMG); ?>" alt=""  style="width: 100%;height: 100%;border-radius: 25px;">
													<?php }else{?>
														<img src="<?php echo base_url(); ?>assets/themeassets/img/user/upsc-icon-10.jpg" alt=""  style="width: 100%;height: 100%;border-radius: 25px;">
													<?php }?>
												</div>
												<div class="widget-list-content">
													<h4 class="widget-list-title">
														<a href="<?php echo base_url('student/library_detail/'.$exam["LIB_ID"]); ?>" class=""><?php echo $exam["LIB_NAME"]; ?>
														</a>
													</h4>
													<p class="widget-list-desc">

														<?php
                                                        $library_data = [
                                                            "free_test_papers" => 0,
                                                            "paid_test_papers" => 0,
                                                            "free_quizzes"     => 0,
                                                            "paid_quizzes"     => 0
                                                        ];
                                                        $test_series_count = 0;
                                                        foreach ($this->Studentgetmodel->get_active_testseries_by_lib_id($exam["LIB_ID"]) as $row) {
                                                            $test_series_count += 1;
                                                            $test_papers = $this->Studentgetmodel->get_test_papers_by_tsid($row->TS_ID);
                                                            if (!empty($test_papers)) {
                                                                foreach ($test_papers as $test_paper) {
                                                                    if ($test_paper->TP_STATUS==1) {
                                                                        if ($test_paper->TP_PAY_STATUS==1) {  //Free
                                                                            if ($test_paper->TP_QUIZ == 1) { //Quiz
                                                                                $library_data["free_quizzes"] += 1;
                                                                            } else { //paper
                                                                                $library_data["free_test_papers"] += 1;
                                                                            }
                                                                        } else {   //Non Free
                                                                            if ($test_paper->TP_QUIZ == 1) { //Quiz
                                                                                $library_data["paid_quizzes"] += 1;
                                                                            } else { //paper
                                                                                $library_data["paid_test_papers"] += 1;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>

														Test Series <?php echo $test_series_count; ?> (Paid) <br>
														Test Papers <?php echo $library_data["paid_test_papers"]; ?> (Paid) <?php echo $library_data["free_test_papers"]; ?> (Free) <br>
														Quizzes <?php echo $library_data["paid_quizzes"]; ?> (Paid) <?php echo $library_data["free_quizzes"]; ?> (Free) <br>
															<br>

														<i class="fas fa-calendar-alt fa-sm"></i>
														<?php echo $exam["LIB_START_DATE"]; ?> - <?php echo $exam["LIB_END_DATE"]; ?>
													</p>
												</div>
												<div class="widget-list-action">
												</div>
											</div>

										</div>
									<?php }?>
									</div>
								</div>
							</div>
						</div>
						<!-- end card -->
								
						<?php $c++; }?>

					</div>
					<!-- end #accordion -->

									</div>
								</div>
							</div>
							<!--End Container-->
					</div>
			</div>	
				
			</div>
			<!-- end row -->
			
		</div>
		<!-- end #content -->
		
<!-- APPLY COUPON MODEL -->
<!-- <div class="modal fade" id="applycoupon">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">CHECKOUT</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
			    <div class="col-md-12 checkout_html_div">
			        
			    </div>
			</div>
			<div class="modal-footer">
				<button type="button "class="btn btn-success checkout">Checkout</a>
			</div>
		</div>
	</div>
</div> -->

<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
<link href="<?php echo base_url(); ?>assets/themeassets/css/default/invoice-print.min.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
<!-- #modal-without-animation -->
<div class="modal " id="modal-prucahseproduct">
	<div class="modal-dialog  modal-xl">
		<div class="modal-content">
			<div class="modal-header hidden-print">
				<h4 class="modal-title">Review Your Order</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body ">
				<!-- begin invoice -->
				<div class="invoice">
					<!-- begin invoice-company -->
					<div class="invoice-company">
						<span class="pull-right hidden-print">
							<a href="javascript:;" class="btn btn-sm btn-white m-b-10"><i class="fa fa-file-pdf t-plus-1 text-danger fa-fw fa-lg"></i> Export as PDF</a>
							<a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-white m-b-10"><i class="fa fa-print t-plus-1 fa-fw fa-lg"></i> Print</a>
						</span>
						PCSKAKA
					</div>
					<!-- end invoice-company -->
					<!-- begin invoice-header -->
					<div class="invoice-header">
						<div class="invoice-from">
							<small>from</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse">PCSKAKA, Inc.</strong><br />
								Phone: +91 945-273-6000<br />
								Email: support@pcskaka.com
							</address>
						</div>
						<div class="invoice-to">
							<small>to</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse"><span class="name"></span></strong><br />
								<span class="student_address_html"></span></br>
								Phone: <span class="student_mobileno"></span><br />
								Email: <span class="student_name_upper"></span>
							</address>
						</div>
						<div class="invoice-date">
							<small>Invoice / <?php echo date('F');?> period</small>
							<div class="date text-inverse m-t-5"><?php echo date('d-F-Y'); ?></div>
							<div class="invoice-detail">
								#0000123DSS<br />
								Services Product
							</div>
						</div>
					</div>
					<!-- end invoice-header -->
					<!-- begin invoice-content -->
					<div class="invoice-content">
						<!-- begin table-responsive -->
						<div class="table-responsive ">
							<table class="table table-invoice buycarttable">
								<thead>
									<tr>
										<th class="text-nowrap"> PRODUCT DESCRIPTION</th>
										<th class="text-right" width="5%">PRICE</th>
										<th class="text-right" width="5%">DISCOUNT</th>
										<th class="text-right" width="5%">TOTAL</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot></tfoot>
							</table>
						</div>
						<!-- end table-responsive -->
						<!-- begin invoice-price -->
						<div class="invoice-price">
							<div class="invoice-price-left bg-white">
								<div class="invoice-price-row hide">
									<div class="sub-price">
										<small>SUBTOTAL</small>
										<span class="text-inverse subtotal"></span>
									</div>
								</div>
							</div>
							<div class="invoice-price-right totalpay">
								<small>TOTAL</small> <span class="f-w-600"></span>
							</div>
						</div>
						<!-- end invoice-price -->
					</div>
					<!-- end invoice-content -->
					<div class="text-right">
						<a href="javascript:;" class="btn btn-primary checkout">Pay Now</a>
					</div>
					<!-- begin invoice-note -->
					<div class="invoice-note">
						* Make all cheques payable to [Your Company Name]<br />
						* Payment is due within 30 days<br />
						* If you have any questions concerning this invoice, contact  [Name, Phone Number, Email]
					</div>
					<!-- end invoice-note -->
					<!-- begin invoice-footer -->
					<div class="invoice-footer">
						<p class="text-center m-b-5 f-w-600">
							THANK YOU FOR YOUR BUSINESS
						</p>
						<p class="text-center">
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-globe"></i> matiasgallipoli.com</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-phone-volume"></i> T:016-18192302</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-envelope"></i> rtiemps@gmail.com</span>
						</p>
					</div>
					<!-- end invoice-footer -->
				</div>
				<!-- end invoice -->
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<div class="hide singleproduct">
	<input type="hidden" class="product-id">
	<input type="hidden" class="product-type">
	<input type="hidden" class="couponcode">
</div>
		
		
		