<script>


function get_testpaper_result(){
    $.ajax({
		type: 'GET',
		url: base_loc + 'studentgetajax/get_testpaper_result',
		data:'tpid=' + <?php echo "'".$this->uri->segment(3)."'"; ?>,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
		    console.log(msg);
			if(msg.status==200 && msg.message=='ok'){
			    $('.score').html(msg.data.obtained_mark);
			    $('.total_ques').html(' / '+msg.data.total_ques);
			    $('.attempted').html(msg.data.attempted);
			    $('.percentage').html(msg.data.percentage+' %');
			    $('.max_mark').html(' / '+msg.data.max_mark);
			}else{
			 //   location.href = base_loc+"student/dashboard";
			}
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc; 
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc; 
			}
		}
	});
}
</script>