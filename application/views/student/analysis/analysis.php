<!-- begin #content -->
<div id="content" class="content">
	<div class="container p-30">
		<div class="section-body">
			<div class="row">
				<div class="col-md-12">
    			    <div class="card">
						<div class="card-block">
							<div class="row">
							    <div class="col-md-3">
							        <li class="media media-sm">
    									<a class="media-right" href="javascript:;">
    										<img src="https://cdn0.iconfinder.com/data/icons/business-2-41/129/166-512.png" alt="" class="media-object rounded-corner">
    									</a>
    									<div class="media-body p-10">
                                                <h4 class="">
                                                    <span class="rank"><?php echo rand(200, 1000); ?></span> <span class="help-label ng-binding max_mark">/ 1343</span>
                                                </h4>
                                                <div class="help-text">Rank</div>
                                            
    									</div>
    								</li>
							    </div>
							    <div class="col-md-3">
							        <li class="media media-sm">
    									<a class="media-right" href="javascript:;">
    										<img src="https://www.clipartwiki.com/clipimg/detail/105-1058199_award-transparent-ribbon-clipart-ribbon-award-logo-png.png" alt="" class="media-object rounded-corner">
    									</a>
    									<div class="media-body p-10">
    										
                                                <h4 class="">
                                                     <span class="score"><?php echo $result['obtained_mark']; ?></span> <span class="help-label ng-binding"> / <?php echo $result['max_mark']; ?></span>
                                                </h4>
                                                <div class="help-text">Score</div>
                                            
    									</div>
    								</li>
							    </div>
							    <div class="col-md-3">
							        <li class="media media-sm">
    									<a class="media-right" href="javascript:;">
    										<img src="https://st4.depositphotos.com/20838724/24590/v/1600/depositphotos_245906616-stock-illustration-text-book-pen-filled-outline.jpg" alt="" class="media-object rounded-corner">
    									</a>
    									<div class="media-body p-10">
    										
                                                <h4 class="">
                                                     <span class="attempted"><?php echo $result['attempted']; ?> </span> <span class="help-label ng-binding total_ques">/ <?php echo $result['total_ques']; ?></span>
                                                </h4>
                                                <div class="help-text">Attempted</div>
                                            
    									</div>
    								</li>
							    </div>
							    <div class="col-md-3">
							        <li class="media media-sm">
    									<a class="media-right" href="javascript:;">
    										<img src="https://cdn4.vectorstock.com/i/1000x1000/25/73/gold-percentage-logo-vector-4482573.jpg" alt="" class="media-object rounded-corner">
    									</a>
    									<div class="media-body p-10">
    										
                                                <h4 class="">
                                                     <span class="percentage"><?php echo $result['percentage']; ?> %</span>
                                                </h4>
                                                <div class="help-text">Percentage</div>
                                            
    									</div>
    								</li>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- begin row -->
			<div class="row">
				
				<div class="col-md-12 m-t-30">
				    <div class="panel panel-inverse">
						<div class="panel-heading">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Test Paper Result</h4>
						</div>
						<div class="panel-body">
							<div id="testpaper-analysis-chart"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- end row -->
		</div>
		<div style="text-align:center;">
	        <a href="<?php echo base_url(); ?>student/testseries" class="btn btn-success m-r-5 m-b-5"> Go to Test series</a>
	    </div>
	</div>
	    
</div>
<!-- end #content -->