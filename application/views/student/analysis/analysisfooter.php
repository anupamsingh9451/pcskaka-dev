
        
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
		<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/js-cookie/js.cookie.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/theme/default.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/apps.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/studentassets/js/studentget.js"></script>
	<script src="<?php echo base_url(); ?>assets/studentassets/js/studentpost.js"></script>
	<!--<script src="<?php echo base_url(); ?>assets/studentassets/js/studentcommon.js"></script>-->
	<script src="<?php echo base_url(); ?>assets/studentassets/js/studentvalidation.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/d3/d3.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/nvd3/build/nv.d3.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/demo/chart-d3.demo.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/apexcharts@3.0.0"></script>
	<script>
	var options = {
            chart: {
            width: '100%',
            height: 350,
                type: 'bar'
            },
            plotOptions: {
                bar: {
                    barHeight: '100%',
                    distributed: true,
                    horizontal: true,
                    dataLabels: {
                        position: 'bottom'
                    },
                }
            },
            colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e', '#f48024', '#69d2e7'],
            dataLabels: {
                enabled: true,
                textAnchor: 'start',
                style: {
                    colors: ['#000000']
                },
                formatter: function(val, opt) {
                    return val
                },
                offsetX: 0,
                dropShadow: {
                  enabled: true
                }
            },
            series: [{
               data: [<?php echo $result['total_ques']; ?>, <?php echo $result['attempted']; ?>, <?php echo $result['attempt_boomark']; ?>, <?php echo $result['not_visited']; ?>, <?php echo $result['skipped']; ?>, <?php echo $result['number_of_true_ques']; ?>, <?php echo $result['number_of_wrong_ques']; ?>]
               
            }],
            stroke: {
                // width: 1,
              colors: ['#fff']
            },
            xaxis: {
                categories: ['Total Ques', 'Attempted Ques', 'Attempted Marked', 'Not Visited', 'Skipped Ques', 'True Ques', 'Wrong Ques'],
            },
            yaxis: {
                labels: {
                    show: true
                },
                // max:210
                
            },
            title: {
                text: 'Test Paper Result',
                align: 'center',
                floating: true
            },
            tooltip: {
                theme: 'dark',
                x: {
                    show: false
                },
                y: {
                    title: {
                        formatter: function() {
                            return ''
                        }
                    }
                }
            }
        }
       var chart = new ApexCharts(
            document.querySelector("#testpaper-analysis-chart"),
            options
        );
        chart.render();
        handleBarChart=function(){
            "use strict";
            var e=[
                {
                    key:"Cumulative Return",
                    values:[
                        {label:"Total Ques",value:<?php echo $result['total_ques']; ?>,color:COLOR_RED},
                        {label:"Attempted Ques",value:<?php echo $result['attempted']; ?>,color:COLOR_ORANGE},
                        {label:"Attempted Marked",value:<?php echo $result['attempt_boomark']; ?>,color:COLOR_GREEN},
                        {label:"Not Visited",value:<?php echo $result['not_visited']; ?>,color:COLOR_YELLOW},
                        {label:"Skipped Ques",value:<?php echo $result['skipped']; ?>,color:COLOR_BLUE},
                        {label:"True Ques",value:<?php echo $result['number_of_true_ques']; ?>,color:COLOR_PURPLE},
                        {label:"Wrong Ques",value:<?php echo $result['number_of_wrong_ques']; ?>,color:COLOR_BLACK},
                        
                    ]
                }
            ];
            nv.addGraph(function(){
                var a=nv.models.discreteBarChart().x(function(e){
                    return e.label
                }).y(function(e){
                    return e.value
                }).showValues(!0).duration(250);
                return a.yAxis.axisLabel("Number Of Question"),
                a.xAxis.axisLabel("Result"),
                
                d3.select("#nv-bar-chart").append("svg").datum(e).call(a),
                nv.utils.windowResize(a.update),
                a
            })
            
        },
        handlePieAndDonutChart=function(){
            "use strict";
            var e=[
                {label:"One",value:29,color:COLOR_RED},
                {label:"Two",value:12,color:COLOR_ORANGE},
                {label:"Three",value:32,color:COLOR_GREEN},
                {label:"Four",value:196,color:COLOR_AQUA},
                {label:"Five",value:17,color:COLOR_BLUE},
                {label:"Six",value:98,color:COLOR_PURPLE},
                {label:"Seven",value:13,color:COLOR_GREY},
                {label:"Eight",value:5,color:COLOR_BLACK}
            ];
            nv.addGraph(function(){
                var a=nv.models.pieChart().x(function(e){
                    return e.label
                }).y(function(e){
                    return e.value
                }).showLabels(!0).labelThreshold(.05);
                return d3.select("#nv-pie-chart").append("svg").datum(e).transition().duration(350).call(a),
            a}),
            nv.addGraph(function(){
                var a=nv.models.pieChart().x(function(e){
                    return e.label
                }).y(function(e){
                    return e.value
                }).showLabels(!0).labelThreshold(.05).labelType("percent").donut(!0).donutRatio(.35);
                return d3.select("#nv-donut-chart").append("svg").datum(e).transition().duration(350).call(a),
                a
            })
        },
        ChartNvd3=function(){
            "use strict";
            return{
                init:function(){
                    handleBarChart(),
                    handlePieAndDonutChart()
                }
            }
        }();
</script>
	<script>
		$(document).ready(function() {
			App.init();
			ChartNvd3.init();
		});
	</script>
</body>

</html>
