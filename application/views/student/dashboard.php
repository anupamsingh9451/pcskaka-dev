<link href="<?php echo base_url(); ?>assets/studentassets/css/dashboard.css" rel="stylesheet" />
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active"><a href="javascript:;">Performance</a></li>
				<!--<li class="breadcrumb-item active">Dashboard v2</li>-->
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Performance </h1>
			<!-- end page-header -->
			
			
			<div class="row">
			    <div class="col-lg-4 col-md-6">
					<div class="widget widget-stats bg-gradient-teal">
						<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
						<div class="stats-content">
							<div class="stats-title">Total Test Series / My Purchase</div>
							<div class="stats-number"><?php echo $this->Studentgetmodel->count_all_active_tetseries()." / ".$this->Studentgetmodel->get_count_purchase_ts(); ?></div>
							<div class="stats-progress progress">
								<div class="progress-bar" style="width: 70.1%;"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="widget widget-stats bg-gradient-blue">
						<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
						<div class="stats-content">
							<div class="stats-title">Progress</div>
							<div class="stats-number"><?php $dt = $this->Studentgetmodel->get_student_progress(); if (!empty($dt)) {
    echo $dt['TOTAL_TESTPAPER']." / ".$dt['ATTEMPTED_TESTPAPER'];
} else {
    echo 0 / 0;
} ?></div>
							<div class="progress rounded-corner">
								<div class="progress-bar bg-info progress-bar-striped" style="width: <?php if (!empty($dt)) {
    echo $dt['PERCENT'];
} else {
    echo 0;
} ?>%;justify-content: flex-end;"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="widget widget-stats bg-gradient-purple">
						<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
						<div class="stats-content">
							<div class="stats-title">Accuracy</div>
							<div class="stats-number"><?php $acc = $this->Studentgetmodel->get_student_accuracy();  if (!empty($acc)) {
    echo $acc['accuracy'];
} else {
    echo 0;
} ?></div>
							<div class="stats-progress progress">
								<div class="progress-bar" style="width: 70.1%;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- begin row -->
			<div class="row">
			    <div class="col-md-12">
				    <div class="panel panel-inverse" data-sortable-id="table-basic-9">
						<!-- begin panel-heading -->
						<div class="panel-heading ui-sortable-handle">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Performance</h4>
						</div>
						<!-- end panel-heading -->
						<!-- begin panel-body -->
						<div class="panel-body">
							<!-- begin table-responsive -->
							<div class="table-responsive">
								<table class="table m-b-0" style="border: 1px solid #cecece;">
									<thead>
										<tr>
											<th>#</th>
											<th>Test Paper Name</th>
											<th>Total Ques</th>
											<th>Attempted Ques</th>
											<th>Marked Ques</th>
											<th>Skipped Ques</th>
											<th>Not Visited Ques</th>
											<th>True Ques</th>
											<th>Wrong Ques</th>
											<th>Obtained marks</th>
											<th>Rank</th>
											<th>Tools</th>
										</tr>
									</thead>
									<tbody>
									    <?php if (!empty($result['data'])) {
    $class = array("active","info","success","warning","danger");
    $i = 1;
    $categories = array();
    $data2 = array();
    $morris_e = array();
    foreach ($result['data'] as $results) {
        shuffle($class);
        $categories[] = $results['TP_NAME'];
        $data2[] = $results['RESULT']['RANK'];
        $morris_e[] = array("period"=>$results['TP_NAME'],"student"=>$results['RESULT']['RANK']); ?>
    										<tr class="<?php echo $class[0]; ?>">
    											<td><?php echo $i; ?></td>
    											<td><?php echo $results['TP_NAME']; ?></td>
    											<td><?php echo $results['RESULT']['total_ques']; ?></td>
    											<td><?php echo $results['RESULT']['attempted']; ?></td>
    											<td><?php echo $results['RESULT']['boomark']; ?></td>
    											<td><?php echo $results['RESULT']['skipped']; ?></td>
    											<td><?php echo $results['RESULT']['not_visited']; ?></td>
    											<td><?php echo $results['RESULT']['number_of_true_ques']; ?></td>
    											<td><?php echo $results['RESULT']['number_of_wrong_ques']; ?></td>
    											<td><?php echo $results['RESULT']['obtained_mark']; ?></td>
    											<td><?php echo $results['RESULT']['RANK']; ?></td>
    											<td style="padding: 6px 10px !important;"><a href="<?php echo base_url(); ?>test/solution/<?php echo $results['TP_ID']; ?>" class="btn btn-success" style="padding: 3px 8px 3px 8px;"> Solution</a></td>
    											
    										</tr>
										<?php $i++;
    }
} else { ?>
    										<tr class="info" style="text-align:center;">
    											<td colspan="12">You Have Not attempted any Test Paper Yet!</td>
    											
    										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<!-- end table-responsive -->
						</div>
						<!-- end panel-body -->
						
					</div>
				</div>
			</div>
			<!-- end row -->
			<!-- begin row -->
			<div class="row">
				<!-- begin col-8 -->
				<div class="col-lg-12">
					<div class="panel panel-inverse" data-sortable-id="morris-chart-1">
						<div class="panel-heading">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Marks Distribution Chart</h4>
						</div>
						<div class="panel-body">
							<h4 class="text-center">Marks Distribution</h4>
							<div id="morris-line-chart" class="height-sm"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- end row -->
			<!-- begin row -->
			<div class="row">
				<div class="col-md-6">
				    <div class="panel panel-inverse">
						<div class="panel-heading">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Test Paper Result</h4>
						</div>
						<div class="panel-body">
							<div id="testpaper-result-chart"></div>
						</div>
					</div>
				</div>
			    <div class="col-md-6">
					<!-- begin panel -->
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Attempted Test paper Result</h4>
						</div>
						<div class="panel-body">
						    
							<div id="attempted_tp_barcahrt" class="height-sm"></div>
						</div>
					</div>
					<!-- end panel -->
				</div>
				
			</div>
			<!-- end row -->					
			
			
			
		
		</div>
		<!-- end #content -->
		
<script>
   
        
var handleBarChart=function(){
    "use strict";
    var e=[
        {
            key:"Cumulative Return",
            values:[
                <?php $acc = $this->Studentgetmodel->get_attempted_testpaperdt(); foreach ($acc['data'] as $accs) { ?>
                    <?php echo "{label:'".$accs['TP_NAME']."',value:'".$accs['percentage']."'},"; ?>
                <?php } ?>
            ]
        }
    ];
    nv.addGraph(function(){
        
        
           
        var a=nv.models.discreteBarChart();
        
        a.x(function(e){return e.label}).y(function(e){return e.value}).showValues(!0).duration(250);
        a.forceY([0,100]);
        return a.yAxis.axisLabel("Percent (%)"),
        a.xAxis.axisLabel("Test Paper"),
        d3.select("#attempted_tp_barcahrt").append("svg").datum(e).call(a),
        
        nv.utils.windowResize(a.update),
        
       
    a})
},ChartNvd3=function(){
    "use strict";
    return{init:function(){
        handleBarChart()
    }}
}();
var options = {
	chart: {
	width: '100%',
	height: 500,
		type: 'bar'
	},
	plotOptions: {
		bar: {
			barHeight: '100%',
			distributed: true,
			horizontal: true,
			dataLabels: {
				position: 'bottom'
			},
		}
	},
	colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e', '#f48024', '#69d2e7'],
	dataLabels: {
		enabled: true,
		textAnchor: 'start',
		style: {
			colors: ['#fff']
		},
		formatter: function(val, opt) {
			return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
		},
		offsetX: 0,
		dropShadow: {
			enabled: true
		}
	},
	series: [{
		data: JSON.parse('<?php echo json_encode($data2); ?>')
		
	}],
	stroke: {
		width: 1,
		colors: ['#fff']
	},
	xaxis: {
		categories: JSON.parse('<?php echo json_encode($categories); ?>'),
	},
	yaxis: {
		labels: {
			show: false
		},
		// max:210
		
	},
	title: {
		text: 'Test Paper Result',
		align: 'center',
		floating: true
	},
	// subtitle: {
	//     text: 'Category Names as DataLabels inside bars',
	//     align: 'center',
	// },
	tooltip: {
		theme: 'dark',
		x: {
			show: false
		},
		y: {
			title: {
				formatter: function() {
					return ''
				}
			}
		}
	}
}
var chart = new ApexCharts(
	document.querySelector("#testpaper-result-chart"),
	options
);
chart.render();
var handleMorrisLineChart=function(){
	var e= JSON.parse('<?php echo json_encode($morris_e); ?>');
	Morris.Line({
		parseTime: false,
		element:"morris-line-chart",
		data:e,
		
		xkey:"period",
		ykeys:["Student"],
		labels:["Mark"],
		
		hoverCallback: function(index, options, content, row) {
			console.log(row);
			var hover = "<div class='morris-hover-row-label'>"+row.period+"</div><div class='morris-hover-point' style='color: #A4ADD3'><p color:black>"+content+"</p></div>";
				return hover;
		},
		xLabelFormat: function(x) { return ''; },
		resize:!0,
		pointSize:5,
		lineWidth:2.0,
		gridLineColor:[COLOR_GREY_LIGHTER],
		gridTextFamily:FONT_FAMILY,
		gridTextColor:FONT_COLOR,
		gridTextWeight:FONT_WEIGHT,
		gridTextSize:FONT_SIZE,
		lineColors:[COLOR_GREEN,COLOR_BLUE]
	})
},MorrisChart=function(){
	"use strict";
	return{
		init:function(){
			handleMorrisLineChart()
		}
	}
}();
$(document).ready(function() {
	ChartNvd3.init();
	MorrisChart.init();
});
</script>
