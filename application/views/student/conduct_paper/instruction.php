<link href="<?php echo base_url(); ?>assets/studentassets/css/conduct_paper/instruction.css" rel="stylesheet" />
<!-- begin #content -->
<div id="content" style="padding: 0;" class="content instruction_content_div">
    <div class="p-5">
        <div class="row" style="">

        <div class="col-md-12 instruction_first">
            <div class="instruction_content cont1">
                <div class="section-header m-t-10">
            		<h4 style="font-size:18px;font-weight:500;">General Instructions: </h4>
            	</div>
                <ol start="1">
                    <li>
                        <p>The clock will be set at the server. The countdown timer at the top right corner of screen will display the remaining time available for you to complete the examination. When the timer reaches zero, the examination will end by itself. You need not terminate the examination or submit your paper.</p>
                    </li>
                    <li>
                        <p>The Question Palette displayed on the right side of screen will show the status of each question using one of the following symbols:</p>
                        <ul class="instruction_ul" style="">
                            <li>
                                <span class="label"></span> You have not visited the question yet.
                            </li>
                            <li>
                                <span class="label skipped"></span> You have not answered the question.</li>
                            <li>
                                <span class="label attempted"></span> You have answered the question.
                            </li>
                            <li>
                                <span class="label bookmarked"></span> You have NOT answered the question, but have marked the question for review.
                            </li>
                            <li>
                                <span class="label attempted bookmarked"></span> You have answered the question, but marked it for review.
                            </li>
                        </ul>
                    </li>
                </ol>
                <p>The <b>Mark For Review</b> status for a question simply indicates that you would like to look at that question again. If a question is answered, but marked for review, then the answer will be considered for evaluation unless the status is modified by the candidate.</p>
                <p><b>Navigating to a Question :</b></p>
                <ol start="3">
                    <li>
                        <p>To answer a question, do the following:</p>
                <ol>
                <li>Click on the question number in the Question Palette at the right of your screen to go to that numbered question directly. Note that using this option does NOT save your answer to the current question.</li>
                <li>Click on <b>Save &amp; Next</b> to save your answer for the current question and then go to the next question.</li><li>Click on <b>Mark for Review <span class="hide-on-railway">&amp; Next</span></b> to save your answer for the current question and also mark it for review <span class="hide-on-railway">, and then go to the next question.</span></li></ol></li></ol>
                <p>Note that your answer for the current question will not be saved, if you navigate to another question directly by clicking on a question number <span>without saving</span> the answer to the previous question.</p>
                <p>You can view all the questions by clicking on the <b>Question Paper</b> button. <span style="color:#ff0000">This feature is provided, so that if you want you can just see the entire question paper at a glance.</span></p>
            
                <h4 style="font-size:18px;font-weight:500;">Answering a Question :</h4>
                <ol start="4">
                    <li>
                        <p>Procedure for answering a multiple choice (MCQ) type question:</p>
                        <ol>
                            <li>
                                Choose one answer from the 4 options (A,B,C,D) given below the question
                                <span class="hide-on-railway">, click on the bubble placed before the chosen option.</span>
                            </li>
                            <li class="hide-on-railway">
                                To deselect your chosen answer, click on the bubble of the chosen option again or click on the 
                                <b><span class="hide-on-railway">Clear Response</span> <span class="show-on-railway">Erase Answer</span></b> button
                            </li>
                            <li>
                                To change your chosen answer, click on the bubble of another option.
                            </li>
                            <li>
                                To save your answer, you MUST click on the <b>Save &amp; Next</b>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <p>Procedure for answering a numerical answer type question :</p>
                        <ol>
                            <li>
                                To enter a number as your answer, use the virtual numerical keypad.
                            </li>
                            <li>
                                A fraction (e.g. -0.3 or -.3) can be entered as an answer with or without "0" before the decimal point. 
                                <span style="color: red">As many as four decimal points, e.g. 12.5435 or 0.003 or -932.6711 or 12.82 can be entered.</span>
                            </li>
                            <li>
                                To clear your answer, click on the <b>Clear Response</b> button
                            </li>
                            <li>
                                To save your answer, you MUST click on the <b>Save &amp; Next</b>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <p>
                            To mark a question for review, click on the 
                            <b>Mark for Review 
                                <span class="hide-on-railway">&amp; Next</span>
                            </b> button. If an answer is selected (for MCQ) entered (for numerival answer type) for a question that is 
                            <b>
                                Marked for Review
                            </b> , 
                            that answer will be considered in the evaluation unless the status is modified by the candidate.
                        </p>
                    </li>
                    <li>
                        <p>
                            To change your answer to a question that has already been answered, first select that question for answering and then follow the procedure for answering that type of question.
                        </p>
                    </li>
                    <li>
                        <p>
                            Note that ONLY Questions for which answers are <b>saved</b> or <b>marked for review after answering</b> will be considered for evaluation.
                        </p>
                    </li>
                    <li>
                        <p>
                            Sections in this question paper are displayed on the top bar of the screen. Questions in a Section can be viewed by clicking on the name of that Section. The Section you are currently viewing will be highlighted.
                        </p>
                    </li>
                    <li>
                        <p>
                            After clicking the <b>Save &amp; Next</b> button for the last question in a Section, you will automatically be taken to the first question of the next Section in sequence.
                        </p>
                    </li>
                    <li>
                        <p>
                            You can move the mouse cursor over the name of a Section to view the answering status for that Section.
                        </p>
                    </li>
                </ol>
            </div>
            <div class="instruction_footer myfoot1">
                <!--<span style="color:#00acac;"><i class="fas fa-arrow-left fa-fw m-t-14" aria-hidden="true"></i>Go to Test Paper</span>-->
                <a href="<?php echo base_url().'student/testpaper/'.$tpdts[0]->TS_ID; ?>" style="" class="btn btn-success pull-left"> <i class="fas fa-arrow-left fa-fw m-t-14" aria-hidden="true"></i> Go to Test Paper</a>
                &nbsp;&nbsp;<button type="button" class="btn btn-success next_instructss"> NEXT</button>
            </div>
        </div>
        <div class="col-md-12 instruction_second" style="display:none;">
            <div class="instruction_content cont2">
                <div class="section-header" style="text-align: center;padding: 30px;font-size:35px;">
            		<h3 style="">General Instructions:</h3>
            	</div>
            	<div class="row">
            	    
            	    
                	<div  class="col-md-12">
                	     <span class="pull-left">Duration : <?php echo $tsdts[0]->TS_DURATION; ?> Min</span>
                	    <span class="pull-right">Maximum Marks : <?php echo ceil($tsdts[0]->TS_P_MARKS*$tsdts[0]->TS_QUESTION_NOS); ?></span>
                	</div>
                	<div class="col-md-12 m-t-10">
                	    <p><b>Read Following Instructions carefully :</b></p>
                	</div>
                	<div class="col-md-12">
                        <ol>
                            <li style="font-family:arial">
                                This test comprises multiple-choice questions (MCQs).
                            </li>
                            <li style="font-family:arial">
                                Each question will have only 1 of the available options as the correct answer.
                            </li>
                            <li style="font-family:arial">
                                <?php echo $tsdts[0]->TS_P_MARKS; ?> mark(s) will be awarded for each correct answer and <?php echo $tsdts[0]->TS_N_MARKS; ?> mark(s) will be deducted for every wrong answer.
                            </li>
                            <li style="font-family:arial">
                                You are advised not to close the browser window before submitting the test.
                            </li>
                            <li style="font-family:arial">
                                In case the test does not load completely or becomes un-responsive, click on browser's refresh button to reload.
                            </li>
                            <li style="font-family:arial">
                                You can write this test only once, so for best results do not try to guess answers.
                            </li>
                        </ol>
                    </div>
                    <div class="col-md-12">
                        <p>
                            <b>Declaration :</b>
                        </p>
                        <label class="declaration-label"><input type="checkbox" class="declaration-checkbox" name="" > 
                            I have read all the instructions carefully and have understood them. I agree not to cheat or use unfair means in this examination. I understand that using unfair means of any sort for my own or someone else’s advantage will lead to my immediate disqualification. The decision of pcskaka.com will be final in these matters and cannot be appealed.
                        </label>
                    </div>
                </div>
            </div>
            <div class="instruction_footer myfoot2" style="">
                <div class="col-md-12">
                    <a href="javascript:void(0);" style="" class="btn btn-success previous_instruct"> <i class="fas fa-arrow-left fa-fw m-t-14" aria-hidden="true"></i>Previous</a>
                    <!--<a href="<?php echo base_url().'test/start_test/'.url_encode($tpdts[0]->TP_ID); ?>" style="" class="btn btn-success pull-right declare_instruction disabled" disabled="disabled"> I am ready to begun</a>-->
                    &nbsp;&nbsp;&nbsp;
                    <a href="javascript:void(0);" style="" class="btn btn-success declare_instruction disabled" disabled="disabled"> I am ready to begun</a>
                </div>
            </div>
        </div>
       
    </div>
	</div>
</div>
    	

<script>
		$(document).ready(function() {
		     var windowHeight = $(window).height();
		     var headerHeight = $('#header').height();
		     var instruction_footer_height = $('.myfoot1').height();
		     var instruction2_footer_height = $('.myfoot2').height();
	    	 $('.cont1').css("max-height", windowHeight-headerHeight-instruction_footer_height-10);
	    	 $('.cont2').css("max-height", windowHeight-headerHeight-instruction2_footer_height-55);
	    	 $('.cont2').css("min-height", windowHeight-headerHeight-instruction2_footer_height-55);
		});
	</script>