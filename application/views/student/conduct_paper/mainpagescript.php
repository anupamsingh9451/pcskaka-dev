<script>
var interval = null;


$(document).ready(function() {
	    var tpid = $('.uri').val();
	    if(localStorage.getItem("duration_"+tpid)===null ){
	        get_instruction_contect();
	    }else{
	        get_start_test_paper_html();
	    }
	    
        $('body').on('click','.declare_instruction',function(){
	         get_start_test_paper_html();
	     });
	});
function get_instruction_contect(){
    $.ajax({
		type: 'GET',
		url: base_loc + 'studentgetajax/get_instruction_html',
		data:'tpid=' + <?php echo "'".$this->uri->segment(3)."'"; ?>,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
			$('.main_page_div').html(msg);
		
// 			$('.theme-collapse-btn').trigger('click');
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc;
			}
		}
	});
}
   
function get_start_test_paper_html(){
    var windowHeight = $(window).height();
    var headerHeight = $('#header').height();

        $.ajax({
    		type: 'GET',
    		url: base_loc + 'studentgetajax/get_start_test_paper_html',
    		data:'tpid=' + <?php echo "'".$this->uri->segment(3)."'"; ?>,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.main_page_div').html(msg);
    			$('.theme-collapse-btn').trigger('click');
    			var instruction_footer_height = $('.footer_div').height();
                $('.main_page_div').css("max-height", windowHeight-headerHeight-instruction_footer_height-25);
    		    $('.main_page_div').css("min-height", windowHeight-headerHeight-instruction_footer_height-25);
    		    $('.main_page_div').css("overflow", "auto");
    			$.ajax({
            		type: 'GET',
            		url: base_loc + 'test/get_cookies',
            		data:'tpid=' + <?php echo "'".$this->uri->segment(3)."'"; ?>,
            		dataType:"json",
            		headers: {
            			'Client-Service': clientserv,
            			'Auth-Key': apikey,
            			'User-ID': loginid,
            			'Authorization': token,
            			'type': type
            		},
            		success: function (msg) {
            		    
            		    $.each(msg,function(index,element){
            		       $('.ques-icon-li[attr-zwdqi="'+element.quesid+'"]').find('.ques-icons').addClass(element.quesclass);
            		       if(element.ques_ans!=0){
            		           $('.questions[attr-zwdqi="'+element.quesid+'"]').find('.form-check-input[attr-opt-srno="'+element.ques_ans+'"]').prop('checked',true);
            		       }
            		    });
            		 
            		    fullcalc(2);
            		    
            		    if(localStorage.getItem("duration_"+$('.uri').val()+"")==null){
                	        requestFullScreen();
                	    }
                	    $("body").on("contextmenu",function(e){
                            return false;
                        });
                	    $(document).keydown(function (event) {
                            if (event.keyCode == 123) {
                                return false; //Disable F12
                            } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
                                return false; //Disable ctrl+shift+i
                            } else if (event.ctrlKey && event.keyCode == 73) {
                                return false; //Disable ctrl+shift+i
                            } else if (event.ctrlKey && event.keyCode == 85) {
                                return false; //Disable ctrl+u
                            }else if (event.ctrlKey && event.keyCode == 80) {
                                return false; //Disable ctrl+p
                            }
                            if (event.keyCode == 44) {
                                return false; //Disable Print Screen
                            }
                        });
                        disableSelection(document.body);
            		},
            		error: function (msg) {
            			if (msg.responseJSON['status'] == 303) {
            				location.href = base_loc;
            			}
            			if (msg.responseJSON['status'] == 401) {
            				location.href = base_loc;
            			}
            			if (msg.responseJSON['status'] == 400) {
            				location.href = base_loc;
            			}
            		}
            	});
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
    	});
   
}
	function requestFullScreen() {
        var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
        (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
        (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
        (document.msFullscreenElement && document.msFullscreenElement !== null);

        var docElm = document.documentElement;
        if (docElm.requestFullscreen) {
            docElm.requestFullscreen();
        } else if (docElm.mozRequestFullScreen) {
            docElm.mozRequestFullScreen();
        } else if (docElm.webkitRequestFullScreen) {
            docElm.webkitRequestFullScreen();
        } else if (docElm.msRequestFullscreen) {
            docElm.msRequestFullscreen();
        }
    }
function exitFullScreen(){
    var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
        (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
        (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
        (document.msFullscreenElement && document.msFullscreenElement !== null);
    if (isInFullScreen) {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
    update_cookie(2);
    submit_test_paper_modal();
}
function disableSelection(target) {
    if (typeof target.onselectstart != "undefined")
        target.onselectstart = function() {
            return false
        }
    else if (typeof target.style.MozUserSelect != "undefined")
        target.style.MozUserSelect = "none"
    else
        target.onmousedown = function() {
            return false
        }
    target.style.cursor = "default"
}
</script>