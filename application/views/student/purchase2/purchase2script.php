<script type="text/javascript">

	var selected_items = {};
	var selected_course_count = 0;
	var selected_library_count = 0;
	var selected_subjects_count = 0;
	var selected_books_count = 0;

	$('.purchase2type').parent().removeClass('active');
	$('.purchase2type').first().parent().addClass('active');
	$(".list-email").html('');
	getpurchase2content($('.purchase2type').first().attr("data-purchase2type"));

	$('body').on('click','.purchase2type',function(){
		$('.purchase2type').parent().removeClass('active');
		$(this).parent().addClass('active');
		$(".list-email").html('');
		getpurchase2content($(this).attr("data-purchase2type"));
	});


	$('.list-email').on('click','.addtocart',function(){
		var value = JSON.parse($(this).val());
		indexid = value.PURCHASE2TYPE + '_' + value.ID;
		selected_items[indexid] = value;
		if(value.PURCHASE2TYPE==0){
			selected_course_count += 1;
			$(".selected_course_count").html(selected_course_count);
		}else if(value.PURCHASE2TYPE==1){
			selected_library_count += 1;
			$(".selected_library_count").html(selected_library_count);
		}else if(value.PURCHASE2TYPE==2){
			selected_subjects_count += 1;
			$(".selected_subjects_count").html(selected_subjects_count);
		}else if(value.PURCHASE2TYPE==3){
			selected_books_count += 1;
			$(".selected_books_count").html(selected_books_count);
		}
	});


	$('.list-email').on('click','.removefromcart',function(){
		var value = JSON.parse($(this).val());
		indexid = value.PURCHASE2TYPE + '_' + value.ID;
		delete selected_items[indexid];
		if(value.PURCHASE2TYPE==0){
			selected_course_count -= 1;
			$(".selected_course_count").html(selected_course_count);
		}else if(value.PURCHASE2TYPE==1){
			selected_library_count -= 1;
			$(".selected_library_count").html(selected_library_count);
		}else if(value.PURCHASE2TYPE==2){
			selected_subjects_count -= 1;
			$(".selected_subjects_count").html(selected_subjects_count);
		}else if(value.PURCHASE2TYPE==3){
			selected_books_count -= 1;
			$(".selected_books_count").html(selected_books_count);
		}
	});



/*	$('.list-email').on('change','.selectpurchase2content',function(){
		var value = JSON.parse($(this).val());
		indexid = value.PURCHASE2TYPE + '_' + value.ID;
		if(this.checked){
			selected_items[indexid] = value;
			$(this).siblings(".addtocart").trigger("click");
			if(value.PURCHASE2TYPE==0){
				selected_course_count += 1;
				$(".selected_course_count").html(selected_course_count);
			}else if(value.PURCHASE2TYPE==1){
				selected_library_count += 1;
				$(".selected_library_count").html(selected_library_count);
			}else if(value.PURCHASE2TYPE==2){
				selected_subjects_count += 1;
				$(".selected_subjects_count").html(selected_subjects_count);
			}else if(value.PURCHASE2TYPE==3){
				selected_books_count += 1;
				$(".selected_books_count").html(selected_books_count);
			}
		}else{
			delete selected_items[indexid];
			$(this).siblings(".removefromcart").trigger("click");
			if(value.PURCHASE2TYPE==0){
				selected_course_count -= 1;
				$(".selected_course_count").html(selected_course_count);
			}else if(value.PURCHASE2TYPE==1){
				selected_library_count -= 1;
				$(".selected_library_count").html(selected_library_count);
			}else if(value.PURCHASE2TYPE==2){
				selected_subjects_count -= 1;
				$(".selected_subjects_count").html(selected_subjects_count);
			}else if(value.PURCHASE2TYPE==3){
				selected_books_count -= 1;
				$(".selected_books_count").html(selected_books_count);
			}
		}
		var size = Object.keys(selected_items).length;
		console.log(size);
	});*/


	function getpurchase2content(purchase2type){

		 $.ajax({
            type: 'GET',
            url: base_loc + 'studentgetajax/getpurchase2content',
            data: {loginid: loginid,purchase2type:purchase2type},
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                console.log("before");
            },
            success: function(msg) {

            	if(msg.status==200){
                    $('.insertid').val(msg.data);
                    $.each(msg.data,function(index,value){
                    	indexid = value.PURCHASE2TYPE + '_' + value.ID;
                    	if(selected_items.hasOwnProperty(indexid)){
                    		checked = 'checked';
                    		eventclass = 'removefromcart';
                    		eventTitle = '<i class="fa fa-minus"></i> Remove From Cart';
                    	}else{
                    		checked = '';
                    		eventclass = 'addtocart';
                    		eventTitle = '<i class="fa fa-plus"></i> Add To Cart';
                    	}
                    	var content_summmary = ``;
                    	$.each(value.CONTENT_SUMMARY,function(index2,value2){ 
                    		content_summmary += `
                    			<span class="badge">
								  ${value2.name} <span class="badge badge-light">${value2.count}</span>
								</span>
                    		`;
						}); 
						if(content_summmary.length>0){
                    		//content_summmary += `<span class="badge">View More</span>`;
						}
                    	$(".list-email").append(`
                    		<li class="list-group-item unread">

                    			<div class="media-body">
										<h5 class="media-heading">
											${value.NAME} <span class="badge badge-info">₹${value.AMT}</span>
											<button class="btn btn-xs btn-warning  ${eventclass}" attr-prodid="${value.ID}" attr-prodtype="${value.PURCHASE2TYPE}" attr-prodcost="${value.AMT}" attr-prodname="${value.NAME}" attr-discountamt="0" attr-totalamt="0" value='${JSON.stringify(value)}' style="float:right;">${eventTitle}</button> 
										</h5>
										<p></p>
										<p class="mb-0">${content_summmary}</p>
									</div>
								
								
								
							</li>
                    	`);
                    });
				}else{
					GritterNotification("Error",msg.message,"my-sticky-class");
				}
            	
                console.log(msg);
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });

	}
</script>

<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="https://pcskaka.com/developer/assets/themeassets/img/user/user-13.jpg"></script>
<script src="<?php echo base_url();?>assets/studentassets/js/studentpurchase.js"> </script>