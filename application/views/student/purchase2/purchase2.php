<?php
function getCallbackUrlsuccess()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/successpayment';
}
function getCallbackUrlfailure()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return base_url().'studentpostajax/failurepayment';
}
?>
<style type="text/css">
	@media (min-width: 768px){
		.content {
			position: absolute;
			top: 50px;
			left: 0;
			right: 0;
			bottom: 0;
		}
	}
</style>
		<!-- begin #content -->
		<div id="content" class="content content-full-width">
			<!-- begin vertical-box -->
			<div class="vertical-box with-grid inbox bg-light">
				<!-- begin vertical-box-column -->
				<div class="vertical-box-column width-200">
					<!-- begin vertical-box -->
					<div class="vertical-box">
						<!-- begin wrapper -->
						<div class="wrapper">
							<div class="d-flex align-items-center justify-content-center">
								<a href="#emailNav" data-toggle="collapse" class="btn btn-inverse btn-sm mr-auto d-block d-lg-none">
									<i class="fa fa-cog"></i>
								</a>
								<a href="email_compose.html" class="btn btn-inverse p-l-40 p-r-40 btn-sm">
									Purchase
								</a>
							</div>
						</div>
						<!-- end wrapper -->
						<!-- begin vertical-box-row -->
						<div class="vertical-box-row collapse d-lg-table-row" id="emailNav">
							<!-- begin vertical-box-cell -->
							<div class="vertical-box-cell">
								<!-- begin vertical-box-inner-cell -->
								<div class="vertical-box-inner-cell">
									<!-- begin scrollbar -->
									<div data-scrollbar="true" data-height="100%">
										<!-- begin wrapper -->
										<div class="wrapper p-0">
											<div class="nav-title"><b>FOLDERS</b></div>
											<ul class="nav nav-inbox">
												<li class="active"><a href="#" class="purchase2type" data-purchase2type='0'>Course<span class="badge pull-right selected_course_count">0</span></a></li>
												<li><a href="#" class="purchase2type" data-purchase2type='1'>Library<span class="badge pull-right selected_library_count">0</span></a></li>
												<li><a href="#" class="purchase2type" data-purchase2type='2' >Subject<span class="badge pull-right selected_subjects_count">0</span></a></li>
												<li><a href="#" class="purchase2type" data-purchase2type='3'>Books<span class="badge pull-right selected_books_count">0</span></a></li>
											</ul>
											
										</div>
										<!-- end wrapper -->
									</div>
									<!-- end scrollbar -->
								</div>
								<!-- end vertical-box-inner-cell -->
							</div>
							<!-- end vertical-box-cell -->
						</div>
						<!-- end vertical-box-row -->
					</div>
					<!-- end vertical-box -->
				</div>
				<!-- end vertical-box-column -->
				<!-- begin vertical-box-column -->
				<div class="vertical-box-column">
					<!-- begin vertical-box -->
					<div class="vertical-box">
						<!-- begin wrapper -->
						<div class="wrapper">
							<!-- begin btn-toolbar -->
							<div class="btn-toolbar align-items-center">
								<div class="custom-control custom-checkbox mr-2">
									<input type="checkbox" class="custom-control-input" data-checked="email-checkbox" id="emailSelectAll" data-change="email-select-all" />
									<label class="custom-control-label" for="emailSelectAll"></label>
								</div>
								<div class="dropdown mr-2">
									<button class="btn btn-white btn-sm" data-toggle="dropdown">
										Select All <span class="caret m-l-3"></span>
									</button>
									<!-- <div class="dropdown-menu">
										<a href="javascript:;" class="dropdown-item"><i class="fa fa-circle f-s-9 fa-fw mr-2"></i> All</a>
										<a href="javascript:;" class="dropdown-item"><i class="fa fa-circle f-s-9 fa-fw mr-2 text-muted"></i> Unread</a>
										<a href="javascript:;" class="dropdown-item"><i class="fa fa-circle f-s-9 fa-fw mr-2 text-blue"></i> Contacts</a>
										<a href="javascript:;" class="dropdown-item"><i class="fa fa-circle f-s-9 fa-fw mr-2 text-success"></i> Groups</a>
										<a href="javascript:;" class="dropdown-item"><i class="fa fa-circle f-s-9 fa-fw mr-2 text-warning"></i> Newsletters</a>
										<a href="javascript:;" class="dropdown-item"><i class="fa fa-circle f-s-9 fa-fw mr-2 text-danger"></i> Social updates</a>
										<a href="javascript:;" class="dropdown-item"><i class="fa fa-circle f-s-9 fa-fw mr-2 text-indigo"></i> Everything else</a>
									</div> -->
								</div>
								<!-- <button class="btn btn-sm btn-white mr-2"><i class="fa fa-redo"></i></button> -->
								<!-- begin btn-group -->
								<div class="btn-group">
									<button class="btn btn-sm btn-white hide" data-email-action="delete"><i class="fa fa-times mr-2"></i> <span class="hidden-xs">Delete</span></button>
									<button class="btn btn-sm btn-white hide" data-email-action="archive"><i class="fa fa-folder mr-2"></i> <span class="hidden-xs">Archive</span></button>
									<button class="btn btn-sm btn-white hide" data-email-action="archive"><i class="fa fa-trash mr-2"></i> <span class="hidden-xs">Junk</span></button>
								</div>
								<!-- end btn-group -->
								<!-- begin btn-group -->
								<div class="btn-group ml-auto">
									<ol class="breadcrumb float-xl-right buyallcartproduct"></ol>
									<!-- <button class="btn btn-white btn-sm">
										<i class="fa fa-chevron-left"></i>
									</button>
									<button class="btn btn-white btn-sm">
										<i class="fa fa-chevron-right"></i>
									</button> -->
								</div>
								<!-- end btn-group -->
							</div>
							<!-- end btn-toolbar -->
						</div>
						<!-- end wrapper -->
						<!-- begin vertical-box-row -->
						<div class="vertical-box-row">
							<!-- begin vertical-box-cell -->
							<div class="vertical-box-cell">
								<!-- begin vertical-box-inner-cell -->
								<div class="vertical-box-inner-cell bg-white">
									<!-- begin scrollbar -->
									<div data-scrollbar="true" data-height="100%">
										<!-- begin list-email -->
										<input type="hidden" id="surl" name="surl" value="<?php echo getCallbackUrlsuccess(); ?>" />
										<input type="hidden" id="udf1" name="udf1" value="1" />
										<input type="hidden" id="furl" name="furl" value="<?php echo getCallbackUrlfailure(); ?>" />
										<ul class="list-group list-group-lg no-radius list-email">



										</ul>
										<!-- end list-email -->
									</div>
									<!-- end scrollbar -->
								</div>
								<!-- end vertical-box-inner-cell -->
							</div>
							<!-- end vertical-box-cell -->
						</div>
						<!-- end vertical-box-row -->
						<!-- begin wrapper -->
						<div class="wrapper clearfix d-flex align-items-center">
							<div class="text-inverse f-w-600">1,232 messages</div>
							<div class="btn-group ml-auto">
								<button class="btn btn-white btn-sm">
									<i class="fa fa-fw fa-chevron-left"></i>
								</button>
								<button class="btn btn-white btn-sm">
									<i class="fa fa-fw fa-chevron-right"></i>
								</button>
							</div>
						</div>
						<!-- end wrapper -->
					</div>
					<!-- end vertical-box -->
				</div>
				<!-- end vertical-box-column -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end #content -->


<!-- end #content -->
<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
<link href="<?php echo base_url(); ?>assets/themeassets/css/default/invoice-print.min.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
<!-- #modal-without-animation -->
<div class="modal " id="modal-prucahseproduct">
	<div class="modal-dialog  modal-xl">
		<div class="modal-content">
			<div class="modal-header hidden-print">
				<h4 class="modal-title">Review Your Order</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body ">
				<!-- begin invoice -->
				<div class="invoice">
					<!-- begin invoice-company -->
					<div class="invoice-company">
						<span class="pull-right hidden-print">
							<a href="javascript:;" class="btn btn-sm btn-white m-b-10"><i class="fa fa-file-pdf t-plus-1 text-danger fa-fw fa-lg"></i> Export as PDF</a>
							<a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-white m-b-10"><i class="fa fa-print t-plus-1 fa-fw fa-lg"></i> Print</a>
						</span>
						PCSKAKA
					</div>
					<!-- end invoice-company -->
					<!-- begin invoice-header -->
					<div class="invoice-header">
						<div class="invoice-from">
							<small>from</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse">PCSKAKA, Inc.</strong><br />
								Phone: +91 945-273-6000<br />
								Email: support@pcskaka.com
							</address>
						</div>
						<div class="invoice-to">
							<small>to</small>
							<address class="m-t-5 m-b-5">
								<strong class="text-inverse"><span class="name"></span></strong><br />
								<span class="student_address_html"></span></br>
								Phone: <span class="student_mobileno"></span><br />
								Email: <span class="student_name_upper"></span>
							</address>
						</div>
						<div class="invoice-date">
							<small>Invoice / <?php echo date('F');?> period</small>
							<div class="date text-inverse m-t-5"><?php echo date('d-F-Y'); ?></div>
							<div class="invoice-detail">
								#0000123DSS<br />
								Services Product
							</div>
						</div>
					</div>
					<!-- end invoice-header -->
					<!-- begin invoice-content -->
					<div class="invoice-content">
						<!-- begin table-responsive -->
						<div class="table-responsive ">
							<table class="table table-invoice buycarttable">
								<thead>
									<tr>
										<th class="text-nowrap"> PRODUCT DESCRIPTION</th>
										<th class="text-right" width="5%">PRICE</th>
										<th class="text-right" width="5%">DISCOUNT</th>
										<th class="text-right" width="5%">TOTAL</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot></tfoot>
							</table>
						</div>
						<!-- end table-responsive -->
						<!-- begin invoice-price -->
						<div class="invoice-price">
							<div class="invoice-price-left bg-white">
								<div class="invoice-price-row hide">
									<div class="sub-price">
										<small>SUBTOTAL</small>
										<span class="text-inverse subtotal"></span>
									</div>
								</div>
							</div>
							<div class="invoice-price-right totalpay">
								<small>TOTAL</small> <span class="f-w-600"></span>
							</div>
						</div>
						<!-- end invoice-price -->
					</div>
					<!-- end invoice-content -->
					<div class="text-right">
						<a href="javascript:;" class="btn btn-primary checkout">Pay Now</a>
					</div>
					<!-- begin invoice-note -->
					<div class="invoice-note">
						* Make all cheques payable to [Your Company Name]<br />
						* Payment is due within 30 days<br />
						* If you have any questions concerning this invoice, contact  [Name, Phone Number, Email]
					</div>
					<!-- end invoice-note -->
					<!-- begin invoice-footer -->
					<div class="invoice-footer">
						<p class="text-center m-b-5 f-w-600">
							THANK YOU FOR YOUR BUSINESS
						</p>
						<p class="text-center">
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-globe"></i> matiasgallipoli.com</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-phone-volume"></i> T:016-18192302</span>
							<span class="m-r-10"><i class="fa fa-fw fa-lg fa-envelope"></i> rtiemps@gmail.com</span>
						</p>
					</div>
					<!-- end invoice-footer -->
				</div>
				<!-- end invoice -->
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<div class="hide singleproduct">
	<input type="hidden" class="product-id">
	<input type="hidden" class="product-type">
	<input type="hidden" class="couponcode">
</div>
