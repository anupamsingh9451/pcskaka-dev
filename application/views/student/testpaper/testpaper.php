<link href="<?php echo base_url(); ?>assets/studentassets/css/testpaper/testpaper.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style-responsive.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/style.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/frontendasset/css/e-commerce/theme/default.css" rel="stylesheet" />
<style type="text/css">
    .widget.widget-stats{
        position: relative;
        color: #000;
        padding: 15px;
        border-radius: 3px;
        background-image: linear-gradient(rgb(255, 255, 255) 0%, rgba(255, 255, 255, 0.54) 46%, white 100%) !important;
        min-height: 10px;
    }
</style>

<!-- begin #content -->
<div id="content" class="content p-t-0">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right  hide">
        <li class="breadcrumb-item"><button class="btn btn-primary m-r-5 buy_checked_prod"><i class="fa fa-lock"></i> Buy All Checked Library </button></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <div class="search-toolbar sticky-top" style="top: 64px">
                <h4 style="font-size: 22px">Test Series / <?php echo $tsdt[0]->TS_NAME; ?></h4>
    </div>
    <!-- end page-header -->
    
    <div class="row">
            <div class="col-lg-9 col-md-12">


                    <!--Start Container-->
                    <div class="container mt-4">
                        <div class="row">

                            
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="promotion bg-primary text-white card shadow">
                                            <div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom text-right">
                                                <img src="../assets/img/product/product-mac-accessories.png" alt="">
                                            </div>
                                            <div class="promotion-caption text-center">
                                                <h4 class="promotion-title text-white">PCSKAKA</h4>
                                                <div class="promotion-price text-white"><small></small></div>
                                                <p class="promotion-desc text-white"></p>
                                                <a href="#" class="promotion-btn text-white border-white">View More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <div class="mt-4">
                <div class="section-header m-b-15">
                    <h3>Free Test Paper</h3>
                </div>
                <div class="section-body">
                    <!-- begin row -->
                    <div class="row">
                        <!-- begin col-3 -->
                        <?php $freetp = 0; if (!empty($testpaperdt)) {
    foreach ($testpaperdt as $testpaperdts) {
        if ($testpaperdts->TP_PAY_STATUS==1) {
            $freetp++; ?>
                        <div class="col-lg-12 col-md-12">
                            <div class=" widget  widget-list-item shadow">
                                            <div class="widget-list-media p-4 p-r-10">
                                                <img src="<?php echo base_url(); ?>assets/logo/logo.jpeg" alt=""  style="width: 100%;height: 100%;border-radius: 50%;">
                                            </div>
                                            <div class="widget-list-content">
                                                <h4 class="widget-list-title text-truncate width-150">
                                                    <a href="#" class=""><?php echo $testpaperdts->TP_NAME; ?>
                                                    </a>
                                                </h4>
                                                <p class="widget-list-desc">
                                                    <i class="fas fa-home fa-fw" aria-hidden="true"></i><?php echo $tsdt[0]->TS_QUESTION_NOS; ?> Total Question
                                                </p>

                                                
                                            </div>
                                            <div class="widget-list-media p-0 p-r-10">
                                                <!-- <a href="" class="btn btn-info btn-sm text-nowrap">Start Now</a> -->

                                                <?php if (strtotime(date('Y-m-d H:i:s'))>=strtotime($testpaperdts->TP_START_DATE)) { ?>
                                                    <?php $paperattempt = $this->Studentgetmodel->check_attempt_testpaper($testpaperdts->TP_ID); if (!$paperattempt) { ?>
                                                        <a href="<?php echo base_url().'test/solution/'.url_encode($testpaperdts->TP_ID); ?>" class="btn btn-success m-r-5 m-b-5 btn-sm text-nowrap" > Solution</a>
                                                    <?php } else { ?>
                                                    
                                                        <a href="<?php echo base_url().'test/instruction/'.url_encode($testpaperdts->TP_ID); ?>" class="btn btn-danger m-r-5 m-b-5 btn-sm text-nowrap" > Start Paper</a>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <a href="javascript:;" class="btn btn-warning f-s-10 btn-sm text-nowrap" style="border-color:#5c5e5f;"><i class="fa fa-lock"></i> Start on <?php echo date('M d', strtotime($testpaperdts->TP_START_DATE)); ?></a>
                                                    
                                                <?php } ?>

                                            </div>

                                        </div>
                        </div>
                        <?php
        }
    }
} if (empty($testpaperdt) || $freetp==0) { ?>
                            <div class="col-lg-12 col-md-12">
                                <div class="widget widget-stats bg-gradient-teal">
                                    <!--<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>-->
                                    <div class="stats-content pull-left">
                                        <div class="stats-number m-b-10">Free Test paper are not available! Coming Soon!</div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <!-- end col-3 -->
                        
                    </div>
                    <!-- end row -->
                </div>      
            </div>
            <div class="mt-4">
                <div class="section-header m-b-15">
                    <h3>All Test Paper</h3>
                </div>
                <div class="section-body">
                    <!-- begin row -->
                    <div class="row">
                        <!-- begin col-3 -->
                        <?php $paidtp = 0; if (!empty($testpaperdt)) {
    foreach ($testpaperdt as $testpaperdts) {
        if ($testpaperdts->TP_PAY_STATUS==0) {
            $paidtp++; ?>
                        <div class="col-lg-12 col-md-12">


                            <div class=" widget  widget-list-item shadow">
                                            <div class="widget-list-media p-4 p-r-10">
                                                <img src="<?php echo base_url(); ?>assets/logo/logo.jpeg" alt=""  style="width: 100%;height: 100%;border-radius: 50%;">
                                            </div>
                                            <div class="widget-list-content">
                                                <h4 class="widget-list-title text-truncate width-150">
                                                    <a href="#" class=""><?php echo $testpaperdts->TP_NAME; ?>
                                                    </a>
                                                </h4>
                                                <p class="widget-list-desc">
                                                    <i class="fas fa-home fa-fw" aria-hidden="true"></i><?php echo $tsdt[0]->TS_QUESTION_NOS; ?> Total Question
                                                </p>

                                                
                                            </div>
                                            <div class="widget-list-media p-0 p-r-10">
                                                <!-- <a href="" class="btn btn-info btn-sm text-nowrap">Start Now</a> -->

                                                <?php
                                                date_default_timezone_set('Asia/Kolkata');
            $check = $this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 4, $tsdt[0]->TS_ID);
                                                
            if (!empty($check) && strtotime(date('Y-m-d H:i:s'))>=strtotime($testpaperdts->TP_START_DATE)) { ?>
                                                    <?php $paperattempt = $this->Studentgetmodel->check_attempt_testpaper($testpaperdts->TP_ID); if (!$paperattempt) { ?>
                                                        <a href="<?php echo base_url().'test/solution/'.url_encode($testpaperdts->TP_ID); ?>" class="btn btn-success m-r-5 m-b-5 btn-sm text-nowrap" > Solution</a>
                                                    <?php } else { ?>
                                                    
                                                        <a href="<?php echo base_url().'test/instruction/'.url_encode($testpaperdts->TP_ID); ?>" class="btn btn-danger m-r-5 m-b-5 btn-sm text-nowrap" > Start Paper</a>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <a href="javascript:;" class="btn btn-warning f-s-10 btn-sm text-nowrap" style="border-color:#5c5e5f;"><i class="fa fa-lock"></i> Start on <?php echo date('M d', strtotime($testpaperdts->TP_START_DATE)); ?></a>
                                                    
                                                <?php } ?>

                                            </div>

                                        </div>
                        
                        </div>
                        <?php
        }
    }
} if (empty($testpaperdt) || $paidtp==0) { ?>
                            <div class="col-lg-12 col-md-12">
                                <div class="widget widget-stats bg-gradient-teal">
                                    <!--<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>-->
                                    <div class="stats-content pull-left">
                                        <div class="stats-number m-b-10">Test paper are not available! Coming Soon!</div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <!-- end col-3 -->
                        
                    </div>
                    <!-- end row -->
                </div>      
            </div>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Container-->
            </div>

            <div class="col-lg-3 col-md-12">
                <div class="container mt-4">
                <div class="row">
                    <?php for ($i=0;$i<=2;$i++): ?>
                       <div  class="col-lg-12 col-md-12">
                            <!-- BEGIN item -->
                            <div class="item item-thumbnail card" style="border-radius: 7%;background-image: linear-gradient(to top, rgba(255,0,0,0), rgba(255,0,0,0.3))">
                                <a href="product_detail.html" class="item-image">
                                    <img src="../assets/img/product/product-iphone.png" alt="">
                                    <div class="discount">15% OFF</div>
                                </a>
                                <div class="item-info">
                                    <h4 class="item-title">
                                        <a href="product_detail.html">PCSKAKA<br></a>
                                    </h4>
                                    <p class="item-desc"></p>
                                    <div class="item-price"></div>
                                    <div class="item-discount-price"></div>
                                </div>
                            </div>
                            <!-- END item -->
                        </div>
                        <?php endfor; ?>
                </div>
            </div>
            </div>
    </div>  

    <div class="row">
        <!-- <h1 class="page-header f-w-700" ><?php //echo $tsdt[0]->TS_NAME;?></h1> -->
            <!-- end page-header -->
            <div class="container m-b-20 m-l-10">
                <div class="section-header" style="padding: 40px;background: #fafbfc;border-radius: 3px;">
                    <p style="margin: 0;font-size: 15px;letter-spacing: .5px;line-height: 1.8;font-weight: 600;color:#000000;"><?php echo $tsdt[0]->TS_DESCRIPTION; ?></p>
                </div>
                        
            </div>
    </div>
        
    </div>
    <!-- end row -->
    
</div>
<!-- end #content -->