<!DOCTYPE html>

<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>PCSKAKA Student | <?php echo $page; ?> </title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-151895981-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-151895981-1');
    </script>
	<!--<link rel="shortcut icon" type="image/png" href="http://dcntv.in/assets/images/dcnlogo.png"/>-->
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
		 <link rel="icon" href="<?php echo base_url(); ?>assets/logo/logo.jpeg">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/font-awesome/css/all.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/css/default/style.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/css/default/style-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/css/default/theme/default.css" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-jvectormap/jquery-jvectormap.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-calendar/css/bootstrap_calendar.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/nvd3/build/nv.d3.css" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
	 <!-- DATATABLE-->
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/AutoFill/css/autoFill.bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/ColReorder/css/colReorder.bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/KeyTable/css/keyTable.bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/RowReorder/css/rowReorder.bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Select/css/select.bootstrap.min.css" rel="stylesheet" />
	<!-- DATE PICKER AND MULTI SELECT-->
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/ionRangeSlider/css/ion.rangeSlider.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/ionRangeSlider/css/ion.rangeSlider.skinNice.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/password-indicator/css/password-indicator.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-combobox/css/bootstrap-combobox.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-tag-it/css/jquery.tagit.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker-fontawesome.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker-glyphicons.css" rel="stylesheet" />


	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/pace/pace.min.js"></script>
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/morris/morris.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/themeassets/plugins/nvd3/build/nv.d3.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery/jquery-3.3.1.min.js"></script>
	<link href="<?php echo base_url(); ?>assets/studentassets/css/main.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/apexcharts@3.0.0"></script>


	<script>
		var api_loc="http://localhost:3000/";
		var base_loc="<?php echo base_url(); ?>";
		var loginid="<?php echo $this->session->userdata('studentloginid'); ?>";
        var token="<?php echo $this->session->userdata('studenttoken'); ?>";
        var type="<?php echo $this->session->userdata('studenttype'); ?>";
		var clientserv="Ratan";
        var apikey="pcskaka";
	</script>
	
	<!-- ================== END BASE JS ================== -->
<style>
    .student_name_upper{
        word-break: break-word;
    }
    body #content{
		background: #f0f3f5;
	}
    
    .carousel-item>img{
    	max-height: 330px;
    }
    
</style>
</head>
<body>

	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed page-with-light-sidebar ">
		<!-- begin #header -->
		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="<?php echo base_url(); ?>student/dashboard" class="navbar-brand"><span class="navbar-logo"></span> <b>PCSKAKA</b> STUDENT</a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header -->
			
			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				
				
				<li class="dropdown navbar-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo base_url(); ?>assets/themeassets/img/user/user-15.png" alt="" /> 
						<span class="d-none d-md-inline student_name_upper"></span> <b class="caret"></b>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<!--<a href="javascript:;" class="dropdown-item">Edit Profile</a>-->
						
						<a href="javascript:;" class="dropdown-item logoutstudent">Log Out</a>
					</div>
				</li>
			</ul>
			<!-- end header navigation right -->
		</div>
		<!-- end #header -->

<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<a href="javascript:;" data-toggle="nav-profile">
							<div class="cover with-shadow"></div>
							<div class="image">
								<img src="<?php echo base_url(); ?>assets/themeassets/img/user/user-15.png" alt="" />
							</div>
							<div class="info">
							
							    <span class="name"></span>
							    <span class="student_name_upper"></span>
								<small class="student_address_upper"></small>
							</div>
						</a>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">Navigation</li>
					<li class="<?php if ($page=='dashboard') {
    echo 'active';
} ?>"><a href="<?php echo base_url(); ?>student/dashboard"><i class="fas fa-lg fa-fw m-r-10 fa fa-book"></i> <span>Dashboard</span></a></li>
					<li class="<?php if ($page=='performance') {
    echo 'active';
} ?> has-sub"><a href="<?php echo base_url(); ?>student/performance"><i class="fa fa-th-large"></i><span>Performance </span></a></li>
					
					<li class="has-sub <?php if ($page=='profile') {
    echo 'active';
}?>" >
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="fas fa-lg fa-fw m-r-10 fa-user"></i>
							<span>Profile</span>
						</a>
						<ul class="sub-menu">
							<li class="<?php if ($subpage=='Edit profile') {
    echo 'active';
} ?>"><a href="<?php echo base_url(); ?>student/profile/editprofile">Edit Profile</a></li>
							<li class="<?php if ($subpage=='Change Password') {
    echo 'active';
} ?>"><a href="<?php echo base_url(); ?>student/profile/changepwd">Change Password</a></li>
						</ul>
					</li>
					<li class="<?php if ($page=='courses') {
    echo 'active';
} ?>"><a href="<?php echo base_url(); ?>student/courses"><i class="fas fa-lg fa-fw m-r-10 fa fa-book"></i> <span>Courses</span></a></li>
					<li class="<?php if ($page=='test series') {
    echo 'active';
} ?>"><a href="<?php echo base_url(); ?>student/testseries"><i class="fas fa-lg fa-fw m-r-10 fa fa-book"></i> <span>Test Series</span></a></li>
					
					
					<li class="<?php if ($page=='exams') {
    echo 'active';
} ?>"><a href="<?php echo base_url(); ?>student/library"><i class="fas fa-lg fa-fw m-r-10 fa fa-book"></i> <span>Exams</span></a></li>
					<!-- <li class="<?php /*if($page=='purchase2'){ echo 'active'; } ?>"><a href="<?php echo base_url();*/ ?>student/purchase2"><i class="fas fa-lg fa-fw m-r-10 fa fa-book"></i> <span>Purchase</span></a></li> -->
					<li class="logoutstudent"><a href="javascript:void(0);"><i class="fas fa-lg fa-fw m-r-10 fa fa-key"></i> <span>Logout</span></a></li>
				
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		
		<div class="sidebar-bg"></div>