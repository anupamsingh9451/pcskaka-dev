<script>
var attrbookid = '';
$('document').ready(function(){
    $('body').on('click','.table_tab', function () {
        get_ques_table($(this).attr('attr-book-id'));
        attrbookid = $(this).attr('attr-book-id');
    });
    $('body').on('click','.deleteques',function(){
	    var quesid = $(this).attr('attr-quesid');
		swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Question!",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true,
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
            		type: 'POST',
            		url: base_loc + 'userpostajax/deletequestion',
            		data: 'quesid='+quesid,
            		headers: {
            			'Client-Service': clientserv,
            			'Auth-Key': apikey,
            			'User-ID': loginid,
            			'Authorization': token,
            			'type': type
            		},
            		success: function (msg) {
            			if(msg.data=="ok"){
            			    get_ques_table(attrbookid)
            				swal({
                                title: 'Deleted!',
                                text: 'Question Deleted successfully!',
                                icon: 'success'
                            }).then(function() {
                                 // <--- submit form programmatically
                            });
            			}else{
            				GritterNotification("Error",msg.message,"my-sticky-class");
            			}
            			updatelistques();
            		},
            		error: function (msg) {
            			if (msg.responseJSON['status'] == 303) {
            				location.href = base_loc;
            			}
            			if (msg.responseJSON['status'] == 401) {
            				location.href = base_loc;
            			}
            			if (msg.responseJSON['status'] == 400) {
            				location.href = base_loc;
            			}
            		}
                });
            } else {
                swal("Cancelled", "Your Question is safe :)", "error");
            }
        })
	});
});
function get_ques_table(id){
	$('#question_table_'+id+'').dataTable().fnDestroy();
		$('#question_table_'+id+'').DataTable({
			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"ajax":{
				"url":base_loc + 'usergetajax/listquestion',
				"type": "GET",
				"data": { bookid:id  },
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"QUES"},
				{"data":"QUES_CREATED_AT"},
				{"data":"QUES_STATUS"},
				{"data":"TOOLS"},
			]
		});
}
</script>