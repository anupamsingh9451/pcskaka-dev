<link href="<?php echo base_url(); ?>assets/userassets/css/book/createbook.css" rel="stylesheet" />
<!-- begin panel -->
<div class="panel panel-inverse panel-with-tabs">
	<!-- begin panel-heading -->
	<div class="panel-heading p-0">
		<div class="panel-heading-btn m-r-10 m-t-10">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
		</div>
		<!-- begin nav-tabs -->
		<div class="tab-overflow ">
			<ul class="nav nav-tabs nav-tabs-inverse book_tab_div">
				<li class="nav-item prev-button "><a href="javascript:;" data-click="prev-tab" class="nav-link text-success"><i class="fa fa-arrow-left"></i></a></li>
				<?php if(!empty($bookdt)){ $first_tab="first_tab"; $act_class="active"; $i=1; foreach($bookdt as $bookdts){ ?>
				    <li class="nav-item ">
				        <a href="#nav-tab-<? echo $i; ?>" attr-book-id="<?php echo $bookdts->BOOK_ID;?>" data-toggle="tab" class="<?php echo $first_tab;?> table_tab nav-link <?php echo $act_class; ?>"><?php echo $bookdts->BOOK_NAME;?></a>
				    </li>
				<?php $act_class=""; $first_tab=""; $i++;  } } ?>
				<li class="nav-item next-button"><a href="javascript:;" data-click="next-tab" class="nav-link text-success"><i class="fa fa-arrow-right"></i></a></li>
			</ul>
		</div>
		<!-- end nav-tabs -->
	</div>
	<!-- end panel-heading -->
	<!-- begin tab-content -->
	<div class="tab-content">
		<!-- begin tab-pane -->
		
    	<?php if(!empty($bookdt)){ $act_class="active show"; $i=1; foreach($bookdt as $bookdts){ ?>
			<div class="tab-pane fade <?php echo $act_class; ?>" id="nav-tab-<? echo $i; ?>">
				<div class="bckcss">
				    <form class="create_quest">
				        <input type="hidden" name="book_id" value="<?php echo $bookdts->BOOK_ID;?>">
        				<div class="col-md-12">
        				    <div class="row">
        				        <div class="col-md-12">
        							<label class="col-form-label">Question <span class="star">*</span></label>
                                    <textarea class="summernote question" name="question" id="mysummernote"></textarea>
            					</div>
            					<div class="col-md-6">
        							<label class="col-form-label">No of options <span class="star">*</span></label>
        							<input type="text" class="form-control obj_options input_num" name="obj_options" placeholder="Enter Number of Options" value="4">
        					    </div>
                                <div class="col-md-12">
                                    <div class="row options_div">
                                        
                                    </div>
            				    </div>
            				    <div class="col-md-12">
        							<label class="col-form-label">Question Explanation <span class="star">*</span></label>
                                    <textarea rows="2" class="form-control question_explain" name="question_explain" required></textarea>
            					</div>
            					<div class="col-md-12 m-t-10 m-b-10" >
        							<div class="col-md-12 text-center">
        								<button type="submit" class="btn btn-primary questioncreate">SAVE</button>
        							</div>
        						</div>
        				    </div>
        				</div>
    				</form>
				</div>
			</div>
    		
    	<?php $act_class=""; $i++; } } ?>
    		<!-- end tab-pane -->
		
	</div>
	<!-- end tab-content -->
</div>
<!-- end panel -->
