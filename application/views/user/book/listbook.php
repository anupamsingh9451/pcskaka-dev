<!-- begin panel -->
<div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
	<!-- begin panel-heading -->
	<div class="panel-heading p-0">
		<div class="panel-heading-btn m-r-10 m-t-10">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
		</div>
		<!-- begin nav-tabs -->
		<div class="tab-overflow ">
			<ul class="nav nav-tabs nav-tabs-inverse book_tab_div">
				<li class="nav-item prev-button "><a href="javascript:;" data-click="prev-tab" class="nav-link text-success"><i class="fa fa-arrow-left"></i></a></li>
				<?php if(!empty($bookdt)){ $first_tab="first_tab"; $act_class="active"; $i=1; foreach($bookdt as $bookdts){ ?>
				    <li class="nav-item ">
				        <a href="#nav-tab-<? echo $i; ?>" attr-book-id="<?php echo $bookdts->BOOK_ID;?>" data-toggle="tab" class="<?php echo $first_tab;?> book-tab table_tab nav-link <?php echo $act_class; ?>"><?php echo $bookdts->BOOK_NAME;?></a>
				    </li>
				<?php $act_class=""; $first_tab=""; $i++;  } } ?>
				<li class="nav-item next-button"><a href="javascript:;" data-click="next-tab" class="nav-link text-success"><i class="fa fa-arrow-right"></i></a></li>
			</ul>
		</div>
		<!-- end nav-tabs -->
	</div>
	<!-- end panel-heading -->
	<!-- begin tab-content -->
	<div class="tab-content">
		<!-- begin tab-pane -->
		<?php if(!empty($bookdt)){ $act_class="active show"; $i=1; foreach($bookdt as $bookdts){ ?>
			<div class="tab-pane fade <?php echo $act_class; ?>" id="nav-tab-<? echo $i; ?>">
			    <div class="m-t-5 m-b-5 text-right">
            			<button type="button" class="btn btn-warning add_ques_modal" attr-book-id="<?php echo $bookdts->BOOK_ID;?>" style="font-size: 10px;line-height: 16px;"><i class="fa fa-fw fa-hdd"></i> Add Question</button>
			    </div>
				<table id="question_table_<?php echo $bookdts->BOOK_ID ?>" class="table table-striped table-bordered datatable">
					<thead>
						<tr>
							<th width="1%">Sr No.</th>
							<th width="65%">Question</th>
							
							<th>Created Date</th>
							<th>Status</th>
							<th width="7%" class="text-nowrap">Tools</th>
						</tr>
					</thead>
					<tbody>
					
					</tbody>
				</table>
			</div>
		<?php $act_class=""; $i++; } } ?>
		<!-- end tab-pane -->
		
	</div>
	<!-- end tab-content -->
</div>
<!-- end panel -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/themeassets/plugins/highlight/highlight.common.js"></script>
<script src="<?php echo base_url(); ?>assets/themeassets/js/demo/render.highlight.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->
<script>
	App.init();
</script>

 