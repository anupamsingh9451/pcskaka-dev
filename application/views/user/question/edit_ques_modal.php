<link href="<?php echo base_url(); ?>assets/userassets/css/question/edit_ques_modal.css" rel="stylesheet" />
<div class="row">
    <div class="col-md-12">
        <form class="questionedit">
            <input type="hidden" name="quesid" value="<?php echo $quesdt[0]->QUES_ID; ?>">
            <input type="hidden" class="bookid" value="<?php echo $quesdt[0]->BOOK_ID; ?>">
		    <div class="row">
		        <div class="col-md-6 hide">
					<label class="col-form-label">Question Type <span class="star">*</span></label>
					<select class="form-control ques_type" name="ques_type" data-style="btn-white" required>
                        <option  value="">--Select Question Type--</option>
                        <!--<option value="1" <?php if($quesdt[0]->QUES_TYPE==1){ echo 'selected';}?> >Subjective</option>-->
                        <option  value="2" <?php if($quesdt[0]->QUES_TYPE==2){ echo 'selected';}?>>Objective</option>
					</select>
				</div>
		        <div class="col-md-12">
					<label class="col-form-label">Question <span class="star">*</span></label>
                    <!--<textarea rows="3" class="form-control question" name="question" required><?php if($quesdt[0]->QUES_TYPE==2){ echo json_decode($quesdt[0]->QUES)->QUESTION; }else{ echo $quesdt[0]->QUES; }?></textarea>-->
                    <textarea class="summernote question" name="question" id="mysummernote"><?php if($quesdt[0]->QUES_TYPE==2){ echo json_decode($quesdt[0]->QUES)->QUESTION; }else{ echo $quesdt[0]->QUES; }?></textarea>
				</div>
				<div class="col-md-12 for_objective">
				    <div class="row">
					    <div class="col-md-12">
					        <div class="row">
					            <div class="col-md-6">
        							<label class="col-form-label">No of options <span class="star">*</span></label>
        							<input type="text" class="form-control edit_obj_options input_num" name="obj_options" placeholder="Enter Number of Options" value="4">
    						    </div>
    						    
                                <div class="col-md-12">
                                    <div class="row edit_options_div">
                                        
                                    </div>
            				    </div>
            				    <div class="col-md-6">
        							<label class="col-form-label">Question Explanation <span class="star">*</span></label>
                                    <textarea rows="2" class="form-control obj_ques_explain" name="obj_ques_explain"><?php echo $quesdt[0]->QUES_EXPLANATION; ?></textarea>
            					</div>
					        </div>
					    </div>
				    </div>
				</div>
				<div class="col-md-12" style="margin-top:10px">
					<div class="col-md-12 text-center">
						<button type="submit" class="btn btn-primary questioneditbtn">SAVE</button>
					</div>
				</div>
			</div>
		 </form>
    </div>
</div>
<script>

$('document').ready(function(){
	App.init();
	FormSummernote.init();
		
    if($('.ques_type').length>0){
	    $ques_type = $('.ques_type').val();
		if($ques_type==1){
		    $('.for_subjective').show();
		    $('.for_objective').hide();
		}else{
		    $('.for_subjective').hide();
		    optionss = '';
		    add_obj_option_div(optionss);
		    $('.for_objective').show();
		}
	}
	if($('.options_divs').length>0){
	    if(<?php echo $quesdt[0]->QUES_TYPE ?>==2){
	        <?php $option = json_decode($quesdt[0]->QUES)->OPTION;  ?>
	        optionss=<?php echo json_encode($option); ?>;
	        answer='<?php echo $quesdt[0]->QUES_ANSWERS; ?>';
	        var i=0;
	        $(".options_divs").each(function() {
                $(this).find('.ques_option').html(optionss[i])
                i++;
            });
            
            var k = 1;
    	    $(".options_divs").each(function() {
    	        var answers = answer.toString().split(',');
    	        var l = k.toString();
                if(answers.includes(l)){
                    $(this).find('.obj_answer').prop('checked', true);
                }
                 k++;
            });
	        
	    }
	}

});
</script>
