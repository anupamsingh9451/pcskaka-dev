<link href="<?php echo base_url(); ?>assets/userassets/css/question/view_ques_modal.css" rel="stylesheet" />
<div class="row">
    <div class="col-md-12">
        <div class="card" style="border: 1px solid rgba(0,0,0,.2);">
			<div class="card-block"> 
			    <?php if($quesdt[0]->QUES_TYPE==2){ ?>
				    <p class="card-text ques_font">Q. &nbsp;<?php echo json_decode($quesdt[0]->QUES)->QUESTION; ?> </p>
				  <?php }else{?>
				       <p class="card-text ques_font">Q. &nbsp;<?php echo $quesdt[0]->QUES; ?> </p>
				  <?php } ?>
			    <div class="row">
			        <?php if($quesdt[0]->QUES_TYPE==2){ ?>
				        <div class="col-md-6">
				            <?php $azRange = range('A', 'Z'); $i=0; $option = json_decode($quesdt[0]->QUES)->OPTION; foreach($option as $options){ ?>
				            <div class="qa_progress" <?php if(in_array($i+1,explode(',',$quesdt[0]->QUES_ANSWERS))){ echo 'style="width:100%;border: #4caace solid 1px;" '; }else{ echo 'style="width:100%;" '; } ?> style="width:100%">
                                <div class="qa_progress_text">
                                    <ul>
                                        <li class="qa_number" <?php if(in_array($i+1,explode(',',$quesdt[0]->QUES_ANSWERS))){ echo 'style="background: #4caace;color: #fff;" '; } ?>><?php echo $azRange[$i]; ?></li>
                                        <li class="qa_text" style="display: inline-block">
                                            <p><?php echo $options; ?></p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php $i++; } ?>
				        </div>
				    <?php }else{?>
				        <div class="col-md-6">
				            <div class="qa_progress" style="width:100%;">
                                <div class="qa_progress_text">
                                    <ul>
                                        <li class="qa_text" style="display: inline-block">
                                            <p><?php echo $quesdt[0]->QUES_ANSWERS; ?></p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
				        </div>
				    <?php } ?>
				    <div class="col-md-12">
                		<?php if($quesdt[0]->QUES_EXPLANATION!=''){ ?>
                			<div class="row m-t-10">
                			    <div class="col-md-3">
                			        <h5 class="">Question Explanation : 
                			    </div>
                			    <div class="col-md-8">
                			        </span></h5><span class=""><?php echo $quesdt[0]->QUES_EXPLANATION; ?></span>
                	            </div>
                	        </div>
            	        <?php } ?>
            		</div>
			    </div>
			    
			</div>
		</div>
    </div>
</div>