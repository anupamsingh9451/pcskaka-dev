<link href="<?php echo base_url(); ?>assets/userassets/css/question/add_ques_modal.css" rel="stylesheet" />
<div class="row">
    <div class="col-md-12">
        <form class="create_quest">
            <input type="hidden" name="book_id" value="<?php echo $bookid; ?>">
		    <div class="row">
		        <div class="col-md-6 hide">
					<label class="col-form-label">Question Type <span class="star">*</span></label>
					<select class="form-control ques_type" name="ques_type" data-style="btn-white">
                        <option  value="">--Select Question Type--</option>
                        <option  value="2" selected>Objective</option>
					</select>
				</div>
		        <div class="col-md-12">
					<label class="col-form-label">Question <span class="star">*</span></label>
                   	<textarea class="summernote question" name="question" id="mysummernote"></textarea>
				</div>
			
				<div class="col-md-12 for_objective">
				    <div class="row">
					    <div class="col-md-12">
					        <div class="row">
					            <div class="col-md-6">
        							<label class="col-form-label">No of options <span class="star">*</span></label>
        							<input type="text" class="form-control obj_options edit_obj_options input_num" name="obj_options" placeholder="Enter Number of Options" value="4">
    						    </div>
    						    
                                <div class="col-md-12">
                                    <div class="row edit_options_div">
                                        
                                    </div>
            				    </div>
            				    <div class="col-md-6">
        							<label class="col-form-label">Question Explanation <span class="star">*</span></label>
                                    <textarea rows="2" class="form-control question_explain" name="question_explain"></textarea>
            					</div>
					        </div>
					    </div>
					   
				    </div>
				</div>
				<div class="col-md-12" style="margin-top:10px">
					<div class="col-md-12 text-center">
						<button type="submit" class="btn btn-primary questioncreate">SAVE</button>
					</div>
				</div>
			</div>
		 </form>
    </div>
</div>
<script>

$('document').ready(function(){
	FormSummernote.init();
    
    if($('.ques_type').length>0){
	    $ques_type = $('.ques_type').val();
		if($ques_type==1){
		    $('.for_subjective').show();
		    $('.for_objective').hide();
		}else{
		    $('.for_subjective').hide();
		    optionss = '';
		    add_obj_option_div(optionss);
		    $('.for_objective').show();
		}
	}
});
</script>