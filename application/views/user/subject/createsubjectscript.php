<script>
$('document').ready(function(){
    $('.books_list').on('click', function () {
        $('.books_list').attr('style','color:#2d353c;');
        get_books_html($(this).attr('attr-subid'));
        $(this).attr('style','color:#348fe2;');
    });
    $('body').on('keyup','.obj_options',function(){
		$options = parseInt($(this).val());
		
		    $str = '';
		    for($i=1; $i<=$options; $i++){
		        $str += '<div class="col-md-6"><label class="col-form-label">Option '+$i+'<span class="star">*</span></label><div class="input-group m-b-10"><div class="input-group-prepend"><span class="input-group-text"><input type="radio" id="defaultRadio" class="obj_answer" name="obj_answer[]" value="'+$i+'"></span></div><textarea rows="1" class="form-control ques_option" name="ques_option[]"></textarea></div></div>';
		    }
		    $('.options_div').html($str);
	});
});
function get_books_html(id){
	$.ajax({
		type: 'GET',
		url: base_loc + 'usergetajax/createbookhtml',
		data: 'subid='+id,
		async:false,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
			$('.book_div').html(msg);
			$('.book_tab_div').find('.first_tab').trigger('click');
			$('.book_div').find('.obj_options').trigger('keyup');
			
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc; 
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc; 
			}
		}
	});
}
</script>