<script>
$('document').ready(function(){
    $('body').on('click','.books_list', function () {
        
        $('.books_list').attr('style','color:#2d353c;');
        get_books_html($(this).attr('attr-subid'));
        $(this).attr('style','color:#348fe2;');
    });
    $('body').on('click','.view_ques_modal', function () {
        var quisid = $(this).attr('attr-quesid');
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'usergetajax/view_ques_html',
    		data: 'quisid='+quisid,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.view_ques_modal_div').html(msg);
    			$('#viewquestion').modal('show');
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
    });
    
    $('body').on('click','.edit_ques_modal', function () {
        var quisid = $(this).attr('attr-quesid');
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'usergetajax/edit_ques_html',
    		data: 'quisid='+quisid,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.edit_ques_modal_div').html(msg);
    			$('#editquestion').modal('show');
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    		
    	});
    });
    
    $('body').on('click','.add_ques_modal', function () {
        var bookid = $(this).attr('attr-book-id');
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'usergetajax/add_ques_html',
    		data: 'bookid='+bookid,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.add_ques_modal_div').html(msg);
    			$('#addquestion').modal('show');
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
    });
});
function get_books_html(id){
	$.ajax({
		type: 'GET',
		url: base_loc + 'usergetajax/listbookhtml',
		data: 'subid='+id,
		async:false,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
			$('.book_div').html(msg);
			$('.book_tab_div').find('.first_tab').trigger('click');
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc; 
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc; 
			}
		}
	});
	
}
</script>