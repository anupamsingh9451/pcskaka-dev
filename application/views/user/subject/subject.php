<link href="<?php echo base_url(); ?>assets/userassets/css/subject/subject.css" rel="stylesheet" />
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active"><a href="javascript:;">Subject</a></li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Subject <?php echo $this->session->userdata('userloginid'); ?>
			</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
				<!-- begin col-3 -->
				<div class="col-lg-2">
				    <table class="text-inverse m-b-20 width-full">
						<tbody>
						    <?php if(!empty($sub)){ $i=1; foreach($sub as $subs){ ?>
    							<tr>
    								<td>
    									<i class="fa fa-book fa-lg pull-left fa-fw m-r-10 m-t-5 p-t-2"></i>
    									<div class="m-t-4 books_list pointer"  attr-subid="<?php echo $subs->SUB_ID; ?>"><?php echo $subs->SUB_NAME; ?></div>
    								</td>
    							</tr>
    						<?php } $i++; } ?>
						</tbody>
					</table>
				    
				</div>
				<!-- end col-3 -->
				<!-- begin col-9 -->
				<div class="col-lg-10 book_div">
					
					
				</div>
				<!-- end col-9 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end #content -->
		
		
		
		<!-- View Question modal start -->
		<div class="modal fade" id="viewquestion" role="dialog">
        	<div class="modal-dialog modal-lg">
        		<div class="modal-content" style="background-color:unset">
        			<div class="modal-header" style="background-color: #348fe2 ;">
        				<h4 class="modal-title" style="color:white">View <span class='librarynm'></span> Question</h4>
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        			</div>
        			<div class="modal-body bg_grey">
        				<div class='col-md-12'>
                			<!-- begin panel -->
                			<div class="panel bg_grey">
                				<!-- begin panel-body -->
                				<div class="panel-body view_ques_modal_div">
                				
                					
                				</div>
                			</div>
                		</div>
        			</div>
        		</div>
        	</div>
        </div>
        <!-- View Question modal end -->
        
        
        <!-- Edit Question modal start -->
		<div class="modal fade" id="editquestion" role="dialog">
        	<div class="modal-dialog modal-lg">
        		<div class="modal-content" style="background-color:unset">
        			<div class="modal-header" style="background-color: #348fe2 ;">
        				<h4 class="modal-title" style="color:white">Edit <span class='librarynm'></span> Question</h4>
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        			</div>
        			<div class="modal-body bg_grey">
        				<div class='col-md-12'>
                			<!-- begin panel -->
                			<div class="panel bg_grey">
                				<!-- begin panel-body -->
                				<div class="panel-body edit_ques_modal_div" style="border: 1px solid rgba(0,0,0,.2);border-radius: 4px;background-color: white;">
                                    
                				</div>
                			</div>
                		</div>
        			</div>
        		</div>
        	</div>
        </div>
        <!-- Edit Question modal end -->
        
        
        <!-- Edit Question modal start -->
		<div class="modal fade" id="addquestion" role="dialog">
        	<div class="modal-dialog modal-lg">
        		<div class="modal-content" style="background-color:unset">
        			<div class="modal-header" style="background-color: #348fe2 ;">
        				<h4 class="modal-title" style="color:white">Add <span class='librarynm'></span> Question</h4>
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        			</div>
        			<div class="modal-body bg_grey">
        				<div class='col-md-12'>
                			<!-- begin panel -->
                			<div class="panel bg_grey">
                				<!-- begin panel-body -->
                				<div class="panel-body add_ques_modal_div" style="border: 1px solid rgba(0,0,0,.2);border-radius: 4px;background-color: white;">
                                    
                				</div>
                			</div>
                		</div>
        			</div>
        		</div>
        	</div>
        </div>
        <!-- Edit Question modal end -->