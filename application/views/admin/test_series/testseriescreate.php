<link href="<?php echo base_url(); ?>assets/adminassets/css/test_series/testseriescreate.css" rel="stylesheet" />
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Test Series</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Create</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Test Series</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class='col-md-8'>
			<!-- begin panel -->
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">NEW TEST SERIES</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<form id="testseriescreate" enctype="multipart/form-data">
					    <div class="col-md-12">
					        <div class="row">
                                <div class="col-md-6">
        							<label class="col-form-label">Test Series Name <span class="star">*</span></label>
        							<input type="text" class="form-control testseriesname" name="testseriesname" placeholder="Enter Test series name" required>
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">Cost <span class="star">*</span></label>
        							<input type="text" class="form-control cost input_num" name="cost" placeholder="Enter Test Series Cost" required>
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">No. of Test paper <span class="star">*</span></label>
        							<input type="text" class="form-control input_num" name="tspaperno" placeholder="Enter No. of Test Paper" required>
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">No. of Questions <span class="star">*</span></label>
        							<input type="text" class="form-control quesno input_num" name="quesno" placeholder="Enter No. of Questions" required>
        						</div>
        						<div class="col-md-6">
                    				<label class="col-form-label">Positive Mark <span class="star">*</span></label>
                    				<input type="text" class="form-control input_num pmark" name="pmark" placeholder="Enter Positive Mark" required>
                    		    </div>
                    		    <div class="col-md-6">
                    				<label class="col-form-label">Negative Mark <span class="star">*</span></label>
                    				<input type="text" class="form-control input_num nmark" name="nmark" placeholder="Enter Negative Mark" required>
                    		    </div>
        						<div class="col-md-6">
            						<label class="col-form-label">Start Date <span class="star">*</span></label>
            						 <input type="text" class="form-control dtpicker" name="startdate" value="" required>
        						</div>
        						<div class="col-md-6">
            						<label class="col-form-label">End Date <span class="star">*</span></label>
            						 <input type="text" class="form-control dtpicker" name="enddate" value="" required>
        						</div>
        						<!--<div class="col-md-6">-->
            		<!--				<label class="col-form-label">Validity <span class="star">*</span></label>-->
            		<!--				 <input type="text" class="form-control dtpicker" name="validity" value="" required>-->
        						<!--</div>-->
        						<div class="col-md-6 hide">
            						<label class="col-form-label">Created Date <span class="star">*</span></label>
            						 <input type="text" class="form-control dtpicker" name="createdate" value="<?php echo date('Y-m-d')?>" required>
        						</div>
        		<!--				<div class='col-sm-6'>-->
          <!--                          <label class="col-form-label">Duration </label>-->
          <!--                          <div class='input-group date' id='datetimepicker2'>-->
          <!--                              <input type='text' class="form-control duration" value="" name="duration" />-->
          <!--                              <span class="input-group-addon">-->
										<!--    <i class="fa fa-clock"></i>-->
										<!--</span>-->
          <!--                          </div>-->
          <!--                      </div>-->
                                <div class="col-md-6">
                    				<label class="col-form-label">Duration <span class="star"> Enter Duration in Minutes *</span></label>
                    				<input type="text" class="form-control input_num duration" name="duration" required >
                    		    </div>
								<div class="col-md-12 image-section">
									<div class=" row ">
										<div class="col-form-label">
											<label class="" for="course_img">Test Series Image </label>
											<div class="">
												<span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
													<i class="fas fa-plus"></i>
													<span>Add files...</span>
												</span>
												<input type="file" name="testseries_img" id="course_img" class="hide" accept="image/*">
											</div>
										</div>
										<div class="col text-right mt-2">
											<a href="javascript:;" target="_blank" ><img src="javascript:;" alt=""></a>
										</div>
									</div>    
								</div>
        						<div class="col-md-12">
        							<label class="col-form-label">Description <span class="star">*</span></label>
                                    <textarea class="summernote desc" name="desc" id="mysummernote"></textarea>
            					</div>
					        </div>
					    </div>
						<div class="form-group row m-b-10 m-t-10">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary testseriescreate">SAVE</button>
							</div>
						</div>
					</form>
				</div>
			
			</div>
			
		</div>

		
	</div>
</div>

<script>
	$(document).ready(function() {
		FormSummernote.init();
	});
</script>