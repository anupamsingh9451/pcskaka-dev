<link href="<?php echo base_url(); ?>assets/adminassets/css/test_series/testserieseditmodal.css" rel="stylesheet" />
<input type='hidden' class='tsid' name="tsid" value='<?php echo $tsdt[0]->TS_ID; ?>' />
<div class="col-md-12">
    <div class="row">
        <div class="col-md-6">
        	<label class="col-form-label">Test Series Name <span class="star">*</span></label>
        	<input type="text" class="form-control testseriesname" name="testseriesname" placeholder="Enter Test series name" required="" value="<?php echo $tsdt[0]->TS_NAME; ?>">
        </div>
        <div class="col-md-6">
        	<label class="col-form-label">Cost <span class="star">*</span></label>
        	<input type="text" class="form-control cost input_num" name="cost" placeholder="Enter Test Series Cost" value="<?php echo $tsdt[0]->TS_COST; ?>" required="">
        </div>
        <div class="col-md-6">
        	<label class="col-form-label">No. of Test paper <span class="star">*</span></label>
        	<input type="text" class="form-control input_num tspaperno" name="tspaperno" placeholder="Enter No. of Test Paper" value="<?php echo $tsdt[0]->TS_TEST_PAPER_NOS; ?>" required="">
        </div>
        <div class="col-md-6">
        	<label class="col-form-label">No. of Questions <span class="star">*</span></label>
        	<input type="text" class="form-control quesno input_num" name="quesno" placeholder="Enter No. of Questions" value="<?php echo $tsdt[0]->TS_QUESTION_NOS; ?>" required="">
        </div>
        <div class="col-md-6">
        <label class="col-form-label">Positive Mark <span class="star">*</span></label>
        <input type="text" class="form-control input_num pmark" name="pmark" placeholder="Enter Positive Mark" value="<?php echo $tsdt[0]->TS_P_MARKS; ?>" required>
        </div>
        <div class="col-md-6">
        <label class="col-form-label">Negative Mark <span class="star">*</span></label>
        <input type="text" class="form-control input_num nmark" name="nmark" placeholder="Enter Negative Mark" value="<?php echo $tsdt[0]->TS_N_MARKS; ?>" required>
        </div>
        <div class="col-md-6">
        	<label class="col-form-label">Start Date <span class="star">*</span></label>
        	 <input type="text" class="form-control dtpicker startdate" name="startdate" value="<?php echo date('Y-m-d',strtotime($tsdt[0]->TS_START_DATE)); ?>" required="">
        </div>
        <div class="col-md-6">
        	<label class="col-form-label">End Date <span class="star">*</span></label>
        	 <input type="text" class="form-control dtpicker enddate" name="enddate" value="<?php echo date('Y-m-d',strtotime($tsdt[0]->TS_END_DATE)); ?>" required="">
        </div>
        <div class="col-md-6">
			<label class="col-form-label">Duration <span class="star"> Enter Duration in Minutes *</span></label>
			<input type="text" class="form-control input_num duration" name="duration" value="<?php echo $tsdt[0]->TS_DURATION; ?>" required>
	    </div>
        <div class="col-md-6">
        	<label class="col-form-label">Status <span class="star">*</span></label>
        	 <select class="form-control tsstatus" name="tsstatus" data-style="btn-white" required="">
                <option value="1" <?php if($tsdt[0]->TS_STATUS==1){ echo 'selected'; } ?> >Active</option>
                <option value="0" <?php if($tsdt[0]->TS_STATUS==0){ echo 'selected'; } ?> >Inactive</option>
        	</select>
        </div>
        <div class="col-md-12 image-section">
            <div class=" row ">
                <div class="col-form-label">
                    <label class="" for="course_img">Test Series Image </label>
                    <div class="">
                        <span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
                            <i class="fas fa-plus"></i>
                            <span>Add files...</span>
                        </span>
                        <input type="file" name="testseries_img" id="course_img" class="hide" accept="image/*">
                    </div>
                </div>
                <div class="col text-right mt-2">
                    <a href="javascript:;" target="_blank" ><img src="<?php echo $tsdt[0]->TS_IMG; ?>" alt=""></a>
                </div>
            </div>    
        </div>
        <div class="col-md-12">
        	<label class="col-form-label">Description <span class="star">*</span></label>
            <textarea class="summernote desc" name="desc" id="mysummernote" ><?php echo $tsdt[0]->TS_DESCRIPTION; ?></textarea>
        </div>
    </div>
</div>

<script>
	$(document).ready(function() {
		App.init();
        FormSummernote.init();
        $('img[src!=""]').css({'max-width':'120px','max-height':'80px'});
        $('.fileinput-button').on('click', function(e) {
            e.preventDefault();
            $(this).parent().find('input[type=file]').trigger('click');
        });
	});
</script>