<script>
	$('document').ready(function(){
	    $('#datetimepicker2').datetimepicker({
            format: 'HH:mm',
            
        });
		$('body').on('click','.testseriesedit',function(){
            var tsid = $(this).attr('attr-tsid');
            $.ajax({
        		type: 'GET',
        		url: base_loc + 'admingetajax/get_testseries_edit_modalhtml',
        		data: 'tsid='+tsid,
        		async:false,
        		headers: {
        			'Client-Service': clientserv,
        			'Auth-Key': apikey,
        			'User-ID': loginid,
        			'Authorization': token,
        			'type': type
        		},
        		success: function (msg) {
        			$('.edittestseries_modal_div').html(msg);
        			$('#edittestseries').modal('show');
        		},
        		error: function (msg) {
        			if (msg.responseJSON['status'] == 303) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 401) {
        				location.href = base_loc; 
        			}
        			if (msg.responseJSON['status'] == 400) {
        				location.href = base_loc; 
        			}
        		}
        	});
		});
		testserieslists();
	});
	
	function testserieslists(){
		$('#testserieslist').dataTable().fnDestroy();
		$('#testserieslist').DataTable({
			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"ajax":{
				"url":base_loc + 'admingetajax/testserieslist',
				"type": "GET",
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"TS_NAME"},
				{"data":"TS_TEST_PAPER_NOS"},
				{"data":"TS_CREATED_AT"},
				{"data":"TS_STATUS"},
				{"data":"TOOLS"},
			]
		});
	}
</script>