
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Student</a></li>
		<li class="breadcrumb-item active">list</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">LIST ALL STUDENT</h1>
	<!-- begin page-header -->
	<!-- end page-header -->
	<div class="row no_mrgn addbx_top_rowhead pd_15">
		<div class="col-md-12">
		    <div class="row">
				<div class="col-md-3">
					<label class="col-form-label">Student Type <span class="star">*</span></label>
					<select class="form-control student_type" data-style="btn-white" >
                        <option value="2">All</option>
                        <option value="0">Free</option>
                        <option value="1">Paid</option>
					</select>
				</div>
			    <div class="col-md-3 tsdiv" style="display:none;">
					<label class="col-form-label">Test Series <span class="star">*</span></label>
					<select class="form-control testseries" data-style="btn-white">
                       
					</select>
				</div>
				<div class="col-md-3">
					<label class="col-form-label">Status <span class="star">*</span></label>
					<select class="form-control status" data-style="btn-white">
                        <option value="2">All</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
					</select>
				</div>
				<div class="col-md-3 p-30">
					<button type="button" class="btn btn-success searchstudent"><i class="fa fa-fw fa-search"></i> Search</button>
				</div>
			</div>
		</div>
	</div>
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Student List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="studentlist" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th width="1%">Sr No.</th>
								<th>Student Name</th>
								<th>Contact</th>
								<th>Email</th>
								<th>Created Date</th>
								<th>Buyed Test Series</th>
								<th>Submitted test paper</th>
								<th>Status</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-6 -->
		
	</div>
</div>
