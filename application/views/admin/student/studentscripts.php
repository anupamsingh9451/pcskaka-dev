<script>
	$('document').ready(function(){
        get_all_testseries();
        
		
		$('body').on('change','.student_type', function() {
		    var type = $('option:selected',this).val();
		    
		    if(type==1){
		        $('.tsdiv').attr('style','display:block;');
		    }else{
		        $('.tsdiv').attr('style','display:none;');
		    }
		});
		$('body').on('click','.searchstudent', function() {
		   var status = $('option:selected','.status').val();
		   var tsid = $('option:selected','.testseries').val();
		   var student_type = $('option:selected','.student_type').val();
		   $('#studentlist').dataTable().fnDestroy();
		   $('#studentlist').DataTable({
    			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
    			"buttons": [
    				'excel', 'pdf', 'print'
    			],
    			"ajax":{
    				"url":base_loc + 'admingetajax/studentlist',
    				"type": "GET",
    				"data":{ status:status,tsid:tsid,student_type:student_type },
    				"headers": {
    					'Client-Service': clientserv,
    					'Auth-Key': apikey,
    					'User-ID': loginid,
    					'Authorization': token,
    					'type': type
    				},
    				"error": function (msg) {
        				if (msg.responseJSON['status'] == 303) {
        					location.href = base_loc;
        				}
        				if (msg.responseJSON['status'] == 401) {
        					location.href = base_loc; 
        				}
        				if (msg.responseJSON['status'] == 400) {
        					location.href = base_loc; 
        				}
        			}
    			},
    			"columns":[
    				{"data":"SR_NO"},
    				{"data":"USER_NAME"},
    				{"data":"USER_CONTACT"},
    				{"data":"USER_EMAIL"},
    				{"data":"USER_ENTRY_TT"},
    				{"data":"BUYED_TEST_SERIES_NO"},
    				{"data":"SUBMITTED_TP_NO"},
    				{"data":"USER_STATUS"},
    				{"data":"TOOLS"},
    			]
		    });
		});
		
	});
	
	function testserieslists(){
		$('#studentlist').DataTable({
			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"ajax":{
				"url":base_loc + 'admingetajax/studentlist',
				"type": "GET",
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"USER_NAME"},
				{"data":"USER_CONTACT"},
				{"data":"USER_EMAIL"},
				{"data":"USER_ENTRY_TT"},
				{"data":"BUYED_TEST_SERIES_NO"},
				{"data":"USER_STATUS"},
				{"data":"TOOLS"},
			]
		});
	}
	function get_all_testseries(){
         $.ajax({
        		type: 'GET',
        		url: base_loc + 'admingetajax/get_all_testseries',
        		headers: {
        			'Client-Service': clientserv,
        			'Auth-Key': apikey,
        			'User-ID': loginid,
        			'Authorization': token,
        			'type': type
        		},
        		success: function (msg) {
        		    
        		    $('.testseries option').remove();
        			var str='';
        			if(msg.status==200){
        			    if(msg.data.length>0){
        			        var tsidarr = new Array();
        					$.each(msg.data, function(index, element) {
        						str += '<option value="'+element.TS_ID+'">'+element.TS_NAME+'</option>';
        						tsidarr.push(element.TS_ID);
        					});
        					var tsids = tsidarr.join();
        		            var str1 = "<option selected value='" + tsids + "'>"+'All'+"</option>";
            				$('.testseries').append(str1);
            				$('.testseries').append(str);
            				$('.searchstudent').trigger('click');
        			    }
        			}else{
        				location.href = base_loc+"ratan/dashboard";
        			}
        		},
        		error: function (msg) {
        			if (msg.responseJSON['status'] == 303) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 401) {
        				location.href = base_loc; 
        			}
        			if (msg.responseJSON['status'] == 400) {
        				location.href = base_loc; 
        			}
        		}
        	});
     }
</script>