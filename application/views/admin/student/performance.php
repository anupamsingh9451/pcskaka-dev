
<style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 12px 15px !important;
    }
</style>
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Student</a></li>
		<li class="breadcrumb-item active">Performance</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">PERFORMANCE OF <span style="color: #dc3041;font-weight: 700;"><?php echo $userdt['data'][0]->USER_FNAME." ".$userdt['data'][0]->USER_LNAME; ?></h1>
	<!-- begin page-header -->

	<div class="row">
			    <div class="col-md-12">
				    <div class="panel panel-inverse" data-sortable-id="table-basic-9">
						<!-- begin panel-heading -->
						<div class="panel-heading ui-sortable-handle">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Student Performance</span></h4>
						</div>
						<!-- end panel-heading -->
						<!-- begin panel-body -->
						<div class="panel-body">
							<!-- begin table-responsive -->
							<div class="table-responsive">
								<table class="table m-b-0" style="border: 1px solid #cecece;">
									<thead>
										<tr>
											<th>#</th>
											<th>Test Paper Name</th>
											<th>Total Ques</th>
											<th>Attempted Ques</th>
											<th>Marked Ques</th>
											<th>Skipped Ques</th>
											<th>Not Visited Ques</th>
											<th>True Ques</th>
											<th>Wrong Ques</th>
											<th>Obtained marks</th>
											<th>Taken Time [MM:SS]</bt></th>
											<th>Rank</th>
										</tr>
									</thead>
									<tbody>
									    <?php if (!empty($result['data'])) {
    $class = array("active","info","success","warning","danger");
    $i = 1;
    foreach ($result['data'] as $results) {
        shuffle($class); ?>
    										<tr class="<?php echo $class[0]; ?>">
    											<td><?php echo $i; ?></td>
    											<td><?php echo $results['TP_NAME']; ?></td>
    											<td><?php echo $results['RESULT']['total_ques']; ?></td>
    											<td><?php echo $results['RESULT']['attempted']; ?></td>
    											<td><?php echo $results['RESULT']['boomark']; ?></td>
    											<td><?php echo $results['RESULT']['skipped']; ?></td>
    											<td><?php echo $results['RESULT']['not_visited']; ?></td>
    											<td><?php echo $results['RESULT']['number_of_true_ques']; ?></td>
    											<td><?php echo $results['RESULT']['number_of_wrong_ques']; ?></td>
    											<td><?php echo $results['RESULT']['obtained_mark']; ?></td>
    											<td><?php echo $results['RESULT']['duration']; ?></td>
    											<td><?php echo $results['RESULT']['RANK']; ?></td>

    										</tr>
										<?php $i++;
    }
} else { ?>
    										<tr class="info" style="text-align:center;">
    											<td colspan="12">This Student has Not attempted any Test Paper Yet!</td>
    										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<!-- end table-responsive -->
						</div>
						<!-- end panel-body -->
						
					</div>
				</div>
			</div>
</div>
