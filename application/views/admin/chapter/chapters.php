<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Chapter</a></li>
		<!--<li class="breadcrumb-item active"><a href="javascript:;">Create</a></li>-->
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Chapter</h1>
	<!-- end page-header -->
	<div class="row no_mrgn addbx_top_rowhead pd_15">
		<div class="col-md-8">
		    <div class="row">
		        
			    <h5 class="">Liberary Name &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; </h5><span class=""><?php echo $chapterdt['data'][0]->LIB_NAME; ?></span>
			</div>
			<div class="row">
			    <h5 class="">Subject Name &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php echo $chapterdt['data'][0]->SUB_NAME; ?></span>
			</div>
			<div class="row">
			    <h5 class="">Book Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $chapterdt['data'][0]->BOOK_NAME; ?></span>
	        </div>
		</div>
		
		<div class="col-md-4 text-right">
			<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#addchapter" ><i class="fa fa-fw fa-hdd"></i> Add Chapter</button>
		</div>
	
	</div>
	<!-- begin row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Chapter List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="chapterlist" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th width="1%">Sr No.</th>
								<th>Chapter Name</th>
								<!--<th>Book Name</th>-->
								<!--<th>Subject Name</th>-->
								<!--<th>Library Name</th>-->
								<th>No. of Question</th>
								<!--<th>Description</th>-->
								<th>Created Date</th>
								<th>Status</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		
	</div>
</div>

<!-- Add Chapter Modal -->

<div class="modal fade" id="addchapter" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #e40000;">
				<h4 class="modal-title" style="color:white">Add <span class='librarynm'></span> Chapter</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body bg_grey">
				<div class='col-md-12'>
        			<!-- begin panel -->
        			<div class="panel" style="border-radius: 10px;border: 1px solid #ccc6c6;">
        				<!-- begin panel-body -->
        				<div class="panel-body">
        				
        					<form id="chaptercreate">
        					    <div class="row">
        					        <div class="col-md-6">
            							<label class="col-form-label">Chapter Name <span class="star">*</span></label>
            							<input type="text" class="form-control chaptername" name="chaptername" placeholder="Enter Chapter name" required>
        						    </div>
        						    
            						<div class="col-md-6 hide">
            							<label class="col-form-label">Library <span class="star">*</span></label>
            							<select class="form-control" name="library" data-style="btn-white" required>
                                            <option selected value="<?php echo $chapterdt['data'][0]->LIB_ID; ?>"><?php echo $chapterdt['data'][0]->LIB_NAME; ?></option>
            							</select>
            						</div>
            						<div class="col-md-6 hide">
            							<label class="col-form-label">Subject <span class="star">*</span></label>
        								<select class="form-control" name="subject" data-style="btn-white" required>
                                            <option selected value="<?php echo $chapterdt['data'][0]->SUB_ID; ?>"><?php echo $chapterdt['data'][0]->SUB_NAME; ?></option>
        								</select>
                					</div>	
                					<div class="col-md-6 hide">
            							<label class="col-form-label">Book <span class="star">*</span></label>
        								<select class="form-control" name="book" data-style="btn-white" required>
                                            <option selected value="<?php echo $chapterdt['data'][0]->BOOK_ID; ?>"><?php echo $chapterdt['data'][0]->BOOK_NAME; ?></option>
        								</select>
                					</div>
                					
                					<div class="col-md-6">
            							<label class="col-form-label">Total Question <span class="star">*</span></label>
            							<input type="text" class="form-control totalques input_num" name="totalques" placeholder="Enter Total Number of Question" required>
                					</div>
                					
                					<div class="col-md-6">
                						<label class="col-form-label">Created Date <span class="star">*</span></label>
                						<input type="text" class="form-control dtpicker" name="chaptercreatedate" value="<?php echo date('Y-m-d')?>" required>
                					</div>
                					<div class="col-md-6">
            							<label class="col-form-label">Chapter Description <span class="star">*</span></label>
                                        <textarea rows="4" class="form-control chapter_desc" name="chapter_desc"></textarea>
                					</div>
            					</div>
            					<div class="row" style="margin-top:10px">
        							<div class="col-md-12 text-center">
        								<button type="submit" class="btn btn-primary chaptercreate">SAVE</button>
        							</div>
        						</div>
        					</form>
        				
        				</div>
        			
        			</div>
        			
        		</div>
			</div>
				
		</div>
	</div>
</div>

<!-- Edit Chapter Modal -->
<div class="modal fade" id="editchapter">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">EDIT <span class='booknm'></span> Chapter</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="editchapterform">
				<div class="modal-body">
					<input type='hidden' class='chapterid' name="chapterid"  />
					<input type='hidden' class='bookid' name="bookid"  />
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Chapter Name<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 chapternm" name="chapternm" placeholder="Enter Chapter name here" required>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">No. of Question<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 quesno" name="quesno" placeholder="Enter Number of Question" required>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Status<span class="star">*</span></label>
						<div class="col-md-9">
							<select name="chapterstatus" class='form-control chapterstatus'>
								<option value='1'>Active</option>
								<option value='0'>Inactive</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success editchapterform">Update</a>
				</div>
			</form>
		</div>
	</div>
</div>