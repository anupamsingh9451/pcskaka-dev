<script>
	$('document').ready(function(){
		/* for edit company detail*/
		$('body').on('click','.chapteredit',function(){
			$('#editchapter').find('.quesno').val($(this).attr('attr-quesno'));
			$('#editchapter').find('.chapterstatus').val($(this).attr('attr-status'));
			$('#editchapter').find('.bookid').val($(this).attr('attr-bookid'));
			$('#editchapter').find('.chapternm').val($(this).attr('attr-chapternm'));
			$('#editchapter').find('.chapterid').val($(this).attr('attr-chapterid'));
			$('#editchapter').modal('show');
		});
		// to get dcn all areas predifined
		var autocompletearea = [];
	    getlibrary();
		updatelistchapter();
	});
	
	function updatelistchapter(){
	    var bookid = <?php echo $this->uri->segment(3); ?>;
		$('#chapterlist').dataTable().fnDestroy();
		$('#chapterlist').DataTable({
			"dom": 'Bfrtip,l',
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"ajax":{
				"url":base_loc + 'admingetajax/listchapter',
				"type": "GET",
				"data": { bookid:bookid  },
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"CHAPTER_NAME"},
				{"data":"CHAPTER_QUE_NOS"},
				{"data":"CHAPTER_CREATED_AT"},
				{"data":"CHAPTER_STATUS"},
				{"data":"TOOLS"},
			]
		});
	}
</script>