<link href="<?php echo base_url(); ?>assets/adminassets/css/coupon/couponlist.css" rel="stylesheet" />
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Coupon</a></li>
		<li class="breadcrumb-item active">list</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">LIST ALL COUPON</h1>
	<!-- begin page-header -->
	<!-- end page-header -->
	
	
	
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Coupon List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="couponlist" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th width="1%">Sr No.</th>
								<th>Coupon Title</th>
								<th>Product Type</th>
								<th>Coupon Type</th>
								<th>Mode</th>
								<th>Amount</th>
								<th>Coupon Start Date</th>
								<th>STATUS</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-6 -->
		
	</div>
</div>
		<!-- end #content -->
<div class="modal fade" id="editcouponmodel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">EDIT COUPON</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="editcoupon">
				<div class="modal-body">
				    <div class="col-md-12">
				        <input type="hidden" value="" class="couponid" name="couponid" >
						<input type="hidden" class="form-control cpntitle" name="cpnoldtitle" >
				        <div class="row">
				            <div class="col-md-6">
    							<label class="col-form-label">Coupon Title <span class="star">*</span></label>
    							<input type="text" class="form-control cpntitle" name="cpntitle" >
    						</div>
    						<div class="col-md-6">
    							<label class="col-form-label">Coupon Mode <span class="star">*</span></label>
    							<select class="form-control coupon_type" name="coupon_type" data-style="btn-white" required>
    							    <option value="">Select Coupon Type</option>
                                    <option value="1">Promo Code</option>
                                    <option value="2">Discount</option>
    							</select>
        					</div>
    						<div class="col-md-6">
    							<label class="col-form-label">Product Type <span class="star">*</span></label>
    							<select class="form-control prod_type" name="prod_type" data-style="btn-white" required>
    							    <option value="">Select Prod Type</option>
                                    <?php  if (!empty($prod_type)) {
    $alloption="";
    $option="";
    foreach ($prod_type as $prod_types) {
        $option.="<option value='".$prod_types->PROD_TYPE_ID."'>".$prod_types->PROD_TYPE_NAME."</option>";
        $alloption.=$prod_types->PROD_TYPE_ID.",";
    }
    echo "<option value='".rtrim($alloption, ",")."'>All</option>";
    echo $option;
} ?>
    							</select>
        					</div>
    						<div class="col-md-6">
							    <label class="col-form-label">Product </label>
								<select class="multiple-select2 form-control cpnproduct" name="cpnproduct[]" multiple="multiple">
								    
								</select>
							</div>
				            <div class="col-md-6">
    							<label class="col-form-label">Coupon Mode <span class="star">*</span></label>
    							<select class="form-control cpnmode" name="cpnmode" data-style="btn-white" required>
    							    <option value="">Select Mode</option>
                                    <option value="1">Flat</option>
                                    <option value="2">Percent</option>
    							</select>
        					</div>
    						
    						<div class="col-md-6">
    							<label class="col-form-label">Coupon Amount <span class="star">*</span></label>
    							<input type="text" class="form-control cpnamt input_num" name="cpnamt"  >
    						</div>
    						<div class="col-md-6">
        						<label class="col-form-label">Start Date <span class="star">*</span></label>
        						 <input type="text" class="form-control dtpicker startdate" name="startdate" value="" required="">
    						</div>
    						<div class="col-md-6">
        						<label class="col-form-label">End Date <span class="star">*</span></label>
        						 <input type="text" class="form-control dtpicker enddate" name="enddate" value="" required="">
    						</div>
    						<div class="col-md-6">
    							<label class="col-form-label">Coupon Usage Limit </label>
    							<input type="text" class="form-control cpn_usage_limit input_num" name="cpn_usage_limit" >
    						</div>
    						<div class="col-md-6">
    							<label class="col-form-label">Coupon Usage Limit for per User </label>
    							<input type="text" class="form-control cpn_usage_limit_peruser input_num" name="cpn_usage_limit_peruser"  >
    						</div>
    						<div class="col-md-6">
    							<label class="col-form-label">Coupon Status <span class="star">*</span></label>
    							<select class="form-control cpn_status" name="cpn_status" data-style="btn-white" required>
    							    <option value="">Select Status</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
    							</select>
        					</div>
    						<div class="col-md-6">
    							<label class="col-form-label">Coupon Description <span class="star">*</span></label>
                                <textarea rows="3" class="form-control cpn_desc" name="cpn_desc"></textarea>
        					</div>
				        </div>
				    </div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success editcoupon">Update</a>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		FormPlugins.init();
		
		$(".cpnproduct").attr("data-placeholder","Select Product");
        $(".cpnproduct").select2();
        $('.cpnproduct').on('change' , function(){
            if($('option:selected',this).html()=="All"){
                var value = $('option:selected',this).val();
                $('.cpnproduct').val(null);
                $(".cpnproduct").val(value);
                $(".cpnproduct").select2();
                
                $(".cpnproduct option[attr-opt='2']").prop('disabled','disabled');
                $(".cpnproduct").select2();
                
            }else{
                $(".cpnproduct option[attr-opt='2']").prop('disabled',false);
                $(".cpnproduct").select2();
            }
        });
	});
</script>