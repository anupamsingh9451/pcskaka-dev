<script>
	$(document).ready(function() {
        listcoupon();
        $('body').on('change','.prod_type', function(){
            var prod_type = $('option:selected',this).val();
            get_product_by_prodtype(prod_type);
        });
        $('body').on('click','.couponedit', function(){
            var couponid = $(this).attr('attr-couponid');
            get_coupon_edit_modal(couponid);
            $('#editcouponmodel').modal('show');
        });
	});
	function listcoupon(){
		$('#couponlist').dataTable().fnDestroy();
		$('#couponlist').DataTable({
			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"ajax":{
				"url":base_loc + 'admingetajax/listcoupon',
				"type": "GET",
				"data": { loginid: loginid},
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"COUPON_TITLE"},
				{"data":"PROD_TYPE_NAME"},
				{"data":"COUPON_TYPE"},
				{"data":"COUPON_MODE"},
				{"data":"COUPON_AMOUNT"},
				{"data":"COUPON_START_DATE"},
				{"data":"COUPON_STATUS"},
				{"data":"TOOLS"},
			]
		});
	}
	function get_product_by_prodtype(prod_type){
        var stdtype = $('option:selected','.stdtypesched').val();
        $.ajax({
    		type: 'GET',
    		data:{'prod_type': prod_type},
    		url: base_loc + 'admingetajax/get_product_by_prodtype',
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
		        $('.cpnproduct').html('');
    		    $(".cpnproduct").select2();
    			var str='';
    			if(msg.status==200){
    			    if(msg.data.length>0){
    			        var productidarr = new Array();
    					$.each(msg.data, function(index, element) {
    					    if(msg.prod_type==1){
    					        str += '<option attr-html="single" attr-opt="2" value="'+element.LIB_ID+'">'+element.LIB_NAME+'</option>';
    					        productidarr.push(element.LIB_ID);
    					    }
    						if(msg.prod_type==2){
    					        str += '<option attr-html="single" attr-opt="2" value="'+element.SUB_ID+'">'+element.SUB_NAME+'</option>';
    					        productidarr.push(element.SUB_ID);
    					    }
    					    if(msg.prod_type==3){
    					        str += '<option attr-html="single" attr-opt="2" value="'+element.BOOK_ID+'">'+element.BOOK_NAME+'</option>';
    					        productidarr.push(element.BOOK_ID);
    					    }
    					    if(msg.prod_type==4){
    					        str += '<option attr-html="single" attr-opt="2" value="'+element.TS_ID+'">'+element.TS_NAME+'</option>';
    					        productidarr.push(element.TS_ID);
    					    }
    					});
    					var productids = productidarr.join();
    		            var str1 = "<option attr-opt='1' attr-html='All' value='" + productids + "'>"+'All'+"</option>";
    				    $('.cpnproduct').append(str1);
        				$('.cpnproduct').append(str);
        				$(".cpnproduct").select2();
    			    }
    			}else{
    				location.href = base_loc;
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
    }
    
    function get_coupon_edit_modal(couponid){
        var stdtype = $('option:selected','.stdtypesched').val();
        $.ajax({
    		type: 'GET',
    		data:'couponid='+ couponid,
    		url: base_loc + 'admingetajax/get_coupon_dt_by_couponid',
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
		        $('.cpnproduct').html('');
    		    $(".cpnproduct").select2();
    			var str='';
    			if(msg.status==200){
    			    if(msg.data.length>0){
    			        var data = msg.data[0];
    			        $('#editcoupon').find('.prod_type').val(data.PROD_TYPE_ID);
    			        $('.prod_type').trigger('change');
    			        $('#editcoupon').find('.couponid').val(data.COUPON_ID);
    			        $('#editcoupon').find('.cpntitle').val(data.COUPON_TITLE);
    			        $('#editcoupon').find('.cpnmode').val(data.COUPON_MODE);
    			        $('#editcoupon').find('.cpnamt').val(data.COUPON_AMOUNT);
    			        $('#editcoupon').find('.startdate').val(data.COUPON_START_DATE);
    			        $('#editcoupon').find('.enddate').val(data.COUPON_END_DATE);
    			        $('#editcoupon').find('.cpn_usage_limit').val(data.COUPON_USAGE_LIMIT);
    			        $('#editcoupon').find('.cpn_usage_limit_peruser').val(data.COUPON_LIMIT_PER_USER);
    			        $('#editcoupon').find('.cpn_desc').val(data.COUPON_DESC);
    			        $('#editcoupon').find('.coupon_type').val(data.COUPON_TYPE);
    			        $('#editcoupon').find('.cpn_status').val(data.COUPON_STATUS);
    			        
    			        if(msg.prod_type==1){
					        str += '<option attr-html="single" attr-opt="2" value="'+element.LIB_ID+'">'+element.LIB_NAME+'</option>';
					        productidarr.push(element.LIB_ID);
					    }
						if(msg.prod_type==2){
					        str += '<option attr-html="single" attr-opt="2" value="'+element.SUB_ID+'">'+element.SUB_NAME+'</option>';
					        productidarr.push(element.SUB_ID);
					    }
					    if(msg.prod_type==3){
					        str += '<option attr-html="single" attr-opt="2" value="'+element.BOOK_ID+'">'+element.BOOK_NAME+'</option>';
					        productidarr.push(element.BOOK_ID);
					    }
					    if(msg.prod_type==4){
					        str += '<option attr-html="single" attr-opt="2" value="'+element.TS_ID+'">'+element.TS_NAME+'</option>';
					        productidarr.push(element.TS_ID);
					    }
    			        
					}
					var alluserval = $('.cpnproduct').find("option[attr-html='All']").val();
					if(alluserval==data.PRODUCT){
						$(".cpnproduct").val(data.PRODUCT).trigger('change');
					}else{
						var couponidsstr = data.PRODUCT;
						couponidsarr = couponidsstr.split(',');
						$(".cpnproduct").val(couponidsarr);
						$(".cpnproduct").trigger('change');
					}
    			}else{
    				location.href = base_loc;
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
    }
</script>