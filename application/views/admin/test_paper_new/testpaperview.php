<link href="<?php echo base_url(); ?>assets/adminassets/css/test_paper_new/testpaperview.css" rel="stylesheet" />
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Test Paper</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">View</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Test Paper Details</h1>
	<!-- end page-header -->
	<div class="col-md-12 no_mrgn addbx_top_rowhead pd_15">
		    <div class="row">
		        <div class="col-md-3">
		              <h5 class="">Test Series Name &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo $tsdt[0]->TS_NAME; ?></span></h5>
		        </div>
		        <div class="col-md-3">
            		   <h5 class="">Test Paper Name &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo $tpdt[0]->TP_NAME; ?></span></h5>
            	</div>
            	<!--<div class="col-md-3">-->
            	<!--	   <h5 class="">Positive Mark &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo $tpdt[0]->TP_P_MARKS; ?></span></h5>-->
            	<!--</div>-->
            	<!--<div class="col-md-3">-->
            	<!--	   <h5 class="">Negative Mark &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo $tpdt[0]->TP_N_MARKS; ?></span></h5>-->
            	<!--</div>-->
            	<div class="col-md-3">
            		   <h5 class="">Start Date &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo date('d-m-Y', strtotime($tpdt[0]->TP_START_DATE)); ?></span></h5>
            	</div>
            	<div class="col-md-3">
            		   <h5 class="">End Date &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo date('d-m-Y', strtotime($tpdt[0]->TP_END_DATE)); ?></span></h5>
            	</div>
            	<div class="col-md-3">
            	    <h5 class="">Created Date &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo date('d-m-Y', strtotime($tpdt[0]->TP_CREATED_AT)); ?></span></h5></h5>
            	</div>
            	<div class="col-md-3">
            		   <h5 class="">Descreptione &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo $tpdt[0]->TP_DESCRIPTION; ?></span></h5>
            	</div>
            	<div class="col-md-3">
            		   <h5 class="">Test Paper Type &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php if ($tpdt[0]->TP_PAY_STATUS==1) {
    echo "Free";
} else {
    echo "Not Free";
} ?></span></h5>
            	</div>
			</div>
	    
		<div class="col-md-12 text-right">
		 <?php if ($tpdt[0]->TP_STATUS==1 && $tpdt[0]->TP_APPROVED_BY=='') { ?>
		    <a class="btn btn-success publish_testpaper" attr-publish="1" attr-tpid="<?php echo $tpdt[0]->TP_ID; ?>" href="javascript:void(0)" ><i class="fa fa-fw fa-hdd"></i> Publish</a>
		    <!--<a class="btn btn-success publish_testpaper" attr-publish="0" attr-tpid="<?php echo $tpdt[0]->TP_ID; ?>" href="javascript:void(0)" ><i class="fa fa-fw fa-hdd"></i> Unpublish</a>-->
	     <?php } elseif ($tpdt[0]->TP_STATUS==0) { ?>
		    <a class="btn btn-info" disabled="disabled" attr-publish="0" attr-tpid="<?php echo $tpdt[0]->TP_ID; ?>" href="javascript:void(0)" ><i class="fa fa-fw fa-hdd"></i> Publish</a>
	     <?php } ?>
		    <a class="btn btn-warning" href="<?php echo base_url(); ?>ratan/testpaper/testpaperedit/<?php echo $tpdt[0]->TP_ID; ?>/<?php echo $tsdt[0]->TS_ID; ?>" ><i class="fa fa-fw fa-hdd"></i> Test Paper Edit</a>
		</div>
		 
	</div>
	<!-- begin row -->
	<div class="row">
        <div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Test Paper Questions List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive" style="padding:15px 0 0 0;max-height: 425px;">
				   <div class="col-md-12">
                        <div class="row">
                            <table class="table table-striped table-bordered">
                            	<thead>
                            		<tr>
                            			<th width="6%">Sr. No.</th>
                            			<th width="70%" >Question</th>
                            			<th>Subject</th>
                            			<th>Difficulty level</th>
                            		</tr>
                            	</thead>
                            	<tbody>
                            	    <?php $i=1; foreach ($quesdt as $quesdts) { ?>
                                    	<tr class="">
                                			<td width="6%" style="font-weight:700;"><?php echo $i.'.'; ?></td>
                                			<td style="font-size: 14px;font-weight: 700;" width="70%"><?php if ($quesdts->QUES_TYPE==1) {
    echo $quesdts->QUES;
} else {
    echo json_decode($quesdts->QUES)->QUESTION;
} ?></td>
                                			<td style="font-size: 14px;font-weight: 700;"><?php echo $quesdts->SUB_NAME; ?><br><span style="font-size: 11px;font-weight: 500; ">(<?php echo $quesdts->BOOK_NAME; ?>)</span></td>
                                			<td style="font-size: 13px;font-weight: 600;"><?php if ($quesdts->QUES_DIFFICULTY_LEVEL==1) {
    echo "LOW";
} elseif ($quesdts->QUES_DIFFICULTY_LEVEL==2) {
    echo "MEDIUM";
} else {
    echo "HIGH";
}?></td>
                                		</tr>
                                	<?php $i++; } ?>
                            	</tbody>
                            </table>
                        </div>
                    </div>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
	</div>
</div>