<script>
$('document').ready(function(){
    $('body').on('click','.publish_testpaper',function(){
		 var tpid = $(this).attr('attr-tpid');
		 var tppublish = $(this).attr('attr-publish');
		 publish_test_paper(tpid,tppublish);
	});
});

	function publish_test_paper(tpid,tppublish){
	    $.ajax({
			type: 'POST',
			url: base_loc + 'adminpostajax/publishtestpaper',
			data: 'tpid='+tpid+'&tppublish='+tppublish,
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
			success: function (msg) {
				if(msg.data=="ok"){
				    if(tppublish==1){
				        GritterNotification("Success","TEST PAPER PUBLISHED SUCCESSFULLY","my-sticky-class");
				    }else{
				        GritterNotification("Success","TEST PAPER UNPUBLISHED SUCCESSFULLY","my-sticky-class");
				    }
					location.reload()
				}else{
					GritterNotification("Error",msg.message,"my-sticky-class");
				}
			},
			error: function (msg) {
				if (msg.responseJSON['status'] == 303) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 401) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 400) {
					location.href = base_loc;
				}
			}
		});
	}

    
</script>