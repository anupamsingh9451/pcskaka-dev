<!-- begin #content -->
<link href="<?php echo base_url(); ?>assets/adminassets/css/question/edit_ques.css" rel="stylesheet" />
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Question</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Edit</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Question</h1>
	<!-- end page-header -->
	<div class="row no_mrgn addbx_top_rowhead pd_15">
		<div class="col-md-8">
		    
			<div class="row">
			    <h5 class="">Subject Name &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php echo $quesdt[0]->SUB_NAME; ?></span>
			</div>
			<div class="row">
			    <h5 class="">Book Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $quesdt[0]->BOOK_NAME; ?></span>
	        </div>
		</div>
		
		<div class="col-md-4 text-right">
		    <?php if ($this->uri->segment(5)=='user') {
    $url="/ques_user/question";
    $url2 = 'user';
} else {
    $url="/question/".$this->uri->segment(6)."";
    $url2 = 'main/'.$this->uri->segment(6);
} ?>
		    <a class="btn btn-primary" href="<?php echo base_url().'ratan'.$url; ?>"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
		</div>
	
	</div>
	<!-- begin row -->
	<div class="row">
        <div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Edit Question</h4>
				</div>
			    <div class="panel-body">
			        <div class="row">
			            <div class="col-md-12">
			                <form id="questionedit">
			                    <input type="hidden" name="quesid" value="<?php echo $quesdt[0]->QUES_ID; ?>">
        					    <div class="row">
        					        <div class="col-md-3">
            							<label class="col-form-label">Question Type <span class="star">*</span></label>
            							<select class="form-control ques_type" name="ques_type" data-style="btn-white" required>
                                            <option  value="">--Select Question Type--</option>
                                            <option value="1" <?php if ($quesdt[0]->QUES_TYPE==1) {
    echo 'selected';
}?> >Subjective</option>
                                            <option  value="2" <?php if ($quesdt[0]->QUES_TYPE==2) {
    echo 'selected';
}?>>Objective</option>
            							</select>
                					</div>
                					<div class="col-md-3">
            							<label class="col-form-label">Level of Question <span class="star">*</span></label>
            							<select class="form-control ques_levele" name="ques_levele" data-style="btn-white" required>
                                            <option  value="">--Select Question level--</option>
                                            <option  value="1" <?php if ($quesdt[0]->QUES_DIFFICULTY_LEVEL==1) {
    echo 'selected';
}?>>Low</option>
                                            <option  value="2" <?php if ($quesdt[0]->QUES_DIFFICULTY_LEVEL==2) {
    echo 'selected';
}?>>Medium</option>
                                            <option  value="3" <?php if ($quesdt[0]->QUES_DIFFICULTY_LEVEL==3) {
    echo 'selected';
}?>>Hard</option>
            							</select>
                					</div>
                					<div class="col-md-3">
            							<label class="col-form-label">Subject <span class="star">*</span></label>
            							<select class="form-control subjects" name="subject" data-style="btn-white" required>
            							    <?php if (!empty($all_sub['data'])) {
    foreach ($all_sub['data'] as $all_subs) { ?>
                                                <option value="<?php echo $all_subs->SUB_ID; ?>" <?php if ($quesdt[0]->SUB_ID==$all_subs->SUB_ID) {
        echo 'selected';
    }?> ><?php echo $all_subs->SUB_NAME; ?></option>
            							    <?php }
} ?>
            							</select>
                					</div>
                					<div class="col-md-3">
            							<label class="col-form-label">Book <span class="star">*</span></label>
            							<select class="form-control books" name="book" data-style="btn-white" required>
                                            <option  value="<?php echo $quesdt[0]->BOOK_ID; ?>"><?php echo $quesdt[0]->BOOK_NAME; ?></option>
            							</select>
                					</div>
                					<div class="col-md-12">
            							<label class="col-form-label">Question <span class="star">*</span></label>
                                        <textarea class="summernote question" name="question" id="mysummernote"><?php if ($quesdt[0]->QUES_TYPE==2) {
    echo json_decode($quesdt[0]->QUES)->QUESTION;
} else {
    echo $quesdt[0]->QUES;
}?></textarea>
                					</div>
                					<div class="col-md-12 for_subjective">
                					    <div class="row">
                					        <div class="col-md-12">
                    							<label class="col-form-label">Answer <span class="star">*</span></label>
                                                <textarea rows="3" class="form-control subj_answer" name="subj_answer"><?php echo $quesdt[0]->QUES_ANSWERS; ?></textarea>
                        					</div>
                    					    
                						    <div class="col-md-6">
                    							<label class="col-form-label">Question Hint</label>
                                                <textarea rows="2" class="form-control subj_ques_hint"  name="subj_ques_hint"><?php echo $quesdt[0]->QUES_HINT; ?></textarea>
                        					</div>
                        					<div class="col-md-6">
                    							<label class="col-form-label">Question Explanation</label>
                                                <textarea rows="2" class="form-control subj_ques_explain" name="subj_ques_explain"><?php echo $quesdt[0]->QUES_EXPLANATION; ?></textarea>
                        					</div>
                        					<div class="col-md-3">
                    							<label class="col-form-label">Positive Mark <span class="star">*</span></label>
                    							<input type="text" class="form-control subj_positive_mark input_num" name="subj_positive_mark" value="<?php echo $quesdt[0]->QUES_P_MARKING; ?>" placeholder="Enter Positive mark">
                						    </div>
                						    <div class="col-md-3">
                    							<label class="col-form-label">Negative Mark <span class="star">*</span></label>
                    							<input type="text" class="form-control subj_negative_mark input_num" name="subj_negative_mark" value="<?php echo $quesdt[0]->QUES_N_MARKING; ?>" placeholder="Enter Negative mark">
                						    </div>
                        					<div class='col-sm-3'>
                                                <label class="col-form-label">Duration </label>
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' class="form-control subj_duration" value="<?php echo $quesdt[0]->QUES_DURATION; ?>" name="subj_duration" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-time"></i>
                                                    </span>
                                                </div>
                                            </div>
                    					</div>
                					</div>
                					<div class="col-md-12 for_objective hidez">
                					    <div class="row">
                    					    <div class="col-md-6">
                    					        <div class="row">
                    					            <div class="col-md-12">
                            							<label class="col-form-label">No of options <span class="star">*</span></label>
                            							<input type="text" class="form-control edit_obj_options input_num" name="obj_options" placeholder="Enter Number of Options" value="4">
                        						    </div>
                                                    <div class="col-md-12">
                                                        <div class="row edit_options_div">
                                                            
                                                        </div>
                                				    </div>
                    					        </div>
                    					    </div>
                    					    <div class="col-md-6">
                    					        <div class="row">
                    					            <div class="col-md-6">
                            							<label class="col-form-label">Positive Mark </label>
                            							<input type="text" class="form-control obj_positive_mark input_num" value="<?php echo $quesdt[0]->QUES_P_MARKING; ?>" name="obj_positive_mark" placeholder="Enter Positive mark">
                        						    </div>
                        						    <div class="col-md-6">
                            							<label class="col-form-label">Negative Mark </label>
                            							<input type="text" class="form-control obj_negative_mark input_num" value="<?php echo $quesdt[0]->QUES_N_MARKING; ?>" name="obj_negative_mark" placeholder="Enter Negative mark">
                        						    </div>
                        						    <div class="col-md-6">
                            							<label class="col-form-label">Question Hint</label>
                                                        <textarea rows="2" class="form-control obj_ques_hint" name="obj_ques_hint"><?php echo $quesdt[0]->QUES_HINT; ?></textarea>
                                					</div>
                                					<div class="col-md-6">
                            							<label class="col-form-label">Question Explanation <span class="star">*</span></label>
                                                        <textarea rows="2" class="form-control obj_ques_explain" name="obj_ques_explain"><?php echo $quesdt[0]->QUES_EXPLANATION; ?></textarea>
                                					</div>
                                					
                                                    <div class='col-sm-6'>
                                                        <label class="col-form-label">Duration <?php echo $quesdt[0]->QUES_ANSWERS; ?></label>
                                                        <div class='input-group date' id='datetimepicker4'>
                                                            <input type='text' class="form-control obj_duration" value="<?php echo $quesdt[0]->QUES_DURATION; ?>" name="obj_duration" />
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-time"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                    					        </div>
                    					    </div>
                					    </div>
                					</div>
                					<div class="col-md-12" style="margin-top:10px">
            							<div class="col-md-12 text-center">
            								<button type="submit" class="btn btn-primary questionedit">SAVE</button>
            							</div>
            						</div>
            					</div>
        					 </form>
			            </div>
				    </div>
			    </div>
		    </div>
	    </div>
    </div>
</div>
<script>

$('document').ready(function(){
	FormSummernote.init();
    if($('.ques_type').length>0){
	    $ques_type = $('.ques_type').val();
		if($ques_type==1){
		    $('.for_subjective').show();
		    $('.for_objective').hide();
		}else{
		    $('.for_subjective').hide();
		    optionss = '';
		    add_obj_option_div(optionss);
		    $('.for_objective').show();
		}
	}
	if($('.options_divs').length>0){
	    if(<?php echo $quesdt[0]->QUES_TYPE ?>==2){
	        <?php $option = json_decode($quesdt[0]->QUES)->OPTION;  ?>
	        optionss=<?php echo json_encode($option); ?>;
	        answer='<?php echo $quesdt[0]->QUES_ANSWERS; ?>';
	        var i=0;
	        $(".options_divs").each(function() {
                $(this).find('.ques_option').html(optionss[i])
                i++;
            });
            
            var k = 1;
    	    $(".options_divs").each(function() {
    	        var answers = answer.toString().split(',');
    	        var l = k.toString();
                if(answers.includes(l)){
                    $(this).find('.obj_answer').prop('checked', true);
                }
                 k++;
            });
	        
	    }
	}
    
});
</script>