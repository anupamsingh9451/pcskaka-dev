<link href="<?php echo base_url(); ?>assets/adminassets/css/question/view_ques_modal.css" rel="stylesheet" />
<div class="row">
    <div class="col-md-12">
        <div class="card" style="border: 1px solid rgba(0,0,0,.2);">
			<div class="card-block"> 
			    <div class="row no_mrgn">
            		<div class="col-md-8">
            			<div class="row">
            			    <h5 class="">Subject Name &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php echo $quesdt[0]->SUB_NAME; ?></span>
            			</div>
            			<div class="row">
            			    <h5 class="">Book Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </h5><span class=""><?php echo $quesdt[0]->BOOK_NAME; ?></span>
            	        </div>
            	        <div class="row">
            			    <h5 class="">Chapter Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </h5><span class=""><?php echo $quesdt[0]->CHAPTER_NAME; ?></span>
            	        </div>
            		</div>
            		<div class="col-md-4 text-right">
            		    <div class="row">
            		        <div class="col-md-12">
            		            <a href="javascript:void(0);">
                    			    <button type="button" class="btn btn-danger edit_ques_on_modal" attr-quesid="<?php echo $quesdt[0]->QUES_ID; ?>"><i class="fa fa-fw fa-hdd"></i> Edit</button>
                    			</a>
            		        </div>
                		    <?php if ($quesdt[0]->QUES_APPROVED_BY==0) { ?>
                		        <div class="col-md-12 m-t-5">
                		            <a href="javascript:void(0);">
                        			    <button type="button" class="btn btn-success approve_ques" attr-quesid="<?php echo $quesdt[0]->QUES_ID; ?>"><i class="fa fa-fw fa-hdd"></i> Approve</button>
                        			</a>
                		        </div>
                		        <?php if ($quesdt[0]->QUES_STATUS!=3) { ?>
                		        <div class="col-md-12 m-t-5">
                		            <a href="javascript:void(0);">
                        			    <button type="button" class="btn btn-warning hold_ques" attr-quesid="<?php echo $quesdt[0]->QUES_ID; ?>"><i class="fa fa-fw fa-hdd"></i> Hold</button>
                        			</a>
                		        </div>
                		        <?php } ?>
                		    <?php } else { ?>
                    		    <div class="col-md-12 m-t-5">
                		            <a href="javascript:void(0);">
                		                <?php $check = check_ques_exist_in_all_test_series($quesdt[0]->QUES_ID); if ($check) { ?>
                        			        <button type="button" class="btn btn-success nfs_ques" attr-quesid="<?php echo $quesdt[0]->QUES_ID; ?>" disabled='disabled' style='cursor: not-allowed' ><i class="fa fa-fw fa-hdd"></i> NFS</button>
                        		        <?php } else { ?>
                        		            <?php if ($quesdt[0]->QUES_STATUS==4) { ?>
                        			            <button type="button" class="btn btn-danger nfs_ques" attr-quesid="<?php echo $quesdt[0]->QUES_ID; ?>" attr-nfsact="1" ><i class="fa fa-fw fa-hdd"></i> NFS</button>
                        		            <?php } else { ?>
                        			            <button type="button" class="btn btn-success nfs_ques" attr-quesid="<?php echo $quesdt[0]->QUES_ID; ?>" attr-nfsact="4" ><i class="fa fa-fw fa-hdd"></i> NFS</button>
                        		        <?php } } ?>
                        			</a>
                		        </div>
                		       <?php } ?>
            		   </div>
            		</div>
            	</div>
			</div>
		</div>
    </div>
    <div class="col-md-12">
        <div class="card" style="border: 1px solid rgba(0,0,0,.2);">
			<div class="card-block"> 
			    <?php if ($quesdt[0]->QUES_TYPE==2) { ?>
				    <p class="card-text ques_font">Q. &nbsp;<?php echo json_decode($quesdt[0]->QUES)->QUESTION; ?> </p>
				  <?php } else {?>
				       <p class="card-text ques_font">Q. &nbsp;<?php echo $quesdt[0]->QUES; ?> </p>
				  <?php } ?>
			    <div class="row">
			        <?php if ($quesdt[0]->QUES_TYPE==2) { ?>
				        <div class="col-md-12">
				            <?php $azRange = range('A', 'Z'); $i=0; $option = json_decode($quesdt[0]->QUES)->OPTION; foreach ($option as $options) { ?>
				            <div class="qa_progress" <?php if (in_array($i+1, explode(',', $quesdt[0]->QUES_ANSWERS))) {
    echo 'style="width:100%;border: #4caace solid 1px;" ';
} else {
    echo 'style="width:100%;" ';
} ?> style="width:100%">
                                <div class="qa_progress_text">
                                    <ul>
                                        <li class="qa_number" <?php if (in_array($i+1, explode(',', $quesdt[0]->QUES_ANSWERS))) {
    echo 'style="background: #4caace;color: #fff;" ';
} ?>><?php echo $azRange[$i]; ?></li>
                                        <li class="qa_text" style="display: inline-block">
                                            <p><?php echo $options; ?></p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php $i++; } ?>
				        </div>
				    <?php } else {?>
				        <div class="col-md-6">
				            <div class="qa_progress" style="width:100%;">
                                <div class="qa_progress_text">
                                    <ul>
                                        <li class="qa_text" style="display: inline-block">
                                            <p><?php echo $quesdt[0]->QUES_ANSWERS; ?></p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
				        </div>
				    <?php } ?>
				    <div class="col-md-12">
                		<?php if ($quesdt[0]->QUES_EXPLANATION!='') { ?>
                			<div class="row m-t-10">
                			    <div class="col-md-3">
                			        <h5 class="">Question Explanation : 
                			    </div>
                			    <div class="col-md-8">
                			        </span></h5><span class=""><?php echo $quesdt[0]->QUES_EXPLANATION; ?></span>
                	            </div>
                	        </div>
            	        <?php } ?>
            		</div>
			    </div>
			    
			</div>
		</div>
    </div>
    <div class="col-md-12">
        <div class="card" style="border: 1px solid rgba(0,0,0,.2);">
			<div class="card-block"> 
			    <div class="row no_mrgn">
            		<div class="col-md-6">
            			<div class="row">
            			    <h5 class="">Created By &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php $userrole = $this->Admingetmodel->get_user_role_by_id($quesdt[0]->USER_ROLES); echo $quesdt[0]->USER_FNAME.' '.$quesdt[0]->USER_LNAME.' ('.$userrole[0]->USER_ROLE_NAME.')'; ?></span>
            			</div>
            			<div class="row">
            			    <h5 class="">Positive Mark &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $quesdt[0]->QUES_P_MARKING; ?></span>
            	        </div>
            	        <div class="row">
            			    <h5 class="">Question Duration &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo date('H:i', strtotime($quesdt[0]->QUES_DURATION)); ?></span>
            	        </div>
            	        <div class="row">
            			    <h5 class="">Created Date &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo date('d-m-Y', strtotime($quesdt[0]->QUES_CRATED_AT)); ?></span>
            	        </div>
            		</div>
            		<div class="col-md-6">
            			<div class="row">
            			    <h5 class="">Approved By &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php if ($quesdt[0]->QUES_APPROVED_BY!=0) {
    $response = $this->Admingetmodel->get_user_role_by_id($quesdt[0]->QUES_APPROVED_BY);
    echo $response[0]->USER_FNAME.' '.$response[0]->USER_LNAME.' ('.$response[0]->USER_ROLE_NAME.')';
} else {
    echo "Not Approved";
} ?></span>
            			</div>
            			<div class="row">
            			    <h5 class="">Negative Mark &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $quesdt[0]->QUES_N_MARKING; ?></span>
            	        </div>
            	        <div class="row">
            			    <h5 class="">Difficulty Level &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php $response = $this->Admingetmodel->get_difficulty_level($quesdt[0]->QUES_DIFFICULTY_LEVEL); echo $response; ?></span>
            	        </div>
            	        <div class="row">
            			    <h5 class="">Status &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class="" ><?php if ($quesdt[0]->QUES_STATUS==1) {
    echo '<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
} elseif ($quesdt[0]->QUES_STATUS==3) {
    echo '<button type="button" class="btn btn-warning pad_4">HOLD</button>';
} elseif ($quesdt[0]->QUES_STATUS==2) {
    echo '<button type="button" class="btn btn-danger pad_4">INACTIVE</button>';
} else {
    echo '<button type="button" class="btn btn-danger pad_4">NFS</button>';
} ?></span>
            	        </div>
            		</div>
            		<div class="col-md-12">
            		    <?php if ($quesdt[0]->QUES_HINT!='') { ?>
                			<div class="row">
                			    <h5 class="">Question Hint &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php echo $quesdt[0]->QUES_HINT; ?></span>
                			</div>
                		<?php } ?>
                		<?php if ($quesdt[0]->QUES_EXPLANATION!='') { ?>
                			<div class="row">
                			    <h5 class="">Question Explanation &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $quesdt[0]->QUES_EXPLANATION; ?></span>
                	        </div>
            	        <?php } ?>
            		</div>
            	</div>
			</div>
		</div>
    </div>
</div>