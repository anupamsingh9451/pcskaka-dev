<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Question</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Question</h1>
	<!-- end page-header -->
	<div class="row no_mrgn addbx_top_rowhead pd_15">
		<div class="col-md-8">
		    <div class="row">
		        
			    <h5 class="">Library Name &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; </h5><span class=""><?php echo $chapterdt[0]->LIB_NAME; ?></span>
			</div>
			<div class="row">
			    <h5 class="">Subject Name &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php echo $chapterdt[0]->SUB_NAME; ?></span>
			</div>
			<div class="row">
			    <h5 class="">Book Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $chapterdt[0]->BOOK_NAME; ?></span>
	        </div>
	        <div class="row">
			    <h5 class="">Chapter Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $chapterdt[0]->CHAPTER_NAME; ?></span>
	        </div>
		</div>
		<div class="col-md-4 text-right">
    		<a class="text-right" href="<?php echo base_url().'ratan/question/create/'.$this->uri->segment(3); ?>">
    			<button type="button" class="btn btn-warning"><i class="fa fa-fw fa-hdd"></i> Add Question</button>
    		</a>
	    </div>
	</div>
	<!-- begin row -->
	<div class="row">
        <div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Question List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="question_table" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th width="1%">Sr No.</th>
								<th>Question</th>
								<th>Created By</th>
								<th>Created Date</th>
								<th width="9%">Edited By</th>
								<th width="9%">Approved By</th>
								<th>Level</th>
								<th>Status</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
	</div>
</div>
<!-- View Question modal start -->
<div class="modal fade" id="viewquestionmodal" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="background-color:unset">
			<div class="modal-header" style="background-color: #348fe2 ;">
				<h4 class="modal-title" style="color:white">View Question</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body bg_grey">
				<div class='col-md-12'>
        			<!-- begin panel -->
        			<div class="panel bg_grey">
        				<!-- begin panel-body -->
        				<div class="panel-body view_ques_modal_div">
        				
        					
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
</div>
<!-- View Question modal end -->

<!-- Edit Question modal start -->
<div class="modal fade" id="editquestionmodal" role="dialog">
	<div class="modal-dialog modal-lg" style="max-width:1200px;">
		<div class="modal-content" style="background-color:unset">
			<div class="modal-header" style="background-color: #348fe2 ;">
				<h4 class="modal-title" style="color:white">Edit Question</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body bg_grey">
				<div class='col-md-12'>
        			<!-- begin panel -->
        			<div class="panel bg_grey">
        				<!-- begin panel-body -->
        				<div class="panel-body edit_ques_modal_div">
        				
        					
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
</div>
<!-- Edit Question modal end -->

<!-- Add Chapter Modal -->

<div class="modal fade" id="addquestion" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="background-color:unset">
			<div class="modal-header" style="background-color: #348fe2 ;">
				<h4 class="modal-title" style="color:white">Add <span class='librarynm'></span> Question</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body bg_grey">
				<div class='col-md-12'>
        			<!-- begin panel -->
        			<div class="panel" style="border-radius: 10px;border: 1px solid #ccc6c6;">
        				<!-- begin panel-body -->
        				<div class="panel-body">
        				
        					<form id="questioncreate">
        					    <input type="hidden" name="subject" value="<?php echo $chapterdt[0]->SUB_ID; ?>">
        					    <input type="hidden" name="book" value="<?php echo $chapterdt[0]->BOOK_ID; ?>">
        					    <input type="hidden" name="chapter" value="<?php echo $chapterdt[0]->CHAPTER_ID; ?>">
        					    <div class="row">
        					        <div class="col-md-6">
            							<label class="col-form-label">Question Type <span class="star">*</span></label>
            							<select class="form-control ques_type" name="ques_type" data-style="btn-white" required>
                                            <option  value="">--Select Question Type--</option>
                                            <option  value="1">Subjective</option>
                                            <option  value="2">Objective</option>
            							</select>
                					</div>
                					<div class="col-md-6">
            							<label class="col-form-label">Level of Question <span class="star">*</span></label>
            							<select class="form-control ques_levele" name="ques_levele" data-style="btn-white" required>
                                            <option  value="">--Select Question level--</option>
                                            <option  value="1">Low</option>
                                            <option  value="2">Medium</option>
                                            <option  value="3">Hard</option>
            							</select>
                					</div>
        					        <div class="col-md-12">
            							<label class="col-form-label">Question <span class="star">*</span></label>
                                        <textarea rows="4" class="form-control question" name="question" required></textarea>
                					</div>
                					<div class="col-md-12">
            							<textarea class="summernote" name="content"></textarea>
                					</div>
                					
                					<div class="col-md-12 for_subjective">
                					    <div class="row">
                					        <div class="col-md-12">
                    							<label class="col-form-label">Answer <span class="star">*</span></label>
                                                <textarea rows="3" class="form-control subj_answer" name="subj_answer"></textarea>
                        					</div>
                    					    <div class="col-md-6">
                    							<label class="col-form-label">Positive Mark </label>
                    							<input type="text" class="form-control subj_positive_mark input_num" name="subj_positive_mark" placeholder="Enter Positive mark">
                						    </div>
                						    <div class="col-md-6">
                    							<label class="col-form-label">Negative Mark </label>
                    							<input type="text" class="form-control subj_negative_mark input_num" name="subj_negative_mark" placeholder="Enter Negative mark">
                						    </div>
                						    <div class="col-md-6">
                    							<label class="col-form-label">Question Hint</label>
                                                <textarea rows="2" class="form-control subj_ques_hint" name="subj_ques_hint"></textarea>
                        					</div>
                        					<div class="col-md-6">
                    							<label class="col-form-label">Question Explanation <span class="star">*</span></label>
                                                <textarea rows="2" class="form-control subj_ques_explain" name="subj_ques_explain"></textarea>
                        					</div>
                        					<div class='col-sm-6'>
                                                <label class="col-form-label">Duration </label>
                                                <div class='input-group date' id='datetimepicker3'>
                                                    <input type='text' class="form-control subj_duration" name="subj_duration" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-time"></i>
                                                    </span>
                                                </div>
                                            </div>
                        					<div class="col-md-6 hide">
                        						<label class="col-form-label"> Created Date <span class="star">*</span></label>
                        						<input type="text" class="form-control dtpicker subj_created_date" name="subj_created_date" value="<?php echo date('Y-m-d')?>">
                        					</div>
                    					</div>
                					</div>
                					<div class="col-md-12 for_objective hidez">
                					    <div class="row">
                    					    <div class="col-md-6">
                    					        <div class="row">
                    					            <div class="col-md-12">
                            							<label class="col-form-label">No of options <span class="star">*</span></label>
                            							<input type="text" class="form-control obj_options input_num" name="obj_options" placeholder="Enter Number of Options" value="4">
                        						    </div>
                                                    <div class="col-md-12">
                                                        <div class="row options_div">
                                                            
                                                        </div>
                                				    </div>
                    					        </div>
                    					    </div>
                    					    <div class="col-md-6">
                    					        <div class="row">
                    					            <div class="col-md-6">
                            							<label class="col-form-label">Positive Mark </label>
                            							<input type="text" class="form-control obj_positive_mark input_num" name="obj_positive_mark" placeholder="Enter Positive mark">
                        						    </div>
                        						    <div class="col-md-6">
                            							<label class="col-form-label">Negative Mark </label>
                            							<input type="text" class="form-control obj_negative_mark input_num" name="obj_negative_mark" placeholder="Enter Negative mark">
                        						    </div>
                        						    <div class="col-md-12">
                            							<label class="col-form-label">Question Hint </label>
                                                        <textarea rows="2" class="form-control obj_ques_hint" name="obj_ques_hint"></textarea>
                                					</div>
                                					<div class="col-md-12">
                            							<label class="col-form-label">Question Explanation<span class="star">*</span></label>
                                                        <textarea rows="2" class="form-control obj_ques_explain" name="obj_ques_explain"></textarea>
                                					</div>
                                					
                                                    <div class='col-sm-12'>
                                                        <label class="col-form-label">Duration </label>
                                                        <div class='input-group date' id='datetimepicker2'>
                                                            <input type='text' class="form-control obj_duration" name="obj_duration" />
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-time"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                					<div class="col-md-6 hide">
                                						<label class="col-form-label"> Created Date <span class="star">*</span></label>
                                						<input type="text" class="form-control dtpicker obj_created_date" name="obj_created_date" value="<?php echo date('Y-m-d')?>">
                                					</div>
                    					        </div>
                    					    </div>
                					    </div>
                					</div>
                					<div class="col-md-12" style="margin-top:10px">
            							<div class="col-md-12 text-center">
            								<button type="submit" class="btn btn-primary questioncreate">SAVE</button>
            							</div>
            						</div>
        					 </form>
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
</div>

<!-- Edit Chapter Modal -->
<div class="modal fade" id="editchapter">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">EDIT <span class='booknm'></span> Chapter</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="editchapterform">
				<div class="modal-body">
					<input type='hidden' class='chapterid' name="chapterid"  />
					<input type='hidden' class='bookid' name="bookid"  />
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Chapter Name<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 chapternm" name="chapternm" placeholder="Enter Chapter name here" required>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">No. of Question<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 quesno" name="quesno" placeholder="Enter Number of Question" required>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Status<span class="star">*</span></label>
						<div class="col-md-9">
							<select name="chapterstatus" class='form-control chapterstatus'>
								<option value='1'>Active</option>
								<option value='0'>Inactive</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success editchapterform">Update</a>
				</div>
			</form>
		</div>
	</div>
</div>
