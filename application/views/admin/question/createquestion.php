<!-- begin #content -->
<link href="<?php echo base_url(); ?>assets/adminassets/css/question/createquestion.css" rel="stylesheet" />
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Question</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Create</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Question</h1>
	<!-- end page-header -->
	<div class="row no_mrgn addbx_top_rowhead pd_15">
		<div class="col-md-8"></div>
		<div class="col-md-4 text-right">
			<a class="btn btn-primary" href="<?php echo base_url().'ratan/question/'.$chapterdt[0]->CHAPTER_ID; ?>"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
		</div>
	</div>
	<!-- begin row -->
	<div class="row">
        <div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Create Question</h4>
				</div>
			    <div class="panel-body p-20" >
			        <div class="row">
			            <div class="col-md-12">
			                <form id="questioncreate">
        					    <input type="hidden" name="subject" value="<?php echo $chapterdt[0]->SUB_ID; ?>">
        					    <input type="hidden" name="book" class="bookid" value="<?php echo $chapterdt[0]->BOOK_ID; ?>">
        					    <input type="hidden" name="chapter" class="chapterid" value="<?php echo $chapterdt[0]->CHAPTER_ID; ?>">
        					    <div class="row">
        					        <div class="col-md-6">
            							<label class="col-form-label">Question Type <span class="star">*</span></label>
            							<select class="form-control ques_type" name="ques_type" data-style="btn-white" required>
                                            <option  value="">--Select Question Type--</option>
                                            <option  value="1">Subjective</option>
                                            <option  value="2">Objective</option>
            							</select>
                					</div>
                					<div class="col-md-6">
            							<label class="col-form-label">Level of Question <span class="star">*</span></label>
            							<select class="form-control ques_levele" name="ques_levele" data-style="btn-white" required>
                                            <option  value="">--Select Question level--</option>
                                            <option  value="1">Low</option>
                                            <option  value="2">Medium</option>
                                            <option  value="3">Hard</option>
            							</select>
                					</div>
        					        <div class="col-md-12">
            							<label class="col-form-label">Question <span class="star">*</span></label>
                                        <textarea class="summernote question" name="question" id="mysummernote"></textarea>
                					</div>
                				
                					<div class="col-md-12 for_subjective">
                					    <div class="row">
                					        <div class="col-md-12">
                    							<label class="col-form-label">Answer <span class="star">*</span></label>
                                                <textarea rows="3" class="form-control subj_answer" name="subj_answer"></textarea>
                        					</div>
                    					    <div class="col-md-6">
                    							<label class="col-form-label">Positive Mark </label>
                    							<input type="text" class="form-control subj_positive_mark input_num" name="subj_positive_mark" placeholder="Enter Positive mark">
                						    </div>
                						    <div class="col-md-6">
                    							<label class="col-form-label">Negative Mark </label>
                    							<input type="text" class="form-control subj_negative_mark input_num" name="subj_negative_mark" placeholder="Enter Negative mark">
                						    </div>
                						    <div class="col-md-6">
                    							<label class="col-form-label">Question Hint</label>
                                                <textarea rows="2" class="form-control subj_ques_hint" name="subj_ques_hint"></textarea>
                        					</div>
                        					<div class="col-md-6">
                    							<label class="col-form-label">Question Explanation <span class="star">*</span></label>
                                                <textarea rows="2" class="form-control subj_ques_explain" name="subj_ques_explain"></textarea>
                        					</div>
                        					<div class='col-sm-6'>
                                                <label class="col-form-label">Duration </label>
                                                <div class='input-group date' id='datetimepicker3'>
                                                    <input type='text' class="form-control subj_duration" name="subj_duration" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-time"></i>
                                                    </span>
                                                </div>
                                            </div>
                        					<div class="col-md-6 hide">
                        						<label class="col-form-label"> Created Date <span class="star">*</span></label>
                        						<input type="text" class="form-control dtpicker subj_created_date" name="subj_created_date" value="<?php echo date('Y-m-d')?>">
                        					</div>
                    					</div>
                					</div>
                					<div class="col-md-12 for_objective hidez">
                					    <div class="row">
                    					    <div class="col-md-6">
                    					        <div class="row">
                    					            <div class="col-md-12">
                            							<label class="col-form-label">No of options <span class="star">*</span></label>
                            							<input type="text" class="form-control obj_options input_num" name="obj_options" placeholder="Enter Number of Options" value="4">
                        						    </div>
                                                    <div class="col-md-12">
                                                        <div class="row options_div">
                                                            
                                                        </div>
                                				    </div>
                    					        </div>
                    					    </div>
                    					    <div class="col-md-6">
                    					        <div class="row">
                    					            <div class="col-md-6">
                            							<label class="col-form-label">Positive Mark </label>
                            							<input type="text" class="form-control obj_positive_mark input_num" name="obj_positive_mark" placeholder="Enter Positive mark">
                        						    </div>
                        						    <div class="col-md-6">
                            							<label class="col-form-label">Negative Mark </label>
                            							<input type="text" class="form-control obj_negative_mark input_num" name="obj_negative_mark" placeholder="Enter Negative mark">
                        						    </div>
                        						    <div class="col-md-12">
                            							<label class="col-form-label">Question Hint </label>
                                                        <textarea rows="2" class="form-control obj_ques_hint" name="obj_ques_hint"></textarea>
                                					</div>
                                					<div class="col-md-12">
                            							<label class="col-form-label">Question Explanation<span class="star">*</span></label>
                                                        <textarea rows="2" class="form-control obj_ques_explain" name="obj_ques_explain"></textarea>
                                					</div>
                                					
                                                    <div class='col-sm-12'>
                                                        <label class="col-form-label">Duration </label>
                                                        <div class='input-group date' id='datetimepicker2'>
                                                            <input type='text' class="form-control obj_duration" name="obj_duration" />
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-time"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                					<div class="col-md-6 hide">
                                						<label class="col-form-label"> Created Date <span class="star">*</span></label>
                                						<input type="text" class="form-control dtpicker obj_created_date" name="obj_created_date" value="<?php echo date('Y-m-d')?>">
                                					</div>
                    					        </div>
                    					    </div>
                					    </div>
                					</div>
                					<div class="col-md-12" style="margin-top:10px">
            							<div class="col-md-12 text-center">
            								<button type="submit" class="btn btn-primary questioncreate">SAVE</button>
            							</div>
            						</div>
        					 </form>
			            </div>
				    </div>
			    </div>
		    </div>
	    </div>
    </div>
</div>
<script>
	$(document).ready(function() {
		App.init();
		FormSummernote.init();
	});
</script>