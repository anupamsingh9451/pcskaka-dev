<link href="<?php echo base_url(); ?>assets/adminassets/css/question/edit_ques_modal.css" rel="stylesheet" />
<div class="row">
    <div class="col-md-12">
        <div class="card" style="border: 1px solid rgba(0,0,0,.2);">
			<div class="card-block"> 
			    <div class="row no_mrgn">
            		<div class="col-md-8">
            			<div class="row">
            			    <h5 class="">Subject Name &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php echo $quesdt[0]->SUB_NAME; ?></span>
            			</div>
            			<div class="row">
            			    <h5 class="">Book Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </h5><span class=""><?php echo $quesdt[0]->BOOK_NAME; ?></span>
            	        </div>
            	        <div class="row">
            			    <h5 class="">Chapter Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </h5><span class=""><?php echo $quesdt[0]->CHAPTER_NAME; ?></span>
            	        </div>
            		</div>
            		
            		<div class="col-md-4 text-right">
            		    <div class="row">
            		        <?php if ($quesdt[0]->QUES_APPROVED_BY==0) { ?>
                		        <div class="col-md-12">
                		            <a href="javascript:void(0);">
                        			    <button type="button" class="btn btn-success approve_ques" attr-quesid="<?php echo $quesdt[0]->QUES_ID; ?>"><i class="fa fa-fw fa-hdd"></i> Approve</button>
                        			</a>
                		        </div>
            		            <?php if ($quesdt[0]->QUES_STATUS!=3) { ?>
                    		        <div class="col-md-12 m-t-5">
                    		            <a href="javascript:void(0);">
                            			    <button type="button" class="btn btn-warning hold_ques" attr-quesid="<?php echo $quesdt[0]->QUES_ID; ?>"><i class="fa fa-fw fa-hdd"></i> Hold</button>
                            			</a>
                    		        </div>
            		            <?php } ?>
            		        <?php } else { ?>
                    		    <div class="col-md-12 m-t-5">
                		            <a href="javascript:void(0);">
                		                <?php $check = check_ques_exist_in_all_test_series($quesdt[0]->QUES_ID); if ($check) { ?>
                        			        <button type="button" class="btn btn-success nfs_ques" attr-quesid="<?php echo $quesdt[0]->QUES_ID; ?>" disabled='disabled' style='cursor: not-allowed' ><i class="fa fa-fw fa-hdd"></i> NFS</button>
                        		        <?php } else { ?>
                        		            <?php if ($quesdt[0]->QUES_STATUS==4) { ?>
                        			            <button type="button" class="btn btn-danger nfs_ques" attr-quesid="<?php echo $quesdt[0]->QUES_ID; ?>" attr-nfsact="1" ><i class="fa fa-fw fa-hdd"></i> NFS</button>
                        		            <?php } else { ?>
                        			            <button type="button" class="btn btn-success nfs_ques" attr-quesid="<?php echo $quesdt[0]->QUES_ID; ?>" attr-nfsact="4" ><i class="fa fa-fw fa-hdd"></i> NFS</button>
                        		        <?php } } ?>
                        			</a>
                		        </div>
                		    <?php } ?>
            		    </div>
            		 </div>
            	</div>
			</div>
		</div>
    </div>
    <div class="col-md-12">
        <div class="card" style="border: 1px solid rgba(0,0,0,.2);">
			<div class="card-block"> 
			    <div class="row">
		            <div class="col-md-12">
		                <form id="questionedit">
		                    <input type="hidden" name="quesid" value="<?php echo $quesdt[0]->QUES_ID; ?>">
    					    <div class="row">
    					        <div class="col-md-3">
        							<label class="col-form-label">Question Type <span class="star">*</span></label>
        							<select class="form-control ques_type" name="ques_type" data-style="btn-white" required>
                                        <option  value="">--Select Question Type--</option>
                                        <option value="1" <?php if ($quesdt[0]->QUES_TYPE==1) {
    echo 'selected';
}?> >Subjective</option>
                                        <option  value="2" <?php if ($quesdt[0]->QUES_TYPE==2) {
    echo 'selected';
}?>>Objective</option>
        							</select>
            					</div>
            					<div class="col-md-3">
        							<label class="col-form-label">Level of Question <span class="star">*</span></label>
        							<select class="form-control ques_levele" name="ques_levele" data-style="btn-white" required>
                                        <option  value="">--Select Question level--</option>
                                        <option  value="1" <?php if ($quesdt[0]->QUES_DIFFICULTY_LEVEL==1) {
    echo 'selected';
}?>>Low</option>
                                        <option  value="2" <?php if ($quesdt[0]->QUES_DIFFICULTY_LEVEL==2) {
    echo 'selected';
}?>>Medium</option>
                                        <option  value="3" <?php if ($quesdt[0]->QUES_DIFFICULTY_LEVEL==3) {
    echo 'selected';
}?>>Hard</option>
        							</select>
            					</div>
            					<div class="col-md-3">
        							<label class="col-form-label">Subject <span class="star">*</span></label>
        							<select class="form-control subjects" name="subject" data-style="btn-white" required>
        							    <?php if (!empty($all_sub['data'])) {
    foreach ($all_sub['data'] as $all_subs) { ?>
                                            <option value="<?php echo $all_subs->SUB_ID; ?>" <?php if ($quesdt[0]->SUB_ID==$all_subs->SUB_ID) {
        echo 'selected';
    }?> ><?php echo $all_subs->SUB_NAME; ?></option>
        							    <?php }
} ?>
        							</select>
            					</div>
            					<div class="col-md-3">
        							<label class="col-form-label">Book <span class="star">*</span></label>
        							<select class="form-control books" name="book" data-style="btn-white" required>
                                        <option  value="<?php echo $quesdt[0]->BOOK_ID; ?>"><?php echo $quesdt[0]->BOOK_NAME; ?></option>
        							</select>
            					</div>
            					<div class="col-md-12">
        							<label class="col-form-label">Question <span class="star">*</span></label>
                                    <textarea class="summernote question" name="question" id="mysummernote"><?php if ($quesdt[0]->QUES_TYPE==2) {
    echo json_decode($quesdt[0]->QUES)->QUESTION;
} else {
    echo $quesdt[0]->QUES;
}?></textarea>
            					</div>
            					<div class="col-md-12 for_subjective">
            					    <div class="row">
            					        <div class="col-md-12">
                							<label class="col-form-label">Answer <span class="star">*</span></label>
                                            <textarea rows="3" class="form-control subj_answer" name="subj_answer"><?php echo $quesdt[0]->QUES_ANSWERS; ?></textarea>
                    					</div>
                					    
            						    <div class="col-md-6">
                							<label class="col-form-label">Question Hint</label>
                                            <textarea rows="2" class="form-control subj_ques_hint"  name="subj_ques_hint"><?php echo $quesdt[0]->QUES_HINT; ?></textarea>
                    					</div>
                    					<div class="col-md-6">
                							<label class="col-form-label">Question Explanation</label>
                                            <textarea rows="2" class="form-control subj_ques_explain" name="subj_ques_explain"><?php echo $quesdt[0]->QUES_EXPLANATION; ?></textarea>
                    					</div>
                    					<div class="col-md-3">
                							<label class="col-form-label">Positive Mark <span class="star">*</span></label>
                							<input type="text" class="form-control subj_positive_mark input_num" name="subj_positive_mark" value="<?php echo $quesdt[0]->QUES_P_MARKING; ?>" placeholder="Enter Positive mark">
            						    </div>
            						    <div class="col-md-3">
                							<label class="col-form-label">Negative Mark <span class="star">*</span></label>
                							<input type="text" class="form-control subj_negative_mark input_num" name="subj_negative_mark" value="<?php echo $quesdt[0]->QUES_N_MARKING; ?>" placeholder="Enter Negative mark">
            						    </div>
                    					<div class='col-sm-3'>
                                            <label class="col-form-label">Duration </label>
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' class="form-control subj_duration" value="<?php echo $quesdt[0]->QUES_DURATION; ?>" name="subj_duration" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-time"></i>
                                                </span>
                                            </div>
                                        </div>
                					</div>
            					</div>
            					<div class="col-md-12 for_objective hidez">
            					    <div class="row">
            					        <div class="col-md-3">
                							<label class="col-form-label">No of options <span class="star">*</span></label>
                							<input type="text" class="form-control edit_obj_options input_num" name="obj_options" placeholder="Enter Number of Options" value="4">
            						    </div>
            						    <div class="col-md-3">
                							<label class="col-form-label">Positive Mark </label>
                							<input type="text" class="form-control obj_positive_mark input_num" value="<?php echo $quesdt[0]->QUES_P_MARKING; ?>" name="obj_positive_mark" placeholder="Enter Positive mark">
            						    </div>
            						    <div class="col-md-3">
                							<label class="col-form-label">Negative Mark </label>
                							<input type="text" class="form-control obj_negative_mark input_num" value="<?php echo $quesdt[0]->QUES_N_MARKING; ?>" name="obj_negative_mark" placeholder="Enter Negative mark">
            						    </div>
            						    <div class='col-sm-3'>
                                            <label class="col-form-label">Duration <?php echo $quesdt[0]->QUES_ANSWERS; ?></label>
                                            <div class='input-group date' id='datetimepicker4'>
                                                <input type='text' class="form-control obj_duration" value="<?php echo $quesdt[0]->QUES_DURATION; ?>" name="obj_duration" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-time"></i>
                                                </span>
                                            </div>
                                        </div>
                					    <div class="col-md-6">
                					        <div class="row">
                					            
                                                <div class="col-md-12">
                                                    <div class="row edit_options_div">
                                                        
                                                    </div>
                            				    </div>
                					        </div>
                					    </div>
                					    <div class="col-md-6">
                					        <div class="row">
                					            <div class="col-md-12">
                        							<label class="col-form-label">Question Explanation <span class="star">*</span></label>
                                                    <textarea rows="3" class="form-control obj_ques_explain" name="obj_ques_explain"><?php echo $quesdt[0]->QUES_EXPLANATION; ?></textarea>
                            					</div>
                    						    <div class="col-md-12">
                        							<label class="col-form-label">Question Hint</label>
                                                    <textarea rows="3" class="form-control obj_ques_hint" name="obj_ques_hint"><?php echo $quesdt[0]->QUES_HINT; ?></textarea>
                            					</div>
                					        </div>
                					    </div>
            					    </div>
            					</div>
            					<div class="col-md-12" style="margin-top:10px">
        							<div class="col-md-12 text-center">
        								<button type="submit" class="btn btn-primary questionedit">SAVE</button>
        							</div>
        						</div>
        					</div>
    					 </form>
		            </div>
			    </div>
			</div>
		</div>
    </div>
    
</div>
<script>

$('document').ready(function(){
	FormSummernote.init();
    if($('.ques_type').length>0){
	    $ques_type = $('.ques_type').val();
		if($ques_type==1){
		    $('.for_subjective').show();
		    $('.for_objective').hide();
		}else{
		    $('.for_subjective').hide();
		    optionss = '';
		    add_obj_option_div(optionss);
		    $('.for_objective').show();
		}
	}
	if($('.options_divs').length>0){
	    if(<?php echo $quesdt[0]->QUES_TYPE ?>==2){
	        <?php $option = json_decode($quesdt[0]->QUES)->OPTION;  ?>
	        optionss=<?php echo json_encode($option); ?>;
	        answer='<?php echo $quesdt[0]->QUES_ANSWERS; ?>';
	        var i=0;
	        $(".options_divs").each(function() {
                $(this).find('.ques_option').html(optionss[i])
                i++;
            });
            
            var k = 1;
    	    $(".options_divs").each(function() {
    	        var answers = answer.toString().split(',');
    	        var l = k.toString();
                if(answers.includes(l)){
                    $(this).find('.obj_answer').prop('checked', true);
                }
                 k++;
            });
	        
	    }
	}
    
});
</script>