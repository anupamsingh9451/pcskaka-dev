<link href="<?php echo base_url(); ?>assets/adminassets/css/mail/mail.css" rel="stylesheet" />
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item active">Mail</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">Sned Mail</h1>
	<!-- begin page-header -->
	<!-- end page-header -->
	<div class="row">
    	<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Scheduling List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
				<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Sr no</th>
									<th>Sched Type</th>
									<th>Template</th>
									<th>Status</th>
									<th>Tools</th>
								</tr>
							</thead>
							<tbody class="schedulinglist">
								
							</tbody>
						</table>
					</div>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
	    <!-- begin col-6 -->
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Template List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
				<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Sr no</th>
									<th>Template Title</th>
									<th>Template details</th>
									<th width="1%">Tools</th>
								</tr>
							</thead>
							<tbody class="temptablebody">
								
							</tbody>
						</table>
					</div>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		
			<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Set Scheduling</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<form id="setscheduling">
					    <input type="hidden" class="sending_type" name="sending_type" value="2">
					    <legend class="m-b-15">Set Scheduling</legend>
					    <div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Student Type <span class="star">*</span></label>
							<div class="col-md-9">
								<select class="form-control stdtypesched" name="stdtype" data-style="btn-white" required>
								    <option value="2">All</option>
								    <option value="1">Paid</option>
								    <option value="0">Free</option>
								</select>
							</div>
						</div>
						<div class="form-group row m-b-15 testseriesscheddiv hide">
							<label class="col-form-label col-md-3">Test Series <span class="star">*</span></label>
							<div class="col-md-9">
								<select class="form-control testseriessched" name="testseries" data-style="btn-white" required>
								    <?php $tsdtarr = array();
                                     if (!empty($tsdt)) {
                                         foreach ($tsdt as $tsdts) {
                                             $tsdtarr[] = $tsdts->TS_ID;
                                         }
                                     }
                                    $tsids = implode(',', $tsdtarr);
                                    ?>
                                    <option value="<?php echo $tsids; ?>">All</option>
                                    <?php if (!empty($tsdt)) {
                                        foreach ($tsdt as $tsdts) { ?>
                                        <option value="<?php echo $tsdts->TS_ID; ?>" ><?php echo $tsdts->TS_NAME; ?></option>
                                    <?php }
                                    } ?>
								</select>
							</div>
						</div>
					    <div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">User <span class="star">*</span></label>
							<div class="col-md-9">
								<select class="usermultipleselectsched form-control" name="users[]" multiple="multiple" placeholder="select users" required>
								
								</select>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Template option <span class="star">*</span></label>
							<div class="col-md-9">
								<select class="form-control sendmsgtemplatesched" name="sendmsgtemplate" required>

								</select>
							</div>
						</div>
					    <div class="form-group row m-b-15 m-b-10">
							<label class="col-form-label col-md-2">Template</label>
							<div class="col-md-10">
								<textarea class="sendmessagetempsched" name="sendmessagetempsched" id="mysummernote"></textarea>
							</div>
						</div>
						
						<div class="form-group row m-b-15" style="font-size: 14px;">
    						<label class="col-md-3 col-form-label">Scheduling</label>
    						<div class="col-md-9">
    							<div class="radio radio-css radio-inline">
    								<input type="radio" class="scheduletype" id="inlineCssRadio1" name="scheduletype" value="1" />
    								<label for="inlineCssRadio1">Daily</label>
    							</div>
    							<div class="radio radio-css radio-inline">
    								<input type="radio" class="scheduletype" id="inlineCssRadio2" name="scheduletype" value="2" />
    								<label for="inlineCssRadio2">Weekly</label>
    							</div>
    							<div class="radio radio-css radio-inline">
    								<input type="radio" class="scheduletype" id="inlineCssRadio3" name="scheduletype" value="3" />
    								<label for="inlineCssRadio3">Monthly</label>
    							</div>
    							<div class="radio radio-css radio-inline">
    								<input type="radio" class="scheduletype" id="inlineCssRadio4" name="scheduletype" value="4" />
    								<label for="inlineCssRadio4">Yearly</label>
    							</div>
    						</div>
    					</div>
    					
				        <div class="form-group row m-b-15 m-b-10">
				            <label class="col-form-label col-md-3">Date <span class="star">*</span></label>
							<div class="col-md-9">
								<div class="input-group date" id="datetimepicker1">
									<input type="text" name="datetime" class="form-control"  value="<?php echo date('Y-m-d H:i:s'); ?>" required>
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
								</div>
							</div>
				        </div>
    					   
						<div class="form-group row m-b-10 m-t-10">
							<div class="col-md-12 text-right">
								<button type="submit" class="btn btn-primary setscheduling">Set Schedule</button>
							</div>
						</div>
					</form>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		
			<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Send Mail</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body ">
					<form id="sendmail">
					    <input type="hidden" class="sending_type" name="sending_type" value="2">
					    <legend class="m-b-15">Send Mail</legend>
					    <div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Student Type <span class="star">*</span></label>
							<div class="col-md-9">
								<select class="form-control stdtype" name="stdtype" data-style="btn-white" required>
								    <option value="2">All</option>
								    <option value="1">Paid</option>
								    <option value="0">Free</option>
								</select>
							</div>
						</div>
						<div class="form-group row m-b-15 testseriesdiv hide">
							<label class="col-form-label col-md-3">Test Series <span class="star">*</span></label>
							<div class="col-md-9">
								<select class="form-control testseries" name="testseries" data-style="btn-white" required>
								     <?php $tsdtarr = array();
                                         if (!empty($tsdt)) {
                                             foreach ($tsdt as $tsdts) {
                                                 $tsdtarr[] = $tsdts->TS_ID;
                                             }
                                         }
                                        $tsids = implode(',', $tsdtarr);
                                        ?>
                                        <option value="<?php echo $tsids; ?>">All</option>
                                        <?php if (!empty($tsdt)) {
                                            foreach ($tsdt as $tsdts) { ?>
                                            <option value=<?php echo $tsdts->TS_ID; ?>><?php echo $tsdts->TS_NAME; ?></option>
                                        <?php }
                                        } ?>
								</select>
							</div>
						</div>
					    <div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">User <span class="star">*</span></label>
							<div class="col-md-9">
								<select class="usermultipleselect form-control users" name="users[]" multiple="multiple" placeholder="select users" required>
								
								</select>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Template option <span class="star">*</span></label>
							<div class="col-md-9">
								<select class="form-control sendmsgtemplate" name="sendmsgtemplate" required>

								</select>
							</div>
						</div>
					    <div class="form-group row m-b-15 m-b-10">
							<label class="col-form-label col-md-2">Template</label>
							<div class="col-md-10">
							    <textarea class="sendmessagetemp" name="sendmessagetemp" id="mysummernote"></textarea>
							</div>
						</div>
						
						<div class="form-group row m-b-10 m-t-10">
							<div class="col-md-12 text-right">
								<button type="submit" class="btn btn-primary sendmail">Send</button>
							</div>
						</div>
					</form>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- begin col-6 -->
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Template</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					
					<form id="templateform">
					    <input type="hidden" class="sending_type" name="sending_type" value="2">
					    <legend class="m-b-15">Create Template</legend>
					    <div class="form-group row m-b-15 m-b-10">
							<label class="col-form-label col-md-2">Template Title <span class="star">*</span></label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="temptitle" required>
							</div>
						</div>
					    <div class="form-group row m-b-15 m-b-10">
							<label class="col-form-label col-md-2">Template <span class="star">*</span></label>
							<div class="col-md-10">
                                <textarea class="temp" name="temp" id="mysummernote"></textarea>
							</div>
						</div>
						<div class="form-group row m-b-10 m-t-10">
							<div class="col-md-12 text-right">
								<button type="submit" class="btn btn-primary templateform">Add Template</button>
							</div>
						</div>
					</form>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-6 -->
		
		<!-- end col-6 -->
		<!-- begin col-6 -->
	
		<!-- end col-6 -->
		
		<!-- begin col-6 -->
	
		<!-- end col-6 -->
		<!-- begin col-6 -->
	
		<!-- end col-6 -->
	</div>
</div>


<div class="modal fade" id="edittemplatemodal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">EDIT TEMPLATE</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="edittemplateform">
				<div class="modal-body">
					<input type='hidden' class='tempid' name="tempid" value='' />
					<div class="form-group row m-b-15 m-b-10">
						<label class="col-form-label col-md-2">Template Title <span class="star">*</span></label>
						<div class="col-md-10">
							<input type="text" class="form-control temptitle" name="temptitle"  required>
						</div>
					</div>
				    <div class="form-group row m-b-15 m-b-10">
						<label class="col-form-label col-md-2">Template <span class="star">*</span></label>
						<div class="col-md-10">
							<textarea class="temp" name="temp" id="mysummernote"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit" class="btn btn-success edittemplateform">Update Template</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="editsetscheduling">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">EDIT SCHEDULING</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="editsetschedulingform">
			    <input type="hidden" class="sending_type" name="sending_type" value="2">
			    <input type="hidden" class="schid" name="schid" >
				<div class="modal-body">
				    <div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Student Type <span class="star">*</span></label>
						<div class="col-md-9">
							<select class="form-control stdtypeschededit" name="stdtype" data-style="btn-white" required>
							    <option value="2">All</option>
							    <option value="1">Paid</option>
							    <option value="0">Free</option>
							</select>
						</div>
					</div>
					<div class="form-group row m-b-15 testseriesschededitdiv hide">
						<label class="col-form-label col-md-3">Test Series <span class="star">*</span></label>
						<div class="col-md-9">
							<select class="form-control testseriesschededit"  name="testseries" data-style="btn-white" required>
							    <?php $tsdtarr = array();
                                 if (!empty($tsdt)) {
                                     foreach ($tsdt as $tsdts) {
                                         $tsdtarr[] = $tsdts->TS_ID;
                                     }
                                 }
                                $tsids = implode(',', $tsdtarr);
                                ?>
                                <option value="<?php echo $tsids; ?>" attr-html="All">All</option>
                                <?php if (!empty($tsdt)) {
                                    foreach ($tsdt as $tsdts) { ?>
                                    <option value="<?php echo $tsdts->TS_ID; ?>" attr-html="Single"><?php echo $tsdts->TS_NAME; ?></option>
                                <?php }
                                } ?>
							</select>
						</div>
					</div>
				    <div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">User <span class="star">*</span></label>
						<div class="col-md-9">
							<select class="usermultipleschededit form-control"data-width="100%" name="users[]" multiple="multiple" placeholder="select users" >
							
							</select>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Template option <span class="star">*</span></label>
						<div class="col-md-9">
							<select class="form-control sendmsgtemplateschededit" name="sendmsgtemplate" required>

							</select>
						</div>
					</div>
				    <div class="form-group row m-b-15 m-b-10">
						<label class="col-form-label col-md-2">Template</label>
						<div class="col-md-10">
							<textarea class="form-control sendmessagetempschededit" name="sendmessagetemp" ></textarea>
						</div>
					</div>
					
					<div class="form-group row m-b-15" style="font-size: 14px;">
						<label class="col-md-3 col-form-label">Scheduling</label>
						<div class="col-md-9">
							<div class="radio radio-css radio-inline">
								<input type="radio" class="scheduletype" id="inlineCssRadio5" name="scheduletype" value="1" />
								<label for="inlineCssRadio5">Daily</label>
							</div>
							<div class="radio radio-css radio-inline">
								<input type="radio" class="scheduletype" id="inlineCssRadio6" name="scheduletype" value="2" />
								<label for="inlineCssRadio6">Weekly</label>
							</div>
							<div class="radio radio-css radio-inline">
								<input type="radio" class="scheduletype" id="inlineCssRadio7" name="scheduletype" value="3" />
								<label for="inlineCssRadio7">Monthly</label>
							</div>
							<div class="radio radio-css radio-inline">
								<input type="radio" class="scheduletype" id="inlineCssRadio8" name="scheduletype" value="4" />
								<label for="inlineCssRadio8">Yearly</label>
							</div>
						</div>
					</div>
					
			        <div class="form-group row m-b-15 m-b-10">
			            <label class="col-form-label col-md-3">Date <span class="star">*</span></label>
						<div class="col-md-9">
							<div class="input-group date" id="datetimepicker2">
								<input type="text" name="datetime" class="form-control datetime"  value="" required>
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
							</div>
						</div>
						
			        </div>
			        <div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Status <span class="star">*</span></label>
						<div class="col-md-9">
							<select class="form-control status" name="status" data-style="btn-white" required>
							    <option value="1">Active</option>
							    <option value="0">Inactive</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success editsetschedulingform">Update Scheduling</button>
				</div>
			</form>
		</div>
	</div>
</div>


<script>
	$(document).ready(function() {
	    
	   
	    
	    $("#datetimepicker1").datetimepicker({format:"YYYY-MM-DD HH:mm:ss"})
	    $("#datetimepicker2").datetimepicker({format:"YYYY-MM-DD HH:mm:ss"})
	    $(".usermultipleselect").attr("data-placeholder","Select users");
        $(".usermultipleselect").select2();
        $('.usermultipleselect').on('change' , function(){
            if($('option:selected',this).html()=="All"){
                var value = $('option:selected',this).val();
                $('.usermultipleselect').val(null);
                $(".usermultipleselect").val(value);
                $(".usermultipleselect").select2();
                
                $(".usermultipleselect option[attr-opt='2']").prop('disabled','disabled');
                $(".usermultipleselect").select2();
                
            }else{
                $(".usermultipleselect option[attr-opt='2']").prop('disabled',false);
                $(".usermultipleselect").select2();
            }
        });
        
        $(".usermultipleselectsched").attr("data-placeholder","Select users");
        $(".usermultipleselectsched").select2();
        $('.usermultipleselectsched').on('change' , function(){
            if($('option:selected',this).html()=="All"){
                var value = $('option:selected',this).val();
                $('.usermultipleselectsched').val(null);
                $(".usermultipleselectsched").val(value);
                $(".usermultipleselectsched").select2();
                
                $(".usermultipleselectsched option[attr-opt='2']").prop('disabled','disabled');
                $(".usermultipleselectsched").select2();
                
            }else{
                $(".usermultipleselectsched option[attr-opt='2']").prop('disabled',false);
                $(".usermultipleselectsched").select2();
            }
        });
        
        $(".usermultipleschededit").select2();
        $(".usermultipleschededit").attr("data-placeholder","Select users");
        $('.usermultipleschededit').on('change' , function(){
            if($('option:selected',this).html()=="All"){
                var value = $('option:selected',this).val();
                $('.usermultipleschededit').val(null);
                $(".usermultipleschededit").val(value);
                $(".usermultipleschededit").select2();
                
                $(".usermultipleschededit option[attr-opt='2']").prop('disabled','disabled');
                $(".usermultipleschededit").select2();
                
            }else{
                $(".usermultipleschededit option[attr-opt='2']").prop('disabled',false);
                $(".usermultipleschededit").select2();
            }
        });
        
        $(".temp").summernote({
            placeholder:"",
            fontSizes: ['12', '14', '16', '18', '24', '36', '48']
        });
        $(".sendmessagetempsched").summernote({
            placeholder:"",
            fontSizes: ['12', '14', '16', '18', '24', '36', '48']
        });
        $(".sendmessagetemp").summernote({
            placeholder:"",
            fontSizes: ['12', '14', '16', '18', '24', '36', '48']
        });
	});
</script>