<script>
 $('document').ready(function(){
     get_student_by_paytype(1);
     get_student_by_paytype(2);
     
     getalltemplate(1,0);
     getalltemplate(2,0);
     getalltemplate(3,0);
     getalltemplate(0,1);
     getallscheduling();
     $('body').on('change','.stdtype,.testseries',function(){
         if($('option:selected','.stdtype').val()==2 || $('option:selected','.stdtype').val()==0){
		    $('.testseriesdiv').addClass('hide');
		}else{
		    $('.testseriesdiv').removeClass('hide');
		}
		get_student_by_paytype(1);
	 })
	 $('body').on('change','.stdtypesched,.testseriessched',function(){
	     if($('option:selected','.stdtypesched').val()==2 || $('option:selected','.stdtypesched').val()==0){
		    $('.testseriesscheddiv').addClass('hide');
		}else{
		    $('.testseriesscheddiv').removeClass('hide');
		}
		get_student_by_paytype(2);
		
	 })
	 $('body').on('change','.stdtypeschededit,.testseriesschededit',function(){
	     if($('option:selected','.stdtypeschededit').val()==2 || $('option:selected','.stdtypeschededit').val()==0){
		    $('.testseriesschededitdiv').addClass('hide');
		}else{
		    $('.testseriesschededitdiv').removeClass('hide');
		}
		get_student_by_paytype(3);
		
	 })
	$('body').on('change','.sendmsgtemplate',function(){
	    var tempid = $('option:selected',this).val();
		get_template_by_tempid(tempid,1);
	 })
	 $('body').on('change','.sendmsgtemplatesched',function(){
	    var tempid = $('option:selected',this).val();
		get_template_by_tempid(tempid,2);
	 })
	 $('body').on('click','.edittemp',function(){
	     var tempid = $(this).attr('attr-tempid');
	     get_template_by_tempid(tempid,3);
	 })
	 $('body').on('click','.edit_scheduling',function(){
	     $(".usermultipleschededit").select2();
	     var schid = $(this).attr('attr-schedid');
	     var tempid = $(this).attr('attr-tempid');
	     get_template_by_tempid(tempid,4);
	     get_student_by_paytype(3);
	     $('#editsetscheduling').modal('show');
	     get_schedulingdt_by_schedid(schid);
	 })
	 $('body').on('change','.sendmsgtemplateschededit',function(){
	     var tempid = $('option:selected',this).val();
	     get_template_by_tempid(tempid,4);
	 })
 });
  function get_student_by_paytype(types){
     if(types==2){
         var tsids = $('option:selected','.testseriessched').val();
        var stdtype = $('option:selected','.stdtypesched').val();
    }else if(types==1){
        var tsids = $('option:selected','.testseries').val();
        var stdtype = $('option:selected','.stdtype').val();
    }else{
        var tsids = $('option:selected','.testseriesschededit').val();
        var stdtype = $('option:selected','.stdtypeschededit').val();
    }
     $.ajax({
		type: 'GET',
		data:'type='+ stdtype+"&tsids="+tsids,
		url: base_loc + 'admingetajax/get_student_by_paytype',
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
		    if(types==2){
		        $('.usermultipleselectsched').html('');
    		    $(".usermultipleselectsched").select2();
		    }else if(types==1){
		        $('.usermultipleselect').html('');
    		    $(".usermultipleselect").select2();
		    }else{
		        $('.usermultipleschededit').html('');
    		    $(".usermultipleschededit").select2();
		    }
		    
			var str='';
			if(msg.status==200){
			    if(msg.data.length>0){
			        var useridarr = new Array();
					$.each(msg.data, function(index, element) {
						str += '<option attr-html="single" attr-opt="2" value="'+element.USER_ID+'">'+element.USER_FNAME+" "+element.USER_LNAME+'</option>';
						useridarr.push(element.USER_ID);
					});
					var userids = useridarr.join();
		            var str1 = "<option attr-opt='1' attr-html='All' value='" + userids + "'>"+'All'+"</option>";
    				
    				if(types==2){
    				    $('.usermultipleselectsched').append(str1);
        				$('.usermultipleselectsched').append(str);
        				$(".usermultipleselectsched").select2();
    				}else if(types==1){
    				    $('.usermultipleselect').append(str1);
        				$('.usermultipleselect').append(str);
        				$(".usermultipleselect").select2();
    				}else{
    				    
    				    $('.usermultipleschededit').append(str1);
        				$('.usermultipleschededit').append(str);
        				$(".usermultipleschededit").select2();
    				}
    				
			    }
			}else{
				location.href = base_loc;
			}
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc; 
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc; 
			}
		}
	});
 }
 
 function getalltemplate(types,tables){
     $.ajax({
		type: 'GET',
		url: base_loc + 'admingetajax/getalltemplate',
		data:'sending_type='+2,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
		    if(tables==1){
		        $('.temptablebody').html('');
		    }else{
		        if(types==1){
    		        $('.sendmsgtemplate').html('');
    		    }else if(types==2){
    		        $('.sendmsgtemplatesched').html('');
    		    }else{
    		        $('.sendmsgtemplateschededit').html('');
    		    }
		    }
			var str='<option value="">Select Template</option>';
			var tablestr = '';
			var i = 1;
			if(msg.status==200){
			    if(msg.data.length>0){
			        
					$.each(msg.data, function(index, element) {
					    if(tables==1){
            		        tablestr += '<tr> <td>'+i+'</td> <td>'+element.MSGTEMP_TITLE+'</td> <td>'+element.MSGTEMP_MSG.substring(0,40)+'  .....</td><td class="with-btn">	<a href="javascript:void(0)" class="btn btn-sm btn-primary width-60 m-r-2 edittemp" attr-tempid="'+element.MSGTEMP_ID+'">Edit</a></td></tr>';
            		    }else{
            		        str += '<option attr-opt="2" value="'+element.MSGTEMP_ID+'">'+""+element.MSGTEMP_TITLE+'</option>';
            		    }
						i++;
					});
					if(tables==1){
					    $('.temptablebody').html(tablestr);
					}else{
					    if(types==1){
                	        $('.sendmsgtemplate').append(str);
                	    }else if(types==2){
                	        $('.sendmsgtemplatesched').append(str);
                	    }else{
            		        $('.sendmsgtemplateschededit').append(str);
            		    }
					}
			    }else{
			        tablestr = '<tr> <td style="text-align:center;" colspan="4">Data Not availabe!</td></tr>';
			        if(tables==1){
			            $('.temptablebody').html(tablestr);
			        }
            		 
			    }
			}else{
				location.href = base_loc;
			}
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc; 
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc; 
			}
		}
	});
 }
 function get_template_by_tempid(tempid,types){
     if(tempid!=''){
         $.ajax({
    		type: 'GET',
    		url: base_loc + 'admingetajax/get_templatedt_by_tempid',
    		data:'tempid='+ tempid,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    		    if(types==1){
                    $('.sendmessagetemp').html('');
                }else if(types==2){
                    $('.sendmessagetempsched').html('');
                }else if(types==3){
                    $('#edittemplatemodal').find('.temp').summernote("code", '');
                    $('#edittemplatemodal').find('.temptitle').val('');
                    $('#edittemplatemodal').find('.tempid').val('');
                }else{
                    $('.sendmsgtemplateschededit').val('');
                    $('.sendmessagetempschededit').html('');
                }
    			if(msg.status==200){
    			    if(types==1){
                        $('.sendmessagetemp').summernote("code", msg.data.MSGTEMP_MSG);
                    }else if(types==2){
                        $('.sendmessagetempsched').summernote("code", msg.data.MSGTEMP_MSG);
                    }else if(types==3){
                        $('#edittemplatemodal').find('.temp').summernote("code", msg.data.MSGTEMP_MSG);
                        // $('#edittemplatemodal').find('.temp').html(msg.data.MSGTEMP_MSG);
                        $('#edittemplatemodal').find('.temptitle').val(msg.data.MSGTEMP_TITLE);
                        $('#edittemplatemodal').find('.tempid').val(msg.data.MSGTEMP_ID);
                        $('#edittemplatemodal').modal('show');
                    }else{ 
                        $('.sendmsgtemplateschededit').val(msg.data.MSGTEMP_ID);
                        $('.sendmessagetempschededit').summernote("code", msg.data.MSGTEMP_MSG);
                    }
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
     }else{
         if(types==1){
             $('.sendmessagetemp').html('');
         }else{
             $('.sendmessagetempsched').html('');
         }
         
     }
 }
  function getallscheduling(){
     $.ajax({
		type: 'GET',
		url: base_loc + 'admingetajax/getallscheduling',
		data:'sending_type='+ 2,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
		    console.log(msg);
    	    $('.schedulinglist').html('');
			var tablestr = '';
			var i=1;
			if(msg.status==200){
			    if(msg.data.length>0){
					$.each(msg.data, function(index, element) {
					    if(element.SCH_TYPE==1){
					        var schedtype = "Daily";
					    }else if(element.SCH_TYPE==2){
					        var schedtype = "Weekly";
					    }else if(element.SCH_TYPE==3){
					        var schedtype = "Monthly";
					    }else{
					        var schedtype = "Yearly";
					    }
					    if(element.SCH_STATUS==1){
					        var status = "Active";
					        var addclass="success";
					    }else{
					        var status = "Inactive";
					        var addclass="danger";
					    }
            		    tablestr += '<tr> <td>'+i+'</td><td>'+schedtype+'</td> <td>'+element.MSGTEMP_TITLE+'</td> <td class="with-btn"><a href="javascript:void(0)" class="btn btn-sm btn-'+addclass+' width-60 m-r-2">'+status+'</a></td> <td class="with-btn"><a href="javascript:void(0)" class="btn btn-sm btn-primary width-60 m-r-2 edit_scheduling" attr-schedid="'+element.SCH_ID+'" attr-tempid="'+element.MSGTEMP_ID+'">Edit</a></td> </tr>';
					    i++;
					});
					$('.schedulinglist').html(tablestr);
			    }else{
            		   tablestr += '<tr> <td colspan="5" style="text-align:center;">Data not available!</td> </tr>';
            		   $('.schedulinglist').html(tablestr);
			    }
			}else{
				location.href = base_loc;
			}
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc; 
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc; 
			}
		}
	});
 }
 function get_schedulingdt_by_schedid(schid){
     $.ajax({
		type: 'GET',
		url: base_loc + 'admingetajax/get_schedulingdt_by_schedid',
		data:'schid='+ schid,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
			if(msg.status==200){
			    $('.stdtypeschededit').val(msg.data.SCH_PAY_TYPE);
                $('.testseriesschededit').val(msg.data.SCH_TS_ID);
                $('.schid').val(msg.data.SCH_ID);
                $('.status').val(msg.data.SCH_STATUS);
        		if(msg.data.SCH_PAY_TYPE!=0){
        			 $('.stdtypeschededit').trigger('change');
			    }else{
        			 $('.testseriesschededit').trigger('change');
			    }
                setTimeout(function(){
                    var alluserval = $('.usermultipleschededit').find("option[attr-html='All']").val();
    			    if(alluserval==msg.data.USER_ID){
    			        $(".usermultipleschededit").val(msg.data.USER_ID).trigger('change');
    			    }else{
    			        var useridsstr = msg.data.USER_ID;
    			        useridsarr = useridsstr.split(',');
    			        $(".usermultipleschededit").val(useridsarr).trigger('change');
    			    }
    			    if(msg.data.SCH_STATUS==1){
    			        
    			    }else{
    			        
    			    }
                },2000);
                $(".scheduletype[value='"+msg.data.SCH_TYPE+"']").prop('checked',true);
			    $('.datetime').val(msg.data.SCH_DATE);
			    $(".sendmessagetempschededit").summernote("disable");
			}else{
				location.href = base_loc;
			}
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc; 
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc; 
			}
		}
	});
 }
 
 
</script>