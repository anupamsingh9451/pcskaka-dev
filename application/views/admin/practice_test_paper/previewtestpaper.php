<link href="<?php echo base_url(); ?>assets/adminassets/css/test_paper/previewtestpaper.css" rel="stylesheet" />
<div class="col-md-12">
    <div class="row">
        <div class="col-md-4">
    		   <h5 class="">Test Paper Name &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo $tpname; ?></span></h5>
    	</div>
    	<div class="col-md-4">
    		   <h5 class="">Start Date &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo $fstartdate; ?></span></h5>
    	</div>
    	<div class="col-md-4">
    		   <h5 class="">End Date &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo $fenddate; ?></span></h5>
    	</div>
    	<div class="col-md-4">
    	    <h5 class="">Created Date &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo $fcreateddate; ?></span></h5></h5>
    	</div>
    	<div class="col-md-4">
    		   <h5 class="">Descreptione &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <span class="f-w-600 f-s-12 colorspan"><?php echo $fdesc; ?></span></h5>
    	</div>
    </div>
    <div class="row">
        <div class="col-md-3">
			<label class="col-form-label">Subjects <span class="star">*</span></label>
			<select class="form-control all_sub" name="all_sub" data-style="btn-white" required="">
                
			</select>
		</div>
		<div class="col-md-3">
			<label class="col-form-label">Books <span class="star">*</span></label>
			<select class="form-control all_book" name="all_book" data-style="btn-white" required="">
                
			</select>
		</div>
		<div class="col-md-3">
			<label class="col-form-label">Level <span class="star">*</span></label>
			<select class="form-control level" name="level" data-style="btn-white" required="">
                <option value="1,2,3">All</option>
                <option value="1">Low</option>
                <option value="2">Medium</option>
                <option value="3">High</option>
			</select>
		</div>
		<div class="col-md-3" style="margin-top: auto;">
			<a class="btn btn-success filtertppreviewbtn" href="javascript:void(0);"><i class="fa fa-fw fa-search"></i> Search</a>
		</div>
    </div>
    <div class="row m-t-5">
        <table class="table table-striped table-bordered">
        	<thead>
        		<tr>
        			<th width="6%">Sr. No.</th>
        			<th width="60%" >Question</th>
        			<th>Subject</th>
        			<th>Difficulty level</th>
        			<th>Tools</th>
        		</tr>
        	</thead>
        	<tbody>
        	    <?php $i=1; foreach ($quesdt as $quesdts) { ?>
                	<tr class="previewtabletr" attr-subid="<?php echo $quesdts->SUB_ID; ?>" attr-bookid="<?php echo $quesdts->BOOK_ID; ?>" attr-level="<?php echo $quesdts->QUES_DIFFICULTY_LEVEL; ?>" >
            			<td width="6%" style="font-weight:700;"><?php echo $i.'.'; ?></td>
            			<td style="font-size: 13px;font-weight: 600;" width="60%"><?php if ($quesdts->QUES_TYPE==1) {
    echo $quesdts->QUES;
} else {
    echo json_decode($quesdts->QUES)->QUESTION;
} ?></td>
            			<td style="font-size: 13px;font-weight: 600;"><?php echo $quesdts->SUB_NAME; ?><br><span style="font-size: 11px;font-weight: 500; ">(<?php echo $quesdts->BOOK_NAME; ?>)</span></td>
            			<td style="font-size: 13px;font-weight: 600;"><?php if ($quesdts->QUES_DIFFICULTY_LEVEL==1) {
    echo "LOW";
} elseif ($quesdts->QUES_DIFFICULTY_LEVEL==2) {
    echo "MEDIUM";
} else {
    echo "HIGH";
}?></td>
            		    <td><a href="javascript:void(0);" class="btn btn-danger btn-icon btn-circle btn-sm quesaddintpbtn" attr-quesid="<?php echo $quesdts->QUES_ID; ?>" attr-added="1" attr-pre="1"><i class="fa fa-times"></i></a></td>
            		</tr>
            	<?php $i++; } ?>
        	</tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-lg-12 text-center m-t-10">
            <a href="javascript:void(0);" class="btn btn-sm btn-warning m-r-2 p-4 m-b-10 submittestpaper">Submit</a>
        </div>
    </div>
</div>