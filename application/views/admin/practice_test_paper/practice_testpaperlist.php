<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item active"><a href="javascript:;">Practice Test</a></li>
        
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Practice Test</h1>
    <!-- end page-header -->
    <div class="row no_mrgn addbx_top_rowhead pd_15">
        <div class="col-md-6">
            <div class="row">
                <h5 class="">Test Set Name &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; </h5><span class=""><?php echo $tsdt[0]->TS_NAME; ?></span>
            </div>
        </div>
        
        <div class="col-md-6 text-right">
            <a class="btn btn-success" href="<?php echo base_url(); ?>ratan/practice_test/list/<?php echo $tsdt[0]->TS_ID; ?>" ><i class="fa fa-fw fa-hdd"></i> Back</a>
            <!-- <a class="btn btn-warning" href="<?php echo base_url(); ?>ratan/practicetestpaper/create_practicetestpapers/<?php echo $tsdt[0]->TS_ID; ?>" ><i class="fa fa-fw fa-hdd"></i> Add Practice Test Paper New</a> -->

        </div>
    </div>
    <!-- begin row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel brdr">
                <!-- begin panel-heading -->
                <div class="panel-heading brdr_btm">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Practice Test List</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body table-responsive">
                    <input type="hidden" class="tsid" value="<?php echo $tsdt[0]->TS_ID; ?>">
                    <table id="testpapertable" class="table table-striped table-bordered datatable">
                        <thead>
                            <tr>
                                <th width="1%">Sr No.</th>
                                <th>Prcatice Test Name</th>
                                <!--<th>Subject Name</th>-->
                                <th>No. of Question</th>
                                <th>Created Date</th>
                                <!-- <th>Start Date</th> -->
                                <th>Status</th>
                                <th>Published</th>
                                <th class="text-nowrap">Tools</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                </div>
                <!-- end panel-body -->
            </div>
        </div>
    </div>
</div>

<!-- Add Question Modal -->
<div class="modal fade" id="addtetspaper" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="background-color:unset">
            <div class="modal-header" style="background-color: #348fe2 ;">
                <h4 class="modal-title" style="color:white">Add Test Paper</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body bg_grey">
                <div class='col-md-12'>
                    <!-- begin panel -->
                    <div class="panel" style="border-radius: 10px;border: 1px solid #ccc6c6;">
                        <!-- begin panel-body -->
                        <div class="panel-body">
                            <form id="testpapercreate">
                                <input type="hidden" name="testseriesid" value="<?php echo $tsdt[0]->TS_ID; ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="col-form-label">Test Paper Name <span class="star">*</span></label>
                                        <input type="text" class="form-control testpapername" name="testpapername" placeholder="Enter Test Paper Name" required>
                                    </div>
                                    <div class="col-md-6 hide">
                                        <label class="col-form-label">Test Paper Type <span class="star">*</span></label>
                                        <select class="form-control testpapertype" name="testpapertype" data-style="btn-white" required>
                                           <option value="2">Objective</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-form-label">Subject <span class="star">*</span></label>
                                        <select class="form-control subject" name="subject" data-style="btn-white" required>
                                           
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-form-label">Books <span class="star">*</span></label>
                                        <select class="form-control book" name="book" data-style="btn-white" required>
                                           
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-form-label">Questions <span class="star">*</span> </label><span class="m-t-10 m-r-5 f-w-600 text-blue countques" style="float:right"></span>
                                        <select class="form-control questions selectpicker" multiple name="questions[]" data-live-search="true" data-style="btn-white">
                                           
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-form-label">Positive Mark <span class="star">*</span></label>
                                        <input type="text" class="form-control p_mark input_num" name="p_mark" placeholder="Enter Positive Mark" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-form-label">Negatie Mark </label>
                                        <input type="text" class="form-control n_mark input_num" name="n_mark" placeholder="Enter Negative mark" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-form-label"> Start Date <span class="star">*</span></label>
                                        <input type="text" class="form-control dtpicker start_date" name="start_date" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-form-label"> End Date <span class="star">*</span></label>
                                        <input type="text" class="form-control dtpicker end_date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                    <div class='col-sm-6 hide'>
                                        <label class="col-form-label">Duration </label>
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' class="form-control duration" name="duration" value="<?php echo "00:00"; ?>" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-time"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-form-label"> Created Date <span class="star">*</span></label>
                                        <input type="text" class="form-control dtpicker created_date" name="created_date" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-form-label">Test Paper Description <span class="star">*</span></label>
                                        <textarea rows="2" class="form-control desc" name="desc"></textarea>
                                    </div>
                                    
                                    
                                    <div class="col-md-12" style="margin-top:10px">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-primary testpapercreate">SAVE</button>
                                        </div>
                                    </div>
                             </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
