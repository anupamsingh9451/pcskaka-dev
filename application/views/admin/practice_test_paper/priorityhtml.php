<link href="<?php echo base_url(); ?>assets/adminassets/css/test_paper/priorityhtml.css" rel="stylesheet" />
<div class="row">
    <div class="col-lg-12">
        <!-- begin panel -->
        <div class="panel panel-default panel-with-tabs" style="border: 1px solid #348fe2;">
        	<!-- begin panel-heading -->
        	<div class="p-0" style="background-color: #348fe2;">
        		<div class="panel-heading-btn m-r-10 m-t-10">
        			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        		</div>
        		<!-- begin nav-tabs -->
        		<div class="tab-overflow ">
        			<ul class="nav nav-tabs nav-priority-tabs" style="width:100%;background-color: #348fe2;">
						<li class="nav-item text-center">
							<a href="#nav-tab-<?php echo $subid.'-high-'.$bookid; ?>" priority-tab-attr="<?php echo $subid.'_high_'.$bookid; ?>" attr-priority-name="high" attr-subid="<?php echo $subid; ?>" attr-bookid="<?php echo $bookid; ?>" data-toggle="tab" class="priority_tab nav-link first-priority-tab active">HIGH<span class="priority_no label label-success pull-right mrgn-r-10"><?php echo $high_level; ?></span></a>
						</li>
						<li class="nav-item text-center">
							<a href="#nav-tab-<?php echo $subid.'-medium-'.$bookid; ?>" priority-tab-attr="<?php echo $subid.'_medium_'.$bookid; ?>" attr-priority-name="medium" attr-subid="<?php echo $subid; ?>" attr-bookid="<?php echo $bookid; ?>" data-toggle="tab" class="priority_tab nav-link ">MEDIUM<span class="priority_no label label-success pull-right mrgn-r-10"><?php echo $medium_level; ?></span></a>
						</li>
						<li class="nav-item text-center">
							<a href="#nav-tab-<?php echo $subid.'-low-'.$bookid; ?>" priority-tab-attr="<?php echo $subid.'_low_'.$bookid; ?>" attr-priority-name="low" attr-subid="<?php echo $subid; ?>" attr-bookid="<?php echo $bookid; ?>" data-toggle="tab" class="priority_tab nav-link ">LOW<span class="priority_no label label-success pull-right mrgn-r-10"><?php echo $low_level; ?></span></a>
						</li>
        			</ul>
        		</div>
        		<!-- end nav-tabs -->
        	</div>
        	<!-- end panel-heading -->
        	<!-- begin tab-content -->
        	<div class="tab-content p-t-0">
    		    <!-- begin tab-pane -->
				    <div class="priority-tab-content tab-pane fade active show" priority-tab-content-attr="<?php echo $subid.'_high_'.$bookid; ?>" id="nav-tab-<?php echo $subid.'-high-'.$bookid; ?>">
				        
				    </div>
				    <div class="priority-tab-content tab-pane fade" priority-tab-content-attr="<?php echo $subid.'_medium_'.$bookid; ?>" id="nav-tab-<?php echo $subid.'-medium-'.$bookid; ?>">
				        
				    </div>
				    <div class="priority-tab-content tab-pane fade" priority-tab-content-attr="<?php echo $subid.'_low_'.$bookid; ?>" id="nav-tab-<?php echo $subid.'-low-'.$bookid; ?>">
				        
				    </div>
        	    <!-- end tab-pane -->
        	</div>
        	<!-- end tab-content -->
        </div>
    <!-- end panel -->
    </div>
</div>

<script>
    $('document').ready(function(){
        if($('.first-priority-tab').length>0){
            var attrpriorityname = $('.first-priority-tab').attr('attr-priority-name');
            $('.priority_tab[attr-priority-name="'+attrpriorityname+'"]').trigger('click');
        }
    });
</script>