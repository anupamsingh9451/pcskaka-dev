<link href="<?php echo base_url(); ?>assets/adminassets/css/test_paper/bookhtml.css" rel="stylesheet" />
<div class="row">
    <div class="col-lg-12">
        <!-- begin panel -->
        <div class="panel panel-default panel-with-tabs">
        	<!-- begin panel-heading -->
        	<div class="p-0" style="background: #c1ccd1;">
        		<div class="panel-heading-btn m-r-10 m-t-10">
        			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        		</div>
        		<!-- begin nav-tabs -->
        		<div class="tab-overflow ">
        			<ul class="nav p-8">
        				<li class="nav-item prev-button "><a href="javascript:;" data-click="prev-tab" class="nav-link text-success"><i class="fa fa-arrow-left"></i></a></li>
    					<?php $firstbooktab="first-book-tab active"; foreach ($bookdt['data'] as $bookdts) { ?>
    					    <li class="nav-item ">
        				        <a href="#nav-tab-<?php echo $subid; ?>-<?php echo $bookdts->BOOK_ID;?>" book-tab-attr="<?php echo $subid.'_book_'.$bookdts->BOOK_ID; ?>" attr-subid="<?php echo $subid; ?>" attr-bookid="<?php echo $bookdts->BOOK_ID; ?>" data-toggle="tab" class="book_tab nav-link <?php echo $firstbooktab; ?>"><?php echo $bookdts->BOOK_NAME; ?></a>
        				    </li>
    					<?php $firstbooktab="";} ?>
    					<li class="nav-item next-button"><a href="javascript:;" data-click="next-tab" class="nav-link text-success"><i class="fa fa-arrow-right"></i></a></li>
        			</ul>
        		</div>
        		<!-- end nav-tabs -->
        	</div>
        	<!-- end panel-heading -->
        	<!-- begin tab-content -->
        	<div class="tab-content p-t-0 p-0">
    		    <!-- begin tab-pane -->
    		    <?php $firsttabdiv="active show"; foreach ($bookdt['data'] as $bookdts) { ?>
				    <div class="book-tab-content tab-pane fade <?php echo $firsttabdiv;?>" book-tab-content-attr="<?php echo $subid.'_book_'.$bookdts->BOOK_ID; ?>" id="nav-tab-<?php echo $subid; ?>-<?php echo $bookdts->BOOK_ID;?>">
				        <div class="col-md-12">
    				        <div class="row">
    				            <div class="col-md-1">
    				                <label class="col-form-label">High&nbsp;&nbsp;&nbsp; <span class="totalquesbyhighpriority" attr-bookid="<?php echo $bookdts->BOOK_ID; ?>" attr-subid="<?php echo $subid; ?>" style="color:#348fe2;font-size:12px;"><?php echo sizeof($this->Admingetmodel->get_remain_practice_question_by_subid_bookid_level($subid, $bookdts->BOOK_ID, 3, "", $tsid)); ?></span></label>
    				                <!--<label class="col-form-label">High</label>-->
                            		<input type="text" class="form-control high_level input_num height-25" name="high">
    				            </div>
    				            <div class="col-md-1">
    				                <label class="col-form-label">Medium&nbsp;&nbsp;&nbsp; <span class="totalquesbymediumpriority" attr-bookid="<?php echo $bookdts->BOOK_ID; ?>" attr-subid="<?php echo $subid; ?>" style="color:#348fe2;font-size:12px;" ><?php echo sizeof($this->Admingetmodel->get_remain_practice_question_by_subid_bookid_level($subid, $bookdts->BOOK_ID, 2, "", $tsid)); ?></label>
    				                <!--<label class="col-form-label">Medium</label>-->
                            		<input type="text" class="form-control medium_level input_num height-25" name="medium">
    				            </div>
    				            <div class="col-md-1">
    				                <label class="col-form-label">Low&nbsp;&nbsp;&nbsp; <span class="totalquesbylowpriority" attr-bookid="<?php echo $bookdts->BOOK_ID; ?>" attr-subid="<?php echo $subid; ?>" style="color:#348fe2;font-size:12px;"><?php echo sizeof($this->Admingetmodel->get_remain_practice_question_by_subid_bookid_level($subid, $bookdts->BOOK_ID, 1, "", $tsid)); ?></label>
    				                <!--<label class="col-form-label">Low</label>-->
                            		<input type="text" class="form-control low_level input_num height-25" name="low">
    				            </div>
    				            <div class="col-md-1 text-center">
                            		<button type="button" class="btn btn-warning m-t-30 addprioritybtn height-25" style="padding:0 10px;" attr-bookid="<?php echo $bookdts->BOOK_ID; ?>" attr-subid="<?php echo $subid; ?>">Submit</button>
    				            </div>
    				        </div>
    				        
    				    </div>
    				    <div class="col-md-12 m-t-10">
    				        <div class=" <?php echo $subid; ?>_priority_div_<?php echo $bookdts->BOOK_ID; ?>">
    				            
    				        </div>
    				    </div>
				    </div>
				<?php $firsttabdiv=""; } ?>
        	    <!-- end tab-pane -->
        	</div>
        	<!-- end tab-content -->
        </div>
    <!-- end panel -->
    </div>
</div>

<script>
    $('document').ready(function(){
        if($('.first-book-tab').length>0){
            // $('.firs-tsubject-tab').trigger('click');
            var attrbookid = $('.first-book-tab').attr('attr-bookid');
            $('.book_tab[attr-bookid="'+attrbookid+'"]').trigger('click');
        }
    });
</script>