<script>
$('document').ready(function(){
    $('body').on('click','.subject_tab', function () {
        var attrsubid = $(this).attr('attr-subid');
        get_book_html(attrsubid);
    });

    $('body').on('click','.addprioritybtn', function () {
        var attrbookid = $(this).attr('attr-bookid');
        var attrsubid = $(this).attr('attr-subid');
        get_priority_html(attrbookid,attrsubid);
    });
    $('body').on('click','.priority_tab', function () {
        var attrbookid = $(this).attr('attr-bookid');
        var attrsubid = $(this).attr('attr-subid');
        var attrpriorityname = $(this).attr('attr-priority-name');
        
        var quesno = $(this).find('.priority_no').html();
        get_question_html(attrbookid,attrsubid,attrpriorityname,quesno);
    });
    $('body').on('click','.addmorequesbybook', function () {
        var attrbookid = $(this).attr('attr-bookid');
        var attrsubid = $(this).attr('attr-subid');
        var attrpriorityname = $(this).attr('attr-priority-name');
        var icnquesno = $(this).attr('attr-quesno');
        var totalques = $('.priority-tab-content[priority-tab-content-attr="'+attrsubid+'_'+attrpriorityname+'_'+attrbookid+'"]').find('.totalques').val();
        get_more_ques_by_allbooks(attrbookid,attrsubid,attrpriorityname,icnquesno,totalques);
    });
    $('body').on('click','.addmorequesbysub', function () {
        var attrbookid = $(this).attr('attr-bookid');
        var attrsubid = $(this).attr('attr-subid');
        var attrpriorityname = $(this).attr('attr-priority-name');
        var icnquesno = $(this).attr('attr-quesno');
        var totalques = $('.priority-tab-content[priority-tab-content-attr="'+attrsubid+'_'+attrpriorityname+'_'+attrbookid+'"]').find('.totalques').val();
        get_more_ques_by_allsub(attrbookid,attrsubid,attrpriorityname,icnquesno,totalques);
    });
    $('body').on('click','.quesnotfndbtn', function () {
        var attrbookid = $(this).attr('attr-bookid');
        var attrsubid = $(this).attr('attr-subid');
        var attrpriorityname = $(this).attr('attr-priority-name');
        var icnquesno = $(this).attr('attr-quesno');
        var totalques = $('.priority-tab-content[priority-tab-content-attr="'+attrsubid+'_'+attrpriorityname+'_'+attrbookid+'"]').find('.totalques').val();
         $('.priority_tab[priority-tab-attr="'+attrsubid+'_'+attrpriorityname+'_'+attrbookid+'"]').find('.priority_no').html(totalques);
         var level = '.'+attrpriorityname+'_level';
         $('.book-tab-content[book-tab-content-attr="'+attrsubid+'_book_'+attrbookid+'"]').find(level).val(totalques);
         $('.priority-tab-content[priority-tab-content-attr="'+attrsubid+'_'+attrpriorityname+'_'+attrbookid+'"]').find('.alert_box').html('');
    });
    $('body').on('click','.quesaddintpbtn', function () {
        var quesid = $(this).attr('attr-quesid');
        var attraddedcheck = $(this).attr('attr-added');
        add_ques_in_tp(quesid,attraddedcheck);
    });
    $('body').on('click','.previewtestpaper', function () {
        var quesid = $('.alladdedquesintp').val();
        var tpname = $('.ftestpname').val();
        var fpmark = $('.fpmark').val();
        var fnmark = $('.fnmark').val();
        var fstartdate = $('.fstartdate').val();
        var fenddate = $('.fenddate').val();
        var fcreateddate = $('.fcreateddate').val();
        var fsubject = $('.subject').val();
        var tsid = $('.tsid').val();
        var fdesc = $('.fdesc').html();
       
        var check = 1;
        $(".alert_box").each(function() {
		    if($.trim($(this).html())!==''){
		        check = '';
		    }
        });
        if(quesid!='' && tpname!='' && fpmark!='' && fnmark!='' && fstartdate!='' && fenddate!='' && fcreateddate!='' && check!='' && fsubject!='' && tsid!=''){
            get_tppreview_modalhtml(quesid,tpname,fpmark,fnmark,fstartdate,fenddate,fcreateddate,fsubject,tsid,fdesc);
        }else{
            if(quesid=='' || check==''){
                var msg = "Please Add More Question!";
            }
            if(tpname=='' || fpmark=='' || fnmark=='' || fstartdate=='' || fenddate=='' || fcreateddate=='' || fsubject==''){
                var msg = "Please Enter All required Field!";
            }
            return $.gritter.add({
                title: "Error!",
                text: msg
            }), !1
        }
    });
    $('body').on('click','.submittestpaper', function () {
        var quesid = $('.alladdedquesintp').val();
        var tpname = $('.ftestpname').val();
        var fpmark = $('.fpmark').val();
        var fnmark = $('.fnmark').val();
        var fstartdate = $('.fstartdate').val();
        var fenddate = $('.fenddate').val();
        var fcreateddate = $('.fcreateddate').val();
        var fsubject = $('.subject').val();
        var tsid = $('.tsid').val();
        var fdesc = $('.fdesc').html();
    	$.ajax({
    		type: 'POST',
    		url: base_loc + 'adminpostajax/submit_test_paper',
    		data: 'quesid='+quesid + '&tpname='+tpname + '&fpmark='+fpmark + '&fnmark='+fnmark + '&fstartdate='+fstartdate + '&fenddate='+fenddate + 
    		      '&fcreateddate='+fcreateddate + '&fdesc='+fdesc+ '&fsubject='+fsubject+'&testseriesid='+tsid,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		beforeSend: function () {
    			$('.submittestpaper').attr("disabled", "disabled");
    			$('.submittestpaper').html("Please Wait...");
    		},
    		success: function (msg) {
    			if(msg.data=="ok"){
    				GritterNotification("Success","TEST PAPER CREATED SUCCESSFULLY","my-sticky-class");
    				$('.submittestpaper').attr("disabled",false);
    				$('.submittestpaper').html("Submit");
    				location.href = base_loc+'ratan/testpaper/'+tsid+'';
    			}else{
    				GritterNotification("Error",msg.message,"my-sticky-class");
    				$('.submittestpaper').attr("disabled",false);
    				$('.submittestpaper').html("Submit");	
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
        });
    })
});
function get_book_html(id){
    if($.trim($('.subject-tab-content[sub-tab-content-attr="sub_'+id+'"]').html())==''){
    	$.ajax({
    		type: 'GET',
    		url: base_loc + 'admingetajax/get_practice_book_html',
    		data: 'subid='+id,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.subject-tab-content[sub-tab-content-attr="sub_'+id+'"]').html(msg);
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
    	});
    }
}
function get_priority_html(bookid,subid){
    var high_level = $('.book-tab-content[book-tab-content-attr="'+subid+'_book_'+bookid+'"]').find('.high_level').val();
    var medium_level = $('.book-tab-content[book-tab-content-attr="'+subid+'_book_'+bookid+'"]').find('.medium_level').val();
    var low_level = $('.book-tab-content[book-tab-content-attr="'+subid+'_book_'+bookid+'"]').find('.low_level').val();
    if(high_level!='' && medium_level!='' && low_level!=''){
    	$.ajax({
    		type: 'GET',
    		url: base_loc + 'admingetajax/get_priority_html',
    		data: 'subid='+subid+'&bookid='+bookid+'&high_level='+high_level+'&medium_level='+medium_level+'&low_level='+low_level,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.book-tab-content[book-tab-content-attr="'+subid+'_book_'+bookid+'"]').find('.'+subid+'_priority_div_'+bookid+'').html(msg);
    			$('.submittestpaperdiv').removeClass('hide');
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
    	});
    }else{
        return $.gritter.add({
            title: "Error!",
            text: "Please Set Priority First!"
        }), !1
    }
}
function get_question_html(bookid,subid,priorityname,quesno){
    if($.trim($('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').html())==''){
    	$.ajax({
    		type: 'GET',
    		url: base_loc + 'admingetajax/get_question_html',
    		data: 'subid='+subid+'&bookid='+bookid+'&priorityname='+priorityname+'&tsid='+$('.tsid').val(),
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').html(msg);
    			var alladdedquesintp = $('.alladdedquesintp').val();
    		    var alladdedquesintparr = alladdedquesintp.split(',');
    			$(".quesaddintpbtn").each(function() {
    			    if(alladdedquesintparr.includes($(this).attr('attr-quesid'))){
    			        $(this).attr('attr-added','1');
    			        $(this).html('Remove');
    			    }else{
    			        $(this).attr('attr-added','0');
    			        $(this).html('Add');
    			    }
                });
    			var totalques = $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').find('.totalques').val();
    			if(parseInt(totalques)<parseInt(quesno)){
		            var alerthtml = '<div class="alert alert-warning fade show m-t-8"><span class="close" data-dismiss="alert">×</span><strong>Alert!</strong>No More Questions are Available! Want to get it from another Book ?<span class="label label-success curser addmorequesbybook" style="margin-left:15%;" attr-subid='+subid+' attr-bookid='+bookid+' attr-priority-name='+priorityname+' attr-quesno='+quesno+' attr-totalques='+totalques+'>Yes</span><span class="quesnotfndbtn label label-danger curser" data-dismiss="alert" style="margin-left:1%;" attr-subid='+subid+' attr-bookid='+bookid+' attr-priority-name='+priorityname+' attr-quesno='+quesno+' attr-totalques='+totalques+'>No</span></div>';
                    $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').find('.alert_box').html(alerthtml);
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
    	});
    }else{
        var alladdedquesintp = $('.alladdedquesintp').val();
    	var alladdedquesintparr = alladdedquesintp.split(',');
        $(".quesaddintpbtn").each(function() {
		    if(alladdedquesintparr.includes($(this).attr('attr-quesid'))){
		        $(this).attr('attr-added','1');
		        $(this).html('Remove');
		    }else{
		        $(this).attr('attr-added','0');
		        $(this).html('Add');
		    }
        });
    }
}
function get_more_ques_by_allbooks(bookid,subid,attrpriorityname,icnquesno,totalques){
    $totalques = totalques;
    if(attrpriorityname=='high'){
        $leveltype = 3;
    }else if(attrpriorityname=='medium'){
        $leveltype = 2;
    }else{
        $leveltype = 1;
    }
    $.ajax({
		type: 'GET',
		url: base_loc + 'admingetajax/get_more_ques_by_allbooks',
		data: 'subid='+subid+'&leveltype='+$leveltype+'&bookid='+bookid+'&tsid='+$('.tsid').val(),
		async:false,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
		    var addedquesval = $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.alladdedques').val();
		    var addedquesvalsplit = addedquesval.split(',');
		    var srno =parseInt($totalques)+1;
		    var tr = '';
		    var allnewquesidarr = new Array();
		    $.each(msg.data, function(key,data) {
		        if(addedquesvalsplit.includes(data.QUES_ID)){
		           
		        }else{
    		        allnewquesidarr.push(data.QUES_ID);
    		        if(data.QUES_TYPE==2){
    		            var ques = JSON.parse(data.QUES).QUESTION;
    		        }else{
    		            var ques = data.QUES;
    		        }
    		        var alladdedquesintp = $('.alladdedquesintp').val();
    		        var alladdedquesintparr = alladdedquesintp.split(',');
    		        if(alladdedquesintparr.includes(data.QUES_ID)){
    		            var attradded = 1;
    		            var txt = 'Remove';
    		        }else{
    		            var attradded = 0;
    		            var txt = 'Add';
    		        }
    		         tr += '<tr class=""><td width="8%" style="font-weight:700;">'+srno+'</td><td style="font-size: 13px;font-weight: 600;" width="80%">'+ques+'</td><td class="with-btn" nowrap=""><a href="javascript:void(0);" attr-quesid="'+data.QUES_ID+'" attr-added="'+attradded+'" class="btn btn-sm btn-primary width-60 m-r-2 p-4 quesaddintpbtn">'+txt+'</a></td></tr>';
    		        srno++;
		        }
		    });
		    
		    if(addedquesval!=''){
		        var allquesaddedarr = addedquesval.split(",");;
		        var mergedarr = allnewquesidarr.concat(allquesaddedarr);
		    }else{
		        var mergedarr = allnewquesidarr;
		    }
		    var allquesidjoin = mergedarr.join();
		     $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.alladdedques').val(allquesidjoin);
		     $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.totalques').val(parseInt(srno)-1);
		     if(parseInt($totalques)>0){
		           $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.questabletbody').append(tr);
		     }else{
		         $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.questabletbody').html(tr);
		     }
		     if(srno-1>=icnquesno){
		         $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.alert_box').html('');
		     }else{
		         var totalques = $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.totalques').val();
		         var alertboxthtml = '<div class="alert alert-danger fade show m-t-8"><span class="close" data-dismiss="alert">×</span><strong>Alert!</strong>No More Questions are Available in this subject! Want to get it from another Subject ?<span class="label label-success curser addmorequesbysub" style="margin-left:15%;" attr-subid='+subid+' attr-bookid='+bookid+' attr-priority-name='+attrpriorityname+' attr-quesno='+icnquesno+' attr-totalques='+totalques+'>Yes</span><span class="quesnotfndbtn label label-danger curser" data-dismiss="alert" style="margin-left:1%;" attr-subid='+subid+' attr-bookid='+bookid+' attr-priority-name='+attrpriorityname+' attr-quesno='+icnquesno+' attr-totalques='+totalques+'>No</span></div>';
                 $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.alert_box').html(alertboxthtml);
		         $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.alert_box').attr('allbookquesadded','1');
		         if($('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.questabletbody').html()==''){
		             var tr = '<tr><td colspan="3" style="text-align:center;">Data Not Available</td></tr>';
		             $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.questabletbody').html(tr);
		         }
		     }
		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
	});
}
function get_more_ques_by_allsub(bookid,subid,attrpriorityname,icnquesno,totalques){
    $totalques = totalques;
    if(attrpriorityname=='high'){
        $leveltype = 3;
    }else if(attrpriorityname=='medium'){
        $leveltype = 2;
    }else{
        $leveltype = 1;
    }
    $.ajax({
		type: 'GET',
		url: base_loc + 'admingetajax/get_more_ques_by_allsub',
		data: 'subid='+subid+'&leveltype='+$leveltype+'&bookid='+bookid+'&tsid='+$('.tsid').val(),
		async:false,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
		    var addedquesval = $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.alladdedques').val();
		    var addedquesvalsplit = addedquesval.split(',');
		    var srno =parseInt($totalques)+1;
		    var tr = '';
		    var allnewquesidarr = new Array();
		    $.each(msg.data, function(key,data) {
		        if(addedquesvalsplit.includes(data.QUES_ID)){
		           
		        }else{
		            
    		        allnewquesidarr.push(data.QUES_ID);
    		        if(data.QUES_TYPE==2){
    		            var ques = JSON.parse(data.QUES).QUESTION;
    		        }else{
    		            var ques = data.QUES;
    		        }
    		        var alladdedquesintp = $('.alladdedquesintp').val();
    		        var alladdedquesintparr = alladdedquesintp.split(',');
    		        if(alladdedquesintparr.includes(data.QUES_ID)){
    		            var attradded = 1;
    		            var txt = 'Remove';
    		        }else{
    		            var attradded = 0;
    		            var txt = 'Add';
    		        }
    		         tr += '<tr class=""><td width="8%" style="font-weight:700;">'+srno+'</td><td style="font-size: 13px;font-weight: 600;" width="80%">'+ques+'</td><td class="with-btn" nowrap=""><a href="javascript:void(0);" attr-quesid="'+data.QUES_ID+'" attr-added="'+attradded+'" class="btn btn-sm btn-primary width-60 m-r-2 p-4 quesaddintpbtn">'+txt+'</a></td></tr>';
    		        srno++;
		        }
		    });
		    
		    if(addedquesval!=''){
		        var allquesaddedarr = addedquesval.split(",");;
		        var mergedarr = allnewquesidarr.concat(allquesaddedarr);
		    }else{
		        var mergedarr = allnewquesidarr;
		    }
		    var allquesidjoin = mergedarr.join();
		    
		     $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.alladdedques').val(allquesidjoin);
		     $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.totalques').val(srno-1);
		     if(parseInt($totalques)>0){
		           $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.questabletbody').append(tr);
		     }else{
		         $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.questabletbody').html(tr);
		     }
		     if(srno-1>=icnquesno){
		         $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.alert_box').html('');
		     }else{
		         var totalques = $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.totalques').val();
		         var alertboxthtml = '<div class="alert alert-danger fade show m-t-8"><span class="close" data-dismiss="alert">×</span><strong>Alert!</strong>No More Questions are Available!<span class="label label-success curser quesnotfndbtn" style="margin-left:15%;" attr-subid='+subid+' attr-bookid='+bookid+' attr-priority-name='+attrpriorityname+' attr-quesno='+icnquesno+' attr-totalques='+totalques+'>Yes</span><span class="quesnotfndbtn label label-danger curser" data-dismiss="alert" style="margin-left:1%;" attr-subid='+subid+' attr-bookid='+bookid+' attr-priority-name='+attrpriorityname+' attr-quesno='+icnquesno+' attr-totalques='+totalques+'>No</span></div>';
                 $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.alert_box').html(alertboxthtml);
		         $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.alert_box').attr('allsubquesadded','1');
		         if($('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.questabletbody').html()==''){
		             var tr = '<tr><td colspan="3" style="text-align:center;">Data Not Available</td></tr>';
		             $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+attrpriorityname+'_'+bookid+'"]').find('.questabletbody').html(tr);
		         }
		     }
		     
		     
		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
		
	});
}
function add_ques_in_tp(quesid,attraddedcheck){
    if(attraddedcheck=='0'){
        var alladdedquesintp = $('.alladdedquesintp').val();
        if(alladdedquesintp!=''){
            var alladdedquesintparr = alladdedquesintp.split(',');
            var quesidarr = quesid.split();
            var allquesarr = alladdedquesintparr.concat(quesidarr);
            var joinquesid = allquesarr.join();
            $('.alladdedquesintp').val(joinquesid);
        }else{
            $('.alladdedquesintp').val(quesid);
        }
        update_ques_addedbtn(quesid,0);
    }else{
        var alladdedquesintp = $('.alladdedquesintp').val();
        var alladdedquesintparr = alladdedquesintp.split(',');
        alladdedquesintparr = jQuery.grep(alladdedquesintparr, function(value) {
          return value != quesid;
        });
        var joinquesid = alladdedquesintparr.join();
        $('.alladdedquesintp').val(joinquesid);
        $('.alladdedquesintp').val(joinquesid);
        update_ques_addedbtn(quesid,1);
    }
    
    
}
function update_ques_addedbtn(quesid,attradded){
    $(".quesaddintpbtn[attr-quesid='"+quesid+"']").each(function() {  
        if(attradded==0){
            $(this).attr('attr-added','1');
            $(this).html('Remove');
        }else{
            $(this).attr('attr-added','0');
            $(this).html('Add');
        }
    });
}
function get_tppreview_modalhtml(quesid,tpname,fpmark,fnmark,fstartdate,fenddate,fcreateddate,fsubject,tsid,fdesc){
	    $.ajax({
			type: 'GET',
			url: base_loc + 'admingetajax/get_prac_testppreview_modalhtml',
			data:'quesid=' + quesid + '&tpname=' + tpname + '&fpmark=' + fpmark + '&fnmark=' + fnmark + '&fstartdate=' + fstartdate +
			        '&fenddate=' + fenddate +'&fcreateddate=' + fcreateddate +'&fsubject=' + fsubject + '&tsid=' + tsid +'&fdesc='+fdesc,
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
			success: function (msg) {
                $('#testpaperpreviewmodal').modal('show');
				$('.previewtestpaperbody').html(msg);
			},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
		});
	}
</script>