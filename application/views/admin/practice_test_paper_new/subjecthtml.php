
<!-- begin #content -->
<div id="content" class="content">
  
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Test Paper</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Create</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Create Practice Test Paper</h1>
	<!-- end page-header -->
	<div class="row no_mrgn addbx_top_rowhead pd_15">
		<div class="col-md-8">
		    <div class="row">
			    <h5 class="">Practice Test Name &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; </h5><span class=""><?php echo $tsdt[0]->TS_NAME; ?></span>
			    <input type="hidden" class="alladdedquesintp">
			    <input type="hidden" class="tsid" value="<?php echo $tsdt[0]->TS_ID; ?>">
			</div>
		</div>
		<div class="col-md-4 text-right">
			<div class="submittestpaperdiv hide">
                <a href="javascript:void(0);" class="btn btn-warning previewtestpaper"><i class="fa fa-fw fa-hdd"></i> Preview</a>
            </div>
		</div>
	</div>
	<!-- begin row -->
<div class="row no_mrgn addbx_top_rowhead pd_15">
		<div class="col-md-12">
		    <div class="row">
			    <div class="col-md-4">
					<label class="col-form-label">Test Paper Name <span class="star">*</span></label>
					<input type="text" class="form-control ftestpname" name="ftestpname" placeholder="Enter Test Paper Name">
			    </div>
			    <div class="col-md-4">
					<label class="col-form-label"> Start Date <span class="star">*</span></label>
					<input type="text" class="form-control dtpicker fstartdate" name="fstartdate" value="">
				</div>
				<div class="col-md-4">
					<label class="col-form-label"> End Date <span class="star">*</span></label>
					<input type="text" class="form-control dtpicker fenddate" name="fenddate" value="">
				</div>
				<div class="col-md-4 hide">
					<label class="col-form-label"> Created Date <span class="star">*</span></label>
					<input type="text" class="form-control dtpicker fcreateddate" name="fcreateddate" value="<?php echo date('Y-m-d'); ?>">
				</div>
				<div class="col-md-4">
					<label class="col-form-label">Test Paper Type <span class="star">*</span></label>
    				<select class="form-control tptype" name="tptype" required="">
    					<option value="">--Test Paper Type--</option>
    					<option value="1">Free</option>
    					<option value="0">Not Free</option>
    				</select>
                </div>
				<div class="col-md-4">
					<label class="col-form-label">Test Paper Description </label>
                    <textarea rows="2" class="form-control fdesc" name="fdesc"></textarea>
				</div>
			</div>
	    </div>
	</div>
	<div class="row no_mrgn addbx_top_rowhead">
        <div class="col-lg-12 p-0">
            <style>
                .bckcss{
                    padding: 5px;
                    border-radius: 10px;
                    border: 1px solid #ccc6c6;
                }
                
            </style>
            <!-- begin panel -->
            <div class="panel panel-inverse panel-with-tabs">
            	<!-- begin panel-heading -->
            	<div class="panel-heading p-0">
            		<div class="panel-heading-btn p-10">
            			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            		</div>
            		<!-- begin nav-tabs -->
            		<div class="tab-overflow ">
            			<ul class="nav nav-tabs nav-tabs-inverse">
            				<li class="nav-item prev-button "><a href="javascript:;" data-click="prev-tab" class="nav-link text-success"><i class="fa fa-arrow-left"></i></a></li>
        					<?php $firstsubjecttab="first-subject-tab active"; foreach ($allsub as $allsubs) { ?>
        					    <li class="nav-item ">
            				        <a href="#nav-tab-<?php echo $allsubs->SUB_ID;?>" sub-tab-attr="<?php echo 'sub_'.$allsubs->SUB_ID; ?>" attr-subid="<?php echo $allsubs->SUB_ID; ?>" data-toggle="tab" class="<?php echo $firstsubjecttab;?> subject_tab nav-link"><?php echo $allsubs->SUB_NAME; ?></a>
            				    </li>
        					<?php $firstsubjecttab="";} ?>
        					<li class="nav-item next-button"><a href="javascript:;" data-click="next-tab" class="nav-link text-success"><i class="fa fa-arrow-right"></i></a></li>
            			</ul>
            		</div>
            		<!-- end nav-tabs -->
            	</div>
            	<!-- end panel-heading -->
            	<!-- begin tab-content -->
            	<div class="tab-content p-0">
        		    <!-- begin tab-pane -->
        		    <?php $firsttabdiv="active show"; foreach ($allsub as $allsubs) { ?>
					    <div class="subject-tab-content tab-pane fade <?php echo $firsttabdiv;?>" sub-tab-content-attr="<?php echo 'sub_'.$allsubs->SUB_ID; ?>" id="nav-tab-<?php echo $allsubs->SUB_ID;?>">
					            
    				    </div>
					<?php $firsttabdiv=""; } ?>
            	    <!-- end tab-pane -->
            	    <div class="col-lg-12 text-center submittestpaperdiv hide">
                    </div>
            	</div>
            	<!-- end tab-content -->
            </div>
        <!-- end panel -->
        </div>
        </div>
	</div>
</div>
<div class="modal fade" id="testpaperpreviewmodal" role="dialog">
	<div class="modal-dialog modal-xl">
		<div class="modal-content" style="background-color:unset">
			<div class="modal-header" style="background-color: #348fe2 ;">
				<h4 class="modal-title" style="color:white">Preview Test Paper</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body bg_grey">
				<div class='col-md-12'>
        			<!-- begin panel -->
        			<div class="panel bg_grey">
        				<!-- begin panel-body -->
        				<div class="panel-body previewtestpaperbody p-b-1" style="border: 1px solid rgba(0,0,0,.2);border-radius: 4px;background-color: white;">
                            
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
</div>
<script>
    $('document').ready(function(){
        if($('.first-subject-tab').length>0){
            var attrsubid = $('.first-subject-tab').attr('attr-subid');
            $('.subject_tab[attr-subid="'+attrsubid+'"]').trigger('click');
        }
        
    });
</script>

