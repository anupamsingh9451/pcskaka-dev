<link href="<?php echo base_url(); ?>assets/adminassets/css/test_paper_new/questionhtml.css" rel="stylesheet" />
<div class="alert_box m-t-15">
    
</div>

<table class="table table-striped table-bordered hide">
	<thead>
		<tr>
			<th width="7%">Sr. No.</th>
			<th width="70%" >Question</th>
			<th>Subject</th>
			<th>Tools</th>
		</tr>
	</thead>
	<tbody class="questabletbody">
	    <?php $allques=array(); $i=1;  if (!empty($quesdt)) {
    foreach ($quesdt as $quesdts) {
        $allques [] = $quesdts->QUES_ID; ?>
        	<tr class="">
    			<td width="7%" style="font-weight:700;"><?php echo $i.'.'; ?></td>
    			<td style="font-size: 13px;font-weight: 600;" width="70%"><?php if ($quesdts->QUES_TYPE==1) {
            echo $quesdts->QUES;
        } else {
            echo json_decode($quesdts->QUES)->QUESTION;
        } ?></td>
    			<td style="font-size: 13px;font-weight: 600;"><?php echo $quesdts->SUB_NAME; ?><br><span style="font-size: 11px;font-weight: 500; ">(<?php echo $quesdts->BOOK_NAME; ?>)</span></td>
    			<td class="with-btn" nowrap=""><a href="javascript:void(0);" attr-quesid="<?php echo $quesdts->QUES_ID; ?>" attr-added="0" class="btn btn-sm btn-primary width-60 m-r-2 p-4 quesaddintpbtn">Add</a></td>
    		</tr>
    	<?php $i++;
    }
} else { ?>
    	    <tr>
    	        <td colspan="3" style="text-align:center;">Data Not Available</td>
    	    </tr>
    	  <?php } ?>
    	  
	</tbody>
	<input type="hidden" class="totalques" value="<?php echo $i-1; ?>">
	<input type="hidden" class="alladdedques" value="<?php echo implode(',', $allques); ?>">
</table>
<div class="row">
    <div class="col-md-12 m-t-10 numberofaddedquesmsg">
    
    </div>
</div>