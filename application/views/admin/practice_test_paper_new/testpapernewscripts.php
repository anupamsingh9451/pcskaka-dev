<script>
$('document').ready(function(){
    $('body').on('click','.subject_tab', function () {
        var attrsubid = $(this).attr('attr-subid');
        get_book_html(attrsubid);
    });
    $('body').on('click','.book_tab', function () {
        var attrbookid = $(this).attr('attr-bookid');
        var attrsubid = $(this).attr('attr-subid');
       
    });
    $('body').on('click','.addprioritybtn', function () {
        var attrbookid = $(this).attr('attr-bookid');
        var attrsubid = $(this).attr('attr-subid');
        get_priority_html(attrbookid,attrsubid);
    });
    $('body').on('click','.priority_tab', function () {
        var attrbookid = $(this).attr('attr-bookid');
        var attrsubid = $(this).attr('attr-subid');
        var attrpriorityname = $(this).attr('attr-priority-name');
        
        var quesno = $(this).find('.priority_no').html();
        get_question_html(attrbookid,attrsubid,attrpriorityname,quesno);
    });
   
    $('body').on('click','.quesnotfndbtn', function () {
        var attrbookid = $(this).attr('attr-bookid');
        var attrsubid = $(this).attr('attr-subid');
        var attrpriorityname = $(this).attr('attr-priority-name');
        var icnquesno = $(this).attr('attr-quesno');
        var totalques = $('.priority-tab-content[priority-tab-content-attr="'+attrsubid+'_'+attrpriorityname+'_'+attrbookid+'"]').find('.totalques').val();
         $('.priority_tab[priority-tab-attr="'+attrsubid+'_'+attrpriorityname+'_'+attrbookid+'"]').find('.priority_no').html(totalques);
         var level = '.'+attrpriorityname+'_level';
         $('.book-tab-content[book-tab-content-attr="'+attrsubid+'_book_'+attrbookid+'"]').find(level).val(totalques);
         $('.priority-tab-content[priority-tab-content-attr="'+attrsubid+'_'+attrpriorityname+'_'+attrbookid+'"]').find('.alert_box').html('');
    });
    $('body').on('click','.quesaddintpbtn', function () {
        if($(this).attr('attr-pre')){
           $(this).closest("tr").remove();
        }

        var quesid = $(this).attr('attr-quesid');
        var attraddedcheck = $(this).attr('attr-added');
        add_ques_in_tp(quesid,attraddedcheck);
    });
    $('body').on('click','.previewtestpaper', function () {
        var quesid = $('.alladdedquesintp').val();
        var tpname = $('.ftestpname').val();
        // var fpmark = $('.fpmark').val();
        // var fnmark = $('.fnmark').val();
        var fstartdate = $('.fstartdate').val();
        var fenddate = $('.fenddate').val();
        var fcreateddate = $('.fcreateddate').val();
        var tsid = $('.tsid').val();
        var fdesc = $('.fdesc').val();
        var check = 1;
        $(".alert_box").each(function() {
		    if($.trim($(this).html())!==''){
		        check = '';
		    }
        });
        if(quesid!='' && tpname!='' && fstartdate!='' && fenddate!='' && fcreateddate!='' && check!='' && tsid!=''){
            get_tppreview_modalhtml(quesid,tpname,fstartdate,fenddate,fcreateddate,tsid,fdesc);
        }else{
            if(quesid=='' || check==''){
                var msg = "Please Add More Question!";
            }
            if(tpname=='' || fstartdate=='' || fenddate=='' || fcreateddate==''){
                var msg = "Please Enter All required Field!";
            }
            return $.gritter.add({
                title: "Error!",
                text: msg
            }), !1
        }
    });
    $('body').on('click','.submittestpaper', function () {
        var quesid = $('.alladdedquesintp').val();
        var tpname = $('.ftestpname').val();
        var fstartdate = $('.fstartdate').val();
        var fstartdate = $('.fstartdate').val();
        var fenddate = $('.fenddate').val();
        var fcreateddate = $('.fcreateddate').val();
        var tsid = $('.tsid').val();
        var fdesc = $('.fdesc').val();
        var tptype = $('.tptype').val();
        if(quesid!=''){
        	$.ajax({
        		type: 'POST',
        		url: base_loc + 'adminpostajax/submit_practice_test_paper',
        		data: 'quesid='+quesid + '&tpname='+tpname + '&fstartdate='+fstartdate + '&fenddate='+fenddate + 
        		      '&fcreateddate='+fcreateddate + '&fdesc='+fdesc+'&testseriesid='+tsid+'&tptype='+tptype,
        		headers: {
        			'Client-Service': clientserv,
        			'Auth-Key': apikey,
        			'User-ID': loginid,
        			'Authorization': token,
        			'type': type
        		},
        		beforeSend: function () {
        			$('.submittestpaper').attr("disabled", "disabled");
        			$('.submittestpaper').html("Please Wait...");
        		},
        		success: function (msg) {
        			if(msg.data=="ok"){
        				GritterNotification("Success","TEST PAPER CREATED SUCCESSFULLY","my-sticky-class");
        				$('.submittestpaper').attr("disabled",false);
        				$('.submittestpaper').html("Submit");
        				location.href = base_loc+'ratan/practicetestpaper/'+tsid+'';
        			}else{
        				GritterNotification("Error",msg.message,"my-sticky-class");
        				$('.submittestpaper').attr("disabled",false);
        				$('.submittestpaper').html("Submit");	
        			}
        		},
        		error: function (msg) {
        			if (msg.responseJSON['status'] == 303) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 401) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 400) {
        				location.href = base_loc;
        			}
        		}
            });
        }else{
            GritterNotification("Error","Please Add More Question","my-sticky-class");
        }
    })
    
    $('body').on('change','.all_sub', function () {
        get_books_by_subid($(this).val(),'');
    });
    $('body').on('click','.filtertppreviewbtn', function () {
        var sub_html = $('option:selected','.all_sub').html();
        var book_html = $('option:selected','.all_book').html();
        var level_html = $('option:selected','.level').html();
        
        var sub_id = $('option:selected','.all_sub').val();
        var book_id= $('option:selected','.all_book').val();
        var level_id = $('option:selected','.level').val();
        console.log(sub_id);
        console.log(book_id);
        console.log(level_id);
        
        
        if(sub_html=="All"){
            if(book_html=="All"){
                if(level_html=="All"){
                    $(".previewtabletr").each(function() {
                        $(this).removeClass('hide');
                    });
                }else{
                    $(".previewtabletr").each(function() {
                        if($(this).attr('attr-level')==level_id){
                            $(this).removeClass('hide');
                        }else{
                            $(this).addClass('hide');
                        }
                    });
                }
            }else{
                if(level_html=="All"){
                    $(".previewtabletr").each(function() {
                        if($(this).attr('attr-bookid')==book_id){
                            $(this).removeClass('hide');
                        }else{
                            $(this).addClass('hide');
                        }
                    });
                }else{
                    $(".previewtabletr").each(function() {
                        if($(this).attr('attr-bookid')==book_id && $(this).attr('attr-level')==level_id){
                            $(this).removeClass('hide');
                        }else{
                            $(this).addClass('hide');
                        }
                    });
                }
            }
        }else{
            if(book_html=="All"){
                if(level_html=="All"){
                    $(".previewtabletr").each(function() {
                        if($(this).attr('attr-subid')==sub_id){
                            $(this).removeClass('hide');
                        }else{
                            $(this).addClass('hide');
                        }
                    });
                }else{
                    $(".previewtabletr").each(function() {
                        if($(this).attr('attr-subid')==sub_id && $(this).attr('attr-level')==level_id ){
                            $(this).removeClass('hide');
                        }else{
                            $(this).addClass('hide');
                        }
                    });
                }
            }else{
                if(level_html=="All"){
                    $(".previewtabletr").each(function() {
                        if($(this).attr('attr-subid')==sub_id && $(this).attr('attr-bookid')==book_id){
                            $(this).removeClass('hide');
                        }else{
                            $(this).addClass('hide');
                        }
                    });
                }else{
                    $(".previewtabletr").each(function() {
                        if($(this).attr('attr-subid')==sub_id && $(this).attr('attr-bookid')==book_id && $(this).attr('attr-level')==level_id){
                            $(this).removeClass('hide');
                        }else{
                            $(this).addClass('hide');
                        }
                    });
                }
            }
        }
    });
});
function get_book_html(id){
    if($.trim($('.subject-tab-content[sub-tab-content-attr="sub_'+id+'"]').html())==''){
    	$.ajax({
    		type: 'GET',
    		url: base_loc + 'admingetajax/get_practice_book_html',
    		data: 'subid='+id+'&tsid='+$('.tsid').val(),
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.subject-tab-content[sub-tab-content-attr="sub_'+id+'"]').html(msg);
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
    	});
    }
}
function get_priority_html(bookid,subid){
    var high_level = parseInt($('.book-tab-content[book-tab-content-attr="'+subid+'_book_'+bookid+'"]').find('.high_level').val());
    var medium_level = parseInt($('.book-tab-content[book-tab-content-attr="'+subid+'_book_'+bookid+'"]').find('.medium_level').val());
    var low_level = parseInt($('.book-tab-content[book-tab-content-attr="'+subid+'_book_'+bookid+'"]').find('.low_level').val());
    
    var totalquesbyhighpriority = parseInt($('.totalquesbyhighpriority[attr-bookid="'+bookid+'"]').html());
    var totalquesbymediumpriority = parseInt($('.totalquesbymediumpriority[attr-bookid="'+bookid+'"]').html());
    var totalquesbylowpriority = parseInt($('.totalquesbylowpriority[attr-bookid="'+bookid+'"]').html());
    
    if(high_level!=='' && medium_level!=='' && low_level!==''){
        if(high_level<=totalquesbyhighpriority && medium_level<=totalquesbymediumpriority && low_level<=totalquesbylowpriority){
        	$.ajax({
        		type: 'GET',
        		url: base_loc + 'admingetajax/get_priority_html',
        		data: 'subid='+subid+'&bookid='+bookid+'&high_level='+high_level+'&medium_level='+medium_level+'&low_level='+low_level,
        		async:false,
        		headers: {
        			'Client-Service': clientserv,
        			'Auth-Key': apikey,
        			'User-ID': loginid,
        			'Authorization': token,
        			'type': type
        		},
        		success: function (msg) {
        			$('.book-tab-content[book-tab-content-attr="'+subid+'_book_'+bookid+'"]').find('.'+subid+'_priority_div_'+bookid+'').html(msg);
        			$('.submittestpaperdiv').removeClass('hide');
        			
        			
        		},
        		error: function (msg) {
        			if (msg.responseJSON['status'] == 303) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 401) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 400) {
        				location.href = base_loc;
        			}
        		}
        	});
        }else{
            return $.gritter.add({
                title: "Error!",
                text: "Please Set valid Priority!"
            }), !1
        }
    }else{
        return $.gritter.add({
            title: "Error!",
            text: "Please Set Priority First!"
        }), !1
    }
}
function get_question_html(bookid,subid,priorityname,quesno){
    if($.trim($('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').html())==''){
    	$.ajax({
    		type: 'GET',
    		url: base_loc + 'admingetajax/get_practice_question_html_new',
    		data: 'subid='+subid+'&bookid='+bookid+'&priorityname='+priorityname+'&tsid='+$('.tsid').val()+'&quesno='+quesno,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').html(msg);
    			var allques = $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').find('.alladdedques').val();
    		    if(allques!=''){
    		        var allquesarr = allques.split(',');
    		    }else{
    		        allquesarr = '';
    		    }
    			var alladdedquesintp = $('.alladdedquesintp').val();
    			
    			if(alladdedquesintp!=''){
    			    var alladdedquesintparr = alladdedquesintp.split(',');
    			    var newid = union_arrays(allquesarr,alladdedquesintparr);
    			}else{
    			    var newid = allquesarr;
    			}
                if(newid!=''){
                    var newids = newid.join()
    		        $('.alladdedquesintp').val(newids);
                }
    		    
    		    var alladdedquesintp = $('.alladdedquesintp').val();
    		    var alladdedquesintparr = alladdedquesintp.split(',');
    		  //  $('.alladdedquesintp').val(alladdedquesintparr);
    		    
    			$(".quesaddintpbtn").each(function() {
    			    if(alladdedquesintparr.includes($(this).attr('attr-quesid'))){
    			        $(this).attr('attr-added','1');
    			        $(this).html('Remove');
    			    }else{
    			        $(this).attr('attr-added','0');
    			        $(this).html('Add');
    			    }
                });
    			var totalques = $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').find('.totalques').val();
    			if(parseInt(totalques)<parseInt(quesno)){
                    
                    var alertsuccesshtml = '<div class="alert alert-danger fade show m-t-8"><span class="close" data-dismiss="alert">×</span><strong style="font-size:14px;">Success!</strong>&nbsp;&nbsp;<span style="font-size:14px;">Only '+parseInt(totalques)+' Questions are added successfuly</span></div>';
                    $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').find('.numberofaddedquesmsg').html(alertsuccesshtml);
    			    $('.priority_tab[priority-tab-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').find('.priority_no').html(totalques);
                    var level = '.'+priorityname+'_level';
                    $('.book-tab-content[book-tab-content-attr="'+subid+'_book_'+bookid+'"]').find(level).val(totalques);
    			}else{
    			    var alertsuccesshtml = '<div class="alert alert-success fade show m-t-8"><span class="close" data-dismiss="alert">×</span><strong style="font-size:14px;">Success!</strong>&nbsp;&nbsp;<span style="font-size:14px;"> '+parseInt(totalques)+' Questions added successfuly</span></div>';
                    $('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').find('.numberofaddedquesmsg').html(alertsuccesshtml);
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
    	});
    }else{
        var alladdedquesintp = $('.alladdedquesintp').val();
    	var alladdedquesintparr = alladdedquesintp.split(',');
        $(".quesaddintpbtn").each(function() {
		    if(alladdedquesintparr.includes($(this).attr('attr-quesid'))){
		        $(this).attr('attr-added','1');
		        $(this).html('Remove');
		    }else{
		        $(this).attr('attr-added','0');
		        $(this).html('Add');
		    }
        });
        
        if($('.priority-tab-content[priority-tab-content-attr="'+subid+'_'+priorityname+'_'+bookid+'"]').find('.alert_box').attr('allbookquesadded')>0){
            
        }else{
   
        }
    }
}


function add_ques_in_tp(quesid,attraddedcheck){
    if(attraddedcheck=='0'){
        var alladdedquesintp = $('.alladdedquesintp').val();
        if(alladdedquesintp!=''){
            var alladdedquesintparr = alladdedquesintp.split(',');
            var quesidarr = quesid.split();
            var allquesarr = alladdedquesintparr.concat(quesidarr);
            var joinquesid = allquesarr.join();
            $('.alladdedquesintp').val(joinquesid);
        }else{
            $('.alladdedquesintp').val(quesid);
        }
        update_ques_addedbtn(quesid,0);
    }else{
        var alladdedquesintp = $('.alladdedquesintp').val();
        var alladdedquesintparr = alladdedquesintp.split(',');
        alladdedquesintparr = jQuery.grep(alladdedquesintparr, function(value) {
          return value != quesid;
        });
        var joinquesid = alladdedquesintparr.join();
        $('.alladdedquesintp').val(joinquesid);
        $('.alladdedquesintp').val(joinquesid);
        update_ques_addedbtn(quesid,1);
    }
    
    
}
function update_ques_addedbtn(quesid,attradded){
    $(".quesaddintpbtn[attr-quesid='"+quesid+"']").each(function() {  
        if(attradded==0){
            $(this).attr('attr-added','1');
            $(this).html('Remove');
        }else{
            $(this).attr('attr-added','0');
            $(this).html('Add');
        }
    });
}
    function get_tppreview_modalhtml(quesid,tpname,fstartdate,fenddate,fcreateddate,tsid,fdesc){
	    $.ajax({
			type: 'GET',
			url: base_loc + 'admingetajax/get_prac_testppreview_modalhtml',
			data:'quesid=' + quesid + '&tpname=' + tpname + '&fstartdate=' + fstartdate +
			        '&fenddate=' + fenddate +'&fcreateddate=' + fcreateddate + '&tsid=' + tsid +'&fdesc='+fdesc,
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
			success: function (msg) {
                $('#testpaperpreviewmodal').modal('show');
				$('.previewtestpaperbody').html(msg);
				get_all_sub();
			},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
		});
	}
	
	function union_arrays (x, y) {
      var obj = {};
      for (var i = x.length-1; i >= 0; -- i)
         obj[x[i]] = x[i];
      for (var i = y.length-1; i >= 0; -- i)
         obj[y[i]] = y[i];
      var res = []
      for (var k in obj) {
        if (obj.hasOwnProperty(k))  // <-- optional
          res.push(obj[k]);
      }
      return res;
    }
    function get_all_sub(){
     $.ajax({
    		type: 'GET',
    		url: base_loc + 'admingetajax/get_all_sub',
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    		    $('.all_sub option').remove();
    			var str='';
    			if(msg.status==200){
    			    if(msg.data.length>0){
    			        var subidarr = new Array();
    					$.each(msg.data, function(index, element) {
    						str += '<option value="'+element.SUB_ID+'">'+element.SUB_NAME+'</option>';
    						subidarr.push(element.SUB_ID);
    					});
    					var subids = subidarr.join();
    		            var str1 = "<option selected value='" + subids + "'>"+'All'+"</option>";
        				$('.all_sub').append(str1);
        				$('.all_sub').append(str);
        				
        				var change = '';
        				get_books_by_subid(subids,change);
    			    }
    			}else{
    				location.href = base_loc;
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
 }
  function get_books_by_subid(subide,change){
      $.ajax({
    		type: 'GET',
    		data:'subid='+ subide,
    		url: base_loc + 'admingetajax/get_book_by_subid',
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    		    $('.all_book option').remove();
    			var str='';
    			if(msg.status==200){
    				if(msg.data.length>0){
    				    var bookid = new Array();
    					$.each(msg.data, function(index, element) {
    						str+='<option value="'+element.BOOK_ID+'">'+element.BOOK_NAME+'</option>';
    						bookid.push(element.BOOK_ID);
    					});
    					var bookids = bookid.join();
			            var str1 = "<option selected value='" + bookids+ "'>"+'All'+"</option>";
    				}
    				$('.all_book').append(str1);
    				$('.all_book').append(str);
    				var userid = $('option:selected','.all_user').val();

    			}else{
    				location.href = base_loc;
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
  }
</script>