
<div class="modal fade" id="stopbox">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Stop Box ( VC NO - <span class='vcno'></span> ) <span class='star'> (* fields are mandatory)</span></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="del_box_cust" data-parsley-validate="true">
			    <input type='hidden' name="stbvcpairid" value="">
			    <input type='hidden' name="hrmid" value="">
			    <input type='hidden' name="firmid" value="">
				<input type="hidden" name="browsernm" value='<?php echo  $_SERVER['HTTP_USER_AGENT']; ?>' >
				<div class="modal-body">
				    <div class="row">
				        <div class="col-md-12">
        				    <div class="m-b-5 text-inverse f-w-600"><label> <input type="checkbox" name="daywisecalc" value="1" checked /> Check for to day wise payment calculation</label></div>
        				</div>
    				</div>
    				<div class="row">
						<div class="col-md-12">
							<label class="col-form-label">Reason <span class="star">*</span></label>
							<textarea class='form-control' name="delreason" placeholder="Enter some reason here for stopping this box" data-parsley-required="true"></textarea>
						</div>
				    </div>
					<div class="row">
					    <div class="col-md-12">
    						<label class="col-form-label">Box Stopped Date <span class="star">*</span></label>
    						<input type="text" name="box_deact_dt" class="form-control m-b-5 dtpicker"  placeholder="Enter box deactivation date" data-parsley-required="true">
    					</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit" class="btn btn-danger del_box_cust"><i class="fas fa-lg fa-fw m-r-10 fa-trash"></i> Stop</button>
				</div>
			</form>
		</div>
	</div>
</div>	

<div class="modal fade" id="startbox">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Start Box ( VC NO - <span class='vcno'></span> ) <span class='star'> (* fields are mandatory)</span></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="act_box_cust" data-parsley-validate="true">
			    <input type='hidden' name="stbvcpairid" value="">
			    <input type='hidden' name="hrmid" value="">
			    <input type='hidden' name="firmid" value="">
				<input type="hidden" name="browsernm" value='<?php echo  $_SERVER['HTTP_USER_AGENT']; ?>' >
				<div class="modal-body">
				    <div class="row">
						<div class="col-md-12">
							<label class="col-form-label">Reason <span class="star">*</span></label>
							<textarea class='form-control' name="actreason" placeholder="Enter some reason here for starting this box" data-parsley-required="true"></textarea>
						</div>
				    </div>
					<div class="row">
					    <div class="col-md-12">
    						<label class="col-form-label">Box Activation Date <span class="star">*</span></label>
    						<input type="text" name="box_act_dt" class="form-control m-b-5 dtpicker"  placeholder="Enter box activation date" data-parsley-required="true">
    					</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit" class="btn btn-danger act_box_cust"><i class="fas fa-lg fa-fw m-r-10 fa-clock"></i> Start</button>
				</div>
			</form>
		</div>
	</div>
</div>	

		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/js-cookie/js.cookie.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/theme/default.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/apps.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/parsley/dist/parsley.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-smart-wizard/src/js/jquery.smartWizard.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/demo/form-wizards-validation.demo.min.js"></script>
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>assets/themeassets/js/demo/email-inbox.demo.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/d3/d3.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/nvd3/build/nv.d3.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/gritter/js/jquery.gritter.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/demo/dashboard-v2.min.js"></script>
	
	<!--DATATABLE JS-->
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/AutoFill/js/dataTables.autoFill.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/DataTables/extensions/Select/js/dataTables.select.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/demo/table-manage-combine.demo.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
	<!-- DATE PICKER JS-->
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/masked-input/masked-input.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/password-indicator/js/password-indicator.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-combobox/js/bootstrap-combobox.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-tag-it/js/tag-it.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-daterangepicker/moment.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-show-password/bootstrap-show-password.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/clipboard/clipboard.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/demo/form-plugins.demo.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/pagination.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/chart-js/Chart.min.js"></script>
	<!--<script src="<?php echo base_url(); ?>assets/themeassets/js/demo/chart-js.demo.min.js"></script>-->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/superbox/js/jquery.superbox.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/lity/dist/lity.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/demo/profile.demo.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/highlight/highlight.common.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/js/demo/render.highlight.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/themeassets/plugins/summernote/summernote.min.js"></script>
<script src="<?php echo base_url(); ?>assets/themeassets/js/demo/form-summernote.demo.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script src="<?php echo base_url(); ?>assets/themeassets/plugins/jstree/dist/jstree.min.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/adminassets/js/adminget.js"></script>
	<script src="<?php echo base_url(); ?>assets/adminassets/js/adminpost.js"></script>
	<script src="<?php echo base_url(); ?>assets/adminassets/js/admincommon.js"></script>
	<script src="<?php echo base_url(); ?>assets/adminassets/js/adminvalidation.js"></script>
	<script>
		$(document).ready(function() {
			Highlight.init();
		});
	</script>
</body>