<link href="<?php echo base_url(); ?>assets/adminassets/css/test_paper/questionhtml.css" rel="stylesheet" />
<div class="alert_box">
    
</div>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th width="8%">Sr. No.</th>
			<th width="80%" >Question</th>
			<th>Tools</th>
		</tr>
	</thead>
	<tbody class="questabletbody">
	    <?php $allques=array(); $i=1;  if (!empty($quesdt)) {
    foreach ($quesdt as $quesdts) {
        $allques [] = $quesdts->QUES_ID; ?>
        	<tr class="">
    			<td width="8%" style="font-weight:700;"><?php echo $i.'.'; ?></td>
    			<td style="font-size: 13px;font-weight: 600;" width="80%"><?php if ($quesdts->QUES_TYPE==1) {
            echo $quesdts->QUES;
        } else {
            echo json_decode($quesdts->QUES)->QUESTION;
        } ?></td>
    			<td class="with-btn" nowrap=""><a href="javascript:void(0);" attr-quesid="<?php echo $quesdts->QUES_ID; ?>" attr-added="0" class="btn btn-sm btn-primary width-60 m-r-2 p-4 quesaddintpbtn">Add</a></td>
    		</tr>
    	<?php $i++;
    }
} else { ?>
    	    <tr>
    	        <td colspan="3" style="text-align:center;">Data Not Available</td>
    	    </tr>
    	  <?php } ?>
    	  
	</tbody>
	<input type="hidden" class="totalques" value="<?php echo $i-1; ?>">
	<input type="hidden" class="alladdedques" value="<?php echo implode(',', $allques); ?>">
</table>
