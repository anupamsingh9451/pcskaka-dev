<script>
 $('document').ready(function(){
     var totalques = <?php echo $tsdt[0]->TS_QUESTION_NOS;?>;
     testpaperlist();
     get_all_sub();
	 $('body').on('change','.book',function(){
		var bookids = $(this).val();
		
		get_question_by_bookid(bookids);
	});
	$('body').on('click','.testpaperedit',function(){
		var tpid = $(this).attr('attr-testpaperid');
		
		get_tpedit_modal(tpid);
        

	});
	$('body').on('change','.testpapertype',function(){
		var questypeid = $(this).val();
		var bookids = $('option:selected','.book').val();
		if(bookids!=''){
		    get_question_by_bookid(bookids);
		}
	});
	$('body').on('change','.questions',function(){
	    $('.countques').html($(".questions :selected").length);
	});
	$('body').on('keyup','.quesno',function(){
        totalques = $(this).val();
	});
   $('.selectpicker').selectpicker({
       maxOptions:totalques
   });
   
    $('#datetimepicker1').datetimepicker({
        format: 'HH:mm'
    });
   $('#datetimepicker1').keydown(false);
   
 });
 function get_all_sub(){
     $.ajax({
    		type: 'GET',
    		url: base_loc + 'admingetajax/get_all_sub',
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    		    $('.subject option').remove();
    			var str= '<option value="">--Select Subject--</option>';
    			if(msg.status==200){
				    var subidarr = new Array();
					$.each(msg.data, function(index, element) {
						str += '<option value="'+element.SUB_ID+'">'+element.SUB_NAME+'</option>';
					});
    				$('.subject').append(str);
    			}else{
    				location.href = base_loc;
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
 }
  function get_question_by_bookid(bookids){
      console.log(bookids);
      var tsid = $('.tsid').val();
      var ques_type = $('option:selected','.testpapertype').val();
      $.ajax({
    		type: 'GET',
    		data:'bookid='+ bookids + '&ques_type='+ques_type + '&tsid='+ tsid,
    		url: base_loc + 'admingetajax/get_question_by_bookid',
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    		    $('.questions option').remove();
    			var str='';
    			if(msg.status==200){
    				if(msg.data.length>0){
    					$.each(msg.data, function(index, element) {
    						str+='<option value="'+element.QUES_ID+'">'+element.QUES+'</option>';
    					});
    				}
    				$('.questions select').append(str);
    				$('.questions').selectpicker('refresh');
    			}else{
    				location.href = base_loc;
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
  }
 function testpaperlist(){
	   var tsid = $('.tsid').val();
		$('#testpapertable').dataTable().fnDestroy();
		$('#testpapertable').DataTable({
			"dom": "<'row'<'col-sm-12'Bifrt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"ajax":{
				"url":base_loc + 'admingetajax/listtestpaper',
				"type": "GET",
				"data": { tsid:tsid  },
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"TP_NAME"},
				{"data":"QUES_NO"},
				{"data":"TP_CREATED_AT"},
				{"data":"TP_START_DATE"},
				{"data":"TP_STATUS"},
				{"data":"TP_PUBLISHED"},
				{"data":"TOOLS"},
			]
		});
	}
	function get_tpedit_modal(tpid){
	    $.ajax({
			type: 'GET',
			url: base_loc + 'admingetajax/gettestpapermodal',
			data:'tpid=' + tpid,
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
			success: function (msg) {
                $('#edittestpapermodal').modal('show');
				$('.testpapermodaldiv').html(msg);
			},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc;
    			}
    		}
		});
	}
	
</script>