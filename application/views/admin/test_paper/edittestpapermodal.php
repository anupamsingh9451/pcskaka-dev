<!--<form id="testpaperedit">-->
<input type="hidden" name="tpid" value="<?php echo $tpdt[0]->TP_ID; ?>">
<div class="row">
	<div class="col-md-6">
		<label class="col-form-label">Test Paper Name <span class="star">*</span></label>
		<input type="text" class="form-control testpapername" name="testpapername" placeholder="Enter Test Paper Name" value="<?php echo $tpdt[0]->TP_NAME;?>" required>
	</div>
	<div class="col-md-6">
		<label class="col-form-label">Test Paper Type <span class="star">*</span></label>
		<select class="form-control testpapertype" name="testpapertype" data-style="btn-white" required>
			<option <?php  if ($tpdt[0]->TP_TYPE=='1,2') {
    echo 'selected';
}?> value="1,2">All</option>
			<option <?php  if ($tpdt[0]->TP_TYPE=='1') {
    echo 'selected';
}?> value="1">Subjective</option>
			<option <?php  if ($tpdt[0]->TP_TYPE=='2') {
    echo 'selected';
}?> value="2">Objective</option>
		</select>
	</div>

	<div class="col-md-6">
		<label class="col-form-label">Subject <span class="star">*</span></label>
		<select class="form-control subject" name="subject" data-style="btn-white" required>
			<option value="<?php echo $quesdt[0]->SUB_ID; ?>"><?php echo $quesdt[0]->SUB_NAME; ?></option>
		</select>
	</div>
	<div class="col-md-6">
		<label class="col-form-label">Books <span class="star">*</span></label>
		<select class="form-control book" name="book" data-style="btn-white" required>
			<option value="<?php echo $quesdt[0]->BOOK_ID; ?>"><?php echo $quesdt[0]->BOOK_NAME; ?></option>
		</select>
	</div>
	<div class="col-md-6">
		<label class="col-form-label">Questions <span class="star">*</span> </label><span class="m-t-10 m-r-5 f-w-600 text-blue countques" style="float:right"></span>
		<select class="form-control questions selectpicker" multiple name="questions[]" data-live-search="true" data-style="btn-white">
			<option value="">----</option>
		</select>
	</div>
	<div class="col-md-6">
		<label class="col-form-label">Cost </label>
		<input type="text" class="form-control cost input_num" value="<?php echo $tpdt[0]->TP_COST; ?>" name="cost" placeholder="Enter Test Paper Cost">
	</div>
	<div class="col-md-6">
		<label class="col-form-label">Mark </label>
		<input type="text" class="form-control mark input_num" value="<?php echo $tpdt[0]->TP_MARKS; ?>" name="mark" placeholder="Enter mark">
	</div>
	<div class="col-md-6">
		<label class="col-form-label">Cut of </label>
		<input type="text" class="form-control cutof input_num" value="<?php echo $tpdt[0]->TP_CUT_OFF; ?>" name="cutof" placeholder="Enter Cut of mark">
	</div>
	<div class='col-sm-6'>
		<label class="col-form-label">Duration </label>
		<div class='input-group date' id='datetimepicker1'>
			<input type='text' class="form-control duration" value="<?php echo $tpdt[0]->TP_DURATION; ?>" name="duration" />
			<span class="input-group-addon">
				<i class="fa fa-time"></i>
			</span>
		</div>
	</div>
	<div class="col-md-6">
		<label class="col-form-label"> Created Date <span class="star">*</span></label>
		<input type="text" class="form-control dtpicker created_date" name="created_date" value="<?php echo $tpdt[0]->TP_CREATED_AT; ?>">
	</div>
	<div class="col-md-6">
		<label class="col-form-label"> Validity  <span class="star">*</span></label>
		<input type="text" class="form-control dtpicker validity" value="<?php echo $tpdt[0]->TP_VALIDITY; ?>" name="validity">
	</div>
	<div class="col-md-6">
		<label class="col-form-label">Test Paper Description <span class="star">*</span></label>
		<textarea rows="2" class="form-control desc" name="desc"><?php echo $tpdt[0]->TP_DESCRIPTION; ?></textarea>
	</div>
	
	
	<div class="col-md-12" style="margin-top:10px">
		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary testpapercreate">SAVE</button>
		</div>
	</div>
</div>
<!--</form>-->
