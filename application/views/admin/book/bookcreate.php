<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Book</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Create</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Book</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class='col-md-6'>
			<!-- begin panel -->
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">NEW BOOK</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<form id="bookcreate"  enctype="multipart/form-data">
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Course <span class="star">*</span></label>
							<div class="col-md-9">
								<select class="form-control m-b-5 courseid" name="course"  required>
									<option value="">--Select Course --</option>
									<?php $courses=get_all_courses();
                                        if (!empty($courses)) {
                                            foreach ($courses as $course) {
                                                if ($course->COURSE_STATUS==1) {
                                                    echo '<option value="'.$course->COURSE_ID.'">'.$course->COURSE_NAME.' </option>';
                                                }
                                            }
                                        }
                                    ?>
								</select>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Library <span class="star">*</span></label>
							<div class="col-md-9">
								<select class="form-control library" name="library" data-style="btn-white" required>

								</select>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Subject <span class="star">*</span></label>
							<div class="col-md-9">
								<select class="form-control subject" name="subject" data-style="btn-white" required>

								</select>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Book Name <span class="star">*</span></label>
							<div class="col-md-9">
								<input type="text" class="form-control m-b-5 bookname" name="bookname" placeholder="Enter Book name" required>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Chapter No. </label>
							<div class="col-md-9">
								<input type="text" class="form-control m-b-5 chapterno input_num" name="chapterno" placeholder="Enter Chapter Number" >
							</div>
						</div>
						<div class="form-group row m-b-15 hide">
    						<label class="col-form-label col-md-3">Created Date <span class="star">*</span></label>
    						<div class="col-md-9">
    						    <input type="text" class="form-control m-b-5 dtpicker" name="bookcreatedat" value="<?php echo date('Y-m-d')?>" required>
						    </div>
						</div>
						<div class="form-group form-row m-b-15 image-section">
							<div class="col row">
								<label class="col-form-label col-md" for="book_img">Book Image </label>
								<div class="col-md">
									<span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
										<i class="fas fa-plus"></i>
										<span>Add files...</span>
									</span>
									<input type="file" name="book_img" id="book_img" accept="image/*">
								</div>
							</div>
							<div class="col text-right">
								<a href="javascript:;" target="_blank" ><img src="javascript" alt=""></a>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary bookcreate">SAVE</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

