<script>
	$('document').ready(function(){
	    get_all_library();
		/* for edit company detail*/
		$('body').on('click','.bookedit',function(){
			$('#editbook').find('.chapterno').val($(this).attr('attr-chapterno'));
			$('#editbook').find('.bookstatus').val($(this).attr('attr-status'));
			$('#editbook').find('.bookid').val($(this).attr('attr-bookid'));
			$('#editbook').find('.booknm').val($(this).attr('attr-booknm'));
			$('#editbook').find('.subid').val($(this).attr('attr-subid'));
			$('#editbook').find('.bookcost').val($(this).attr('attr-cost'));
			$('#editbook').find('.book_pay_type').val($(this).attr('attr-pay-type'));
			if($(this).attr('attr-bookimg')!=""){
                $('#editbook').find('img').attr({'src':$(this).attr('attr-bookimg')}).css({ 'max-height': '80px', 'max-width': '120px' });
                $('#editbook').find('img').parent('a').attr('href',$(this).attr('attr-bookimg'));
            }else{
                $('#editbook').find('img').attr({'src':''}).css({ 'max-height': '0px', 'max-width': '0px' });
                $('#editbook').find('img').parent('a').attr('href','javascript:;');
            }
			$('#editbook').modal('show');
		});
		$('body').on('change','.all_lib',function(){
    		var lib_id = $(this).val();
    		if(lib_id!='' && lib_id!=null){
    		    var change = 1;
    		    get_sub_by_libid(lib_id,change);
    		}
		});
		$('body').on('change','.course',function(){
			$('.all_lib').children('option').show();
			$('.all_lib').val('');
			if($(this).val()!=""){
				$('.all_lib').children('option[attr_course!="'+$(this).val()+'"]').hide();
			}
			$('.all_sub').html('');
		});
		$('body').on('change','.courseid', function () {
			getlibrary();	
		});	
		$('body').on('change','.library', function () {
			libids=$(this).val();
			change=" ";
			get_sub_by_libid(libids,change)
		});	
			
	   
		$('body').on('click','.searchbook',function(){
			var subid = $('option:selected','.all_sub').val();
			if(subid!='' && subid!=null){
				updatelistbooks();
			}
		});
		if($('#booklist').length>0){
			updatelistbooks();
		}
	});
	function get_all_library(){
		html = "";
		if ($('.courseid').val() != "" && $('.courseid').val() != "undefined" && $('.courseid').length > 0) {
			html += "courseid=" + $('.courseid').val();
		}
        $.ajax({
			type: 'GET',
			//data:'subid='+ subide,
			url: base_loc + 'admingetajax/getlibrary?'+html,
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
			success: function (msg) {
				$('.all_lib option').remove();
				var str='';
				if(msg.status==200){
					if(msg.data.length>0){
						var libidarr = new Array();
						var libicourse = new Array();
						$.each(msg.data, function(index, element) {
							str += '<option value="'+element.LIB_ID+'" attr_course="'+element.COURSE_ID+'">'+element.LIB_NAME+'</option>';
							libidarr.push(element.LIB_ID);
							libicourse.push(element.COURSE_ID);
						});
						var libids = libidarr.join();
						var str1 = "<option selected value='" + libids + "' attr_course='"+libicourse+"'>"+'All'+"</option>";
						$('.all_lib').append(str1);
						$('.all_lib').append(str);
						var change = '';
						if($('#booklist').length>0){
							get_sub_by_libid(libids,change);
						}
						
					}
				}else{
					location.href = base_loc;
				}
			},
			error: function (msg) {
				if (msg.responseJSON['status'] == 303) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 401) {
					location.href = base_loc; 
				}
				if (msg.responseJSON['status'] == 400) {
					location.href = base_loc; 
				}
			}
		});
    }
     
	function get_sub_by_libid(libids,change){
		$.ajax({
			type: 'GET',
			data:'subid='+ libids,
			url: base_loc + 'admingetajax/get_subject_by_libid',
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
			success: function (msg) {
				$('.all_sub option').remove();
				var str='';
				if(msg.status==200){
					if(msg.data.length>0){
						var subid = new Array();
						$.each(msg.data, function(index, element) {
							str+='<option value="'+element.SUB_ID+'">'+element.SUB_NAME+'</option>';
							subid.push(element.SUB_ID);
						});
						var subids = subid.join();
						var str1 = "<option selected value='" + subids+ "'>"+'All'+"</option>";
					}
					$('.all_sub').append(str1);
					$('.all_sub').append(str);
					if(change=='' && $('#booklist').length>0){
						updatelistbooks();
					}
				}else{
					location.href = base_loc;
				}
			},
			error: function (msg) {
				if (msg.responseJSON['status'] == 303) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 401) {
					location.href = base_loc; 
				}
				if (msg.responseJSON['status'] == 400) {
					location.href = base_loc; 
				}
			}
		});
	}
	function updatelistbooks(){
	    var subid = $('option:selected','.all_sub').val();
    	if(subid!='' && subid!=null){
    		$('#booklist').dataTable().fnDestroy();
    		$('#booklist').DataTable({
    			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
                // dom: 'Bfrtip,l',
    			pageLength: '10',
    		    "lengthMenu": [[10, 25, 50, 100, 1000, 2000, 5000], [10, 25, 50, 100, 1000, 2000, 5000]],
    			"buttons": [
    				'excel', 'pdf', 'print'
    			],
    			"processing": true,
    		    "serverSide": true,
    			"ajax":{
    				"url":base_loc + 'admingetajax/listbook',
    				"type": "GET",
    				"data": { subid:subid},
    				"headers": {
    					'Client-Service': clientserv,
    					'Auth-Key': apikey,
    					'User-ID': loginid,
    					'Authorization': token,
    					'type': type
    				},
    				"error": function (msg) {
        				if (msg.responseJSON['status'] == 303) {
        					location.href = base_loc;
        				}
        				if (msg.responseJSON['status'] == 401) {
        					location.href = base_loc; 
        				}
        				if (msg.responseJSON['status'] == 400) {
        					location.href = base_loc; 
        				}
        			}
    			},
    			"columns":[
					{"data":"SR_NO"},
					{"data":"BOOK_NAME"},
					{"data":"SUB_NAME"},
					{"data":"LIB_NAME"},
					{"data":"COURSE_NAME"},
    				// {"data":"BOOK_CHAPTER_NOS"},
    				{"data":"BOOK_CREATED_AT"},
    				{"data":"PENDING_QUES"},
    				{"data":"BOOK_STATUS"},
    				{"data":"TOOLS"},
    			]
    		});
    	}
	}
</script>