<?php if(!empty($books['data'])){?>
    <div class="table-responsive">
        <table class="table border">
            <thead class="bg-blue">
                <tr>
                    <th>Sr No.</th>
                    <th>Book Name</th>
                    <th>Price</th>
                    <th>Created Date</th>
                </tr>    
            </thead>
            <tbody>
            <?php $sr_bk=0; foreach ($books['data'] as $book) {?>
                <tr>
                    <td >
                        <?php echo ++$sr_bk; ?>
                    </td>
                    <td><?php echo $book->BOOK_NAME; ?></td>
                    <td><?php echo $book->BOOK_COST; ?></td>
                    <td><?php echo $book->BOOK_CREATED_AT; ?></td>
                </tr>
            <?php }?>
            </tbody>
        </table>
    <div>    
<?php }else{ ?>
    <div class="text-center">No Book Found</div>
<?php } ?>