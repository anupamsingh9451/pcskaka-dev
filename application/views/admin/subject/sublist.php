<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Subject</a></li>
		<li class="breadcrumb-item active">list</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">LIST ALL SUBJECTS</h1>
	<!-- begin page-header -->
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Library List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="sublist" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th>Sr No.</th>
								<th>Subject Name</th>
								<th>Library Name</th>
								<th>Created Date</th>
								<th>Price</th>
								<th>Status</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-6 -->
		
	</div>
</div>
		<!-- end #content -->
		<div class="modal fade" id="editsubject">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">EDIT <span class='librarynm'></span> Subject</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="editsubjectform">
				<div class="modal-body">
					<input type='hidden' class='subid' name="subid" value='' />
					<input type='hidden' class='libid' name="libid" value='' />
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Subject Name<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 subnm" name="subnm" placeholder="Enter Subject name here" required>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Status<span class="star">*</span></label>
						<div class="col-md-9">
							<select name="substatus" class='form-control substatus'>
								<option value='1'>Active</option>
								<option value='0'>Inactive</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success editsubjectform">Update</a>
				</div>
			</form>
		</div>
	</div>
</div>