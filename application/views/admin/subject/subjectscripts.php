<script>
	$('document').ready(function(){
		/* for edit company detail*/
		$('body').on('click','.subedit',function(){
			$('#editsubject').find('.subnm').val($(this).attr('attr-subnm'));
			$('#editsubject').find('.substatus').val($(this).attr('attr-status'));
			$('#editsubject').find('.subid').val($(this).attr('attr-subid'));
			$('#editsubject').find('.libid').val($(this).attr('attr-libid'));
			$('#editsubject').modal('show');
		});
		var autocompletearea = [];
		$('body').on('change','.courseid', function () {
			getlibrary();	
		});
		$('body').on('click','.add-row-book-collapse',function(){
			sub_id=$(this).parents('tr').find('li a').attr('attr-subid');
			d=$(this);
			if(sub_id){
				if($(this).find('i').hasClass('fa-plus-circle')){
					$.ajax({
						type: 'GET',
						url: base_loc + 'admingetajax/subject_contentpage',
						data: {'sub_id':sub_id},
						headers: {
							'Client-Service': clientserv,
							'Auth-Key': apikey,
							'User-ID': loginid,
							'Authorization': token,
							'type': type
						},
						success: function(msg) {
							d.parents('tr').after('<tr class="subject-row-'+sub_id+'" id="collapsebook"><td colspan="7">'+msg+'</td></tr>');
						},
						error: function(msg) {
							if (msg.responseJSON['status'] == 303) {
								location.href = base_loc;
							}
							if (msg.responseJSON['status'] == 401) {
								location.href = base_loc;
							}
							if (msg.responseJSON['status'] == 400) {
								location.href = base_loc;
							}
						}
					});	
					
				}else{
					$('.subject-row-'+sub_id).hide();
				}
			}	
			$(this).find('i').toggleClass("fas fa-plus-circle fas fa-minus-circle");
		});
		updatelistsubject();
	});
	
	function updatelistsubject(){
		$('#sublist').dataTable().fnDestroy();
		$('#sublist').DataTable({
			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"ajax":{
				"url":base_loc + 'admingetajax/subjectlist',
				"type": "GET",
				"data": { loginid: loginid},
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"SUB_NAME"},
				{"data":"LIB_NAME"},
				{"data":"SUB_CREATED_AT"},
				{"data":"SUB_COST"},
				{"data":"SUB_STATUS"},
				{"data":"TOOLS"},
			]
		});
	}
</script>