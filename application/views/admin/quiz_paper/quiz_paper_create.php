<style>
    .note-editing-area{
        margin-top:7px !important;
        height:225px;
    }
    .note-editor.note-frame {
        border: 1px solid #b3b3b3 !important;
    }
    .note-editor.note-frame .note-editing-area {
        border: 1px solid #b3b3b3 !important;
    }
    .note-insert{
        display:none;
    }
    
    .btn-fullscreen{
        display:none;
    }
    .btn-sm[aria-label="Help"] {
        display:none;
    }
    .note-editable{
        height:225px !important;
    }
    .note-recent-color{
        background-color: rgb(255, 255, 255) !important;
    }
    .summernote .note-editable {
      p {
        font-size: 14px;
        font-weight: normal;
      }
      h1 {
        text-decoration: underline;
      },
      ...
    }
</style>
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Quiz paper</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Create</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Quiz Paper</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class='col-md-8'>
			<!-- begin panel -->
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">New Quiz Paper</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<form id="testseriescreate">
					    <div class="col-md-12">
					        <div class="row">
                                <div class="col-md-6">
        							<label class="col-form-label">Library <span class="star">*</span></label>
        							<select class="form-control library" name="library" data-style="btn-white" required>

								</select>
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">Subject <span class="star">*</span></label>
        							<select class="form-control subject" name="subject" data-style="btn-white" required>

								</select>
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">Book <span class="star">*</span></label>
        							<select class="form-control book" name="book" data-style="btn-white" required>

								</select>
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">No. of Questions <span class="star">*</span></label>
        							<input type="text" class="form-control quesno input_num" name="quesno" placeholder="Enter No. of Questions" required>
        						</div>
        						<div class="col-md-6">
                    				<label class="col-form-label">Positive Mark <span class="star">*</span></label>
                    				<input type="text" class="form-control input_num pmark" name="pmark" placeholder="Enter Positive Mark" required>
                    		    </div>
                    		    <div class="col-md-6">
                    				<label class="col-form-label">Negative Mark <span class="star">*</span></label>
                    				<input type="text" class="form-control input_num nmark" name="nmark" placeholder="Enter Negative Mark" required>
                    		    </div>
        						<div class="col-md-6">
            						<label class="col-form-label">Start Date <span class="star">*</span></label>
            						 <input type="text" class="form-control dtpicker" name="startdate" value="" required>
        						</div>
        						<div class="col-md-6">
            						<label class="col-form-label">End Date <span class="star">*</span></label>
            						 <input type="text" class="form-control dtpicker" name="enddate" value="" required>
        						</div>
        						<div class="col-md-6 hide">
            						<label class="col-form-label">Created Date <span class="star">*</span></label>
            						 <input type="text" class="form-control dtpicker" name="createdate" value="<?php echo date('Y-m-d')?>" required>
        						</div>
                                <div class="col-md-6">
                    				<label class="col-form-label">Duration <span class="star"> Enter Duration in Minutes *</span></label>
                    				<input type="text" class="form-control input_num duration" name="duration" required >
                    		    </div>
        						<div class="col-md-12">
        							<label class="col-form-label">Description <span class="star">*</span></label>
                                    <textarea class="summernote desc" name="desc" id="mysummernote"></textarea>
            					</div>
					        </div>
					    </div>
						<div class="form-group row m-b-10 m-t-10">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary testseriescreate">SAVE</button>
							</div>
						</div>
					</form>
				</div>
			
			</div>
			
		</div>

		
	</div>
</div>

<script>
	$(document).ready(function() {
		FormSummernote.init();
	});
</script>