<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">sell</a></li>
		<li class="breadcrumb-item active">Exam</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">ADD SELL EXAMS</h1>
	<!-- begin page-header -->
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-lg-6">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Add sell Exams</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
                <div class="panel-body ">
                    <form id="add_sellexam">
                        <div class="form-inline mb-2">
                            <label for="examname">Exam Name</label>
                            <input type="text" name="examname" id="examname" class="form-control col-9 ml-auto" maxlenght="100">
                        </div>  
                        <div class="form-inline mb-2">
                            <label for="examprice">Exam Name</label>
                            <input type="text" name="examprice" id="examprice" class="form-control col-9 ml-auto input_num" maxlength="10">
                        </div>
                        <div class="form-inline mb-2">
                            <label class="col-form-label">Exam Content<span class="star">*</span></label>
							<div class="col-9">
								<input type="hidden" class='coursecontent' name="coursecontent" value="">
								
							</div>
                        </div>    
                    </form>
                </div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-6 -->
	</div>
</div>