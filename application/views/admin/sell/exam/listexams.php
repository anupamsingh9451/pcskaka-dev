<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">sell</a></li>
		<li class="breadcrumb-item active">Exams</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">LIST ALL SELL EXAMS</h1>
	<!-- begin page-header -->
	<!-- end page-header -->
	
	<div class="d-flex mb-2">
        <div class="ml-auto">
            <a href="<?php echo base_url('ratan/sell/exam/addexams'); ?>"  class="btn btn-warning">Add Exam</a>
        </div>
    </div>
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Sell Exams List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="librarylist" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th width="text-wrap">Sr No.</th>
								<th>Exam Name</th>
								<th>Exam Price</th>
								<th>Exam Status</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-6 -->
		
	</div>
</div>
		<!-- end #content -->
