<script>
	$('document').ready(function(){
		/* for edit company detail*/
		$('body').on('click','.libraryedit',function(){
			$('#editlibrary').find('.libnm').val($(this).attr('attr-libnm'));
			$('#editlibrary').find('.libstatus').val($(this).attr('attr-status'));
			$('#editlibrary').find('.libid').val($(this).attr('attr-libid'));
			$('#editlibrary').find('.libcost').val($(this).attr('attr-libcost'));
			$('#editlibrary').find('.lib_pay_type').val($(this).attr('attr-pay_type'));
			$('#editlibrary').find('.lib_courseid').val($(this).attr('attr-courseid'));
			$('#editlibrary').find('.popular').val($(this).attr('attr-popular'));
			if($(this).attr('attr-bookimg')!=""){
                $('#editlibrary').find('img').attr({'src':$(this).attr('attr-libimg')}).css({ 'max-height': '80px', 'max-width': '120px' });
                $('#editlibrary').find('img').parent('a').attr('href',$(this).attr('attr-libimg'));
            }else{
                $('#editlibrary').find('img').attr({'src':''}).css({ 'max-height': '0px', 'max-width': '0px' });
                $('#editlibrary').find('img').parent('a').attr('href','javascript:;');
            }
			$('#editlibrary').modal('show');
		});
	
		$('body').on('change','#paystatus', function () {
			if($(this).val()==2){
				$('.libcost').attr({'required':'false','readonly':'true'}).val('0');
			}else{
				$('.libcost').attr({'required':'true','readonly':false});
			}
		});
		$('body').on('click','.librarysale',function(){
			$('#editlibrarysale').find('.librarynm').html($(this).attr('attr-libnm'));
			$('#editlibrarysale').find('.libid').val($(this).attr('attr-libid'));
			$('#editlibrarysale').find('.saled').val($(this).attr('attr-salestatus'));
			paystatus=$(this).attr('attr-libpaystatus');
			$('#editlibrarysale').find('.paystatus').val(paystatus);
			if(paystatus==2){
				$('#editlibrarysale').find('.libcost').val($(this).attr('attr-libcost')).attr('readonly',true);
			}else{
				$('#editlibrarysale').find('.libcost').val($(this).attr('attr-libcost')).attr('readonly',false);
			}
			$('#editlibrarysale').modal('show');
		});
		// to get dcn all areas predifined
		var autocompletearea = [];
		$('body').on('click','.add-row-subject-collapse',function(){
			lib_id=$(this).parents('tr').find('li a').attr('attr-libid');
			d=$(this);
			if(lib_id){
				if($(this).find('i').hasClass('fa-plus-circle')){
					$.ajax({
						type: 'GET',
						url: base_loc + 'admingetajax/library_contentpage',
						data: {'lib_id':lib_id},
						headers: {
							'Client-Service': clientserv,
							'Auth-Key': apikey,
							'User-ID': loginid,
							'Authorization': token,
							'type': type
						},
						success: function(msg) {
							d.parents('tr').after('<tr class="subject-row-'+lib_id+'" id="collapsesubject"><td colspan="7">'+msg+'</td></tr>');
						},
						error: function(msg) {
							if (msg.responseJSON['status'] == 303) {
								location.href = base_loc;
							}
							if (msg.responseJSON['status'] == 401) {
								location.href = base_loc;
							}
							if (msg.responseJSON['status'] == 400) {
								location.href = base_loc;
							}
						}
					});	
					
				}else{
					$('.subject-row-'+lib_id).hide();
				}
			}	
			$(this).find('i').toggleClass("fas fa-plus-circle fas fa-minus-circle");
		});
		updatelistlibrary();
	});
	
	function updatelistlibrary(){
		$('#librarylist').dataTable().fnDestroy();
		$('#librarylist').DataTable({
			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"ajax":{
				"url":base_loc + 'admingetajax/listlibrary',
				"type": "GET",
				"data": { loginid: loginid},
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"NAME"},
				{"data":"COURSE_NAME"},
				{"data":"LIB_PRICE"},
				{"data":"LIB_STATUS"},
				{"data":"SALE_STATUS"},
				{"data":"TOOLS"},
			]
		});
	}
</script>