<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Library</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Create</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Library</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class='col-md-6'>
			<!-- begin panel -->
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">NEW LIBRARY</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<form id="librarycreate" enctype="multipart/form-data">
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Course <span class="star">*</span></label>
							<div class="col-md-9">
    							<select class="form-control m-b-5 course" name="course" required>
    								<option value="">--Select Course --</option>
    								<?php $courses=get_all_courses();
                                        if (!empty($courses)) {
                                            foreach ($courses as $course) {
                                                if ($course->COURSE_STATUS==1) {
                                                    echo '<option value="'.$course->COURSE_ID.'">'.$course->COURSE_NAME.' </option>';
                                                }
                                            }
                                        }
                                    ?>
    							</select>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Library Name <span class="star">*</span></label>
							<div class="col-md-9">
								<input type="text" class="form-control m-b-5 libraryname" name="libraryname" placeholder="Enter Library name" required>
							</div>
						</div>
						<div class="form-group row m-b-15">
    						<label class="col-form-label col-md-3">Library Cost<span class="star">*</span></label>
    						<div class="col-md-9">
    							<input type="text" class="form-control m-b-5 libcost input_num" name="libcost" placeholder="Enter Library Cost here" required>
    						</div>
    					</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3" for="popular">Is popular? <span class="star">*</span></label>
							<div class="col-md-9" >
                                <select name="popular" id="popular"  class="form-control">
									<option value="1">Yes</option>
									<option value="0">No</option>
								</select>
							</div>
						</div>
						<div class="form-group form-row m-b-15 image-section">
							<div class="col row">
								<label class="col-form-label col-md" for="library_img">Library Image </label>
								<div class="col-md">
									<span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
										<i class="fas fa-plus"></i>
										<span>Add files...</span>
									</span>
									<input type="file" name="library_img" id="library_img" accept="image/*">
								</div>
							</div>
							<div class="col text-right">
								<a href="javascript:;" target="_blank" ><img src="javascript" alt=""></a>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary librarycreate">SAVE</button>
							</div>
						</div>
					</form>
				</div>
			
			</div>
		</div>

		
	</div>
</div>

