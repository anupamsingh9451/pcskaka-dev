<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Library</a></li>
		<li class="breadcrumb-item active">list</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">LIST ALL LIBRARY</h1>
	<!-- begin page-header -->
	<!-- end page-header -->
	
	
	
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Library List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="librarylist" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th width="text-wrap">Sr No.</th>
								<th>Library Name</th>
								<th>Course</th>
								<th>Library Price</th>
								<th>Status</th>
								<th>Sale Status</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-6 -->
		
	</div>
</div>
		<!-- end #content -->
<div class="modal fade" id="editlibrary">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">EDIT <span class='librarynm'></span> Area</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="editlibraryform"  enctype="multipart/form-data">
				<div class="modal-body">
					<input type='hidden' class='libid' name="libid" value='' />
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Course <span class="star">*</span></label>
						<div class="col-md-9">
							<select class="form-control m-b-5 lib_courseid" name="course"  required>
								<option value="">--Select Course --</option>
								<?php $courses=get_all_courses();
                                    if (!empty($courses)) {
                                        foreach ($courses as $course) {
                                            echo '<option value="'.$course->COURSE_ID.'">'.$course->COURSE_NAME.' </option>';
                                        }
                                    }
                                ?>
							</select>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Library Name<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 libnm" name="libnm" placeholder="Enter Library name here" required>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Library Cost<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 libcost input_num" name="libcost" placeholder="Enter Library Cost here" required>
						</div>
					</div>
					
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Status<span class="star">*</span></label>
						<div class="col-md-9">
							<select name="libstatus" class='form-control libstatus'>
								<option value='1'>Active</option>
								<option value='0'>Inactive</option>
							</select>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3" for="popular">Is popular? <span class="star">*</span></label>
						<div class="col-md-9" >
							<select name="popular" id="popular"  class="form-control">
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
					</div>
					<div class="form-group form-row m-b-15 image-section">
						<div class="col row">
							<label class="col-form-label col-md" for="library_img">Course Image </label>
							<div class="col-md">
								<span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
									<i class="fas fa-plus"></i>
									<span>Add files...</span>
								</span>
								<input type="file" name="library_img" id="library_img" accept="image/*">
							</div>
						</div>
						<div class="col text-right">
							<a href="javascript:;" target="_blank" ><img src="javascript" alt=""></a>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success editlibraryform">Update</a>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editlibrarysale">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Sale Library <span class='librarynm'></span> </h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="editlibrarysaleform"  enctype="multipart/form-data">
				<div class="modal-body">
					<input type='hidden' class='libid' name="libid" value='' />
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3" id="paystatus">Library Pay Status<span class="star">*</span></label>
						<div class="col-md-9">
							<select name="paystatus" class="form-control paystatus" id="paystatus" required>
								<option value="1">Paid</option>
								<option value="2">Free</option>
							</select>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Library Cost<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 libcost input_num" name="libcost" placeholder="Enter Library Cost here" required>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3" id="saled">Is Sale?<span class="star">*</span></label>
						<div class="col-md-9">
							<select name="paystatus" class="form-control saled" id="saled" required>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success editlibrarysaleform">Update</a>
				</div>
			</form>
		</div>
	</div>
</div>