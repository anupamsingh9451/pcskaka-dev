<?php if(!empty($subjects['data'])){  ?>
    <div class=" table-responsive">
        <table class="table  border">
            <thead class="bg-gradient-aqua">
                <tr>
                    <th>Sr No.</th>
                    <th>Subject Name</th>
                    <th>Price</th>
                    <th>Created Date</th>
                </tr>    
            </thead>
            <tbody>
            <?php $sr_sub=0; foreach ($subjects['data'] as $subject) { ?>
                <tr>
                    <td>
                        <?php echo ++$sr_sub; ?>
                        <a  class="collapse-btn" data-toggle="collapse" href="#collapsesubject_<?php echo $sr_sub; ?>" aria-expanded="true" aria-controls="collapsesubject_<?php echo $sr_sub; ?>">
                            <i class="fas fa-lg fa-fw m-r-10 fa-plus-circle" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td><?php echo $subject->SUB_NAME; ?></td>
                    <td><?php echo $subject->SUB_COST; ?></td>
                    <td><?php echo $subject->SUB_CREATED_AT; ?></td>
                </tr>
                <?php $books=$this->Admingetmodel->get_book_by_subid($subject->SUB_ID);?>
                <tr id="collapsesubject_<?php echo $sr_sub; ?>" class="collapse">
                    <td colspan="4">
                    <?php if(!empty($books['data'])){?>
                        <div class="table-responsive">
                            <table class="table border">
                                <thead class="bg-blue">
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Book Name</th>
                                        <th>Price</th>
                                        <th>Created Date</th>
                                    </tr>    
                                </thead>
                                <tbody>
                                <?php $sr_bk=0; foreach ($books['data'] as $book) {?>
                                    <tr>
                                        <td >
                                            <?php echo ++$sr_bk; ?>
                                        </td>
                                        <td><?php echo $book->BOOK_NAME; ?></td>
                                        <td><?php echo $book->BOOK_COST; ?></td>
                                        <td><?php echo $book->BOOK_CREATED_AT; ?></td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>
                        <div>    
                    <?php }else{ ?>
                        <div class="text-center">No Book Found</div>
                    <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
<?php }else{ ?>
    <div class="text-center">No Subject Found</div>
<?php } ?>
<script>
    $('.collapse-btn').on('click', function () {
		$(this).find('i').toggleClass("fas fa-plus-circle fas fa-minus-circle");
	});
</script>