<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Member</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Create</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Member</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class='col-md-6'>
			<!-- begin panel -->
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">NEW MEMBER</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<form id="membercreate">
					    <div class="col-md-12">
					        <div class="row">
					            <div class="col-md-6">
        							<label class="col-form-label">Member Type <span class="star">*</span></label>
        							<select class="form-control mtype" name="mtype" data-style="btn-white" required>
                                        <option value="">--Select Member Type--</option>
                                        <?php foreach ($membertype as $membertypes) {
    if ($membertypes->USER_ROLE_ID!=3 && $membertypes->USER_ROLE_ID!=1) { ?>
                                            <option value="<?php echo $membertypes->USER_ROLE_ID; ?>"><?php echo $membertypes->USER_ROLE_NAME; ?></option>
                                        <?php }
} ?>
        							</select>
            					</div>
                                <div class="col-md-6">
        							<label class="col-form-label">Member First Name <span class="star">*</span></label>
        							<input type="text" class="form-control mfname" name="mfname" placeholder="Enter First Name" required>
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">Member Last Name <span class="star">*</span></label>
        							<input type="text" class="form-control mlname" name="mlname" placeholder="Enter First Name" required>
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">Member Father Name </label>
        							<input type="text" class="form-control mfathername" name="mfathername" placeholder="Enter First Name" >
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">Member Address </label>
        							<input type="text" class="form-control maddress" name="maddress" placeholder="Enter Addresse">
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">Member Contact </label>
        							<input type="text" class="form-control mcontact input_num" name="mcontact" placeholder="Enter Contact" >
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">Email <span class="star">*</span></label>
        							<input type="text" class="form-control memail" name="memail" placeholder="Enter Email" required>
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">Qualification </label>
        							<input type="text" class="form-control mqualification" name="mqualification" placeholder="Enter Qualification" >
        						</div>
        					    <div class="col-md-6">
        							<label class="col-form-label">Password <span class="star">*</span></label>
        							<input type="password" class="form-control mpass" name="mpass" placeholder="Password" required>
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">Confirm Password <span class="star">*</span></label>
        							<input type="password" class="form-control mcnfrmpass" name="mcnfrmpass" placeholder="Confirm Password" required>
        						</div>
					        </div>
					    </div>
						<div class="form-group row m-b-10 m-t-10">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary membercreate">SAVE</button>
							</div>
						</div>
					</form>
				</div>
			
			</div>
			
		</div>

		
	</div>
</div>

