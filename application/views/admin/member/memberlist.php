<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Member</a></li>
		<li class="breadcrumb-item active">list</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">LIST ALL MEMBER</h1>
	<!-- begin page-header -->
	<!-- end page-header -->
	
	
	
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Member List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="memberlist" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th width="1%">Sr No.</th>
								<th>Member Name</th>
								<th>Member Type</th>
								<th>Member Email</th>
								<th>Status</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-6 -->
		
	</div>
</div>
		<!-- end #content -->
<div class="modal fade" id="editmember">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">EDIT MEMBER</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="editmemberform">
				<div class="modal-body">
				    <div class="col-md-12">
    					<input type='hidden' class='memberid' name="memberid" />
    					<div class="form-group row m-b-15">
    						<div class="col-md-6">
        						<label class="col-form-label">Member Type <span class="star">*</span></label>
        						<select class="form-control mtype" name="mtype" data-style="btn-white" required>
                                    <?php foreach ($membertype as $membertypes) {
    if ($membertypes->USER_ROLE_ID!=3) { ?>
                                        <option value="<?php echo $membertypes->USER_ROLE_ID; ?>"><?php echo $membertypes->USER_ROLE_NAME; ?></option>
                                    <?php }
} ?>
        						</select>
        					</div>
                            <div class="col-md-6">
        						<label class="col-form-label">Member First Name <span class="star">*</span></label>
        						<input type="text" class="form-control mfname" name="mfname" placeholder="Enter First Name" required>
        					</div>
        					<div class="col-md-6">
        						<label class="col-form-label">Member Last Name <span class="star">*</span></label>
        						<input type="text" class="form-control mlname" name="mlname" placeholder="Enter First Name" required>
        					</div>
        					<div class="col-md-6">
        						<label class="col-form-label">Member Father Name </label>
        						<input type="text" class="form-control mfathername" name="mfathername" placeholder="Enter First Name" >
        					</div>
        					<div class="col-md-6">
        						<label class="col-form-label">Member Address </label>
        						<input type="text" class="form-control maddress" name="maddress" placeholder="Enter Addresse">
        					</div>
        					<div class="col-md-6">
        						<label class="col-form-label">Member Contact </label>
        						<input type="text" class="form-control mcontact input_num" name="mcontact" placeholder="Enter Contact" >
        					</div>
        					<div class="col-md-6">
        						<label class="col-form-label">Qualification </label>
        						<input type="text" class="form-control mqualification" name="mqualification" placeholder="Enter Qualification" >
        					</div>
        					<div class="col-md-6">
    							<label class="col-form-label">Status <span class="star">*</span></label>
    							<select class="form-control mstatus" name="mstatus" data-style="btn-white" required>
                                    <option value="1">Active</option>
                                    <option value="1">Inactive</option>
    							</select>
        					</div>
    					</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success editmemberform">Update</a>
				</div>
			</form>
		</div>
	</div>
</div>