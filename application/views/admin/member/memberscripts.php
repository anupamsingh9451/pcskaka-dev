<script>
    $('document').ready(function(){
        listmember();
        $('body').on('click','.memberedit',function(){
			$('#editmemberform').find('.mtype').val($(this).attr('attr-type'));
			$('#editmemberform').find('.mfname').val($(this).attr('attr-fname'));
			$('#editmemberform').find('.mlname').val($(this).attr('attr-lname'));
			$('#editmemberform').find('.mfathername').val($(this).attr('attr-fathername'));
			$('#editmemberform').find('.maddress').val($(this).attr('attr-address'));
			$('#editmemberform').find('.mcontact').val($(this).attr('attr-contact'));
			$('#editmemberform').find('.mqualification').val($(this).attr('attr-qualification'));
			$('#editmemberform').find('.mstatus').val($(this).attr('attr-status'));
			$('#editmemberform').find('.memberid').val($(this).attr('attr-id'));
			$('#editmember').modal('show');
		});
        $('body').on('keyup','.mpass, .mcnfrmpass', function () {
            var cnfrmpass = $('.mcnfrmpass').val();
            var password = $('.mpass').val();
            if(password!='' && cnfrmpass!=''){
                if (password != cnfrmpass){
                    $('.mcnfrmpass').css({ "border": "1px solid #d88080"});
                    $('.membercreate').attr("disabled", "disabled")
                }
                if (password == cnfrmpass){
                    $('.mcnfrmpass').css({ "border": "1px solid #d3d8de"});
                       $('.membercreate').attr("disabled", false);
                }
            }
            
        });
    });
    function listmember(){
		$('#memberlist').dataTable().fnDestroy();
		$('#memberlist').DataTable({
			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"ajax":{
				"url":base_loc + 'admingetajax/listmember',
				"type": "GET",
				"data": { loginid: loginid},
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"NAME"},
				{"data":"USER_ROLE_NAME"},
				{"data":"USER_EMAIL"},
				{"data":"USER_STATUS"},
				{"data":"TOOLS"},
			]
		});
	}
</script>