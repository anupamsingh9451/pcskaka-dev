<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Slider</a></li>
		<li class="breadcrumb-item active">list</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">LIST ALL SLIDER</h1>
	<!-- begin page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Book List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="sliderlist" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
							    <th width="1%">#</th>
								<th>Slider Name</th>
								<th>Product Type</th>
								<th>Status</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-6 -->
		
	</div>
</div>
<!-- end #content -->
<div class="modal fade edit-slider-modal" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">EDIT SLIDER</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="editslider">
				<div class="modal-body">
					<input type='hidden' class='sliderid' name="sliderid" value='' />
					<div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3" for="slidername">Slider Name <span class="star">*</span></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control m-b-5 slidername" id="slidername" name="slidername" placeholder="Enter Slider Name" required>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3" for="prodtype">Product Type <span class="star">*</span></label>
                        <div class="col-md-9">
                            <select name="prodtype" id="prodtype" class="form-control prodtype">
								<option value="">Select Product</option>
								<option value="1">Home</option>
								<option value="2">Courses</option>
								<option value="3">Test Series</option>
								<option value="4">Exam</option>
								<option value="5">Test Paper</option>
								<option value="6">Practice Test</option>	
                            </select>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3" for="status">Status <span class="star">*</span></label>
                        <div class="col-md-9">
                            <select name="status" id="status" class="form-control status">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-row m-b-15 image-section">
                        <div class="col row">
							<label class="col-form-label col-md" for="course_img">Course Image </label>
							<div class="col-md">
								<span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
									<i class="fas fa-plus"></i>
									<span>Add files...</span>
								</span>
								<input type="file" name="slider_img" id="course_img" accept="image/*">
							</div>
						</div>
						<div class="col text-right">
							<a href="javascript:;" target="_blank" ><img src="javascript" alt=""></a>
						</div>
                    </div>    
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success editslider">Update</a>
				</div>
			</form>
		</div>
	</div>
</div>