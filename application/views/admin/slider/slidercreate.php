<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Course</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Create</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Slider</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class='col-md-6'>
			<!-- begin panel -->
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">NEW SLIDER</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<form id="slidercreate" enctype="multipart/form-data">
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Slider Name <span class="star">*</span></label>
							<div class="col-md-9">
								<input type="text" class="form-control m-b-5 slidername" name="slidername" placeholder="Enter Slider Name" required>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3" id="prodtype">Product Type <span class="star">*</span></label>
							<div class="col-md-9">
                                <select name="prodtype" id="prodtype" class="form-control">
                                    <option value="">Select Product</option>
                                    <option value="1">Home</option>
									<option value="2">Courses</option>
									<option value="3">Test Series</option>
									<option value="4">Exam</option>
									<option value="5">Test Paper</option>
									<option value="6">Practice Test</option>	
                                </select>
							</div>
						</div>
						<div class="form-group form-row m-b-15 image-section">
							<div class="col row">
								<label class="col-form-label col-md" for="slider_img">Slider Image </label>
								<div class="col-md">
									<span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
										<i class="fas fa-plus"></i>
										<span>Add Slider</span>
									</span>
									<input type="file" name="slider_img" id="slider_img" accept="image/*">
								</div>
							</div>
							<div class="col text-right">
                                <a href="javascript:;" target="_blank" ><img src="javascript" alt=""></a>
                            </div>
						</div>
						<div class="form-group row m-b-15">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary slidercreate">SAVE</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>		
	</div>
</div>

