<script>
    $(document).ready(function () {
        var slidertable=$('#sliderlist').DataTable({
            "dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
            "pageLength": '10',
            "lengthMenu": [[10, 25, 50, 100, 1000, 2000, 5000], [10, 25, 50, 100, 1000, 2000, 5000]],
            "buttons": [
                'excel', 'pdf', 'print'
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url":base_loc + 'admingetajax/listslider',
                "type": "GET",
                "data":{"loginid":loginid},
                "headers": {
                    'Client-Service': clientserv,
                    'Auth-Key': apikey,
                    'User-ID': loginid,
                    'Authorization': token,
                    'type': type
                },
                "error": function (msg) {
                    if (msg.responseJSON['status'] == 303) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 401) {
                        location.href = base_loc; 
                    }
                    if (msg.responseJSON['status'] == 400) {
                        location.href = base_loc; 
                    }
                }
            },
            "drawCallback": function (response) { 
                // Here the response
                    responseData = response.json.aaData;
            },
            "columns":[
                {"data":"SR_NO"},
                {"data":"SLIDER_NAME"},
                {"data":"PROD_TYPE_NAME"},
                {"data":"SLIDER_STATUS_A"},
                {"data":"TOOLS"},
            ]
        });  
        $('body').on('click','.edit-slider', function () {
            let rowId = $(this).attr('data-id');
            let row = responseData.find(item => item.ID == rowId);
            modal=$('.edit-slider-modal');
            modal.find('.modal-title').html('EDIT SLIDER : '+row.SLIDER_NAME);
            form=modal.find('form');
            form.trigger("reset");
            form.find('.sliderid').val(row.ID);
            form.find('#slidername').val(row.SLIDER_NAME);
            form.find('#prodtype').val(row.PROD_TYPE);
            form.find('#status').val(row.SLIDER_STATUS);
            if(row.SLIDER_IMAGE!=""){
                form.find('img').attr({'src':row.SLIDER_IMAGE}).css({ 'max-height': '80px', 'max-width': '120px' });
                form.find('img').parent('a').attr('href',row.SLIDER_IMAGE);
            }else{
                form.find('img').attr({'src':''}).css({ 'max-height': '0px', 'max-width': '0px' });
                form.find('img').parent('a').attr('href','javascript:;');
            }
        });  
    });
    function table_reload(){
        $('#sliderlist').DataTable().ajax.reload();
    }
</script>