<script>
	$('document').ready(function(){
	    $('#datetimepicker2').datetimepicker({
            format: 'HH:mm',
            
		});
		getlibrary();
		/*$('body').on('change','.courseid', function () {
			if($(this).val()==""|| $(this).val()=="undefined"){
				$('.book,.subject,.library').html('');
			}else{
				getlibrary();	
			}
		});	*/	

		$('body').on('change','.library',function(){
			if($(this).val()==""|| $(this).val()=="undefined"){
				$('.book,.subject').html('');
			}
		});
		$('body').on('change','.subject',function(){
			if($(this).val()==""|| $(this).val()=="undefined"){
				$('.book').html('');
			}
		});
		$('body').on('click','.testseriesedit',function(){
            var tsid = $(this).attr('attr-tsid');
            $.ajax({
        		type: 'GET',
        		url: base_loc + 'admingetajax/get_practicetest_edit_modalhtml',
        		data: 'tsid='+tsid,
        		async:false,
        		headers: {
        			'Client-Service': clientserv,
        			'Auth-Key': apikey,
        			'User-ID': loginid,
        			'Authorization': token,
        			'type': type
        		},
        		success: function (msg) {
        			$('.edittestseries_modal_div').html(msg);
        			$('#edittestseries').modal('show');
        		},
        		error: function (msg) {
        			if (msg.responseJSON['status'] == 303) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 401) {
        				location.href = base_loc; 
        			}
        			if (msg.responseJSON['status'] == 400) {
        				location.href = base_loc; 
        			}
        		}
        	});
		});
		testserieslists();
		/*Shubham 2020-06-13*/
		$('body').on('change','.book',function(){
				$('.avlquescount').val(0);
				get_remains_questions_count();
		});
		$('body').on('keydown','.quesno',function(){
				var quesno = $(this).val();
				var avlquescount= $(".avlquescount").val();
				if( parseInt(avlquescount) < parseInt(quesno) ){
					$(".quesno").val(quesno.substr(0, quesno.length - 1));
				}else{
					tspaperno = parseInt(avlquescount)/parseInt(quesno);
					if(isNaN(tspaperno)){
						$(".tspaperno").val(0);
					}else{
						$(".tspaperno").val(parseInt(tspaperno));
					}
				}
		});
		/**/
	});


	/*Shubham 2020-06-13*/
	function get_remains_questions_count() {
	    $.ajax({
	        type: 'GET',
	        url: base_loc + 'admingetajax/get_remains_questions_count?bookid=' + $(".book").val(),
	        headers: {
	            'Client-Service': clientserv,
	            'Auth-Key': apikey,
	            'User-ID': loginid,
	            'Authorization': token,
	            'type': type
	        },
	        success: function(msg) {
	            if (msg.status == 200) {
	               $(".avlquescount").val(msg.data);
	            } else {
	                location.href = base_loc;
	            }
	        },
	        error: function(msg) {
	            if (msg.responseJSON['status'] == 303) {
	                location.href = base_loc;
	            }
	            if (msg.responseJSON['status'] == 401) {
	                location.href = base_loc;
	            }
	            if (msg.responseJSON['status'] == 400) {
	                location.href = base_loc;
	            }
	        }
	    });
	}
	/**/
	
	function testserieslists(){
		$('#testserieslist').dataTable().fnDestroy();
		$('#testserieslist').DataTable({
			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"ajax":{
				"url":base_loc + 'admingetajax/practicetest_list',
				"type": "GET",
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"TS_NAME"},
				{"data":"TS_TEST_PAPER_NOS"},
				{"data":"TS_CREATED_AT"},
				{"data":"TS_STATUS"},
				{"data":"TOOLS"},
			]
		});
	}
</script>