<link href="<?php echo base_url(); ?>assets/adminassets/css/test_series/testserieseditmodal.css" rel="stylesheet" />
<input type='hidden' class='tsid' name="tsid" value='<?php echo $tsdt[0]->TS_ID; ?>' />
<div class="col-md-12">
    <div class="row">
        <!-- <div class="col-md-6" hidden="true">
            <label class="col-form-label">Course <span class="star">*</span></label>
            <select class="form-control m-b-5 courseid" name="course"  required>
                <option value="">--Select Course --</option>
                <?php $courses=get_all_courses();
                    if (!empty($courses)) {
                        foreach ($courses as $course) {
                            if ($course->COURSE_STATUS==1) {
                                if($course->COURSE_ID==$tsdt[0]->COURSE_ID){
                                    echo '<option value="'.$course->COURSE_ID.'" selected>'.$course->COURSE_NAME.' </option>';
                                }else{
                                    echo '<option value="'.$course->COURSE_ID.'">'.$course->COURSE_NAME.' </option>';
                                }
                                
                            }
                        }
                    }
                ?>
            </select>
        </div>
        <div class="col-md-6" hidden="true">
            <label class="col-form-label">Library <span class="star">*</span></label>
            <select class="form-control library" name="library" data-style="btn-white" required>
                <option value="">-- Select Library--</option>
                <?php $libraries = $this->Admingetmodel->get_all_library($tsdt[0]->COURSE_ID)['data'];
                    if(!empty($libraries)){
                        foreach ($libraries as $library) {
                            if($library->LIB_ID==$tsdt[0]->LIBRARY_ID){
                                echo '<option value="'.$library->LIB_ID.'"  selected>'.$library->LIB_NAME.'</option>';    
                            }else{
                                echo '<option value="'.$library->LIB_ID.'"  >'.$library->LIB_NAME.'</option>';    
                            }
                        }
                    }
                ?>    
            </select>
        </div>
        <div class="col-md-6" hidden="true">
            <label class="col-form-label">Subject <span class="star">*</span></label>
            <select class="form-control subject" name="subject" data-style="btn-white" required>
                <option value="">-- Select Subjects--</option>
                <?php $subjects = $this->Admingetmodel->get_subject_by_libid($tsdt[0]->LIBRARY_ID)['data'];
                    if(!empty($subjects)){
                        foreach ($subjects as $subject) {
                            if($subject->SUB_ID==$tsdt[0]->SUBJECT_ID){
                                echo '<option value="'.$subject->SUB_ID.'"  selected>'.$subject->SUB_NAME.'</option>';    
                            }else{
                                echo '<option value="'.$subject->SUB_ID.'"  >'.$subject->SUB_NAME.'</option>';    
                            }
                        }
                    }
                ?>   
            </select>
        </div>
        <div class="col-md-6" hidden="true">
            <label class="col-form-label">Book <span class="star">*</span></label>
            <select class="form-control book" name="book" data-style="btn-white" required>
                <option value="">-- Select Books--</option>
                <?php $books = $this->Admingetmodel->get_book_by_subid($tsdt[0]->SUBJECT_ID)['data'];
                    if(!empty($books)){
                        foreach ($books as $book) {
                            if($book->BOOK_ID==$tsdt[0]->BOOK_ID){
                                echo '<option value="'.$book->BOOK_ID.'"  selected>'.$book->BOOK_NAME.'</option>';    
                            }else{
                                echo '<option value="'.$book->BOOK_ID.'"  >'.$book->BOOK_NAME.'</option>';    
                            }
                        }
                    }
                ?>   
            </select>
        </div> -->
        <div class="col-md-6">
            <label class="col-form-label">Test Set Name<span class="star">*</span></label>
            <input type="text" class="form-control testseriesname" name="testseriesname" placeholder="Enter Test Set Name" required="" value="<?php echo $tsdt[0]->TS_NAME; ?>">
        </div>
        <!-- <div class="col-md-6" hidden="true">
            <label class="col-form-label">Cost <span class="star">*</span></label>
            <input type="text" class="form-control cost input_num" name="cost" placeholder="Enter Test Series Cost" value="<?php echo $tsdt[0]->TS_COST; ?>" required="">
        </div>
        <div class="col-md-6" hidden="true">
            <label class="col-form-label">No. of Test paper <span class="star">*</span></label>
            <input type="text" class="form-control input_num tspaperno" name="tspaperno" placeholder="Enter No. of Test Paper" value="<?php echo $tsdt[0]->TS_TEST_PAPER_NOS; ?>" required="">
        </div>
        <div class="col-md-6" hidden="true">
            <label class="col-form-label">No. of Questions <span class="star">*</span></label>
            <input type="text" class="form-control quesno input_num" name="quesno" placeholder="Enter No. of Questions" value="<?php echo $tsdt[0]->TS_QUESTION_NOS; ?>" required="">
        </div>
        <div class="col-md-6" hidden="true">
        <label class="col-form-label">Positive Mark <span class="star">*</span></label>
        <input type="text" class="form-control input_num pmark" name="pmark" placeholder="Enter Positive Mark" value="<?php echo $tsdt[0]->TS_P_MARKS; ?>" required>
        </div>
        <div class="col-md-6" hidden="true">
        <label class="col-form-label">Negative Mark <span class="star">*</span></label>
        <input type="text" class="form-control input_num nmark" name="nmark" placeholder="Enter Negative Mark" value="<?php echo $tsdt[0]->TS_N_MARKS; ?>" required>
        </div>
        <div class="col-md-6" hidden="true">
            <label class="col-form-label">Start Date <span class="star">*</span></label>
             <input type="text" class="form-control dtpicker startdate" name="startdate" value="<?php echo date('Y-m-d',strtotime($tsdt[0]->TS_START_DATE)); ?>" required="">
        </div>
        <div class="col-md-6" hidden="true">
            <label class="col-form-label">End Date <span class="star">*</span></label>
             <input type="text" class="form-control dtpicker enddate" name="enddate" value="<?php echo date('Y-m-d',strtotime($tsdt[0]->TS_END_DATE)); ?>" required="">
        </div> -->
        <div class="col-md-6">
            <label class="col-form-label">Duration <span class="star"> Enter Duration in Minutes *</span></label>
            <input type="text" class="form-control input_num duration" name="duration" value="<?php echo $tsdt[0]->TS_DURATION; ?>" required>
        </div>
        <div class="col-md-6">
            <label class="col-form-label">Status <span class="star">*</span></label>
             <select class="form-control tsstatus" name="tsstatus" data-style="btn-white" required="">
                <option value="1" <?php if($tsdt[0]->TS_STATUS==1){ echo 'selected'; } ?> >Active</option>
                <option value="0" <?php if($tsdt[0]->TS_STATUS==0){ echo 'selected'; } ?> >Inactive</option>
            </select>
        </div>
        <div class="col-md-12 image-section">
            <div class=" row ">
                <div class="col-form-label">
                    <label class="" for="practicetest_img">Practice Test Image </label>
                    <div class="">
                        <span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
                            <i class="fas fa-plus"></i>
                            <span>Add files...</span>
                        </span>
                        <input type="file" name="practicetest_img" id="practicetest_img" class="hide" accept="image/*">
                    </div>
                </div>
                <div class="col text-right mt-2">
                    <a href="javascript:;" target="_blank" ><img src="<?php echo $tsdt[0]->TS_IMG; ?>" alt=""></a>
                </div>
            </div>    
        </div>
        <div class="col-md-12 image-section">
            <div class=" row ">
                <div class="col-form-label">
                    <label class="" for="practicetestbanner_img">Practice Banner Image </label>
                    <div class="">
                        <span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
                            <i class="fas fa-plus"></i>
                            <span>Add files...</span>
                        </span>
                        <input type="file" name="practicetestbanner_img" id="practicetestbanner_img" class="hide" accept="image/*">
                    </div>
                </div>
                <div class="col text-right mt-2">
                    <a href="javascript:;" target="_blank" ><img src="<?php echo $tsdt[0]->BANNER_IMG; ?>" alt=""></a>
                </div>
            </div>    
        </div>
        <div class="col-md-12">
            <label class="col-form-label">Description <span class="star">*</span></label>
            <textarea class="summernote desc" name="desc" id="mysummernote" ><?php echo $tsdt[0]->TS_DESCRIPTION; ?></textarea>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        App.init();
        FormSummernote.init();
        $('img[src!=""]').css({'max-width':'120px','max-height':'80px'});
        $('.fileinput-button').on('click', function(e) {
            e.preventDefault();
            $(this).parent().find('input[type=file]').trigger('click');
        });
    });
</script>