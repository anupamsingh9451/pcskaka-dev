<link href="<?php echo base_url(); ?>assets/adminassets/css/test_series/testseriescreate.css" rel="stylesheet" />
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Test Set</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Create</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Factory / Create</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class='col-md-8'>
			<!-- begin panel -->
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">NEW TEST SET</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<form id="practicetestcreate" enctype="multipart/form-data" method="post">
					    <div class="col-md-12">
					        <div class="row">
								<!-- <div class="col-md-6">
									<label class="col-form-label">Course <span class="star">*</span></label>
									<select class="form-control m-b-5 courseid" name="course"  required>
										<option value="">--Select Course --</option>
										<?php /*$courses=get_all_courses();
											if (!empty($courses)) {
												foreach ($courses as $course) {
													if ($course->COURSE_STATUS==1) {
														echo '<option value="'.$course->COURSE_ID.'">'.$course->COURSE_NAME.' </option>';
													}
												}
											}
										*/?>
									</select>
								</div> -->
								<div class="col-md-6">
									<label class="col-form-label">Library <span class="star">*</span></label>
									<select class="form-control library" name="library" data-style="btn-white" required>

									</select>
								</div>
								<div class="col-md-6">
									<label class="col-form-label">Subject <span class="star">*</span></label>
									<select class="form-control subject" name="subject" data-style="btn-white" required>

									</select>
								</div>
								<div class="col-md-6">
									<label class="col-form-label">Book <span class="star">*</span></label>
									<select class="form-control book" name="book" data-style="btn-white" required>

									</select>
								</div>
								<div class="col-md-6">
        							<label class="col-form-label">Available Questions In Selected Book<span class="star">*</span></label>
        							<input type="text" class="form-control input_num border border-primary avlquescount" name="avlquescount" placeholder="" value="0" readonly="true" required>
        						</div>
                                <div class="col-md-6">
        							<label class="col-form-label">Test Set Name <span class="star">*</span></label>
        							<input type="text" class="form-control testseriesname" name="testseriesname" placeholder="Enter Test Set Name" required>
        						</div>
        						<div class="col-md-6" hidden="true">
        							<label class="col-form-label">Cost <span class="star">*</span></label>
        							<input type="text" class="form-control cost input_num" name="cost" value="0" placeholder="Enter Test Series Cost" required>
        						</div> 
        						
        						<div class="col-md-6" hidden="true">
        							<label class="col-form-label">No. of Test paper <span class="star">*</span></label>
        							<input type="text" class="form-control input_num tspaperno" name="tspaperno" placeholder="Enter No. of Test Paper" required readonly="true" value="0">
        						</div>
        						<div class="col-md-6">
        							<label class="col-form-label">No. Of Questions In Pratice Test<span class="star">*</span></label>
        							<input type="text" class="form-control quesno input_num" name="quesno" placeholder="Enter No. Of Questions In Pratice Test" required>
        						</div>
        						<div class="col-md-6" hidden="true">
                    				<label class="col-form-label">Positive Mark <span class="star">*</span></label>
                    				<input type="text" class="form-control input_num pmark" name="pmark" placeholder="Enter Positive Mark" required value="1">
                    		    </div>
                    		    <div class="col-md-6" hidden="true">
                    				<label class="col-form-label">Negative Mark <span class="star">*</span></label>
                    				<input type="text" class="form-control input_num nmark" name="nmark" placeholder="Enter Negative Mark" required value="0">
                    		    </div>
        						<div class="col-md-6" hidden="true">
            						<label class="col-form-label">Start Date <span class="star">*</span></label>
            						 <input type="text" class="form-control dtpicker" name="startdate" value="<?php echo date('1971-01-01')?>" required>
        						</div>
        						<div class="col-md-6" hidden="true">
            						<label class="col-form-label">End Date <span class="star">*</span></label>
            						 <input type="text" class="form-control dtpicker" name="enddate" value="<?php echo date('2100-01-01')?>" required>
        						</div>
        						<!--<div class="col-md-6">-->
            		<!--				<label class="col-form-label">Validity <span class="star">*</span></label>-->
            		<!--				 <input type="text" class="form-control dtpicker" name="validity" value="" required>-->
        						<!--</div>-->
        						<div class="col-md-6 hide">
            						<label class="col-form-label">Created Date <span class="star">*</span></label>
            						 <input type="text" class="form-control dtpicker" name="createdate" value="<?php echo date('Y-m-d')?>" required>
        						</div>
        		<!--				<div class='col-sm-6'>-->
          <!--                          <label class="col-form-label">Duration </label>-->
          <!--                          <div class='input-group date' id='datetimepicker2'>-->
          <!--                              <input type='text' class="form-control duration" value="" name="duration" />-->
          <!--                              <span class="input-group-addon">-->
										<!--    <i class="fa fa-clock"></i>-->
										<!--</span>-->
          <!--                          </div>-->
          <!--                      </div>-->
                                <div class="col-md-6">
                    				<label class="col-form-label">Duration <span class="star"> Enter Duration in Minutes *</span></label>
                    				<input type="text" class="form-control input_num duration" name="duration" required >
                    		    </div>
								<div class="col-md-12 image-section">
									<div class=" row ">
										<div class="col-form-label">
											<label class="" for="practicetest_img">Test Set Image<span class="star">*</span></label>
											<div class="">
												<span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
													<i class="fas fa-plus"></i>
													<span>Add files...</span>
												</span>
												<input type="file" name="practicetest_img" id="practicetest_img" class="hide" accept="image/*">
											</div>
										</div>
										<div class="col text-right mt-2">
											<a href="javascript:;" target="_blank" ><img src="" alt=""></a>
										</div>
									</div>    
								</div>
								<div class="col-md-12 image-section">
									<div class=" row ">
										<div class="col-form-label">
											<label class="" for="practicetestbanner_img">Test Set Banner Image <span class="star">*</span></label>
											<div class="">
												<span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
													<i class="fas fa-plus"></i>
													<span>Add files...</span>
												</span>
												<input type="file" name="practicetestbanner_img" id="practicetestbanner_img" class="hide" accept="image/*">
											</div>
										</div>
										<div class="col text-right mt-2">
											<a href="javascript:;" target="_blank" ><img src="" alt=""></a>
										</div>
									</div>    
								</div>
        						<div class="col-md-12">
        							<label class="col-form-label">Description <span class="star">*</span></label>
                                    <textarea class="summernote desc" name="desc" id="mysummernote"></textarea>
            					</div>
					        </div>
					    </div>
						<div class="form-group row m-b-10 m-t-10">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary practicetestcreate">SAVE</button>
							</div>
						</div>
					</form>
				</div>
			
			</div>
			
		</div>

		
	</div>
</div>

<script>
	$(document).ready(function() {
		FormSummernote.init();
	});
</script>