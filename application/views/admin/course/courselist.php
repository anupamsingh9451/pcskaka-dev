<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Course</a></li>
		<li class="breadcrumb-item active">list</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">LIST ALL COURSE</h1>
	<!-- begin page-header -->
	<!-- end page-header -->
	
	
	
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">LiCoursebrary List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="courselist" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th width="1%">Sr No.</th>
								<th>Course Name</th>
								<th>Status</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-6 -->
		
	</div>
</div>
		<!-- end #content -->
<div class="modal fade" id="editcourse">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">EDIT <span class='librarynm'></span> COURSE</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="editcourseform">
				<div class="modal-body">
					<input type='hidden' class='courseid' name="courseid" value='' />
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Course Name<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 coursenm" name="coursename" placeholder="Enter Course name here" required>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Course Desc </label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 coursedesc " name="coursedesc" placeholder="Enter Library Cost here" >
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Status<span class="star">*</span></label>
						<div class="col-md-9">
							<select name="coursestatus" class='form-control coursestatus'>
								<option value='1'>Active</option>
								<option value='0'>Inactive</option>
							</select>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3" for="popular">Is popular? <span class="star">*</span></label>
						<div class="col-md-9" >
							<select name="popular" id="popular"  class="form-control">
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3" for="coursecategory">Course Category <span class="star">*</span></label>
						<div class="col-md-9">
							<select name="coursecategory" id="coursecategory" class="form-control">
								<option value=""> Select Course Category</option>
								<?php $cc=get_all_active_course_category(); 
									print_r($cc);
									if(!empty($cc)){
										foreach ($cc as $course_category) {
											echo '<option value="'.$course_category->CC_ID.'" >'.$course_category->CC_NAME.'</option>';
										}
									}

								?>
							</select>
						</div>
					</div>
					<div class="form-group form-row m-b-15 image-section">
						<div class="col row">
							<label class="col-form-label col-md" for="course_img">Course Image </label>
							<div class="col-md">
								<span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
									<i class="fas fa-plus"></i>
									<span>Add files...</span>
								</span>
								<input type="file" name="course_img" id="course_img" accept="image/*">
							</div>
						</div>
						<div class="col text-right">
							<a href="javascript:;" target="_blank" ><img src="javascript" alt=""></a>
						</div>
					</div>
					
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success editcourseform">Update</a>
				</div>
			</form>
		</div>
	</div>
</div>



<div class="modal fade" id="course_content">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">COURSE<span class='librarynm'></span> CONTENT</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="course_content_form"  enctype="multipart/form-data">
				<div class="modal-body">
					<input type='hidden' class='courseid' name="courseid" value='' />
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Course Amt<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 courseamt" name="courseamt" placeholder="Enter Course Amount here" required>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Course Content Disc </label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 coursedisc " name="coursedisc" placeholder="Enter Course Discount here" >
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Course Content<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="hidden" class='coursecontent' name="coursecontent" value="">
							<div id="jstree" style="overflow-x: scroll;"></div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success course_content_form">Update</a>
				</div>
			</form>
		</div>
	</div>
</div>