<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Course</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Create</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Course</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class='col-md-6'>
			<!-- begin panel -->
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">NEW COURSE</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<form id="coursecreate" enctype="multipart/form-data">
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Course Name <span class="star">*</span></label>
							<div class="col-md-9">
								<input type="text" class="form-control m-b-5 coursename" name="coursename" placeholder="Enter Course Name" required>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Course Description </label>
							<div class="col-md-9">
                                <input type="text" class="form-control m-b-5 coursedesc" name="coursedesc" placeholder="Enter Course Description" >
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3" for="popular">Is popular? <span class="star">*</span></label>
							<div class="col-md-9" >
                                <select name="popular" id="popular"  class="form-control">
									<option value="1">Yes</option>
									<option value="0">No</option>
								</select>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3" for="coursecategory">Course Category <span class="star">*</span></label>
							<div class="col-md-9">
                                <select name="coursecategory" id="coursecategory" class="form-control">
									<option value=""> Select Course Category</option>
									<?php $cc=get_all_active_course_category(); 
										print_r($cc);
										if(!empty($cc)){
											foreach ($cc as $course_category) {
												echo '<option value="'.$course_category->CC_ID.'" >'.$course_category->CC_NAME.'</option>';
											}
										}

									?>
								</select>
							</div>
						</div>
						<div class="form-group form-row m-b-15 image-section">
							<div class="col row">
								<label class="col-form-label col-md" for="course_img">Course Image </label>
								<div class="col-md">
									<span class="btn btn-primary text-nowrap fileinput-button btn-sm m-r-3 m-b-3">
										<i class="fas fa-plus"></i>
										<span>Add files...</span>
									</span>
									<input type="file" name="course_img" id="course_img" accept="image/*">
								</div>
							</div>
							<div class="col text-right">
								<a href="javascript:;" target="_blank" ><img src="javascript" alt=""></a>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary coursecreate">SAVE</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>		
	</div>
</div>

