<script>
	$('document').ready(function(){
		/* for edit company detail*/
		$('body').on('click','.courseedit',function(){
			$('#editcourse').find('.coursenm').val($(this).attr('attr-coursenm'));
			$('#editcourse').find('.coursestatus').val($(this).attr('attr-status'));
			$('#editcourse').find('.courseid').val($(this).attr('attr-courseid'));
			$('#editcourse').find('.coursedesc').val($(this).attr('attr-desc'));
			$('#editcourse').find('#popular').val($(this).attr('attr-popular'));
			$('#editcourse').find('#coursecategory').val($(this).attr('attr-cc'));
			if($(this).attr('attr-img')!=""){
                $('#editcourse').find('img').attr({'src':$(this).attr('attr-img')}).css({ 'max-height': '80px', 'max-width': '120px' });
                $('#editcourse').find('img').parent('a').attr('href',$(this).attr('attr-img'));
            }else{
                $('#editcourse').find('img').attr({'src':''}).css({ 'max-height': '0px', 'max-width': '0px' });
                $('#editcourse').find('img').parent('a').attr('href','javascript:;');
            }
			$('#editcourse').modal('show');
		});
		// to get dcn all areas predifined
		var autocompletearea = [];
	
		courselist();


		/*Course Content*/
		$('body').on('click','.course_content',function(){
			course_content($(this).attr('attr-courseid'));
		});

	});
	
	
	function courselist(){
		$('#courselist').dataTable().fnDestroy();
		$('#courselist').DataTable({
			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"ajax":{
				"url":base_loc + 'admingetajax/listcourse',
				"type": "GET",
				"data": { loginid: loginid},
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"NAME"},
				{"data":"COURSE_STATUS"},
				{"data":"TOOLS"},
			]
		});
	}

	function course_content(courseid){

		 $.ajax({
            type: 'GET',
            url: base_loc + 'admingetajax/course_content',
            data: {loginid: loginid,courseid:courseid},
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                console.log("before");
            },
            success: function(msg) {
            	$('#course_content').find('.courseamt').val(msg.data.course_amt);
				$('#course_content').find('.courseid').val(msg.data.course_id);
				$('#course_content').find('.coursedisc').val(msg.data.course_content_disc);
            	$('#jstree').jstree("destroy");

				   $('#jstree').jstree({
				    "core" : {
				      "themes" : {
				        "variant" : "medium"
				      },
				      "data" : msg.data.course_content
				    },
				    "checkbox" : {
				      "keep_selected_style" : true
				    },
				    "plugins" : [ "wholerow", "checkbox" ]
				  });
				    
				    $('#jstree').on("changed.jstree", function (e, data) {
				    	console.log(e);
				      console.log(data.selected);
				      $('#course_content').find('.coursecontent').val(data.selected);
				    });

            	 $('#course_content').modal('show');
                console.log(msg);
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });

	}


</script>