<!-- begin #content -->
<link href="<?php echo base_url(); ?>assets/subadminassets/css/question/view_ques.css" rel="stylesheet" />
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Question</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">View</a></li>
		
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Question</h1>
	<!-- end page-header -->
	<div class="row no_mrgn addbx_top_rowhead pd_15">
		<div class="col-md-8">
		    
			<div class="row">
			    <h5 class="">Subject Name &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php echo $quesdt[0]->SUB_NAME; ?></span>
			</div>
			<div class="row">
			    <h5 class="">Book Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $quesdt[0]->BOOK_NAME; ?></span>
	        </div>
	        <div class="row">
			    <h5 class="">Chapter Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $quesdt[0]->CHAPTER_NAME; ?></span>
	        </div>
		</div>
		
		<div class="col-md-4 text-right">
		    <div class="row">
		        <div class="col-md-12">
		           
		            <a class="btn btn-primary" href="<?php echo base_url().'subadmin/ques_user/question'; ?>"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
		            <a href="<?php echo base_url().'subadmin/question/edit/'.$this->uri->segment(4); ?>">
        			    <button type="button" class="btn btn-warning" ><i class="fa fa-fw fa-hdd"></i> Edit Question</button>
        			</a>
		        </div>
		    </div>
		    <?php if($quesdt[0]->QUES_APPROVED_BY==0){ ?>
    		    <div class="row m-t-5">
    		        <div class="col-md-12">
    		            <a href="javascript:void(0)" class="approve_ques" attr-quesid="<?php echo $this->uri->segment(4);?>">
            			    <button type="button" class="btn btn-success" ><i class="fa fa-fw fa-hdd"></i> Approv</button>
            			</a>
    		        </div>
    		    </div>
    		   <?php } ?>
		</div>
	    
	</div>
	<!-- begin row -->
	<div class="row">
		
		
        
        
        <div class="col-lg-12">
			<div class="panel">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Question Details</h4>
				</div>
			    <div class="panel-body">
			        <div class="row">
			            <div class="col-md-12">
			                <div class="card" style="border: 1px solid rgba(0,0,0,.2);">
        						<div class="card-block"> 
        						    <?php if($quesdt[0]->QUES_TYPE==2){ ?>
        							    <p class="card-text ques_font"><?php echo json_decode($quesdt[0]->QUES)->QUESTION; ?> </p>
        							  <?php }else{?>
        							       <p class="card-text ques_font"><?php echo $quesdt[0]->QUES; ?> </p>
        							  <?php } ?>
        						    <div class="row">
        						        <?php if($quesdt[0]->QUES_TYPE==2){ ?>
            						        <div class="col-md-6">
            						            <?php $azRange = range('A', 'Z'); $i=0; $option = json_decode($quesdt[0]->QUES)->OPTION; foreach($option as $options){ ?>
            						            <div class="qa_progress" <?php if(in_array($i+1,explode(',',$quesdt[0]->QUES_ANSWERS))){ echo 'style="width:100%;border: #4caace solid 1px;" '; }else{ echo 'style="width:100%;" '; } ?> style="width:100%">
                                                    <div class="qa_progress_text">
                                                        <ul>
                                                            <li class="qa_number" <?php if(in_array($i+1,explode(',',$quesdt[0]->QUES_ANSWERS))){ echo 'style="background: #4caace;color: #fff;" '; } ?>><?php echo $azRange[$i]; ?></li>
                                                            <li class="qa_text" style="display: inline-block">
                                                                <p><?php echo $options; ?></p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <?php $i++; } ?>
            						        </div>
            						    <?php }else{?>
            						        <div class="col-md-6">
            						            <div class="qa_progress" style="width:100%;">
                                                    <div class="qa_progress_text">
                                                        <ul>
                                                            <li class="qa_text" style="display: inline-block">
                                                                <p><?php echo $quesdt[0]->QUES_ANSWERS; ?></p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
            						        </div>
            						    <?php } ?>
        						    </div>
        						    
        						</div>
        					</div>
			            </div>
			            
			            <div class="col-md-12">
			                <div class="card" style="border: 1px solid rgba(0,0,0,.2);">
        						<div class="card-block"> 
        							<div class="row no_mrgn addbx_top_rowhead pd_15">
                                		<div class="col-md-6">
                                			<div class="row">
                                			    <h5 class="">Created By &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php echo $quesdt[0]->USER_FNAME.' '.$quesdt[0]->USER_LNAME.' ('.$userrole[0]->USER_ROLE_NAME.')'; ?></span>
                                			</div>
                                			<div class="row">
                                			    <h5 class="">Positive Mark &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $quesdt[0]->QUES_P_MARKING; ?></span>
                                	        </div>
                                	        <div class="row">
                                			    <h5 class="">Question Duration &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo date('H:i',strtotime($quesdt[0]->QUES_DURATION)); ?></span>
                                	        </div>
                                	        <div class="row">
                                			    <h5 class="">Created Date &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo date('d-m-Y',strtotime($quesdt[0]->QUES_CRATED_AT)); ?></span>
                                	        </div>
                                	        
                                		</div>
                                		<div class="col-md-6">
                                			<div class="row">
                                			    <h5 class="">Approved By &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php if($quesdt[0]->QUES_APPROVED_BY!=0){$response = $this->Subadmingetmodel->get_user_role_by_id($quesdt[0]->QUES_APPROVED_BY); echo $response[0]->USER_FNAME.' '.$response[0]->USER_LNAME.' ('.$response[0]->USER_ROLE_NAME.')'; }else{ echo "Not Approved";} ?></span>
                                			</div>
                                			<div class="row">
                                			    <h5 class="">Negative Mark &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $quesdt[0]->QUES_N_MARKING; ?></span>
                                	        </div>
                                	        <div class="row">
                                			    <h5 class="">Difficulty Level &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php $response = $this->Subadmingetmodel->get_difficulty_level($quesdt[0]->QUES_DIFFICULTY_LEVEL); echo $response; ?></span>
                                	        </div>
                                	        <div class="row">
                                			    <h5 class="">Status &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class="" ><?php if($quesdt[0]->QUES_STATUS==1){ echo '<button type="button" class="btn btn-success pad_4">ACTIVE</button>';}else{ echo '<button type="button" class="btn btn-danger pad_4">INACTIVE</button>'; } ?></span>
                                	        </div>
                                		</div>
                                		<div class="col-md-12">
                                		    <?php if($quesdt[0]->QUES_HINT!=''){ ?>
                                    			<div class="row">
                                    			    <h5 class="">Question Hint &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php echo $quesdt[0]->QUES_HINT; ?></span>
                                    			</div>
                                    		<?php } ?>
                                    		<?php if($quesdt[0]->QUES_EXPLANATION!=''){ ?>
                                    			<div class="row">
                                    			    <h5 class="">Question Explanation &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $quesdt[0]->QUES_EXPLANATION; ?></span>
                                    	        </div>
                                	        <?php } ?>
                                		</div>

                                	</div>
        						</div>
        					</div>
			            </div>
			        </div>
				</div>
			</div>
		</div>
		
	</div>
</div>
