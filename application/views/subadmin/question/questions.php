<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item active"><a href="javascript:;">Question</a></li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Question</h1>
	<!-- end page-header -->
	<div class="row no_mrgn addbx_top_rowhead pd_15">
		<div class="col-md-8">
		    <div class="row">
		        
			    <h5 class="">Library Name &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; </h5><span class=""><?php echo $chapterdt[0]->LIB_NAME; ?></span>
			</div>
			<div class="row">
			    <h5 class="">Subject Name &nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  </h5><span class=""><?php echo $chapterdt[0]->SUB_NAME; ?></span>
			</div>
			<div class="row">
			    <h5 class="">Book Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $chapterdt[0]->BOOK_NAME; ?></span>
	        </div>
	        <div class="row">
			    <h5 class="">Chapter Name &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </span></h5><span class=""><?php echo $chapterdt[0]->CHAPTER_NAME; ?></span>
	        </div>
		</div>
		<div class="col-md-4 text-right">
    		<a class="text-right" href="<?php echo base_url().'subadmin/question/create/'.$this->uri->segment(3); ?>">
    			<button type="button" class="btn btn-warning"><i class="fa fa-fw fa-hdd"></i> Add Question</button>
    		</a>
    		<button type="button" class="btn btn-primary add_ques_modal" attr-chapterid="<?php echo $chapterdt[0]->CHAPTER_ID; ?>"><i class="fa fa-fw fa-hdd"></i> Add Question on Modal</button>
	    </div>
	</div>
	<!-- begin row -->
	<div class="row">
        <div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Question List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="question_table" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th width="1%">Sr No.</th>
								<th>Question</th>
								<th>Created By</th>
								<th>Created Date</th>
								<th width="9%">Edited By</th>
								<th width="9%">Approved By</th>
								<th>Level</th>
								<th>Status</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
	</div>
</div>
<!-- Add Question modal start -->
<div class="modal fade" id="addquestionmodal" role="dialog">
	<div class="modal-dialog modal-xl">
		<div class="modal-content" style="background-color:unset">
			<div class="modal-header" style="background-color: #348fe2 ;">
				<h4 class="modal-title" style="color:white">Add Question</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body bg_grey">
				<div class='col-md-12'>
        			<!-- begin panel -->
        			<div class="panel bg_grey">
        				<!-- begin panel-body -->
        				<div class="panel-body add_ques_modal_div">
        				
        					
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
</div>
<!-- Add Question modal end -->

<!-- View Question modal start -->
<div class="modal fade" id="viewquestionmodal" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="background-color:unset">
			<div class="modal-header" style="background-color: #348fe2 ;">
				<h4 class="modal-title" style="color:white">View Question</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body bg_grey">
				<div class='col-md-12'>
        			<!-- begin panel -->
        			<div class="panel bg_grey">
        				<!-- begin panel-body -->
        				<div class="panel-body view_ques_modal_div">
        				
        					
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
</div>
<!-- View Question modal end -->

<!-- Edit Question modal start -->
<div class="modal fade" id="editquestionmodal" role="dialog">
	<div class="modal-dialog modal-lg"  style="max-width:1200px;">
		<div class="modal-content" style="background-color:unset">
			<div class="modal-header" style="background-color: #348fe2 ;">
				<h4 class="modal-title" style="color:white">Edit Question</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body bg_grey">
				<div class='col-md-12'>
        			<!-- begin panel -->
        			<div class="panel bg_grey">
        				<!-- begin panel-body -->
        				<div class="panel-body edit_ques_modal_div">
        				
        					
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
</div>
<!-- Edit Question modal end -->


<script>
	$('document').ready(function(){
        updatelistques();
	});
</script>