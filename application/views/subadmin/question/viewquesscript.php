<script>
	$('document').ready(function(){
		$('body').on('click','.approve_ques',function(){
			 var quesid = $(this).attr('attr-quesid');
			 $.ajax({
    			type: 'POST',
    			url: base_loc + 'subadminpostajax/approveques',
    			data : {"quesid" : quesid, "loginid" : loginid},
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    				'User-ID': loginid,
    				'Authorization': token,
    				'type': type
    			},
    			success: function (msg) {
    				if(msg.data=="ok"){
    					GritterNotification("Success","QUESTION APPROVED SUCCESSFULLY","my-sticky-class");
    				    $('.searchques').trigger('click');
    				    $('#viewquestionmodal').modal('hide');
    				    $('#editquestionmodal').modal('hide');
    				    updatelistques();
    				}else{
    					GritterNotification("Error",msg.message,"my-sticky-class");
    				}
    			},
    			error: function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc;
    				}
    			}
    		});
		});
		$('body').on('click','.hold_ques',function(){
			 var quesid = $(this).attr('attr-quesid');
			 $.ajax({
    			type: 'POST',
    			url: base_loc + 'subadminpostajax/holdques',
    			data : {"quesid" : quesid, "loginid" : loginid},
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    				'User-ID': loginid,
    				'Authorization': token,
    				'type': type
    			},
    			success: function (msg) {
    				if(msg.data=="ok"){
    					GritterNotification("Success","QUESTION STATUS CHANGE SUCCESSFULLY","my-sticky-class");
    				    $('.searchques').trigger('click');
    				    $('#viewquestionmodal').modal('hide');
    				    $('#editquestionmodal').modal('hide');
    				    updatelistques();
    				}else{
    					GritterNotification("Error",msg.message,"my-sticky-class");
    				}
    			},
    			error: function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc;
    				}
    			}
    		});
		});
	});
</script>