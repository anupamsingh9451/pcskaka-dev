<script>
	$('document').ready(function(){
	      
       $('#datetimepicker1').datetimepicker({
            format: 'HH:mm'
        });
       $('#datetimepicker1').keydown(false);
       
        $('#datetimepicker4').datetimepicker({
            format: 'HH:mm'
        });
       $('#datetimepicker4').keydown(false);
		/* for edit company detail*/
		$('body').on('click','.chapteredit',function(){
			$('#editchapter').find('.quesno').val($(this).attr('attr-quesno'));
			$('#editchapter').find('.chapterstatus').val($(this).attr('attr-status'));
			$('#editchapter').find('.bookid').val($(this).attr('attr-bookid'));
			$('#editchapter').find('.chapternm').val($(this).attr('attr-chapternm'));
			$('#editchapter').find('.chapterid').val($(this).attr('attr-chapterid'));
			$('#editchapter').modal('show');
		});
		$('body').on('click','.deleteques',function(){
		    var quesid = $(this).attr('attr-quesid');
			swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Question!",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                		type: 'POST',
                		url: base_loc + 'subadminpostajax/deletequestion',
                		data: 'quesid='+quesid,
                		headers: {
                			'Client-Service': clientserv,
                			'Auth-Key': apikey,
                			'User-ID': loginid,
                			'Authorization': token,
                			'type': type
                		},
                		success: function (msg) {
                			if(msg.data=="ok"){
                				swal({
                                    title: 'Deleted!',
                                    text: 'Question Deleted successfully!',
                                    icon: 'success'
                                }).then(function() {
                                     // <--- submit form programmatically
                                });
                			}else{
                				GritterNotification("Error",msg.message,"my-sticky-class");
                			}
                			updatelistques();
                		},
                		error: function (msg) {
                			if (msg.responseJSON['status'] == 303) {
                				location.href = base_loc;
                			}
                			if (msg.responseJSON['status'] == 401) {
                				location.href = base_loc;
                			}
                			if (msg.responseJSON['status'] == 400) {
                				location.href = base_loc;
                			}
                		}
                    });
                } else {
                    swal("Cancelled", "Your Question is safe :)", "error");
                }
            })
		});
		    
		$('body').on('change','.ques_type',function(){
			$ques_type = $(this).val();
			console.log($ques_type);
			if($ques_type==1){
			    $('.for_subjective').show();
			    $('.for_objective').hide();
			}else{
			    $('.for_subjective').hide();
			    $('.obj_options').trigger('keyup');
			    $('.edit_obj_options').trigger('keyup');
			    $('.for_objective').show();
			}
		});
		$('body').on('keyup','.obj_options',function(){
			$options = parseInt($(this).val());
			
			    $str = '';
			    for($i=1; $i<=$options; $i++){
			        $str += '<div class="col-md-12"><label class="col-form-label">Option '+$i+'<span class="star">*</span></label><div class="input-group m-b-10"><div class="input-group-prepend"><span class="input-group-text"><input type="radio" id="defaultRadio" class="obj_answer" name="obj_answer[]" value="'+$i+'"></span></div><textarea rows="1" class="form-control ques_option" name="ques_option[]"></textarea></div></div>';
			    }
			    $('.options_div').html($str);
		});

		$('#datetimepicker3').datetimepicker({
            format: 'HH:mm'
        });
       $('#datetimepicker3').keydown(false);
       
       $('#datetimepicker2').datetimepicker({
            format: 'HH:mm'
        });
       $('#datetimepicker2').keydown(false);
       
       
	 $('body').on('click','.view_ques_modal', function () {
        var quesid = $(this).attr('attr-quesid');	     
        // alert(quesid);
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'subadmingetajax/view_ques_html',
    		data: 'quesid='+quesid,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.view_ques_modal_div').html(msg);
    			$('#viewquestionmodal').modal('show');
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
    });
    $('body').on('click','.edit_ques_on_modal', function () {
        var quesid = $(this).attr('attr-quesid');
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'subadmingetajax/edit_ques_html',
    		data: 'quesid='+quesid,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.edit_ques_modal_div').html(msg);
    			
    			$('#editquestionmodal').modal('show');
    			
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
    });
    $('body').on('click','.add_ques_modal', function () {
        var chapterid = $(this).attr('attr-chapterid');	     
        // alert(quesid);
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'subadmingetajax/add_ques_html',
    		data: 'chapterid='+chapterid,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.add_ques_modal_div').html(msg);
    			$('#addquestionmodal').modal('show');
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
    });
      
});
function updatelistques(){
	    var chapterid = <?php echo $this->uri->segment(3); ?>;
		$('#question_table').dataTable().fnDestroy();
		$('#question_table').DataTable({
			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
			pageLength: '10',
		    "lengthMenu": [[10, 25, 50, 100, 1000, 2000, 5000], [10, 25, 50, 100, 1000, 2000, 5000]],
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"processing": true,
		    "serverSide": true,
			"ajax":{
				"url":base_loc + 'subadmingetajax/listquestion',
				"type": "GET",
				"data": { chapterid:chapterid,uriseg:'main'  },
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"QUES"},
				{"data":"QUES_CREATED_BY"},
				{"data":"QUES_CREATED_AT"},
				{"data":"QUES_EDITED_BY"},
				{"data":"QUES_APPROVED_BY"},
				{"data":"QUES_DIFFICULTY_LEVEL"},
				{"data":"QUES_STATUS"},
				{"data":"TOOLS"},
			]
		});
	}
</script>