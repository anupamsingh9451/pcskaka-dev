<!-- begin #content -->
<link href="<?php echo base_url(); ?>assets/subadminassets/css/question/add_ques_modal.css" rel="stylesheet" />
<div class="row">
    <div class="col-md-12">
        <div class="card" style="border: 1px solid rgba(0,0,0,.2);">
			<div class="card-block"> 
			    <div class="row">
		            <div class="col-md-12">
		                <form id="questioncreate">
    					    <input type="hidden" name="subject" value="<?php echo $chapterdt[0]->SUB_ID; ?>">
    					    <input type="hidden" name="book" class="bookid" value="<?php echo $chapterdt[0]->BOOK_ID; ?>">
    					    <input type="hidden" name="chapter" class="chapterid" value="<?php echo $chapterdt[0]->CHAPTER_ID; ?>">
    					    <div class="row">
    					        <div class="col-md-6">
        							<label class="col-form-label">Question Type <span class="star">*</span></label>
        							<select class="form-control ques_type" name="ques_type" data-style="btn-white" required>
                                        <option  value="">--Select Question Type--</option>
                                        <option  value="1">Subjective</option>
                                        <option  value="2">Objective</option>
        							</select>
            					</div>
            					<div class="col-md-6">
        							<label class="col-form-label">Level of Question <span class="star">*</span></label>
        							<select class="form-control ques_levele" name="ques_levele" data-style="btn-white" required>
                                        <option  value="">--Select Question level--</option>
                                        <option  value="1">Low</option>
                                        <option  value="2">Medium</option>
                                        <option  value="3">Hard</option>
        							</select>
            					</div>
    					        <div class="col-md-12">
        							<label class="col-form-label">Question <span class="star">*</span></label>
                                    <textarea class="summernote question" name="question" id="mysummernote"></textarea>
            					</div>
            				
            					<div class="col-md-12 for_subjective">
            					    <div class="row">
            					        <div class="col-md-12">
                							<label class="col-form-label">Answer <span class="star">*</span></label>
                                            <textarea rows="3" class="form-control subj_answer" name="subj_answer"></textarea>
                    					</div>
                					    <div class="col-md-6">
                							<label class="col-form-label">Positive Mark </label>
                							<input type="text" class="form-control subj_positive_mark input_num" name="subj_positive_mark" placeholder="Enter Positive mark">
            						    </div>
            						    <div class="col-md-6">
                							<label class="col-form-label">Negative Mark </label>
                							<input type="text" class="form-control subj_negative_mark input_num" name="subj_negative_mark" placeholder="Enter Negative mark">
            						    </div>
            						    <div class="col-md-6">
                							<label class="col-form-label">Question Hint</label>
                                            <textarea rows="2" class="form-control subj_ques_hint" name="subj_ques_hint"></textarea>
                    					</div>
                    					<div class="col-md-6">
                							<label class="col-form-label">Question Explanation <span class="star">*</span></label>
                                            <textarea rows="2" class="form-control subj_ques_explain" name="subj_ques_explain"></textarea>
                    					</div>
                    					<div class='col-sm-6'>
                                            <label class="col-form-label">Duration </label>
                                            <div class='input-group date' id='datetimepicker3'>
                                                <input type='text' class="form-control subj_duration" name="subj_duration" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-time"></i>
                                                </span>
                                            </div>
                                        </div>
                    					<div class="col-md-6 hide">
                    						<label class="col-form-label"> Created Date <span class="star">*</span></label>
                    						<input type="text" class="form-control dtpicker subj_created_date" name="subj_created_date" value="<?php echo date('Y-m-d')?>">
                    					</div>
                					</div>
            					</div>
            					<div class="col-md-12 for_objective hidez">
            					    <div class="row">
                					    <div class="col-md-6">
                					        <div class="row">
                					            <div class="col-md-12">
                        							<label class="col-form-label">No of options <span class="star">*</span></label>
                        							<input type="text" class="form-control obj_options input_num" name="obj_options" placeholder="Enter Number of Options" value="4">
                    						    </div>
                                                <div class="col-md-12">
                                                    <div class="row options_div">
                                                        
                                                    </div>
                            				    </div>
                					        </div>
                					    </div>
                					    <div class="col-md-6">
                					        <div class="row">
                					            <div class="col-md-6">
                        							<label class="col-form-label">Positive Mark </label>
                        							<input type="text" class="form-control obj_positive_mark input_num" name="obj_positive_mark" placeholder="Enter Positive mark">
                    						    </div>
                    						    <div class="col-md-6">
                        							<label class="col-form-label">Negative Mark </label>
                        							<input type="text" class="form-control obj_negative_mark input_num" name="obj_negative_mark" placeholder="Enter Negative mark">
                    						    </div>
                    						    <div class="col-md-12">
                        							<label class="col-form-label">Question Hint </label>
                                                    <textarea rows="2" class="form-control obj_ques_hint" name="obj_ques_hint"></textarea>
                            					</div>
                            					<div class="col-md-12">
                        							<label class="col-form-label">Question Explanation<span class="star">*</span></label>
                                                    <textarea rows="2" class="form-control obj_ques_explain" name="obj_ques_explain"></textarea>
                            					</div>
                            					
                                                <div class='col-sm-12'>
                                                    <label class="col-form-label">Duration </label>
                                                    <div class='input-group date' id='datetimepicker2'>
                                                        <input type='text' class="form-control obj_duration" name="obj_duration" />
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-time"></i>
                                                        </span>
                                                    </div>
                                                </div>
                            					<div class="col-md-6 hide">
                            						<label class="col-form-label"> Created Date <span class="star">*</span></label>
                            						<input type="text" class="form-control dtpicker obj_created_date" name="obj_created_date" value="<?php echo date('Y-m-d')?>">
                            					</div>
                					        </div>
                					    </div>
            					    </div>
            					</div>
            					<div class="col-md-12" style="margin-top:10px">
        							<div class="col-md-12 text-center">
        								<button type="submit" class="btn btn-primary questioncreate">SAVE</button>
        							</div>
        						</div>
        					</div>
        				</form>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>
<script>

$('document').ready(function(){
	FormSummernote.init();
	$('#questioncreate').find('.ques_type').val(2);
	$('#questioncreate').find('.ques_type').trigger('change');
});
</script>
