<script>
var optionss='';
var answer='';
	$('document').ready(function(){
	    
       $('#datetimepicker1').datetimepicker({
            format: 'HH:mm'
        });
       $('#datetimepicker1').keydown(false);
       
        $('#datetimepicker4').datetimepicker({
            format: 'HH:mm'
        });
       $('#datetimepicker4').keydown(false);
	
        
		$('body').on('change','.ques_type',function(){
			$ques_type = $(this).val();		    
			if($ques_type==1){
			    $('.for_subjective').show();
			    $('.for_objective').hide();
			}else{
			    $('.for_subjective').hide();
			    $('.edit_obj_options').trigger('keyup');
			    $('.for_objective').show();
			}
		});
	
		$('body').on('keyup','.edit_obj_options',function(){
			 add_obj_option_div(optionss);
		});

	    $('body').on('change','.subjects',function(e){
    	    var bookid = $('option:selected','.books').val();
        	$.ajax({
        		type: 'GET',
        		data:'subid='+$(this).val(),
        		url: base_loc + 'admingetajax/get_book_by_subid',
        		headers: {
        			'Client-Service': clientserv,
        			'Auth-Key': apikey,
        			'User-ID': loginid,
        			'Authorization': token,
        			'type': type
        		},
        		success: function (msg) {
        		    $('.books option').remove();
        			var str='';
        			if(msg.status==200){
        				if(msg.data.length>0){
        				    
        					$.each(msg.data, function(index, element) {
        					    if(element.BOOK_ID==bookid){
        					        str+='<option selected value="'+element.BOOK_ID+'">'+element.BOOK_NAME+'</option>';
        					    }else{
        					        str+='<option  value="'+element.BOOK_ID+'">'+element.BOOK_NAME+'</option>';
        					    }
        						
        					});
        				}
        				$('.books').append(str);
        				
        			}else{
        				location.href = base_loc;
        			}
        		},
        		error: function (msg) {
        			if (msg.responseJSON['status'] == 303) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 401) {
        				location.href = base_loc; 
        			}
        			if (msg.responseJSON['status'] == 400) {
        				location.href = base_loc; 
        			}
        		}
        	});
    	});
        if($('.subjects').length>0){
    	    $('.subjects').trigger('change'); 
    	}
	});
	function add_obj_option_div(optionss){
	    $options = parseInt($('.edit_obj_options').val());
	    $str = '';
	    if(optionss!=''){
	        for($i=1; $i<=$options; $i++){
	            var j = $i-1;
	            if(optionss[j]!=undefined){
    	            $str += '<div class="col-md-6 options_divs"><label class="col-form-label">Option '+$i+'<span class="star">*</span></label><div class="input-group m-b-10"><div class="input-group-prepend"><span class="input-group-text"><input type="radio" id="defaultRadio" class="obj_answer" name="obj_answer[]" value="'+$i+'"></span></div><textarea rows="2" class="form-control ques_option" name="ques_option[]">'+optionss[j]+'</textarea></div></div>';
	            }else{
    	            $str += '<div class="col-md-6 options_divs"><label class="col-form-label">Option '+$i+'<span class="star">*</span></label><div class="input-group m-b-10"><div class="input-group-prepend"><span class="input-group-text"><input type="radio" id="defaultRadio" class="obj_answer" name="obj_answer[]" value="'+$i+'"></span></div><textarea rows="2" class="form-control ques_option" name="ques_option[]"></textarea></div></div>';
	            }
    	    }
    	    $('.edit_options_div').html($str);
    	    var k = 1;
    	    $(".options_divs").each(function() {
    	        var answers = answer.toString().split(',');
    	        var l = k.toString();
                if(answers.includes(l)){
                    console.log(l);
                    $(this).find('.obj_answer').prop('checked', true);
                }
                 k++;
            });
	    }else{
    	    for($i=1; $i<=$options; $i++){
    	        $str += '<div class="col-md-6 options_divs"><label class="col-form-label">Option '+$i+'<span class="star">*</span></label><div class="input-group m-b-10"><div class="input-group-prepend"><span class="input-group-text"><input type="radio" id="defaultRadio" class="obj_answer" name="obj_answer[]" value="'+$i+'"></span></div><textarea rows="2" class="form-control ques_option" name="ques_option[]"></textarea></div></div>';
    	    }
    	    $('.edit_options_div').html($str);
	    }
	}
	
</script>