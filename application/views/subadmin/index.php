<head>
	<meta charset="utf-8" />
	<title>PCSKAKA Sub Admin | Login Page</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/themeassets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/themeassets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/themeassets/plugins/font-awesome/css/all.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/themeassets/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/themeassets/css/default/style.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/themeassets/css/default/style-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/themeassets/css/default/theme/default.css" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="<?php echo base_url(); ?>/assets/themeassets/plugins/parsley/src/parsley.css" rel="stylesheet" />
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>/assets/themeassets/plugins/pace/pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	<script>
		var api_loc="http://localhost:3000/";
		var base_loc="<?php echo base_url(); ?>";
		var clientserv="Ratan";
        var apikey="pcskaka";
	</script>
</head>
<body class="pace-top bg-white">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
		<!-- begin login -->
		<div class="login login-with-news-feed">
			<!-- begin news-feed -->
			<div class="news-feed">
				<div class="news-image" style="background-image: url(<?php echo base_url(); ?>/assets/themeassets/img/login-bg/theme_cover.jpg)"></div>
				<div class="news-caption">
					<h4 class="caption-title"><b>PCSKAKA</b> App</h4>
				</div>
			</div>
			<!-- end news-feed -->
			<!-- begin right-content -->
			<div class="right-content">
				<!-- begin login-header -->
				<div class="login-header">
					<div class="brand">
						<span class="logo"></span> <b>PCSKAKA</b> App
						<small>Connecting the world</small>
					</div>
					<div class="icon">
						<i class="fa fa-sign-in"></i>
					</div>
				</div>
				<!-- end login-header -->
				<!-- begin login-content -->
				<div class="login-content">
					<form action="javascript:void(0);" id="loginsubadmin" class="margin-bottom-0" data-parsley-validate="true">
						<div class="form-group m-b-15">
							<div class="alert mymsg">
							</div>
						</div>
						<div class="form-group m-b-15">
							<input type="text" name="username" class="form-control username form-control-lg" placeholder="User Name" data-parsley-required="true" />
						</div>
						<div class="form-group m-b-15">
							<input type="password" name="password" class="form-control password form-control-lg" placeholder="Password" data-parsley-required="true" />
						</div>
						<div class="checkbox checkbox-css m-b-30">
							<input type="checkbox" id="remember_me_checkbox" value="" />
							<label for="remember_me_checkbox">
							Remember Me
							</label>
						</div>
						<div class="login-buttons">
							<button type="submit" class="btn btn-success btn-block loginsubadmin btn-lg">Sign me in</button>
						</div>
						
						<hr />
						<p class="text-center text-grey-darker">
							&copy; PCSKAKA All Right Reserved 2019
						</p>
					</form>
				</div>
				<!-- end login-content -->
			</div>
			<!-- end right-container -->
		</div>
		<!-- end login -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>/assets/themeassets/plugins/jquery/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/themeassets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/themeassets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/themeassets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/themeassets/plugins/js-cookie/js.cookie.js"></script>
	<script src="<?php echo base_url(); ?>/assets/themeassets/js/theme/default.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/themeassets/js/apps.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>/assets/themeassets/plugins/parsley/dist/parsley.js"></script>
	<script src="<?php echo base_url(); ?>/assets/themeassets/plugins/highlight/highlight.common.js"></script>
	<script src="<?php echo base_url(); ?>/assets/themeassets/js/demo/render.highlight.js"></script>
	<script src="<?php echo base_url(); ?>assets/themeassets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>

	<!-- ================== END PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url(); ?>/assets/subadminassets/js/subadminpost.js"></script>
	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>

</body>
</html>