<script>
 $('document').ready(function(){
     get_all_sub();
     $('body').on('change','.all_sub',function(){
		var subide = $(this).val();	
		var change= 1;
		get_books_by_subid(subide,change);
	 });

	
	$('body').on('click','.deleteques',function(){
		    var quesid = $(this).attr('attr-quesid');
			swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Question!",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                		type: 'POST',
                		url: base_loc + 'subadminpostajax/deletequestion',
                		data: 'quesid='+quesid,
                		headers: {
                			'Client-Service': clientserv,
                			'Auth-Key': apikey,
                			'User-ID': loginid,
                			'Authorization': token,
                			'type': type
                		},
                		success: function (msg) {
                			if(msg.data=="ok"){
                				swal({
                                    title: 'Deleted!',
                                    text: 'Question Deleted successfully!',
                                    icon: 'success'
                                }).then(function() {
                                     // <--- submit form programmatically
                                });
                			}else{
                				GritterNotification("Error",msg.message,"my-sticky-class");
                			}
                			$('.all_book').trigger('change');
                		},
                		error: function (msg) {
                			if (msg.responseJSON['status'] == 303) {
                				location.href = base_loc;
                			}
                			if (msg.responseJSON['status'] == 401) {
                				location.href = base_loc;
                			}
                			if (msg.responseJSON['status'] == 400) {
                				location.href = base_loc;
                			}
                		}
                    });
                } else {
                    swal("Cancelled", "Your Question is safe :)", "error");
                }
            })
		});
		
    $('body').on('click','.searchques',function(){
		var userid = $('option:selected','.all_user').val();
		var bookids = $('option:selected','.all_book').val();
		if(bookids!='' && bookids!=null){
		    updatelistques();
		}
	});
	
	 $('body').on('click','.view_ques_modal', function () {
        var quesid = $(this).attr('attr-quesid');
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'subadmingetajax/view_ques_html',
    		data: 'quesid='+quesid,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.view_ques_modal_div').html(msg);
    			$('#viewquestionmodal').modal('show');
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
    });
    $('body').on('click','.edit_ques_on_modal', function () {
        var quesid = $(this).attr('attr-quesid');
        $.ajax({
    		type: 'GET',
    		url: base_loc + 'subadmingetajax/edit_ques_html',
    		data: 'quesid='+quesid,
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    			$('.edit_ques_modal_div').html(msg);
    			$('#editquestionmodal').modal('show');
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
    });
 });
 function get_all_sub(){
     $.ajax({
    		type: 'GET',
    		url: base_loc + 'subadmingetajax/get_all_sub',
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    		    $('.all_sub option').remove();
    			var str='';
    			if(msg.status==200){
    			    if(msg.data.length>0){
    			        var subidarr = new Array();
    					$.each(msg.data, function(index, element) {
    						str += '<option value="'+element.SUB_ID+'">'+element.SUB_NAME+'</option>';
    						subidarr.push(element.SUB_ID);
    					});
    					var subids = subidarr.join();
    		            var str1 = "<option selected value='" + subids + "'>"+'All'+"</option>";
        				$('.all_sub').append(str1);
        				$('.all_sub').append(str);
        				
        				var change = '';
        				get_books_by_subid(subids,change);
    			    }
    			}else{
    				location.href = base_loc;
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
 }
  function get_books_by_subid(subide,change){
      $.ajax({
    		type: 'GET',
    		data:'subid='+ subide,
    		url: base_loc + 'subadmingetajax/get_book_by_subid',
    		async:false,
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    		    $('.all_book option').remove();
    			var str='';
    			if(msg.status==200){
    				if(msg.data.length>0){
    				    var bookid = new Array();
    					$.each(msg.data, function(index, element) {
    						str+='<option value="'+element.BOOK_ID+'">'+element.BOOK_NAME+'</option>';
    						bookid.push(element.BOOK_ID);
    					});
    					var bookids = bookid.join();
			            var str1 = "<option selected value='" + bookids+ "'>"+'All'+"</option>";
    				}
    				$('.all_book').append(str1);
    				$('.all_book').append(str);
    				var userid = $('option:selected','.all_user').val();
    				if(change==''){
    				    updatelistques();
    				}
    			}else{
    				location.href = base_loc;
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
  }
  
 function updatelistques(){
	    var level = $('option:selected','.level').val();
		var status = $('option:selected','.status').val();
		var userid = $('option:selected','.all_user').val();
		var bookids = $('option:selected','.all_book').val();
		var approve_by = $('option:selected','.approve_by').val();
	if(userid!='' && bookids!='' && userid!=null && bookids!=null){
		$('#user_question_table').dataTable().fnDestroy();
		$('#user_question_table').DataTable({
			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
            // dom: 'Bfrtip,l',
			pageLength: '10',
		    "lengthMenu": [[10, 25, 50, 100, 1000, 2000, 5000], [10, 25, 50, 100, 1000, 2000, 5000]],
			"buttons": [
				'excel', 'pdf', 'print'
			],
			"processing": true,
		    "serverSide": true,
			"ajax":{
				"url":base_loc + 'subadmingetajax/listquestion',
				"dataType": "json",
				"type": "GET",
				"data": { 'userid':userid,'bookids':bookids,'level':level,'status':status,'uriseg':'user','approve_by':approve_by },
				"headers": {
					'Client-Service': clientserv,
					'Auth-Key': apikey,
					'User-ID': loginid,
					'Authorization': token,
					'type': type
				},
				"error": function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc;
    				}
    			}
			},
			"columns":[
				{"data":"SR_NO"},
				{"data":"QUES"},
				{"data":"QUES_CREATED_BY"},
				{"data":"QUES_CREATED_AT"},
				{"data":"QUES_EDITED_BY"},
				{"data":"QUES_APPROVED_BY"},
				{"data":"QUES_DIFFICULTY_LEVEL"},
				{"data":"QUES_STATUS"},
				{"data":"TOOLS"},
			]
		});
    }
}
</script>