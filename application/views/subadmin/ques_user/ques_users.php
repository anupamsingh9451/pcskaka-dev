
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Question</a></li>
		<li class="breadcrumb-item active">list</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">LIST ALL QUESTION</h1>
	<!-- begin page-header -->
	<!-- end page-header -->
	
    <div class="row no_mrgn addbx_top_rowhead pd_15">
		<div class="col-md-12">
		    <div class="row">
		        <div class="col-md-2">
					<label class="col-form-label">Users <span class="star">*</span></label>
					<select class="form-control all_user" name="all_user" data-style="btn-white" required="">
                        <?php $userarr = array();
                        foreach ($alluser as $allusers) {
                            $userarr[] = $allusers->USER_ID;
                        }
                        
                        $userid = implode(',', $userarr);
                        ?>
                        <option value="<?php echo $userid; ?>">All</option>
                        <?php foreach ($alluser as $allusers) { ?>
                            <option value=<?php echo $allusers->USER_ID; ?>><?php echo $allusers->USER_FNAME." ".$allusers->USER_LNAME; ?></option>
                        <?php } ?>
					</select>
				</div>
				<div class="col-md-2">
					<label class="col-form-label">Subjects <span class="star">*</span></label>
					<select class="form-control all_sub" name="all_sub" data-style="btn-white" required="">
                        
					</select>
				</div>
				<div class="col-md-2">
					<label class="col-form-label">Books <span class="star">*</span></label>
					<select class="form-control all_book" name="all_book" data-style="btn-white" required="">
                        
					</select>
				</div>
				<div class="col-md-2">
					<label class="col-form-label">Level <span class="star">*</span></label>
					<select class="form-control level" name="level" data-style="btn-white" required="">
                        <option value="1,2,3">All</option>
                        <option value="1">Low</option>
                        <option value="2">Medium</option>
                        <option value="3">High</option>
					</select>
				</div>
				<div class="col-md-2">
					<label class="col-form-label">status <span class="star">*</span></label>
					<select class="form-control status" name="status" data-style="btn-white" required="">
                        <option value="0,1,3">All</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                        <option value="3">Hold</option>
					</select>
				</div>
				<div class="col-md-2">
					<label class="col-form-label">Approved By <span class="star">*</span></label>
					<select class="form-control approve_by" name="approve_by" data-style="btn-white" required="">
                        <?php $userarr = array();
                        foreach ($alluser as $allusers) {
                            if($allusers->USER_ROLES==1 || $allusers->USER_ROLES==4){
                                $userarr[] = $allusers->USER_ID;
                            }
                        }
                        $userarr[] = 0;
                        $userid = implode(',', $userarr);
                        ?>
                        <option value="<?php echo $userid; ?>">All</option>
                        <?php foreach ($alluser as $allusers) { if($allusers->USER_ROLES==1 || $allusers->USER_ROLES==4){ ?>
                            <option value=<?php echo $allusers->USER_ID; ?>><?php echo $allusers->USER_FNAME." ".$allusers->USER_LNAME; ?></option>
                        <?php } } ?>
					</select>
				</div>
				<div class="col-md-2 m-t-10">
					<button type="button" class="btn btn-success searchques"><i class="fa fa-fw fa-search"></i> Search</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- begin row -->
	<div class="row">
        <div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Question List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="user_question_table" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th width="1%">Sr No.</th>
								<th width="50%">Question</th>
								<th width="8%">Created By</th>
								<th width="7%">Created Date</th>
								<th width="7%">Edited By</th>
								<th width="7%">Approved By</th>
								<th>Level</th>
								<th>Status</th>
								<th width="6%" class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
	</div>
</div>
		<!-- end #content -->


<!-- Edit Question modal start -->
<div class="modal fade" id="addquestionmodal" role="dialog">
	<div class="modal-dialog modal-lg"  style="max-width:1200px;">
		<div class="modal-content" style="background-color:unset">
			<div class="modal-header" style="background-color: #348fe2 ;">
				<h4 class="modal-title" style="color:white">Add Question</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body bg_grey">
				<div class='col-md-12'>
        			<!-- begin panel -->
        			<div class="panel bg_grey">
        				<!-- begin panel-body -->
        				<div class="panel-body add_ques_modal_div">
        				
        					
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
</div>
<!-- Edit Question modal end -->

<!-- View Question modal start -->
<div class="modal fade" id="viewquestionmodal" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="background-color:unset">
			<div class="modal-header" style="background-color: #348fe2 ;">
				<h4 class="modal-title" style="color:white">View Question</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body bg_grey">
				<div class='col-md-12'>
        			<!-- begin panel -->
        			<div class="panel bg_grey">
        				<!-- begin panel-body -->
        				<div class="panel-body view_ques_modal_div">
        				
        					
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
</div>
<!-- View Question modal end -->

<!-- Edit Question modal start -->
<div class="modal fade" id="editquestionmodal" role="dialog">
	<div class="modal-dialog modal-lg"  style="max-width:1200px;">
		<div class="modal-content" style="background-color:unset">
			<div class="modal-header" style="background-color: #348fe2 ;">
				<h4 class="modal-title" style="color:white">Edit Question</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body bg_grey">
				<div class='col-md-12'>
        			<!-- begin panel -->
        			<div class="panel bg_grey">
        				<!-- begin panel-body -->
        				<div class="panel-body edit_ques_modal_div">
        				
        					
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
</div>
<!-- Edit Question modal end -->
