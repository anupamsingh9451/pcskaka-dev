<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">Book</a></li>
		<li class="breadcrumb-item active">list</li>
	</ol>
	<!-- end breadcrumb -->
	<h1 class="page-header">LIST ALL BOOK</h1>
	<!-- begin page-header -->
	<!-- end page-header -->
	<div class="row no_mrgn addbx_top_rowhead pd_15">
		<div class="col-md-12">
		    <div class="row">
				<div class="col-md-3">
					<label class="col-form-label">Library <span class="star">*</span></label>
					<select class="form-control all_lib" name="all_lib" data-style="btn-white" required="">
                        
					</select>
				</div>
				<div class="col-md-3">
					<label class="col-form-label">Subject <span class="star">*</span></label>
					<select class="form-control all_sub" name="all_sub" data-style="btn-white" required="">
                        
					</select>
				</div>
				<div class="col-md-3 p-30">
					<button type="button" class="btn btn-success searchbook"><i class="fa fa-fw fa-search"></i> Search</button>
				</div>
			</div>
		</div>
	</div>
	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-lg-12">
			<div class="panel brdr">
				<!-- begin panel-heading -->
				<div class="panel-heading brdr_btm">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Book List</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body table-responsive">
					<table id="booklist" class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th width="1%">Sr No.</th>
								<th>Library Name</th>
								<th>Subject Name</th>
								<th>Book Name</th>
								<th>Created Date</th>
								<th>Pending Ques</th>
								<th>Status</th>
								<th class="text-nowrap">Tools</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
		</div>
		<!-- end col-6 -->
		
	</div>
</div>
		<!-- end #content -->
		<div class="modal fade" id="editbook">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">EDIT <span class='booknm'></span> Book</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="editbookform">
				<div class="modal-body">
					<input type='hidden' class='subid' name="subid" value='' />
					<input type='hidden' class='bookid' name="bookid" value='' />
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Book Name<span class="star">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 booknm" name="booknm" placeholder="Enter Book name here" required>
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Chapter No.</label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5 chapterno" name="chapterno" placeholder="Enter Chapter Number">
						</div>
					</div>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Status<span class="star">*</span></label>
						<div class="col-md-9">
							<select name="bookstatus" class='form-control bookstatus'>
								<option value='1'>Active</option>
								<option value='0'>Inactive</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="submit "class="btn btn-success editbookform">Update</a>
				</div>
			</form>
		</div>
	</div>
</div>