<script>
	$('document').ready(function(){
	    get_all_library();
		/* for edit company detail*/
		$('body').on('click','.bookedit',function(){
			$('#editbook').find('.chapterno').val($(this).attr('attr-chapterno'));
			$('#editbook').find('.bookstatus').val($(this).attr('attr-status'));
			$('#editbook').find('.bookid').val($(this).attr('attr-bookid'));
			$('#editbook').find('.booknm').val($(this).attr('attr-booknm'));
			$('#editbook').find('.booknm').html($(this).attr('attr-booknm'));
			$('#editbook').find('.subid').val($(this).attr('attr-subid'));
			$('#editbook').modal('show');
		});
		$('body').on('change','.all_lib',function(){
    		var lib_id = $(this).val();
    		if(lib_id!='' && lib_id!=null){
    		    var change = 1;
    		    get_sub_by_libid(lib_id,change);
    		}
    	});
    	
	  getlibrary();
	   
    $('body').on('click','.searchbook',function(){
		var subid = $('option:selected','.all_sub').val();
    	if(subid!='' && subid!=null){
		    updatelistbooks();
		}
	});
	});
	function get_all_library(){
         $.ajax({
        		type: 'GET',
        		url: base_loc + 'subadmingetajax/getlibrary',
        		headers: {
        			'Client-Service': clientserv,
        			'Auth-Key': apikey,
        			'User-ID': loginid,
        			'Authorization': token,
        			'type': type
        		},
        		success: function (msg) {
        		    $('.all_lib option').remove();
        			var str='';
        			if(msg.status==200){
        			    if(msg.data.length>0){
        			        var libidarr = new Array();
        					$.each(msg.data, function(index, element) {
        						str += '<option value="'+element.LIB_ID+'">'+element.LIB_NAME+'</option>';
        						libidarr.push(element.LIB_ID);
        					});
        					var libids = libidarr.join();
        		            var str1 = "<option selected value='" + libids + "'>"+'All'+"</option>";
            				$('.all_lib').append(str1);
            				$('.all_lib').append(str);
            				var change = '';
            				get_sub_by_libid(libids,change);
        			    }
        			}else{
        				location.href = base_loc;
        			}
        		},
        		error: function (msg) {
        			if (msg.responseJSON['status'] == 303) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 401) {
        				location.href = base_loc; 
        			}
        			if (msg.responseJSON['status'] == 400) {
        				location.href = base_loc; 
        			}
        		}
        	});
     }
     
     function get_sub_by_libid(libids,change){
          $.ajax({
        		type: 'GET',
        		data:'subid='+ libids,
        		url: base_loc + 'subadmingetajax/get_subject_by_libid',
        		headers: {
        			'Client-Service': clientserv,
        			'Auth-Key': apikey,
        			'User-ID': loginid,
        			'Authorization': token,
        			'type': type
        		},
        		success: function (msg) {
        		    $('.all_sub option').remove();
        			var str='';
        			if(msg.status==200){
        				if(msg.data.length>0){
        				    var subid = new Array();
        					$.each(msg.data, function(index, element) {
        						str+='<option value="'+element.SUB_ID+'">'+element.SUB_NAME+'</option>';
        						subid.push(element.SUB_ID);
        					});
        					var subids = subid.join();
    			            var str1 = "<option selected value='" + subids+ "'>"+'All'+"</option>";
        				}
        				$('.all_sub').append(str1);
        				$('.all_sub').append(str);
        				if(change==''){
        				    updatelistbooks();
        				}
        			}else{
        				location.href = base_loc;
        			}
        		},
        		error: function (msg) {
        			if (msg.responseJSON['status'] == 303) {
        				location.href = base_loc;
        			}
        			if (msg.responseJSON['status'] == 401) {
        				location.href = base_loc; 
        			}
        			if (msg.responseJSON['status'] == 400) {
        				location.href = base_loc; 
        			}
        		}
        	});
      }
	function updatelistbooks(){
	    var subid = $('option:selected','.all_sub').val();
    	if(subid!='' && subid!=null){
    		$('#booklist').dataTable().fnDestroy();
    		$('#booklist').DataTable({
    			"dom": "<'row'<'col-sm-12'Bf>><'row'<'col-sm-12'irt>>" + "<'row'<'col-md-4'l><'col-md-8'p>>",
                // dom: 'Bfrtip,l',
    			pageLength: '10',
    		    "lengthMenu": [[10, 25, 50, 100, 1000, 2000, 5000], [10, 25, 50, 100, 1000, 2000, 5000]],
    			"buttons": [
    				'excel', 'pdf', 'print'
    			],
    			"processing": true,
    		    "serverSide": true,
    			"ajax":{
    				"url":base_loc + 'subadmingetajax/listbook',
    				"type": "GET",
    				"data": { subid:subid},
    				"headers": {
    					'Client-Service': clientserv,
    					'Auth-Key': apikey,
    					'User-ID': loginid,
    					'Authorization': token,
    					'type': type
    				},
    				"error": function (msg) {
        				if (msg.responseJSON['status'] == 303) {
        					location.href = base_loc;
        				}
        				if (msg.responseJSON['status'] == 401) {
        					location.href = base_loc; 
        				}
        				if (msg.responseJSON['status'] == 400) {
        					location.href = base_loc; 
        				}
        			}
    			},
    			"columns":[
    				{"data":"SR_NO"},
    				{"data":"LIB_NAME"},
    				{"data":"SUB_NAME"},
    				{"data":"BOOK_NAME"},
    				{"data":"BOOK_CREATED_AT"},
    				{"data":"PENDING_QUES"},
    				{"data":"BOOK_STATUS"},
    				{"data":"TOOLS"},
    			]
    		});
    	}
	}
</script>