<?php
class Subadmingetmodel extends CI_Model {
 
    public function get_subadmin_dt_model($id)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from users where USER_ID="'.$id.'"');
		$result= $query->result();
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
	}
	
	public function get_library_by_name($libname)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from library where LIB_NAME="'.$libname.'" ');
		$result= $query->result();
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
	}
	public function get_all_library()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from library where LIB_STATUS=1 order by LIB_ID DESC');
		$result= $query->result(); 
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
		return  $result;
	}
	public function get_all_librarys()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from library order by LIB_ID DESC');
		$result= $query->result(); 
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
		return  $result;
	}
	public function get_subject_by_name_and_libid($subjname,$libid)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from subjects where SUB_NAME="'.$subjname.'" and LIB_ID="'.$libid.'" ');
		$result= $query->result();
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
	}
	public function get_all_subject()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from subjects s LEFT JOIN library l on l.LIB_ID=s.LIB_ID order by SUB_ID DESC');
		$result= $query->result(); 
		$this->db->trans_commit();
		return  $result;
	}
	public function get_all_subids(){
	    $this->db->trans_start();
        $query  = $this->db->query('select GROUP_CONCAT(DISTINCT SUB_ID) as SUB_ID from subjects ');
		$result= $query->result();
		$this->db->trans_commit();
		return $result[0]->SUB_ID;
	}
	public function get_all_subjects()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from subjects s LEFT JOIN library l on l.LIB_ID=s.LIB_ID where SUB_STATUS=1 order by SUB_ID DESC');
		$result= $query->result(); 
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
	}
	
	public function get_subject_by_libid($libid){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from subjects where LIB_ID="'.$libid.'" and SUB_STATUS=1 ');
		$result= $query->result();
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
	}

	public function get_all_book()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from books b LEFT JOIN subjects s on s.SUB_ID=b.SUB_ID LEFT JOIN library l on l.LIB_ID=b.LIB_ID order by b.BOOK_ID DESC');
		$result= $query->result(); 
		$this->db->trans_commit();
		return $result;
	}
	public function get_book_by_subid($subid){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from books b LEFT JOIN subjects s on s.SUB_ID=b.SUB_ID LEFT JOIN library l on l.LIB_ID=b.LIB_ID where b.SUB_ID in ('.$subid.') and b.BOOK_STATUS=1 ');
		$result= $query->result();
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
	}
	public function get_bookids_by_subid($subid){
	    $this->db->trans_start();
        $query  = $this->db->query('select GROUP_CONCAT(DISTINCT BOOK_ID) as BOOK_ID from books where SUB_ID in ('.$subid.')');
		$result= $query->result();
		$this->db->trans_commit();
		return $result[0]->BOOK_ID;
	}

	public function get_all_chapter_by_bookid($bookid)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from chapter c LEFT JOIN books b on b.BOOK_ID=c.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=c.SUB_ID LEFT JOIN library l on l.LIB_ID=c.LIB_ID where c.BOOK_ID="'.$bookid.'" order by c.CHAPTER_ID DESC');
		$result= $query->result(); 
		$this->db->trans_commit();
		return $result;
	}

	public function get_all_ques_by_chapterid($chapterid,$userid='',$level="",$status="")
    {
		$this->db->trans_start();
		if($userid!=''){
            $query  = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID in ('.$chapterid.') and q.QUES_CREATED_BY in ('.$userid.') and q.QUES_STATUS in ('.$status.') and q.QUES_DIFFICULTY_LEVEL in ('.$level.') order by q.QUES_ID DESC');
		}else{
            $query  = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID="'.$chapterid.'" and q.QUES_STATUS!=2 order by q.QUES_ID DESC');
		}
		$result= $query->result(); 
		$this->db->trans_commit();
		return $result;
	}
	public function get_quesdt_by_id($ques_id){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.QUES_ID in ('.$ques_id.') ');
		$result= $query->result(); 
		$this->db->trans_commit();
		return $result;
	}
	public function get_user_role_by_id($id){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from users u LEFT JOIN user_role ur on ur.USER_ROLE_ID=u.USER_ROLES where u.USER_ID="'.$id.'"  ');
		$result= $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_difficulty_level($level){
	    if($level==1){
	        $result = "Low";
	    }else if($level==2){
	        $result = "Medium";
	    }else{
	        $result = "Heigh";
	    }
		return $result;
	}
	public function get_all_users()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from users where USER_ROLES!=3 order by USER_ID ASC');
		$result= $query->result(); 
		$this->db->trans_commit();
		return  $result;
	}
	public function get_all_ques_by_chapterid_questype($chapterid,$questype)
    {
		$this->db->trans_start(); 
        $query  = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID in ('.$chapterid.') and q.QUES_TYPE in ('.$questype.')  and QUES_STATUS=1 order by q.QUES_ID DESC');
		$result= $query->result(); 
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_ques_by_chapterid_difflevel($chapterid,$level,$questype="")
    {
        if($questype!=''){
            $questype = ' and q.QUES_TYPE=2 ';
        }
		$this->db->trans_start(); 
        $query  = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID where q.CHAPTER_ID in ('.$chapterid.') and q.QUES_DIFFICULTY_LEVEL in ('.$level.') '.$questype.'  and QUES_STATUS=1 order by q.QUES_ID ASC');
		$result= $query->result(); 
		$this->db->trans_commit();
		return $result;
	}
	public function get_chapterids_by_booksid($bookid){
	    $this->db->trans_start();
        $query  = $this->db->query('select GROUP_CONCAT(DISTINCT CHAPTER_ID) as CHAPTER_ID from chapter where BOOK_ID in ('.$bookid.')  ');
		$result= $query->result();
		$this->db->trans_commit();
		return $result[0]->CHAPTER_ID;
	}	

	public function get_question_by_subid_bookid_level($subid,$bookid,$level,$questype=""){
	    if($questype!=''){
	        $questype = ' and q.QUES_TYPE=2 ';
	    }
	    $this->db->trans_start(); 
        $query  = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.SUB_ID="'.$subid.'" and q.BOOK_ID in ('.$bookid.') and q.QUES_DIFFICULTY_LEVEL in ('.$level.') '.$questype.'  and QUES_STATUS=1 order by q.QUES_ID DESC');
		$result= $query->result(); 
		$this->db->trans_commit();
		return $result;
	}
	

	public function get_all_member_type(){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from user_role order by USER_ROLE_ID ASC ');
		$result= $query->result(); 
		$this->db->trans_commit();
		return  $result;
	}

	public function get_all_members(){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from users u LEFT JOIN user_role ur on ur.USER_ROLE_ID=u.USER_ROLES where u.USER_ROLES!="3" ');
		$result= $query->result(); 
		$this->db->trans_commit();
		return  $result;
	}
	public function get_ques_log_by_quesid($quesid){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from question_log ql LEFT JOIN user_role ur on ur.USER_ROLE_ID=ql.USER_ROLES LEFT JOIN users u on u.USER_ID=ql.USER_ID where ql.QUES_ID="'.$quesid.'" order by ql.QUES_LOG_ID DESC limit 1 ');
		$result= $query->result(); 
		$this->db->trans_commit();
		return  $result;
	}
	public function check_book_exist($bookname,$subid,$bookid=''){
	    $this->db->trans_start();
	    if($bookid==''){
	        $query  = $this->db->query('select * from books where BOOK_NAME="'.$bookname.'" and SUB_ID="'.$subid.'" ');
	    }else{
	        $query  = $this->db->query('select * from books where BOOK_NAME="'.$bookname.'" and SUB_ID="'.$subid.'" and BOOK_ID NOT IN ('.$bookid.') ');
	    }
        $result= $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_pending_ques($chapterid)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select count(*) as count from question where CHAPTER_ID="'.$chapterid.'" and QUES_APPROVED_BY=0 ');
		$result= $query->result(); 
		$this->db->trans_commit();
		return  $result[0]->count;
	}
	public function get_chaptersdt_by_id($chapterid){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from chapter c LEFT JOIN books b on b.BOOK_ID=c.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=c.SUB_ID LEFT JOIN library l on l.LIB_ID=c.LIB_ID where c.CHAPTER_ID="'.$chapterid.'"  ');
		$result= $query->result();
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
	}
}
?>