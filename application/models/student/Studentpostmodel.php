<?php
class Studentpostmodel extends CI_Model {

    public function login($username,$password,$type,$loginagain="")
    {
        $query  = $this->db->query('select * from users where ( USER_EMAIL="'.$username.'" OR USER_CONTACT="'.$username.'" ) and USER_PASSWORD="'.$password.'" and USER_ROLES="'.$type.'" and USER_STATUS=1');
		$result= $query->result();
        if(empty($result)){
            return array('status' => 200,'message' => 'Invalid Login Credential');
        } else {
			$last_login = date('Y-m-d H:i:s');
			$token = crypt(substr( md5(rand()), 0, 7),rand());
			$expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
			$this->db->trans_start();
			$q  = $this->db->select('*')->from('users_authentication')->where('HRM_ID',$result[0]->USER_ID)->where('HRM_TYPE',$type)->get()->row();
			if($q == ""){
			    $this->db->insert('users_authentication',array('HRM_ID' => $result[0]->USER_ID,'TOKEN' => $token,'EXPIRED_AT' => $expired_at,'HRM_TYPE' => $type));
            }else{
                if(empty($loginagain)){
                    if($q->EXPIRED_AT < date('Y-m-d H:i:s')){
                        $this->db->insert('users_authentication',array('HRM_ID' => $result[0]->USER_ID,'TOKEN' => $token,'EXPIRED_AT' => $expired_at,'HRM_TYPE' => $type));
                        $this->db->where('HRM_ID',$result[0]->USER_ID)->where('TOKEN',$q->TOKEN)->where('HRM_TYPE',$type)->delete('users_authentication');
                    }else{
                        // $token=$q->TOKEN;
                        return array('status' => 200,'message' => 'Already Login');
                    }
                }else{
                    $this->db->insert('users_authentication',array('HRM_ID' => $result[0]->USER_ID,'TOKEN' => $token,'EXPIRED_AT' => $expired_at,'HRM_TYPE' => $type));
                    $this->db->where('HRM_ID',$result[0]->USER_ID)->where('TOKEN',$q->TOKEN)->where('HRM_TYPE',$type)->delete('users_authentication');
                }
            }
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' => 'Internal server error.');
			} else {
				$this->db->where('USER_ID',$result[0]->USER_ID)->update('users',array('USER_LAST_LOGIN' => $last_login));
				$this->session->set_userdata('studentloginid',$result[0]->USER_ID);
				$this->session->set_userdata('studenttype','3'); /* for student login */
				$this->session->set_userdata('studenttoken',$token); 
				$this->db->trans_commit();
				return array('status' => 200,'message' => 'ok','id' => $result[0]->USER_ID, 'token' => $token);
			}
        }
    }
    public function studentregistration($data)
    {
        $this->db->insert('users',$data);
		$insertid =  $this->db->insert_id();
		if(!empty($insertid)){
            return $insertid;
		}else{
		    return '';
		}
		
    }
    public function studentregupdate($data,$id){
        $this->db->where('USER_ID',$id);
        $this->db->update('users',$data);
		return true;
    }
    public function logout()
    {
        $users_id= $this->input->get_request_header('User-ID', TRUE);
        $token= $this->input->get_request_header('Authorization', TRUE);
        $type= $this->input->get_request_header('type', TRUE);
        $this->db->where('HRM_ID',$users_id)->where('TOKEN',$token)->where('HRM_TYPE',$type)->delete('users_authentication');
        $this->session->unset_userdata('studentloginid');
        $this->session->unset_userdata('studenttype');
        $this->session->unset_userdata('studenttoken');
        return array('status' => 200,'message' => 'Successfully logout.');
    }
    public function addsuccesspayment($data){
        $this->db->insert('payment',$data);
		return $this->db->insert_id();
    }
    public function submittestpaper($data){
        $this->db->insert('student_result',$data);
		return $this->db->insert_id();
    }
    public function submit_ptpaper($data){
        $this->db->insert('practice_test_result',$data);
		return $this->db->insert_id();
    }
    public function seteditprofile($data,$id)
    {
        $this->db->where('USER_ID',$id);
        $this->db->update('users',$data);
        return true;
    }
    public function setpassword($data,$id)
    {
        $this->db->where('USER_ID',$id);
        $this->db->update('users',$data);
        return true;
    }
    public function addinvoice($data){
        $this->db->insert('invoice',$data);
		return $this->db->insert_id();
    }
}
?>