<?php
class Studentgetmodel extends CI_Model {
    public function check_student_exist($email)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from users where USER_EMAIL="'.$email.'" and USER_ROLES=3 ');
		$result = $query->result(); 
		$this->db->trans_commit();
		
		if(empty($result)){
		  //  return array('status' => 200,'message' => 'ok', 'data' => '');
		    return '';
		}else{
		  //	return array('status' => 404,'message' => 'Something went wrong!', 'data' => $result);
		   return $result;
		}
	
	}
	public function varify_otp($otp,$id){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from users where USER_OTP="'.$otp.'" and USER_ID="'.$id.'" ');
		$result = $query->result(); 
		$this->db->trans_commit();
		
		if(empty($result)){
		  //  return array('status' => 200,'message' => 'ok', 'data' => '');
		    return '';
		}else{
		  //	return array('status' => 404,'message' => 'Something went wrong!', 'data' => $result);
		   return $result;
		}
	}
	public function get_user_dt_by_id($id)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from users where USER_ID="'.$id.'" ');
		$result = $query->result(); 
		$this->db->trans_commit();
		
		if(empty($result)){
		  //  return array('status' => 200,'message' => 'ok', 'data' => '');
		    return '';
		}else{
		  //	return array('status' => 404,'message' => 'Something went wrong!', 'data' => $result);
		   return $result;
		}
	
	}
	public function get_student_dt_by_id($id)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from users where USER_ID="'.$id.'"');
		$result= $query->result();
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
	}
	public function get_all_tetseries()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from test_series  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		
		if(empty($result)){
		    return '';
		}else{
		   return $result;
		}
	
	}
    public function get_test_papers_by_tsid($tsid){
        $this->db->trans_start();
        $query  = $this->db->query('select * from test_paper where TS_ID="'.$tsid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(empty($result)){
		    return '';
		}else{
		   return $result;
		}
    }
    public function get_tetseries_dt_by_id($tsid){
        $this->db->trans_start();
        $query  = $this->db->query('select * from test_series where TS_ID="'.$tsid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(empty($result)){
		    return '';
		}else{
		   return $result;
		}
    }
    public function get_tetpaper_dt_by_id($tpid){
        $this->db->trans_start();
        $query  = $this->db->query('select * from test_paper where TP_ID="'.$tpid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(empty($result)){
		    return '';
		}else{
		   return $result;
		}
    }
     public function get_ques_log_by_quesid($tsid){
        $tsdt = $this->Studentgetmodel->get_tetseries_dt_by_id($tsid);
        $studentdt = $this->Studentgetmodel->get_user_dt_by_id($this->session->userdata('studentloginid'));
        $key = 'uhgBmIht';
        $txnid = $this->Studentgetmodel->generate_txn();
        $amount = $tsdt[0]->TS_COST;
        $pinfo = $tsdt[0]->TS_ID;
        $fname = $studentdt[0]->USER_FNAME.' '.$studentdt[0]->USER_LNAME;
        $email = $studentdt[0]->USER_EMAIL;
        $udf5 = "PCSKAKA_KIT";
        $salt = "s1pQEgYiT9";
        $phone = $studentdt[0]->USER_CONTACT;
        
        $hash=hash('sha512', $key.'|'.$txnid.'|'.$amount.'|'.$pinfo.'|'.$fname.'|'.$email.'|||||'.$udf5.'||||||'.$salt);
        $min['key'] = $key;
        $min['txnid'] = $txnid;
        $min['hash'] = $hash;
        $min['amount'] = $amount;
        $min['fname'] = $fname;
        $min['email'] = $email;
        $min['phone'] = $phone;
        $min['pinfo'] = $pinfo;
        $min['udf5'] = $udf5;

        // return "'$key','$txnid','$hash','$amount','$fname','$email','$phone','$pinfo','$udf5'";
        return $min;
    }
    public function generate_txn(){
       $txn =  "Txn".rand(111111,999999);
       $res = $this->Studentgetmodel->check_txn_exist($txn);
       if(!empty($res)){
           $this->Studentgetmodel->generate_txn($txn);
       }else{
           return $txn;
       }
    }
    public function check_payment_exist($tsid){
        $this->db->trans_start();
        $userid = $this->session->userdata('studentloginid');
        $query  = $this->db->query('select * from payment where TS_ID="'.$tsid.'" and USER_ID="'.$userid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		   return '';
		}
    }
    public function check_txn_exist($txnid){
        $this->db->trans_start();
        $query  = $this->db->query('select * from payment where PAY_TXN_NO="'.$txnid.'" ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		   return '';
		}
    }
    public function get_quesdt_by_id($ques_id){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from question where QUES_ID in ('.$ques_id.') ');
		$result= $query->result(); 
		$this->db->trans_commit();
		return $result;
	}
    
    public function get_testpaper_result($tsid,$tpid,$userid){
        
        $this->db->trans_start();
        $query  = $this->db->query('select * from student_result where USER_ID="'.$userid.'" and STD_RESULT_TYPE=1 and TP_ID="'.$tpid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    $tpdt = $this->Studentgetmodel->get_tetpaper_dt_by_id($tpid);
		    $tpname = $tpdt[0]->TP_NAME;
		    
    		$tsdt = $this->Studentgetmodel->get_tetseries_dt_by_id($tsid);
    		
            $positive_mark = $tsdt[0]->TS_P_MARKS;
            $negative_mark = $tsdt[0]->TS_N_MARKS;
            $total_ques = $tsdt[0]->TS_QUESTION_NOS;
            
            $max_mark = ceil($total_ques*$positive_mark);
            
		    $std_result = json_decode($result[0]->STD_RESULT);
		    $attempt = 0;
		    $attempt_boomark = 0;
		    $not_seen = 0;
		    $skipped = 0;
		    $bookmark = 0;
		    $bookmark_both = 0;
		    $not_visited = 0;
		    $obtained_mark = 0;
		    $number_of_true_ques = 0; 
		    $number_of_wrong_ques = 0; 
		    $rank = $result[0]->STD_RESULT_RANK;
		  //  $duration = $result[0]->STD_RESULT_DURATION;
		    foreach($std_result as $std_results){
		        
		        if($std_results->quesclass=="attempted"){
		            $attempt++;
		            $markdt = $this->Studentgetmodel->get_marked(url_decode($std_results->quesid),$std_results->ques_ans);
		            if($markdt){
		                $number_of_true_ques++;
		                $obtained_mark = $obtained_mark+$positive_mark;
		            }else{
		                $obtained_mark = $obtained_mark-$negative_mark;
		                $number_of_wrong_ques++; 
		            }
		        }else if($std_results->quesclass=="skipped"){
		            $skipped++;
		        }else if(empty($std_results->quesclass)){
		            $not_seen++;
		        }else if($std_results->quesclass=="attempted bookmarked"){
		            $attempt_boomark++;
		            $attempt++;
		            $bookmark_both++;
		            $markdt = $this->Studentgetmodel->get_marked(url_decode($std_results->quesid),$std_results->ques_ans);
		            if($markdt){
		                $number_of_true_ques++;
		                $obtained_mark = $obtained_mark+$positive_mark;
		            }else{
		                $obtained_mark = $obtained_mark-$negative_mark;
		                $number_of_wrong_ques++; 
		            }
		        }else if($std_results->quesclass=="bookmarked"){
		            $bookmark++;
		            $bookmark_both++;
		        }
		        
		    }
		    $data = array();
		    $data['TP_NAME'] = $tpname;
		    $data['RANK'] = $rank;
		    $data['attempted'] = $attempt;
		    $data['skipped'] = $skipped;
		    $data['not_visited'] = $not_seen;
		    $data['bookmark_both'] = $bookmark_both++;
		    $data['attempt_boomark'] = $attempt_boomark;
		    $data['boomark'] = $bookmark;
		    $data['obtained_mark'] = number_format((float)$obtained_mark, 2, '.', '');
		    if($data['obtained_mark']>0){
		        $data['percentage'] = $data['obtained_mark']*100/$max_mark;
		    }else{
		        $data['percentage'] = 0;
		    }
		  //  $data['DURATION'] = $duration;
		    $data['total_ques'] = $total_ques;
		    $data['max_mark'] = $max_mark;
		    $data['number_of_true_ques'] = $number_of_true_ques;
		    $data['number_of_wrong_ques'] = $number_of_wrong_ques;
		    return $data;
		}else{
		   return '';
		}
	}
	//Practice Test
	public function get_practicetestpaper_result($tsid,$tpid,$userid){
        
        $this->db->trans_start();
        $query  = $this->db->query('select * from practice_test_result where USER_ID="'.$userid.'" and STD_RESULT_TYPE=1 and TP_ID="'.$tpid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    $tpdt = $this->Studentgetmodel->get_practice_test_papers_by_id($tpid);
		    $tpname = $tpdt[0]->TP_NAME;
		    
    		$tsdt = $this->Studentgetmodel->get_practicetest_by_id($tsid);
    		
            $positive_mark = $tsdt[0]->TS_P_MARKS;
            $negative_mark = $tsdt[0]->TS_N_MARKS;
            $total_ques = $tsdt[0]->TS_QUESTION_NOS;
            
            $max_mark = ceil($total_ques*$positive_mark);
            
		    $std_result = json_decode($result[0]->STD_RESULT);
		    $attempt = 0;
		    $attempt_boomark = 0;
		    $not_seen = 0;
		    $skipped = 0;
		    $bookmark = 0;
		    $bookmark_both = 0;
		    $not_visited = 0;
		    $obtained_mark = 0;
		    $number_of_true_ques = 0; 
		    $number_of_wrong_ques = 0; 
		    $rank = $result[0]->STD_RESULT_RANK;
		  //  $duration = $result[0]->STD_RESULT_DURATION;
		    foreach($std_result as $std_results){
		        
		        if($std_results->quesclass=="attempted"){
		            $attempt++;
		            $markdt = $this->Studentgetmodel->get_marked(url_decode($std_results->quesid),$std_results->ques_ans);
		            if($markdt){
		                $number_of_true_ques++;
		                $obtained_mark = $obtained_mark+$positive_mark;
		            }else{
		                $obtained_mark = $obtained_mark-$negative_mark;
		                $number_of_wrong_ques++; 
		            }
		        }else if($std_results->quesclass=="skipped"){
		            $skipped++;
		        }else if(empty($std_results->quesclass)){
		            $not_seen++;
		        }else if($std_results->quesclass=="attempted bookmarked"){
		            $attempt_boomark++;
		            $attempt++;
		            $bookmark_both++;
		            $markdt = $this->Studentgetmodel->get_marked(url_decode($std_results->quesid),$std_results->ques_ans);
		            if($markdt){
		                $number_of_true_ques++;
		                $obtained_mark = $obtained_mark+$positive_mark;
		            }else{
		                $obtained_mark = $obtained_mark-$negative_mark;
		                $number_of_wrong_ques++; 
		            }
		        }else if($std_results->quesclass=="bookmarked"){
		            $bookmark++;
		            $bookmark_both++;
		        }
		        
		    }
		    $data = array();
		    $data['TP_NAME'] = $tpname;
		    $data['RANK'] = $rank;
		    $data['attempted'] = $attempt;
		    $data['skipped'] = $skipped;
		    $data['not_visited'] = $not_seen;
		    $data['bookmark_both'] = $bookmark_both++;
		    $data['attempt_boomark'] = $attempt_boomark;
		    $data['boomark'] = $bookmark;
		    $data['obtained_mark'] = number_format((float)$obtained_mark, 2, '.', '');
		    if($data['obtained_mark']>0){
		        $data['percentage'] = $data['obtained_mark']*100/$max_mark;
		    }else{
		        $data['percentage'] = 0;
		    }
		  //  $data['DURATION'] = $duration;
		    $data['total_ques'] = $total_ques;
		    $data['max_mark'] = $max_mark;
		    $data['number_of_true_ques'] = $number_of_true_ques;
		    $data['number_of_wrong_ques'] = $number_of_wrong_ques;
		    return $data;
		}else{
		   return '';
		}
    }
    public function get_marked($quesid,$ques_ans){
        $quesdt = $this->Studentgetmodel->get_quesdt_by_id($quesid);
        if($quesdt[0]->QUES_ANSWERS==$ques_ans){
            return true;
        }else{
            return false;
        }
    }
    public function check_attempt_testpaper($tpid){
        $userid = $this->session->userdata('studentloginid');
        $this->db->trans_start();
        $query  = $this->db->query('select * from student_result where USER_ID="'.$userid.'" and STD_RESULT_TYPE=1 and TP_ID="'.$tpid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return false;
		}else{
		    return true;
		}
	}
	public function check_attempt_practice_testpaper($tpid){
        $userid = $this->session->userdata('studentloginid');
        $this->db->trans_start();
		$query  = $this->db->query('select * from practice_test_result where USER_ID="'.$userid.'" and STD_RESULT_TYPE=1 and TP_ID="'.$tpid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return false;
		}else{
		    return true;
		}
    }
    public function get_student_result_dt($tpid){
        $userid = $this->session->userdata('studentloginid');
        $this->db->trans_start();
        $query  = $this->db->query('select * from student_result where USER_ID="'.$userid.'" and STD_RESULT_TYPE=1 and TP_ID="'.$tpid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		    return '';
		}
	}
	public function get_practice_test_result($tpid){
        $userid = $this->session->userdata('studentloginid');
        $this->db->trans_start();
        $query  = $this->db->query('select * from practice_test_result where USER_ID="'.$userid.'" and STD_RESULT_TYPE=1 and TP_ID="'.$tpid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		    return '';
		}
    }
    public function get_attempt_testpaper_dt(){
        $userid = $this->session->userdata('studentloginid');
        $this->db->trans_start();
        $query  = $this->db->query('select * from student_result where USER_ID="'.$userid.'" and STD_RESULT_TYPE=1 order by STD_RESULT_ID DESC ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		    return '';
		}
    }
    public function get_attempt_testpaper_resultdt_andname($result){
        $max['data'] = array();
        foreach($result as $results){
            $min = array();
            $tpdt = $this->Studentgetmodel->get_tetpaper_dt_by_id($results->TP_ID);
            $tsdt = $this->Studentgetmodel->get_tetseries_dt_by_id($tpdt[0]->TS_ID);
           	$min['TS_ID'] = $tsdt[0]->TS_ID;
           	$min['TS_NAME'] = $tsdt[0]->TS_NAME;
            $min['TP_NAME'] = $tpdt[0]->TP_NAME;
            $min['TP_ID'] = url_encode($tpdt[0]->TP_ID);
            $dt = $this->Studentgetmodel->get_testpaper_result($tpdt[0]->TS_ID,$tpdt[0]->TP_ID,$results->USER_ID);
            $min['RESULT'] = $dt;
            array_push($max['data'],$min);
        }
        return $max;
    }
    public function count_all_active_tetseries()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select count(*) as count from test_series where TS_STATUS=1 ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result[0]->count)){
		    return $result[0]->count;
		}else{
		   return 0;
		}
	}
	public function get_count_purchase_ts(){
        $this->db->trans_start();
        $userid = $this->session->userdata('studentloginid');
        $query  = $this->db->query('select count(*) as count from payment where  USER_ID="'.$userid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result[0]->count)){
		    return $result[0]->count;
		}else{
		   return 0;
		}
    }
    public function count_tspaper_by_tsids($tsids)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select count(*) as count from test_paper where TS_ID in ('.$tsids.') ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result[0]->count)){
		    return $result[0]->count;
		}else{
		   return 0;
		}
	}
	public function count_attempted_testpaper()
    {
		$this->db->trans_start();
		$userid = $this->session->userdata('studentloginid');
        $query  = $this->db->query('select count(*) as count from student_result where USER_ID="'.$userid.'" and STD_RESULT_TYPE=1  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result[0]->count)){
		    return $result[0]->count;
		}else{
		   return 0;
		}
	}
    public function get_student_progress(){
        $this->db->trans_start();
        $userid = $this->session->userdata('studentloginid');
        $currentdate = date('Y-m-d H:i:s');
        $query  = $this->db->query('select group_concat(TS_ID) as TS_ID from test_series where TS_START_DATE<="'.$currentdate.'" and TS_STATUS=1 ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result[0]->TS_ID)){
		    $ts_ids = $result[0]->TS_ID;
		    $total_testpaper = $this->Studentgetmodel->count_tspaper_by_tsids($ts_ids);
		    $attempted_testpaper = $this->Studentgetmodel->count_attempted_testpaper();
		    $data = array();
		    $data['TOTAL_TESTPAPER'] = $total_testpaper;
		    $data['ATTEMPTED_TESTPAPER'] = $attempted_testpaper;
		    $data['PERCENT'] = ($attempted_testpaper*100)/$total_testpaper;
		    return $data;
		}else{
		   return '';
		}
    }
    public function get_student_accuracy(){
        $this->db->trans_start();
        $userid = $this->session->userdata('studentloginid');
        $currentdate = date('Y-m-d H:i:s');
        $query  = $this->db->query('select * from student_result sr LEFT JOIN test_paper tp on tp.TP_ID=sr.TP_ID where sr.USER_ID="'.$userid.'" and sr.STD_RESULT_TYPE=1 order by sr.STD_RESULT_ID DESC ');
		$res = $query->result(); 
		$this->db->trans_commit();
		if(!empty($res)){
		    $percent = 0;
		    $totalpercent = 0;
		    foreach($res as $ress){
		        $result = $this->Studentgetmodel->get_testpaper_result($ress->TS_ID,$ress->TP_ID,$userid);
		        $percent += $result['percentage'];
		        $totalpercent += 100;
		    }
		    $data = array();
		    $data['accuracy'] = ($percent*100)/$totalpercent;
		    return $data;
		}else{
		   return '';
		}
    }
    public function get_attempted_testpaperdt(){
        $this->db->trans_start();
        $userid = $this->session->userdata('studentloginid');
        $currentdate = date('Y-m-d H:i:s');
        $query  = $this->db->query('select * from student_result sr LEFT JOIN test_paper tp on tp.TP_ID=sr.TP_ID where sr.USER_ID="'.$userid.'" and sr.STD_RESULT_TYPE=1 order by sr.STD_RESULT_ID DESC ');
		$res = $query->result(); 
		$this->db->trans_commit();
		$max['data'] = array();
		if(!empty($res)){
		    foreach($res as $ress){
		        $min = array();
		        $dt = $this->Studentgetmodel->get_testpaper_result($ress->TS_ID,$ress->TP_ID,$userid);
		        $min = $dt;
		        array_push($max['data'],$min);
		    }
		    return $max;
		}else{
		   return $max;
		}
    }
    public function get_last_invoice_no(){
        $this->db->trans_start();
        $query  = $this->db->query('select INVOICE_NO from payment order by PAY_ID DESC ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result[0]->INVOICE_NO)){
		    return $result[0]->INVOICE_NO;
		}else{
		   return 0;
		}
    }
    // public function check_payment_exist_by_prod_type($userid,$prod_type,$productid){
    //     $this->db->trans_start();
    //     $userid = $this->session->userdata('studentloginid');
    //     $query  = $this->db->query('select * from payment pay LEFT JOIN invoice inv on pay.INVOICE_NO=inv.INVOICE_NO where inv.PROD_TYPE_ID="'.$prod_type.'" and inv.PRODUCT_ID in ('.$productid.') and pay.USER_ID="'.$userid.'" and pay.PAY_STATUS="success"  ');
	// 	$result = $query->result(); 
	// 	$this->db->trans_commit();
	// 	if(!empty($result)){
	// 	    return $result;
	// 	}else{
	// 	   return '';
	// 	}
	// }
	public function get_invoicedt_by_user_type_and_id($userid,$prod_type,$productid){
		$this->db->trans_start();
        $userid = $this->session->userdata('studentloginid');
        $query  = $this->db->query('select * from payment pay LEFT JOIN invoice inv on pay.INVOICE_NO=inv.INVOICE_NO where inv.PROD_TYPE_ID="'.$prod_type.'" and inv.PRODUCT_ID in ('.$productid.') and pay.USER_ID="'.$userid.'" and pay.PAY_STATUS="success"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		   return '';
		}
	}
	public function check_payment_exist_by_prod_type($userid,$prod_type,$productid){
        if($prod_type=='0'){ //Course
			return $this->get_invoicedt_by_user_type_and_id($userid,$prod_type,$productid);
		}else if($prod_type==2){ //Subject
			$check=$this->get_invoicedt_by_user_type_and_id($userid,$prod_type,$productid);
			if(!empty($checkpayment)){
				return $check;
			}else{
				$productdt=$this->get_subject_by_id($productid);
				return $this->check_payment_exist_by_prod_type($userid,'0',$productdt[0]->COURSE_ID);
			}
		}else if($prod_type==3){ //book
			$check=$this->get_invoicedt_by_user_type_and_id($userid,$prod_type,$productid);
			if(!empty($checkpayment)){
				return $check;
			}else{
				$productdt=$this->get_booksdt_by_id($productid);
				return $this->check_payment_exist_by_prod_type($userid,'2',$productdt[0]->SUB_ID);
			}
		}else if($prod_type==6){ //Practice Test
			$check=$this->get_invoicedt_by_user_type_and_id($userid,$prod_type,$productid);
			if(!empty($check)){
				return $check;
			}else{
				$productdt=$this->get_practicetest_by_id($productid);
				return $this->check_payment_exist_by_prod_type($userid,'3',$productdt[0]->BOOK_ID);
			}
		}else if($prod_type==1){ //Exam or Library
			return $this->get_invoicedt_by_user_type_and_id($userid,$prod_type,$productid);
		}
		else if($prod_type==4){ //Test Series
			$check=$this->get_invoicedt_by_user_type_and_id($userid,$prod_type,$productid);
			if(!empty($check)){
				return $check;
			}else{
				$productdt=$this->get_tetseries_dt_by_id($productid);
				return $this->check_payment_exist_by_prod_type($userid,'1',$productdt[0]->LIBRARY_ID);
			}
		}

    }
    public function get_product_amount_by_prod_type($prod_type,$productid){
        $this->db->trans_start();
        if($prod_type==1){
            $query  = $this->db->query('select SUM(LIB_COST) as AMOUNT from library where LIB_ID in ('.$productid.') ');
        }
        if($prod_type==2){ 
            $query  = $this->db->query('select SUM(SUB_COST) as AMOUNT from subjects where SUB_ID in ('.$productid.') ');
        }
        if($prod_type==3){ 
            $query  = $this->db->query('select SUM(BOOK_COST) as AMOUNT from books where BOOK_ID in ('.$productid.') ');
        }
        if($prod_type==4){ 
            $query  = $this->db->query('select SUM(TS_COST) as AMOUNT from test_series where TS_ID in ('.$productid.') ');
        }
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result[0]->AMOUNT)){
		    return $result[0]->AMOUNT;
		}else{
		   return 0;
		}
    }
    
    public function get_payumoney_details($prod_type,$productid,$amount,$promoid="",$discid=""){
       
        $studentdt = $this->Studentgetmodel->get_user_dt_by_id($this->session->userdata('studentloginid'));
        // $key = 'uhgBmIht';
        $key = 'e8hqJOOq';
        $txnid = $this->Studentgetmodel->generate_txn();
        $amount = $amount;
        $pinfo = $productid;
        $fname = $studentdt[0]->USER_FNAME.' '.$studentdt[0]->USER_LNAME;
        $email = $studentdt[0]->USER_EMAIL;
        $udf5 = "PCSKAKA_KIT";
        $udf1 = $prod_type;
        $udf2 = $promoid;
        $udf3 = $discid;
        // $salt = "s1pQEgYiT9";
        $salt = "6DdFdvskcb";
        $phone = $studentdt[0]->USER_CONTACT;
        
        $hash=hash('sha512', $key.'|'.$txnid.'|'.$amount.'|'.$pinfo.'|'.$fname.'|'.$email.'|'.$udf1.'|'.$udf2.'|'.$udf3.'||'.$udf5.'||||||'.$salt);
        $min['key'] = $key;
        $min['txnid'] = $txnid;
        $min['hash'] = $hash;
        $min['amount'] = $amount;
        $min['fname'] = $fname;
        $min['email'] = $email;
        $min['phone'] = $phone;
        $min['pinfo'] = $pinfo;
        $min['udf5'] = $udf5;
        $min['udf1'] = $udf1;
        $min['udf2'] = $udf2;
        $min['udf3'] = $udf3;
        return $min;
    }
    
    public function get_discount_by_prod_type($prod_type,$coupontype,$productid){
        $this->db->trans_start();
        $productidarr = explode(',',$productid);
        $findquery = '(';
        $str = '';
        foreach($productidarr as $productidarrs){
            $str = 'or';
            $findquery .= ' FIND_IN_SET("'.$productidarrs.'",PRODUCT) '.$str.' ';
        }
        $findquery = substr($findquery, 0, -3).")";
        $currentdate = date('Y-m-d');
        $add_query = ' and COUPON_STATUS=1 and "'.$currentdate.'">=COUPON_START_DATE and "'.$currentdate.'"<=COUPON_END_DATE  ';
        $query  = $this->db->query('select * from coupon where PROD_TYPE_ID="'.$prod_type.'" and '.$findquery.' and COUPON_TYPE="'.$coupontype.'" '.$add_query.' ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		   return '';
		}
    }
  
    public function get_promo_dt($promo_input,$prod_type,$productid){
        $this->db->trans_start();
        $productidarr = explode(',',$productid);
        $findquery = '(';
        $str = '';
        foreach($productidarr as $productidarrs){
            $str = 'or';
            $findquery .= ' FIND_IN_SET("'.$productidarrs.'",PRODUCT) '.$str.' ';
        }
        $findquery = substr($findquery, 0, -3).")";
        $currentdate = date('Y-m-d');
        $add_query = ' and COUPON_STATUS=1 and "'.$currentdate.'">=COUPON_START_DATE and "'.$currentdate.'"<=COUPON_END_DATE and COUPON_USAGE_LIMIT>=COUPON_TOT_USES ';
        $query  = $this->db->query('select * from coupon where PROD_TYPE_ID="'.$prod_type.'" and '.$findquery.' and COUPON_TYPE=1 and COUPON_TITLE="'.$promo_input.'" '.$add_query.' ');
        
		$result = $query->result();
		$this->db->trans_commit();
		if(!empty($result)){
		    $promo_id = $result[0]->COUPON_ID;
		    
		    return $this->Studentgetmodel->get_promo_dt_by_promoid($promo_id);
		}else{
		   return '';
		}
    }
    public function get_coupon_detail($promo_input,$prod_type,$productid){
        $this->db->trans_start();
        $productidarr = explode(',',$productid);
        $findquery = '(';
        $str = '';
        foreach($productidarr as $productidarrs){
            $str = 'or';
            $findquery .= ' FIND_IN_SET("'.$productidarrs.'",PRODUCT) '.$str.' ';
        }
        $findquery = substr($findquery, 0, -3).")";
        $currentdate = date('Y-m-d');
        $add_query = ' and COUPON_STATUS=1 and "'.$currentdate.'">=COUPON_START_DATE and "'.$currentdate.'"<=COUPON_END_DATE and COUPON_USAGE_LIMIT>=COUPON_TOT_USES ';
        $query  = $this->db->query('select * from coupon where PROD_TYPE_ID="'.$prod_type.'" and '.$findquery.' and COUPON_TYPE=1 and COUPON_TITLE="'.$promo_input.'" '.$add_query.' ');
        
		$result = $query->result();
		$this->db->trans_commit();
		if(!empty($result)){
		    $promo_id = $result[0]->COUPON_ID;
		    
		    return $this->Studentgetmodel->get_promo_dt_by_promoid($promo_id);
		}else{
		   return '';
		}
    }
    public function get_promo_dt_by_promoid($promo_id){
        $this->db->trans_start();
        $currentdate = date('Y-m-d');
        $count_per_user = $this->Studentgetmodel->count_promo_uses_by_user($promo_id);
        $add_query = ' and COUPON_STATUS=1 and "'.$currentdate.'">=COUPON_START_DATE and "'.$currentdate.'"<=COUPON_END_DATE and COUPON_USAGE_LIMIT>COUPON_TOT_USES and COUPON_LIMIT_PER_USER>"'.$count_per_user.'" ';
        $query  = $this->db->query('select * from coupon where COUPON_ID="'.$promo_id.'" '.$add_query.' ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		   return '';
		}
    }
    public function count_promo_uses_by_user($promo_id){
        $this->db->trans_start();
        $currentdate = date('Y-m-d');
        $userid = $this->session->userdata('studentloginid');
        $query  = $this->db->query('select count(*) count from invoice inv LEFT JOIN payment pay on pay.INVOICE_NO=inv.INVOICE_NO where inv.USER_ID="'.$userid.'" and inv.COUPON_ID="'.$promo_id.'" and pay.PAY_STATUS="success" ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return $result[0]->count;
		}else{
		   return 0;
		}
    }
    public function get_coupon_dt_by_couponid($couponid){
        $this->db->trans_start();
        $currentdate = date('Y-m-d');
        $query  = $this->db->query('select * from coupon where COUPON_ID="'.$couponid.'" ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		   return '';
		}
	}
	public function deletenotpurchased_invoice_ofuser($userid){
		$this->db->trans_start();
		$paymentids=$this->db->query('SELECT GROUP_CONCAT(DISTINCT INVOICE_NO) AS INVOICE_NOS FROM `payment` WHERE USER_ID="'.$userid.'"')->row()->INVOICE_NOS;
		if($paymentids==""){
			$paymentids=0;
		}
        $query  = $this->db->query('DELETE FROM `invoice` WHERE `USER_ID` = "'.$userid.'" AND `INVOICE_NO` NOT IN ('.$paymentids.') ORDER BY `INVOICE_ID` ASC');
		$this->db->trans_commit();
	}



	public function get_all_courses($start,$limit)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from courses LEFT JOIN course_category cc on cc.CC_ID=courses.CC_ID  where COURSE_STATUS = 1 LIMIT '.$start.','. $limit);
		$result = $query->result(); 
		$this->db->trans_commit();
		return $result;
	
	}

	public function get_all_library($start,$limit)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from library where LIB_STATUS = 1 LIMIT '.$start.','. $limit);
		$result = $query->result(); 
		$this->db->trans_commit();
		return $result;
	
	}


	public function get_all_subjects($start,$limit)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from subjects where SUB_STATUS = 1 LIMIT '.$start.','. $limit);
		$result = $query->result(); 
		$this->db->trans_commit();
		return $result;
	
	}


	public function get_all_books($start,$limit)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from books where BOOK_STATUS = 1 LIMIT '.$start.','. $limit);
		$result = $query->result(); 
		$this->db->trans_commit();
		return $result;
	
	}

	public function get_product_type_count($where,$table)
    {
		$this->db->trans_start();
        $query  = $this->db->where($where)->get($table);
		$this->db->trans_commit();
		return $query->num_rows();
	}


	public function get_user_purchase_summary(){
		$userid = $this->session->userdata('studentloginid');
		$data = array();
		$data[0] = 0;
		$data[1] = 0;
		$data[2] = 0;
		$data[3] = 0;
		$data[4] = 0;

		foreach($this->db->get_where('invoice',["USER_ID"=>$userid])->result() as $row){

			if($this->db->get_where('payment',["INVOICE_NO"=>$row->INVOICE_NO,'PAY_STATUS'=>'success'])->num_rows()>0){
				$data[$row->PROD_TYPE_ID] += 1;
			}
		}
		return $data;
	}

	public function check_attempt_ts($tsid){
        $userid = $this->session->userdata('studentloginid');
        $this->db->trans_start();
        $query  = $this->db->query('select * from student_result where USER_ID="'.$userid.'" and STD_RESULT_TYPE=1 and TP_ID="'.$tpid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    return false;
		}else{
		    return true;
		}
    }



    public function get_testpaper_result2($tsid,$tpid,$userid){
        
        $this->db->trans_start();
        $query  = $this->db->query('select * from student_result where USER_ID="'.$userid.'" and STD_RESULT_TYPE=1 and TP_ID="'.$tpid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(!empty($result)){
		    $tpdt = $this->Studentgetmodel->get_tetpaper_dt_by_id($tpid);
		    $tpname = $tpdt[0]->TP_NAME;
		    
    		$tsdt = $this->Studentgetmodel->get_tetseries_dt_by_id($tsid);
    		
            $positive_mark = $tsdt[0]->TS_P_MARKS;
            $negative_mark = $tsdt[0]->TS_N_MARKS;
            $total_ques = $tsdt[0]->TS_QUESTION_NOS;
            
            $max_mark = ceil($total_ques*$positive_mark);
            $data = array();
            $i=0;
            foreach($result as $row){
            	$std_result = json_decode($row->STD_RESULT);
			    $attempt = 0;
			    $attempt_boomark = 0;
			    $not_seen = 0;
			    $skipped = 0;
			    $bookmark = 0;
			    $bookmark_both = 0;
			    $not_visited = 0;
			    $obtained_mark = 0;
			    $number_of_true_ques = 0; 
			    $number_of_wrong_ques = 0; 
			    $rank = $row->STD_RESULT_RANK;
			  //  $duration = $row->STD_RESULT_DURATION;
			    foreach($std_result as $std_results){
			        
			        if($std_results->quesclass=="attempted"){
			            $attempt++;
			            $markdt = $this->Studentgetmodel->get_marked(url_decode($std_results->quesid),$std_results->ques_ans);
			            if($markdt){
			                $number_of_true_ques++;
			                $obtained_mark = $obtained_mark+$positive_mark;
			            }else{
			                $obtained_mark = $obtained_mark-$negative_mark;
			                $number_of_wrong_ques++; 
			            }
			        }else if($std_results->quesclass=="skipped"){
			            $skipped++;
			        }else if(empty($std_results->quesclass)){
			            $not_seen++;
			        }else if($std_results->quesclass=="attempted bookmarked"){
			            $attempt_boomark++;
			            $attempt++;
			            $bookmark_both++;
			            $markdt = $this->Studentgetmodel->get_marked(url_decode($std_results->quesid),$std_results->ques_ans);
			            if($markdt){
			                $number_of_true_ques++;
			                $obtained_mark = $obtained_mark+$positive_mark;
			            }else{
			                $obtained_mark = $obtained_mark-$negative_mark;
			                $number_of_wrong_ques++; 
			            }
			        }else if($std_results->quesclass=="bookmarked"){
			            $bookmark++;
			            $bookmark_both++;
			        }
			        
			    }
			    
			    $data[$i]['TP_NAME'] = $tpname;
			    $data[$i]['RANK'] = $rank;
			    $data[$i]['attempted'] = $attempt;
			    $data[$i]['skipped'] = $skipped;
			    $data[$i]['not_visited'] = $not_seen;
			    $data[$i]['bookmark_both'] = $bookmark_both++;
			    $data[$i]['attempt_boomark'] = $attempt_boomark;
			    $data[$i]['boomark'] = $bookmark;
			    $data[$i]['obtained_mark'] = number_format((float)$obtained_mark, 2, '.', '');
			    if($data[$i]['obtained_mark']>0){
			        $data[$i]['percentage'] = $data[$i]['obtained_mark']*100/$max_mark;
			    }else{
			        $data[$i]['percentage'] = 0;
			    }
			  //  $data[$i]['DURATION'] = $duration;
			    $data[$i]['total_ques'] = $total_ques;
			    $data[$i]['max_mark'] = $max_mark;
			    $data[$i]['number_of_true_ques'] = $number_of_true_ques;
			    $data[$i]['number_of_wrong_ques'] = $number_of_wrong_ques;
			    $i++;
            }
		    
		    return $data;
		}else{
		   return '';
		}
    }



	public function get_all_unattempted_or_attempted_result_ts(){
		$userid = $this->session->userdata('studentloginid');
		$data = array();
		$total = 0;
		$attempted = 0;
		$attendance = 0;
		foreach($this->db->get_where('invoice',["USER_ID"=>$userid,"PROD_TYPE_ID"=>4])->result() as $row){

			if($this->db->get_where('payment',["INVOICE_NO"=>$row->INVOICE_NO,'PAY_STATUS'=>'success'])->num_rows()>0){
				$tsdt = $this->Studentgetmodel->get_tetseries_dt_by_id($row->PRODUCT_ID);
				$tpdt = $this->Studentgetmodel->get_test_papers_by_tsid($row->PRODUCT_ID);
				if(!empty($tpdt)){
					foreach($tpdt as $paper){
						$total++;
						$result = $this->Studentgetmodel->get_testpaper_result2($row->PRODUCT_ID,$paper->TP_ID,$userid);
						if(!empty($result)){
							$attendance++;
							$attempted += count($result);
							$data[] = [
								"TS_ID" => $tsdt[0]->TS_ID,
								"TS_NAME" => $tsdt[0]->TS_NAME,
								"TS_QUESTION_NOS" => $tsdt[0]->TS_QUESTION_NOS,
								"MAX_MARK" => ceil($tsdt[0]->TS_QUESTION_NOS*$tsdt[0]->TS_P_MARKS),
								"TP_ID" => url_encode($paper->TP_ID),
								"TP_NAME" => $paper->TP_NAME,
								"RESULT" => $result
							];
						}else{
							$data[] = [
								"TS_ID" => $tsdt[0]->TS_ID,
								"TS_NAME" => $tsdt[0]->TS_NAME,
								"TS_QUESTION_NOS" => $tsdt[0]->TS_QUESTION_NOS,
								"MAX_MARK" => ceil($tsdt[0]->TS_QUESTION_NOS*$tsdt[0]->TS_P_MARKS),
								"TP_ID" => url_encode($paper->TP_ID),
								"TP_NAME" => $paper->TP_NAME,
								"RESULT" => '',
							];
						}
						
					}
				}

			}
		}
		return [
			"total" => $total,
			"attempted" => $attempted,
			"attendance" => $attendance,
			"data" => $data 
		];
	}

	public function get_course_by_id($course_id){
		$this->db->trans_start();
        $query  = $this->db->where('COURSE_ID',$course_id)->get('courses');
		$this->db->trans_commit();
		return $query->result();
	}


	public function get_all_popular_tetseries()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from test_series  where TS_POPULAR = 1 AND TS_STATUS = 1');
		$result = $query->result(); 
		$this->db->trans_commit();
		
		if(empty($result)){
		    return '';
		}else{
		   return $result;
		}
	
	}


	public function get_all_suggested_tetseries()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from test_series  where TS_SUGGESTED = 1 AND TS_STATUS = 1');
		$result = $query->result(); 
		$this->db->trans_commit();
		
		if(empty($result)){
		    return '';
		}else{
		   return $result;
		}
	
	}

	public function get_all_test_papers(){
        $this->db->trans_start();
        $query  = $this->db->query('select * from test_paper where TP_STATUS = 1');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(empty($result)){
		    return '';
		}else{
		   return $result;
		}
    }

    public function get_all_suggested_test_papers(){
        $this->db->trans_start();
        $query  = $this->db->query('select * from test_paper WHERE TP_SUGGESTED = 1 AND TP_STATUS = 1');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(empty($result)){
		    return '';
		}else{
		   return $result;
		}
    }

    public function get_all_current_affairs_test_papers(){
        $this->db->trans_start();
        $query  = $this->db->query('select * from test_paper where TP_CURRENT_AFFAIRS = 1 AND TP_STATUS = 1');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(empty($result)){
		    return '';
		}else{
		   return $result;
		}
    }


    public function get_upcoming_popular_library(){
    	$currentdate = date("Y-m-d");
		$this->db->trans_start();
        $query  = $this->db->query("SELECT * FROM library WHERE LIB_START_DATE > '$currentdate' AND LIB_STATUS = 1 AND LIB_POPULAR = 1");
		$this->db->trans_commit();
		return $query->result();
	}


	public function get_course_category(){
		$this->db->trans_start();
        $query  = $this->db->query("SELECT * FROM course_category WHERE CC_STATUS = 1");
		$this->db->trans_commit();
		return $query->result();
	}

	public function get_course_by_cc_id($cc_id){
		$this->db->trans_start();
        $query  = $this->db->query("SELECT * FROM courses WHERE CC_ID = '$cc_id' AND COURSE_STATUS = 1");
		$this->db->trans_commit();
		return $query->result();
	}


	public function get_library_by_course_id($course_id){
		$this->db->trans_start();
        $query  = $this->db->query("SELECT * FROM library WHERE COURSE_ID = '$course_id' AND LIB_STATUS = 1");
		$this->db->trans_commit();
		return $query->result();
	}

	public function current_year_exams(){
		$currentyear = date("Y");
		$this->db->trans_start();
        $query  = $this->db->query("SELECT * FROM library WHERE YEAR(LIB_START_DATE) = '$currentyear' AND LIB_STATUS = 1");
		$this->db->trans_commit();
		return $query->result();
	}

	public function get_active_library_by_lib_id($lib_id){
		$this->db->trans_start();
        $query  = $this->db->query("SELECT * FROM library WHERE LIB_ID = '$lib_id' AND LIB_STATUS = 1");
		$this->db->trans_commit();
		return $query->result();
	}

	public function get_active_testseries_by_lib_id($lib_id){
		$this->db->trans_start();
        $query  = $this->db->query("SELECT * FROM test_series WHERE LIBRARY_ID = '$lib_id' AND TS_STATUS = 1");
		$this->db->trans_commit();
		return $query->result();
	}

	public function get_all_suggested_sliders($prod_type=""){
		$this->db->trans_start(); 
		$this->db->select('*');
		$this->db->from('slider');
		if($prod_type!=""){
			$this->db->where('PROD_TYPE',$prod_type);
		}
		$this->db->where('SLIDER_STATUS','1');
		$query=$this->db->get();
		$this->db->trans_commit();
		return $query->result();
	}

	public function get_subjectdt_by_course_id($courseid){ //Subject
		$this->db->trans_start();
		$this->db->select('*');
		$this->db->from('subjects');
		$this->db->where('COURSE_ID',$courseid);
		$query=$this->db->get();
		$this->db->trans_commit();
		return $query->result();
	}
	public function get_bookdt_by_course_id($courseid){ //book
		$this->db->trans_start();
		$this->db->select('*');
		$this->db->from('books');
		$this->db->where('COURSE_ID',$courseid);
		$query=$this->db->get();
		$this->db->trans_commit();
		return $query->result();
	}
	public function get_booksdt_by_sub_id($subid){ //book
		$this->db->trans_start();
		$this->db->select('*');
		$this->db->from('books');
		$this->db->where('SUB_ID',$subid);
		$query=$this->db->get();
		$this->db->trans_commit();
		return $query->result();
	}
	public function get_booksdt_by_id($bookid){ //book
		$this->db->trans_start();
		$this->db->select('*');
		$this->db->from('books');
		$this->db->where('BOOK_ID',$bookid);
		$query=$this->db->get();
		$this->db->trans_commit();
		return $query->result();
	}
	public function get_subject_by_id($subjectid){ //Subject
		$this->db->trans_start();
		$this->db->select('*');
		$this->db->from('subjects');
		$this->db->where('SUB_ID',$subjectid);
		$query=$this->db->get();
		$this->db->trans_commit();
		return $query->result();
	}
	public function get_practicetest_by_book_id($bookid){ //Practice Test by book id
		$this->db->trans_start();
		$this->db->select('*');
		$this->db->from('practice_test');
		$this->db->where('BOOK_ID',$bookid);
		$query=$this->db->get();
		$this->db->trans_commit();
		return $query->result();
	}
	public function get_practicetest_by_id($pt_id){ //Practice Test by id
		$this->db->trans_start();
		$this->db->select('*');
		$this->db->from('practice_test');
		$this->db->where('TS_ID',$pt_id);
		$query=$this->db->get();
		$this->db->trans_commit();
		return $query->result();
	}

	public function get_practice_test_papers_by_ptsid($tsid){
        $this->db->trans_start();
        $query  = $this->db->query('select * from practice_test_paper where TS_ID="'.$tsid.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(empty($result)){
		    return '';
		}else{
		   return $result;
		}
	}

	public function get_practice_test_papers_by_id($id){
        $this->db->trans_start();
        $query  = $this->db->query('select * from practice_test_paper where TP_ID="'.$id.'"  ');
		$result = $query->result(); 
		$this->db->trans_commit();
		if(empty($result)){
		    return '';
		}else{
		   return $result;
		}
	}
	
	public function get_bookdt_by_id($bookid){ //book
		$this->db->trans_start();
		$this->db->select('*');
		$this->db->where('BOOK_ID',$bookid);
		$query=$this->db->get('books');
		$this->db->trans_commit();
		return $query->result();
	}
}

?>