<?php
class Sitegetmodel extends CI_Model {
    
    public function get_admin_dt_model()
    {
        $state = "INDIA ( IND )";
		$this->db->trans_start();
        $query  = $this->db->query('select * from cities where COUNTRY_NAME="'.$state.'"');
		$result= $query->result();
		$this->db->trans_commit();
		return $result;
	}
	
	 public function get_subject_dt_model()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from subjects where SUB_STATUS IN (1)');
		$result= $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function ques_count() {
        return $this->db->count_all("question");
    }

    public function get_records($limit, $offset) {
		$str="";
		if($this->input->get('bk')){
			$str=" and  BOOK_ID='".$this->input->get('bk')."'";
		}
		$query  = $this->db->query('select * from question where SUB_ID=7 '.$str.' LIMIT '.$limit.' OFFSET '.$offset.' ');
		$result= $query->result();
        if (!empty($result)) {
            return $result;
        }else{
            return '';
        }
   }
   public function get_current_affairs_ques_count($subid,$bookid="")
    {
		$this->db->trans_start();
		$str="";
		if($bookid!=''){
			$str = 'and BOOK_ID='.$bookid.' ';
		}
        $query  = $this->db->query('select count(*) as count from question where SUB_ID IN ('.$subid.') '.$str.' and QUES_STATUS=1 ');
		$result= $query->result();
		$this->db->trans_commit();
		return  $result[0]->count;
	}
   public function get_all_test_series()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from test_series ');
		$result= $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_test_series_dt_by_id($tsid)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from test_series where TS_ID="'.$tsid.'" ');
		$result= $query->result();
		$this->db->trans_commit();
		return $result;
	}
}
?>