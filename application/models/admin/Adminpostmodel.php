<?php 
class Adminpostmodel extends CI_Model {

    public function login($username,$password,$type,$loginagain="")
    {
        $query  = $this->db->query('select * from users where ( USER_EMAIL="'.$username.'" OR USER_CONTACT="'.$username.'" ) and USER_PASSWORD="'.$password.'" and USER_ROLES="'.$type.'" and USER_STATUS=1');
		$result= $query->result();
        if(empty($result)){
            return array('status' => 200,'message' => 'Invalid Login Credential');
        } else {
			$last_login = date('Y-m-d H:i:s');
			$token = crypt(substr( md5(rand()), 0, 7),rand());
			$expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
			$ip = $this->input->ip_address();
			$this->db->trans_start();
			$q  = $this->db->select('*')->from('users_authentication')->where('HRM_ID',$result[0]->USER_ID)->where('HRM_TYPE',$type)->get()->row();
			if($q == ""){
			    $this->db->insert('users_authentication',array('HRM_ID' => $result[0]->USER_ID,'TOKEN' => $token,'EXPIRED_AT' => $expired_at,'HRM_TYPE' => $type));
            }else{
                if(empty($loginagain)){
                    if($q->EXPIRED_AT < date('Y-m-d H:i:s')){
                        $this->db->insert('users_authentication',array('HRM_ID' => $result[0]->USER_ID,'TOKEN' => $token,'EXPIRED_AT' => $expired_at,'HRM_TYPE' => $type));
                        $this->db->where('HRM_ID',$result[0]->USER_ID)->where('TOKEN',$q->TOKEN)->where('HRM_TYPE',$type)->delete('users_authentication');
                    }else{
                        // $token=$q->TOKEN;
                        return array('status' => 200,'message' => 'Already Login');
                    }
                }else{
                    $this->db->insert('users_authentication',array('HRM_ID' => $result[0]->USER_ID,'TOKEN' => $token,'EXPIRED_AT' => $expired_at,'HRM_TYPE' => $type));
                    $this->db->where('HRM_ID',$result[0]->USER_ID)->where('TOKEN',$q->TOKEN)->where('HRM_TYPE',$type)->delete('users_authentication');
                }
            }
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' => 'Internal server error.');
			} else {
				$this->db->where('USER_ID',$result[0]->USER_ID)->update('users',array('USER_LAST_LOGIN' => $last_login));
				$this->session->set_userdata('adminloginid',$result[0]->USER_ID);
				$this->session->set_userdata('admintype','1'); /* for admin login */
				$this->session->set_userdata('admintoken',$token); 
				$this->db->trans_commit();
				return array('status' => 200,'message' => 'ok','id' => $result[0]->USER_ID, 'token' => $token);
			}
        }
    }
    
    public function logout()
    {
        $users_id= $this->input->get_request_header('User-ID', TRUE);
        $token= $this->input->get_request_header('Authorization', TRUE);
        $type= $this->input->get_request_header('type', TRUE);
        $this->db->where('HRM_ID',$users_id)->where('TOKEN',$token)->where('HRM_TYPE',$type)->delete('users_authentication');
        $this->session->unset_userdata('adminloginid');
        $this->session->unset_userdata('admintype');
        $this->session->unset_userdata('admintoken');
        return array('status' => 200,'message' => 'Successfully logout.');
    }
    public function addlibrary($data)
    {
        $this->db->insert('library',$data);
		return $this->db->insert_id();
    }
    public function updatelibrary($data,$id)
    {
        $this->db->where('LIB_ID',$id);
		$this->db->update('library',$data);
		return true;
    }
    public function addsubject($data)
    {
        $this->db->insert('subjects',$data);
		return $this->db->insert_id();
    }
    public function updatesubject($data,$id)
    {
        $this->db->where('SUB_ID',$id);
		$this->db->update('subjects',$data);
		return true;
    }
    public function updatesubject_by_lib($data,$id)
    {
        $this->db->where('LIB_ID',$id);
		$this->db->update('subjects',$data);
		return $this->db->affected_rows();
    }
    public function addbook($data)
    {
        $this->db->insert('books',$data);
		return $this->db->insert_id();
    }
    public function updatebook($data,$id)
    {
        $this->db->where('BOOK_ID',$id);
		$this->db->update('books',$data);
		return true;
    }
    public function updatebook_by_lib($data,$id)
    {
        $this->db->where('LIB_ID',$id);
		$this->db->update('books',$data);
		return $this->db->affected_rows();
    }
    public function addchapter($data)
    {
        $this->db->insert('chapter',$data);
		return $this->db->insert_id();
    }
    public function updatechapter($data,$id)
    {
        $this->db->where('CHAPTER_ID',$id);
		$this->db->update('chapter',$data);
		return true;
    }
    public function addquestion($data)
    {
        $this->db->insert('question',$data);
		return $this->db->insert_id();
    }
    
    public function updatequestion($data,$id)
    {
        $this->db->where('QUES_ID',$id);
		$this->db->update('question',$data);
		return true;
    }
    
    public function approveques($data,$id)
    {
        $this->db->where('QUES_ID',$id);
		$this->db->update('question',$data);
		return true;
    }
    public function addtestseries($data)
    {
        $this->db->insert('test_series',$data);
		return $this->db->insert_id();
    }
    public function addpracticetest($data)
    {
        $this->db->insert('practice_test',$data);
		return $this->db->insert_id();
    }
    public function addtestpaper($data)
    {
        $this->db->insert('test_paper',$data);
		return $this->db->insert_id();
    }
    public function updatetestseries($data,$id)
    {
        $this->db->where('TS_ID',$id);
		$this->db->update('test_series',$data);
		return true;
    }
    public function updatepracticetest($data,$id)
    {
        $this->db->where('TS_ID',$id);
		$this->db->update('practice_test',$data);
		return true;
    }
    public function addpracticetestpaper($data)
    {
        $this->db->insert('practice_test_paper',$data);
		return $this->db->insert_id();
    }
    
    public function updatetestpaper($data,$id)
    {
        $this->db->where('TP_ID',$id);
		$this->db->update('test_paper',$data);
		return true;
    }
    public function updatepracticetestpaper($data,$id)
    {
        $this->db->where('TP_ID',$id);
		$this->db->update('practice_test_paper',$data);
		return true;
    }
    public function deleteques($id)
    {
        $this->db->where('QUES_ID',$id);
		$this->db->delete('question');
		return true;
    }
    public function update_test_paper_status($tpid,$tsid){
        $tsdt = $this->Admingetmodel->get_testseriesdt_by_id($tsid);
        $tpdt = $this->Admingetmodel->get_testpaperdt_by_id($tpid);
        $addedquesintparr = json_decode($tpdt[0]->TP_QUESTIONS);
        if(sizeof($addedquesintparr)==$tsdt[0]->TS_QUESTION_NOS){
            $data = array(
                'TP_STATUS'=> 1,
                'TP_LAST_UPDATED' => date('Y-m-d H:i:s')
            );
            $testpaperid=$this->Adminpostmodel->updatetestpaper($data,$tpid);
        }
    }
    public function update_practice_test_paper_status($tpid,$tsid){
        $tsdt = $this->Admingetmodel->get_practicetestdt_by_id($tsid);
        $tpdt = $this->Admingetmodel->get_practicetestpaperdt_by_id($tpid);
        $addedquesintparr = json_decode($tpdt[0]->TP_QUESTIONS);
        if(sizeof($addedquesintparr)==$tsdt[0]->TS_QUESTION_NOS){
            $data = array(
                'TP_STATUS'=> 1,
                'TP_LAST_UPDATED' => date('Y-m-d H:i:s')
            );
            $testpaperid=$this->Adminpostmodel->updatepracticetestpaper($data,$tpid);
        }
    }
    public function addmember($data)
    {
        $this->db->insert('users',$data);
		return $this->db->insert_id();
    }
    public function updatemember($data,$id)
    {
        $this->db->where('USER_ID',$id);
		$this->db->update('users',$data);
		return true;
    }
    public function update_practicetest_papersby_ptid($data,$id)
    {
        $this->db->where('TS_ID',$id);
		$this->db->update('practice_test_paper',$data);
		return true;
    }
    public function update_ques_log($quesid,$queslogaction,$userid){
        $userdt = $this->Admingetmodel->get_admin_dt_model($userid);
        $data = array(
            'QUES_ID' => $quesid,
            'QUES_LOG_ACTION' => $queslogaction,
            'USER_ID' => $userid,
            'USER_ROLES' => $userdt['data'][0]->USER_ROLES,
            'QUES_LOG_TT' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('question_log',$data);
		return $this->db->insert_id();
    }
    public function addcoupon($data)
    {
        $this->db->insert('coupon',$data);
		return $this->db->insert_id();
    }
    public function updatecoupon($data,$id)
    {
        $this->db->where('COUPON_ID',$id);
		$this->db->update('coupon',$data);
		return true;
    }
    public function addtemplate($data)
    {
        $this->db->insert('msgtemplate',$data);
		return $this->db->insert_id();
    }
    public function setscheduling($data)
    {
        $this->db->insert('msgschedule',$data);
		return $this->db->insert_id();
    }
    public function updatetemplate($data,$id)
    {
        $this->db->where('MSGTEMP_ID',$id);
		$this->db->update('msgtemplate',$data);
		return true;
    }
    public function update_scheduling($data,$id)
    {
		$this->db->where('SCH_ID',$id);
		$this->db->update('msgschedule',$data);
		return true;
    }
    public function addcourse($data)
    {
        $this->db->insert('courses',$data);
		return $this->db->insert_id();
    }
    public function updatecourse($data,$id)
    {
        $this->db->where('COURSE_ID',$id);
		$this->db->update('courses',$data);
		return true;
    }
    public function addslider($data)
    {
        $this->db->insert('slider',$data);
		return $this->db->insert_id();
    }
    public function updateslider($data,$id)
    {
        $this->db->where('SLIDER_ID',$id);
		$this->db->update('slider',$data);
		return true;
    }

    /*Shubham 2020-06-13*/
    public function update_ques_used_in_tp($ques_id_arr){
        if(count($ques_id_arr)>0){
           $this->db->where_in('QUES_ID',$ques_id_arr);
           $this->db->update('question',array("QUES_USED_IN_TP"=>1));
           return true; 
        }else{
            return false;
        }
    }
    /**/
}
?>