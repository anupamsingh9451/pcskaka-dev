<?php
class Admingetmodel extends CI_Model {

	public function get_admin_dt_model($id) {
		$this->db->trans_start();
		$query = $this->db->query('select * from users where USER_ID="' . $id . '"');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}

	public function get_library_by_name($libname) {
		$this->db->trans_start();
		$query = $this->db->query('select * from library where LIB_NAME="' . $libname . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}
	public function get_all_library($course_id = "") {
		$str = "";
		if ($course_id != "") {
			$str .= ' and COURSE_ID="' . $course_id . '"';
		}
		$this->db->trans_start();
		$query = $this->db->query('select * from library where LIB_STATUS=1 ' . $str . ' order by LIB_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
		return $result;
	}
	public function get_all_librarys() {
		$this->db->trans_start();
		$query = $this->db->query('select * from library LEFT JOIN courses on courses.COURSE_ID=library.COURSE_ID order by LIB_ID DESC');
		//$query  = $this->db->query('select * from library order by LIB_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
		return $result;
	}
	public function get_subject_by_name_and_libid($subjname, $libid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from subjects where SUB_NAME="' . $subjname . '" and LIB_ID="' . $libid . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}
	public function get_all_subject() {
		$this->db->trans_start();
		$query = $this->db->query('select * from subjects s LEFT JOIN library l on l.LIB_ID=s.LIB_ID order by SUB_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_subids() {
		$this->db->trans_start();
		$query = $this->db->query('select GROUP_CONCAT(DISTINCT SUB_ID) as SUB_ID from subjects ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result[0]->SUB_ID;
	}
	public function get_all_subjects() {
		$this->db->trans_start();
		$query = $this->db->query('select * from subjects s LEFT JOIN library l on l.LIB_ID=s.LIB_ID where SUB_STATUS=1 order by SUB_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}
	public function check_subject_exist($subjname, $libid, $subid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from subjects where SUB_NAME="' . $subjname . '" and LIB_ID="' . $libid . '" and SUB_ID NOT IN (' . $subid . ') ');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}
	public function get_subject_by_libid($libid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from subjects where LIB_ID in (' . $libid . ') and SUB_STATUS=1 ');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}
	public function check_book_exist($bookname, $subid, $bookid = '') {
		$this->db->trans_start();
		if ($bookid == '') {
			$query = $this->db->query('select * from books where BOOK_NAME="' . $bookname . '" and SUB_ID="' . $subid . '" ');
		} else {
			$query = $this->db->query('select * from books where BOOK_NAME="' . $bookname . '" and SUB_ID="' . $subid . '" and BOOK_ID NOT IN (' . $bookid . ') ');
		}
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_book() {
		$this->db->trans_start();
		$query = $this->db->query('select * from books b LEFT JOIN subjects s on s.SUB_ID=b.SUB_ID LEFT JOIN library l on l.LIB_ID=b.LIB_ID order by b.BOOK_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_book_by_subid($subid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from books b LEFT JOIN subjects s on s.SUB_ID=b.SUB_ID LEFT JOIN library l on l.LIB_ID=b.LIB_ID where b.SUB_ID in (' . $subid . ') and b.BOOK_STATUS=1 ');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}
	public function get_bookids_by_subid($subid) {
		$this->db->trans_start();
		$query = $this->db->query('select GROUP_CONCAT(DISTINCT BOOK_ID) as BOOK_ID from books where SUB_ID in (' . $subid . ')');
		$result = $query->result();
		$this->db->trans_commit();
		return $result[0]->BOOK_ID;
	}
	public function get_all_active_books() {
		$this->db->trans_start();
		$query = $this->db->query('select * from books b LEFT JOIN subjects s on s.SUB_ID=b.SUB_ID LEFT JOIN library l on l.LIB_ID=b.LIB_ID WHERE b.BOOK_STATUS="1" order by b.BOOK_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}
	public function get_booksdt_by_id($bookid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from books b LEFT JOIN subjects s on s.SUB_ID=b.SUB_ID LEFT JOIN library l on l.LIB_ID=b.LIB_ID where b.BOOK_ID="' . $bookid . '"  ');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}
	public function check_chapter_exist($chaptername, $bookid, $chapterid = '') {
		$this->db->trans_start();
		if ($chapterid == '') {
			$query = $this->db->query('select * from chapter where CHAPTER_NAME="' . $chaptername . '" and BOOK_ID="' . $bookid . '" ');
		} else {
			$query = $this->db->query('select * from chapter where CHAPTER_NAME="' . $chaptername . '" and BOOK_ID="' . $bookid . '" and CHAPTER_ID NOT IN (' . $chapterid . ') ');
		}
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}

	public function get_all_chapter_by_bookid($bookid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from chapter c LEFT JOIN books b on b.BOOK_ID=c.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=c.SUB_ID LEFT JOIN library l on l.LIB_ID=c.LIB_ID where c.BOOK_ID="' . $bookid . '" order by c.CHAPTER_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_chaptersdt_by_id($chapterid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from chapter c LEFT JOIN books b on b.BOOK_ID=c.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=c.SUB_ID LEFT JOIN library l on l.LIB_ID=c.LIB_ID where c.CHAPTER_ID="' . $chapterid . '"  ');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}

	public function get_all_ques_by_chapterid($chapterid, $userid = '', $level = "", $status = "") {
		$this->db->trans_start();
		if ($userid != '') {
			$query = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID in (' . $chapterid . ') and q.QUES_CREATED_BY in (' . $userid . ') and q.QUES_STATUS in (' . $status . ') and q.QUES_DIFFICULTY_LEVEL in (' . $level . ') order by q.QUES_ID DESC');
		} else {
			$query = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID="' . $chapterid . '" and q.QUES_STATUS!=2 order by q.QUES_ID DESC');
		}
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_quesdt_by_id($ques_id) {
		$this->db->trans_start();
		$query = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.QUES_ID in (' . $ques_id . ') ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_user_role_by_id($id) {
		$this->db->trans_start();
		$query = $this->db->query('select * from users u LEFT JOIN user_role ur on ur.USER_ROLE_ID=u.USER_ROLES where u.USER_ID="' . $id . '"  ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_difficulty_level($level) {
		if ($level == 1) {
			$result = "Low";
		} else if ($level == 2) {
			$result = "Medium";
		} else {
			$result = "Heigh";
		}
		return $result;
	}
	public function get_all_users() {
		$this->db->trans_start();
		$query = $this->db->query('select * from users where USER_ROLES!=3 order by USER_ID ASC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_ques_by_chapterid_questype($chapterid, $questype) {
		$this->db->trans_start();
		$query = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID in (' . $chapterid . ') and q.QUES_TYPE in (' . $questype . ')  and QUES_STATUS=1 order by q.QUES_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_ques_by_chapterid_difflevel($chapterid, $level, $questype = "") {
		if ($questype != '') {
			$questype = ' and q.QUES_TYPE=2 ';
		}
		$this->db->trans_start();
		$query = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID where q.CHAPTER_ID in (' . $chapterid . ') and q.QUES_DIFFICULTY_LEVEL in (' . $level . ') ' . $questype . '  and QUES_STATUS=1 order by q.QUES_ID ASC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_chapterids_by_booksid($bookid) {
		$this->db->trans_start();
		$query = $this->db->query('select GROUP_CONCAT(DISTINCT CHAPTER_ID) as CHAPTER_ID from chapter where BOOK_ID in (' . $bookid . ')  ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result[0]->CHAPTER_ID;
	}
	public function get_pending_ques($chapterid) {
		$this->db->trans_start();
		$query = $this->db->query('select count(*) as count from question where CHAPTER_ID="' . $chapterid . '" and QUES_APPROVED_BY=0 ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result[0]->count;
	}
	public function get_all_testseries() {
		$this->db->trans_start();
		$query = $this->db->query('select * from test_series ts LEFT JOIN user_role ur on ur.USER_ROLE_ID=ts.TS_CREATED_BY order by ts.TS_ID ASC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_practicetest() {
		$this->db->trans_start();
		$query = $this->db->query('select * from practice_test ts LEFT JOIN user_role ur on ur.USER_ROLE_ID=ts.TS_CREATED_BY order by ts.TS_ID ASC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_testseriesids() {
		$this->db->trans_start();
		$query = $this->db->query('select GROUP_CONCAT(TS_ID) TS_ID from test_series order by TS_ID ASC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result[0]->TS_ID;
	}
	public function get_all_active_testseries() {
		$this->db->trans_start();
		$query = $this->db->query('select * from test_series ts LEFT JOIN user_role ur on ur.USER_ROLE_ID=ts.TS_CREATED_BY where ts.TS_STATUS=1 order by ts.TS_ID ASC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function check_testseries_exist($tsname) {
		$this->db->trans_start();
		$query = $this->db->query('select * from practice_test where TS_NAME="' . $tsname . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function check_practicetest_exist($tsname) {
		$this->db->trans_start();
		$query = $this->db->query('select * from test_series where TS_NAME="' . $tsname . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_testseriesdt_by_id($tsid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from test_series where TS_ID="' . $tsid . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_practicetestdt_by_id($tsid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from practice_test where TS_ID="' . $tsid . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_testpaper_by_id($tpid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from test_paper tp LEFT JOIN user_role ur on ur.USER_ROLE_ID=tp.TP_CREATED_BY where tp.TP_ID in (' . $tpid . ') order by tp.TP_ID ASC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_practicetestpaper_by_id($tpid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from practice_test_paper tp LEFT JOIN user_role ur on ur.USER_ROLE_ID=tp.TP_CREATED_BY where tp.TP_ID in (' . $tpid . ') order by tp.TP_ID ASC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_question_by_subid_bookid_level($subid, $bookid, $level, $questype = "") {
		if ($questype != '') {
			$questype = ' and q.QUES_TYPE=2 ';
		}
		$this->db->trans_start();
		$query = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.SUB_ID="' . $subid . '" and q.BOOK_ID in (' . $bookid . ') and q.QUES_DIFFICULTY_LEVEL in (' . $level . ') ' . $questype . '  and QUES_STATUS=1 order by q.QUES_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}

	function get_all_ques_from_allsub($bookid, $leveltype, $tpid = "") {
		$subids = $this->Admingetmodel->get_all_subids();
		$bookids = $this->Admingetmodel->get_bookids_by_subid($subids);
		if (!empty($bookids)) {

			$newbookids = implode(',', array_diff(explode(',', $bookids), explode(',', $bookid)));

			if ($newbookids != '') {
				$chapterids = $this->Admingetmodel->get_chapterids_by_booksid($newbookids);
			} else {
				$chapterids = '';
			}

			$questype = 2; /*For objective Type*/
			if ($chapterids != '') {
				$quesdt = $this->Admingetmodel->get_all_ques_by_chapterid_difflevel($chapterids, $leveltype, $questype);
			} else {
				$quesdt = '';
			}

			$addedquesdt = $this->Admingetmodel->get_testseriesdt_by_id($_GET['tsid']);
			if ($tpid != '') {
				$addedquesintpdt = $this->Admingetmodel->get_testpaperdt_by_id($_GET['tpid']);
				$addedquesintparr = json_decode($addedquesintpdt[0]->TP_QUESTIONS);

				$addedquesarr = json_decode($addedquesdt[0]->TS_INSERTED_QUES);
			} else {
				$addedquesarr = json_decode($addedquesdt[0]->TS_INSERTED_QUES);
			}

			$max = array();
			if (!empty($quesdt)) {

				foreach ($quesdt as $quesdts) {
					if ($addedquesdt[0]->TS_INSERTED_QUES == '') {
						$min = array();
						$min['QUES_ID'] = $quesdts->QUES_ID;
						$min['QUES'] = $quesdts->QUES;
						$min['SUB_NAME'] = $quesdts->SUB_NAME;
						$min['BOOK_NAME'] = $quesdts->BOOK_NAME;
						$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
						array_push($max, $min);
					} else {
						if (!in_array($quesdts->QUES_ID, $addedquesarr)) {
							$min = array();
							$min['QUES_ID'] = $quesdts->QUES_ID;
							$min['QUES'] = $quesdts->QUES;
							$min['SUB_NAME'] = $quesdts->SUB_NAME;
							$min['BOOK_NAME'] = $quesdts->BOOK_NAME;
							$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
							array_push($max, $min);
						}
					}
				}
			}

			return $max;
		}
	}
	function get_distinct_subid_by_quesid($quesid) {
		$this->db->trans_start();
		$query = $this->db->query('select GROUP_CONCAT(DISTINCT SUB_ID) as SUB_ID from question where QUES_ID in (' . $quesid . ') ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result[0]->SUB_ID;
	}
	function check_test_paper_name_exist($tpname, $tsid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from test_paper where TP_NAME="' . $tpname . '" and TS_ID="' . $tsid . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}

	function check_practice_test_paper_name_exist($tpname, $tsid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from practice_test_paper where TP_NAME="' . $tpname . '" and TS_ID="' . $tsid . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_testpaperdt_by_id($tpid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from test_paper where TP_ID="' . $tpid . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_practicetestpaperdt_by_id($tpid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from practice_test_paper where TP_ID="' . $tpid . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_member_type() {
		$this->db->trans_start();
		$query = $this->db->query('select * from user_role order by USER_ROLE_ID ASC ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}

	public function get_member_by_email($email) {
		$this->db->trans_start();
		$query = $this->db->query('select * from users where USER_EMAIL="' . $email . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_user_by_usertype($type, $status, $payuserids, $student_type) {
		if ($status == 2) {
			$str = '';
		} else {
			$str = 'and USER_STATUS=' . $status . ' ';
		}
		if ($student_type == 2) {
			$addquery = '';
		} else if ($student_type == 0) {
			$addquery = 'and USER_ID NOT IN (' . $payuserids . ') ';
		} else {
			if ($payuserids != '') {
				$addquery = 'and USER_ID IN (' . $payuserids . ') ';
			} else {
				return array();
			}

		}
		$this->db->trans_start();
		$query = $this->db->query('select * from users where USER_ROLES="' . $type . '" ' . $str . ' ' . $addquery . ' ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_members() {
		$this->db->trans_start();
		$query = $this->db->query('select * from users u LEFT JOIN user_role ur on ur.USER_ROLE_ID=u.USER_ROLES where u.USER_ROLES!="3" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_ques_log_by_quesid($quesid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from question_log ql LEFT JOIN user_role ur on ur.USER_ROLE_ID=ql.USER_ROLES LEFT JOIN users u on u.USER_ID=ql.USER_ID where ql.QUES_ID="' . $quesid . '" order by ql.QUES_LOG_ID DESC limit 1 ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_remain_question_by_subid_bookid_level($subid, $bookid, $level, $questype = "", $tsid) {
		if ($questype != '') {
			$questype = ' and q.QUES_TYPE=2 ';
		}
		$str = "";
		$tsdt = $this->Admingetmodel->get_testseriesdt_by_id($tsid);
		$tsidarr = json_decode($tsdt[0]->TS_INSERTED_QUES);
		if (!empty($tsidarr)) {
			$tsids = implode(',', $tsidarr);
			$str = ' and q.QUES_ID NOT IN (' . $tsids . ') ';
		}
		$this->db->trans_start();
		$query = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.SUB_ID="' . $subid . '" and q.BOOK_ID in (' . $bookid . ') and q.QUES_DIFFICULTY_LEVEL in (' . $level . ') ' . $questype . ' ' . $str . ' and QUES_STATUS=1 order by q.QUES_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}

	public function get_remain_practice_question_by_subid_bookid_level($subid, $bookid, $level, $questype = "", $tsid) {
		if ($questype != '') {
			$questype = ' and q.QUES_TYPE=2 ';
		}
		$str = "";
		$tsdt = $this->Admingetmodel->get_practicetestdt_by_id($tsid);
		$tsidarr = json_decode($tsdt[0]->TS_INSERTED_QUES);
		if (!empty($tsidarr)) {
			$tsids = implode(',', $tsidarr);
			$str = ' and q.QUES_ID NOT IN (' . $tsids . ') ';
		}
		$this->db->trans_start();
		$query = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.SUB_ID="' . $subid . '" and q.BOOK_ID in (' . $bookid . ') and q.QUES_DIFFICULTY_LEVEL in (' . $level . ') ' . $questype . ' ' . $str . ' and QUES_STATUS=1 order by q.QUES_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	
	public function get_buyed_testseries_no($userid) {
		$this->db->trans_start();
		$query = $this->db->query('select count(*) as count from payment where USER_ID="' . $userid . '" and PAY_STATUS="success" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result[0]->count;
	}

	public function pay_user_ids($tsid) {

		$this->db->trans_start();
		$query = $this->db->query('select GROUP_CONCAT(DISTINCT USER_ID) as USER_ID from payment where PAY_STATUS="success" and TS_ID IN (' . $tsid . ')  ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result[0]->USER_ID;
	}
	public function get_attempt_testpaper_dt($userid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from student_result where USER_ID="' . $userid . '" order by STD_RESULT_ID DESC ');
		$result = $query->result();
		$this->db->trans_commit();
		if (!empty($result)) {
			return $result;
		} else {
			return '';
		}
	}
	public function get_attempt_testpaper_resultdt_andname($result) {
		$max['data'] = array();
		foreach ($result as $results) {
			$min = array();
			$tpdt = $this->Admingetmodel->get_testpaperdt_by_id($results->TP_ID);
			$min['TP_NAME'] = $tpdt[0]->TP_NAME;
			$min['TP_ID'] = url_encode($tpdt[0]->TP_ID);
			$dt = $this->Admingetmodel->get_testpaper_result($tpdt[0]->TS_ID, $tpdt[0]->TP_ID, $results->USER_ID);
			$min['RANK'] = $dt;
			$min['RESULT'] = $dt;
			array_push($max['data'], $min);
		}
		return $max;
	}
	public function get_testpaper_result($tsid, $tpid, $userid) {

		$this->db->trans_start();
		$query = $this->db->query('select * from student_result where USER_ID="' . $userid . '" and TP_ID="' . $tpid . '"  ');
		$result = $query->result();
		$this->db->trans_commit();
		if (!empty($result)) {

			$tsdt = $this->Admingetmodel->get_testseriesdt_by_id($tsid);
			$positive_mark = $tsdt[0]->TS_P_MARKS;
			$negative_mark = $tsdt[0]->TS_N_MARKS;
			$total_ques = $tsdt[0]->TS_QUESTION_NOS;
			$max_mark = ceil($total_ques * $positive_mark);
			$tsduration = $tsdt[0]->TS_DURATION;
			$std_result = json_decode($result[0]->STD_RESULT);
			$attempt = 0;
			$attempt_boomark = 0;
			$not_seen = 0;
			$skipped = 0;
			$bookmark = 0;
			$bookmark_both = 0;
			$not_visited = 0;
			$obtained_mark = 0;
			$number_of_true_ques = 0;
			$number_of_wrong_ques = 0;
			$rank = $result[0]->STD_RESULT_RANK;
			$time = explode(':', $result[0]->STD_RESULT_DURATION);
			$minute = $time[0];
			$second = $time[1];
			$duration = $tsduration - $minute;
			if ($second > 0) {
				$duration = $duration - 1;
				$second = 60 - $second;
			}
			if ($duration < 10) {
				$duration = "0" . $duration;
			}
			if ($second < 10) {
				$second = "0" . $second;
			}
			$takentime = $duration . ":" . $second;
			foreach ($std_result as $std_results) {

				if ($std_results->quesclass == "attempted") {
					$attempt++;
					$markdt = $this->Admingetmodel->get_marked(url_decode($std_results->quesid), $std_results->ques_ans);
					if ($markdt) {
						$number_of_true_ques++;
						$obtained_mark = $obtained_mark + $positive_mark;
					} else {
						$obtained_mark = $obtained_mark - $negative_mark;
						$number_of_wrong_ques++;
					}
				} else if ($std_results->quesclass == "skipped") {
					$skipped++;
				} else if (empty($std_results->quesclass)) {
					$not_seen++;
				} else if ($std_results->quesclass == "attempted bookmarked") {
					$attempt_boomark++;
					$attempt++;
					$bookmark_both++;
					$markdt = $this->Admingetmodel->get_marked(url_decode($std_results->quesid), $std_results->ques_ans);
					if ($markdt) {
						$number_of_true_ques++;
						$obtained_mark = $obtained_mark + $positive_mark;
					} else {
						$obtained_mark = $obtained_mark - $negative_mark;
						$number_of_wrong_ques++;
					}
				} else if ($std_results->quesclass == "bookmarked") {
					$bookmark++;
					$bookmark_both++;
				}
			}
			$data = array();
			$data['RANK'] = $rank;
			$data['duration'] = $takentime;
			$data['attempted'] = $attempt;
			$data['skipped'] = $skipped;
			$data['not_visited'] = $not_seen;
			$data['bookmark_both'] = $bookmark_both++;
			$data['attempt_boomark'] = $attempt_boomark;
			$data['boomark'] = $bookmark;
			$data['obtained_mark'] = number_format((float) $obtained_mark, 2, '.', '');
			if ($data['obtained_mark'] > 0) {
				$data['percentage'] = $data['obtained_mark'] * 100 / $max_mark;
			} else {
				$data['percentage'] = 0;
			}
			$data['total_ques'] = $total_ques;
			$data['max_mark'] = $max_mark;
			$data['number_of_true_ques'] = $number_of_true_ques;
			$data['number_of_wrong_ques'] = $number_of_wrong_ques;
			return $data;
		} else {
			return '';
		}
	}
	public function get_marked($quesid, $ques_ans) {
		$quesdt = $this->Admingetmodel->get_quesdt_by_id($quesid);
		if ($quesdt[0]->QUES_ANSWERS == $ques_ans) {
			return true;
		} else {
			return false;
		}
	}
	public function get_coupon_title_exist($coupontitle, $couponid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from coupon where COUPON_TITLE="' . $coupontitle . '" and COUPON_ID!="' . $couponid . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_coupon() {
		$this->db->trans_start();
		$query = $this->db->query('select c.*,pt.PROD_TYPE_NAME from coupon c LEFT JOIN prod_type pt on pt.PROD_TYPE_ID=c.PROD_TYPE_ID order by c.COUPON_ID DESC ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_coupon_dt_by_couponid($couponid) {
		$this->db->trans_start();
		$query = $this->db->query('select c.*,pt.PROD_TYPE_NAME from coupon c LEFT JOIN prod_type pt on pt.PROD_TYPE_ID=c.PROD_TYPE_ID where c.COUPON_ID="' . $couponid . '" order by c.COUPON_ID DESC ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_coupon_by_title($title) {
		$this->db->trans_start();
		$query = $this->db->query('select * from coupon where COUPON_TITLE="' . $title . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}

	public function get_user_by_userroleid($userrole) {
		$this->db->trans_start();
		$query = $this->db->query('select * from users where USER_ROLES="' . $userrole . '"  ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_template($sending_type) {
		$this->db->trans_start();
		$query = $this->db->query('select * from msgtemplate where MSGTEMP_SEND_TYPE="' . $sending_type . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_templatedt_by_tempid($tempid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from msgtemplate where MSGTEMP_ID="' . $tempid . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_all_scheduling($sending_type) {
		$this->db->trans_start();
		$query = $this->db->query('select * from msgschedule ms LEFT JOIN msgtemplate mt on mt.MSGTEMP_ID=ms.MSGTEMP_ID where mt.MSGTEMP_SEND_TYPE="' . $sending_type . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_schedulingdt_by_schedid($schid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from msgschedule ms LEFT JOIN msgtemplate mt on mt.MSGTEMP_ID=ms.MSGTEMP_ID where ms.SCH_ID="' . $schid . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	//A Start 11-04-2020
	public function get_course_by_name($coursename) {
		$this->db->trans_start();
		$query = $this->db->query('select * from courses where COURSE_NAME="' . $coursename . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}
	public function get_all_courses() {
		$this->db->trans_start();
		$query = $this->db->query('select * from courses order by COURSE_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
		return $result;
	}
	public function get_all_prod_type() {
		$this->db->trans_start();
		$query = $this->db->query('select * from prod_type ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_library_by_libid($libid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from library where LIB_ID in (' . $libid . ') ');
		$result = $query->result();
		$this->db->trans_commit();
		return $result;
	}

	public function get_course_by_id($courseid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from courses where COURSE_ID="' . $courseid . '" ');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}

	public function get_subject_by_subid($subid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from subjects where SUB_ID = "' . $subid . '" order by SUB_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}

	public function get_all_sliders($postData) {
		$draw = $postData['draw'];
		$start = $postData['start'];
		$rowperpage = $postData['length']; // Rows display per page
		$columnIndex = $postData['order'][0]['column']; // Column index
		$columnName = $postData['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $postData['order'][0]['dir']; // asc or desc
		$searchValue = $postData['search']['value']; // Search value

		## Total number of records without filtering
		$totalRecords = $this->db->count_all('slider');

		## Custom ordering
		$column_order = array('SLIDER_ID', 'SLIDER_NAME', 'PROD_TYPE_NAME', null, null, null);
		$columnName = $column_order[$columnIndex];

		## Fetch Records
		// $this->db->order_by($columnName, $columnSortOrder);
		$this->db->select('*');
		if ($searchValue != '') {
			$this->db->or_like('SLIDER_NAME', trim($searchValue), 'after');
			$this->db->or_like('PROD_TYPE_NAME', trim($searchValue), 'both');
		}
		$this->db->from('slider');
		$this->db->limit($rowperpage, $start);
		$this->db->join('prod_type p', 'p.PROD_TYPE_ID=slider.PROD_TYPE');
		$this->db->order_by($columnName, $columnSortOrder);
		$result = $this->db->get()->result();
		// For each row in databasse

		return array($result, $totalRecords, $totalRecords);

	}

	function get_all_subject_from_courseid($courseid) {
		$this->db->trans_start();
		$query = $this->db->query('select * from subjects where COURSE_ID = "' . $courseid . '" order by SUB_ID DESC');
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}

	/*Shubham 2020-06-13*/
	function get_remains_questions($bookid) {
		$this->db->trans_start();
		$query = $this->db->query("SELECT * FROM question WHERE QUES_STATUS=1 AND BOOK_ID='$bookid' AND QUES_USED_IN_TP=0");
		$result = $query->result();
		$this->db->trans_commit();
		return array('status' => 200, 'message' => 'ok', 'data' => $result);
	}
	/**/
}
?>