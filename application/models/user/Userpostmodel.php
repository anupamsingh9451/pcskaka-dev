<?php
class Userpostmodel extends CI_Model {

    public function login($username,$password,$type,$loginagain="")
    {
        $query  = $this->db->query('select * from users where ( USER_EMAIL="'.$username.'" OR USER_CONTACT="'.$username.'" ) and USER_PASSWORD="'.$password.'" and USER_ROLES="'.$type.'" and USER_STATUS=1');
		$result= $query->result();
        if(empty($result)){
            return array('status' => 200,'message' => 'Invalid Login Credential');
        } else {
			$last_login = date('Y-m-d H:i:s');
			$token = crypt(substr( md5(rand()), 0, 7),rand());
			$expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
			$this->db->trans_start();
			$q  = $this->db->select('*')->from('users_authentication')->where('HRM_ID',$result[0]->USER_ID)->where('HRM_TYPE',$type)->get()->row();
			if($q == ""){
			    $this->db->insert('users_authentication',array('HRM_ID' => $result[0]->USER_ID,'TOKEN' => $token,'EXPIRED_AT' => $expired_at,'HRM_TYPE' => $type));
            }else{
                if(empty($loginagain)){
                    if($q->EXPIRED_AT < date('Y-m-d H:i:s')){
                        $this->db->insert('users_authentication',array('HRM_ID' => $result[0]->USER_ID,'TOKEN' => $token,'EXPIRED_AT' => $expired_at,'HRM_TYPE' => $type));
                        $this->db->where('HRM_ID',$result[0]->USER_ID)->where('TOKEN',$q->TOKEN)->where('HRM_TYPE',$type)->delete('users_authentication');
                    }else{
                        // $token=$q->TOKEN;
                        return array('status' => 200,'message' => 'Already Login');
                    }
                }else{
                    $this->db->insert('users_authentication',array('HRM_ID' => $result[0]->USER_ID,'TOKEN' => $token,'EXPIRED_AT' => $expired_at,'HRM_TYPE' => $type));
                    $this->db->where('HRM_ID',$result[0]->USER_ID)->where('TOKEN',$q->TOKEN)->where('HRM_TYPE',$type)->delete('users_authentication');
                }
            }
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' => 'Internal server error.');
			} else {
				$this->db->where('USER_ID',$result[0]->USER_ID)->update('users',array('USER_LAST_LOGIN' => $last_login));
				$this->session->set_userdata('userloginid',$result[0]->USER_ID);
				$this->session->set_userdata('usertype','2'); /* for user login */
				$this->session->set_userdata('usertoken',$token); 
				$this->db->trans_commit();
				return array('status' => 200,'message' => 'ok','id' => $result[0]->USER_ID, 'token' => $token);
			}
        }
    }
    public function logout()
    {
        $users_id= $this->input->get_request_header('User-ID', TRUE);
        $token= $this->input->get_request_header('Authorization', TRUE);
        $type= $this->input->get_request_header('type', TRUE);
        $this->db->where('HRM_ID',$users_id)->where('TOKEN',$token)->where('HRM_TYPE',$type)->delete('users_authentication');
        $this->session->unset_userdata('userloginid');
        $this->session->unset_userdata('usertype');
        $this->session->unset_userdata('usertoken');
        return array('status' => 200,'message' => 'Successfully logout.');
    }
    public function addquestion($data)
    {
        $this->db->insert('question',$data);
		return $this->db->insert_id();
    }
    public function updatequestion($data,$id)
    {
        $this->db->where('QUES_ID',$id);
		$this->db->update('question',$data);
		return true;
    }
    public function update_ques_log($quesid,$queslogaction,$userid){
        $userdt = $this->Usergetmodel->get_user_dt_model($userid);
        $data = array(
            'QUES_ID' => $quesid,
            'QUES_LOG_ACTION' => $queslogaction,
            'USER_ID' => $userid,
            'USER_ROLES' => $userdt['data'][0]->USER_ROLES,
            'QUES_LOG_TT' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('question_log',$data);
		return $this->db->insert_id();
    }
}
?>