<?php
class Usergetmodel extends CI_Model {

    public function get_user_dt_model($userid)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from users where USER_ID="'.$userid.'"');
		$result= $query->result();
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
	}
	public function get_all_subject_model()
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from subjects ');
		$result= $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_books_by_subid($subid)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from books where SUB_ID="'.$subid.'" order by SUB_ID ASC ');
		$result= $query->result();
		$this->db->trans_commit();
		return $result;
	}
	public function get_bookdt_by_bookid($bookid)
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from books where BOOK_ID="'.$bookid.'" ');
		$result= $query->result();
		$this->db->trans_commit();
		return $result;
	}
    public function get_all_chapter_by_bookid($bookid)
    {
    		$this->db->trans_start();
            $query  = $this->db->query('select * from chapter c LEFT JOIN books b on b.BOOK_ID=c.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=c.SUB_ID LEFT JOIN library l on l.LIB_ID=c.LIB_ID where c.BOOK_ID="'.$bookid.'" order by c.CHAPTER_ID DESC');
    		$result= $query->result(); 
    		$this->db->trans_commit();
    		return $result;
    }
    public function get_all_ques_by_chapterid($chapterid,$userid="")
    {
		$this->db->trans_start();
        $query  = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID where q.CHAPTER_ID="'.$chapterid.'" and q.QUES_CREATED_BY="'.$userid.'" and q.QUES_STATUS!=2 order by q.QUES_ID DESC');
		$result= $query->result(); 
		$this->db->trans_commit();
		return $result;
	}
	public function get_quesdt_by_id($ques_id){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN user_role ur on ur.USER_ROLE_ID=q.QUES_CREATED_BY where q.QUES_ID="'.$ques_id.'" ');
		$result= $query->result(); 
		$this->db->trans_commit();
		return $result;
	}
	public function get_chaptersdt_by_id($chapterid){
	    $this->db->trans_start();
        $query  = $this->db->query('select * from chapter c LEFT JOIN books b on b.BOOK_ID=c.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=c.SUB_ID LEFT JOIN library l on l.LIB_ID=c.LIB_ID where c.CHAPTER_ID="'.$chapterid.'"  ');
		$result= $query->result();
		$this->db->trans_commit();
		return array('status' => 200,'message' => 'ok','data' => $result);
	}
	public function get_difficulty_level($level){
	    if($level==1){
	        $result = "Low";
	    }else if($level==2){
	        $result = "Medium";
	    }else{
	        $result = "Heigh";
	    }
		return $result;
	}
}
?>