<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	function send_mail($from,$to,$subject,$message){
	    $ci= &get_instance();
	    $result = $ci->email
	        ->from($from,"PCSKAKA")
            ->to($to)
            ->subject($subject)
            ->message($message)
            ->send();
            
        if($result){
            return 1;
        }else{
            return  0;
        }    
    }
    function send_enquiry($name,$email,$subject,$message,$contact){
        $from="support@pcskaka.com";
        $mymessage="<h4>AN ENQUIRY HAS BEEN RECEIVED</h4><br>";
        $mymessage.="<table width='100%' border='2' cellspacing='2' cellpadding='2'><tr><th>NAME</th><th>".$name."</th></tr><tr><th>EMAIL</th><th>".$email."</th></tr><tr><th>SUBJECT</th><th>".$subject."</th></tr><tr><th>CONTACT NO</th><th>".$contact."</th></tr><tr><th>MESSAGE</th><th>".$message."</th></tr></table>";
        $subjects='ENQUIRY';
        send_mail($from,$from,$subjects,$mymessage);
        $mymessage="<h4>Your enquiry has been submitted</h4><br>";
        $mymessage.="<table width='100%' border='2' cellspacing='2' cellpadding='2'><tr><th>NAME</th><th>".$name."</th></tr><tr><th>EMAIL</th><th>".$email."</th></tr><tr><th>SUBJECT</th><th>".$subject."</th></tr><tr><th>CONTACT NO</th><th>".$contact."</th></tr><tr><th>MESSAGE</th><th>".$message."</th></tr></table>";
        send_mail($from,$email,$subjects,$mymessage);
        $data['mesg']='Enquiry sended successfully!';
    }
	function convertToObject($array) {
        $object = new stdClass();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = convertToObject($value);
            }
            $object->$key = $value;
        }
        return $object;
    }
    
    function insert_activity_history($activity_type)
	{
		date_default_timezone_set('Asia/Calcutta'); 
		$ci = &get_instance();
		$ci->load->database();
		$date = date('Y-m-d H:i:s');
		$data = array(
			'ACTIVITY_TYPE_ID' => $ci->session->userdata('adminloginid'),
			'HRM_TYPE_ID' => $ci->session->userdata('type'),
			'RELATED_ID' => $activity_type,
			'ACTIVITY_IP' => $_SERVER['REMOTE_ADDR'],
			'ACTIVITY_TIME' => $date
		);
		$ci->db->insert('users_activity', $data);
		return $ci->db->insert_id();
	}
	
	
	/*================================================Server side DATATABLE for QUESTION LIST START=====================================================*/
	function count_all_ques($chapterid,$userid='',$level="",$status="",$approveby=""){
	    $ci = &get_instance();
        $ci->load->database();
        if($userid!=''){
            $query = $ci->db->query('Select count(*) as count from question where CHAPTER_ID in ('.$chapterid.') and QUES_CREATED_BY in ('.$userid.') and QUES_STATUS in ('.$status.') and QUES_DIFFICULTY_LEVEL in ('.$level.') and QUES_APPROVED_BY in ('.$approveby.') order by QUES_ID DESC');
		    
            
        }else{
            $query = $ci->db->query('Select count(*) as count from question where CHAPTER_ID="'.$chapterid.'" and QUES_STATUS!=2 order by QUES_ID DESC');
		}
        $res = $query->result();
        if (!empty($res)) {
        	return $res[0]->count;
        } else {
        	return 0;
        }
	}
	function get_all_ques($limit, $start, $order, $dir, $chapterid,$userid='',$level="",$status="", $approveby=""){
	    $ci = &get_instance();
    	$ci->load->database();
    	if($userid!=''){
    	    $query = $ci->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID in ('.$chapterid.') and q.QUES_CREATED_BY in ('.$userid.') and q.QUES_STATUS in ('.$status.') and q.QUES_DIFFICULTY_LEVEL in ('.$level.') and QUES_APPROVED_BY in ('.$approveby.') order by q.QUES_ID ' . $dir . ' limit ' . $limit . ' OFFSET ' . $start . '');
		}else{
    	    $query = $ci->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID="'.$chapterid.'" and q.QUES_STATUS!=2 order by q.QUES_ID ' . $dir . ' limit ' . $limit . ' OFFSET ' . $start . '');
		}
    	if ($query->num_rows() > 0) {
    		return $query->result();
    	} else {
    		return '';
    	}
	}
	function get_all_ques_search($limit, $start, $search, $order, $dir, $chapterid,$userid='',$level="",$status="", $approveby=""){
	    $ci = &get_instance();
    	$ci->load->database();
    	if($userid!=''){
    	    $query = $ci->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID in ('.$chapterid.') and q.QUES_CREATED_BY in ('.$userid.') and q.QUES_STATUS in ('.$status.') and q.QUES_DIFFICULTY_LEVEL in ('.$level.') and QUES_APPROVED_BY in ('.$approveby.') and ( q.QUES LIKE "%' . $search . '%" OR q.QUES_CREATED_BY LIKE "' . $search . '%" )  ORDER BY q.QUES_ID ' . $dir . ' limit ' . $limit . ' OFFSET ' . $start . '');
		}else{
    	    $query = $ci->db->query('select * from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID="'.$chapterid.'" and q.QUES_STATUS!=2 and ( q.QUES LIKE "%' . $search . '%" OR q.QUES_CREATED_BY LIKE "' . $search . '%" ) order by q.QUES_ID ' . $dir . ' limit ' . $limit . ' OFFSET ' . $start . '');
		}
    	if ($query->num_rows() > 0) {
    		return $query->result();
    	} else {
    		return '';
    	}
	}
	function get_all_ques_search_count($search, $chapterid,$userid='',$level="",$status="", $approveby=""){
	    $ci = &get_instance();
    	$ci->load->database();
    	if($userid!=''){
    	    $query = $ci->db->query('Select count(*) as count from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID in ('.$chapterid.') and q.QUES_CREATED_BY in ('.$userid.') and q.QUES_STATUS in ('.$status.') and q.QUES_DIFFICULTY_LEVEL in ('.$level.') and QUES_APPROVED_BY in ('.$approveby.') and ( q.QUES LIKE "%' . $search . '%" OR q.QUES_CREATED_BY LIKE "%' . $search . '%" )  ORDER BY q.QUES_ID DESC ');
		}else{
    	    $query = $ci->db->query('Select count(*) as count from question q LEFT JOIN chapter c on c.CHAPTER_ID=q.CHAPTER_ID LEFT JOIN books b on b.BOOK_ID=q.BOOK_ID LEFT JOIN subjects s on s.SUB_ID=q.SUB_ID LEFT JOIN users ur on ur.USER_ID=q.QUES_CREATED_BY where q.CHAPTER_ID in ('.$chapterid.') and q.QUES_STATUS!=2 and ( q.QUES LIKE "%' . $search . '%" OR q.QUES_CREATED_BY LIKE "%' . $search . '%" )  ORDER BY q.QUES_ID DESC ');
		}
    	$res = $query->result();
    	if (!empty($res)) {
    		return $res[0]->count;
    	} else {
    		return 0;
    	}
	}
	/*================================================Server side DATATABLE for QUESTION LIST END=====================================================*/
	
	
	/*================================================Server side DATATABLE for BOOK LIST START=====================================================*/
	
	function count_all_book($subid){
	    $ci = &get_instance();
        $ci->load->database();
        $query = $ci->db->query('Select count(*) as count from books b LEFT JOIN subjects s on s.SUB_ID=b.SUB_ID LEFT JOIN library l on l.LIB_ID=b.LIB_ID where b.SUB_ID in ('.$subid.') order by b.BOOK_ID DESC');
        $res = $query->result();
        if (!empty($res)) {
        	return $res[0]->count;
        } else {
        	return 0;
        }
	}
	function get_all_book($limit, $start, $order, $dir, $subid){
	    $ci = &get_instance();
    	$ci->load->database();
    	$query = $ci->db->query('select * from books b LEFT JOIN courses  on b.COURSE_ID=courses.COURSE_ID LEFT JOIN subjects s on s.SUB_ID=b.SUB_ID LEFT JOIN library l on l.LIB_ID=b.LIB_ID where b.SUB_ID in ('.$subid.') order by b.BOOK_ID ' . $dir . ' limit ' . $limit . ' OFFSET ' . $start . '');
    	if ($query->num_rows() > 0) {
    		return $query->result();
    	} else {
    		return '';
    	}
	}
	function get_all_book_search($limit, $start, $search, $order, $dir, $subid){
	    $ci = &get_instance();
    	$ci->load->database();
    	$query = $ci->db->query('select * from books b LEFT JOIN courses  on b.COURSE_ID=courses.COURSE_ID LEFT JOIN subjects s on s.SUB_ID=b.SUB_ID LEFT JOIN library l on l.LIB_ID=b.LIB_ID where b.SUB_ID in ('.$subid.') and ( l.LIB_NAME LIKE "' . $search . '%" OR b.BOOK_NAME LIKE "' . $search . '%" OR s.SUB_NAME LIKE "' . $search . '%" )  ORDER BY b.BOOK_ID ' . $dir . ' limit ' . $limit . ' OFFSET ' . $start . '');
    	if ($query->num_rows() > 0) {
    		return $query->result();
    	} else {
    		return '';
    	}
	}
	function get_all_book_search_count($search, $subid){
	    $ci = &get_instance();
    	$ci->load->database();
    	$query = $ci->db->query('Select count(*) as count from books b LEFT JOIN subjects s on s.SUB_ID=b.SUB_ID LEFT JOIN library l on l.LIB_ID=b.LIB_ID where b.SUB_ID in ('.$subid.') and ( l.LIB_NAME LIKE "' . $search . '%" OR b.BOOK_NAME LIKE "' . $search . '%" OR s.SUB_NAME LIKE "' . $search . '%" )  ORDER BY b.BOOK_ID DESC ');
    	$res = $query->result();
    	if (!empty($res)) {
    		return $res[0]->count;
    	} else {
    		return 0;
    	}
	}
	function url_encode($id){
        $encodedid = (($id*5555)/5)*100;
        return base64_encode($encodedid);

	}
	function url_decode($id){
        $decodedid = base64_decode($id);
        if(is_numeric($decodedid)){
            $decodedid = (($decodedid/100)*5)/5555;
            return $decodedid;
        }else{
            return '';
        }
	}
	/*================================================Server side DATATABLE for QUESTION LIST END=====================================================*/
	
	function check_ques_exist_in_all_test_series($quesid){
	    $ci = &get_instance();
    	$ci->load->database();
    	$query = $ci->db->query("SELECT TS_INSERTED_QUES FROM `test_series` WHERE TS_INSERTED_QUES LIKE '%\"".$quesid."\"%' ");
    	if ($query->num_rows() > 0) {
    		return true;
    	} else {
    		return false;
    	}
	}
	
	function send_sms($mobileno,$msg){
	    $ci =& get_instance();
		$ci->load->database();
    
	    $username="rohit123456";
		$password="dcntv.16";
		$sender='BRIGHT';
	    $mobile=$mobileno;
	    $message=$msg;
	    $username=urlencode($username);
		$password=urlencode($password);
		$sender=urlencode($sender);
		$message=urlencode($message);
		if ($mobile > 1111111111 && $mobile < 9999999999) {
			$parameters="username=".$username."&password=".$password."&mobile=".$mobile."&sendername=".$sender."&message=".$message;

			$url="http://priority.muzztech.in/sms_api/sendsms.php";

			$ch = curl_init($url);

			if(isset($_POST))
			{
				curl_setopt($ch, CURLOPT_POST,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$parameters);
			}
			else
			{
				$get_url=$url."?".$parameters;

				curl_setopt($ch, CURLOPT_POST,0);
				curl_setopt($ch, CURLOPT_URL, $get_url);
			}

			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1); 
			curl_setopt($ch, CURLOPT_HEADER,0);  // DO NOT RETURN HTTP HEADERS 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);  // RETURN THE CONTENTS OF THE CALL
			
			$return_val = curl_exec($ch);
			if($return_val==""){
				$jbid="Failed";
				$stat=0;
			}
			else{
				$stat=1;
				$jbid=$return_val;
			}
			return $jbid;
		  //  insert_sms_track($touserid,true,$jbid,$msg);
        }else{
		  //  insert_sms_track($touserid,false,'Invalid mobile no',$msg);
        }
	}
	function get_user_dt_by_id($id){
	    $ci = &get_instance();
    	$ci->load->database();
        $query  = $ci->db->query('select * from users where USER_ID="'.$id.'"  ');
		$result= $query->result();
		
		return $result;
	}
	function scheduling_msg(){
	    $ci = &get_instance();
    	$ci->load->database();
    	$query = $ci->db->query("SELECT * FROM msgschedule ms LEFT JOIN msgtemplate mt on mt.MSGTEMP_ID=ms.MSGTEMP_ID WHERE SCH_STATUS=1 AND ms.SCH_SEND_TYPE=1");
    	$res = $query->result();
    	$jbarr = array();
    	if ($query->num_rows() > 0) {
    		foreach($res as $ress){
    		    $newTime = strtotime('-59 minutes');
                $startdate = date('Y-m-d H:i:s', $newTime);
    		    $currentdate = date('Y-m-d H:i:s');
    		    
    		    $saveddate = date('Y-m-d H:i:s', strtotime($ress->SCH_DATE));
    		    if($ress->SCH_TYPE==1){
    		        $nextdate = date('Y-m-d H:i:s',strtotime($saveddate . "+1 days"));
    		    }else if($ress->SCH_TYPE==2){
    		        $nextdate = date('Y-m-d H:i:s',strtotime($saveddate . "+7 days"));
    		    }else if($ress->SCH_TYPE==3){
    		        $nextdate = date('Y-m-d H:i:s',strtotime($saveddate . "+1 month"));
    		    }else{
    		        $nextdate = date('Y-m-d H:i:s',strtotime($saveddate . "+1 year"));
    		    }
    		    
    		    if($saveddate <= $currentdate && $saveddate > $startdate){
    		        $useridarr = explode(',',$ress->USER_ID);
        		    foreach($useridarr as $useridarrs){
        		        $userdt = get_user_dt_by_id($useridarrs)[0];
        		        $msg = $ress->MSGTEMP_MSG;
        		        $title= $ress->MSGTEMP_TITLE;
        		        if($ress->SCH_SEND_TYPE==1){
        		            $mobileno = $userdt->USER_CONTACT;
            		        $jbid = send_sms($mobileno,$msg);
            		        insert_msg_activity($jbid,$useridarrs,$ress->MSGTEMP_ID,1);
            		        $ci->db->query('UPDATE msgschedule SET SCH_DATE="'.$nextdate.'" where SCH_ID="'.$ress->SCH_ID.'" ');
        		        }
        		    }
    		    }
    		}
    	}
	}
	function scheduling_email(){
	    $ci = &get_instance();
    	$ci->load->database();
    	$query = $ci->db->query("SELECT * FROM msgschedule ms LEFT JOIN msgtemplate mt on mt.MSGTEMP_ID=ms.MSGTEMP_ID WHERE SCH_STATUS=1 AND ms.SCH_SEND_TYPE=2");
    	$res = $query->result();
    	$jbarr = array();
    	if ($query->num_rows() > 0) {
    		foreach($res as $ress){
    		    $newTime = strtotime('-59 minutes');
                $startdate = date('Y-m-d H:i:s', $newTime);
    		    $currentdate = date('Y-m-d H:i:s');
    		    
    		    $saveddate = date('Y-m-d H:i:s', strtotime($ress->SCH_DATE));
    		    if($ress->SCH_TYPE==1){
    		        $nextdate = date('Y-m-d H:i:s',strtotime($saveddate . "+1 days"));
    		    }else if($ress->SCH_TYPE==2){
    		        $nextdate = date('Y-m-d H:i:s',strtotime($saveddate . "+7 days"));
    		    }else if($ress->SCH_TYPE==3){
    		        $nextdate = date('Y-m-d H:i:s',strtotime($saveddate . "+1 month"));
    		    }else{
    		        $nextdate = date('Y-m-d H:i:s',strtotime($saveddate . "+1 year"));
    		    }
    		    
    		    if($saveddate <= $currentdate && $saveddate > $startdate){
    		        $useridarr = explode(',',$ress->USER_ID);
        		    foreach($useridarr as $useridarrs){
        		        $userdt = get_user_dt_by_id($useridarrs)[0];
        		        $msg = $ress->MSGTEMP_MSG;
        		        $title= $ress->MSGTEMP_TITLE;
        		        if($ress->SCH_SEND_TYPE==2){
        		            $from="support@pcskaka.com";
        		            $to = $userdt->USER_EMAIL;
    					    $status = send_mail($from,$to,$title,$msg);
    					    insert_msg_activity($status,$useridarrs,$ress->MSGTEMP_ID,2);
    					    $ci->db->query('UPDATE msgschedule SET SCH_DATE="'.$nextdate.'" where SCH_ID="'.$ress->SCH_ID.'" ');
        		        }
        		    }
    		    }
    		}
    	}
	}
	function insert_msg_activity($jbid,$userid,$tempid,$type)
	{
		date_default_timezone_set('Asia/Calcutta'); 
		$ci = &get_instance();
		$ci->load->database();
		$date = date('Y-m-d H:i:s');
		$data = array(
			'JB_ID' => $jbid,
			'USER_ID' => $userid,
			'MSGTEMP_ID' => $tempid,
			'MSG_LOG_TYPE'=>$type,
			'MSG_LOG_TT' => date('Y-m-d H:i:s')
		);
		$ci->db->insert('msg_log', $data);
		return $ci->db->insert_id();
	}
		
	//Get All Courses
	function get_all_courses(){
	    $ci = &get_instance();
    	$ci->load->database();
		$query = $ci->db->query('SELECT * FROM `courses` ')->result();
		if (!empty($query)) {
    		return $query;
    	} else {
    		return '';
    	}
	}
	//Get Library By courseid
	function get_all_lib_by_courseid($courseid=""){
		$str="";
		if($courseid!=""){
			$str.=' and COURSE_ID="'.$courseid.'"';	
		}
		$str.=' and LIB_STATUS="1"';	
	    $ci = &get_instance();
    	$ci->load->database();
		$query = $ci->db->query('SELECT * FROM `library` where 1 '.$str)->result();
		if (!empty($query)) {
    		return $query;
    	} else {
    		return '';
    	}
	}
	//Get Library By courseid
	function get_all_subject_by_libraryid($libid=""){
		$str="";
		if($libid!=""){
			$str.=' and LIB_ID="'.$libid.'"';	
		}	
	    $ci = &get_instance();
    	$ci->load->database();
		$query = $ci->db->query('SELECT * FROM `subjects` where 1 '.$str)->result();
		if (!empty($query)) {
    		return $query;
    	} else {
    		return '';
    	}
	}
	function convert_decimal_two($number)
	{
		return number_format((float)$number, 2, '.', '');
	}
	function get_producttype_dt_by_id($type_id){
		$str="";
		if($type_id!=""){
			$str.=' and PROD_TYPE_ID="'.$type_id.'"';	
		}	
	    $ci = &get_instance();
    	$ci->load->database();
		$query = $ci->db->query('SELECT * FROM `prod_type` where 1 '.$str)->result();
		if (!empty($query)) {
    		return $query[0];
    	} else {
    		return '';
    	}
	}
	function get_itemdt_byid($tablename,$column,$itemid){
		$ci = &get_instance();
    	$ci->load->database();
		$query = $ci->db->query('SELECT * FROM '.$tablename.' WHERE `'.$column.'` = '.$itemid)->result();
		if (!empty($query)) {
    		return $query[0];
    	} else {
    		return '';
    	}
	}
	function get_product_cost_prod_type_prod_id($prodtype,$prod_id){
		
        if($prodtype==0){
            return get_itemdt_byid('courses','COURSE_ID',$prod_id)->COURSE_AMT;
        }else if($prodtype==1){
			return get_itemdt_byid('library','LIB_ID',$prod_id)->LIB_COST;
		}else if($prodtype==2){
			return get_itemdt_byid('subjects','SUB_ID',$prod_id)->SUB_COST;
		}else if($prodtype==3){
			return get_itemdt_byid('books','BOOK_ID',$prod_id)->BOOK_COST;
		}else if($prodtype==4){
			return get_itemdt_byid('test_series','TS_ID',$prod_id)->TS_COST;
		}else if($prodtype==6){
			return get_itemdt_byid('practice_test','TS_ID',$prod_id)->TS_COST;
		}else{
			return "";
		} 
	}
	function get_product_dt_prod_type_prod_id($prodtype,$prod_id){
		//retuen only name,price,validity
		
        if($prodtype==0){
            $dt=get_itemdt_byid('courses','COURSE_ID',$prod_id);
            return array('PROD_NAME'=>$dt->COURSE_NAME,'PROD_COST'=>$dt->COURSE_AMT,'END_DATE'=>' --');
        }else if($prodtype==1){
			$dt=get_itemdt_byid('library','LIB_ID',$prod_id);
			return array('PROD_NAME'=>$dt->LIB_NAME,'PROD_COST'=>$dt->LIB_COST,'END_DATE'=>' --');
		}else if($prodtype==2){
			$dt=get_itemdt_byid('subjects','SUB_ID',$prod_id);
			return array('PROD_NAME'=>$dt->SUB_NAME,'PROD_COST'=>$dt->SUB_COST,'END_DATE'=>' --');
		}else if($prodtype==3){
			$dt=get_itemdt_byid('books','BOOK_ID',$prod_id);
			return array('PROD_NAME'=>$dt->BOOK_NAME,'PROD_COST'=>$dt->BOOK_COST,'END_DATE'=>' --');
		}else if($prodtype==4){
			$dt=get_itemdt_byid('test_series','TS_ID',$prod_id);
			return array('PROD_NAME'=>$dt->TS_NAME,'PROD_COST'=>$dt->TS_COST,'END_DATE'=>date('m-d-Y H:i',strtotime($dt->TS_END_DATE)));
		}else{
			return "";
		} 
	}
	function get_new_invoiceno(){
		$ci = &get_instance();
    	$ci->load->database();
        $query  = $ci->db->query('SELECT MAX(INVOICE_NO)+1 as INVOICE_NO FROM `invoice`');
		$result = $query->result(); 
		$ci->db->trans_commit();
		if(!empty($result[0]->INVOICE_NO)){
		    return $result[0]->INVOICE_NO;
		}else{
		   return 0;
		}
	}
	function getallcoupons($get,$coupontype=""){
		$ci = &get_instance();
		$ci->load->database();
		$allproducts=$get['allproducts'];
		$duplicatedata=$data=array();
		foreach ($allproducts as $res) {
			$str="";
			$currentdate = date('Y-m-d');
			$str.=" and COUPON_STATUS=1 ";
			$str.=" and COUPON_START_DATE <= '".$currentdate."' ";
			$str.=" and COUPON_END_DATE >= '".$currentdate."' ";
			$str.=" and COUPON_USAGE_LIMIT>COUPON_TOT_USES ";
			if($get['totalproducttype']> 1){
				if($coupontype!=""){
					$str.=" and COUPON_TYPE=1 ";
				}
				$str.=" and FIND_IN_SET('".$res['producttype']."',PROD_TYPE_ID) ";
				$str.=" and FIND_IN_SET('0',PRODUCT) ";
				$result  = $ci->db->query('SELECT * FROM `coupon` where 1 ' .$str)->result();
			}else{
				if($coupontype!=""){
					$str.=" and COUPON_TYPE=1 ";
				}
				$str.=" and FIND_IN_SET('".$res['producttype']."',PROD_TYPE_ID) ";
				$str.=" and FIND_IN_SET('".$res['productid']."',PRODUCT) ";
				$result  = $ci->db->query('SELECT * FROM `coupon` where 1 ' .$str)->result();	
			}
			if (!empty($result)) {
				foreach ($result as $key) {
					$min=array();
					$min['ID']=$key->COUPON_ID;
					$min['TITLE']=$key->COUPON_TITLE;
					$min['DESCRIPTION']=$key->COUPON_DESC;
					$min['MODE']=$key->COUPON_MODE;
					$min['VALUE']=$key->COUPON_AMOUNT;
					$prodtypedt=get_producttype_dt_by_id($key->PROD_TYPE_ID);
					$min['PROD_IDS']=$key->PRODUCT;
					$min['PROD_TYPE']=$prodtypedt->PROD_TYPE_NAME;
					$min['PROD_TYPE_ID']=$key->PROD_TYPE_ID;
					if (!in_array($key->COUPON_ID, $duplicatedata)) {
						$duplicatedata[]=$key->COUPON_ID;
						if($get['totalproducttype']> 1){
							if(count(explode (",", $key->PROD_TYPE_ID))>1){
								$data[]=$min;
							}
						}else{
							$data[]=$min;
						}
					}
				}
			}
		}
		$ci->db->trans_commit();
		return $data;
	}

	function coupon_uses_by_user($coupon_id){
		$ci = &get_instance();
		$ci->load->database();
        $currentdate = date('Y-m-d');
        $userid = $ci->session->userdata('studentloginid');
        $query  = $ci->db->query('select count(*) count from invoice inv LEFT JOIN payment pay on pay.INVOICE_NO=inv.INVOICE_NO where inv.USER_ID="'.$userid.'" and inv.COUPON_ID="'.$promo_id.'" and pay.PAY_STATUS="success" ');
		$result = $query->result(); 
		$ci->db->trans_commit();
		if(!empty($result)){
		    return $result[0]->count;
		}else{
		   return 0;
		}
	}
	function get_invoicedt($invoiceid){
		$ci = &get_instance();
		$ci->load->database();
        $result  = $ci->db->query('SELECT * FROM `invoice` WHERE `INVOICE_ID` = '.$invoiceid.' ORDER BY `INVOICE_ID`')->row();
		$ci->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		   return '';
		}
	}
	function update_coupontotal_uses($invoiceid){
		$ci = &get_instance();
		$ci->load->database();
        $ci->db->query('UPDATE `coupon` SET `COUPON_TOT_USES`=COUPON_TOT_USES+1 WHERE COUPON_ID IN ('.$invoiceid.') AND COUPON_TYPE=2');
		$ci->db->trans_commit();
	}
	function get_all_currentaffairs_activebooks(){
		$ci = &get_instance();
		$ci->load->database();
        $result=$ci->db->query('SELECT bk.*,(SELECT COUNT(*) FROM question WHERE bk.SUB_ID=7 AND BOOK_ID=bk.BOOK_ID) as TOTAL_QUES FROM `books` bk WHERE bk.SUB_ID=7 and bk.BOOK_STATUS=1 ORDER BY BOOK_ID DESC')->result();
		$ci->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		   return '';
		}
	}
	function get_bookdt_by_id($boookid){
		$ci = &get_instance();
		$ci->load->database();
        $result=$ci->db->query('SELECT bk.*,(SELECT COUNT(*) FROM question WHERE bk.SUB_ID=7 AND BOOK_ID=bk.BOOK_ID) as TOTAL_QUES FROM `books` bk WHERE BOOK_ID="'.$boookid.'" ')->row();
		$ci->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		   return '';
		}
	}

	function get_all_active_course_category(){
		$ci = &get_instance();
		$ci->load->database();
        $result=$ci->db->query('SELECT * FROM `course_category` WHERE CC_STATUS="1" ')->result();
		$ci->db->trans_commit();
		if(!empty($result)){
		    return $result;
		}else{
		   return '';
		}
	}

    /*Shubham 2020-06-13*/
    function create_test_set_practice_test($ts_id,$total_paper,$ques_per_paper,$start_date,$end_date,$duration){
        $ci = &get_instance();
        $ci->load->database();

        $tsdt = $ci->Admingetmodel->get_practicetestdt_by_id($ts_id);
        $response = $ci->Admingetmodel->get_remains_questions($tsdt[0]->BOOK_ID);
        $ques_set = $response["data"];
        $total_ques_used = $total_paper * $ques_per_paper;
        $ques_id_arr = array();
        $paper_no = 1;
        $status = false;
        foreach($ques_set as $ques_no=>$ques){
            if( $ques_no < $total_ques_used ){
                //collect paper question
                $ques_id_arr[] = $ques->QUES_ID;
                $status = true;
                if( count($ques_id_arr) == $ques_per_paper ){
                    //create paper
                    $tp_name = sprintf("Prcatice Test %s",($paper_no++));
                    $details = array(
                        'TP_NAME'=> $tp_name,
                        'TS_ID'=> $ts_id,
                        'TP_QUESTIONS'=> json_encode($ques_id_arr),
                        'TP_P_MARKS'=> $tsdt[0]->TS_P_MARKS,
                        'TP_N_MARKS'=> $tsdt[0]->TS_N_MARKS,
                        'TP_START_DATE'=> $start_date,
                        'TP_END_DATE'=> $end_date,
                        'TP_DURATION'=> $duration,
                        'TP_CREATED_AT'=> date("Y-m-d"),
                        'TP_DESCRIPTION'=> '',
                        'TP_PAY_STATUS'=> 1,
                        'TP_STATUS'=> 0,
                        'TP_CREATED_BY' => 1,
                        'TP_ENTRY_TT' => date('Y-m-d H:i:s')
                    );
                    $testpaperid=$ci->Adminpostmodel->addpracticetestpaper($details);

                    $tspaperarr1 = explode(',',$testpaperid);
                    $tsdt = $ci->Admingetmodel->get_practicetestdt_by_id($ts_id);
                    if($tsdt[0]->TS_TEST_PAPERS!=''){
                        $tspaperarr2 = json_decode($tsdt[0]->TS_TEST_PAPERS);
                        $mergetpidarr = array_merge($tspaperarr2,$tspaperarr1);
                        $encodedtpid= json_encode($mergetpidarr);
                        $data = array(
                            'TS_TEST_PAPERS'=> $encodedtpid,
                        );
                    }else{
                        $encodedtpid= json_encode($tspaperarr1);
                        $data = array(
                            'TS_TEST_PAPERS'=> $encodedtpid,
                        );
                    }
                    $updateid=$ci->Adminpostmodel->updatepracticetest($data,$ts_id);

                    if($tsdt[0]->TS_INSERTED_QUES!=''){
                        $tsquesarr = json_decode($tsdt[0]->TS_INSERTED_QUES);
                        $mergequesarr = array_merge($tsquesarr,$ques_id_arr);
                        $encodedquesid= json_encode($mergequesarr);
                        $data1 = array(
                            'TS_INSERTED_QUES'=> $encodedquesid,
                        );
                    }else{
                        $encodedquesid= json_encode($ques_id_arr);
                        $data1 = array(
                            'TS_INSERTED_QUES'=> $encodedquesid,
                        );
                    }
                    $ts_ids = $ci->Adminpostmodel->updatepracticetest($data1,$ts_id);

                    $distinctsubid = explode(',',$ci->Admingetmodel->get_distinct_subid_by_quesid(implode(",",$ques_id_arr)));
                    $data2 = array(
                        'SUB_ID'=> json_encode($distinctsubid)
                    );
                    $tpupdatedid = $ci->Adminpostmodel->updatepracticetestpaper($data2,$testpaperid);
                    
                    $ci->Adminpostmodel->update_practice_test_paper_status($testpaperid,$ts_id);

                    $ci->Adminpostmodel->update_ques_used_in_tp($ques_id_arr);

                    $ques_id_arr = array();
                }
                
            }else{
                break;
            }
        }
        return $status;
        
    }
    /**/
?>