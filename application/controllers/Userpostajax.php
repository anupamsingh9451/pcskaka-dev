<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userpostajax extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		header("Access-Control-Allow-Origin: *");
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database('default');
		$this->load->model('user/Authentication_model');
        $this->load->model('user/Userpostmodel');
		$this->load->model('user/Usergetmodel');
		$this->load->helper('json_output_helper');
		$this->load->helper('common_helper');
		date_default_timezone_set('Asia/Calcutta'); 
	    // Your own constructor code
	}
	public function userlogin()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                
				$username = $_POST['username'];
                $password = md5($_POST['password']);
                $type = 2; // 2 for User
                if(isset($_POST['loginagain'])){
                    $loginagain = $_POST['loginagain'];
                }else{
                    $loginagain = '';
                }
                $response = $this->Userpostmodel->login($username, $password, $type,$loginagain);
                
                json_output($response['status'], $response);
                if ($response['message'] == "ok"){
                    // insert_activity_history(1);
                }
            }
        }
    }
	public function userlogout()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $response = $this->Userpostmodel->logout();
                    
                    json_output(200, $response);
                    // insert_activity_history(2);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Userpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function create_quest()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    
					$bookid = $_POST['book_id'];
					$chapterdt = $this->Usergetmodel->get_all_chapter_by_bookid($bookid);
					$bookdt= $this->Usergetmodel->get_bookdt_by_bookid($bookid);
				    $subject = $bookdt[0]->SUB_ID;
				    $chapter = $chapterdt[0]->CHAPTER_ID;
				    $ques_levele = 1;
				    $question = $_POST['question'];
				    $ques_type = 2;
				    
					    $ques_option = $_POST['ques_option'];
					    $data1 = array(
    						'QUESTION' =>$question,
    						'OPTION' =>$ques_option
    					);
        				$ques = json_encode($data1);
						$details = array(
							'CHAPTER_ID' => $chapter,
							'BOOK_ID' => $bookid,
							'SUB_ID' => $subject,
							'QUES_TYPE' => $ques_type,
							'QUES' => $ques,
							'QUES_ANSWERS' => implode(',',$_POST['obj_answer']),
							'QUES_P_MARKING' => 0,
							'QUES_N_MARKING' => 0,
							'QUES_DURATION' => '00:00',
							'QUES_DIFFICULTY_LEVEL' => 1,
							'QUES_EXPLANATION' => $_POST['question_explain'],
							'QUES_STATUS'=> 0,
							'QUES_CREATED_BY' => $this->session->userdata('userloginid'),
							'QUES_APPROVED_BY' => 0,
							'QUES_CRATED_AT' => date('Y-m-d H:i:s'),
							'QUES_ENTRY_TT' => date('Y-m-d H:i:s'),
						);
					
					$ques_id=$this->Userpostmodel->addquestion($details);
					if(!empty($ques_id)){
					    
					    /* For Update Question Log */
    					$queslogaction = 1; // 1 For ques Add Log
    					$userid = $this->session->userdata('userloginid');
    					$this->Userpostmodel->update_ques_log($ques_id,$queslogaction,$userid);
					    
						$response['message']='Data Added Successfully';
						$response['data']='ok';
					}else{
						$response['message']='Something wents wrong';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Userpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
	
	public function update_quest()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                   
					$quesid = $_POST['quesid'];
					
				    $question = $_POST['question'];
				    $ques_type = 2;
				    
					    $ques_option = $_POST['ques_option'];
					    $data1 = array(
    						'QUESTION' =>$question,
    						'OPTION' =>$ques_option
    					);
        				$ques = json_encode($data1);
						$details = array(
							'QUES' => $ques,
							'QUES_ANSWERS' => implode(',',$_POST['obj_answer']),
							'QUES_EXPLANATION' => $_POST['obj_ques_explain'],
							'QUES_LAST_UPDATED' => date('Y-m-d H:i:s'),
						);
					
					$ques_id=$this->Userpostmodel->updatequestion($details,$quesid);
					if(!empty($ques_id)){
					    
					    /* For Update Question Log */
    					$queslogaction = 2; // 1 For ques Update Log
    					$userid = $this->session->userdata('userloginid');
    					$this->Userpostmodel->update_ques_log($quesid,$queslogaction,$userid);
					    
						$response['message']='Data Added Successfully';
						$response['data']='ok';
					}else{
						$response['message']='Something wents wrong';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Userpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function deletequestion()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$quesid=$_POST['quesid'];
					$details = array(
							'QUES_STATUS' => 2,
							'QUES_LAST_UPDATED' => date('Y-m-d H:i:s'),
						);
					$ques_id=$this->Userpostmodel->updatequestion($details,$quesid);
					
					/* For Update Question Log */
					$queslogaction = 4; // 4 For ques delete Log
					$userid = $this->session->userdata('userloginid');
					$this->Userpostmodel->update_ques_log($quesid,$queslogaction,$userid);
					
					$response['message']='Question Deleted successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Userpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
}

