<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Subadminpostajax extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database('default');
		$this->load->model('subadmin/Authentication_model');
        $this->load->model('subadmin/Subadminpostmodel');
		$this->load->model('subadmin/Subadmingetmodel');
		$this->load->helper('json_output_helper');
		$this->load->helper('common_helper');
		date_default_timezone_set('Asia/Calcutta'); 
	    // Your own constructor code
	}
	public function subadminlogin()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
				$username = $_POST['username'];
                $password = md5($_POST['password']);
                $type = 4; // 4 for Sub admin
                if(isset($_POST['loginagain'])){
                    $loginagain = $_POST['loginagain'];
                }else{
                    $loginagain = '';
                }
                $response = $this->Subadminpostmodel->login($username, $password, $type,$loginagain);
                json_output($response['status'], $response);
                if ($response['message'] == "ok"){
                    
                    // insert_activity_history(1);
                }
            }
        }
    }
	public function subadminlogout()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $response = $this->Subadminpostmodel->logout();
                    
                    json_output(200, $response);
                    // insert_activity_history(2);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function questioncreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$check=array();
					$chapter = $_POST['chapter'];
				    $subject = $_POST['subject'];
				    $book = $_POST['book'];
				    $ques_levele = $_POST['ques_levele'];
				    $question = $_POST['question'];
				    $ques_type = $_POST['ques_type'];
				    
					if($ques_type==1){
						$details = array(
							'CHAPTER_ID' => $chapter,
							'BOOK_ID' => $book,
							'SUB_ID' => $subject,
							'QUES_TYPE' => $ques_type,
							'QUES' => $question,
							'QUES_ANSWERS' => $_POST['subj_answer'],
							'QUES_P_MARKING' => $_POST['subj_positive_mark'],
							'QUES_N_MARKING' => $_POST['subj_negative_mark'],
							'QUES_DURATION' => $_POST['subj_duration'],
							'QUES_DIFFICULTY_LEVEL' => $ques_levele,
							'QUES_HINT' => $_POST['subj_ques_hint'],
							'QUES_EXPLANATION' => $_POST['subj_ques_explain'],
							'QUES_STATUS'=> 0,
							'QUES_CREATED_BY' => $this->session->userdata('subadminloginid'),
							'QUES_APPROVED_BY' => 0,
							'QUES_CRATED_AT' => $_POST['subj_created_date'],
							'QUES_ENTRY_TT' => date('Y-m-d H:i:s'),
						);
						
					}else{
					    $ques_option = $_POST['ques_option'];
					    $data1 = array(
    						'QUESTION' =>$question,
    						'OPTION' =>$ques_option
    					);
        				$ques = json_encode($data1);
						$details = array(
							'CHAPTER_ID' => $chapter,
							'BOOK_ID' => $book,
							'SUB_ID' => $subject,
							'QUES_TYPE' => $ques_type,
							'QUES' => $ques,
							'QUES_ANSWERS' => implode(',',$_POST['obj_answer']),
							'QUES_P_MARKING' => $_POST['obj_positive_mark'],
							'QUES_N_MARKING' => $_POST['obj_negative_mark'],
							'QUES_DURATION' => $_POST['obj_duration'],
							'QUES_DIFFICULTY_LEVEL' => $ques_levele,
							'QUES_HINT' => $_POST['obj_ques_hint'],
							'QUES_EXPLANATION' => $_POST['obj_ques_explain'],
							'QUES_STATUS'=> 0,
							'QUES_CREATED_BY' => $this->session->userdata('subadminloginid'),
							'QUES_APPROVED_BY' => 0,
							'QUES_CRATED_AT' => $_POST['obj_created_date'],
							'QUES_ENTRY_TT' => date('Y-m-d H:i:s'),
						);
					}
					$ques_id=$this->Subadminpostmodel->addquestion($details);
					if(!empty($ques_id)){
					    
					    /* For Update Question Log */
    					$queslogaction = 1; // 1 For ques Create Log
    					$userid = $this->session->userdata('subadminloginid');
    					$this->Subadminpostmodel->update_ques_log($ques_id,$queslogaction,$userid);
					    
						$response['message']='Data Added Successfully';
						$response['data']='ok';
					}else{
						$response['message']='Something wents wrong';
						$response['data']='Notok';
					}
                
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
	
	public function questionedit()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    
                    
					$check=array();
					
				    $ques_levele = $_POST['ques_levele'];
				    $question = $_POST['question'];
				    $ques_type = $_POST['ques_type'];
				    $ques_id = $_POST['quesid'];
				    $chapterdt = $this->Subadmingetmodel->get_all_chapter_by_bookid($_POST['book']);
					if($ques_type==1){
						$details = array(
							'SUB_ID' => $_POST['subject'],
				            'CHAPTER_ID' => $chapterdt[0]->CHAPTER_ID,
							'BOOK_ID' => $_POST['book'],
							'QUES_TYPE' => $ques_type,
							'QUES' => $question,
							'QUES_ANSWERS' => $_POST['subj_answer'],
							'QUES_P_MARKING' => $_POST['subj_positive_mark'],
							'QUES_N_MARKING' => $_POST['subj_negative_mark'],
							'QUES_DURATION' => $_POST['subj_duration'],
							'QUES_DIFFICULTY_LEVEL' => $ques_levele,
							'QUES_HINT' => $_POST['subj_ques_hint'],
							'QUES_EXPLANATION' => $_POST['subj_ques_explain'],
							'QUES_ENTRY_TT' => date('Y-m-d H:i:s'),
						);
						
					}else{
					    $ques_option = $_POST['ques_option'];
					    $data1 = array(
    						'QUESTION' =>$question,
    						'OPTION' =>$ques_option
    					);
        				$ques = json_encode($data1);
						$details = array(
					        'SUB_ID' => $_POST['subject'],
					        'CHAPTER_ID' => $chapterdt[0]->CHAPTER_ID,
							'BOOK_ID' => $_POST['book'],		
							'QUES_TYPE' => $ques_type,
							'QUES' => $ques,
							'QUES_ANSWERS' => implode(',',$_POST['obj_answer']),
							'QUES_P_MARKING' => $_POST['obj_positive_mark'],
							'QUES_N_MARKING' => $_POST['obj_negative_mark'],
							'QUES_DURATION' => $_POST['obj_duration'],
							'QUES_DIFFICULTY_LEVEL' => $ques_levele,
							'QUES_HINT' => $_POST['obj_ques_hint'],
							'QUES_EXPLANATION' => $_POST['obj_ques_explain'],
							'QUES_LAST_UPDATED' => date('Y-m-d H:i:s'),
						);
					}
					$quesid=$this->Subadminpostmodel->updatequestion($details,$ques_id);
					if($quesid){
					    if(isset($_POST['mytype'])){
					        if($_POST['mytype']==2){
                                $quesid = $_POST['quesid'];
            					$loginid = $_POST['loginid'];
            					$data = array(
            						'QUES_APPROVED_BY' =>$loginid,
            						'QUES_STATUS' =>1
            					);
            					$this->Subadminpostmodel->approveques($data,$quesid);
            					
            					/* For Update Question Log */
            					$queslogaction = 3; // 3 For ques Approve Log
            					$userid = $this->session->userdata('subadminloginid');
            					$this->Subadminpostmodel->update_ques_log($quesid,$queslogaction,$userid);
					        }
                        }
					    /* For Update Question Log */
    					$queslogaction = 2; // 2 For ques Edit Log
    					$userid = $this->session->userdata('subadminloginid');
    					$this->Subadminpostmodel->update_ques_log($ques_id,$queslogaction,$userid);
					    
						$response['message']='Data Added Successfully';
						$response['data']='ok';
					}else{
						$response['message']='Something wents wrong';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
	public function approveques()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$quesid = $_POST['quesid'];
					$loginid = $_POST['loginid'];
					$data = array(
						'QUES_APPROVED_BY' =>$loginid,
						'QUES_STATUS' =>1
					);
					$this->Subadminpostmodel->approveques($data,$quesid);
					
					/* For Update Question Log */
					$queslogaction = 3; // 3 For ques Approve Log
					$userid = $this->session->userdata('subadminloginid');
					$this->Subadminpostmodel->update_ques_log($quesid,$queslogaction,$userid);
					
					$response['message']='Data Edited Successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function deletequestion()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$quesid=$_POST['quesid'];
					$details = array(
							'QUES_STATUS' => 2,
							'QUES_LAST_UPDATED' => date('Y-m-d H:i:s'),
						);
					$ques_id=$this->Subadminpostmodel->updatequestion($details,$quesid);
					
					/* For Update Question Log */
					$queslogaction = 4; // 4 For ques delete Log
					$userid = $this->session->userdata('subadminloginid');
					$this->Subadminpostmodel->update_ques_log($quesid,$queslogaction,$userid);
					
					$response['message']='Question Deleted successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function bookcreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$check=array();
					
					$data=$this->Subadmingetmodel->check_book_exist(strtoupper($_POST['bookname']),$_POST['subject']);
				    $library = $_POST['library'];
				    $subject = $_POST['subject'];
					if(empty($data)){
						$details = array(
							'SUB_ID'=>$subject,
							'LIB_ID'=>$library,
							'BOOK_NAME' =>$_POST['bookname'],
							'BOOK_CHAPTER_NOS' =>$_POST['chapterno'],
							'BOOK_STATUS'=>1,
							'BOOK_CREATED_AT' =>$_POST['bookcreatedat'],
							'BOOK_ENTRY_TT' =>date('Y-m-d H:i:s'),
						);
						$bookid=$this->Subadminpostmodel->addbook($details);
						
						
						$details1 = array(
							'SUB_ID' => $subject,
							'LIB_ID' => $library,
							'BOOK_ID' => $bookid,
							'CHAPTER_NAME' => $_POST['bookname'],
							'CHAPTER_DESC' => '',
							'CHAPTER_QUE_NOS' => '',
							'CHAPTER_CREATED_AT' => $_POST['bookcreatedat'],
							'CHAPTER_STATUS' => 1,
							'CHAPTER_ENTRY_TT' => date('Y-m-d H:i:s'),
						);
						$chapterid=$this->Subadminpostmodel->addchapter($details1);
						
						if(!empty($bookid)){
							$response['message']='Data Added Successfully';
							$response['data']='ok';
						}else{
							$response['message']='Something wents wrong';
							$response['data']='Notok';
						}
					}else{
						$response['message']='Book is already present';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function editbookform()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$bookid=$_POST['bookid'];
					$subid=$_POST['subid'];
					$details=$this->Subadmingetmodel->check_book_exist(strtoupper($_POST['booknm']),$subid,$bookid);
					if(empty($details)){
    					$data = array(
    						'BOOK_NAME' =>strtoupper($_POST['booknm']),
    						'BOOK_CHAPTER_NOS' =>$_POST['chapterno'],
    						'BOOK_STATUS' => $_POST['bookstatus'],
    						'BOOK_LAST_UPDATED' =>date('Y-m-d H:i:s')
    					);
    					$this->Subadminpostmodel->updatebook($data,$bookid);
    					
    					$response['message']='Data Edited Successfully';
    					$response['data']='ok';
					}else{
						$response['message']='Book is already present';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function holdques()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$quesid = $_POST['quesid'];
					$loginid = $_POST['loginid'];
					$data = array(
						'QUES_STATUS' =>3
					);
					$this->Subadminpostmodel->updatequestion($data,$quesid);
					
					/* For Update Question Log */
					$queslogaction = 5; // 5 For ques Hold Log
					$userid = $this->session->userdata('subadminloginid');
					$this->Subadminpostmodel->update_ques_log($quesid,$queslogaction,$userid);
					
					$response['message']='Data Edited Successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
}