<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usergetajax extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		header("Access-Control-Allow-Origin: *");
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database('default');
		$this->load->model('user/Authentication_model');
        $this->load->model('user/Usergetmodel');
		$this->load->helper('json_output_helper');
		$this->load->helper('common_helper');
	}
	
    public function get_user_dt()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $params = $_REQUEST;
    				$userid = $_GET['id'];
                    $response = $this->Usergetmodel->get_user_dt_model($userid);
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Userpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
    
    public function listbookhtml()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $subid = $_GET['subid'];
    				$data['bookdt']=$this->Usergetmodel->get_books_by_subid($subid);
    				echo $response=json_encode($this->load->view('user/book/listbook',$data));
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Userpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
    public function createbookhtml()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $subid = $_GET['subid'];
    				$data['bookdt']=$this->Usergetmodel->get_books_by_subid($subid);
    				echo $response=json_encode($this->load->view('user/book/createbook',$data));
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Userpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
    public function view_ques_html()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $quesid = $_GET['quisid'];
    				$data['quesdt']=$this->Usergetmodel->get_quesdt_by_id($quesid);
    				echo $response=json_encode($this->load->view('user/question/view_ques_modal',$data));
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Userpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
    public function edit_ques_html()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $quesid = $_GET['quisid'];
    				$data['quesdt']=$this->Usergetmodel->get_quesdt_by_id($quesid);
    				echo $response=json_encode($this->load->view('user/question/edit_ques_modal',$data));
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Userpostmodel->logout();
                    
                    json_output(401, $response);
                }
                    
            }
        }
    }
    public function add_ques_html()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $bookid = $_GET['bookid'];
        			$data['bookid']=$bookid;
        			echo $response=json_encode($this->load->view('user/question/add_ques_modal',$data));
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Userpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function listquestion()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $bookdt = $this->Usergetmodel->get_all_chapter_by_bookid($_GET['bookid']);
                    $userid = $this->session->userdata('userloginid');
    				$response = $this->Usergetmodel->get_all_ques_by_chapterid($bookdt[0]->CHAPTER_ID,$userid);
    				$max['data']=array();
    				$i=1;
    				if(!empty($response)){
    					foreach($response as $res){
                            $str1="";
    						$min=array();
    						$min['SR_NO']=$i;
    						if($res->QUES_TYPE==1){
    						    $ques = $res->QUES;
    						    $min['QUES'] =  $ques.'...';
    						}else{
    						    $ques = json_decode($res->QUES)->QUESTION;
    						    $min['QUES']=  $ques;
    						}
    						
    						$min['QUES_CREATED_AT']=date('d-m-Y',strtotime($res->QUES_CRATED_AT));
    						if($res->QUES_STATUS==1){
    							$stat='<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
    							$str = '';
    							$str1 = '';
    						}else if($res->QUES_STATUS==3){
    						    $str1 = '<li><a class="deleteques" attr-quesid="'.$res->QUES_ID.'" href="javascript:void(0)">Delete</a></li>';
    						    $str = '<li><a class="edit_ques_modal" href="javascript:void(0);" attr-quesid="'.$res->QUES_ID.'">Edit</a></li>';
    							$stat='<button type="button" class="btn btn-warning pad_4">HOLD</button>';
    						}else if($res->QUES_STATUS==2){
    						    $str1 = '<li><a class="deleteques" attr-quesid="'.$res->QUES_ID.'" href="javascript:void(0)">Delete</a></li>';
    						    $str = '<li><a class="edit_ques_modal" href="javascript:void(0);" attr-quesid="'.$res->QUES_ID.'">Edit</a></li>';
    							$stat='<button type="button" class="btn btn-danger pad_4">INACTIVE</button>';
    						}else{
    						    $str = '<li><a class="edit_ques_modal" href="javascript:void(0);" attr-quesid="'.$res->QUES_ID.'">Edit</a></li>';
    							$stat='<button type="button" class="btn btn-danger pad_4">NFS</button>';
    						}
    						$min['QUES_STATUS']=$stat;
    						$min['TOOLS']='<div class="btn-group m-r-5 m-b-5">
    							<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    							<ul class="dropdown-menu">
    								'.$str.''.$str1.'
    							    <li><a class="view_ques_modal" href="javascript:void(0);" attr-quesid="'.$res->QUES_ID.'">View</a></li>
    							</ul>
    						</div>';
    								
    						array_push($max['data'],$min);
    						$i++;
    					}
    				}
    				$i=$i-1;
    				$max['message']='Total '.$i.' records sended successfully';
    				$max['status']=200;
    				$response['status']=200;
    				json_output($response['status'], $max);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Userpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
    
}
