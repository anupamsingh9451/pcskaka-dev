<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Subadmin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	    // Your own constructor code
		$this->load->library('session');
		$this->load->database('default');
		$this->load->helper('common_helper');
		$this->load->model('subadmin/Subadmingetmodel');
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if ($this->session->userdata('subadminloginid') == '') {
			if (base_url() . 'subadmin' != $actual_link) {
				redirect('subadmin');
			}
		}else{
			if (base_url() . 'subadmin' == $actual_link) {
				redirect('subadmin/ques_user/question');
			}
		}
	}
	public function index()
	{
		$this->load->view('subadmin/index');
	}
	public function book()
	{
		$data['page']='book';
		if($this->uri->segment(3)=='create'){
			$data['subpage']='Create book';
			$loginid = $this->session->userdata('subadminloginid');
			$this->load->view('subadmin/subadminheader',$data);
			$this->load->view('subadmin/book/bookcreate',$data);
			$this->load->view('subadmin/book/bookscripts');
		}
		else if($this->uri->segment(3)=='list'){
			$data['subpage']='List book';
			$this->load->view('subadmin/subadminheader',$data);
			$this->load->view('subadmin/book/booklist',$data);
			$this->load->view('subadmin/book/bookscripts');
		}
		else{
// 			redirect('subadmin/dashboard');
		}
		$this->load->view('subadmin/subadminfooter');
	}
	public function question()
	{
		$data['page']='book';
		if($this->uri->segment(3)=='view'){
			$data['subpage']='question';
			$this->load->view('subadmin/subadminheader',$data);
			if($this->uri->segment(4)!=''){
			    $data['quesdt'] = $this->Subadmingetmodel->get_quesdt_by_id($this->uri->segment(4));
			    $data['userrole'] = $this->Subadmingetmodel->get_user_role_by_id($data['quesdt'][0]->USER_ROLES);
			    $this->load->view('subadmin/question/view_ques',$data);
			    $this->load->view('subadmin/question/viewquesscript');
			}else{
			$data['chapterdt'] = $this->Subadmingetmodel->get_chaptersdt_by_id($this->uri->segment(3))['data'];
			    $this->load->view('subadmin/question/questions',$data);
			}
		}else if($this->uri->segment(3)=='edit'){
			$data['subpage']='question';
			$this->load->view('subadmin/subadminheader',$data);
			if($this->uri->segment(4)!=''){
			    $data['quesdt'] = $this->Subadmingetmodel->get_quesdt_by_id($this->uri->segment(4));
			    $data['all_sub'] = $this->Subadmingetmodel->get_subject_by_libid($data['quesdt'][0]->LIB_ID);
			    $this->load->view('subadmin/question/edit_ques',$data);
			    $this->load->view('subadmin/question/editquestionscripts');
			}else{
			$data['chapterdt'] = $this->Subadmingetmodel->get_chaptersdt_by_id($this->uri->segment(3))['data'];
			    $this->load->view('subadmin/question/questions',$data);
			    $this->load->view('subadmin/question/questionscripts');
			}
			
		}else if($this->uri->segment(3)=='create'){
			$data['subpage']='question';
			$this->load->view('subadmin/subadminheader',$data);
			if($this->uri->segment(4)!=''){
			    $data['chapterdt'] = $this->Subadmingetmodel->get_chaptersdt_by_id($this->uri->segment(4))['data'];
			    $this->load->view('subadmin/question/createquestion',$data);
			    $this->load->view('subadmin/question/questionscripts',$data);
			}else{
			    $data['chapterdt'] = $this->Subadmingetmodel->get_chaptersdt_by_id($this->uri->segment(3))['data'];
			    $this->load->view('subadmin/question/questions',$data);
			    $this->load->view('subadmin/question/questionscripts');
			}
			
		}else{
			$data['subpage']='List book';
			$data['chapterdt'] = $this->Subadmingetmodel->get_chaptersdt_by_id($this->uri->segment(3))['data'];
			$this->load->view('subadmin/subadminheader',$data);
			$this->load->view('subadmin/question/questions',$data);
			$this->load->view('subadmin/question/questionscripts');
			$this->load->view('subadmin/question/viewquesscript');
			$this->load->view('subadmin/question/editquestionscripts');
		}
		$this->load->view('subadmin/subadminfooter');
	}
	
	public function ques_user()
	{
		$data['page']='question';
		
		if($this->uri->segment(3)=='question'){
            $data['subpage']='question';
			$data['alluser'] = $this->Subadmingetmodel->get_all_users();
			$data['subdt'] = $this->Subadmingetmodel->get_all_subject();
			$this->load->view('subadmin/subadminheader',$data);
			$this->load->view('subadmin/ques_user/ques_users',$data);
			$this->load->view('subadmin/ques_user/quesuserscripts');
			$this->load->view('subadmin/question/viewquesscript');
			$this->load->view('subadmin/question/editquestionscripts');

		}else{
// 			redirect('subadmin/dashboard');
		}
		$this->load->view('subadmin/subadminfooter');
	}

}
