<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Studentgetajax extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database('default');
		$this->load->model('student/Authentication_model');
        $this->load->model('student/Studentgetmodel');
        $this->load->model('admin/Admingetmodel');        
        //$this->load->model('Broadgetmodel');
		$this->load->helper('json_output_helper');
		$this->load->helper('common_helper');
        date_default_timezone_set('Asia/Kolkata');
        $this->load->library('cart');
	}
	public function get_student_dt()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    
                    $params = $_REQUEST;
    				$studentid = $_GET['id'];
                    $response = $this->Studentgetmodel->get_student_dt_by_id($studentid);
                    
                    
                    
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
    public function get_payumoney_details()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
    				$tsid = $_GET['tsid'];
    				$check_pay = $this->Studentgetmodel->check_payment_exist($tsid);
    				if(empty($check_pay)){
    				    $response['data'] = $this->Studentgetmodel->get_ques_log_by_quesid($tsid);
    				    $response['message'] = 'ok';
    				}else{
    				    $response['data']  = '';
    				    $response['message'] = 'Not ok';
    				}
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
			}
        }
    }
	public function get_question_html()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $tpid = url_decode($_GET['tpid']);
                    $tpdt = $this->Studentgetmodel->get_tetpaper_dt_by_id($tpid);
                    $data['tpdt'] = $tpdt;
                    $data['allques'] = $this->Studentgetmodel->get_quesdt_by_id(implode(',',json_decode($tpdt[0]->TP_QUESTIONS)));
    				echo $response=json_encode($this->load->view('student/start_paper/qustion_html',$data));
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
                
            }
        }
    }
    public function get_instruction_html(){
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $tpid = url_decode($_GET['tpid']);
		            $tpdt = $this->Studentgetmodel->get_tetpaper_dt_by_id($tpid);
		            $data['tsdts'] = $this->Studentgetmodel->get_tetseries_dt_by_id($tpdt[0]->TS_ID);
                    $data['tpdts'] = $tpdt;
                    $data['userdt'] = $this->Studentgetmodel->get_user_dt_by_id($this->session->userdata('studentloginid'));
    				echo $response = json_encode($this->load->view('student/conduct_paper/instruction',$data));
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
                
            }
        }
    }
    public function get_start_test_paper_html()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $tpid = url_decode($_GET['tpid']);
                    $tpdt = $this->Studentgetmodel->get_tetpaper_dt_by_id($tpid);
                    $data['tsdt'] = $this->Studentgetmodel->get_tetseries_dt_by_id($tpdt[0]->TS_ID);
                    $data['tpdt'] = $tpdt;
                    $data['userdt'] = $this->Studentgetmodel->get_user_dt_by_id($this->session->userdata('studentloginid'));
                    $data['allques'] = $this->Studentgetmodel->get_quesdt_by_id(implode(',',json_decode($tpdt[0]->TP_QUESTIONS)));
    				echo $response = json_encode($this->load->view('student/start_paper/startpaper',$data));
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
                
            }
        }
    }
     public function get_testpaper_result()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
    				$tpid = url_decode($_GET['tpid']);
    				$tpdt = $this->Studentgetmodel->get_tetpaper_dt_by_id($tpid);
    				if(!empty($tpdt)){
    				    $userid = $this->session->userdata('studentloginid');
    				    $result = $this->Studentgetmodel->get_testpaper_result($tpdt[0]->TS_ID,$tpid,$userid);
    				    if(!empty($result)){
    				       $response['message'] = 'ok';
    				       $response['data']  = $result;
    				    }else{
    				        $response['data']  = '';
    				        $response['message'] = 'Not ok';
    				    }
    				}else{
    				    $response['data']  = '';
    				    $response['message'] = 'Not ok';
    				}
    				
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
			}
        }
    }
    
    public function get_payumoney_details_new()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $prod_type = $_GET['prod_type']; 
    				$productids = $_GET['productid'];
    				$userid = $this->session->userdata('studentloginid');
    				$check_pay = $this->Studentgetmodel->check_payment_exist_by_prod_type($userid,$prod_type,$productids);
    				if(empty($check_pay)){
    				    $prod_arr = explode(',',$_GET['productid']);
    				    $discamt = 0;
    				    $promoamt = 0;
    				    foreach($prod_arr as $productid){
    				        /*==================FOR GETTING PRODUCT COST AMOUNT START======================*/
    				        $product_cost = 0;
        				    if($prod_type==1){
        			            $productcost = $this->Admingetmodel->get_library_by_libid($productid)[0]->LIB_COST;
        			            $product_cost = $this->Admingetmodel->get_library_by_libid($productid)[0]->LIB_COST;
        			        }
            				if($prod_type==4){
        			            $productcost = $this->Admingetmodel->get_testseriesdt_by_id($productid)[0]->TS_COST;
        			            $product_cost = $this->Admingetmodel->get_testseriesdt_by_id($productid)[0]->TS_COST;
        			        }
        			        /*==================FOR GETTING PRODUCT COST AMOUNT END======================*/
        			        
        				    /*==================FOR GETTING DISCOUNT AMOUNT START======================*/
        				    $discount = 0;
        				    $coupon_type = 2; //for discount
        				    $discountdt = $this->Studentgetmodel->get_discount_by_prod_type($prod_type,$coupon_type,$productid);
        			        if(!empty($discountdt)){
        			            $discid = $discountdt[0]->COUPON_ID;
        			            if($discountdt[0]->COUPON_MODE==1){
        			                $discamt += $discountdt[0]->COUPON_AMOUNT;
        			                 $discount = $discountdt[0]->COUPON_AMOUNT;
        			            }else{
        			                $discamt += ($product_cost*$discountdt[0]->COUPON_AMOUNT)/100;
        			                $discount = ($product_cost*$discountdt[0]->COUPON_AMOUNT)/100;
        			            }
        			        }else{
        			           $discid = '';
        			           $discount = 0;
        			        }
        			        /*==================FOR GETTING DISCOUNT AMOUNT END======================*/
        			        
        			        /*==================FOR GETTING PROMO AMOUNT START======================*/
        			        
        			        if(isset($_GET['promoid'])){
        			            $promoid = $_GET['promoid'];
        			            $rem_prod_cast = $product_cost-$discount;
        			            if(!empty($_GET['promoid'])){
        			                $promodt = $this->Studentgetmodel->get_promo_dt_by_promoid($_GET['promoid']);
                			        if(!empty($promodt)){
                			            if($promodt[0]->COUPON_MODE==1){
                			                $promoamt += $promodt[0]->COUPON_AMOUNT;
                			            }else{
                			                $promoamt += ($rem_prod_cast*$promodt[0]->COUPON_AMOUNT)/100;
                			            }
                			        }
        			            }
        			        }else{
        			            $promoid = '';
        			        }
        			        /*==================FOR GETTING PROMO AMOUNT END======================*/
    				    }
                        
    				    $amount = $this->Studentgetmodel->get_product_amount_by_prod_type($prod_type,$_GET['productid'])-$discamt-$promoamt;
    				    $response['data'] = $this->Studentgetmodel->get_payumoney_details($prod_type,$_GET['productid'],$amount,$promoid,$discid);
    				    $response['message'] = 'ok';
    				}else{
    				    $response['data']  = '';
    				    $response['message'] = 'Not ok';
    				}
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
			}
        }
    }
    public function get_checkout_html()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    if($_GET['prod_type']==1){
                        $data['libdt'] = $this->Admingetmodel->get_library_by_libid($_GET['productid']);
                        echo $response=json_encode($this->load->view('student/library/checkout_html',$data));
                    }
                    if($_GET['prod_type']==4){
                        $data['tsdt'] = $this->Admingetmodel->get_testseriesdt_by_id($_GET['productid']);
                        echo $response=json_encode($this->load->view('student/testseries/checkout_html',$data));
                    }
    				
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
                
            }
        }
    }
    public function check_promo_validity()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $coupon_type = $_GET['coupon_type'];
                    
                    $productid = $_GET['productid'];
                    $prod_type = $_GET['prod_type'];
    				$promo_input = $_GET['promo_input'];
    				
    				$check_pay = $this->Studentgetmodel->get_promo_dt($promo_input,$prod_type,$productid);
    				if(!empty($check_pay)){
    				    $productcost = 0;
    				    $discamt1 = 0;
    				    $discamt2 = 0;
    				    $prod_arr = explode(',',$_GET['productid']);
    				    foreach($prod_arr as $prod_arrs){
    				        
    				        $product_cost = 0;
    				        if($prod_type==1){
        			            $productcost += $this->Admingetmodel->get_library_by_libid($prod_arrs)[0]->LIB_COST;
        			            $product_cost = $this->Admingetmodel->get_library_by_libid($prod_arrs)[0]->LIB_COST;
        			        }
            				if($prod_type==4){
        			            $productcost += $this->Admingetmodel->get_testseriesdt_by_id($prod_arrs)[0]->TS_COST;
        			            $product_cost = $this->Admingetmodel->get_testseriesdt_by_id($prod_arrs)[0]->TS_COST;
        			        }
            				
            				$discdt = $this->Studentgetmodel->get_discount_by_prod_type($prod_type,2,$prod_arrs);
            				$discount = 0;
        			        if(!empty($discdt)){
        			            if($discdt[0]->COUPON_MODE==1){
        			                $discamt1 += $discdt[0]->COUPON_AMOUNT;
        			                $discount = $discdt[0]->COUPON_AMOUNT;
        			            }else{
        			                $discamt1 += ($product_cost*$discdt[0]->COUPON_AMOUNT)/100;
        			                $discount = ($product_cost*$discdt[0]->COUPON_AMOUNT)/100;
        			            }
        			        }else{
        			            $discamt1 += 0;
        			            $discount = 0;
        			        }
        			        $rem_prod_cast = $product_cost-$discount;
        			        if($check_pay[0]->COUPON_MODE==1){
    			                $discamt2 += $check_pay[0]->COUPON_AMOUNT;
    			            }else{
    			                $discamt2 += ($rem_prod_cast*$check_pay[0]->COUPON_AMOUNT)/100;
    			            }
    				    }
    				    $response['FINAL_AMT']  = convert_decimal_two($productcost-$discamt1-$discamt2);
    				    $response['PROMO_AMT']  = convert_decimal_two($discamt2);
    				    $response['PROMO_ID']  = $check_pay[0]->COUPON_ID;
    				    $response['message'] = 'ok';
    				}else{
    				    $response['data']  = '';
    				    $response['message'] = 'Not ok';
    				}
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
			}
        }
    }
    public function check_validityofcoupon()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $productid = $_GET['productid'];
                    $prod_type = $_GET['prod_type'];
    				$promo_input = $_GET['promo_input'];
                    $check_pay = $this->Studentgetmodel->get_coupon_detail($promo_input,$prod_type,$productid);
                    $cost=get_product_cost_prod_type_prod_id($this->input->get('prod_type'),$this->input->get('productid'));
                    $discountamt=0;
                    if(!empty($check_pay)){
    				    $productcost = 0;
    				    $discamt1 = 0;
    				    $discamt2 = 0;
                        if ($check_pay[0]->COUPON_MODE==1) {
                            $discountamt = $check_pay[0]->COUPON_AMOUNT;
                        } else {
                            $discountamt = ($cost*$check_pay[0]->COUPON_AMOUNT)/100;
                        }
                        
    				    $response['PROMO_ID']  = $check_pay[0]->COUPON_ID;
                        $response['message'] = 'ok';
                        $response['data']  = 'discount found';
    				}else{
    				    $response['data']  = '';
    				    $response['message'] = 'Not ok';
                    }
                    $prodtypedt=get_producttype_dt_by_id($this->input->get('prod_type'));
                    $producttax=json_decode($prodtypedt->PROD_GST);
                    if ($producttax!="" && $producttax!=0) {
                        $response['CGST']=$producttax[0]->CGST;
                        $response['SGST']=$producttax[1]->SGST;
                    }else{
                        $response['CGST']=0;
                        $response['SGST']=0;
                    }
                    $response['DISC_AMT']  = convert_decimal_two($discountamt);
                    $response['BASE_PRICE']  = convert_decimal_two($cost);
                    
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
			}
        }
    }
    public function getcouponcodes()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $get=$this->input->get();
                    $totalproducttype=array();
                    foreach ($get['allproducts'] as $key) {
                        $totalproducttype[]=$key['producttype'];
                    }
                    $totalproducttype=array_unique($totalproducttype);
                    $totalproducttype=count($totalproducttype);
                    $get['totalproducttype']=$totalproducttype;
                    $coupontype="1";
                    $coupons=getallcoupons($get,$coupontype);
                    $response['data']=$coupons;
                    $response['message']='record send successfully';
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
			}
        }
    }
    public function get_payumoney_details_products()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $this->load->model('student/Studentpostmodel');
                    $this->Studentgetmodel->deletenotpurchased_invoice_ofuser($this->session->userdata('studentloginid'));
                    $allproducts=$this->input->get('allproducts');
                        $finalamount=0;
                        $userid = $this->session->userdata('studentloginid');
                        $invoiceno=get_new_invoiceno();
                        $alreadypaid=array();
                        $couponid="";
                        $totalproducttype=array();
                        $productidsarray=array();
                        $invoiceid=array();
                        foreach($allproducts as $products){

                            if($products['producttype']==0){//course
                                $check_pay = $this->Studentgetmodel->check_payment_exist_by_prod_type($userid,$products['producttype'],$products['productid']);
                                if (empty($check_pay)) {
                                    $baseprice=get_product_cost_prod_type_prod_id($products['producttype'], $products['productid']);
                                    $discount2=0; $discount1=0;
                                    $disctype=$coupunid=$taxtype=array();
                                    $discount = $this->Studentgetmodel->get_discount_by_prod_type($products['producttype'],2, $products['productid']);
                                    if(!empty($discount)){
                                        if($discount[0]->COUPON_MODE==1){
                                            $discount1 = $discount[0]->COUPON_AMOUNT;
                                        }else{
                                            $discount1 = ($baseprice*$discount[0]->COUPON_AMOUNT)/100;
                                            $discount1=round($discount1,2);
                                        }
                                        array_push($disctype,array('COUPON_ID'=>$discount[0]->COUPON_ID,'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount1));
                                        $coupunid[]=$discount[0]->COUPON_ID;
                                    }
                                    if (isset($products['coupnid'])) {
                                        if ($products['coupnid']!='undefined'  && $products['coupnid']!='') {
                                            $coupondt=$this->Studentgetmodel->get_promo_dt_by_promoid($products['coupnid']);
                                            if(!empty($coupondt)){
                                                if ($coupondt[0]->COUPON_MODE==1) {
                                                    $discount2 = $coupondt[0]->COUPON_AMOUNT;
                                                } else {
                                                    $discount2 = (($baseprice-$discount1)*$coupondt[0]->COUPON_AMOUNT)/100;
                                                    $discount2=round($discount2,2);
                                                }
                                                if ($coupondt[0]->COUPON_TYPE==1) {
                                                    array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'promocode','DISCOUNT_AMT'=>$discount2));
                                                } else {
                                                    array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount2));
                                                }
                                            }
                                            $coupunid[]=$products['coupnid'];
                                        }
                                    }
                                    $totaldiscount=$discount2+$discount1;
                                    $totalpayableamt=$baseprice-$totaldiscount;
                                    $data=array(
                                        'PROD_TYPE_ID'=>$products['producttype'],
                                        'PRODUCT_ID'=>$products['productid'],
                                        'USER_ID'=>$this->session->userdata('studentloginid'),
                                        'PROD_BASEPRICE'=>$baseprice,
                                        'PROD_DISC_TYPE'=>json_encode($disctype),
                                        'PROD_DISC_AMT'=>$totaldiscount,
                                        'PROD_TAXES'=>json_encode($taxtype),
                                        'INVOICE_NO'=>$invoiceno,
                                        'INOICE_AMOUNT'=>$totalpayableamt,
                                        'INVOICE_TT'=>date('Y-m-d H-i-s'),
                                        'INVOICE_DISCOUNT'=>$totaldiscount,
                                        'COUPON_ID'=>implode(",",$coupunid),
                                    );
                                    $finalamount=$finalamount+$totalpayableamt;
                                    $invoiceid[]=$this->Studentpostmodel->addinvoice($data);
                                    $productidsarray[]=$products['productid'];
                                }else{
                                    $alreadypaid[]=array(
                                        "PRODUCT_ID"=>$products['productid'],
                                        "PRODUCT_TYPE"=>$products['producttype'],
                                    );                                    
                                }    

                            }else if($products['producttype']==1){ //LIBRARY
                                $check_pay = $this->Studentgetmodel->check_payment_exist_by_prod_type($userid,$products['producttype'],$products['productid']);
                                if (empty($check_pay)) {
                                    $baseprice=get_product_cost_prod_type_prod_id($products['producttype'], $products['productid']);
                                    $discount2=0; $discount1=0;
                                    $disctype=$coupunid=$taxtype=array();
                                    $discount = $this->Studentgetmodel->get_discount_by_prod_type($products['producttype'],2, $products['productid']);
                                    if(!empty($discount)){
                                        if($discount[0]->COUPON_MODE==1){
                                            $discount1 = $discount[0]->COUPON_AMOUNT;
                                        }else{
                                            $discount1 = ($baseprice*$discount[0]->COUPON_AMOUNT)/100;
											$discount1=round($discount1,2);
                                        }
                                        array_push($disctype,array('COUPON_ID'=>$discount[0]->COUPON_ID,'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount1));
                                        $coupunid[]=$discount[0]->COUPON_ID;
                                    }
                                    if (isset($products['coupnid'])) {
                                        if ($products['coupnid']!='undefined'  && $products['coupnid']!='') {
                                            $coupondt=$this->Studentgetmodel->get_promo_dt_by_promoid($products['coupnid']);
											if(!empty($coupondt)){
												if ($coupondt[0]->COUPON_MODE==1) {
													$discount2 = $coupondt[0]->COUPON_AMOUNT;
												} else {
													$discount2 = (($baseprice-$discount1)*$coupondt[0]->COUPON_AMOUNT)/100;
													$discount2=round($discount2,2);
												}
												if ($coupondt[0]->COUPON_TYPE==1) {
													array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'promocode','DISCOUNT_AMT'=>$discount2));
												} else {
													array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount2));
												}
											}
                                            $coupunid[]=$products['coupnid'];
                                        }
                                    }
                                    $totaldiscount=$discount2+$discount1;
                                    $totalpayableamt=$baseprice-$totaldiscount;
                                    $data=array(
                                        'PROD_TYPE_ID'=>$products['producttype'],
                                        'PRODUCT_ID'=>$products['productid'],
                                        'USER_ID'=>$this->session->userdata('studentloginid'),
                                        'PROD_BASEPRICE'=>$baseprice,
                                        'PROD_DISC_TYPE'=>json_encode($disctype),
                                        'PROD_DISC_AMT'=>$totaldiscount,
                                        'PROD_TAXES'=>json_encode($taxtype),
                                        'INVOICE_NO'=>$invoiceno,
                                        'INOICE_AMOUNT'=>$totalpayableamt,
                                        'INVOICE_TT'=>date('Y-m-d H-i-s'),
                                        'INVOICE_DISCOUNT'=>$totaldiscount,
                                        'COUPON_ID'=>implode(",",$coupunid),
                                    );
                                    $finalamount=$finalamount+$totalpayableamt;
                                    $invoiceid[]=$this->Studentpostmodel->addinvoice($data);
                                    $productidsarray[]=$products['productid'];
                                }else{
                                    $alreadypaid[]=array(
                                        "PRODUCT_ID"=>$products['productid'],
                                        "PRODUCT_TYPE"=>$products['producttype'],
                                    );                                    
                                }    
                            }else if($products['producttype']==2){
                                $check_pay = $this->Studentgetmodel->check_payment_exist_by_prod_type($userid,$products['producttype'],$products['productid']);
                                if (empty($check_pay)) {
                                    $baseprice=get_product_cost_prod_type_prod_id($products['producttype'], $products['productid']);
                                    $discount2=0; $discount1=0;
                                    $disctype=$coupunid=$taxtype=array();
                                    $discount = $this->Studentgetmodel->get_discount_by_prod_type($products['producttype'],2, $products['productid']);
                                    if(!empty($discount)){
                                        if($discount[0]->COUPON_MODE==1){
                                            $discount1 = $discount[0]->COUPON_AMOUNT;
                                        }else{
                                            $discount1 = ($baseprice*$discount[0]->COUPON_AMOUNT)/100;
                                            $discount1=round($discount1,2);
                                        }
                                        array_push($disctype,array('COUPON_ID'=>$discount[0]->COUPON_ID,'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount1));
                                        $coupunid[]=$discount[0]->COUPON_ID;
                                    }
                                    if (isset($products['coupnid'])) {
                                        if ($products['coupnid']!='undefined'  && $products['coupnid']!='') {
                                            $coupondt=$this->Studentgetmodel->get_promo_dt_by_promoid($products['coupnid']);
                                            if(!empty($coupondt)){
                                                if ($coupondt[0]->COUPON_MODE==1) {
                                                    $discount2 = $coupondt[0]->COUPON_AMOUNT;
                                                } else {
                                                    $discount2 = (($baseprice-$discount1)*$coupondt[0]->COUPON_AMOUNT)/100;
                                                    $discount2=round($discount2,2);
                                                }
                                                if ($coupondt[0]->COUPON_TYPE==1) {
                                                    array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'promocode','DISCOUNT_AMT'=>$discount2));
                                                } else {
                                                    array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount2));
                                                }
                                            }
                                            $coupunid[]=$products['coupnid'];
                                        }
                                    }
                                    $totaldiscount=$discount2+$discount1;
                                    $totalpayableamt=$baseprice-$totaldiscount;
                                    $data=array(
                                        'PROD_TYPE_ID'=>$products['producttype'],
                                        'PRODUCT_ID'=>$products['productid'],
                                        'USER_ID'=>$this->session->userdata('studentloginid'),
                                        'PROD_BASEPRICE'=>$baseprice,
                                        'PROD_DISC_TYPE'=>json_encode($disctype),
                                        'PROD_DISC_AMT'=>$totaldiscount,
                                        'PROD_TAXES'=>json_encode($taxtype),
                                        'INVOICE_NO'=>$invoiceno,
                                        'INOICE_AMOUNT'=>$totalpayableamt,
                                        'INVOICE_TT'=>date('Y-m-d H-i-s'),
                                        'INVOICE_DISCOUNT'=>$totaldiscount,
                                        'COUPON_ID'=>implode(",",$coupunid),
                                    );
                                    $finalamount=$finalamount+$totalpayableamt;
                                    $invoiceid[]=$this->Studentpostmodel->addinvoice($data);
                                    $productidsarray[]=$products['productid'];
                                }else{
                                    $alreadypaid[]=array(
                                        "PRODUCT_ID"=>$products['productid'],
                                        "PRODUCT_TYPE"=>$products['producttype'],
                                    );                                    
                                }    

                            }else if($products['producttype']==3){
                                $check_pay = $this->Studentgetmodel->check_payment_exist_by_prod_type($userid,$products['producttype'],$products['productid']);
                                if (empty($check_pay)) {
                                    $baseprice=get_product_cost_prod_type_prod_id($products['producttype'], $products['productid']);
                                    $discount2=0; $discount1=0;
                                    $disctype=$coupunid=$taxtype=array();
                                    $discount = $this->Studentgetmodel->get_discount_by_prod_type($products['producttype'],2, $products['productid']);
                                    if(!empty($discount)){
                                        if($discount[0]->COUPON_MODE==1){
                                            $discount1 = $discount[0]->COUPON_AMOUNT;
                                        }else{
                                            $discount1 = ($baseprice*$discount[0]->COUPON_AMOUNT)/100;
                                            $discount1=round($discount1,2);
                                        }
                                        array_push($disctype,array('COUPON_ID'=>$discount[0]->COUPON_ID,'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount1));
                                        $coupunid[]=$discount[0]->COUPON_ID;
                                    }
                                    if (isset($products['coupnid'])) {
                                        if ($products['coupnid']!='undefined'  && $products['coupnid']!='') {
                                            $coupondt=$this->Studentgetmodel->get_promo_dt_by_promoid($products['coupnid']);
                                            if(!empty($coupondt)){
                                                if ($coupondt[0]->COUPON_MODE==1) {
                                                    $discount2 = $coupondt[0]->COUPON_AMOUNT;
                                                } else {
                                                    $discount2 = (($baseprice-$discount1)*$coupondt[0]->COUPON_AMOUNT)/100;
                                                    $discount2=round($discount2,2);
                                                }
                                                if ($coupondt[0]->COUPON_TYPE==1) {
                                                    array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'promocode','DISCOUNT_AMT'=>$discount2));
                                                } else {
                                                    array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount2));
                                                }
                                            }
                                            $coupunid[]=$products['coupnid'];
                                        }
                                    }
                                    $totaldiscount=$discount2+$discount1;
                                    $totalpayableamt=$baseprice-$totaldiscount;
                                    $data=array(
                                        'PROD_TYPE_ID'=>$products['producttype'],
                                        'PRODUCT_ID'=>$products['productid'],
                                        'USER_ID'=>$this->session->userdata('studentloginid'),
                                        'PROD_BASEPRICE'=>$baseprice,
                                        'PROD_DISC_TYPE'=>json_encode($disctype),
                                        'PROD_DISC_AMT'=>$totaldiscount,
                                        'PROD_TAXES'=>json_encode($taxtype),
                                        'INVOICE_NO'=>$invoiceno,
                                        'INOICE_AMOUNT'=>$totalpayableamt,
                                        'INVOICE_TT'=>date('Y-m-d H-i-s'),
                                        'INVOICE_DISCOUNT'=>$totaldiscount,
                                        'COUPON_ID'=>implode(",",$coupunid),
                                    );
                                    $finalamount=$finalamount+$totalpayableamt;
                                    $invoiceid[]=$this->Studentpostmodel->addinvoice($data);
                                    $productidsarray[]=$products['productid'];
                                }else{
                                    $alreadypaid[]=array(
                                        "PRODUCT_ID"=>$products['productid'],
                                        "PRODUCT_TYPE"=>$products['producttype'],
                                    );                                    
                                }    

                            }else if($products['producttype']==4){//TEST SERIES
                                $check_pay = $this->Studentgetmodel->check_payment_exist_by_prod_type($userid,$products['producttype'],$products['productid']);
                                if (empty($check_pay)) {
                                    $baseprice=get_product_cost_prod_type_prod_id($products['producttype'], $products['productid']);
                                    $discount2=0; $discount1=0;
                                    $disctype=$coupunid=$taxtype=array();
                                    $discount = $this->Studentgetmodel->get_discount_by_prod_type($products['producttype'],2, $products['productid']);
                                    if(!empty($discount)){
                                        if($discount[0]->COUPON_MODE==1){
                                            $discount1 = $discount[0]->COUPON_AMOUNT;
                                        }else{
                                            $discount1 = ($baseprice*$discount[0]->COUPON_AMOUNT)/100;
                                        }
                                        array_push($disctype,array('COUPON_ID'=>$discount[0]->COUPON_ID,'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount1));
                                        $coupunid[]=$discount[0]->COUPON_ID;
                                    }
                                    if (isset($products['coupnid'])) {
                                        if ($products['coupnid']!='undefined' && $products['coupnid']!='') {
                                            $coupondt=$this->Studentgetmodel->get_promo_dt_by_promoid($products['coupnid']);
                                            if ($coupondt[0]->COUPON_MODE==1) {
                                                $discount2 = $coupondt[0]->COUPON_AMOUNT;
                                            } else {
                                                $discount2 = (($baseprice-$discount1)*$coupondt[0]->COUPON_AMOUNT)/100;
                                            }
                                            if ($coupondt[0]->COUPON_TYPE==1) {
                                                array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'promocode','DISCOUNT_AMT'=>$discount2));
                                            } else {
                                                array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount2));
                                            }
                                            $coupunid[]=$products['coupnid'];
                                        }
                                    }
                                    $totaldiscount=$discount2+$discount1;
                                    $totalpayableamt=$baseprice-$totaldiscount;
                                    $data=array(
                                        'PROD_TYPE_ID'=>$products['producttype'],
                                        'PRODUCT_ID'=>$products['productid'],
                                        'USER_ID'=>$this->session->userdata('studentloginid'),
                                        'PROD_BASEPRICE'=>$baseprice,
                                        'PROD_DISC_TYPE'=>json_encode($disctype),
                                        'PROD_DISC_AMT'=>$totaldiscount,
                                        'PROD_TAXES'=>json_encode($taxtype),
                                        'INVOICE_NO'=>$invoiceno,
                                        'INOICE_AMOUNT'=>$totalpayableamt,
                                        'INVOICE_TT'=>date('Y-m-d H-i-s'),
                                        'INVOICE_DISCOUNT'=>$totaldiscount,
                                        'COUPON_ID'=>implode(",",$coupunid),
                                    );
                                    $finalamount=$finalamount+$totalpayableamt;
                                    $invoiceid[]=$this->Studentpostmodel->addinvoice($data);
                                    $productidsarray[]=$products['productid'];
                                }else{
                                    $alreadypaid[]=array(
                                        "PRODUCT_ID"=>$products['productid'],
                                        "PRODUCT_TYPE"=>$products['producttype'],
                                    );
                                }    
                            }else if($products['producttype']==5){
                            }else if($products['producttype']==6){ //Practice Test 
                                $check_pay = $this->Studentgetmodel->check_payment_exist_by_prod_type($userid,$products['producttype'],$products['productid']);
                                if (empty($check_pay)) {
                                    $baseprice=get_product_cost_prod_type_prod_id($products['producttype'], $products['productid']);
                                    $discount2=0; $discount1=0;
                                    $disctype=$coupunid=$taxtype=array();
                                    $discount = $this->Studentgetmodel->get_discount_by_prod_type($products['producttype'],2, $products['productid']);
                                    if(!empty($discount)){
                                        if($discount[0]->COUPON_MODE==1){
                                            $discount1 = $discount[0]->COUPON_AMOUNT;
                                        }else{
                                            $discount1 = ($baseprice*$discount[0]->COUPON_AMOUNT)/100;
                                        }
                                        array_push($disctype,array('COUPON_ID'=>$discount[0]->COUPON_ID,'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount1));
                                        $coupunid[]=$discount[0]->COUPON_ID;
                                    }
                                    if (isset($products['coupnid'])) {
                                        if ($products['coupnid']!='undefined' && $products['coupnid']!='') {
                                            $coupondt=$this->Studentgetmodel->get_promo_dt_by_promoid($products['coupnid']);
                                            if ($coupondt[0]->COUPON_MODE==1) {
                                                $discount2 = $coupondt[0]->COUPON_AMOUNT;
                                            } else {
                                                $discount2 = (($baseprice-$discount1)*$coupondt[0]->COUPON_AMOUNT)/100;
                                            }
                                            if ($coupondt[0]->COUPON_TYPE==1) {
                                                array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'promocode','DISCOUNT_AMT'=>$discount2));
                                            } else {
                                                array_push($disctype,array('COUPON_ID'=>$products['coupnid'],'COUPON_TYPE'=>'discount','DISCOUNT_AMT'=>$discount2));
                                            }
                                            $coupunid[]=$products['coupnid'];
                                        }
                                    }
                                    $totaldiscount=$discount2+$discount1;
                                    $totalpayableamt=$baseprice-$totaldiscount;
                                    $data=array(
                                        'PROD_TYPE_ID'=>$products['producttype'],
                                        'PRODUCT_ID'=>$products['productid'],
                                        'USER_ID'=>$this->session->userdata('studentloginid'),
                                        'PROD_BASEPRICE'=>$baseprice,
                                        'PROD_DISC_TYPE'=>json_encode($disctype),
                                        'PROD_DISC_AMT'=>$totaldiscount,
                                        'PROD_TAXES'=>json_encode($taxtype),
                                        'INVOICE_NO'=>$invoiceno,
                                        'INOICE_AMOUNT'=>$totalpayableamt,
                                        'INVOICE_TT'=>date('Y-m-d H-i-s'),
                                        'INVOICE_DISCOUNT'=>$totaldiscount,
                                        'COUPON_ID'=>implode(",",$coupunid),
                                    );
                                    $finalamount=$finalamount+$totalpayableamt;
                                    $invoiceid[]=$this->Studentpostmodel->addinvoice($data);
                                    $productidsarray[]=$products['productid'];
                                }else{
                                    $alreadypaid[]=array(
                                        "PRODUCT_ID"=>$products['productid'],
                                        "PRODUCT_TYPE"=>$products['producttype'],
                                    );
                                }    
                            }
                            $totalproducttype[]=$products['producttype'];
                        }
                        $totalproducttype=array_unique($totalproducttype);
                        $producttypes=$totalproducttype;
                        $response['message'] = 'ok';
                        $prod_type=$producttypes[0];
                        $invoiceid=implode(",",array_unique($invoiceid));
                        $promoid=$couponid;
                        $discid='';
                        $finalamount=round($finalamount,2);
                        $response['data'] = $this->Studentgetmodel->get_payumoney_details($prod_type,$invoiceid,$finalamount,$promoid,$discid);
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
			}
        }
    }


    public function getpurchase2content()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {

                    $get=$this->input->get();
                    $data = [];

                    switch ($get["purchase2type"]) {
                        case 0:
                            $i = 0;
                            foreach($this->Studentgetmodel->get_all_courses(0,9999999) as $row){
                                $data[$i]["ID"] = $row->COURSE_ID;
                                $data[$i]["PURCHASE2TYPE"] = 0;
                                $data[$i]["PURCHASE2TYPENAME"] = "Course";
                                $data[$i]["NAME"] = $row->COURSE_NAME;
                                $data[$i]["AMT"] = $row->COURSE_AMT;
                                $data[$i]["CONTENT_AMT"] = $row->COURSE_CONTENT_AMT;
                                $data[$i]["CONTENT_DISC"] = $row->COURSE_CONTENT_DISC;
                                $data[$i]["CONTENT_SUMMARY"] = array(
                                    array("name"=>"Exams",
                                          "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"LIB_STATUS"=>1],'library')),
                                    array("name"=>"Subjects",
                                          "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"SUB_STATUS"=>1],'subjects')),
                                    array("name"=>"Test Series",
                                          "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"TS_STATUS"=>1],'test_series')),
                                    array("name"=>"Books",
                                          "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"BOOK_STATUS"=>1],'books'))
                                );
                                $i++;
                            }
                            break;

                        case 1:
                            $i = 0;
                            foreach($this->Studentgetmodel->get_all_library(0,9999999) as $row){
                                $data[$i]["ID"] = $row->LIB_ID;
                                $data[$i]["PURCHASE2TYPE"] = 1;
                                $data[$i]["PURCHASE2TYPENAME"] = "Exams";
                                $data[$i]["NAME"] = $row->LIB_NAME;
                                $data[$i]["AMT"] = $row->LIB_COST;
                                $data[$i]["CONTENT_AMT"] = $row->LIB_CONTENT_AMT;
                                $data[$i]["CONTENT_DISC"] = $row->LIB_CONTENT_DISC;
                                $data[$i]["CONTENT_SUMMARY"] = array(
                                    array("name"=>"Subjects",
                                         "count"=>$this->Studentgetmodel->get_product_type_count(["LIB_ID"=>$row->LIB_ID,"SUB_STATUS"=>1],'subjects')),
                                    array("name"=>"Test Series",
                                          "count"=>$this->Studentgetmodel->get_product_type_count(["LIBRARY_ID"=>$row->LIB_ID,"TS_STATUS"=>1],'test_series')),
                                    array("name"=>"Books",
                                          "count"=>$this->Studentgetmodel->get_product_type_count(["LIB_ID"=>$row->LIB_ID,"BOOK_STATUS"=>1],'books'))
                                );
                                $i++;
                            }
                            break;

                        case 2:
                            $i = 0;
                            foreach($this->Studentgetmodel->get_all_subjects(0,9999999) as $row){
                                $data[$i]["ID"] = $row->SUB_ID;
                                $data[$i]["PURCHASE2TYPE"] = 2;
                                $data[$i]["PURCHASE2TYPENAME"] = "Subjects";
                                $data[$i]["NAME"] = $row->SUB_NAME;
                                $data[$i]["AMT"] = $row->SUB_COST;
                                $data[$i]["CONTENT_AMT"] = $row->SUB_CONTENT_AMT;
                                $data[$i]["CONTENT_DISC"] = $row->SUB_CONTENT_DISC;
                                $data[$i]["CONTENT_SUMMARY"] = array(
                                    array("name"=>"Books",
                                          "count"=>$this->Studentgetmodel->get_product_type_count(["SUB_ID"=>$row->SUB_ID,"BOOK_STATUS"=>1],'books'))
                                );
                                $i++;
                            }
                            break;

                        case 3:
                            $i = 0;
                            foreach($this->Studentgetmodel->get_all_books(0,9999999) as $row){
                                $data[$i]["ID"] = $row->BOOK_ID;
                                $data[$i]["PURCHASE2TYPE"] = 3;
                                $data[$i]["PURCHASE2TYPENAME"] = "Books";
                                $data[$i]["NAME"] = $row->BOOK_NAME;
                                $data[$i]["AMT"] = $row->BOOK_COST;
                                $data[$i]["CONTENT_AMT"] = $row->BOOK_CONTENT_AMT;
                                $data[$i]["CONTENT_DISC"] = $row->BOOK_CONTENT_DISC;
                                $data[$i]["CONTENT_SUMMARY"] = array();
                                $i++;
                            }
                            break;
                        
                        default:
                            
                            break;
                    }
                    
                    $response['data']=$data;
                    $response['message']='record send successfully';
                    json_output($response['status'], $response);

                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
    }
}
