<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Adminpostajax extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database('default');
		$this->load->library('email');
		$this->load->model('admin/Authentication_model');
        $this->load->model('admin/Adminpostmodel');
		$this->load->model('admin/Admingetmodel');
		$this->load->helper('json_output_helper');
		$this->load->helper('common_helper');
		date_default_timezone_set('Asia/Calcutta'); 
	}
	public function adminlogin()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
				$username = $_POST['username'];
                $password = md5($_POST['password']);
                $type = 1; // 1 for Admin
                if(isset($_POST['loginagain'])){
                    $loginagain = $_POST['loginagain'];
                }else{
                    $loginagain = '';
                }
                $response = $this->Adminpostmodel->login($username, $password, $type,$loginagain);
                json_output($response['status'], $response);
                if ($response['message'] == "ok"){
                    // insert_activity_history(1);
                }
            }
        }
    }
	public function adminlogout()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $response = $this->Adminpostmodel->logout();
                    json_output(200, $response);
                    // insert_activity_history(2);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function librarycreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					try{
						$check=array();
						$data=$this->Admingetmodel->get_library_by_name(strtoupper($_POST['libraryname']));
						if(empty($data['data'])){
							$details = array(
								'COURSE_ID'=>$_POST['course'],
								'LIB_NAME'=>strtoupper($_POST['libraryname']),
								'LIB_COST'=>$_POST['libcost'],
								'LIB_POPULAR'=>$_POST['popular'],
								'LIB_STATUS'=>1
							);
							if(!empty($_FILES['library_img']['name'])){
								$upload_loc='assets/product_images/library/';
								$config['upload_path']          = './'.$upload_loc;
								$config['allowed_types']        = 'gif|jpg|png|jpeg';
								$config['encrypt_name']=TRUE;
								
								//Load upload library and initialize configuration
								$this->load->library('upload',$config);
								$this->upload->initialize($config);
								
								if($this->upload->do_upload('library_img')){
									$uploadData = $this->upload->data();
									//$details['loc']=$uploadData;
									$response['LIB_IMG']=$uploadData;
									$details['LIB_IMG'] = $upload_loc.$uploadData['file_name'];
								}else{
									throw new Exception(strip_tags($this->upload->display_errors()));
								}
							}
							$areaid=$this->Adminpostmodel->addlibrary($details);
							
							if(!empty($areaid)){
								$response['message']='Data Added Successfully';
								$response['data']='ok';
							}else{
								$response['message']='Something wents wrong';
								$response['data']='Notok';
							}
						}else{
							$response['message']='Library is already present';
							$response['data']='Notok';
						}
						json_output(200, $response);
					} catch (Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}	
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function editlibrary()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					try{
						$libid=$_POST['libid'];
						$data = array(
							'COURSE_ID'=>$_POST['course'],
							'LIB_NAME' =>strtoupper($_POST['libnm']),
							'LIB_COST'=>$_POST['libcost'],
							'LIB_POPULAR'=>$_POST['popular'],
							'LIB_STATUS' =>strtoupper($_POST['libstatus']),
						);
						if(!empty($_FILES['library_img']['name'])){
							$upload_loc='assets/product_images/library/';
							$config['upload_path']          = './'.$upload_loc;
							$config['allowed_types']        = 'gif|jpg|png|jpeg';
							$config['encrypt_name']=TRUE;
							// $config['max_width']            = 500;
							// $config['max_height']           = 500;
							
							//Load upload library and initialize configuration
							$this->load->library('upload',$config);
							$this->upload->initialize($config);
							
							if($this->upload->do_upload('library_img')){
								$uploadData = $this->upload->data();
								//$details['loc']=$uploadData;
								$data['LIB_IMG'] = $upload_loc.$uploadData['file_name'];
							}else{
								throw new Exception(strip_tags($this->upload->display_errors()));
							}
						}
						$this->Adminpostmodel->updatelibrary($data,$libid);
						$response['message']='Data Edited Successfully';
						$response['data']='ok';
						json_output(200, $response);
					} catch (\Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (\Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}	
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
	}
	public function editlibrarycost()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					try{
						$libid=$_POST['libid'];
						$datalib = array(
							'LIB_COST'=>$_POST['libcost'],
							'LIB_PAY_TYPE'=>$_POST['paystatus'],
						);
                        if ($_POST['paystatus']==1) {
                            $datalib['IS_SALE_PROD_TYPE']=1;
                            $datasub = array(
                                'IS_SALE_PROD_TYPE'=>1,
                            );
                            $databook = array(
                                'IS_SALE_PROD_TYPE'=>1,
							);
							if($this->Adminpostmodel->updatelibrary($datalib,$libid) && $this->Adminpostmodel->updatesubject_by_lib($datasub,$libid) && $this->Adminpostmodel->updatebook_by_lib($databook,$libid)){
								$response['message']='Data Edited Successfully';
								$response['data']='ok';
							}else{
								$response['message']='Failed to edit data.';
								$response['data']='not ok';	
							}
                        }else if($this->Adminpostmodel->updatelibrary($datalib,$libid)){
							$response['message']='Data Edited Successfully';
							$response['data']='ok';
						}else{
							$response['message']='Failed to edit data.';
							$response['data']='not ok';
						}
						json_output(200, $response);
					} catch (\Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (\Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}	
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
    }
    public function subjectcreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                 
					$check=array();
					$data=$this->Admingetmodel->get_subject_by_name_and_libid(strtoupper($_POST['subjname']),$_POST['library']);
				    $library = $_POST['library'];
					if(empty($data['data'])){
						$details = array(
									"SUB_NAME"=>strtoupper($_POST['subjname']),
									"COURSE_ID"=>$_POST['course'],
        							"LIB_ID"=>$_POST['library'],
        							"SUB_STATUS"=>1,
        							"SUB_CREATED_AT" =>$_POST['subcreated'],
        							"SUB_ENTRY_TT" =>date('Y-m-d H:i:s')
        						);
        				$subid=$this->Adminpostmodel->addsubject($details);
        				if(!empty($subid)){
        					$response['message']='Data Added Successfully';
        					$response['data']='ok';
        				}else{
        					$response['message']='Something wents wrong';
        					$response['data']='Notok';
        				}
    				}else{
    					$response['message']='Subject is already present';
    					$response['data']='Notok';
    				}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
    }
    public function editsubject()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$libid=$_POST['libid'];
					$subid=$_POST['subid'];
					$details=$this->Admingetmodel->check_subject_exist(strtoupper($_POST['subnm']),$_POST['libid'],$subid);
					if(empty($details['data'])){
    					$data = array(
    						'SUB_NAME' =>strtoupper($_POST['subnm']),
    						'SUB_STATUS' =>strtoupper($_POST['substatus']),
    						'SUB_LAST_UPDATED' =>date('Y-m-d H:i:s')
    					);
    					$this->Adminpostmodel->updatesubject($data,$subid);
    					
    					$response['message']='Data Edited Successfully';
    					$response['data']='ok';
					}else{
						$response['message']='Subject is already present';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function bookcreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					try{
						$check=array();
						
						$data=$this->Admingetmodel->check_book_exist(strtoupper($_POST['bookname']),$_POST['subject']);
						$library = $_POST['library'];
						$subject = $_POST['subject'];
						if(empty($data)){
							$details = array(
								'SUB_ID'=>$subject,
								'LIB_ID'=>$library,
								'COURSE_ID' =>$_POST['course'],
								'BOOK_NAME' =>$_POST['bookname'],
								'BOOK_CHAPTER_NOS' =>$_POST['chapterno'],
								'BOOK_STATUS'=>1,
								'BOOK_CREATED_AT' =>$_POST['bookcreatedat'],
								'BOOK_ENTRY_TT' =>date('Y-m-d H:i:s'),
							);
							if(!empty($_FILES['book_img']['name'])){
								$upload_loc='assets/product_images/books/';
								$config['upload_path']          = './'.$upload_loc;
								$config['allowed_types']        = 'gif|jpg|jpeg';
								$config['encrypt_name']=TRUE;
								// $config['max_width']            = 400;
								// $config['max_height']           = 400;
								
								//Load upload library and initialize configuration
								$this->load->library('upload',$config);
								$this->upload->initialize($config);
								
								if($this->upload->do_upload('book_img')){
									$uploadData = $this->upload->data();
									//$details['loc']=$uploadData;
									$details['BOOK_IMG'] = base_url($upload_loc.$uploadData['file_name']);
								}else{
									throw new Exception(strip_tags($this->upload->display_errors()));
								}
							}
							$bookid=$this->Adminpostmodel->addbook($details);
							
							
							$details1 = array(
								'SUB_ID' => $subject,
								'LIB_ID' => $library,
								'BOOK_ID' => $bookid,
								'CHAPTER_NAME' => $_POST['bookname'],
								'CHAPTER_DESC' => '',
								'CHAPTER_QUE_NOS' => '',
								'CHAPTER_CREATED_AT' => $_POST['bookcreatedat'],
								'CHAPTER_STATUS' => 1,
								'CHAPTER_ENTRY_TT' => date('Y-m-d H:i:s'),
							);
							$chapterid=$this->Adminpostmodel->addchapter($details1);
							
							if(!empty($bookid)){
								$response['message']='Data Added Successfully';
								$response['data']='ok';
							}else{
								$response['message']='Something wents wrong';
								$response['data']='Notok';
							}
						}else{
							$response['message']='Book is already present';
							$response['data']='Notok';
						}
						json_output(200, $response);
					} catch (\Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (\Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}		
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function editbookform()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$bookid=$_POST['bookid'];
					$subid=$_POST['subid'];
					$details=$this->Admingetmodel->check_book_exist(strtoupper($_POST['booknm']),$subid,$bookid);
					if(empty($details)){
    					$data = array(
    						'BOOK_NAME' =>strtoupper($_POST['booknm']),
    						'BOOK_CHAPTER_NOS' =>$_POST['chapterno'],
    						'BOOK_STATUS' => $_POST['bookstatus'],
    						'BOOK_LAST_UPDATED' =>date('Y-m-d H:i:s')
						);
						if(!empty($_FILES['book_img']['name'])){
							$upload_loc='assets/product_images/courses/';
							$config['upload_path']          = './'.$upload_loc;
							$config['allowed_types']        = 'gif|jpg|jpeg';
							$config['encrypt_name']=TRUE;
							// $config['max_width']            = 400;
							// $config['max_height']           = 400;
							
							//Load upload library and initialize configuration
							$this->load->library('upload',$config);
							$this->upload->initialize($config);
							
							if($this->upload->do_upload('book_img')){
								$uploadData = $this->upload->data();
								//$details['loc']=$uploadData;
								$data['BOOK_IMG'] = base_url($upload_loc.$uploadData['file_name']);
							}else{
								throw new Exception(strip_tags($this->upload->display_errors()));
							}
						}
    					$this->Adminpostmodel->updatebook($data,$bookid);
    					
    					$response['message']='Data Edited Successfully';
    					$response['data']='ok';
					}else{
						$response['message']='Book is already present';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function chaptercreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$check=array();
					$library = $_POST['library'];
				    $subject = $_POST['subject'];
				    $book = $_POST['book'];
				    
					$data=$this->Admingetmodel->check_chapter_exist(strtoupper($_POST['chaptername']),$book);
				    
					if(empty($data)){
						$details = array(
							'SUB_ID'=>$subject,
							'LIB_ID'=>$library,
							'BOOK_ID'=>$book,
							'CHAPTER_NAME' =>$_POST['chaptername'],
							'CHAPTER_DESC' =>$_POST['chapter_desc'],
							'CHAPTER_QUE_NOS' =>$_POST['totalques'],
							'CHAPTER_CREATED_AT' =>$_POST['chaptercreatedate'],
							'CHAPTER_STATUS'=>1,
							'CHAPTER_ENTRY_TT' =>date('Y-m-d H:i:s'),
						);
						$chapterid=$this->Adminpostmodel->addchapter($details);
						
						if(!empty($chapterid)){
							$response['message']='Data Added Successfully';
							$response['data']='ok';
						}else{
							$response['message']='Something wents wrong';
							$response['data']='Notok';
						}
					}else{
						$response['message']='Chapter is already present';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function updatechapter()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$bookid=$_POST['bookid'];
					$chapterid=$_POST['chapterid'];
					$details=$this->Admingetmodel->check_chapter_exist(strtoupper($_POST['chapternm']),$bookid,$chapterid);
					if(empty($details)){
    					$data = array(
    						'CHAPTER_NAME' =>strtoupper($_POST['chapternm']),
    						'CHAPTER_QUE_NOS' =>$_POST['quesno'],
    						'CHAPTER_STATUS' => $_POST['chapterstatus'],
    						'CHAPTER_LAST_UPDATED' =>date('Y-m-d H:i:s')
    					);
    					$this->Adminpostmodel->updatechapter($data,$chapterid);
    					
    					$response['message']='Data Edited Successfully';
    					$response['data']='ok';
					}else{
						$response['message']='Book is already present';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function questioncreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$check=array();
					$chapter = $_POST['chapter'];
				    $subject = $_POST['subject'];
				    $book = $_POST['book'];
				    $ques_levele = $_POST['ques_levele'];
				    $question = $_POST['question'];
				    $ques_type = $_POST['ques_type'];
				    
					if($ques_type==1){
						$details = array(
							'CHAPTER_ID' => $chapter,
							'BOOK_ID' => $book,
							'SUB_ID' => $subject,
							'QUES_TYPE' => $ques_type,
							'QUES' => $question,
							'QUES_ANSWERS' => $_POST['subj_answer'],
							'QUES_P_MARKING' => $_POST['subj_positive_mark'],
							'QUES_N_MARKING' => $_POST['subj_negative_mark'],
							'QUES_DURATION' => $_POST['subj_duration'],
							'QUES_DIFFICULTY_LEVEL' => $ques_levele,
							'QUES_HINT' => $_POST['subj_ques_hint'],
							'QUES_EXPLANATION' => $_POST['subj_ques_explain'],
							'QUES_STATUS'=> 0,
							'QUES_CREATED_BY' => $this->session->userdata('adminloginid'),
							'QUES_APPROVED_BY' => 0,
							'QUES_CRATED_AT' => $_POST['subj_created_date'],
							'QUES_ENTRY_TT' => date('Y-m-d H:i:s'),
						);
						
					}else{
					    $ques_option = $_POST['ques_option'];
					    $data1 = array(
    						'QUESTION' =>$question,
    						'OPTION' =>$ques_option
    					);
        				$ques = json_encode($data1);
						$details = array(
							'CHAPTER_ID' => $chapter,
							'BOOK_ID' => $book,
							'SUB_ID' => $subject,
							'QUES_TYPE' => $ques_type,
							'QUES' => $ques,
							'QUES_ANSWERS' => implode(',',$_POST['obj_answer']),
							'QUES_P_MARKING' => $_POST['obj_positive_mark'],
							'QUES_N_MARKING' => $_POST['obj_negative_mark'],
							'QUES_DURATION' => $_POST['obj_duration'],
							'QUES_DIFFICULTY_LEVEL' => $ques_levele,
							'QUES_HINT' => $_POST['obj_ques_hint'],
							'QUES_EXPLANATION' => $_POST['obj_ques_explain'],
							'QUES_STATUS'=> 0,
							'QUES_CREATED_BY' => $this->session->userdata('adminloginid'),
							'QUES_APPROVED_BY' => 0,
							'QUES_CRATED_AT' => $_POST['obj_created_date'],
							'QUES_ENTRY_TT' => date('Y-m-d H:i:s'),
						);
					}
					$ques_id=$this->Adminpostmodel->addquestion($details);
					if(!empty($ques_id)){
					    
					    /* For Update Question Log */
    					$queslogaction = 1; // 1 For ques Create Log
    					$userid = $this->session->userdata('adminloginid');
    					$this->Adminpostmodel->update_ques_log($ques_id,$queslogaction,$userid);
					    
						$response['message']='Data Added Successfully';
						$response['data']='ok';
					}else{
						$response['message']='Something wents wrong';
						$response['data']='Notok';
					}
                
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
	
	public function questionedit()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    
                    
					$check=array();
					
				    $ques_levele = $_POST['ques_levele'];
				    $question = $_POST['question'];
				    $ques_type = $_POST['ques_type'];
				    $ques_id = $_POST['quesid'];
				    $chapterdt = $this->Admingetmodel->get_all_chapter_by_bookid($_POST['book']);
					if($ques_type==1){
						$details = array(
							'SUB_ID' => $_POST['subject'],
				            'CHAPTER_ID' => $chapterdt[0]->CHAPTER_ID,
							'BOOK_ID' => $_POST['book'],
							'QUES_TYPE' => $ques_type,
							'QUES' => $question,
							'QUES_ANSWERS' => $_POST['subj_answer'],
							'QUES_P_MARKING' => $_POST['subj_positive_mark'],
							'QUES_N_MARKING' => $_POST['subj_negative_mark'],
							'QUES_DURATION' => $_POST['subj_duration'],
							'QUES_DIFFICULTY_LEVEL' => $ques_levele,
							'QUES_HINT' => $_POST['subj_ques_hint'],
							'QUES_EXPLANATION' => $_POST['subj_ques_explain'],
				// 			'QUES_CREATED_BY' => $this->session->userdata('adminloginid'),
				// 			'QUES_APPROVED_BY' => 0,
				// 			'QUES_CRATED_AT' => $_POST['subj_created_date'],
							'QUES_ENTRY_TT' => date('Y-m-d H:i:s'),
						);
						
					}else{
					    $ques_option = $_POST['ques_option'];
					    $data1 = array(
    						'QUESTION' =>$question,
    						'OPTION' =>$ques_option
    					);
        				$ques = json_encode($data1);
						$details = array(
					        'SUB_ID' => $_POST['subject'],
					        'CHAPTER_ID' => $chapterdt[0]->CHAPTER_ID,
							'BOOK_ID' => $_POST['book'],		
							'QUES_TYPE' => $ques_type,
							'QUES' => $ques,
							'QUES_ANSWERS' => implode(',',$_POST['obj_answer']),
							'QUES_P_MARKING' => $_POST['obj_positive_mark'],
							'QUES_N_MARKING' => $_POST['obj_negative_mark'],
							'QUES_DURATION' => $_POST['obj_duration'],
							'QUES_DIFFICULTY_LEVEL' => $ques_levele,
							'QUES_HINT' => $_POST['obj_ques_hint'],
							'QUES_EXPLANATION' => $_POST['obj_ques_explain'],
				// 			'QUES_CREATED_BY' => $this->session->userdata('adminloginid'),
				// 			'QUES_CRATED_AT' => $_POST['obj_created_date'],
							'QUES_LAST_UPDATED' => date('Y-m-d H:i:s'),
						);
					}
					$quesid=$this->Adminpostmodel->updatequestion($details,$ques_id);
					if($quesid){
					    
					    /* For Update Question Log */
    					$queslogaction = 2; // 2 For ques Edit Log
    					$userid = $this->session->userdata('adminloginid');
    					$this->Adminpostmodel->update_ques_log($ques_id,$queslogaction,$userid);
					    
						$response['message']='Data Added Successfully';
						$response['data']='ok';
					}else{
						$response['message']='Something wents wrong';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
	public function approveques()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$quesid = $_POST['quesid'];
					$loginid = $_POST['loginid'];
					$data = array(
						'QUES_APPROVED_BY' =>$loginid,
						'QUES_STATUS' =>1
					);
					$this->Adminpostmodel->approveques($data,$quesid);
					
					/* For Update Question Log */
					$queslogaction = 3; // 3 For ques Approve Log
					$userid = $this->session->userdata('adminloginid');
					$this->Adminpostmodel->update_ques_log($quesid,$queslogaction,$userid);
					
					$response['message']='Data Edited Successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function testseriescreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					try{
						$data=$this->Admingetmodel->check_testseries_exist($_POST['testseriesname']);
					    if(empty($data)){
    						$details = array(
    							'TS_NAME'=> $_POST['testseriesname'],
    							'TS_COST'=> $_POST['cost'],
    							'TS_P_MARKS'=> $_POST['pmark'],
    							'TS_N_MARKS'=> $_POST['nmark'],
    							'TS_CREATED_AT'=> $_POST['createdate'],
    							'TS_START_DATE'=> $_POST['startdate'],
    							'TS_END_DATE'=> $_POST['enddate'],
    							'TS_DESCRIPTION'=> $_POST['desc'],
    							'TS_TEST_PAPER_NOS'=> $_POST['tspaperno'],
    							'TS_QUESTION_NOS'=> $_POST['quesno'],
    							'TS_DURATION'=> $_POST['duration'],
    							'TS_STATUS'=> 1,
    							'TS_CREATED_BY' => 1,
    							'TS_ENTRY_TT' => date('Y-m-d H:i:s')
							);
							if(!empty($_FILES['testseries_img']['name'])){
								$upload_loc='assets/product_images/test_series/';
								$config['upload_path']          = './'.$upload_loc;
								$config['allowed_types']        = 'gif|jpg|png|jpeg';
								$config['encrypt_name']=TRUE;
								$config['max_width']            = 430;
								$config['max_height']           = 240;
								
								//Load upload library and initialize configuration
								$this->load->library('upload',$config);
								$this->upload->initialize($config);
								
								if($this->upload->do_upload('testseries_img')){
									$uploadData = $this->upload->data();
									//$details['loc']=$uploadData;
									$details['TS_IMG'] = base_url($upload_loc.$uploadData['file_name']);
								}else{
									throw new Exception(strip_tags($this->upload->display_errors()));
								}
							}
    						$testseriesid=$this->Adminpostmodel->addtestseries($details);
    						if(!empty($testseriesid)){
    							$response['message']='Data Added Successfully';
    							$response['data']='ok';
    						}else{
    							$response['message']='Something wents wrong';
    							$response['data']='Notok';
    						}
					    }else{
					        $response['message']='Test Series Name already Exist';
    						$response['data']='Notok';
					    }
						json_output(200, $response);
					} catch (\Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (\Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}	 
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function updatetestseries()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					try{
						$tsname = $_POST['testseriesname'];
						$tsdt = $this->Admingetmodel->check_testseries_exist($tsname);
						if(sizeof($tsdt)<1 || (sizeof($tsdt)==1 && $tsdt[0]->TS_ID==$_POST['tsid'])){
							$data = array(
								'TS_NAME'=> $_POST['testseriesname'],
								'TS_COST'=> $_POST['cost'],
								'TS_P_MARKS'=> $_POST['pmark'],
								'TS_N_MARKS'=> $_POST['nmark'],
								'TS_START_DATE'=> $_POST['startdate'],
								'TS_END_DATE'=> $_POST['enddate'],
								'TS_DESCRIPTION'=> $_POST['desc'],
								'TS_TEST_PAPER_NOS'=> $_POST['tspaperno'],
								'TS_QUESTION_NOS'=> $_POST['quesno'],
								'TS_STATUS'=> $_POST['tsstatus'],
								'TS_DURATION'=> $_POST['duration'],
								'TS_LAST_UPDATED' => date('Y-m-d H:i:s')
							);
							if(!empty($_FILES['testseries_img']['name'])){
								$upload_loc='assets/product_images/test_series/';
								$config['upload_path']          = './'.$upload_loc;
								$config['allowed_types']        = 'gif|jpg|png|jpeg';
								$config['encrypt_name']=TRUE;
								$config['max_width']            = 430;
								$config['max_height']           = 240;
								
								//Load upload library and initialize configuration
								$this->load->library('upload',$config);
								$this->upload->initialize($config);
								
								if($this->upload->do_upload('testseries_img')){
									$uploadData = $this->upload->data();
									//$details['loc']=$uploadData;
									$data['TS_IMG'] = base_url($upload_loc.$uploadData['file_name']);
								}else{
									throw new Exception(strip_tags($this->upload->display_errors()));
								}
							}
							$this->Adminpostmodel->updatetestseries($data,$_POST['tsid']);
							$data = array(
								'TP_P_MARKS'=> $_POST['pmark'],
								'TP_N_MARKS'=> $_POST['nmark'],
							);
							$this->Adminpostmodel->updatetestpapersbytsid($data,$_POST['tsid']);
							$response['message']='Data Edited Successfully';
							$response['data']='ok';
						}else{
							$response['message']='Test Series Name already Exist';
							$response['data']='Notok';
						}
						json_output(200, $response);
					} catch (\Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (\Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function testpapercreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
						$details = array(
							'TP_NAME'=> $_POST['testpapername'],
							'TP_TYPE'=> $_POST['testpapertype'],
							'SUB_ID'=> $_POST['subject'],
							'TP_QUESTIONS'=> json_encode($_POST['questions']),
							'TP_P_MARKS'=> $_POST['p_mark'],
							'TP_N_MARKS'=> $_POST['n_mark'],
							'TP_START_DATE'=> $_POST['start_date'],
							'TP_END_DATE'=> $_POST['end_date'],
							'TP_CREATED_AT'=> $_POST['created_date'],
							'TP_DESCRIPTION'=> $_POST['desc'],
							'TP_DURATION'=> $_POST['duration'],
							'TP_STATUS'=> 1,
							'TP_CREATED_BY' => 1,
							'TP_ENTRY_TT' => date('Y-m-d H:i:s')
						);
						$testpaperid=$this->Adminpostmodel->addtestpaper($details);
						if(!empty($testpaperid)){
						    $testseriesid = $_POST['testseriesid'];
						    $tspaperarr1 = explode(',',$testpaperid);
						    $tsdt = $this->Admingetmodel->get_testseriesdt_by_id($testseriesid);
						    if($tsdt[0]->TS_TEST_PAPERS!=''){
						        $tspaperarr2 = json_decode($tsdt[0]->TS_TEST_PAPERS);
						        $mergetpidarr = array_merge($tspaperarr2,$tspaperarr1);
						        $encodedtpid= json_encode($mergetpidarr);
						        $data = array(
						            'TS_TEST_PAPERS'=> $encodedtpid,
						        );
						    }else{
						        $encodedtpid= json_encode($tspaperarr1);
						        $data = array(
						            'TS_TEST_PAPERS'=> $encodedtpid,
						        );
						    }
						    $updateid=$this->Adminpostmodel->updatetestseries($data,$testseriesid);
						    
						    if($tsdt[0]->TS_INSERTED_QUES!=''){
						        $tsquesarr = json_decode($tsdt[0]->TS_INSERTED_QUES);
						        $mergequesarr = array_merge($tsquesarr,$_POST['questions']);
						        $encodedquesid= json_encode($mergequesarr);
						        $data1 = array(
						            'TS_INSERTED_QUES'=> $encodedquesid,
						        );
						    }else{
						        $encodedquesid= json_encode($_POST['questions']);
						        $data1 = array(
						            'TS_INSERTED_QUES'=> $encodedquesid,
						        );
						    }
						    $testseriesids=$this->Adminpostmodel->updatetestseries($data1,$testseriesid);
						    
							$response['message']='Data Added Successfully';
							$response['data']='ok';
						}else{
							$response['message']='Something wents wrong';
							$response['data']='Notok';
						}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
	public function submit_test_paper()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $testseriesid = $_POST['testseriesid'];
					$tsdt = $this->Admingetmodel->get_testseriesdt_by_id($testseriesid);
                    $tpnameexist = $this->Admingetmodel->check_test_paper_name_exist($_POST['tpname'],$_POST['testseriesid']);

                    if(empty($tpnameexist)){
                        if($tsdt[0]->TS_QUESTION_NOS>=sizeof(explode(',',$_POST['quesid']))){
    					    $quesid = explode(',',$_POST['quesid']);
    						$details = array(
    							'TP_NAME'=> $_POST['tpname'],
    							'TS_ID'=> $_POST['testseriesid'],
    							'TP_QUESTIONS'=> json_encode($quesid),
    							'TP_P_MARKS'=> $tsdt[0]->TS_P_MARKS,
    							'TP_N_MARKS'=> $tsdt[0]->TS_N_MARKS,
    							'TP_START_DATE'=> $_POST['fstartdate'],
    							'TP_END_DATE'=> $_POST['fenddate'],
    							'TP_CREATED_AT'=> $_POST['fcreateddate'],
    							'TP_DESCRIPTION'=> $_POST['fdesc'],
    							'TP_PAY_STATUS'=> $_POST['tptype'],
    							'TP_STATUS'=> 0,
    							'TP_CREATED_BY' => 1,
    							'TP_ENTRY_TT' => date('Y-m-d H:i:s')
    						);
    						$testpaperid=$this->Adminpostmodel->addtestpaper($details);
    						if(!empty($testpaperid)){
    						    $testseriesid = $_POST['testseriesid'];
    						    $tspaperarr1 = explode(',',$testpaperid);
    						    $tsdt = $this->Admingetmodel->get_testseriesdt_by_id($testseriesid);
    						    if($tsdt[0]->TS_TEST_PAPERS!=''){
    						        $tspaperarr2 = json_decode($tsdt[0]->TS_TEST_PAPERS);
    						        $mergetpidarr = array_merge($tspaperarr2,$tspaperarr1);
    						        $encodedtpid= json_encode($mergetpidarr);
    						        $data = array(
    						            'TS_TEST_PAPERS'=> $encodedtpid,
    						        );
    						    }else{
    						        $encodedtpid= json_encode($tspaperarr1);
    						        $data = array(
    						            'TS_TEST_PAPERS'=> $encodedtpid,
    						        );
    						    }
    						    $updateid=$this->Adminpostmodel->updatetestseries($data,$testseriesid);
    						    
    						    if($tsdt[0]->TS_INSERTED_QUES!=''){
    						        $tsquesarr = json_decode($tsdt[0]->TS_INSERTED_QUES);
    						        $mergequesarr = array_merge($tsquesarr,$quesid);
    						        $encodedquesid= json_encode($mergequesarr);
    						        $data1 = array(
    						            'TS_INSERTED_QUES'=> $encodedquesid,
    						        );
    						    }else{
    						        $encodedquesid= json_encode($quesid);
    						        $data1 = array(
    						            'TS_INSERTED_QUES'=> $encodedquesid,
    						        );
    						    }
    						    $testseriesids = $this->Adminpostmodel->updatetestseries($data1,$testseriesid);
    						    
    						    $distinctsubid = explode(',',$this->Admingetmodel->get_distinct_subid_by_quesid($_POST['quesid']));
    						    $data2 = array(
    					            'SUB_ID'=> json_encode($distinctsubid)
    					        );
    					        $tpupdatedid = $this->Adminpostmodel->updatetestpaper($data2,$testpaperid);
    					        
    					        $this->Adminpostmodel->update_test_paper_status($testpaperid,$testseriesid);
    							$response['message']='Data Added Successfully';
    							$response['data']='ok';
    						}else{
    							$response['message']='Something wents wrong';
    							$response['data']='Notok';
    						}
                        }else{
                            $response['message']='Question Number Exceed!';
						    $response['data']='Notok';
                        }
                    }else{
                        $response['message']='Test Paper Name Already exist!';
						$response['data']='Notok';
                    }
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
	}
	
	public function submit_practice_test_paper()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $testseriesid = $_POST['testseriesid'];
					$tsdt = $this->Admingetmodel->get_practicetestdt_by_id($testseriesid);
                    $tpnameexist = $this->Admingetmodel->check_practice_test_paper_name_exist($_POST['tpname'],$_POST['testseriesid']);

                    if(empty($tpnameexist)){
                        if($tsdt[0]->TS_QUESTION_NOS>=sizeof(explode(',',$_POST['quesid']))){
    					    $quesid = explode(',',$_POST['quesid']);
    						$details = array(
    							'TP_NAME'=> $_POST['tpname'],
    							'TS_ID'=> $_POST['testseriesid'],
    							'TP_QUESTIONS'=> json_encode($quesid),
    							'TP_P_MARKS'=> $tsdt[0]->TS_P_MARKS,
    							'TP_N_MARKS'=> $tsdt[0]->TS_N_MARKS,
    							'TP_START_DATE'=> $_POST['fstartdate'],
    							'TP_END_DATE'=> $_POST['fenddate'],
    							'TP_CREATED_AT'=> $_POST['fcreateddate'],
    							'TP_DESCRIPTION'=> $_POST['fdesc'],
    							'TP_PAY_STATUS'=> $_POST['tptype'],
    							'TP_STATUS'=> 0,
    							'TP_CREATED_BY' => 1,
    							'TP_ENTRY_TT' => date('Y-m-d H:i:s')
    						);
    						$testpaperid=$this->Adminpostmodel->addpracticetestpaper($details);
    						if(!empty($testpaperid)){
    						    $testseriesid = $_POST['testseriesid'];
    						    $tspaperarr1 = explode(',',$testpaperid);
    						    $tsdt = $this->Admingetmodel->get_practicetestdt_by_id($testseriesid);
    						    if($tsdt[0]->TS_TEST_PAPERS!=''){
    						        $tspaperarr2 = json_decode($tsdt[0]->TS_TEST_PAPERS);
    						        $mergetpidarr = array_merge($tspaperarr2,$tspaperarr1);
    						        $encodedtpid= json_encode($mergetpidarr);
    						        $data = array(
    						            'TS_TEST_PAPERS'=> $encodedtpid,
    						        );
    						    }else{
    						        $encodedtpid= json_encode($tspaperarr1);
    						        $data = array(
    						            'TS_TEST_PAPERS'=> $encodedtpid,
    						        );
    						    }
    						    $updateid=$this->Adminpostmodel->updatepracticetest($data,$testseriesid);
    						    
    						    if($tsdt[0]->TS_INSERTED_QUES!=''){
    						        $tsquesarr = json_decode($tsdt[0]->TS_INSERTED_QUES);
    						        $mergequesarr = array_merge($tsquesarr,$quesid);
    						        $encodedquesid= json_encode($mergequesarr);
    						        $data1 = array(
    						            'TS_INSERTED_QUES'=> $encodedquesid,
    						        );
    						    }else{
    						        $encodedquesid= json_encode($quesid);
    						        $data1 = array(
    						            'TS_INSERTED_QUES'=> $encodedquesid,
    						        );
    						    }
    						    $testseriesids = $this->Adminpostmodel->updatepracticetest($data1,$testseriesid);
    						    
    						    $distinctsubid = explode(',',$this->Admingetmodel->get_distinct_subid_by_quesid($_POST['quesid']));
    						    $data2 = array(
    					            'SUB_ID'=> json_encode($distinctsubid)
    					        );
    					        $tpupdatedid = $this->Adminpostmodel->updatepracticetestpaper($data2,$testpaperid);
    					        
    					        $this->Adminpostmodel->update_practice_test_paper_status($testpaperid,$testseriesid);
    							$response['message']='Data Added Successfully';
    							$response['data']='ok';
    						}else{
    							$response['message']='Something wents wrong';
    							$response['data']='Notok';
    						}
                        }else{
                            $response['message']='Question Number Exceed!';
						    $response['data']='Notok';
                        }
                    }else{
                        $response['message']='Test Paper Name Already exist!';
						$response['data']='Notok';
                    }
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }

    public function update_test_paper()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    
                    $testseriesid = $_POST['testseriesid'];
					$tsdt = $this->Admingetmodel->get_testseriesdt_by_id($testseriesid);
                    $tpnameexist = $this->Admingetmodel->check_test_paper_name_exist($_POST['tpname'],$_POST['testseriesid']);
                    
                    
                    
                    if(sizeof($tpnameexist)<1 || (sizeof($tpnameexist)==1 && $_POST['tpid']==$tpnameexist[0]->TP_ID)){
                        if($tsdt[0]->TS_QUESTION_NOS>=sizeof(explode(',',$_POST['quesid']))){
    					    $quesid = explode(',',$_POST['quesid']);
    					    $distinctsubid = explode(',',$this->Admingetmodel->get_distinct_subid_by_quesid($_POST['quesid']));
    						$details = array(
    						    'SUB_ID'=> json_encode($distinctsubid),
    							'TP_NAME'=> $_POST['tpname'],
    							'TP_QUESTIONS'=> json_encode($quesid),
    							'TP_START_DATE'=> $_POST['fstartdate'],
    							'TP_END_DATE'=> $_POST['fenddate'],
    							'TP_DESCRIPTION'=> $_POST['fdesc'],
    							'TP_LAST_UPDATED' => date('Y-m-d H:i:s')
    						);
    						$testpaperid=$this->Adminpostmodel->updatetestpaper($details,$_POST['tpid']);
    				        
    						if(!empty($testpaperid)){
    						    $testseriesid = $_POST['testseriesid'];
    						    $tsdt = $this->Admingetmodel->get_testseriesdt_by_id($testseriesid);
    
    						    if($tsdt[0]->TS_INSERTED_QUES!=''){
    						        $tsquesarr = json_decode($tsdt[0]->TS_INSERTED_QUES);
    						        $mergequesarr = array_unique(array_merge($tsquesarr,$quesid));
    						        $encodedquesid= json_encode(array_values($mergequesarr));
    						        $data1 = array(
    						            'TS_INSERTED_QUES'=> $encodedquesid,
    						            'TS_LAST_UPDATED'=> date('Y-m-d H:i:s')
    						        );
    						    }
    						    $testseriesids = $this->Adminpostmodel->updatetestseries($data1,$testseriesid);
    						    
    						    $this->Adminpostmodel->update_test_paper_status($_POST['tpid'],$_POST['testseriesid']);
    						    
    							$response['message']='Data Added Successfully';
    							$response['data']='ok';
    						}else{
    							$response['message']='Something wents wrong';
    							$response['data']='Notok';
    						}
                        }else{
                            $response['message']='Question Number Exceed!';
    						$response['data']='Notok';
                        }
                    }else{
                        $response['message']='Test Paper Name Already exist!';
						$response['data']='Notok';
                    }
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
	}
	public function update_practice_test_paper()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    
                    $testseriesid = $_POST['testseriesid'];
					$tsdt = $this->Admingetmodel->get_practicetestdt_by_id($testseriesid);
                    $tpnameexist = $this->Admingetmodel->check_practice_test_paper_name_exist($_POST['tpname'],$_POST['testseriesid']);
                    
                    
                    
                    if(sizeof($tpnameexist)<1 || (sizeof($tpnameexist)==1 && $_POST['tpid']==$tpnameexist[0]->TP_ID)){
                        if($tsdt[0]->TS_QUESTION_NOS>=sizeof(explode(',',$_POST['quesid']))){
    					    $quesid = explode(',',$_POST['quesid']);
    					    $distinctsubid = explode(',',$this->Admingetmodel->get_distinct_subid_by_quesid($_POST['quesid']));
    						$details = array(
    						    'SUB_ID'=> json_encode($distinctsubid),
    							'TP_NAME'=> $_POST['tpname'],
    							'TP_QUESTIONS'=> json_encode($quesid),
    							'TP_START_DATE'=> $_POST['fstartdate'],
    							'TP_END_DATE'=> $_POST['fenddate'],
    							'TP_DESCRIPTION'=> $_POST['fdesc'],
    							'TP_LAST_UPDATED' => date('Y-m-d H:i:s')
    						);
    						$testpaperid=$this->Adminpostmodel->updatepracticetestpaper($details,$_POST['tpid']);
    				        
    						if(!empty($testpaperid)){
    						    $testseriesid = $_POST['testseriesid'];
    						    $tsdt = $this->Admingetmodel->get_practicetestdt_by_id($testseriesid);
    
    						    if($tsdt[0]->TS_INSERTED_QUES!=''){
    						        $tsquesarr = json_decode($tsdt[0]->TS_INSERTED_QUES);
    						        $mergequesarr = array_unique(array_merge($tsquesarr,$quesid));
    						        $encodedquesid= json_encode(array_values($mergequesarr));
    						        $data1 = array(
    						            'TS_INSERTED_QUES'=> $encodedquesid,
    						            'TS_LAST_UPDATED'=> date('Y-m-d H:i:s')
    						        );
    						    }
    						    $testseriesids = $this->Adminpostmodel->updatepracticetest($data1,$testseriesid);
    						    
    						    $this->Adminpostmodel->update_practice_test_paper_status($_POST['tpid'],$_POST['testseriesid']);
    						    
    							$response['message']='Data Added Successfully';
    							$response['data']='ok';
    						}else{
    							$response['message']='Something wents wrong';
    							$response['data']='Notok';
    						}
                        }else{
                            $response['message']='Question Number Exceed!';
    						$response['data']='Notok';
                        }
                    }else{
                        $response['message']='Test Paper Name Already exist!';
						$response['data']='Notok';
                    }
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function deletequestion()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$quesid=$_POST['quesid'];
					$details = array(
							'QUES_STATUS' => 2,
							'QUES_LAST_UPDATED' => date('Y-m-d H:i:s'),
						);
					$ques_id=$this->Adminpostmodel->updatequestion($details,$quesid);
					
					/* For Update Question Log */
					$queslogaction = 4; // 4 For ques delete Log
					$userid = $this->session->userdata('adminloginid');
					$this->Adminpostmodel->update_ques_log($quesid,$queslogaction,$userid);
					
					$response['message']='Question Deleted successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function publishtestpaper()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$tpid = $_POST['tpid'];
					$tppublish = $_POST['tppublish'];
					if($tppublish==1){
					    $data = array(
    						'TP_APPROVED_BY' =>1,
    						'TP_LAST_UPDATED' => date('Y-m-d H:i:s')
    					);
					}else{
					    $data = array(
        					'TP_APPROVED_BY' =>'',
        					'TP_LAST_UPDATED' => date('Y-m-d H:i:s')
        				);
					}
					
					$this->Adminpostmodel->updatetestpaper($data,$tpid);
					$response['message']='Test Paper Pulished Successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }

    public function membercreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$check=array();
				// 	$adminid=$_POST['adminid'];
					$data=$this->Admingetmodel->get_member_by_email(strtoupper($_POST['memail']));
				
					if(empty($data)){
						$details = array(
							'USER_FNAME'=>$_POST['mfname'],
							'USER_LNAME'=>$_POST['mlname'],
							'USER_ADDRESS'=>$_POST['maddress'],
							'USER_CONTACT'=>$_POST['mcontact'],
							'USER_EMAIL'=>$_POST['memail'],
							'USER_PASSWORD'=> md5($_POST['mpass']),
							'USER_QUALIFICATION'=>$_POST['mqualification'],
							'USER_FATHER'=>$_POST['mfathername'],
							'USER_ROLES'=>$_POST['mtype'],
							'USER_ENTRY_TT'=>date('Y-m-d H:i:s'),
							'USER_STATUS'=>1
						);
						$areaid=$this->Adminpostmodel->addmember($details);
						
						if(!empty($areaid)){
							$response['message']='Data Added Successfully';
							$response['data']='ok';
						}else{
							$response['message']='Something wents wrong';
							$response['data']='Notok';
						}
					}else{
						$response['message']='Email already Exist';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function editmemberform()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$memberid=$_POST['memberid'];
					$details = array(
						'USER_FNAME'=>$_POST['mfname'],
						'USER_LNAME'=>$_POST['mlname'],
						'USER_ADDRESS'=>$_POST['maddress'],
						'USER_CONTACT'=>$_POST['mcontact'],
						'USER_QUALIFICATION'=>$_POST['mqualification'],
						'USER_FATHER'=>$_POST['mfathername'],
						'USER_ROLES'=>$_POST['mtype'],
						'USER_STATUS'=>$_POST['mstatus']
					);
					$this->Adminpostmodel->updatemember($details,$memberid);
					
					$response['message']='Data Edited Successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function holdques()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$quesid = $_POST['quesid'];
					$loginid = $_POST['loginid'];
					$data = array(
						'QUES_STATUS' =>3
					);
					$this->Adminpostmodel->updatequestion($data,$quesid);
					
					/* For Update Question Log */
					$queslogaction = 5; // 5 For ques Hold Log
					$userid = $this->session->userdata('adminloginid');
					$this->Adminpostmodel->update_ques_log($quesid,$queslogaction,$userid);
					
					$response['message']='Data Edited Successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
     public function savetestpaperdetails()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    
                    $testseriesid = $_POST['tsid'];
					$tsdt = $this->Admingetmodel->get_testseriesdt_by_id($testseriesid);
                    $tpnameexist = $this->Admingetmodel->check_test_paper_name_exist($_POST['tpname'],$_POST['tsid']);
                    
                    if(sizeof($tpnameexist)<1 || (sizeof($tpnameexist)==1 && $_POST['tpid']==$tpnameexist[0]->TP_ID)){
    						$details = array(
    							'TP_NAME'=> $_POST['tpname'],
    							'TP_START_DATE'=> $_POST['fstartdate'],
    							'TP_END_DATE'=> $_POST['fenddate'],
    							'TP_PAY_STATUS'=> $_POST['tptype'],
    							'TP_DESCRIPTION'=> $_POST['fdesc'],
    							'TP_LAST_UPDATED' => date('Y-m-d H:i:s')
    						);
    						$testpaperid=$this->Adminpostmodel->updatetestpaper($details,$_POST['tpid']);
    						if(!empty($testpaperid)){
    							$response['message']='Data Added Successfully';
    							$response['data']='ok';
    						}else{
    							$response['message']='Something wents wrong';
    							$response['data']='Notok';
    						}
                    }else{
                        $response['message']='Test Paper Name Already exist!';
						$response['data']='Notok';
                    }
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function couponcreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$check=array();
				// 	$adminid=$_POST['adminid'];
					$data=$this->Admingetmodel->get_coupon_by_title(strtoupper($_POST['cpntitle']));
					if(empty($data)){
						$cpnprod=0;
						if($this->input->post('cpnproduct')){
							$cpnprod = implode(',',$_POST['cpnproduct']);
						}
						$details = array(
							'COUPON_TITLE'=>strtoupper($_POST['cpntitle']),
							'COUPON_DESC'=>$_POST['cpn_desc'],
							'COUPON_MODE'=>$_POST['cpnmode'],
							'COUPON_TYPE' =>$_POST['coupon_type'],
							'COUPON_AMOUNT'=>$_POST['cpnamt'],
							'COUPON_START_DATE'=>$_POST['startdate'],
							'COUPON_END_DATE'=> $_POST['enddate'],
							'PROD_TYPE_ID'=> $_POST['prod_type'],
							'PRODUCT'=>$cpnprod,
							'COUPON_USAGE_LIMIT'=>$_POST['cpn_usage_limit'],
							'COUPON_LIMIT_PER_USER'=>$_POST['cpn_usage_limit_peruser'],
							'COUPON_CREATED_DATE'=>date('Y-m-d H:i:s'),
						);
						$areaid=$this->Adminpostmodel->addcoupon($details);
						if(!empty($areaid)){
							$response['message']='Data Added Successfully';
							$response['data']='ok';
						}else{
							$response['message']='Something wents wrong';
							$response['data']='Notok';
						}
					}else{
						$response['message']='Coupon already Exist';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
	
	public function couponedit()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$check=array();
				// 	$adminid=$_POST['adminid'];
					$data=$this->Admingetmodel->get_coupon_title_exist(strtoupper($_POST['cpntitle']),$_POST['couponid']);
					if(empty($data)){
					    $cpnprod=0;
						if($this->input->post('cpnproduct')){
							$cpnprod = implode(',',$_POST['cpnproduct']);
						}
						$details = array(
							'COUPON_TITLE'=>strtoupper($_POST['cpntitle']),
							'COUPON_DESC'=>$_POST['cpn_desc'],
							'COUPON_TYPE'=>$_POST['coupon_type'],
							'COUPON_MODE'=>$_POST['cpnmode'],
							'COUPON_AMOUNT'=>$_POST['cpnamt'],
							'COUPON_START_DATE'=>$_POST['startdate'],
							'COUPON_END_DATE'=> $_POST['enddate'],
							'PROD_TYPE_ID'=>$_POST['prod_type'],
							'PRODUCT'=>$cpnprod,
							'COUPON_USAGE_LIMIT'=>$_POST['cpn_usage_limit'],
							'COUPON_LIMIT_PER_USER'=>$_POST['cpn_usage_limit_peruser'],
							'COUPON_LAST_UPDATED_DATE'=>date('Y-m-d H:i:s'),
							'COUPON_STATUS'=>$_POST['cpn_status'],
						);
						$data=$this->Adminpostmodel->updatecoupon($details,$_POST['couponid']);
						if($data){
							$response['message']='Coupon Updated Successfully';
							$response['data']='ok';
						}else{
							$response['message']='Something wents wrong';
							$response['data']='Notok';
						}
					}else{
						$response['message']='Coupon already Exist';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function nfs_ques()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$quesid = $_POST['quesid'];
					$loginid = $_POST['loginid'];
					$nfsact = $_POST['nfsact'];
					$data = array(
						'QUES_STATUS' =>$nfsact
					);
					$this->Adminpostmodel->updatequestion($data,$quesid);
					
					/* For Update Question Log */
					$queslogaction = 6; // 6 For ques add NFS Log
					$userid = $this->session->userdata('adminloginid');
					$this->Adminpostmodel->update_ques_log($quesid,$queslogaction,$userid);
					
					$response['message']='Data Edited Successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function addtemplate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$check=array();
					
					$details = array(
						'MSGTEMP_MSG'=>$_POST['temp'],
						'MSGTEMP_TITLE'=>$_POST['temptitle'],
						'MSGTEMP_SEND_TYPE'=>$_POST['sending_type'], //for message
						'MSGTEMP_STATUS'=>1
					);
					$templateid=$this->Adminpostmodel->addtemplate($details);
					if(!empty($templateid)){
						$response['message']='Data Added Successfully';
						$response['data']='ok';
					}else{
						$response['message']='Something wents wrong';
						$response['data']='Notok';
					}
				
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function sendmessage()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$useridarr = $_POST['users'];
					$sendmessagetemp = $_POST['sendmessagetemp'];
					foreach($useridarr as $userids){
					    $userdt = $this->Admingetmodel->get_admin_dt_model($userids)['data'][0];
					    $username = $userdt->USER_FNAME." ".$userdt->USER_LNAME;
					    $mobileno = $userdt->USER_CONTACT;
					    $jbid = send_sms($mobileno,$sendmessagetemp);
					    insert_msg_activity($jbid,$userids,$_POST['sendmsgtemplate'],1);
					}
					
					$response['message']='Message sended Successfully JB_ID='.$jbid;
					$response['data']='ok';
					
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function setscheduling()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $users = implode(',',$_POST['users']);
					$details = array(
						'SCH_TYPE'=>$_POST['scheduletype'],
						'MSGTEMP_ID'=>$_POST['sendmsgtemplate'],
						'USER_ID'=>$users,
						'SCH_DATE'=>$_POST['datetime'],
						'SCH_SEND_TYPE'=>$_POST['sending_type'],  //for message
						'SCH_PAY_TYPE'=>$_POST['stdtype'],  //for PAY TYPE
						'SCH_TS_ID'=>$_POST['testseries'],  
						'SCH_STATUS'=>1
					);
					$templateid=$this->Adminpostmodel->setscheduling($details);
					if(!empty($templateid)){
						$response['message']='Data Added Successfully';
						$response['data']='ok';
					}else{
						$response['message']='Something wents wrong';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function updatetemplate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$check=array();
					
					$details = array(
						'MSGTEMP_MSG'=>$_POST['temp'],
						'MSGTEMP_TITLE'=>$_POST['temptitle'],
						'MSGTEMP_STATUS'=>1
					);
					$templateid=$this->Adminpostmodel->updatetemplate($details,$_POST['tempid']);
					if(!empty($templateid)){
						$response['message']='Data Updates Successfully';
						$response['data']='ok';
					}else{
						$response['message']='Something wents wrong';
						$response['data']='Notok';
					}
				
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    public function sendmail()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$useridarr = $_POST['users'];
					$tempid = $_POST['sendmsgtemplate'];
					
					$tempdt = $this->Admingetmodel->get_templatedt_by_tempid($tempid);
					$message = $_POST['sendmessagetemp'];
					$subject = $tempdt[0]->MSGTEMP_TITLE;
					$from="support@pcskaka.com";
					foreach($useridarr as $userids){
					    $userdt = $this->Admingetmodel->get_admin_dt_model($userids)['data'][0];
					    $username = $userdt->USER_FNAME." ".$userdt->USER_LNAME;
					    $to = $userdt->USER_EMAIL;
					    $status = send_mail($from,$to,$subject,$message);
					    insert_msg_activity($status,$userids,$_POST['sendmsgtemplate'],2);
					}
					$response['message']='Message sended Successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    
    public function update_scheduling()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $id = $_POST['schid'];
                    $users = implode(',',$_POST['users']);
					$details = array(
						'SCH_TYPE'=>$_POST['scheduletype'],
						'MSGTEMP_ID'=>$_POST['sendmsgtemplate'],
						'USER_ID'=>$users,
						'SCH_DATE'=>$_POST['datetime'],
						'SCH_SEND_TYPE'=>$_POST['sending_type'],  //for message
						'SCH_PAY_TYPE'=>$_POST['stdtype'],  //for PAY TYPE
						'SCH_TS_ID'=>$_POST['testseries'],  
						'SCH_STATUS'=>$_POST['status']
					);
					$templateid=$this->Adminpostmodel->update_scheduling($details,$id);
					if(!empty($templateid)){
						$response['message']='Data Added Successfully';
						$response['data']='ok';
					}else{
						$response['message']='Something wents wrong';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
	}
	//Course Create
	public function coursecreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    try {
                        $check=array();
                        // 	$adminid=$_POST['adminid'];
                        $data=$this->Admingetmodel->get_course_by_name(strtoupper($_POST['coursename']));
                        if (empty($data['data'])) {
                            $details = array(
                                'COURSE_NAME'=>$this->input->post('coursename'),
								'COURSE_DESC'=>$_POST['coursedesc'],
								'COURSE_POPULAR'=>$_POST['popular'],
								'CC_ID'=>$_POST['coursecategory'],
                                'COURSE_STATUS '=>1,
                                'COURSE_ENTRY_TT'=>date('Y-m-d H:i:s'),
                            );
                            if(!empty($_FILES['course_img']['name'])){
								$upload_loc='assets/product_images/courses/';
								$config['upload_path']          = './'.$upload_loc;
								$config['allowed_types']        = 'gif|jpg|jpeg';
								$config['encrypt_name']=TRUE;
								$config['max_width']            = 430;
								$config['max_height']           = 240;
								
								//Load upload library and initialize configuration
								$this->load->library('upload',$config);
								$this->upload->initialize($config);
								
								if($this->upload->do_upload('course_img')){
									$uploadData = $this->upload->data();
									//$details['loc']=$uploadData;
									$details['COURSE_IMG'] = $upload_loc.$uploadData['file_name'];
								}else{
									throw new Exception(strip_tags($this->upload->display_errors()));
								}
							}
                            $areaid=$this->Adminpostmodel->addcourse($details);
                            
                            if (!empty($areaid)) {
                                $response['message']='Data Added Successfully';
                                $response['data']='ok';
                            } else {
                                $response['message']='Something wents wrong.';
                                $response['data']='Notok';
                            }
                        } else {
                            $response['message']='Course is already present';
                            $response['data']='Notok';
                        }
                        json_output(200, $response);
                    } catch (\Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (\Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}	
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
	}
	//Edit Course
	public function editcourse()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					try {
						$courseid=$_POST['courseid'];
						$data = array(
							'COURSE_NAME' =>$this->input->post('coursename'),
							'COURSE_STATUS' =>strtoupper($_POST['coursestatus']),
							'COURSE_DESC' =>strtoupper($_POST['coursedesc']),
							'COURSE_POPULAR'=>$_POST['popular'],
							'CC_ID'=>$_POST['coursecategory'],
						);
						if(!empty($_FILES['course_img']['name'])){
							$upload_loc='assets/product_images/courses/';
							$config['upload_path']          = './'.$upload_loc;
							$config['allowed_types']        = 'gif|jpg|jpeg';
							$config['encrypt_name']=TRUE;
							$config['max_width']            = 430;
							$config['max_height']           = 240;
							
							//Load upload library and initialize configuration
							$this->load->library('upload',$config);
							$this->upload->initialize($config);
							
							if($this->upload->do_upload('course_img')){
								$uploadData = $this->upload->data();
								//$details['loc']=$uploadData;
								$data['COURSE_IMG'] = $upload_loc.$uploadData['file_name'];
							}else{
								throw new Exception(strip_tags($this->upload->display_errors()));
							}
						}
                        if ($this->Adminpostmodel->updatecourse($data, $courseid)) {
                            $response['message']='Data Edited Successfully';
                            $response['data']='ok';
                            json_output(200, $response);
                        }else{
							throw new Exception();
						}
					} catch (Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (\Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}
					
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }


    public function course_content()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $courseid=$_POST['courseid'];
                    $course_content = array();
                    $cours_content_amt = 0;
                    $type_id_arr = explode(",",$_POST["coursecontent"]);
                    foreach($type_id_arr as $type_id){
                        list($type,$id) = explode("_", $type_id);
                        $course_content[$type][] = $id;
                        switch ($type) {
                            /*case 0:
                                 $course_response = $this->Admingetmodel->get_course_by_id($id);
                                 $course_row = $course_response["data"][0];
                                 $cours_content_amt += $course_row->COURSE_AMT;
                                break;*/

                            case 1:
                                $library_response = $this->Admingetmodel->get_library_by_libid($id);
                                $library_row = $library_response[0];
                                $cours_content_amt += $library_row->LIB_CONTENT_AMT;
                                break;

                            case 2:
                                $subject_response = $this->Admingetmodel->get_subject_by_subid($id);
                                $subject_row = $subject_response["data"][0];
                                $cours_content_amt += $subject_row->SUB_CONTENT_AMT;
                                break;

                            case 3:
                                $book_response = $this->Admingetmodel->get_booksdt_by_id($id);
                                $book_row = $book_response["data"][0];
                                $cours_content_amt += $book_row->BOOK_CONTENT_AMT;
                                break;
                            
                            default:
                                # code...
                                break;
                        }
                    }

                    $data = array(
                        'COURSE_AMT' =>$this->input->post('courseamt'),
                        'COURSE_CONTENT_DISC' =>$_POST['coursedisc'],
                        'COURSE_CONTENT' =>json_encode($course_content),
                        'COURSE_CONTENT_AMT' => $cours_content_amt
                    );
                    $this->Adminpostmodel->updatecourse($data,$courseid);
                    $response['message']='Data Added Successfully';
                    $response['data']='ok';
                    json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
	}
	
	//Course Course
	public function slidercreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    try {
						$details = array(
							'SLIDER_NAME'=>$this->input->post('slidername'),
							'PROD_TYPE'=>$_POST['prodtype'],
							'SLIDER_STATUS  '=>0,
							'SLIDER_TT'=>date('Y-m-d H:i:s'),
						);

						if(!empty($_FILES['slider_img']['name'])){
							$upload_loc='assets/product_images/slider/';
							$config['upload_path']          = './'.$upload_loc;
							$config['allowed_types']        = 'gif|jpg|png|jpeg';
							$config['encrypt_name']=TRUE;
							$config['max_width']            = 2560;
							$config['max_height']           = 1600;
							
							//Load upload library and initialize configuration
							$this->load->library('upload',$config);
							$this->upload->initialize($config);
							
							if($this->upload->do_upload('slider_img')){
								$uploadData = $this->upload->data();
								//$details['loc']=$uploadData;
								$details['SLIDER_IMG'] = $upload_loc.$uploadData['file_name'];
							}else{
								throw new Exception(strip_tags($this->upload->display_errors()));
							}
						}
						$areaid=$this->Adminpostmodel->addslider($details);
						
						if (!empty($areaid)) {
							$response['message']='Data Added Successfully';
							$response['data']='ok';
						} else {
							$response['message']='Something wents wrong.';
							$response['data']='Notok';
						}
                        json_output(200, $response);
                    } catch (\Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (\Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}	
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
	}

	//Edit Course
	public function editslider()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					try {
						$sliderid=$_POST['sliderid'];
						$data = array(
							'SLIDER_NAME'=>$this->input->post('slidername'),
							'PROD_TYPE'=>$_POST['prodtype'],
							'SLIDER_STATUS  '=>$_POST['status'],
							'SLIDER_TT'=>date('Y-m-d H:i:s'),
						);
						if(!empty($_FILES['slider_img']['name'])){
							$upload_loc='assets/product_images/slider/';
							$config['upload_path']          = './'.$upload_loc;
							$config['allowed_types']        = 'gif|jpg|png|jpeg';
							$config['encrypt_name']=TRUE;
							$config['max_width']            = 2560;
							$config['max_height']           = 1600;
							
							//Load upload library and initialize configuration
							$this->load->library('upload',$config);
							$this->upload->initialize($config);
							
							if($this->upload->do_upload('slider_img')){
								$uploadData = $this->upload->data();
								//$details['loc']=$uploadData;
								$data['SLIDER_IMG'] = $upload_loc.$uploadData['file_name'];
							}else{
								throw new Exception(strip_tags($this->upload->display_errors()));
							}
						}
						$this->Adminpostmodel->updateslider($data,$sliderid);
						$response['message']='Data Edited Successfully';
						$response['data']='ok';
						json_output(200, $response);
					} catch (\Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (\Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}
					
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }

    /*Shubham 2020-06-13*/
    public function updatepracticetest()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					try{
						$tsname = $_POST['testseriesname'];
						$tsdt = $this->Admingetmodel->check_practicetest_exist($tsname);
						
						if(sizeof($tsdt)<1 || (sizeof($tsdt)==1 && $tsdt[0]->TS_ID==$_POST['tsid'])){
							$data = array(
								/*'SUBJECT_ID' => $_POST['subject'],
								'LIBRARY_ID' => $_POST['library'],
								'COURSE_ID' => $_POST['course'],
								'BOOK_ID' => $_POST['book'],*/
								'TS_NAME'=> $_POST['testseriesname'],
								/*'TS_COST'=> $_POST['cost'],
								'TS_P_MARKS'=> $_POST['pmark'],
								'TS_N_MARKS'=> $_POST['nmark'],
								'TS_START_DATE'=> $_POST['startdate'],
								'TS_END_DATE'=> $_POST['enddate'],*/
								'TS_DESCRIPTION'=> $_POST['desc'],
								/*'TS_TEST_PAPER_NOS'=> $_POST['tspaperno'],
								'TS_QUESTION_NOS'=> $_POST['quesno'],*/
								'TS_STATUS'=> $_POST['tsstatus'],
								'TS_DURATION'=> $_POST['duration'],
								'TS_LAST_UPDATED' => date('Y-m-d H:i:s')
							);
							if(!empty($_FILES['practicetest_img']['name'])){
								$upload_loc='assets/product_images/practice_test/';
								$config['upload_path']          = './'.$upload_loc;
								$config['allowed_types']        = 'gif|jpg|png|jpeg';
								$config['encrypt_name']=TRUE;
								$config['max_width']            = 430;
								$config['max_height']           = 240;
								
								//Load upload library and initialize configuration
								$this->load->library('upload',$config);
								$this->upload->initialize($config);
								
								if($this->upload->do_upload('practicetest_img')){
									$uploadData = $this->upload->data();
									//$details['loc']=$uploadData;
									$data['TS_IMG'] = base_url($upload_loc.$uploadData['file_name']);
								}else{
									throw new Exception(strip_tags($this->upload->display_errors()));
								}
							}
							if(!empty($_FILES['practicetestbanner_img']['name'])){
								$upload_loc='assets/product_images/practice_test/';
								$config['upload_path']          = './'.$upload_loc;
								$config['allowed_types']        = 'gif|jpg|png|jpeg';
								$config['encrypt_name']=TRUE;
								$config['max_width']            = 2560;
								$config['max_height']           = 500;
								
								//Load upload library and initialize configuration
								$this->load->library('upload',$config);
								$this->upload->initialize($config);
								
								if($this->upload->do_upload('practicetestbanner_img')){
									$uploadData = $this->upload->data();
									//$details['loc']=$uploadData;
									$data['BANNER_IMG'] = base_url($upload_loc.$uploadData['file_name']);
								}else{
									throw new Exception(strip_tags($this->upload->display_errors()));
								}
							}
							$this->Adminpostmodel->updatepracticetest($data,$_POST['tsid']);
							$data = array(
								/*'TP_P_MARKS'=> $_POST['pmark'],
								'TP_N_MARKS'=> $_POST['nmark'],*/
								'TP_DURATION'=> $_POST['duration']
							);
							$this->Adminpostmodel->update_practicetest_papersby_ptid($data,$_POST['tsid']);
							$response['message']='Data Edited Successfully';
							$response['data']='ok';
						}else{
							$response['message']='Test Set Name already Exist';
							$response['data']='Notok';
						}
						json_output(200, $response);
					} catch (\Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (\Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }

    public function practicetestcreate()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					try{
						$data=$this->Admingetmodel->check_practicetest_exist($_POST['testseriesname']);
					    if(empty($data)){
    						$details = array(
								'SUBJECT_ID' => $_POST['subject'],
								'LIBRARY_ID' => $_POST['library'],
								/*'COURSE_ID' => $_POST['course'],*/
								'BOOK_ID' => $_POST['book'],
								'TS_NAME'=> $_POST['testseriesname'],
    							'TS_COST'=> $_POST['cost'],
    							'TS_P_MARKS'=> $_POST['pmark'],
    							'TS_N_MARKS'=> $_POST['nmark'],
    							'TS_CREATED_AT'=> $_POST['createdate'],
    							'TS_START_DATE'=> $_POST['startdate'],
    							'TS_END_DATE'=> $_POST['enddate'],
    							'TS_DESCRIPTION'=> $_POST['desc'],
    							'TS_TEST_PAPER_NOS'=> $_POST['tspaperno'],
    							'TS_QUESTION_NOS'=> $_POST['quesno'],
    							'TS_DURATION'=> $_POST['duration'],
    							'TS_STATUS'=> 1,
    							'TS_CREATED_BY' => 1,
    							'TS_ENTRY_TT' => date('Y-m-d H:i:s')
							);
							if(!empty($_FILES['practicetest_img']['name'])){
								$upload_loc='assets/product_images/practice_test/';
								$config['upload_path']          = './'.$upload_loc;
								$config['allowed_types']        = 'gif|jpg|png|jpeg';
								$config['encrypt_name']=TRUE;
								$config['max_width']            = 430;
								$config['max_height']           = 240;
								
								//Load upload library and initialize configuration
								$this->load->library('upload',$config);
								$this->upload->initialize($config);
								
								if($this->upload->do_upload('practicetest_img')){
									$uploadData = $this->upload->data();
									//$details['loc']=$uploadData;
									$details['TS_IMG'] = $upload_loc.$uploadData['file_name'];
								}else{
									throw new Exception(strip_tags($this->upload->display_errors()));
								}
							}
							if(!empty($_FILES['practicetestbanner_img']['name'])){
								$upload_loc='assets/product_images/practice_test/';
								$config['upload_path']          = './'.$upload_loc;
								$config['allowed_types']        = 'gif|jpg|png|jpeg';
								$config['encrypt_name']=TRUE;
								$config['max_width']            = 2560;
								$config['max_height']           = 500;
								
								//Load upload library and initialize configuration
								$this->load->library('upload',$config);
								$this->upload->initialize($config);
								
								if($this->upload->do_upload('practicetestbanner_img')){
									$uploadData = $this->upload->data();
									//$details['loc']=$uploadData;
									$details['BANNER_IMG'] = $upload_loc.$uploadData['file_name'];
								}else{
									throw new Exception(strip_tags($this->upload->display_errors()));
								}
							}
							$this->db->trans_start();
    						$testseriesid=$this->Adminpostmodel->addpracticetest($details);
    						create_test_set_practice_test($testseriesid,$_POST['tspaperno'],$_POST['quesno'],$_POST['startdate'],$_POST['enddate'],$_POST['duration']);
    						$this->db->trans_complete();
							//$response['data-send']=$details;
							if($this->db->trans_status() !== FALSE){
    							$response['message']='Data Added Successfully';
    							$response['data']='ok';
    						}else{
    							$response['message']='Something wents wrong';
    							$response['data']='Notok';
    						}
					    }else{
					        $response['message']='Test Set Name already Exist';
    						$response['data']='Notok';
					    }
						json_output(200, $response);
					} catch (Throwable $th){
						$response['message']=$th->getMessage();
						$response['data']='notok';
						json_output(200, $response);
					} catch (Exception $e) {
						$response['message']='Something went wrong';
						$response['data']='notok';
						json_output(200, $response);
					}	 
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
	}

	public function publishpracticetestpaper()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$tpid = $_POST['tpid'];
					$tppublish = $_POST['tppublish'];
					if($tppublish==1){
					    $data = array(
    						'TP_APPROVED_BY' =>1,
    						'TP_LAST_UPDATED' => date('Y-m-d H:i:s')
    					);
					}else{
					    $data = array(
        					'TP_APPROVED_BY' =>'',
        					'TP_LAST_UPDATED' => date('Y-m-d H:i:s')
        				);
					}
					
					$this->Adminpostmodel->updatepracticetestpaper($data,$tpid);
					$response['message']='Practice Test Pulished Successfully';
					$response['data']='ok';
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }
    }
    /**/
}

