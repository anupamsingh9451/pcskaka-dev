<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	    // Your own constructor code
		$this->load->library('session');
		$this->load->database('default');
		$this->load->helper('common_helper');
		$this->load->model('user/Usergetmodel');
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if ($this->session->userdata('userloginid') == '') {
			if (base_url() . 'user' != $actual_link) {
				redirect('user');
			}
		}else{
			if (base_url() . 'user' == $actual_link) {
				redirect('user/subject/list');
			}
		}
	}
	public function index()
	{
		$this->load->view('user/index');
	}
	public function subject()
	{
		$data['page']='subject';
		if($this->uri->segment(3)=='create'){
		    $data['subpage']='Create subject';
			$data['sub'] = $this->Usergetmodel->get_all_subject_model();
    		$this->load->view('user/userheader',$data);
    		$this->load->view('user/subject/subject');
    		$this->load->view('user/subject/createsubjectscript');
    		$this->load->view('user/book/listbookscripts');
    	
		}else if($this->uri->segment(3)=='list'){
		    $data['subpage']='List subject';
			$data['sub'] = $this->Usergetmodel->get_all_subject_model();
    		$this->load->view('user/userheader',$data);
    		$this->load->view('user/subject/subject');
    		$this->load->view('user/subject/listsubjectscript');
    		$this->load->view('user/book/listbookscripts');
    	    $this->load->view('user/subject/editquestionscripts');
		}
		$this->load->view('user/userfooter');
	}
	
	
}
