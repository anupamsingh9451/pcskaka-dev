<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		ob_start();
		$this->load->helper('url');		
		$this->load->database('default');
		$this->load->library('session');
		$this->load->library('email');
		$this->load->model('site/Sitegetmodel');
		$this->load->helper('common_helper');
		$this->load->library('pagination');
        $this->perPage = 10;
	    // Your own constructor code
	    
	    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		
		if ($this->session->userdata('studentloginid') == '') {
			if (base_url() . 'student' != $actual_link) {
				
			}
		}else{
			redirect('student/dashboard');
		}
	}
	public function index()
	{		
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
	    $data['title'] = "Best Online library / Test Series for PCS – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best PCS / PSC /State PCS / State PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best Online library / Test Series for PCS – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC /State PCS / State PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC /State PCS / State PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best Online library / Test Series for PCS – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
		$this->load->view('site/header',$data);
		$this->load->view('site/index');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}		
	public function subjects(){	
	    
	    if($this->uri->segment(3)=='math'){			
	        $this->load->view('site/sitesub/math_sub');				
	    }else if($this->uri->segment(3)=='english'){			
	        $this->load->view('site/sitesub/english');		
	    }else if($this->uri->segment(3)=='history'){			
	        $this->load->view('site/sitesub/history');		
	    }	
	    $this->load->view('site/sitesub/login_reg_scripts');			
	    $this->load->view('site/footer');			
	    $this->load->view('site/modal');			
	    
	}	
	public function error()
	{
		$data = array();
		$data['title'] = 'ERROR | PCSKAKA';
		
		/*=======================dynamic meta tags========================*/
	   	$data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
		
		$data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/error');
		$this->load->view('site/footer');
	}
	
	public function contact()
	{		
	    $data['title'] = "Contact – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Contact us";
	    $data['canonical'] = "https://pcskaka.com/site/contact/";
	    $data['og_title'] = "Contact – PCSKAKA";
	    $data['og_description'] = "Contact us";
	    $data['og_url'] = "https://pcskaka.com/site/contact/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Contact us";
	    $data['twitter_title'] = "Contact – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
	    
		$this->load->view('site/header',$data);
		$this->load->view('site/contact');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function careers()
	{		
	    $data['title'] = "Careers – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Careers available with pcskaka";
	    $data['canonical'] = "https://pcskaka.com/site/careers/";
	    $data['og_title'] = "Careers – PCSKAKA";
	    $data['og_description'] = "Careers available with pcskaka";
	    $data['og_url'] = "https://pcskaka.com/site/careers/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Careers available with pcskaka";
	    $data['twitter_title'] = "Careers – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/careers');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function terms()
	{		
	    $data['title'] = "Terms & Condition – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Terms & Condition of our site";
	    $data['canonical'] = "https://pcskaka.com/site/terms/";
	    $data['og_title'] = "Terms & Condition – PCSKAKA";
	    $data['og_description'] = "Terms & Condition of our site";
	    $data['og_url'] = "https://pcskaka.com/site/terms/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Terms & Condition of our site";
	    $data['twitter_title'] = "Terms & Condition – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/terms_conditions');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	public function payment()
	{		
	    $data['title'] = "Payment – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/site/payment/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/site/payment/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/payment');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	public function pcs()
	{		
	    $data['title'] = "PCS – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    /*$data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function currentaffairs()
	{	
	    
	    $data['title']="Best PSC / PCS / STATE PCS online Current Affairs - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best PCS / PSC online coaching / Library with best current affairs.";
	    $data['canonical'] = "https://pcskaka.com/site/currentaffairs/";
	    $data['og_title'] = "Best PSC / PCS / STATE PCS online Current Affairs - PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library with best current affairs.";
	    $data['og_url'] = "https://pcskaka.com/site/currentaffairs/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library with best current affairs.";
	    $data['twitter_title'] = "Best PSC / PCS / STATE PCS online Current Affairs - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
		$data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$str="";
		if($this->input->get('bk')){
			$str="?bk=".$this->input->get('bk');
		}
		$this->load->view('site/header',$data);
		$config = array();
		$config["base_url"] = base_url() . "Site/currentaffairs";
		$config['first_url'] = base_url() . "Site/currentaffairs".$str;
		$config['suffix'] = '?' . http_build_query($_GET);
        $subid = 7;
        $config["total_rows"] = $this->Sitegetmodel->get_current_affairs_ques_count($subid,$this->input->get('bk'));
        $config["per_page"] = 20;
        $config["uri_segment"] = 3;
        $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
        $page = ($this->uri->segment(3));
		if(empty($page)){
			$page = 1;
		}
		$offset = ($page-1)*20;
	    $data["results"] = $this->Sitegetmodel->get_records($config["per_page"], $offset);
        $data["links"] = $this->pagination->create_links();
		$data["page"] = ($page-1)*20;
		
		$this->pagination->create_links();
		$this->load->view('site/current_affairs',$data);	
	    
		
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	public function testseries()
	{		
	    $data['title'] = "Best PCS / state psc online test series – PCSAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best PCS / PSC online coaching / Library / online test series.";
	    $data['canonical'] = "https://pcskaka.com/site/testseries/";
	    $data['og_title'] = "Best PCS / state psc online test series – PCSAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library / online test series.";
	    $data['og_url'] = "https://pcskaka.com/site/testseries/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library / online test series.";
	    $data['twitter_title'] = "Best PCS / state psc online test series – PCSAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
	    $data['tsdt']=$this->Sitegetmodel->get_all_test_series();
		$this->load->view('site/header',$data);
		$this->load->view('site/testseries');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	public function testpaper()
	{	
	    $data['title'] = "Best PCS online tests – PCSAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best PCS online tests – PCSAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best PCS online tests – PCSAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
	    $tsid = url_decode($this->uri->segment(3));
	    $data['tsdt']=$this->Sitegetmodel->get_test_series_dt_by_id($tsid);
	    if(!empty($data['tsdt'])){
	        if($data['tsdt'][0]->TS_STATUS==1){
	            $this->load->view('site/header',$data);
        		$this->load->view('site/testpaper');
        		$this->load->view('site/modal');
        		$this->load->view('site/sitesub/login_reg_scripts');
        		$this->load->view('site/footer');
	        }else{
	            redirect('site/testseries');
	        }
	    }else{
	        redirect('site/testseries');
	    }
	}
	public function getrecords(){
        $this->load->view("current_affairs", $data);
	}
    public function ias()
	{	
	    $data['title'] = "Best UPSC / IAS online Test Series / Library - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best IAS online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/site/ias/";
	    $data['og_title'] = "Best UPSC / IAS online Test Series / Library - PCSKAKA";
	    $data['og_description'] = "Best IAS online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/site/ias/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best IAS online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPSC / IAS online Test Series / Library - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/ias');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
    public function working_progress()
	{		
	    $data['title'] = "Maintenance – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	   /* $data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/working_on');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	//sweksha code
	public function appsc()
	{		
	    $data['title'] = "How to clear  AP PCS  - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    /*$data['description'] = "Best AP PCS / AP PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best APPCS / APPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best AP PCS / AP PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best AP PCS / AP PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best APPCS / APPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/appsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function cgpsc()
	{	
	    $data['title'] = "Best CG PCS / CGPSC Online Test Series / Library – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best CG PCS / CG PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/site/cgpsc/";
	    $data['og_title'] = "Best CG PCS / CGPSC Online Test Series / Library – PCSKAKA";
	    $data['og_description'] = "Best CG PCS / CG PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/site/cgpsc/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best CG PCS / CG PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best CG PCS / CGPSC Online Test Series / Library – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/cgpsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function gpsc()
	{		
	    $data['title'] = "How to clear  Gujarat PCS / GPSC  - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best Gujarat PCS / GPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best Gujarat PCS / GPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best Gujarat PCS / GPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/gpsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function hpsc()
	{		
	    $data['title'] = "PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    /*$data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/hpsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function hpas()
	{		
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/hpas');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function jpsc()
	{		
	    $data['title'] = "Best JPCS / JPSC Online Test Series / Library – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best Jarkhand PCS / JPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/site/jpsc/";
	    $data['og_title'] = "Best JPCS / JPSC Online Test Series / Library – PCSKAKA";
	    $data['og_description'] = "Best Jarkhand PCS / JPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/site/jpsc/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best Jarkhand PCS / JPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best JPCS / JPSC Online Test Series / Library – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/jpsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function kpsc()
	{		
	    $data['title'] = "PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	   /* $data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/kpsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function kerala_psc()
	{		
	    $data['title'] = "PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    /*$data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/kerala_psc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function mppsc()
	{		
	    $data['title'] = "Best MPPCS / MPPSC Online Test Series / Library– PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best MPPCS / MPPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/site/mppsc/";
	    $data['og_title'] = "Best MPPCS / MPPSC Online Test Series / Library– PCSKAKA";
	    $data['og_description'] = "Best MPPCS / MPPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/site/mppsc/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best MPPCS / MPPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best MPPCS / MPPSC Online Test Series / Library– PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/mppsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function mpsc()
	{		
	    $data['title'] = "Best MPSC Online Test Series / Library– PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best MPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/site/mpsc/";
	    $data['og_title'] = "Best MPSC Online Test Series / Library– PCSKAKA";
	    $data['og_description'] = "Best MPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/site/mpsc/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best MPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best MPSC Online Test Series / Library– PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/mpsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function opsc()
	{		
	    $data['title'] = "PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    /*$data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/opsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function ppsc()
	{		
	    $data['title'] = "PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	   /* $data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/ppsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function ras()
	{		
	    $data['title'] = "Best RAS Online Test Series / Library – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best RAS online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/site/ras/";
	    $data['og_title'] = "Best RAS Online Test Series / Library – PCSKAKA";
	    $data['og_description'] = "Best RAS online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/site/ras/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best RAS online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best RAS Online Test Series / Library – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/ras');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function tnpsc()
	{		
	    $data['title'] = "PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    /*$data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/tnpsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function tspsc()
	{	
	    $data['title'] = "PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	   /* $data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/tspsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function ukpsc()
	{	
	    $data['title'] = "PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    /*$data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/ukpsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function uppsc()
	{		
	    $data['title'] = "Best UPPCS / UPPSC Online Test Series / Library – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best UPPCS / UPPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/site/uppsc/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series / Library – PCSKAKA";
	    $data['og_description'] = "Best UPPCS / UPPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/site/uppsc/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best UPPCS / UPPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series / Library – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/uppsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function pscwb()
	{		
	    $data['title'] = "PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	   /* $data['description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/site/uppsc/";
	    $data['og_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";
	    $data['og_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/site/uppsc/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "Best UPPCS / UPPSC Online Test Series – PCSKAKA";*/
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/pscwb');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function bpsc()
	{		
	    $data['title'] = "How to clear  Bihar PCS / BPSC  - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best Bihar PCS / BPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "How to clear  Bihar PCS / BPSC  - PCSKAKA";
	    $data['og_description'] = "Best Bihar PCS / BPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best Bihar PCS / BPSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "How to clear  Bihar PCS / BPSC  - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/pcs/bpsc');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	
	public function geography()
	{		
	    $data['title'] = "Geography for IAS / PCS Online - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "How to prepare geography for CSE / civil services / IAS / PCS";
	    $data['canonical'] = "https://pcskaka.com/site/geography/";
	    $data['og_title'] = "Geography for IAS / PCS Online - PCSKAKA";
	    $data['og_description'] = "How to prepare geography for CSE / civil services / IAS / PCS";
	    $data['og_url'] = "https://pcskaka.com/site/geography/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "How to prepare geography for CSE / civil services / IAS / PCS";
	    $data['twitter_title'] = "Geography for IAS / PCS Online - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/subject/geography');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	public function science()
	{		
	    $data['title'] = "General Science  for IAS / PCS Online - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "How to prepare Science for CSE / civil services / IAS / PCS.";
	    $data['canonical'] = "https://pcskaka.com/site/science/";
	    $data['og_title'] = "General Science for IAS / PCS Online - PCSKAKA";
	    $data['og_description'] = "How to prepare Science for CSE / civil services / IAS / PCS.";
	    $data['og_url'] = "https://pcskaka.com/site/science/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "How to prepare Science for CSE / civil services / IAS / PCS.";
	    $data['twitter_title'] = "General Science  for IAS / PCS Online - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/subject/science');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	public function polity()
	{		
	    $data['title'] = "Polity for IAS / PCS Online - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "How to prepare Polity for CSE / civil services / IAS / PCS.";
	    $data['canonical'] = "https://pcskaka.com/site/polity/";
	    $data['og_title'] = "Polity for IAS / PCS Online - PCSKAKA";
	    $data['og_description'] = "How to prepare Polity for CSE / civil services / IAS / PCS.";
	    $data['og_url'] = "https://pcskaka.com/site/polity/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "How to prepare Polity for CSE / civil services / IAS / PCS.";
	    $data['twitter_title'] = "Polity for IAS / PCS Online - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/subject/polity');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	public function environment()
	{		
	    $data['title'] = "Environment for IAS / PCS Online - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "How to prepare Environment for CSE / civil services / IAS / PCS.";
	    $data['canonical'] = "https://pcskaka.com/site/environment/";
	    $data['og_title'] = "Environment for IAS / PCS Online - PCSKAKA";
	    $data['og_description'] = "How to prepare Environment for CSE / civil services / IAS / PCS.";
	    $data['og_url'] = "https://pcskaka.com/site/environment/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "How to prepare Environment for CSE / civil services / IAS / PCS.";
	    $data['twitter_title'] = "Environment for IAS / PCS Online - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/subject/environment');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	public function history()
	{	
	    $data['title'] = "History for IAS / PCS Online - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "How to prepare History for CSE / civil services / IAS / PCS.";
	    $data['canonical'] = "https://pcskaka.com/site/history/";
	    $data['og_title'] = "History for IAS / PCS Online - PCSKAKA";
	    $data['og_description'] = "How to prepare History for CSE / civil services / IAS / PCS.";
	    $data['og_url'] = "https://pcskaka.com/site/history/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "How to prepare History for CSE / civil services / IAS / PCS.";
	    $data['twitter_title'] = "History for IAS / PCS Online - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/subject/history');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	public function economics()
	{		
	    $data['title'] = "Economics for IAS / PCS Online - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "How to prepare Economics for CSE / civil services / IAS / PCS.";
	    $data['canonical'] = "https://pcskaka.com/site/economics/";
	    $data['og_title'] = "Economics for IAS / PCS Online - PCSKAKA";
	    $data['og_description'] = "How to prepare Economics for CSE / civil services / IAS / PCS.";
	    $data['og_url'] = "https://pcskaka.com/site/economics/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "How to prepare Economics for CSE / civil services / IAS / PCS.";
	    $data['twitter_title'] = "Economics for IAS / PCS Online - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/subject/economics');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
		$this->load->view('site/footer');
	}
	public function library()
	{	
	    $data['title'] = "How to  become PCS / IAS – PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Best IAS / PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "How to  become PCS / IAS – PCSKAKA";
	    $data['og_description'] = "Best IAS / PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Best IAS / PCS / PSC online coaching / Library scientifically designed by team of enginneers.";
	    $data['twitter_title'] = "How to  become PCS / IAS – PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/library/library');
		$this->load->view('site/modal');
		 $this->load->view('site/sitesub/login_reg_scripts');
    	$this->load->view('site/footer');
	}
	public function sales_refund()
	{	
	    $data['title'] = "Sales Refund - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Sales Refund details for our site- PCSKAKA";
	    $data['canonical'] = "https://pcskaka.com/Site/sales_refund/";
	    $data['og_title'] = "Sales Refund - PCSKAKA";
	    $data['og_description'] = "Sales Refund details for our site - PCSKAKA";
	    $data['og_url'] = "https://pcskaka.com/Site/sales_refund/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Sales Refund details for our site - PCSKAKA";
	    $data['twitter_title'] = "Sales Refund - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/salesrefund/salesrefund');
		$this->load->view('site/modal');
		 $this->load->view('site/sitesub/login_reg_scripts');
    	$this->load->view('site/footer');
	}
	public function sitemap()
	{	
	    $data['title'] = "Sitemap - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "";
	    $data['canonical'] = "https://pcskaka.com/";
	    $data['og_title'] = "Sitemap - PCSKAKA";
	    $data['og_description'] = "";
	    $data['og_url'] = "https://pcskaka.com/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "";
	    $data['twitter_title'] = "Sitemap - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/sitemap/sitemap');
		$this->load->view('site/modal');
		 $this->load->view('site/sitesub/login_reg_scripts');
    	$this->load->view('site/footer');
	}
	public function privacypolicy()
	{	
	    $data['title'] = "Privacy Policy - PCSKAKA";
	    
	    /*=======================dynamic meta tags========================*/
	    $data['description'] = "Privacy Policy details for our site- PCSKAKA";
	    $data['canonical'] = "https://pcskaka.com/site/privacypolicy/";
	    $data['og_title'] = "Privacy Policy - PCSKAKA";
	    $data['og_description'] = "Privacy Policy details for our site - PCSKAKA";
	    $data['og_url'] = "https://pcskaka.com/site/privacypolicy/";
	    $data['og_image'] = base_url()."assets/logo/logo.jpeg";
	    $data['og_image_secure_url'] = base_url()."assets/logo/logo.jpeg";
	    $data['twitter_card'] = "summary_large_image";
	    $data['twitter_description'] = "Privacy Policy details for our site - PCSKAKA";
	    $data['twitter_title'] = "Privacy Policy - PCSKAKA";
	    /*=======================dynamic meta tags========================*/
	    
	    $data['subject']=$this->Sitegetmodel->get_subject_dt_model();
		$this->load->view('site/header',$data);
		$this->load->view('site/privacypolicy/privacypolicy');
		$this->load->view('site/modal');
		$this->load->view('site/sitesub/login_reg_scripts');
    	$this->load->view('site/footer');
	}
	public function sendcontactform()
	{
	   
        $name=$_POST['username'];
        $email=$_POST['email'];
        $subject=$_POST['subject'];
        $mobileno=$_POST['mobileno'];
        $message=$_POST['message'];
        send_enquiry($name,$email,$subject,$message,$mobileno);
        echo "ok";
	  
	}
}

