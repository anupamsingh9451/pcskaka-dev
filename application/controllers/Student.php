<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	    // Your own constructor code
		$this->load->library('session');
		$this->load->database('default');
		$this->load->helper('common_helper');
		$this->load->model('student/Studentgetmodel');
		$this->load->model('admin/Admingetmodel');
		$this->load->library('cart'); 
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if ($this->session->userdata('studentloginid') == '') {
			if (base_url() . 'student' != $actual_link) {
				redirect('');
			}
		}else{
			if (base_url() . 'student' == $actual_link) {
				redirect('student/dashboard');
			}
		}
	}
	public function index()
	{
		redirect('');
	}
	public function performance()
	{
		$data=array();
		$data['page']='performance';
		$data['subpage']='';
		$attmptdt = $this->Studentgetmodel->get_attempt_testpaper_dt();
		if(!empty($attmptdt)){
		    $data['result'] = $this->Studentgetmodel->get_attempt_testpaper_resultdt_andname($attmptdt);
		}else{
		    $data['result'] = "";
		}
		$this->load->view('student/studentheader',$data);
		$this->load->view('student/studentfooter');
		$this->load->view('student/dashboard');
	}
	public function testseries()
    {
        $data=array();
        $data['page']='test series';
        $data['subpage']='';
        $this->load->view('student/studentheader',$data);
        	
        	$data['testpaperdt'] = $this->Studentgetmodel->get_all_test_papers();
            $data['populartestseriesdt'] = $this->Studentgetmodel->get_all_popular_tetseries();
            $data['testseriesdt'] = $this->Studentgetmodel->get_all_tetseries();
            $this->load->view('student/testseries/testseries',$data);
            $this->load->view('student/testseries/testseriesscripts');
        
        $this->load->view('student/studentfooter');
    }
	public function testpaper()
	{
		$data=array();
		$data['page']='test series';
		$data['subpage']='';
		if($this->uri->segment(3)!=''){
		    $tsdt = $this->Studentgetmodel->get_tetseries_dt_by_id($this->uri->segment(3));
		    if(!empty($tsdt)){
		        if($tsdt[0]->TS_STATUS==1 && strtotime($tsdt[0]->TS_END_DATE)>strtotime(date('Y-m-d H:i:s'))){
    		        $data['tsdt'] = $tsdt;
        	    	$data['testpaperdt'] = $this->Studentgetmodel->get_test_papers_by_tsid($this->uri->segment(3));
            		$this->load->view('student/studentheader',$data);
            		$this->load->view('student/testpaper/testpaper');
            		$this->load->view('student/conduct_paper/instructionscripts');
		        }else{
		            redirect('student/testseries');
		        }
		    }else{
		        redirect('student/testseries');
		    }
		}else{
		    redirect('student/dashboard');
		}
	
		$this->load->view('student/studentfooter');
	}
	public function profile()
	{
		$data=array();
		$data['page']='profile';
		if($this->uri->segment(3)=='editprofile'){
			$data['subpage']='Edit profile';
			$data['editdt']=$this->Studentgetmodel->get_user_dt_by_id($this->session->userdata('studentloginid'));
			$this->load->view('student/studentheader',$data);
			$this->load->view('student/profile/editprofile');
		}
		else if($this->uri->segment(3)=='changepwd'){
			$data['subpage']='Change Password';
			$data['userid']=$this->session->userdata('studentloginid');
			$this->load->view('student/studentheader',$data);
			$this->load->view('student/profile/changepwd');
		}
		else{
			redirect('student/dashboard');
		}
		$this->load->view('student/studentfooter');

	}
	public function library()
    {
        $data=array();
        $data['page']='exams';
        $data['subpage']='';
        $this->load->view('student/studentheader',$data);

        $i = 0;
    	$arr = array();
        foreach($this->Studentgetmodel->get_all_courses(0,9999999) as $row){
        	if($row->COURSE_POPULAR == 1){
        		$arr[$i]["ID"] = $row->COURSE_ID;
                $arr[$i]["PURCHASE2TYPE"] = 0;
                $arr[$i]["PURCHASE2TYPENAME"] = "Course";
                $arr[$i]["NAME"] = $row->COURSE_NAME;
                $arr[$i]["STATUS"] = $row->COURSE_STATUS;
                $arr[$i]["AMT"] = $row->COURSE_AMT;
                $arr[$i]["IMAGE"] = $row->COURSE_IMG;
                $arr[$i]["CONTENT_AMT"] = $row->COURSE_CONTENT_AMT;
                $arr[$i]["CONTENT_DISC"] = $row->COURSE_CONTENT_DISC;
                $arr[$i]["CONTENT_SUMMARY"] = array(
                    array("name"=>"Exams",
                          "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"LIB_STATUS"=>1],'library')),
                    array("name"=>"Subjects",
                          "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"SUB_STATUS"=>1],'subjects')),
                    array("name"=>"Test Series",
                          "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"TS_STATUS"=>1],'test_series')),
                    array("name"=>"Books",
                          "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"BOOK_STATUS"=>1],'books'))
                );
                $i++;
        	}
        }
        $current_year_exams = array();
        foreach($this->Studentgetmodel->current_year_exams() as $row){
        	$index = date("F Y",strtotime($row->LIB_START_DATE));
        	$current_year_exams[$index][] = [
        		"LIB_ID" => $row->LIB_ID,
                "LIB_NAME" => $row->LIB_NAME,
        		"LIB_START_DATE" => date("j M",strtotime($row->LIB_START_DATE)),
        		"LIB_END_DATE" => date("j M",strtotime($row->LIB_END_DATE))

        	];
        }

    	$data['current_year_exams'] = $current_year_exams;
    	$data['popularcourses'] = $arr;
        $data['upcoming_and_popular_exams'] = $this->Studentgetmodel->get_upcoming_popular_library();
        $data['populartestseriesdt'] = $this->Studentgetmodel->get_all_popular_tetseries();
        $data['testseriesdt'] = $this->Studentgetmodel->get_all_tetseries();

        $this->load->view('student/library/library',$data);
        $this->load->view('student/library/libraryscripts');
        $this->load->view('student/studentfooter');
	}
	public function dashboard()
    {
        $data=array();
        $data['page']='dashboard';
        $data['subpage']='';
        $this->load->view('student/studentheader',$data);

            $i = 0;
            $arr = array();
            foreach($this->Studentgetmodel->get_all_courses(0,9999999) as $row){
                if($row->COURSE_POPULAR == 1){
                    $arr[$i]["ID"] = $row->COURSE_ID;
                    $arr[$i]["PURCHASE2TYPE"] = 0;
                    $arr[$i]["PURCHASE2TYPENAME"] = "Course";
                    $arr[$i]["NAME"] = $row->COURSE_NAME;
                    $arr[$i]["STATUS"] = $row->COURSE_STATUS;
                    $arr[$i]["IMAGE"] = $row->COURSE_IMG;
                    $arr[$i]["AMT"] = $row->COURSE_AMT;
                    $arr[$i]["CONTENT_AMT"] = $row->COURSE_CONTENT_AMT;
                    $arr[$i]["CONTENT_DISC"] = $row->COURSE_CONTENT_DISC;
                    $arr[$i]["CONTENT_SUMMARY"] = array(
                        array("name"=>"Subjects",
                        "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"SUB_STATUS"=>1],'subjects')),
                    array("name"=>"Practice Test",
                            "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"TS_STATUS"=>1],'practice_test')),
                    array("name"=>"Books",
                        "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"BOOK_STATUS"=>1],'books'))
                    );
                    $i++;
                }
            }
            $data['popular_slider']  = $this->Studentgetmodel->get_all_suggested_sliders('1');
            $data['popularcourses'] = $arr;
            $data['testpaperdt'] = $this->Studentgetmodel->get_all_suggested_test_papers();
            $data['catestpaperdt'] = $this->Studentgetmodel->get_all_current_affairs_test_papers();
            $data['suggestedtestseriesdt'] = $this->Studentgetmodel->get_all_suggested_tetseries();
            $data['testseriesdt'] = $this->Studentgetmodel->get_all_tetseries();
            $this->load->view('student/home/home',$data);
            $this->load->view('student/home/homescripts');
        
        $this->load->view('student/studentfooter');
    }


    public function purchase2()
    {
        $data=array();
        $data['page']='purchase2';
        $data['subpage']='';
        $this->load->view('student/studentheader',$data);
        
            $this->load->view('student/purchase2/purchase2',$data);
            $this->load->view('student/purchase2/purchase2script');
        
        $this->load->view('student/studentfooter');
    }


    public function dashboard2()
    {
        $data=array();
        $data['page']='dashboard';
        $data['subpage']='';
		$res = $this->Studentgetmodel->get_all_unattempted_or_attempted_result_ts();
		$data["total"]     = $res["total"];
		$data["attempted"] = $res["attempted"];
		$data["attendance"] = $res["attendance"];
		$data["data"]      = $res["data"];
		$data['purchase_summary'] = $this->Studentgetmodel->get_user_purchase_summary();
		$this->load->view('student/studentheader',$data);
        $this->load->view('student/new_dashboard',$data);
        $this->load->view('student/studentfooter');
    }



    public function library_detail( $LIB_ID = '' )
    {
        $data=array();
        $data['page']='exams';
        $data['subpage']='';

        $lib = $this->Studentgetmodel->get_active_library_by_lib_id($LIB_ID);
        if(count($lib)>0){
        	$library_data = array();
            foreach($this->Studentgetmodel->get_active_testseries_by_lib_id($lib[0]->LIB_ID) as $index => $row){
                $library_data[$index] = [
                    "TS_ID"            => $row->TS_ID,
                    "TS_COST"          => $row->TS_COST,
                    "TS_NAME"          => $row->TS_NAME,
                    "TS_IMG"          => $row->TS_IMG,
                    "free_test_papers" => array(),
                    "paid_test_papers" => array(),
                    "free_quizzes"     => array(),
                    "paid_quizzes"     => array()
                ];
                $test_papers = $this->Studentgetmodel->get_test_papers_by_tsid($row->TS_ID);
                if(!empty($test_papers)){
                    foreach($test_papers as $test_paper){
                        if($test_paper->TP_STATUS==1){
                            if($test_paper->TP_PAY_STATUS==1){  //Free
                                if($test_paper->TP_QUIZ == 1){ //Quiz
                                    $library_data[$index]["free_quizzes"][] = [
                                        "TS_ID"=>$row->TS_ID,
                                        "TP_ID"=>$test_paper->TP_ID,
                                        "TP_NAME" => $test_paper->TP_NAME,
                                        "TP_START_DATE" => $test_paper->TP_START_DATE,
                                        "TS_QUESTION_NOS" => $row->TS_QUESTION_NOS,
                                        "TS_MARK" => ceil($row->TS_P_MARKS*$row->TS_QUESTION_NOS),
                                        "TS_DURATION" => $row->TS_DURATION
                                    ];
                                }else{ //paper
                                    $library_data[$index]["free_test_papers"][] = [
                                        "TS_ID"=>$row->TS_ID,
                                        "TP_ID"=>$test_paper->TP_ID,
                                        "TP_NAME" => $test_paper->TP_NAME,
                                        "TP_START_DATE" => $test_paper->TP_START_DATE,
                                        "TS_QUESTION_NOS" => $row->TS_QUESTION_NOS,
                                        "TS_MARK" => ceil($row->TS_P_MARKS*$row->TS_QUESTION_NOS),
                                        "TS_DURATION" => $row->TS_DURATION
                                    ];
                                }
                            }else{   //Non Free
                                if($test_paper->TP_QUIZ == 1){ //Quiz
                                    $library_data[$index]["paid_quizzes"][] = [
                                        "TS_ID"=>$row->TS_ID,
                                        "TP_ID"=>$test_paper->TP_ID,
                                        "TP_NAME" => $test_paper->TP_NAME,
                                        "TP_START_DATE" => $test_paper->TP_START_DATE,
                                        "TS_QUESTION_NOS" => $row->TS_QUESTION_NOS,
                                        "TS_MARK" => ceil($row->TS_P_MARKS*$row->TS_QUESTION_NOS),
                                        "TS_DURATION" => $row->TS_DURATION
                                    ];
                                }else{ //paper
                                    $library_data[$index]["paid_test_papers"][] = [
                                        "TS_ID"=>$row->TS_ID,
                                        "TP_ID"=>$test_paper->TP_ID,
                                        "TP_NAME" => $test_paper->TP_NAME,
                                        "TP_START_DATE" => $test_paper->TP_START_DATE,
                                        "TS_QUESTION_NOS" => $row->TS_QUESTION_NOS,
                                        "TS_MARK" => ceil($row->TS_P_MARKS*$row->TS_QUESTION_NOS),
                                        "TS_DURATION" => $row->TS_DURATION
                                    ];
                                }
                            }
                        }
                    }
                }
            }
            $data['LIB_DETAIL'] = $lib[0];
            $data["LIB_DATA"] = $library_data;
        }else{
        	redirect(base_url('student/library'));
        }
        $this->load->view('student/studentheader',$data);
        $this->load->view('student/library_detail/library_detail',$data);
        $this->load->view('student/library_detail/libraryscripts');
        $this->load->view('student/studentfooter');
    }
    
    public function courses()
    {
        $data=array();
        $data['page']='courses';
        $data['subpage']='';
        $this->load->view('student/studentheader',$data);
        $j=$i = 0;
        $arr = array();
        $all_courses=array();
        foreach($this->Studentgetmodel->get_all_courses(0,9999999) as $row){
        	if($row->COURSE_POPULAR == 1){
        		$arr[$i]["ID"] = $row->COURSE_ID;
                $arr[$i]["PURCHASE2TYPE"] = 0;
                $arr[$i]["PURCHASE2TYPENAME"] = "Course";
                $arr[$i]["NAME"] = $row->COURSE_NAME;
                $arr[$i]["STATUS"] = $row->COURSE_STATUS;
                $arr[$i]["IMAGE"] = $row->COURSE_IMG;
                $arr[$i]["AMT"] = $row->COURSE_AMT;
                $arr[$i]["CONTENT_AMT"] = $row->COURSE_CONTENT_AMT;
                $arr[$i]["CONTENT_DISC"] = $row->COURSE_CONTENT_DISC;
                $arr[$i]["CONTENT_SUMMARY"] = array(
                    array("name"=>"Subjects",
                        "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"SUB_STATUS"=>1],'subjects')),
                    array("name"=>"Practice Test",
                            "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"TS_STATUS"=>1],'practice_test')),
                    array("name"=>"Books",
                        "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"BOOK_STATUS"=>1],'books'))
                );
                $i++;
            }
            $all_courses[$j]["ID"] = $row->COURSE_ID;
            $all_courses[$j]["PURCHASE2TYPE"] = 0;
            $all_courses[$j]["PURCHASE2TYPENAME"] = "Course";
            $all_courses[$j]["NAME"] = $row->COURSE_NAME;
            $all_courses[$j]["STATUS"] = $row->COURSE_STATUS;
            $all_courses[$j]["IMAGE"] = $row->COURSE_IMG;
            $all_courses[$j]["AMT"] = $row->COURSE_AMT;
            $all_courses[$j]["CONTENT_AMT"] = $row->COURSE_CONTENT_AMT;
            $all_courses[$j]["CONTENT_DISC"] = $row->COURSE_CONTENT_DISC;
            $all_courses[$j]["CATEGORY"] = $row->CC_NAME;
            $all_courses[$j]["CONTENT_SUMMARY"] = array(
                array("name"=>"Subjects",
                        "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"SUB_STATUS"=>1],'subjects')),
                array("name"=>"Practice Test",
                        "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"TS_STATUS"=>1],'practice_test')),
                array("name"=>"Books",
                        "count"=>$this->Studentgetmodel->get_product_type_count(["COURSE_ID"=>$row->COURSE_ID,"BOOK_STATUS"=>1],'books'))
            );
            $j++;
        }
            
        $data['popularcourses'] = $arr;
        $data['course_category']=$this->Studentgetmodel->get_course_category();
        $data['all_courses']=$all_courses;
        $data['testpaperdt'] = $this->Studentgetmodel->get_all_test_papers();
        $data['populartestseriesdt'] = $this->Studentgetmodel->get_all_popular_tetseries();
        $data['testseriesdt'] = $this->Studentgetmodel->get_all_tetseries();
        $this->load->view('student/courses/courses',$data);
        $this->load->view('student/courses/coursesscript');
        
        $this->load->view('student/studentfooter');
    }

    public function course_details()
    {
        $data=array();
        $data['page']='course';
        $data['subpage']='';
        $this->load->view('student/studentheader',$data);
        $course_id=$this->uri->segment(3);
        if($course_id!=""){
            $row=$this->Studentgetmodel->get_course_by_id($course_id);
            if(!empty($row)){
                $data['row']=$row;
                $data["COURSE_ID"] = $row[0]->COURSE_ID;
                $data["COURSE_CC_ID"] = $row[0]->CC_ID;
                $data["PURCHASE2TYPENAME"] = "Course";
                $data["NAME"] = $row[0]->COURSE_NAME;
                $data['allsubjects']=$this->Studentgetmodel->get_subjectdt_by_course_id($course_id);
                $this->load->view('student/courses/course_details',$data);
                $this->load->view('student/courses/coursesscript');
            }else{
                redirect('student/courses');
            }  
        }else{
            redirect('student/courses');
        }
        //$this->load->view('student/courses/course_detailsscript');
        
        $this->load->view('student/studentfooter');
    }

    public function subject_details()
    {
        $data=array();
        $data['page']='course';
        $data['subpage']='';
        $this->load->view('student/studentheader',$data);
        $subject_id=$this->uri->segment(3);
        if($subject_id!=""){
            $row=$this->Studentgetmodel->get_subject_by_id($subject_id);
            if(!empty($row)){
                $data['row']=$row;
                $data["SUBJECT_ID"] = $row[0]->SUB_ID;
                $data["PURCHASE2TYPENAME"] = "Book";
                $data["NAME"] = $row[0]->SUB_NAME;
                $data['allbooks']=$this->Studentgetmodel->get_booksdt_by_sub_id($subject_id);

                $this->load->view('student/courses/subject_details',$data);
                $this->load->view('student/courses/coursesscript');
            }else{
                redirect('student/courses');
            }  
        }else{
            redirect('student/courses');
        }
        //$this->load->view('student/courses/course_detailsscript');
        
        $this->load->view('student/studentfooter');
    }

    public function book_details()
    {
        $data=array();
        $data['page']='course';
        $data['subpage']='';
        $this->load->view('student/studentheader',$data);
        $book_id=$this->uri->segment(3);
        if($book_id!=""){
            $row=$this->Studentgetmodel->get_bookdt_by_id($book_id);
            if(!empty($row)){
                $data['row']=$row;
                $data["BOOK_ID"] = $row[0]->BOOK_ID;
                $data["PURCHASE2TYPENAME"] = "Book";
                $data["NAME"] = $row[0]->BOOK_NAME;
                $data['all_practicetest']=$this->Studentgetmodel->get_practicetest_by_book_id($book_id);

                $this->load->view('student/courses/book_details',$data);
                $this->load->view('student/courses/coursesscript');
            }else{
                redirect('student/courses');
            }  
        }else{
            redirect('student/courses');
        }
        //$this->load->view('student/courses/course_detailsscript');
        
        $this->load->view('student/studentfooter');
    }

    public function ptestpaper()
    {
        $data=array();
        $data['page']='Course';
        $data['subpage']='';
        $pt_id=$this->uri->segment(3);
        $ptdt = $this->Studentgetmodel->get_practicetest_by_id($pt_id);
        if(!empty($ptdt)){
            $index=0;
            $ptdt=$ptdt[0];
            $pt_data[$index] = [
                "PT_ID"            => $ptdt->TS_ID,
                "PT_COST"          => $ptdt->TS_COST,
                "PT_NAME"          => $ptdt->TS_NAME,
                "PT_IMG"          => $ptdt->TS_IMG,
                "free_test_papers" => array(),
                "paid_test_papers" => array(),
                "free_quizzes"     => array(),
                "paid_quizzes"     => array()
            ];
            $test_papers = $this->Studentgetmodel->get_practice_test_papers_by_ptsid($pt_id);
            if(!empty($test_papers)){
                foreach($test_papers as $test_paper){
                    if($test_paper->TP_STATUS==1){
                        if($test_paper->TP_PAY_STATUS==1){  //Free
                            if($test_paper->TP_QUIZ == 1){ //Quiz
                                $pt_data[$index]["free_quizzes"][] = [
                                    "TS_ID"=>$ptdt,
                                    "TP_ID"=>$test_paper->TP_ID,
                                    "TP_NAME" => $test_paper->TP_NAME,
                                    "TP_START_DATE" => $test_paper->TP_START_DATE,
                                    "TS_QUESTION_NOS" => $ptdt->TS_QUESTION_NOS,
                                    "TS_MARK" => ceil($ptdt->TS_P_MARKS*$row->TS_QUESTION_NOS),
                                    "TS_DURATION" => $ptdt->TS_DURATION
                                ];
                            }else{ //paper
                                $pt_data[$index]["free_test_papers"][] = [
                                    "TS_ID"=>$ptdt->TS_ID,
                                    "TP_ID"=>$test_paper->TP_ID,
                                    "TP_NAME" => $test_paper->TP_NAME,
                                    "TP_START_DATE" => $test_paper->TP_START_DATE,
                                    "TS_QUESTION_NOS" => $ptdt->TS_QUESTION_NOS,
                                    "TS_MARK" => ceil($ptdt->TS_P_MARKS*$ptdt->TS_QUESTION_NOS),
                                    "TS_DURATION" => $ptdt->TS_DURATION
                                ];
                            }
                        }else{   //Non Free
                            if($test_paper->TP_QUIZ == 1){ //Quiz
                                $pt_data[$index]["paid_quizzes"][] = [
                                    "TS_ID"=>$ptdt->TS_ID,
                                    "TP_ID"=>$test_paper->TP_ID,
                                    "TP_NAME" => $test_paper->TP_NAME,
                                    "TP_START_DATE" => $test_paper->TP_START_DATE,
                                    "TS_QUESTION_NOS" => $ptdt->TS_QUESTION_NOS,
                                    "TS_MARK" => ceil($ptdt->TS_P_MARKS*$ptdt->TS_QUESTION_NOS),
                                    "TS_DURATION" => $ptdt->TS_DURATION
                                ];
                            }else{ //paper
                                $pt_data[$index]["paid_test_papers"][] = [
                                    "TS_ID"=>$ptdt->TS_ID,
                                    "TP_ID"=>$test_paper->TP_ID,
                                    "TP_NAME" => $test_paper->TP_NAME,
                                    "TP_START_DATE" => $test_paper->TP_START_DATE,
                                    "TS_QUESTION_NOS" => $ptdt->TS_QUESTION_NOS,
                                    "TS_MARK" => ceil($ptdt->TS_P_MARKS*$ptdt->TS_QUESTION_NOS),
                                    "TS_DURATION" => $ptdt->TS_DURATION
                                ];
                            }
                        }
                    }
                }
            }
            $data['PT_DETAIL'] = $ptdt;
            $data["PTP_DATA"] = $pt_data;
        }else{
        	redirect('student/courses');
        }
        $this->load->view('student/studentheader',$data);
        $this->load->view('student/courses/practicetest_detail',$data);
        $this->load->view('student/courses/coursesscript');
        $this->load->view('student/studentfooter');
    }
}
