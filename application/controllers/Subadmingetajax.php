<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Subadmingetajax extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database('default');
		$this->load->model('subadmin/Authentication_model');
        $this->load->model('subadmin/Subadmingetmodel');
        $this->load->model('subadmin/Subadminpostmodel');
		$this->load->helper('json_output_helper');
		$this->load->helper('common_helper');
	}
	public function get_subadmin_dt()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $params = $_REQUEST;
    				$subadminid = $_GET['id'];
                    $response = $this->Subadmingetmodel->get_subadmin_dt_model($subadminid);
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
    public function getlibrary()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $params = $_REQUEST;
                    $response = $this->Subadmingetmodel->get_all_library();
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
    
    public function get_subject_by_libid()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $params = $_REQUEST;
                    $subid = $_GET['subid'];
                    $response = $this->Subadmingetmodel->get_subject_by_libid($subid);
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
 
    public function get_book_by_subid()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $params = $_REQUEST;
                    $subid = $_GET['subid'];
                    $response = $this->Subadmingetmodel->get_book_by_subid($subid);
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
    

    public function listquestion()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $columns = array(
                        0 => 'SR_NO',
                        1 => 'QUES',
                        2 => 'QUES_CREATED_BY',
                        3 => 'QUES_CREATED_AT',
                        4 => 'QUES_EDITED_BY',
                        5 => 'QUES_APPROVED_BY',
                        6 => 'QUES_DIFFICULTY_LEVEL',
                        7 => 'QUES_STATUS',
                        8 => 'TOOLS',
                    );
                    $limit = $this->input->get('length');
                    $start = $this->input->get('start');
                    $order = $columns[$this->input->get('order')[0]['column']];
                    $dir = $this->input->get('order')[0]['dir'];
                    if(isset($_GET['userid']) && isset($_GET['bookids'])){
                        $bookseg = '';
                        $chapterids = $this->Subadmingetmodel->get_chapterids_by_booksid($_GET['bookids']);
                        $totalData = count_all_ques($chapterids,$_GET['userid'],$_GET['level'],$_GET['status'], $_GET['approve_by']);
                        
                        $totalFiltered = $totalData;
                        if (empty($this->input->get('search')['value'])) {
                            $posts = get_all_ques($limit, $start, $order, $dir, $chapterids, $_GET['userid'], $_GET['level'], $_GET['status'], $_GET['approve_by']);
                        } else {
                            $search = $this->input->get('search')['value'];
                            $posts =  get_all_ques_search($limit, $start, $search, $order, $dir,  $chapterids, $_GET['userid'], $_GET['level'], $_GET['status'], $_GET['approve_by']);
                            $totalFiltered = get_all_ques_search_count($search, $chapterids, $_GET['userid'], $_GET['level'], $_GET['status'], $_GET['approve_by']);
                        }
                    }
                    if(isset($_GET['chapterid'])){
                        $bookseg = '/'.$_GET['chapterid'];
                        $totalData = count_all_ques($_GET['chapterid']);
                        $totalFiltered = $totalData;
                        if (empty($this->input->get('search')['value'])) {
                            $posts = get_all_ques($limit, $start, $order, $dir, $_GET['chapterid']);
                        } else {
                            $search = $this->input->get('search')['value'];
                            $posts =  get_all_ques_search($limit, $start, $search, $order, $dir,  $_GET['chapterid']);
                            $totalFiltered = get_all_ques_search_count($search, $_GET['chapterid']);
                        }
                    }
    				$max=array();
    				$i = $start + 1;
    				if(!empty($posts)){
    					foreach($posts as $res){
    						$min=array();
    						$min['SR_NO']=$i;
    						if($res->QUES_TYPE==1){
    						    $ques= $res->QUES;
    						}else{
    						    $ques=json_decode($res->QUES)->QUESTION;
    						}
    						
    				// 		if(strlen($ques)>=102){
    				// 		    $ques = substr($ques, 0, 102)."...";
    				// 		}
    						$min['QUES'] = $ques;
    						$level = $this->Subadmingetmodel->get_difficulty_level($res->QUES_DIFFICULTY_LEVEL);
    						$min['QUES_DIFFICULTY_LEVEL']=$level;
    						
    						$userrole1 = $this->Subadmingetmodel->get_user_role_by_id($res->QUES_CREATED_BY);
    						$min['QUES_CREATED_BY']= $userrole1[0]->USER_FNAME.' '.$userrole1[0]->USER_LNAME.'<br><span style="text-align:center">('.$userrole1[0]->USER_ROLE_NAME.')</span>';
    						
    						$queslog = $this->Subadmingetmodel->get_ques_log_by_quesid($res->QUES_ID);
    						if(!empty($queslog)){
    						    $min['QUES_EDITED_BY'] = $queslog[0]->USER_FNAME.' '.$queslog[0]->USER_LNAME.'<br><span style="text-align:center">('.$queslog[0]->USER_ROLE_NAME.')</span>';
    						}else{
    						    $min['QUES_EDITED_BY'] = '--';
    						}

                            $userrole2 = $this->Subadmingetmodel->get_user_role_by_id($res->QUES_APPROVED_BY);
                            if(!empty($userrole2)){
    						    $min['QUES_APPROVED_BY'] = $userrole2[0]->USER_FNAME.' '.$userrole2[0]->USER_LNAME.'<br><span style="text-align:center">('.$userrole2[0]->USER_ROLE_NAME.')</span>';
                            }else{
                                $min['QUES_APPROVED_BY'] = '--';
                            }

    						$min['QUES_CREATED_AT']=date('d-m-Y',strtotime($res->QUES_CRATED_AT));
    						if($res->QUES_STATUS==1){
    						    $str = '';
    							$stat='<button style="height: 25px;padding: 0 5px 0 5px;font-size: 11px;" type="button" class="btn btn-success pad_4">ACTIVE</button>';
    						}else if($res->QUES_STATUS==3){
    						    $str = '<li><a class="deleteques" attr-quesid="'.$res->QUES_ID.'" href="javascript:void(0)">Delete</a></li>';
    							$stat='<button type="button" style="height: 25px;padding: 0 5px 0 5px;font-size: 11px;" class="btn btn-warning pad_4">HOLD</button>';
    						}else{
    						    $str = '<li><a class="deleteques" attr-quesid="'.$res->QUES_ID.'" href="javascript:void(0)">Delete</a></li>';
    							$stat='<button type="button" style="height: 25px;padding: 0 5px 0 5px;font-size: 11px;" class="btn btn-danger pad_4">INACTIVE</button>';
    						}
    						$min['QUES_STATUS']=$stat;
    						
    						$min['TOOLS']='<div class="btn-group m-r-5 m-b-5">
    							<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    							<ul class="dropdown-menu">
    								<li><a href="'.base_url().'subadmin/question/edit/'.$res->QUES_ID.'">Edit</a></li>
    							    <li><a class="view_ques_modal" href="javascript:void(0);" attr-quesid="'.$res->QUES_ID.'">View</a></li>
    							    <li><a class="edit_ques_on_modal" href="javascript:void(0);" attr-quesid="'.$res->QUES_ID.'">Edit on modal</a></li>
    							    
    							</ul>
    						</div>';
    								// <li><a href="'.base_url().'subadmin/question/view/'.$res->QUES_ID.'">View</a></li>
    						array_push($max,$min);
    						$i++;
    					}
    				}
    				$json_data = array(
                        "draw"            => intval($this->input->post('draw')),
                        "recordsTotal"    => intval($totalData),
                        "recordsFiltered" => intval($totalFiltered),
                        "data"            => $max
                    );
                    echo json_output(200, $json_data);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    json_output(401, $response);
                }
			}
        }
    }
    public function get_all_sub()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $response = $this->Subadmingetmodel->get_all_subjects();
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
    public function get_question_by_bookid()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $bookid = $_GET['bookid'];
                    $tsid = $_GET['tsid'];
                    $tsdt = $this->Subadmingetmodel->get_testseriesdt_by_id($tsid);
                    $tsquesarr = '';
                    if($tsdt[0]->TS_INSERTED_QUES!=''){
                        $tsquesarr = json_decode($tsdt[0]->TS_INSERTED_QUES);
                    }
                    
                    $ques_type = $_GET['ques_type'];
                    $chapterid = $this->Subadmingetmodel->get_chapterids_by_booksid($bookid);
                    
                    $result = $this->Subadmingetmodel->get_all_ques_by_chapterid_questype($chapterid,$ques_type);
                    $max=array();
    				if(!empty($result)){
    					foreach($result as $res){
    					    
    					    if($tsquesarr!=''){
    					        $min=array();
    					        if(!in_array($res->QUES_ID,$tsquesarr)){
    					            $min['QUES_ID']= $res->QUES_ID;
            						if($res->QUES_TYPE==1){
            						    $ques = substr($res->QUES, 0, 30);
            						    $min['QUES']= $small =  $ques.'...';
            						}else{
            						    $ques = substr(json_decode($res->QUES)->QUESTION, 0, 30);
            						    $min['QUES']= $small =  $ques.'...';
            						}
            						array_push($max,$min);
    					        }
    					    }else{
    					        $min=array();
    					        $min['QUES_ID']= $res->QUES_ID;
        						if($res->QUES_TYPE==1){
        						    $ques = substr($res->QUES, 0, 30);
        						    $min['QUES']= $small =  $ques.'...';
        						}else{
        						    $ques = substr(json_decode($res->QUES)->QUESTION, 0, 30);
        						    $min['QUES']= $small =  $ques.'...';
        						}
        						array_push($max,$min);
    					    }
    						
    					}
    				}
    				$response['message'] = 'ok';
    				$response['data'] = $max;
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    
                    json_output(401, $response);
                }
			}
        }
    }
    
    public function view_ques_html()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $quesid = $_GET['quesid'];
                    $data['quesdt'] = $this->Subadmingetmodel->get_quesdt_by_id($quesid);
    				echo $response=json_encode($this->load->view('subadmin/question/view_ques_modal',$data));
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Subadminpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
    }
    
     public function listbook()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $columns = array(
                        0 => 'SR_NO',
                        1 => 'BOOK_NAME',
                        2 => 'SUB_NAME',
                        3 => 'LIB_NAME',
                        4 => 'BOOK_CHAPTER_NOS',
                        5 => 'PENDING_QUES',
                        6 => 'BOOK_CREATED_AT',
                        7 => 'BOOK_STATUS',
                        8 => 'TOOLS',
                    );
                    $limit = $this->input->get('length');
                    $start = $this->input->get('start');
                    $order = $columns[$this->input->get('order')[0]['column']];
                    $dir = $this->input->get('order')[0]['dir'];
                    
                    $subid = $_GET['subid'];
                    if(isset($subid)){
                        $totalData = count_all_book($subid);
                        $totalFiltered = $totalData;
                        if (empty($this->input->get('search')['value'])) {
                            $posts = get_all_book($limit, $start, $order, $dir, $subid);
                        } else {
                            $search = $this->input->get('search')['value'];
                            $posts =  get_all_book_search($limit, $start, $search, $order, $dir,  $subid);
                            $totalFiltered = get_all_book_search_count($search, $subid);
                        }
                    }
    				$max=array();
    				$i = $start + 1;
    				if(!empty($posts)){
    					foreach($posts as $res){
    						$min['SR_NO']=$i;
    						$chapterdt = $this->Subadmingetmodel->get_all_chapter_by_bookid($res->BOOK_ID);
    						$min['BOOK_NAME']=$res->BOOK_NAME;
    						$min['SUB_NAME']=$res->SUB_NAME;
    						$min['LIB_NAME']=$res->LIB_NAME;
    						$min['BOOK_CHAPTER_NOS']=$res->BOOK_CHAPTER_NOS;
    						$min['PENDING_QUES']=$this->Subadmingetmodel->get_pending_ques($chapterdt[0]->CHAPTER_ID);;
    						$min['BOOK_CREATED_AT']=date('d-m-Y',strtotime($res->BOOK_CREATED_AT));
    						if($res->BOOK_STATUS==1){
    							$stat='<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
    						}else{
    							$stat='<button type="button" class="btn btn-danger pad_4">INACTIVE</button>';
    						}
    						$min['BOOK_STATUS']=$stat;
    						$chaptedt = $this->Subadmingetmodel->get_all_chapter_by_bookid($res->BOOK_ID);
    						$min['TOOLS']='<div class="btn-group m-r-5 m-b-5">
    							<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    							<ul class="dropdown-menu">
    								<li><a href="javascript:void(0);" class="bookedit" attr-chapterno="'.$res->BOOK_CHAPTER_NOS.'" attr-bookid="'.$res->BOOK_ID.'" attr-status="'.$res->BOOK_STATUS.'" attr-booknm="'.$res->BOOK_NAME.'" attr-subid="'.$res->SUB_ID.'" >Edit</a></li>
    							     <li><a href="'.base_url().'subadmin/question/'.$chaptedt[0]->CHAPTER_ID.'">Question</a></li>

    							    
    							</ul>
    						</div>';
    						array_push($max,$min);
    						$i++;
    					}
    				}
    				$json_data = array(
                        "draw"            => intval($this->input->post('draw')),
                        "recordsTotal"    => intval($totalData),
                        "recordsFiltered" => intval($totalFiltered),
                        "data"            => $max
                    );
                    echo json_output(200, $json_data);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    json_output(401, $response);
                }
			}
        }
    }
    public function edit_ques_html()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $quesid = $_GET['quesid'];
                    $data['quesdt'] = $this->Subadmingetmodel->get_quesdt_by_id($quesid);
			        $data['all_sub'] = $this->Subadmingetmodel->get_subject_by_libid($data['quesdt'][0]->LIB_ID);
    				echo $response=json_encode($this->load->view('subadmin/question/edit_ques_modal',$data));
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
    }
    public function add_ques_html()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    $chapterid = $_GET['chapterid'];
			        $data['chapterdt'] = $this->Subadmingetmodel->get_chaptersdt_by_id($chapterid)['data'];
    				echo $response=json_encode($this->load->view('subadmin/question/add_ques_modal',$data));
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Adminpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
    }
}
