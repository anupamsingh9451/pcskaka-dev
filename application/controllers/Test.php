<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		
	    $this->load->helper('url');
	    // Your own constructor code
	    $this->load->database('default');
		$this->load->helper('cookie');
		$this->load->library('session');
		$this->load->model('student/Authentication_model');
        $this->load->model('student/Studentpostmodel');
		$this->load->model('student/Studentgetmodel');
		$this->load->helper('json_output_helper');
		$this->load->helper('common_helper');
		date_default_timezone_set('Asia/Kolkata');
	
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if ($this->session->userdata('studentloginid') == '') {
			if (base_url() . 'student' != $actual_link) {
				redirect('');
			}
		}else{
			if (base_url() . 'student' == $actual_link) {
				redirect('student/dashboard');
			}
		}
	}
	public function index()
	{
		redirect('student/dashboard');
	}
	public function instruction()
	{
		$data=array();
		$data['page']='Dashboard';
		$data['subpage']='';
		if(($this->uri->segment(3))!=''){
		    $tpid = url_decode($this->uri->segment(3));
		    $tpdt = $this->Studentgetmodel->get_tetpaper_dt_by_id($tpid);
		    if(!empty($tpid) && !empty($tpdt)){
		        
				$payment_exist = $this->Studentgetmodel->check_payment_exist($tpdt[0]->TS_ID);
				$tsdt = $this->Studentgetmodel->get_tetseries_dt_by_id($tpdt[0]->TS_ID);
		        $paperattemptvalid = $this->Studentgetmodel->check_attempt_testpaper($tpid);
				
				if( ((!empty($payment_exist) && $paperattemptvalid) || ($tpdt[0]->TP_PAY_STATUS==1 && $paperattemptvalid)) &&  strtotime($tsdt[0]->TS_END_DATE)>strtotime(date('Y-m-d H:i:s')) ){
    		        $data['tpdt'] = $tpdt;
    		        
        		    $this->load->view('student/conduct_paper/header',$data);
            		$this->load->view('student/conduct_paper/mainpagescript');
            		$this->load->view('student/conduct_paper/mainpage');
            		
            		$this->load->view('student/conduct_paper/instructionscripts');
            		$this->load->view('student/start_paper/startpaperscripts');
            		$this->load->view('student/conduct_paper/footer');
    		    }else{
    		        redirect('student/dashboard');
    		    }
		    }else{
		        redirect('student/dashboard');
		    }
		}else{
		    redirect('student/dashboard');
		}
		
	}
	public function solution()
	{
		$data=array();
		$data['page']='Dashboard';
		$data['subpage']='';
		if(($this->uri->segment(3))!=''){
		    $tpid = url_decode($this->uri->segment(3));
		    $tpdt = $this->Studentgetmodel->get_tetpaper_dt_by_id($tpid);
		   
		    if(!empty($tpdt)){
		        $payment_exist = $this->Studentgetmodel->check_payment_exist($tpdt[0]->TS_ID);
		        $paperattemptvalid = $this->Studentgetmodel->check_attempt_testpaper($tpid);
		        
    		    if((!empty($payment_exist) && !$paperattemptvalid) || ($tpdt[0]->TP_PAY_STATUS==1 && !$paperattemptvalid)){
    		        $data['tpdt'] = $tpdt;
    		         $data['tsdt'] = $this->Studentgetmodel->get_tetseries_dt_by_id($tpdt[0]->TS_ID);
    		         $userid = $this->session->userdata('studentloginid');
    		         $data['tpresult'] = $this->Studentgetmodel->get_testpaper_result($tpdt[0]->TS_ID,$tpid,$userid);
    				 $data['result'] = $this->Studentgetmodel->get_student_result_dt($tpid);
    				 $data['allques'] = $this->Studentgetmodel->get_quesdt_by_id(implode(',',json_decode($tpdt[0]->TP_QUESTIONS)));
        		     $this->load->view('student/solution/solutionheader',$data);
            		 $this->load->view('student/solution/solution');
            		 $this->load->view('student/solution/solutionscripts');
            		 $this->load->view('student/solution/solutionfooter');
    		    }else{
    		        redirect('student/dashboard');
    		    }
		    }else{
		        redirect('student/dashboard');
		    }
		}else{
		    redirect('student/dashboard');
		}
		
	}
	public function start_test()
	{
		$data=array();
		$data['page']='Dashboard';
		$data['subpage']='';
		if(($this->uri->segment(3))!=''){
		    $tpid = url_decode($this->uri->segment(3));
		    $tpdt = $this->Studentgetmodel->get_tetpaper_dt_by_id($tpid);
		    if(!empty($tpid) && !empty($tpdt)){
		        
		        $payment_exist = $this->Studentgetmodel->check_payment_exist($tpdt[0]->TS_ID);
    		    if(!empty($payment_exist)){
    		        $data['tpdt'] = $tpdt;
    		         $data['tsdt'] = $this->Studentgetmodel->get_tetseries_dt_by_id($tpdt[0]->TS_ID);
        		    $this->load->view('student/start_paper/startpaperheader',$data);
        		    $this->load->view('student/start_paper/startpaperscripts');
            		$this->load->view('student/start_paper/startpaper');
            		
            		$this->load->view('student/start_paper/startpaperfooter');
    		    }else{
    		        redirect('student/dashboard');
    		    }
		    }else{
		        redirect('student/dashboard');
		    }
		}else{
		    redirect('student/dashboard');
		}
		
	}

	public function set_cookies(){
	    $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
            	    $fullarr=array();
                    if($_POST['total']>0){
                        for($x=1;$x<=$_POST['total'];$x++){
                            $min=array();
                            $min['quesid']=$_POST['quesid_'.$x];
                            $min['ques_ans']=$_POST['ques_ans_'.$x];
                            $min['quesclass']=$_POST['quesclass_'.$x];
                            array_push($fullarr,$min);
                        }
                    }
                    if(!empty($fullarr)){
                        $this->session->unset_userdata('questiondata_'.$_POST['uri'].'');
                        $data=json_encode($fullarr);
                        $this->session->set_userdata('questiondata_'.$_POST['uri'].'',$data);
                    }
                    $response['message'] = 'ok';
                    json_output($response['status'], $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }	  
    }
    public function get_cookies() {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
            	    $data=$this->session->userdata('questiondata_'.$_GET['tpid'].'');
            	    if($data!=''){
            	        echo $data;
            	    }else{
            	        echo json_encode('');
            	    }
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }	    
    }
    public function submittestpaper()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    if(empty($this->session->userdata('questiondata_'.$_POST['tpid'].''))){
                        $arr = array();
                        $tspapaerdt = json_encode($arr);
                    }else{
                        $tspapaerdt = $this->session->userdata('questiondata_'.$_POST['tpid'].'');
                    }
                    $tpid = url_decode($_POST['tpid']);
                    $rank = rand(200,1000);
                    $data = array(
            		    'USER_ID'=> $this->session->userdata('studentloginid'),
            		    'TP_ID'=> $tpid,
            		    'STD_RESULT'=> $tspapaerdt,
            		    'STD_RESULT_RANK'=> $rank,
						'STD_RESULT_DURATION'=> $_POST['timer'],
						'STD_RESULT_TYPE'=> 1,
            		    'STD_RESULT_TT'=> date('Y-m-d H:i:s'),
            		);
            		$res = $this->Studentpostmodel->submittestpaper($data);
            		if(!empty($res)){
            		    
                        
            		    $this->session->unset_userdata('questiondata_'.$_POST['tpid'].'');
            		    
                        
                        $response['message'] = "ok";
                        $response['data'] = $_POST['tpid'];
            		}else{
            		    $response['message'] = "Something went wrong";
            		}
                    json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
    }
    public function analysis()
	{
		$data=array();
		$data['page']='Dashboard';
		$data['subpage']='';
		if(($this->uri->segment(3))!=''){
		    $tpid = url_decode($this->uri->segment(3));
		    $tpdt = $this->Studentgetmodel->get_tetpaper_dt_by_id($tpid);
		    if(!empty($tpid) && !empty($tpdt)){
		        
		        $payment_exist = $this->Studentgetmodel->check_payment_exist($tpdt[0]->TS_ID);
		        $paperattemptvalid = $this->Studentgetmodel->check_attempt_testpaper($tpid);
		        
    		    if((!empty($payment_exist) && !$paperattemptvalid) || ($tpdt[0]->TP_PAY_STATUS==1 && !$paperattemptvalid)){
    		        $data['tpdt'] = $tpdt;
    		         $data['tsdt'] = $this->Studentgetmodel->get_tetseries_dt_by_id($tpdt[0]->TS_ID);
    		         
    		         $userid = $this->session->userdata('studentloginid');
    				 $data['result'] = $this->Studentgetmodel->get_testpaper_result($tpdt[0]->TS_ID,$tpid,$userid);
        		     $this->load->view('student/analysis/analysisheader',$data);
            		 $this->load->view('student/analysis/analysis');
            		 $this->load->view('student/analysis/analysisscripts');
            		 $this->load->view('student/analysis/analysisfooter');
    		    }else{
    		        redirect('student/dashboard');
    		    }
		    }else{
		        redirect('student/dashboard');
		    }
		}else{
		    redirect('student/dashboard');
		}
		
	}

	//Practice Test Start
	public function practice_test_solution()
	{
		$data=array();
		$data['page']='Dashboard';
		$data['subpage']='';
		if(($this->uri->segment(3))!=''){
		    $tpid = url_decode($this->uri->segment(3));
			$tpdt = $this->Studentgetmodel->get_practice_test_papers_by_id($tpid);
			
		    if(!empty($tpdt)){
				$userid = $this->session->userdata('studentloginid');
		        $payment_exist = $this->Studentgetmodel->check_payment_exist_by_prod_type($userid,6,$tpdt[0]->TS_ID);
		        $paperattemptvalid = $this->Studentgetmodel->check_attempt_practice_testpaper($tpid);
		        
    		    if((!empty($payment_exist) && !$paperattemptvalid) || ($tpdt[0]->TP_PAY_STATUS==1 && !$paperattemptvalid)){
    		        $data['tpdt'] = $tpdt;
    		         $data['tsdt'] = $this->Studentgetmodel->get_practicetest_by_id($tpdt[0]->TS_ID);
    		         $userid = $this->session->userdata('studentloginid');
    		         $data['tpresult'] = $this->Studentgetmodel->get_practicetestpaper_result($tpdt[0]->TS_ID,$tpid,$userid);
    				 $data['result'] = $this->Studentgetmodel->get_practice_test_result($tpid);
    				 $data['allques'] = $this->Studentgetmodel->get_quesdt_by_id(implode(',',json_decode($tpdt[0]->TP_QUESTIONS)));
        		     $this->load->view('student/solution/solutionheader',$data);
            		 $this->load->view('student/solution/solution');
            		 $this->load->view('student/solution/solutionscripts');
            		 $this->load->view('student/solution/solutionfooter');
    		    }else{
    		     	redirect('student/dashboard');
    		    }
		    }else{
		        redirect('student/dashboard');
		    }
		}else{
		    redirect('student/dashboard');
		}
		
	}
	public function pt_instruction()
	{
		$data=array();
		$data['page']='Dashboard';
		$data['subpage']='';
		if(($this->uri->segment(3))!=''){
		    $tpid = url_decode($this->uri->segment(3));
		    $tpdt = $this->Studentgetmodel->get_practice_test_papers_by_id($tpid);
		    if(!empty($tpid) && !empty($tpdt)){
		        
				$payment_exist=$this->Studentgetmodel->check_payment_exist_by_prod_type($this->session->userdata('studentloginid'), 6, $tpdt[0]->TS_ID);
				$tsdt = $this->Studentgetmodel->get_practicetest_by_id($tpdt[0]->TS_ID);
		        $paperattemptvalid = $this->Studentgetmodel->check_attempt_testpaper($tpid);
				
				if( ((!empty($payment_exist) && $paperattemptvalid) || ($tpdt[0]->TP_PAY_STATUS==1 && $paperattemptvalid)) &&  strtotime($tsdt[0]->TS_END_DATE)>strtotime(date('Y-m-d H:i:s')) ){
    		        $data['tpdt'] = $tpdt;
    		        
        		    $this->load->view('student/conduct_paper/header',$data);
            		$this->load->view('student/conduct_paper/mainpagescript');
            		$this->load->view('student/conduct_paper/mainpage');
            		
            		$this->load->view('student/conduct_paper/instructionscripts');
            		$this->load->view('student/start_paper/startpaperscripts');
            		$this->load->view('student/conduct_paper/footer');
    		    }else{
    		        redirect('student/dashboard');
    		    }
		    }else{
		        redirect('student/dashboard');
		    }
		}else{
		    redirect('student/dashboard');
		}
		
	}

	public function submit_ptpaper()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    if(empty($this->session->userdata('questiondata_'.$_POST['tpid'].''))){
                        $arr = array();
                        $tspapaerdt = json_encode($arr);
                    }else{
                        $tspapaerdt = $this->session->userdata('questiondata_'.$_POST['tpid'].'');
                    }
                    $tpid = url_decode($_POST['tpid']);
                    $rank = rand(200,1000);
                    $data = array(
            		    'USER_ID'=> $this->session->userdata('studentloginid'),
            		    'TP_ID'=> $tpid,
            		    'STD_RESULT'=> $tspapaerdt,
            		    'STD_RESULT_RANK'=> $rank,
						'STD_RESULT_DURATION'=> $_POST['timer'],
						'STD_RESULT_TYPE'=> 1,
            		    'STD_RESULT_TT'=> date('Y-m-d H:i:s'),
					);
            		$res = $this->Studentpostmodel->submit_ptpaper($data);
            		if(!empty($res)){
            		    $this->session->unset_userdata('questiondata_'.$_POST['tpid'].'');
            		    $response['message'] = "ok";
                        $response['data'] = $_POST['tpid'];
            		}else{
            		    $response['message'] = "Something went wrong";
            		}
                    json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
    }
}
