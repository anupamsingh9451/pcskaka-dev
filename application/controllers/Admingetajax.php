<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admingetajax extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database('default');
		$this->load->library('email');
		$this->load->model('admin/Authentication_model');
		$this->load->model('admin/Admingetmodel');
		$this->load->helper('json_output_helper');
		$this->load->helper('common_helper');

	}
	public function cronjob() {
		scheduling_msg();
		scheduling_email();
	}
	public function get_admin_dt() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$params = $_REQUEST;
					$adminid = $_GET['id'];
					$response = $this->Admingetmodel->get_admin_dt_model($adminid);
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function getlibrary() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$params = $_REQUEST;
					$response = $this->Admingetmodel->get_all_library($this->input->get('courseid'));
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	public function listlibrary() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					if (isset($_GET['loginid'])) {
						$loginid = $_GET['loginid'];
						$response = $this->Admingetmodel->get_all_librarys();
						$max['data'] = array();
						$i = 1;
						if (!empty($response['data'])) {
							foreach ($response['data'] as $res) {
								$min = array();
								$min['SR_NO'] = $i.'<a class="add-row-subject-collapse">
														<i class="fas fa-lg fa-fw m-r-10 fa-plus-circle" aria-hidden="true"></i>
													</a>';
								$min['COURSE_NAME'] = $res->COURSE_NAME;
								$min['NAME'] = $res->LIB_NAME;
								$sale=0;
								if ($res->IS_SALE_PROD_TYPE!=-1) {
									$sale=1;
									$min['SALE_STATUS'] = '<button type="button" class="btn btn-xs btn-success pad_4">Saled</button>';
								} else {
									$min['SALE_STATUS'] = '<button type="button" class="btn btn-xs btn-danger pad_4">Not Saled</button>';
								}
								$min['LIB_PRICE'] = "-";
								if($res->LIB_PAY_TYPE==1){
									$min['LIB_PRICE'] = $res->LIB_COST;
								}else if($res->LIB_PAY_TYPE==2){
									$min['LIB_PRICE'] = "Free";
								}
								if ($res->LIB_STATUS == 1) {
									$stat = '<button type="button" class="btn btn-xs btn-success pad_4">ACTIVE</button>';
								} else {
									$stat = '<button type="button" class="btn btn-xs btn-danger pad_4">INACTIVE</button>';
								}
								$min['LIB_STATUS'] = $stat;
								$image="";
								if(!empty($res->LIB_IMG)){
									$image=base_url($res->LIB_IMG);
								}
								$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    								<a href="#" data-toggle="dropdown" class="btn btn-xs pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    								<ul class="dropdown-menu">
									<li><a href="javascript:void(0);" class="libraryedit" attr-pay_type="' . $res->LIB_PAY_TYPE . '" attr-libimg="' . $image . '" attr-status="' . $res->LIB_STATUS . '" attr-libid="' . $res->LIB_ID . '"  attr-libnm="' . $res->LIB_NAME . '" attr-libcost="' . $res->LIB_COST . '" attr-courseid="' . $res->COURSE_ID . '" attr-popular="' . $res->COURSE_POPULAR . '">Edit</a></li>
									<li><a href="javascript:void(0);" class="librarysale" attr-libid="'.$res->LIB_ID.'"  attr-libnm="' . $res->LIB_NAME . '" attr-libcost="'.$res->LIB_COST.'"  attr-libpaystatus="'.$res->LIB_PAY_TYPE.'" attr-salestatus="'.$sale.'">Sale</a></li>
    								</ul>
    							</div>';

								array_push($max['data'], $min);
								$i++;
							}
						}
						$i = $i - 1;
						$max['message'] = 'Total ' . $i . ' records sended successfully';
						$max['status'] = 200;
						$response['status'] = 200;
						json_output($response['status'], $max);
					} else {
						$response['status'] = 200;
						$response['message'] = 'Invalid query string passed';
						json_output($response['status'], $response);
					}
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}

			}
		}
	}
	public function subjectlist() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					if (isset($_GET['loginid'])) {
						$loginid = $_GET['loginid'];
						$response = $this->Admingetmodel->get_all_subject();
						$max['data'] = array();
						$i = 1;
						if (!empty($response)) {
							foreach ($response as $res) {
								$min = array();
								$min['SR_NO'] = $i.'<a class="add-row-book-collapse">
												<i class="fas fa-lg fa-fw m-r-10 fa-plus-circle" aria-hidden="true"></i>
											</a>';;
								$min['SUB_NAME'] = $res->SUB_NAME;
								$min['SUB_COST'] = $res->SUB_COST;
								$min['LIB_NAME'] = $res->LIB_NAME;
								$sale=0;
								if ($res->IS_SALE_PROD_TYPE!=-1) {
									$sale=1;
									$min['SALE_STATUS'] = '<button type="button" class="btn btn-xs btn-success pad_4">Saled</button>';
								} else {
									$min['SALE_STATUS'] = '<button type="button" class="btn btn-xs btn-danger pad_4">Not Saled</button>';
								}
								$min['SUB_CREATED_AT'] = date('d-m-Y', strtotime($res->SUB_CREATED_AT));
								if ($res->SUB_STATUS == 1) {
									$stat = '<button type="button" class="btn btn-xs btn-success pad_4">ACTIVE</button>';
								} else {
									$stat = '<button type="button" class="btn btn-xs btn-danger pad_4">INACTIVE</button>';
								}
								$min['SUB_STATUS'] = $stat;
								$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    								<a href="#" data-toggle="dropdown" class="btn btn-xs pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    								<ul class="dropdown-menu">
    									<li><a href="javascript:void(0);" class="subedit" attr-libid="' . $res->LIB_ID . '" attr-status="' . $res->SUB_STATUS . '" attr-subnm="' . $res->SUB_NAME . '" attr-subid="' . $res->SUB_ID . '" >Edit</a></li>
    								</ul>
    							</div>';

								array_push($max['data'], $min);
								$i++;
							}
						}
						$i = $i - 1;
						$max['message'] = 'Total ' . $i . ' records sended successfully';
						$max['status'] = 200;
						$response['status'] = 200;
						json_output($response['status'], $max);
					} else {
						$response['status'] = 200;
						$response['message'] = 'Invalid query string passed';
						json_output($response['status'], $response);
					}
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	public function get_subject_by_libid() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$params = $_REQUEST;
					$subid = $_GET['subid'];
					$response = $this->Admingetmodel->get_subject_by_libid($subid);
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function listbook() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$columns = array(
						0 => 'SR_NO',
						1 => 'BOOK_NAME',
						2 => 'SUB_NAME',
						3 => 'LIB_NAME',
						4 => 'COURSE_NAME',
						5 => 'BOOK_CHAPTER_NOS',
						6 => 'PENDING_QUES',
						7 => 'BOOK_CREATED_AT',
						8 => 'BOOK_STATUS',
						9 => 'TOOLS',
					);
					$limit = $this->input->get('length');
					$start = $this->input->get('start');
					$order = $columns[$this->input->get('order')[0]['column']];
					$dir = $this->input->get('order')[0]['dir'];

					$subid = $_GET['subid'];
					if (isset($subid)) {
						$totalData = count_all_book($subid);
						$totalFiltered = $totalData;
						if (empty($this->input->get('search')['value'])) {
							$posts = get_all_book($limit, $start, $order, $dir, $subid);
						} else {
							$search = $this->input->get('search')['value'];
							$posts = get_all_book_search($limit, $start, $search, $order, $dir, $subid);
							$totalFiltered = get_all_book_search_count($search, $subid);
						}
					}
					$max = array();
					$i = $start + 1;
					if (!empty($posts)) {
						foreach ($posts as $res) {
							$min['SR_NO'] = $i;
							$chapterdt = $this->Admingetmodel->get_all_chapter_by_bookid($res->BOOK_ID);
							$min['BOOK_NAME'] = $res->BOOK_NAME;
							$min['SUB_NAME'] = $res->SUB_NAME;
							$min['COURSE_NAME'] = $res->COURSE_NAME;
							$min['LIB_NAME'] = $res->LIB_NAME;
							$min['BOOK_CHAPTER_NOS'] = $res->BOOK_CHAPTER_NOS;
							$min['PENDING_QUES'] = $this->Admingetmodel->get_pending_ques($chapterdt[0]->CHAPTER_ID);
							$min['BOOK_CREATED_AT'] = date('d-m-Y', strtotime($res->BOOK_CREATED_AT));
							if ($res->BOOK_STATUS == 1) {
								$stat = '<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
							} else {
								$stat = '<button type="button" class="btn btn-danger pad_4">INACTIVE</button>';
							}
							$min['BOOK_STATUS'] = $stat;
							$chaptedt = $this->Admingetmodel->get_all_chapter_by_bookid($res->BOOK_ID);
							$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    							<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    							<ul class="dropdown-menu">
    								<li><a href="javascript:void(0);" class="bookedit" attr-pay-type="' . $res->BOOK_PAY_TYPE . '" attr-cost="' . $res->BOOK_COST . '" attr-chapterno="' . $res->BOOK_CHAPTER_NOS . '" attr-bookid="' . $res->BOOK_ID . '" attr-bookimg="' . $res->BOOK_IMG . '" attr-status="' . $res->BOOK_STATUS . '" attr-booknm="' . $res->BOOK_NAME . '" attr-subid="' . $res->SUB_ID . '" >Edit</a></li>
    							     <li><a href="' . base_url() . 'ratan/question/' . $chaptedt[0]->CHAPTER_ID . '">Question</a></li>


    							</ul>
    						</div>';
							// <li><a href="'.base_url().'ratan/chapter/'.$res->BOOK_ID.'">Chapter</a></li>
							array_push($max, $min);
							$i++;
						}
					}
					$json_data = array(
						"draw" => intval($this->input->post('draw')),
						"recordsTotal" => intval($totalData),
						"recordsFiltered" => intval($totalFiltered),
						"data" => $max,
					);
					echo json_output(200, $json_data);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}

	public function get_book_by_subid() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$params = $_REQUEST;
					$subid = $_GET['subid'];
					$response = $this->Admingetmodel->get_book_by_subid($subid);
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function listchapter() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {

					$response = $this->Admingetmodel->get_all_chapter_by_bookid($_GET['bookid']);
					$max['data'] = array();
					$i = 1;
					if (!empty($response)) {
						foreach ($response as $res) {
							$min = array();
							$min['SR_NO'] = $i;
							$min['CHAPTER_NAME'] = $res->CHAPTER_NAME;
							$min['BOOK_NAME'] = $res->BOOK_NAME . "<br>(" . $res->SUB_NAME . ")";
							$min['SUB_NAME'] = $res->SUB_NAME;
							$min['LIB_NAME'] = $res->LIB_NAME;
							$min['CHAPTER_QUE_NOS'] = $res->CHAPTER_QUE_NOS;
							$min['CHAPTER_DESC'] = $res->CHAPTER_DESC;
							$min['CHAPTER_CREATED_AT'] = date('d-m-Y', strtotime($res->CHAPTER_CREATED_AT));
							if ($res->CHAPTER_STATUS == 1) {
								$stat = '<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
							} else {
								$stat = '<button type="button" class="btn btn-danger pad_4">INACTIVE</button>';
							}
							$min['CHAPTER_STATUS'] = $stat;
							$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    							<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    							<ul class="dropdown-menu">
    								<li><a href="javascript:void(0);" class="chapteredit" attr-quesno="' . $res->CHAPTER_QUE_NOS . '" attr-chapterid="' . $res->CHAPTER_ID . '" attr-status="' . $res->CHAPTER_STATUS . '" attr-chapternm="' . $res->CHAPTER_NAME . '" attr-bookid="' . $res->BOOK_ID . '" >Edit</a></li>
    							    <li><a href="' . base_url() . 'ratan/question/' . $res->CHAPTER_ID . '">Question</a></li>
    							</ul>
    						</div>';

							array_push($max['data'], $min);
							$i++;
						}
					}
					$i = $i - 1;
					$max['message'] = 'Total ' . $i . ' records sended successfully';
					$max['status'] = 200;
					$response['status'] = 200;
					json_output($response['status'], $max);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}

	public function listquestion() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$columns = array(
						0 => 'SR_NO',
						1 => 'QUES',
						2 => 'QUES_CREATED_BY',
						3 => 'QUES_CREATED_AT',
						4 => 'QUES_EDITED_BY',
						5 => 'QUES_APPROVED_BY',
						6 => 'QUES_DIFFICULTY_LEVEL',
						7 => 'QUES_STATUS',
						8 => 'TOOLS',
					);
					$limit = $this->input->get('length');
					$start = $this->input->get('start');
					$order = $columns[$this->input->get('order')[0]['column']];
					$dir = $this->input->get('order')[0]['dir'];
					if (isset($_GET['userid']) && isset($_GET['bookids'])) {
						$bookseg = '';
						$chapterids = $this->Admingetmodel->get_chapterids_by_booksid($_GET['bookids']);
						$totalData = count_all_ques($chapterids, $_GET['userid'], $_GET['level'], $_GET['status'], $_GET['approve_by']);
						$totalFiltered = $totalData;
						if (empty($this->input->get('search')['value'])) {
							$posts = get_all_ques($limit, $start, $order, $dir, $chapterids, $_GET['userid'], $_GET['level'], $_GET['status'], $_GET['approve_by']);
						} else {
							$search = $this->input->get('search')['value'];
							$posts = get_all_ques_search($limit, $start, $search, $order, $dir, $chapterids, $_GET['userid'], $_GET['level'], $_GET['status'], $_GET['approve_by']);
							$totalFiltered = get_all_ques_search_count($search, $chapterids, $_GET['userid'], $_GET['level'], $_GET['status'], $_GET['approve_by']);
						}
					}
					if (isset($_GET['chapterid'])) {
						$bookseg = '/' . $_GET['chapterid'];
						$totalData = count_all_ques($_GET['chapterid']);
						$totalFiltered = $totalData;
						if (empty($this->input->get('search')['value'])) {
							$posts = get_all_ques($limit, $start, $order, $dir, $_GET['chapterid']);
						} else {
							$search = $this->input->get('search')['value'];
							$posts = get_all_ques_search($limit, $start, $search, $order, $dir, $_GET['chapterid']);
							$totalFiltered = get_all_ques_search_count($search, $_GET['chapterid']);
						}
					}
					$max = array();
					$i = $start + 1;
					if (!empty($posts)) {
						foreach ($posts as $res) {
							$min = array();
							$min['SR_NO'] = $i;
							if ($res->QUES_TYPE == 1) {
								$ques = $res->QUES;
							} else {
								$ques = json_decode($res->QUES)->QUESTION;
							}

							// 		if(strlen($ques)>=102){
							// 		    $ques = substr($ques, 0, 102)."...";
							// 		}
							$min['QUES'] = $ques;
							$level = $this->Admingetmodel->get_difficulty_level($res->QUES_DIFFICULTY_LEVEL);
							$min['QUES_DIFFICULTY_LEVEL'] = $level;

							$userrole1 = $this->Admingetmodel->get_user_role_by_id($res->QUES_CREATED_BY);
							$min['QUES_CREATED_BY'] = $userrole1[0]->USER_FNAME . ' ' . $userrole1[0]->USER_LNAME . '<br><span style="text-align:center">(' . $userrole1[0]->USER_ROLE_NAME . ')</span>';

							$queslog = $this->Admingetmodel->get_ques_log_by_quesid($res->QUES_ID);
							if (!empty($queslog)) {
								$min['QUES_EDITED_BY'] = $queslog[0]->USER_FNAME . ' ' . $queslog[0]->USER_LNAME . '<br><span style="text-align:center">(' . $queslog[0]->USER_ROLE_NAME . ')</span>';
							} else {
								$min['QUES_EDITED_BY'] = '--';
							}

							$userrole2 = $this->Admingetmodel->get_user_role_by_id($res->QUES_APPROVED_BY);
							if (!empty($userrole2)) {
								$min['QUES_APPROVED_BY'] = $userrole2[0]->USER_FNAME . ' ' . $userrole2[0]->USER_LNAME . '<br><span style="text-align:center">(' . $userrole2[0]->USER_ROLE_NAME . ')</span>';
							} else {
								$min['QUES_APPROVED_BY'] = '--';
							}

							$min['QUES_CREATED_AT'] = date('d-m-Y', strtotime($res->QUES_CRATED_AT));
							if ($res->QUES_STATUS == 1) {
								$str = '';
								$stat = '<button style="height: 25px;padding: 0 5px 0 5px;font-size: 11px;" type="button" class="btn btn-success pad_4">ACTIVE</button>';
							} else if ($res->QUES_STATUS == 3) {
								$str = '<li><a class="deleteques" attr-quesid="' . $res->QUES_ID . '" href="javascript:void(0)">Delete</a></li>';
								$stat = '<button type="button" style="height: 25px;padding: 0 5px 0 5px;font-size: 11px;" class="btn btn-warning pad_4">HOLD</button>';
							} else if ($res->QUES_STATUS == 4) {
								$str = '<li><a class="deleteques" attr-quesid="' . $res->QUES_ID . '" href="javascript:void(0)">Delete</a></li>';
								$stat = '<button type="button" style="height: 25px;padding: 0 5px 0 5px;font-size: 11px;" class="btn btn-danger pad_4">NFS</button>';
							} else {
								$str = '<li><a class="deleteques" attr-quesid="' . $res->QUES_ID . '" href="javascript:void(0)">Delete</a></li>';
								$stat = '<button type="button" style="height: 25px;padding: 0 5px 0 5px;font-size: 11px;" class="btn btn-danger pad_4">INACTIVE</button>';
							}
							$min['QUES_STATUS'] = $stat;

							$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    							<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    							<ul class="dropdown-menu">
    								<li><a href="' . base_url() . 'ratan/question/edit/' . $res->QUES_ID . '/' . $_GET['uriseg'] . '' . $bookseg . '">Edit</a></li>
    							    <li><a class="view_ques_modal" href="javascript:void(0);" attr-quesid="' . $res->QUES_ID . '">View</a></li>
    							    <li><a class="edit_ques_on_modal" href="javascript:void(0);" attr-quesid="' . $res->QUES_ID . '">Edit on modal</a></li>
    							    ' . $str . '
    							</ul>
    						</div>';
							// 		<li><a href="'.base_url().'ratan/question/view/'.$res->QUES_ID.'/'.$_GET['uriseg'].''.$bookseg.'">View</a></li>

							array_push($max, $min);
							$i++;
						}
					}
					$json_data = array(
						"draw" => intval($this->input->post('draw')),
						"recordsTotal" => intval($totalData),
						"recordsFiltered" => intval($totalFiltered),
						"data" => $max,
					);
					echo json_output(200, $json_data);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	public function get_all_sub() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$response = $this->Admingetmodel->get_all_subjects();
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function get_question_by_bookid() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$bookid = $_GET['bookid'];
					$tsid = $_GET['tsid'];
					$tsdt = $this->Admingetmodel->get_testseriesdt_by_id($tsid);
					$tsquesarr = '';
					if ($tsdt[0]->TS_INSERTED_QUES != '') {
						$tsquesarr = json_decode($tsdt[0]->TS_INSERTED_QUES);
					}

					$ques_type = $_GET['ques_type'];
					$chapterid = $this->Admingetmodel->get_chapterids_by_booksid($bookid);

					$result = $this->Admingetmodel->get_all_ques_by_chapterid_questype($chapterid, $ques_type);
					$max = array();
					if (!empty($result)) {
						foreach ($result as $res) {

							if ($tsquesarr != '') {
								$min = array();
								if (!in_array($res->QUES_ID, $tsquesarr)) {
									$min['QUES_ID'] = $res->QUES_ID;
									if ($res->QUES_TYPE == 1) {
										$ques = substr($res->QUES, 0, 30);
										$min['QUES'] = $small = $ques . '...';
									} else {
										$ques = substr(json_decode($res->QUES)->QUESTION, 0, 30);
										$min['QUES'] = $small = $ques . '...';
									}
									array_push($max, $min);
								}
							} else {
								$min = array();
								$min['QUES_ID'] = $res->QUES_ID;
								if ($res->QUES_TYPE == 1) {
									$ques = substr($res->QUES, 0, 30);
									$min['QUES'] = $small = $ques . '...';
								} else {
									$ques = substr(json_decode($res->QUES)->QUESTION, 0, 30);
									$min['QUES'] = $small = $ques . '...';
								}
								array_push($max, $min);
							}

						}
					}
					$response['message'] = 'ok';
					$response['data'] = $max;
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function testserieslist() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$response = $this->Admingetmodel->get_all_testseries();
					$max['data'] = array();
					$i = 1;
					if (!empty($response)) {
						foreach ($response as $res) {
							$min = array();
							$min['SR_NO'] = $i;
							$min['TS_NAME'] = $res->TS_NAME;
							if (!empty($res->TS_TEST_PAPERS)) {
								$tspaper_size = sizeof(json_decode($res->TS_TEST_PAPERS));
							} else {
								$tspaper_size = 0;
							}
							$min['TS_TEST_PAPER_NOS'] = '<span style="font-size:14px">' . $tspaper_size . ' / ' . $res->TS_TEST_PAPER_NOS . '</span>';
							// 		$min['TS_QUESTION_NOS']=$res->TS_QUESTION_NOS;
							$min['TS_CREATED_AT'] = date('d-m-Y', strtotime($res->TS_CREATED_AT));
							if ($res->TS_STATUS == 1) {
								$stat = '<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
							} else {
								$stat = '<button type="button" class="btn btn-danger pad_4">INACTIVE</button>';
							}
							$min['TS_STATUS'] = $stat;
							$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    							<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    							<ul class="dropdown-menu">
    								<li><a href="javascript:void(0);" class="testseriesedit"  attr-tsid="' . $res->TS_ID . '"  >Edit</a></li>
    							    <li><a href="' . base_url() . 'ratan/testpaper/' . $res->TS_ID . '">Test Paper</a></li>
    							</ul>
    						</div>';
							array_push($max['data'], $min);
							$i++;
						}
					}
					$i = $i - 1;
					$max['message'] = 'Total ' . $i . ' records sended successfully';
					$max['status'] = 200;
					$response['status'] = 200;
					json_output($response['status'], $max);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}

	public function practicetest_list() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$response = $this->Admingetmodel->get_all_practicetest();
					$max['data'] = array();
					$i = 1;
					if (!empty($response)) {
						foreach ($response as $res) {
							$min = array();
							$min['SR_NO'] = $i;
							$min['TS_NAME'] = $res->TS_NAME;
							if (!empty($res->TS_TEST_PAPERS)) {
								$tspaper_size = sizeof(json_decode($res->TS_TEST_PAPERS));
							} else {
								$tspaper_size = 0;
							}
							$min['TS_TEST_PAPER_NOS'] = '<span style="font-size:14px">' . $tspaper_size . ' / ' . $res->TS_TEST_PAPER_NOS . '</span>';
							// 		$min['TS_QUESTION_NOS']=$res->TS_QUESTION_NOS;
							$min['TS_CREATED_AT'] = date('d-m-Y', strtotime($res->TS_CREATED_AT));
							if ($res->TS_STATUS == 1) {
								$stat = '<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
							} else {
								$stat = '<button type="button" class="btn btn-danger pad_4">INACTIVE</button>';
							}
							$min['TS_STATUS'] = $stat;
							$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    							<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    							<ul class="dropdown-menu">
    								<li><a href="javascript:void(0);" class="testseriesedit"  attr-tsid="' . $res->TS_ID . '"  >Edit</a></li>
    							    <li><a href="' . base_url() . 'ratan/practicetestpaper/' . $res->TS_ID . '">Practice Tests</a></li>
    							</ul>
    						</div>';
							array_push($max['data'], $min);
							$i++;
						}
					}
					$i = $i - 1;
					$max['message'] = 'Total ' . $i . ' records sended successfully';
					$max['status'] = 200;
					$response['status'] = 200;
					json_output($response['status'], $max);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function listtestpaper() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$tsid = $_GET['tsid'];
					$tsdt = $this->Admingetmodel->get_testseriesdt_by_id($tsid);
					if ($tsdt[0]->TS_TEST_PAPERS != '') {
						$tpid = implode(',', json_decode($tsdt[0]->TS_TEST_PAPERS));
						$response = $this->Admingetmodel->get_all_testpaper_by_id($tpid);
					} else {
						$response = '';
					}
					$max['data'] = array();
					$i = 1;
					if (!empty($response)) {
						foreach ($response as $res) {
							$min = array();
							$min['SR_NO'] = $i;
							$min['TP_NAME'] = $res->TP_NAME;
							// 		$min['SUB_NAME']=$res->SUB_NAME;
							$min['QUES_NO'] = '<span style="font-size:14px;">' . sizeof(json_decode($res->TP_QUESTIONS)) . ' / ' . $tsdt[0]->TS_QUESTION_NOS;

							$min['TP_CREATED_AT'] = date('d-m-Y', strtotime($res->TP_CREATED_AT));
							$min['TP_START_DATE'] = date('d-m-Y', strtotime($res->TP_START_DATE));
							if ($res->TP_STATUS == 1) {
								$stat = '<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
							} else {
								$stat = '<button type="button" class="btn btn-danger pad_4">PENDING</button>';
							}
							$min['TP_STATUS'] = $stat;
							if ($res->TP_APPROVED_BY != '' && $res->TP_STATUS == 1) {
								$stat1 = '<button type="button" class="btn btn-success pad_4">Published</button>';
							} else {
								$stat1 = '<button type="button" class="btn btn-warning pad_4">Unpublished</button>';
							}
							$min['TP_PUBLISHED'] = $stat1;

							$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    							<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    							<ul class="dropdown-menu">
    								<li><a href="' . base_url() . 'ratan/testpaper/testpaperedit/' . $res->TP_ID . '/' . $tsid . '" class="testpaperedit" attr-testpaperid="' . $res->TP_ID . '" attr-status="' . $res->TP_STATUS . '"  >Edit</a></li>
    								<li><a href="' . base_url() . 'ratan/testpaper/testpaperview/' . $res->TP_ID . '/' . $tsid . '" class="testpaperedit" attr-testpaperid="' . $res->TP_ID . '" attr-status="' . $res->TP_STATUS . '"  >View</a></li>
    							</ul>
    						</div>';
							array_push($max['data'], $min);
							$i++;
						}
					}
					$i = $i - 1;
					$max['message'] = 'Total ' . $i . ' records sended successfully';
					$max['status'] = 200;
					// $response['status']=200;

					json_output($max['status'], $max);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}

	public function list_practicetestpaper() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$tsid = $_GET['tsid'];
					$tsdt = $this->Admingetmodel->get_practicetestdt_by_id($tsid);
					if ($tsdt[0]->TS_TEST_PAPERS != '') {
						$tpid = implode(',', json_decode($tsdt[0]->TS_TEST_PAPERS));
						$response = $this->Admingetmodel->get_all_practicetestpaper_by_id($tpid);
					} else {
						$response = '';
					}
					$max['data'] = array();
					$i = 1;
					if (!empty($response)) {
						foreach ($response as $res) {
							$min = array();
							$min['SR_NO'] = $i;
							$min['TP_NAME'] = $res->TP_NAME;
							// 		$min['SUB_NAME']=$res->SUB_NAME;
							$min['QUES_NO'] = '<span style="font-size:14px;">' . sizeof(json_decode($res->TP_QUESTIONS)) . ' / ' . $tsdt[0]->TS_QUESTION_NOS;

							$min['TP_CREATED_AT'] = date('d-m-Y', strtotime($res->TP_CREATED_AT));
							$min['TP_START_DATE'] = date('d-m-Y', strtotime($res->TP_START_DATE));
							if ($res->TP_STATUS == 1) {
								$stat = '<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
							} else {
								$stat = '<button type="button" class="btn btn-danger pad_4">PENDING</button>';
							}
							$min['TP_STATUS'] = $stat;
							if ($res->TP_APPROVED_BY != '' && $res->TP_STATUS == 1) {
								$stat1 = '<button type="button" class="btn btn-success pad_4">Published</button>';
							} else {
								$stat1 = '<button type="button" class="btn btn-warning pad_4">Unpublished</button>';
							}
							$min['TP_PUBLISHED'] = $stat1;

							$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    							<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    							<ul class="dropdown-menu">
    								
    								<li><a href="' . base_url() . 'ratan/practicetestpaper/testpaperview/' . $res->TP_ID . '/' . $tsid . '" class="testpaperedit" attr-testpaperid="' . $res->TP_ID . '" attr-status="' . $res->TP_STATUS . '"  >View</a></li>
    							</ul>
    						</div>';
							array_push($max['data'], $min);
							$i++;
						}
					}
					$i = $i - 1;
					$max['message'] = 'Total ' . $i . ' records sended successfully';
					$max['status'] = 200;
					// $response['status']=200;

					json_output($max['status'], $max);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function gettestpapermodal() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$tpid = $_GET['tpid'];
					$tpdt = $this->Admingetmodel->get_all_testpaper_by_id($tpid);
					$tpquesarr = json_decode($tpdt[0]->TP_QUESTIONS);
					$quesdt = $this->Admingetmodel->get_quesdt_by_id($tpquesarr[0]);
					$data['lib'] = $this->Admingetmodel->get_all_library();
					$data['quesdt'] = $quesdt;
					$data['tpdt'] = $tpdt;
					echo $response = json_encode($this->load->view('admin/test_paper/edittestpapermodal', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}

			}
		}
	}
	public function get_book_html() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$subid = $_GET['subid'];
					if (isset($_GET['tsid'])) {
						$data['tsid'] = $_GET['tsid'];
					} else {
						$data['tsid'] = '';
					}
					$data['subid'] = $subid;
					$data['bookdt'] = $this->Admingetmodel->get_book_by_subid($subid);
					echo $response = json_encode($this->load->view('admin/test_paper/bookhtml', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
    }
    public function get_practice_book_html() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$subid = $_GET['subid'];
					if (isset($_GET['tsid'])) {
						$data['tsid'] = $_GET['tsid'];
					} else {
						$data['tsid'] = '';
					}
					$data['subid'] = $subid;
					$data['bookdt'] = $this->Admingetmodel->get_book_by_subid($subid);
					echo $response = json_encode($this->load->view('admin/practice_test_paper/bookhtml', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	public function get_priority_html() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$subid = $_GET['subid'];
					$bookid = $_GET['bookid'];
					$data['high_level'] = $_GET['high_level'];
					$data['medium_level'] = $_GET['medium_level'];
					$data['low_level'] = $_GET['low_level'];
					$data['subid'] = $subid;
					$data['bookid'] = $bookid;
					// $data['bookdt'] = $this->Admingetmodel->get_book_by_subid($subid);
					echo $response = json_encode($this->load->view('admin/test_paper/priorityhtml', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function get_question_html() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$subid = $_GET['subid'];
					$bookid = $_GET['bookid'];
					$priorityname = $_GET['priorityname'];
					$data['subid'] = $subid;
					$data['bookid'] = $bookid;
					$data['priorityname'] = $priorityname;
					$data['tsid'] = $_GET['tsid'];
					if ($priorityname == 'high') {
						$level = 3;
					} else if ($priorityname == 'medium') {
						$level = 2;
					} else {
						$level = 1;
					}
					$questype = 2; /*For objective Type*/
					$addedquesdt = $this->Admingetmodel->get_testseriesdt_by_id($_GET['tsid']);
					$addedquesarr = json_decode($addedquesdt[0]->TS_INSERTED_QUES);

					$obj = '';
					$quesdt = $this->Admingetmodel->get_question_by_subid_bookid_level($subid, $bookid, $level, $questype);
					$max = array();
					foreach ($quesdt as $quesdts) {
						if ($addedquesdt[0]->TS_INSERTED_QUES == '') {
							$min = array();
							$min['QUES_ID'] = $quesdts->QUES_ID;
							$min['QUES'] = $quesdts->QUES;
							$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
							array_push($max, $min);
						} else {
							if (!in_array($quesdts->QUES_ID, $addedquesarr)) {
								$min = array();
								$min['QUES_ID'] = $quesdts->QUES_ID;
								$min['QUES'] = $quesdts->QUES;
								$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
								array_push($max, $min);
							}
						}
					}
					if (!empty($max)) {
						$obj = convertToObject($max);
					}
					$data['quesdt'] = $obj;
					echo $response = json_encode($this->load->view('admin/test_paper/questionhtml', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function get_more_ques_by_allbooks() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$subid = $_GET['subid'];
					$bookid = $_GET['bookid'];
					$leveltype = $_GET['leveltype'];
					$bookids = $this->Admingetmodel->get_bookids_by_subid($subid);
					$obj = '';
					if (!empty($bookids)) {
						$newbookids = implode(',', array_diff(explode(',', $bookids), explode(',', $bookid)));
						if ($newbookids != '') {
							$chapterids = $this->Admingetmodel->get_chapterids_by_booksid($newbookids);
							$questype = 2; /*For objective Type*/
							$quesdt = $this->Admingetmodel->get_all_ques_by_chapterid_difflevel($chapterids, $leveltype, $questype);

							$addedquesdt = $this->Admingetmodel->get_testseriesdt_by_id($_GET['tsid']);
							$addedquesarr = json_decode($addedquesdt[0]->TS_INSERTED_QUES);
							$max = array();
							foreach ($quesdt as $quesdts) {
								if ($addedquesdt[0]->TS_INSERTED_QUES == '') {
									$min = array();
									$min['QUES_ID'] = $quesdts->QUES_ID;
									$min['QUES'] = $quesdts->QUES;
									$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
									array_push($max, $min);
								} else {
									if (!in_array($quesdts->QUES_ID, $addedquesarr)) {
										$min = array();
										$min['QUES_ID'] = $quesdts->QUES_ID;
										$min['QUES'] = $quesdts->QUES;
										$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
										array_push($max, $min);
									}
								}
							}
							$obj = convertToObject($max);
						}
					}
					$response['data'] = $obj;
					$response['message'] = 'ok';
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function get_more_ques_by_allsub() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					// $subid = $_GET['subid'];
					$bookid = $_GET['bookid'];
					$leveltype = $_GET['leveltype'];
					$subids = $this->Admingetmodel->get_all_subids();
					$bookids = $this->Admingetmodel->get_bookids_by_subid($subids);
					$obj = '';
					if (!empty($bookids)) {
						$newbookids = implode(',', array_diff(explode(',', $bookids), explode(',', $bookid)));
						$chapterids = $this->Admingetmodel->get_chapterids_by_booksid($newbookids);
						$questype = 2; /*For objective Type*/
						$quesdt = $this->Admingetmodel->get_all_ques_by_chapterid_difflevel($chapterids, $leveltype, $questype);

						$addedquesdt = $this->Admingetmodel->get_testseriesdt_by_id($_GET['tsid']);
						$addedquesarr = json_decode($addedquesdt[0]->TS_INSERTED_QUES);
						$max = array();
						foreach ($quesdt as $quesdts) {
							if ($addedquesdt[0]->TS_INSERTED_QUES == '') {
								$min = array();
								$min['QUES_ID'] = $quesdts->QUES_ID;
								$min['QUES'] = $quesdts->QUES;
								$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
								array_push($max, $min);
							} else {
								if (!in_array($quesdts->QUES_ID, $addedquesarr)) {
									$min = array();
									$min['QUES_ID'] = $quesdts->QUES_ID;
									$min['QUES'] = $quesdts->QUES;
									$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
									array_push($max, $min);
								}
							}
						}
						$obj = convertToObject($max);
					}
					$response['data'] = $obj;
					$response['message'] = 'ok';
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}

	public function get_tppreview_modalhtml() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$quesid = $_GET['quesid'];
					$data['quesid'] = $_GET['quesid'];
					$data['tpname'] = $_GET['tpname'];
					$data['fstartdate'] = $_GET['fstartdate'];
					$data['fenddate'] = $_GET['fenddate'];
					$data['fcreateddate'] = $_GET['fcreateddate'];
					$data['tsid'] = $_GET['tsid'];
					$data['fdesc'] = $_GET['fdesc'];

					$data['quesdt'] = $this->Admingetmodel->get_quesdt_by_id($quesid);
					echo $response = json_encode($this->load->view('admin/test_paper/previewtestpaper', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}

	public function get_prac_testppreview_modalhtml() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$quesid = $_GET['quesid'];
					$data['quesid'] = $_GET['quesid'];
					$data['tpname'] = $_GET['tpname'];
					$data['fstartdate'] = $_GET['fstartdate'];
					$data['fenddate'] = $_GET['fenddate'];
					$data['fcreateddate'] = $_GET['fcreateddate'];
					$data['tsid'] = $_GET['tsid'];
					$data['fdesc'] = $_GET['fdesc'];

					$data['quesdt'] = $this->Admingetmodel->get_quesdt_by_id($quesid);
					echo $response = json_encode($this->load->view('admin/practice_test_paper/previewtestpaper', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}

	public function get_question_html_new() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$subid = $_GET['subid'];
					$bookid = $_GET['bookid'];
					$quesno = $_GET['quesno'];
					$priorityname = $_GET['priorityname'];
					$data['subid'] = $subid;
					$data['bookid'] = $bookid;
					$data['priorityname'] = $priorityname;
					$data['tsid'] = $_GET['tsid'];
					if ($priorityname == 'high') {
						$level = 3;
					} else if ($priorityname == 'medium') {
						$level = 2;
					} else {
						$level = 1;
					}
					$questype = 2; /*For objective Type*/
					$obj = '';
					$quesdt = $this->Admingetmodel->get_remain_question_by_subid_bookid_level($subid, $bookid, $level, $questype, $_GET['tsid']);
					if (!empty($quesdt) && $quesno > 0) {
						$maxsize = sizeof($quesdt);
						$divide = floor($maxsize / $quesno);

						$max = array();
						$j = 0;
						foreach ($quesdt as $i => $quesdts) {

							if ($i == $j) {
								// print_r($i."==".$j."        ");
								$min = array();
								$min['QUES_ID'] = $quesdts->QUES_ID;
								$min['QUES'] = $quesdts->QUES;
								$min['SUB_NAME'] = $quesdts->SUB_NAME;
								$min['BOOK_NAME'] = $quesdts->BOOK_NAME;
								$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
								array_push($max, $min);
								$j = $j + $divide;
							}
						}
						$max = array_slice($max, 0, $quesno, true);
						$obj = convertToObject($max);
					}
					$data['quesdt'] = $obj;
					echo $response = json_encode($this->load->view('admin/test_paper_new/questionhtml', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}

    public function get_practice_question_html_new() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$subid = $_GET['subid'];
					$bookid = $_GET['bookid'];
					$quesno = $_GET['quesno'];
					$priorityname = $_GET['priorityname'];
					$data['subid'] = $subid;
					$data['bookid'] = $bookid;
					$data['priorityname'] = $priorityname;
					$data['tsid'] = $_GET['tsid'];
					if ($priorityname == 'high') {
						$level = 3;
					} else if ($priorityname == 'medium') {
						$level = 2;
					} else {
						$level = 1;
					}
					$questype = 2; /*For objective Type*/
					$obj = '';
					$quesdt = $this->Admingetmodel->get_remain_practice_question_by_subid_bookid_level($subid, $bookid, $level, $questype, $_GET['tsid']);
					if (!empty($quesdt) && $quesno > 0) {
						$maxsize = sizeof($quesdt);
						$divide = floor($maxsize / $quesno);

						$max = array();
						$j = 0;
						foreach ($quesdt as $i => $quesdts) {

							if ($i == $j) {
								// print_r($i."==".$j."        ");
								$min = array();
								$min['QUES_ID'] = $quesdts->QUES_ID;
								$min['QUES'] = $quesdts->QUES;
								$min['SUB_NAME'] = $quesdts->SUB_NAME;
								$min['BOOK_NAME'] = $quesdts->BOOK_NAME;
								$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
								array_push($max, $min);
								$j = $j + $divide;
							}
						}
						$max = array_slice($max, 0, $quesno, true);
						$obj = convertToObject($max);
					}
					$data['quesdt'] = $obj;
					echo $response = json_encode($this->load->view('admin/test_paper_new/questionhtml', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}

	public function get_question_html_tpedit() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$subid = $_GET['subid'];
					$bookid = $_GET['bookid'];
					$quesno = $_GET['quesno'];
					$priorityname = $_GET['priorityname'];
					$data['subid'] = $subid;
					$data['bookid'] = $bookid;
					$data['priorityname'] = $priorityname;
					$data['tsid'] = $_GET['tsid'];
					$data['tpid'] = $_GET['tpid'];
					if ($priorityname == 'high') {
						$level = 3;
					} else if ($priorityname == 'medium') {
						$level = 2;
					} else {
						$level = 1;
					}
					$questype = 2; /*For objective Type*/
					$obj = '';
					$quesdt = $this->Admingetmodel->get_remain_question_by_subid_bookid_level($subid, $bookid, $level, $questype, $_GET['tsid']);
					if (!empty($quesdt) && $quesno > 0) {
						$maxsize = sizeof($quesdt);
						$divide = floor($maxsize / $quesno);

						$max = array();
						$j = 0;
						foreach ($quesdt as $i => $quesdts) {

							if ($i == $j) {
								// print_r($i."==".$j."        ");
								$min = array();
								$min['QUES_ID'] = $quesdts->QUES_ID;
								$min['QUES'] = $quesdts->QUES;
								$min['SUB_NAME'] = $quesdts->SUB_NAME;
								$min['BOOK_NAME'] = $quesdts->BOOK_NAME;
								$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
								array_push($max, $min);
								$j = $j + $divide;
							}
						}
						$max = array_slice($max, 0, $quesno, true);
						$obj = convertToObject($max);
					}
					$data['quesdt'] = $obj;
					echo $response = json_encode($this->load->view('admin/test_paper_new/questionhtml', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}

	public function get_practice_question_html_tpedit() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$subid = $_GET['subid'];
					$bookid = $_GET['bookid'];
					$quesno = $_GET['quesno'];
					$priorityname = $_GET['priorityname'];
					$data['subid'] = $subid;
					$data['bookid'] = $bookid;
					$data['priorityname'] = $priorityname;
					$data['tsid'] = $_GET['tsid'];
					$data['tpid'] = $_GET['tpid'];
					if ($priorityname == 'high') {
						$level = 3;
					} else if ($priorityname == 'medium') {
						$level = 2;
					} else {
						$level = 1;
					}
					$questype = 2; /*For objective Type*/
					$obj = '';
					$quesdt = $this->Admingetmodel->get_remain_practice_question_by_subid_bookid_level($subid, $bookid, $level, $questype, $_GET['tsid']);
					if (!empty($quesdt) && $quesno > 0) {
						$maxsize = sizeof($quesdt);
						$divide = floor($maxsize / $quesno);

						$max = array();
						$j = 0;
						foreach ($quesdt as $i => $quesdts) {

							if ($i == $j) {
								// print_r($i."==".$j."        ");
								$min = array();
								$min['QUES_ID'] = $quesdts->QUES_ID;
								$min['QUES'] = $quesdts->QUES;
								$min['SUB_NAME'] = $quesdts->SUB_NAME;
								$min['BOOK_NAME'] = $quesdts->BOOK_NAME;
								$min['QUES_TYPE'] = $quesdts->QUES_TYPE;
								array_push($max, $min);
								$j = $j + $divide;
							}
						}
						$max = array_slice($max, 0, $quesno, true);
						$obj = convertToObject($max);
					}
					$data['quesdt'] = $obj;
					echo $response = json_encode($this->load->view('admin/test_paper_new/questionhtml', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function listmember() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					if (isset($_GET['loginid'])) {
						$loginid = $_GET['loginid'];
						$response = $this->Admingetmodel->get_all_members();
						$max['data'] = array();
						$i = 1;
						if (!empty($response)) {
							foreach ($response as $res) {
								$min = array();
								$min['SR_NO'] = $i;
								$min['NAME'] = $res->USER_FNAME . ' ' . $res->USER_LNAME;
								$min['USER_ROLE_NAME'] = $res->USER_ROLE_NAME;
								$min['USER_EMAIL'] = $res->USER_EMAIL;
								if ($res->USER_STATUS == 1) {
									$stat = '<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
								} else {
									$stat = '<button type="button" class="btn btn-danger pad_4">INACTIVE</button>';
								}
								$min['USER_STATUS'] = $stat;
								$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    								<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    								<ul class="dropdown-menu">
    									<li><a href="javascript:void(0);" class="memberedit" attr-qualification="' . $res->USER_QUALIFICATION . '" attr-qualification="' . $res->USER_QUALIFICATION . '" attr-email="' . $res->USER_EMAIL . '" attr-contact="' . $res->USER_CONTACT . '" attr-address="' . $res->USER_ADDRESS . '" attr-fathername="' . $res->USER_FATHER . '" attr-lname="' . $res->USER_LNAME . '" attr-status="' . $res->USER_STATUS . '" attr-id="' . $res->USER_ID . '"  attr-fname="' . $res->USER_FNAME . '" attr-type="' . $res->USER_ROLES . '" >Edit</a></li>
    								</ul>
    							</div>';

								array_push($max['data'], $min);
								$i++;
							}
						}
						$i = $i - 1;
						$max['message'] = 'Total ' . $i . ' records sended successfully';
						$max['status'] = 200;
						$response['status'] = 200;
						json_output($response['status'], $max);
					} else {
						$response['status'] = 200;
						$response['message'] = 'Invalid query string passed';
						json_output($response['status'], $response);
					}
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}

			}
		}
	}

	public function view_ques_html() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$quesid = $_GET['quesid'];
					$data['quesdt'] = $this->Admingetmodel->get_quesdt_by_id($quesid);
					echo $response = json_encode($this->load->view('admin/question/view_ques_modal', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	public function get_testseries_edit_modalhtml() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$data['tsid'] = $_GET['tsid'];
					$data['tsdt'] = $this->Admingetmodel->get_testseriesdt_by_id($_GET['tsid']);
					echo $response = json_encode($this->load->view('admin/test_series/testserieseditmodal', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function get_practicetest_edit_modalhtml() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$data['tsid'] = $_GET['tsid'];
					$data['tsdt'] = $this->Admingetmodel->get_practicetestdt_by_id($_GET['tsid']);
					echo $response = json_encode($this->load->view('admin/practice_test/practice_testeditmodal', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function edit_ques_html() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$quesid = $_GET['quesid'];
					$data['quesdt'] = $this->Admingetmodel->get_quesdt_by_id($quesid);
					$data['all_sub'] = $this->Admingetmodel->get_subject_by_libid($data['quesdt'][0]->LIB_ID);
					echo $response = json_encode($this->load->view('admin/question/edit_ques_modal', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	public function studentlist() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {

					$payuserids = $this->Admingetmodel->pay_user_ids($_GET['tsid']);
					$response = $this->Admingetmodel->get_all_user_by_usertype(3, $_GET['status'], $payuserids, $_GET['student_type']);

					$max['data'] = array();
					$i = 1;
					if (!empty($response)) {
						foreach ($response as $res) {
							$min = array();
							$min['SR_NO'] = $i;
							$min['USER_NAME'] = $res->USER_FNAME . " " . $res->USER_LNAME;
							$min['USER_CONTACT'] = $res->USER_CONTACT;
							$min['USER_EMAIL'] = $res->USER_EMAIL;
							$testseriesno = $this->Admingetmodel->get_buyed_testseries_no($res->USER_ID);
							$resultdt = $this->Admingetmodel->get_attempt_testpaper_dt($res->USER_ID);
							if (!empty($resultdt)) {
								$submitted_tp_no = sizeof($resultdt);
							} else {
								$submitted_tp_no = 0;
							}
							$min['BUYED_TEST_SERIES_NO'] = '<span style="font-size:14px">' . $testseriesno . '</span>';
							$min['SUBMITTED_TP_NO'] = '<span style="font-size:14px">' . $submitted_tp_no . '</span>';
							$min['USER_ENTRY_TT'] = date('d-m-Y', strtotime($res->USER_ENTRY_TT));
							if ($res->USER_STATUS == 1) {
								$stat = '<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
							} else {
								$stat = '<button type="button" class="btn btn-danger pad_4">INACTIVE</button>';
							}
							$min['USER_STATUS'] = $stat;
							$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    							<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    							<ul class="dropdown-menu">
    							    <li><a href="' . base_url() . 'ratan/performance/' . $res->USER_ID . '">Performance</a></li>
    							</ul>
    						</div>';
							array_push($max['data'], $min);
							$i++;
						}
					}
					$i = $i - 1;
					$max['message'] = 'Total ' . $i . ' records sended successfully';
					$max['status'] = 200;
					$response['status'] = 200;
					json_output($response['status'], $max);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}
	public function get_all_testseries() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$params = $_REQUEST;
					$response['data'] = $this->Admingetmodel->get_all_testseries();

					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}
			}
		}
	}

	public function listcoupon() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$response = $this->Admingetmodel->get_all_coupon();
					$max['data'] = array();
					$i = 1;
					if (!empty($response)) {
						foreach ($response as $res) {
							$min = array();
							$min['SR_NO'] = $i;
							$prductypename = "";
							if (count(explode(",", $res->PROD_TYPE_ID)) > 1 && $res->PRODUCT == 0) {
								$prductypename = "ALL";
							} else {
								$prductypename = $res->PROD_TYPE_NAME;
							}
							$min['COUPON_TITLE'] = $res->COUPON_TITLE;
							$min['PROD_TYPE_NAME'] = $prductypename;
							$coupontype = "";
							if ($res->COUPON_TYPE == 1) {
								$coupontype = "Promo code";
							} else {
								$coupontype = "Discount";
							}
							$min['COUPON_TYPE'] = $coupontype;
							$couponmode = "";
							if ($res->COUPON_MODE == 1) {
								$couponmode = "Flat";
							} else {
								$couponmode = "Percent";
							}
							$min['COUPON_MODE'] = $couponmode;
							if ($res->COUPON_MODE == 1) {
								$min['COUPON_AMOUNT'] = $res->COUPON_AMOUNT . " Rs.";
								$stat = '<button type="button" class="btn btn-danger pad_4">FLAT</button>';
							} else {
								$min['COUPON_AMOUNT'] = $res->COUPON_AMOUNT . " %";
								$stat = '<button type="button" class="btn btn-danger pad_4">PERCENT</button>';
							}
							$min['COUPON_DISCOUNT_TYPE'] = $stat;
							$min['COUPON_START_DATE'] = date('d-m-Y', strtotime($res->COUPON_START_DATE));
							$status = "";
							if ($res->COUPON_STATUS == 1) {
								$status = '<button type="button" class="btn btn-xs btn-success pad_4">ACTIVE</button>';
							} else {
								$status = '<button type="button" class="btn  btn-xs btn-danger pad_4">INACTIVE</button>';
							}
							$min['COUPON_STATUS'] = $status;
							$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
								<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
								<ul class="dropdown-menu">
									<li><a href="javascript:void(0);" class="couponedit" attr-couponid="' . $res->COUPON_ID . '" >Edit</a></li>
								</ul>
                            </div>';

							array_push($max['data'], $min);
							$i++;
						}
					}
					$i = $i - 1;
					$max['message'] = 'Total ' . $i . ' records sended successfully';
					$max['status'] = 200;
					$response['status'] = 200;
					json_output($response['status'], $max);

				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}

			}
		}
	}
	//A -19-04-2020 20-31 Coupon Edit Start
	public function get_coupon_dt_by_couponid() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {

					$max['message'] = 'Record sended successfully';
					$max['status'] = 200;
					$max['data'] = $this->Admingetmodel->get_coupon_dt_by_couponid($this->input->get('couponid'));
					$response['status'] = 200;
					json_output($response['status'], $max);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	//Coupon end

	public function get_student_by_paytype() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					if ($_GET['type'] == 1) {
						$tsids = $_GET['tsids'];
					} else {
						$tsids = $this->Admingetmodel->get_all_testseriesids();
					}
					$payuserids = $this->Admingetmodel->pay_user_ids($tsids);
					$response['data'] = $this->Admingetmodel->get_all_user_by_usertype(3, 1, $payuserids, $_GET['type']);
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	public function getalltemplate() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$sending_type = $_GET['sending_type'];
					$response['data'] = $this->Admingetmodel->get_all_template($sending_type);
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	public function get_templatedt_by_tempid() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$tempid = $_GET['tempid'];
					$response['data'] = $this->Admingetmodel->get_templatedt_by_tempid($tempid)[0];
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	public function getallscheduling() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$sending_type = $_GET['sending_type'];
					$response['data'] = $this->Admingetmodel->get_all_scheduling($sending_type);
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	public function get_schedulingdt_by_schedid() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$schid = $_GET['schid'];
					$response['data'] = $this->Admingetmodel->get_schedulingdt_by_schedid($schid)[0];
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	public function listcourse() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					if (isset($_GET['loginid'])) {
						$loginid = $_GET['loginid'];
						$response = $this->Admingetmodel->get_all_courses();
						$max['data'] = array();
						$i = 1;
						if (!empty($response['data'])) {
							foreach ($response['data'] as $res) {
								$min = array();
								$min['SR_NO'] = $i;
								$min['NAME'] = $res->COURSE_NAME;
								$min['DESC'] = $res->COURSE_DESC;
								if ($res->COURSE_STATUS == 1) {
									$stat = '<button type="button" class="btn btn-xs btn-success pad_4">ACTIVE</button>';
								} else {
									$stat = '<button type="button" class="btn  btn-xs btn-danger pad_4">INACTIVE</button>';
								}
								$image="";
								if(!empty($res->COURSE_IMG)){
									$image=base_url($res->COURSE_IMG);
								}
								$min['COURSE_STATUS'] = $stat;
								$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
    								<a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
    								<ul class="dropdown-menu">
    									<li><a href="javascript:void(0);" class="courseedit" attr-img="' . $image . '"  attr-status="' . $res->COURSE_STATUS . '" attr-courseid="' . $res->COURSE_ID . '"  attr-coursenm="' . $res->COURSE_NAME . '" attr-desc="' . $res->COURSE_DESC . '" attr-cc="' . $res->CC_ID . '" attr-popular="' . $res->COURSE_POPULAR . '" >Edit</a></li>
                                        <li><a href="javascript:void(0);" class="course_content"  attr-status="' . $res->COURSE_STATUS . '" attr-courseid="' . $res->COURSE_ID . '"  attr-coursenm="' . $res->COURSE_NAME . '" attr-desc="' . $res->COURSE_DESC . '" >Add Content</a></li>
    								</ul>
    							</div>';

								array_push($max['data'], $min);
								$i++;
							}
						}
						$i = $i - 1;
						$max['message'] = 'Total ' . $i . ' records sended successfully';
						$max['status'] = 200;
						$response['status'] = 200;
						json_output($response['status'], $max);
					} else {
						$response['status'] = 200;
						$response['message'] = 'Invalid query string passed';
						json_output($response['status'], $response);
					}
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}

			}
		}
	}
	// A 18-04-2020 get product by product type
	public function get_product_by_prodtype() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					if ($this->input->get('prod_type')) {
						if ($this->input->get('prod_type') == 1) {
							$response['data'] = $this->Admingetmodel->get_all_library()['data'];
						}
						if ($this->input->get('prod_type') == 2) {
							$response['data'] = $this->Admingetmodel->get_all_subjects()['data'];
						}
						if ($this->input->get('prod_type') == 3) {
							$response['data'] = $this->Admingetmodel->get_all_active_books()['data'];
						}
						if ($this->input->get('prod_type') == 4) {
							$response['data'] = $this->Admingetmodel->get_all_active_testseries();
						}
						if ($this->input->get('prod_type') == 5) {
							$response['data'] = $this->Admingetmodel->get_all_active_testseries();
						}
					}
					$response['prod_type'] = $this->input->get('prod_type');
					// $schid = $_GET['schid'];
					// $response['data'] =  $this->Admingetmodel->get_schedulingdt_by_schedid($schid)[0];
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}

	public function course_content() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					if (isset($_GET['loginid'])) {
						$loginid = $_GET['loginid'];

						$data = array();

						$course_response = $this->Admingetmodel->get_course_by_id($_GET['courseid']);
						$course_row = $course_response["data"][0];

						$data["course_id"] = $course_row->COURSE_ID;
						$data["course_amt"] = $course_row->COURSE_AMT;
						$data["course_content_disc"] = $course_row->COURSE_CONTENT_DISC;

						$data["course_content"][] = [
							"id" => "0_" . $course_row->COURSE_ID,
							"text" => sprintf("<b>[Course] %s [₹%s]</b>", $course_row->COURSE_NAME, $course_row->COURSE_CONTENT_AMT),
							"parent" => "#",
							"type" => 0,
							"lid" => $course_row->COURSE_ID,
							'state' => [
								'selected' => false,
							],
						];

						$library_response = $this->Admingetmodel->get_all_library();
						foreach ($library_response["data"] as $library_row) {

							$data["course_content"][] = [
								"id" => "1_" . $library_row->LIB_ID,
								"text" => sprintf("<b>[Library] %s [₹%s]</b>", $library_row->LIB_NAME, $library_row->LIB_CONTENT_AMT),
								"parent" => "0_" . $course_row->COURSE_ID,
								"type" => 1,
								"lid" => $library_row->LIB_ID,
								'state' => [
									'selected' => false,
								],
							];

							$subject_response = $this->Admingetmodel->get_subject_by_libid($library_row->LIB_ID);
							foreach ($subject_response["data"] as $subject_row) {

								$data["course_content"][] = [
									"id" => "2_" . $subject_row->SUB_ID,
									"text" => sprintf("<b>[Subject] %s [₹%s]</b>", $subject_row->SUB_NAME, $subject_row->SUB_CONTENT_AMT),
									"parent" => "1_" . $library_row->LIB_ID,
									"type" => 2,
									"lid" => $subject_row->SUB_ID,
									'state' => [
										'selected' => false,
									],
								];

								$book_response = $this->Admingetmodel->get_book_by_subid($subject_row->SUB_ID);
								foreach ($book_response["data"] as $book_row) {

									$data["course_content"][] = [
										"id" => "3_" . $book_row->BOOK_ID,
										"text" => sprintf("<b>[Book] %s [₹%s]</b>", $book_row->BOOK_NAME, $book_row->BOOK_CONTENT_AMT),
										"parent" => "2_" . $subject_row->SUB_ID,
										"type" => 3,
										"lid" => $book_row->BOOK_ID,
										'state' => [
											'selected' => false,
										],
									];

								}
							}
						}

						if (!empty($course_row->COURSE_CONTENT)) {
							$selected_content = json_decode($course_row->COURSE_CONTENT, TRUE);
							foreach ($data["course_content"] as $index => $row) {
								$id = $row["lid"];
								$type = (string) $row["type"];
								if (isset($selected_content[$type]) && array_search($id, $selected_content[$type]) !== FALSE) {
									$data["course_content"][$index]["state"]["selected"] = true;
								}
							}
						}

						$response['status'] = 200;
						$response['message'] = 'Course Content Sended Successfully';
						$response['data'] = $data;
						json_output($response['status'], $response);
					} else {
						$response['status'] = 200;
						$response['message'] = 'Invalid query string passed';
						json_output($response['status'], $response);
					}
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}

			}
		}
	}

	public function listslider() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					if (isset($_GET['loginid'])) {
						$loginid = $_GET['loginid'];
						$getData = $this->input->get();
						list($result, $totalFilterRecords, $totalRecords) = $this->Admingetmodel->get_all_sliders($getData);
						$draw = $getData['draw'];
						$start = $getData['start'];
						$data = array();
						$i = $draw;
						$data = array();

						foreach ($result as $record) {
							$min = array();
							$min['SR_NO'] = $i++;
							$min['ID'] = $record->SLIDER_ID;
							$min['PROD_TYPE_NAME'] = "";
							if($record->PROD_TYPE==1){
								$min['PROD_TYPE_NAME'] = "HOME";
							}else if($record->PROD_TYPE==2){
								$min['PROD_TYPE_NAME'] = "COURSES";
							}else if($record->PROD_TYPE==3){
								$min['PROD_TYPE_NAME'] = "TEST SERIES";
							}else if($record->PROD_TYPE==4){
								$min['PROD_TYPE_NAME'] = "EXAMS";
							}else if($record->PROD_TYPE==5){
								$min['PROD_TYPE_NAME'] = "TEST PAPERS";
							}else if($record->PROD_TYPE==6){
								$min['PROD_TYPE_NAME'] = "PRACTICE TEST";
							}
							$min['SLIDER_NAME'] = $record->SLIDER_NAME;
							$min['PROD_TYPE'] = $record->PROD_TYPE;
							$min['SLIDER_IMAGE'] = $record->SLIDER_IMG;
							if ($record->SLIDER_STATUS == 1) {
								$min['SLIDER_STATUS_A'] = '<button type="button" class="btn btn-success pad_4">ACTIVE</button>';
								$min['SLIDER_STATUS'] = 1;
							} else {
								$min['SLIDER_STATUS_A'] = '<button type="button" class="btn btn-danger pad_4">INACTIVE</button>';
								$min['SLIDER_STATUS'] = 0;
							}
							$min['TOOLS'] = '<div class="btn-group m-r-5 m-b-5">
                                            <a href="#" data-toggle="dropdown" class="btn pad_4 btn-primary dropdown-toggle"><i class="fas fa-cog fa-fw"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href=".edit-slider-modal" class="edit-slider" type="button" data-toggle="modal"  data-id="' . $record->SLIDER_ID . '" >Edit</a></li>
                                            </ul>
                                        </div>';
							$data[] = $min;
						}
						## Response
						$response = array(
							"draw" => intval($draw),
							"iTotalRecords" => $totalRecords,
							"iTotalDisplayRecords" => $totalFilterRecords,
							"aaData" => $data,
						);
						$response['status'] = 200;
						json_output($response['status'], $response);
					} else {
						$response['status'] = 200;
						$response['message'] = 'Invalid query string passed';
						json_output($response['status'], $response);
					}
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();

					json_output(401, $response);
				}

			}
		}
	}
	public function get_subject_from_course() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$courses = $this->Admingetmodel->get_all_subject_from_courseid($_GET['courseid']);
					$max['data'] = $courses['data'];
					$max['message'] = 'records sended successfully';
					$max['status'] = 200;
					$response['status'] = 200;
					json_output($response['status'], $max);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}

			}
		}
	}
	public function library_contentpage() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$data['subjects'] = $this->Admingetmodel->get_subject_by_libid($_GET['lib_id']);
					echo $response = json_encode($this->load->view('admin/library/library_content', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}

			}
		}
	}
	public function subject_contentpage() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(400, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$data['books'] = $this->Admingetmodel->get_book_by_subid($_GET['sub_id']);
					echo $response = json_encode($this->load->view('admin/subject/subject_content', $data));
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}

			}
		}
	}

	/*Shubham 2020-06-13*/
	public function get_remains_questions_count() {
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'GET') {
			json_output(200, array('status' => 405, 'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
				$response = $this->Authentication_model->auth();
				if ($response['status'] == 200) {
					$response = $this->Admingetmodel->get_remains_questions($this->input->get('bookid'));
					$response["data"] = count($response["data"]); 
					json_output($response['status'], $response);
				} else if ($response['status'] == 303 || $response['status'] == 401) {
					$this->Adminpostmodel->logout();
					json_output(401, $response);
				}
			}
		}
	}
	/**/
}
