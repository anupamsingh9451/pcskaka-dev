<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Studentpostajax extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database('default');
		$this->load->library('email');
		$this->load->model('student/Authentication_model');
        $this->load->model('student/Studentpostmodel');
		$this->load->model('student/Studentgetmodel');
		$this->load->model('admin/Admingetmodel');
		$this->load->model('admin/Adminpostmodel');
		$this->load->helper('json_output_helper');
		$this->load->helper('common_helper');
		$this->load->library('pdf');
        date_default_timezone_set('Asia/Calcutta');
        $this->load->library('cart'); 
	    // Your own constructor code
	}
	public function studentreg()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
    		$name = $_POST['name'];
    		$email = $_POST['email'];
    		$password = md5($_POST['password']);
            $type = 3; // 3 for Student
            $result = $this->Studentgetmodel->check_student_exist($email);
            $to = $email;
            $from = "connect@pcskaka.com";
            if(empty($result)){
               
                $otp = rand(1000,10000);
                $message = "Dear ".strtoupper($name)."<br>".$otp." is Your one time password (OTP).<br> Please Enter the OTP to proceed<br>Thank You.<br>Team PCSKAKA. ";
                $data = array(
    				'USER_FNAME'=>strtoupper($name),
    				'USER_EMAIL'=>$email,
    				'USER_CONTACT'=>$_POST['mobile'],
    				'USER_ROLES'=>$type,
    				'USER_PASSWORD' =>$password,
    				'USER_STATUS' =>0,
    				'USER_ENTRY_TT' =>date('Y-m-d H:i:s'),
    				'USER_OTP' => $otp
    			);
    			
                $insertid = $this->Studentpostmodel->studentregistration($data);
                $result = send_mail($from,$to,'OTP',$message);
                if($result==1){
                    $response['message']='ok';
                    $response['status']=200;
				    $response['data']=$insertid;
                }else{
                    $response['message']='Something Went Wrong!'; 
                    $response['status']=200;
				    $response['data']='';
                }
                
            }else{
                if($result[0]->USER_STATUS==1){
                    $response['message']='Already Registered Email!';
                    $response['status']=200;
					$response['data']='';
                }else{
                    $id = $result[0]->USER_ID;
                    $otp = rand(1000,10000);
                    $message = "Dear ".$result[0]->USER_FNAME." ".$result[0]->USER_LNAME."<br>".$otp." is Your one time password (OTP).<br> Please Enter the OTP to proceed<br>Thank You.<br>Team PCSKAKA. ";
                    $data = array(
        				'USER_LNAME'=>strtoupper($name),
        				'USER_EMAIL'=>$email,
        				'USER_CONTACT'=>$_POST['mobile'],
        				'USER_PASSWORD' =>$password,
        				'USER_STATUS' =>0,
        				'USER_OTP' => $otp
        			);
                    $this->Studentpostmodel->studentregupdate($data,$id);
                    $result = send_mail($from,$to,'OTP',$message);
                    if($result==1){
                        $response['message']='ok';
                        $response['status']=200;
    				    $response['data']=$id;
                    }else{
                        $response['message']='Something Went Wrong!';
                        $response['status']=200;
    				    $response['data']='';
                    }
                }
            }
            json_output(200, $response);
        }
    }
    public function otpsubmit()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $from = "connect@pcskaka.com";
    		$id = $_POST['insertid'];
    		$otp = $_POST['otp'];
    		$password = $_POST['password'];
            $result = $this->Studentgetmodel->varify_otp($otp,$id);
            if(!empty($result)){
                $data = array(
    				'USER_STATUS' =>1,
    			);
                $res = $this->Studentpostmodel->studentregupdate($data,$id);
                if($res){
                    $response['message']='ok';
                    $response['status']=200;
				    $response['data']='';
				    $dt = $this->Studentgetmodel->get_user_dt_by_id($id);
				    $name=$dt[0]->USER_FNAME." ".$dt[0]->USER_LNAME;
				    $message = "Dear ".$name.",<br> Welcome to the digital world of PCSKAKA for competitive exam preparation!<br>
				    We are glad to welcome you on board.<br>
				    PCSKAKA's family of millions of users is waiting to welcome you. Millions of aspirants started their journey with PCSKAKA.<br>
				    So, start exploring PCSKAKA on your mobile, PC, Tablet or Laptop.<br>
				    In case of any help, please write to us at: support@pcskaka.com<br>
				    Regards Team PCSkaka";
                    $result = send_mail($from,$dt[0]->USER_EMAIL,'Welcome Email',$message);
                }else{
                    $response['message']='Something went wrong!'; 
                    $response['status']=200;
				    $response['data']='';
                }
                
            }else{
                $response['message']='OTP IS INCORRECT!';
                $response['status']=200;
				$response['data']='';
            }
            json_output(200, $response);
        }
    }
    public function resendotp()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $from = "connect@pcskaka.com";
    		$id = $_POST['userid'];
            $userdt = $this->Studentgetmodel->get_user_dt_by_id($id);
            $otp = rand(1000,10000);
            $message = "Dear ".$userdt[0]->USER_FNAME." ".$userdt[0]->USER_LNAME."<br>".$otp." is Your one time password (OTP).<br> Please Enter the OTP to proceed<br>Thank You.<br>Team PCSKAKA. ";
            $data = array(
				'USER_OTP' => $otp
			);
            $this->Studentpostmodel->studentregupdate($data,$id);
            $result = send_mail($from,$userdt[0]->USER_EMAIL,'OTP',$message);
            if($result==1){
                $response['message']='ok';
                $response['status']=200;
			    $response['data']=$id;
            }else{
                $response['message']='Something Went Wrong!';
                $response['status']=200;
			    $response['data']='';
            }
            json_output(200, $response);
        }
    }
	public function studentlogin()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
				$username = $_POST['email'];
                $password = md5($_POST['password']);
                $type = 3; // 3 for Student
                if(isset($_POST['loginagain'])){
                    $loginagain = $_POST['loginagain'];
                }else{
                    $loginagain = '';
                }
                $response = $this->Studentpostmodel->login($username, $password, $type,$loginagain);
                json_output($response['status'], $response);
                if ($response['message'] == "ok"){
                    // insert_activity_history(1);
                }
            }
        }
    }
    public function forgotpassotpsubmit()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
    		$id = $_POST['insertid'];
    		$otp = $_POST['otp'];
            $result = $this->Studentgetmodel->varify_otp($otp,$id);
            if(!empty($result)){
                $response['message']='ok';
                $response['status']=200;
			    $response['data']=$id;
            }else{
                $response['message']='OTP IS INCORRECT!';
                $response['status']=200;
				$response['data']='';
            }
            json_output(200, $response);
        }
    }
     public function studentforgotpass()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $from = "connect@pcskaka.com";
    		
    		$email = $_POST['email'];
            $dt = $this->Studentgetmodel->check_student_exist($email);
            if(!empty($dt)){
                $otp = rand(1000,10000);
                $data = array(
    				'USER_OTP' => $otp
    			);
                $this->Studentpostmodel->studentregupdate($data,$dt[0]->USER_ID);
                $message = "Dear ".$dt[0]->USER_FNAME." ".$dt[0]->USER_LNAME."<br>".$otp." is Your one time password (OTP).<br> Please Enter the OTP to proceed<br>Thank You.<br>Team PCSKAKA. ";
                $result = send_mail($from,$email,'OTP',$message);
                if($result==1){
                    $response['message']='ok';
                    $response['status']=200;
    			    $response['data']=$dt[0]->USER_ID;
                }else{
                    $response['message']='Something Went Wrong!';
                    $response['status']=200;
    			    $response['data']='';
                }
            }else{
                $response['message']='Email Does Not exist!';
                $response['status']=200;
			    $response['data']='';
            }
            json_output(200, $response);
        }
    }
    public function forgotpasswordsubmit()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $from = "connect@pcskaka.com";
    		$id = $_POST['insertid'];
    		$password = md5($_POST['password']);
            $data = array(
    				'USER_PASSWORD' => $password
    			);
            $result = $this->Studentpostmodel->studentregupdate($data,$id);
            if(!empty($result)){
                $response['message']='ok';
                $response['status']=200;
            }else{
                $response['message']='Something went wrong!';
                $response['status']=200;
				$response['data']='';
            }
            json_output(200, $response);
        }
    }
	public function logoutstudent()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
                    // $this->session->sess_destroy();
                    $response = $this->Studentpostmodel->logout();
                    json_output(200, $response);
                    // insert_activity_history(3);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
    }

    public function successpayment()
    {
        $tt = date('Y-m-d H:i:s');
        $post=$this->input->post();
        $productinfo=explode(',',$post['productinfo']);
        $tr="";
        $totalpromodisc=$total=0;
        $paidamount=$this->input->post('net_amount_debit');
        foreach($productinfo as $pinfo){
            $getpinfo=get_invoicedt($pinfo);
            if($getpinfo->COUPON_ID!="" && $getpinfo->COUPON_ID!=0){
                update_coupontotal_uses($getpinfo->COUPON_ID);
            }
            $discount1=$discount2=0;
            if(!empty($getpinfo->PROD_DISC_TYPE)){
                $get_product_disctype_dt=json_decode($getpinfo->PROD_DISC_TYPE);
                if(!empty($get_product_disctype_dt)){
                    foreach ($get_product_disctype_dt as $typedtdisc) {
                        if(!empty($typedtdisc->COUPON_TYPE=='discount')){
                            $discount1=$typedtdisc->DISCOUNT_AMT;
                        }
                        if(!empty($typedtdisc->COUPON_TYPE=='promocode')){
                            $discount2=$typedtdisc->DISCOUNT_AMT;
                            $totalpromodisc+=$discount2;
                            $couponuse=$typedtdisc->COUPON_ID;
                        }
                    }
                }
            }
            $invoiceno=$getpinfo->INVOICE_NO;
            $proddt=get_product_dt_prod_type_prod_id($getpinfo->PROD_TYPE_ID,$getpinfo->PRODUCT_ID);
            $tr .= '<tr>
                        <td style="border:1px solid;border-collapse: collapse;padding-left: 1%;">'.$proddt['PROD_NAME'].'</td>
                        <td style="border:1px solid;border-collapse: collapse;padding-left: 1%;width:18%;">'.$proddt['END_DATE'].'</td>
                        <td style="border:1px solid;border-collapse: collapse;padding-left: 1%;width:18%;">'.$getpinfo->PROD_BASEPRICE.'</td>
                        <td style="border:1px solid;border-collapse: collapse;padding-left: 1%;width:12%;">'.($discount1).'</td>
                        <td style="border:1px solid;border-collapse: collapse;padding-left: 1%;width:19%;">'.($getpinfo->PROD_BASEPRICE-$discount1).'</td>
                    </tr>';
            $total=$total+ $getpinfo->PROD_BASEPRICE-$discount1;       
        }
		
		$promohtml="";
        if(!empty($getpinfo)){
            if($totalpromodisc!=0){
                $coupandt=$this->Admingetmodel->get_coupon_dt_by_couponid($couponuse)[0];
                $promohtml='<tr>
                    <td style="border:1px solid;border-collapse: collapse;padding-left: 1%;" colspan="1">Coupon : '.$coupandt->COUPON_TITLE.'</td>
                    <td style="border:1px solid;border-collapse: collapse;padding-left: 1%;" colspan="3">'.$coupandt->COUPON_DESC.'</td>
                    <td style="border:1px solid;border-collapse: collapse;padding-left: 1%;width:17%;" colspan="1">'.($totalpromodisc).'</td>
                </tr>';
            }
        }
        
        $tr .= '<tr>
        			<td style="border:1px solid;border-collapse: collapse;padding-left: 1%;" colspan="4">Total</td>
        			<td style="border:1px solid;border-collapse: collapse;padding-left: 1%;width:17%;" colspan="1">'.$total.'</td>
                </tr>'. $promohtml.'
                <tr>
        			<td style="border:1px solid;border-collapse: collapse;padding-left: 1%;" colspan="4">Paid Amount</td>
        			<td style="border:1px solid;border-collapse: collapse;padding-left: 1%;width:17%;" colspan="1">'.$paidamount.'</td>
        		</tr>';
        
        if(!empty($getpinfo)){
			$data = array(
    		    'USER_ID'=> $getpinfo->USER_ID,
    		    'PAY_TXN_NO'=> $this->input->post('txnid'),
    		    'PAY_PAYUMONEYID'=>$this->input->post('payuMoneyId'),
    		    'PAY_CARD_NUM'=> $this->input->post('cardnum'),
    		    'PAY_NAME_ON_CARD'=> $this->input->post('name_on_card'),
    		    'PAY_BANK_CODE'=> $this->input->post('bankcode'),
    		    'PAY_BANK_REF_NO'=>  $this->input->post('bank_ref_num'),
    		    'PAY_MOBILE_NO'=>  $this->input->post('phone'),
    		    'PAY_AMOUNT'=>  $this->input->post('amount'),
    		    'PAY_TXN_STATUS'=> $this->input->post('txnStatus'),
    		    'PAY_STATUS'=>$this->input->post('status'),
    		    'PAY_HASH'=> $this->input->post('hash'),
    		    'INVOICE_NO'=>$invoiceno,
    		    'PAY_TT'=>$this->input->post('addedon'),
    		    'PAY_DATE_TIME'=> $tt,
    		    'PAY_MIHPAY_ID'=> $this->input->post('mihpayid'),
            );
			$response = $this->Studentpostmodel->addsuccesspayment($data);
        }        
        $from = "connect@pcskaka.com";
        $userdt = $this->Studentgetmodel->get_user_dt_by_id($this->session->userdata('studentloginid'))[0];
        $username=$userdt->USER_FNAME." ".$userdt->USER_LNAME;
        $email=$userdt->USER_EMAIL;
        $message = '
            <html>
                <head>
                	<title></title>
                	<style type="text/css">
                		@media print {
                body {-webkit-print-color-adjust: exact;}
                }
                	</style>
                </head>
                <body style="margin-top: 2%;">
                	<p style="line-height: 2rem;    word-spacing: 5px;">
                		New Subscription #: '.$this->input->post('txnid').'<br>
                	</p>
                	<img src="'.base_url().'assets/logo/kakapng.png" style="margin-left: 45%;height: 150px;">
                	<p style="line-height: 2rem; word-spacing: 5px;">Dear '.$username.',<br>
                		We have received your payment<br>
                		Payment details are as follows:
                	</p>
                	<table style="width: 100%;border:1px solid;border-collapse: collapse;height: 70px;">
                		<tr>
                			<td style="border:1px solid;border-collapse: collapse;padding-left: 1%;">Product</td>
                			<td style="border:1px solid;border-collapse: collapse;padding-left: 1%;">Valid Till</td>
                			<td style="border:1px solid;border-collapse: collapse;padding-left: 1%;width:17%">Price</td>
                            <td style="border:1px solid;border-collapse: collapse;padding-left: 1%;width:17%">Discount</td>
                			<td style="border:1px solid;border-collapse: collapse;padding-left: 1%;width:17%">Total</td>
                		</tr>
                		'.$tr.'
                	</table>
                	<p style="margin-top: 2%; word-spacing: 5px;">If you think the details mentioned are incorrect, please write to us at: payments@pcskaka.com</p>
                	<p style="line-height: 2rem; word-spacing: 5px;">Thanks for showing your trust in PCSKAKA.<br>
                		Regards,<br>
                		Team PCSKAKA<br>
                	</p>
                
                	<a href="www.pcskaka.com">www.pcskaka.com</a><br><br><br><br>
                	<div style="background-color: black;height: 30px;width:100%;padding:6px;">
                    	<a href="'.base_url().'site/privacypolicy" style="float: left;color: white;font-size: 12px;width:30%;text-align:center;">Privacy Policy</a>
                    	<a href="'.base_url().'site/terms" style="width:30%;color: white;font-size: 12px;text-align:center;">Terms of use</a>
                    	<a href="'.base_url().'site/sales_refund" style="width:35%;float: right;color: white;font-size: 12px;text-align:center;">Sales & Refund Policy</a>
                    </div>
                </body>
            </html>';
        send_mail($from,$email,'Invoice',$message);
        redirect('student/dashboard');
    }
    public function failurepayment()
    {
        redirect('student/dashboard');
        exit;
    }
    public function editprofile()
    {		
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
            		$id=$this->input->post('studentid');
            		$data = array(
            			'USER_FNAME' => $_POST['fname'],
            			'USER_LNAME' => $_POST['lname'],
            			'USER_ADDRESS' => $_POST['add'],
            			'USER_CONTACT' => $_POST['contact'],
            			'USER_QUALIFICATION' => $_POST['quali'],
            			'USER_FATHER' => $_POST['father'],
            		);
            		$result = $this->Studentpostmodel->seteditprofile($data,$id);
                    if($result){
                        $response['message']='ok';
                        $response['status']=200;
                    }else{
                        $response['message']='Something went wrong!';
                        $response['status']=200;
                    }
                    json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    json_output(401, $response);
                }
            }
        }
    }
    public function changepwd()
    {
		$method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					$id=$this->input->post('studentid');
					$result=$this->Studentgetmodel->get_user_dt_by_id($id);
					$oldpassword = md5($this->input->post('oldpwd'));
					$oldpwd = $result[0]->USER_PASSWORD;
					$confrmpass = $this->input->post('newpwd');
					
					
					if($_POST['newpwd'] != '' & $_POST['cnfrmpwd'] != '' & $oldpassword != ''){
						if($oldpwd == $oldpassword){
							if($confrmpass == $_POST['cnfrmpwd']){
							    $data= array(						
            						'USER_PASSWORD' => md5($this->input->post('newpwd')),
            					);
								$this->Studentpostmodel->setpassword($data,$id);
								$response['message']='Password Changed Successfully';
								$response['data']='ok';
							}else{
								$response['message']='Password and Confirm Password do not match';
								$response['data']='Notok';
							}
						}else{
							$response['message']='Old Password does not match';
							$response['data']='Notok';
						}
					}else{
						$response['message']='Fields are required';
						$response['data']='Notok';
					}
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }

	}
    
    public function purchaseproduct()
    {
		$method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(200, array('status' => 405, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->Authentication_model->check_auth_client();
			if ($check_auth_client == true) {
                $response = $this->Authentication_model->auth();
                if ($response['status'] == 200) {
					if(!empty($this->input->post('allproducts'))){
                        $allproducts=$this->input->post('allproducts');
                        $invoiceno=get_new_invoiceno();
                        $finalamount=0;
                        foreach($allproducts as $products){
                            if($products['producttype']==1){ //LIBRARY
                                $baseprice=get_product_cost_prod_type_prod_id($products['producttype'],$products['productid']);
                                $discountamt=0;$taxableamt=0;$totaltax=0;$couponid=0;
                                $disctype=$taxtype=array();
                                if (isset($products['coupnid'])) {
                                    if ($products['coupnid']!='undefined'  && $products['coupnid']!='') {
                                        $coupondt=$this->Studentgetmodel->get_promo_dt_by_promoid($products['coupnid']);
                                        if ($coupondt[0]->COUPON_MODE==1) {
                                            $discountamt = $coupondt[0]->COUPON_AMOUNT;
                                        } else {
                                            $discountamt = ($baseprice*$coupondt[0]->COUPON_AMOUNT)/100;
                                        }
                                        if ($coupondt[0]->COUPON_TYPE==1) {
                                            $disctype['TYPE']='PROMOCODE';
                                        } else {
                                            $disctype['TYPE']='DISCOUNT';
                                        }
                                        $couponid=$products['coupnid'];
                                    }
                                }   
                                $taxableamt=$baseprice-$discountamt;
                                $prodtypedt=get_producttype_dt_by_id($products['producttype']);
                                $producttax=json_decode($prodtypedt->PROD_GST);
                                if ($producttax!="" && $producttax!="" && !empty($producttax)) {
                                    $totaltax=($taxableamt*$producttax[0]->CGST/100) +($taxableamt*$producttax[1]->SGST/100);
                                    $taxtype['CGST']=$producttax[0]->CGST;
                                    $taxtype['SGST']=$producttax[1]->SGST;
                                }
                                $totalpayableamt=$taxableamt+$totaltax;
                                $data=array(
                                    'PROD_TYPE_ID'=>$products['producttype'],
                                    'PRODUCT_ID'=>$products['productid'],
                                    'USER_ID'=>$this->session->userdata('studentloginid'),
                                    'PROD_BASEPRICE'=>$baseprice,
                                    'PROD_DISC_TYPE'=>json_encode($disctype),
                                    'PROD_DISC_AMT'=>$discountamt,
                                    'PROD_TAXES'=>json_encode($taxtype),
                                    'INVOICE_NO'=>$invoiceno,
                                    'INOICE_AMOUNT'=>$totalpayableamt,
                                    'INVOICE_TT'=>date('Y-m-d H-i-s'),
                                    'INVOICE_DISCOUNT'=>$discountamt,
                                    'COUPON_ID'=>$couponid,
                                );
                                $finalamount=$finalamount+$totalpayableamt;
                                $this->Studentpostmodel->addinvoice($data);
                            }else if($products['producttype']==2){

                            }else if($products['producttype']==3){

                            }else if($products['producttype']==4){//TEST SERIES
                                $baseprice=get_product_cost_prod_type_prod_id($products['producttype'],$products['productid']);
                                $discountamt=0;$taxableamt=0;$totaltax=0;$couponid=0;
                                $disctype=$taxtype=array();
                                if (isset($products['coupnid'])) {
                                    if ($products['coupnid']!='undefined' && $products['coupnid']!='') {
                                        $coupondt=$this->Studentgetmodel->get_promo_dt_by_promoid($products['coupnid']);
                                        if ($coupondt[0]->COUPON_MODE==1) {
                                            $discountamt = $coupondt[0]->COUPON_AMOUNT;
                                        } else {
                                            $discountamt = ($baseprice*$coupondt[0]->COUPON_AMOUNT)/100;
                                        }
                                        if ($coupondt[0]->COUPON_TYPE==1) {
                                            $disctype['TYPE']='PROMOCODE';
                                        } else {
                                            $disctype['TYPE']='DISCOUNT';
                                        }
                                        $couponid=$products['coupnid'];
                                    }
                                }   
                                $taxableamt=$baseprice-$discountamt;
                                $prodtypedt=get_producttype_dt_by_id($products['producttype']);
                                $producttax=json_decode($prodtypedt->PROD_GST);
                                if ($producttax!="" && $producttax!="" && !empty($producttax)) {
                                    $totaltax=($taxableamt*$producttax[0]->CGST/100) +($taxableamt*$producttax[1]->SGST/100);
                                    $taxtype['CGST']=$producttax[0]->CGST;
                                    $taxtype['SGST']=$producttax[1]->SGST;
                                }
                                $totalpayableamt=$taxableamt+$totaltax;
                                $data=array(
                                    'PROD_TYPE_ID'=>$products['producttype'],
                                    'PRODUCT_ID'=>$products['productid'],
                                    'USER_ID'=>$this->session->userdata('studentloginid'),
                                    'PROD_BASEPRICE'=>$baseprice,
                                    'PROD_DISC_TYPE'=>json_encode($disctype),
                                    'PROD_DISC_AMT'=>$discountamt,
                                    'PROD_TAXES'=>json_encode($taxtype),
                                    'INVOICE_NO'=>$invoiceno,
                                    'INOICE_AMOUNT'=>$totalpayableamt,
                                    'INVOICE_TT'=>date('Y-m-d H-i-s'),
                                    'INVOICE_DISCOUNT'=>$discountamt,
                                    'COUPON_ID'=>$couponid,
                                );
                                $finalamount=$finalamount+$totalpayableamt;
                                $this->Studentpostmodel->addinvoice($data);
                            }else if($products['producttype']==5){

                            }
                        }
                        $response['FINAL_AMT']=$finalamount;    
                        $response['message']='Product purchased.';
						$response['data']='ok';
                    }else{
                        $response['message']='Product failed to purchased.';
						$response['data']='not ok';
                    }
					json_output(200, $response);
                } else if ($response['status'] == 303 || $response['status'] == 401) {
                    $this->Studentpostmodel->logout();
                    
                    json_output(401, $response);
                }
            }
        }

	}
}

