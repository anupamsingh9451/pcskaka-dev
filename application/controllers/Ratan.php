<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Ratan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database('default');
		$this->load->library('email');
		$this->load->helper('common_helper');
		$this->load->model('admin/Admingetmodel');
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if ($this->session->userdata('adminloginid') == '') {
			if (base_url() . 'ratan' != $actual_link) {
				redirect('ratan');
			}
		}else{
			if (base_url() . 'ratan' == $actual_link) {
				redirect('ratan/dashboard');
			}
		}
	}
	public function index()
	{
		$this->load->view('admin/index');
	}
	public function dashboard()
	{
		$data=array();
		$data['page']='Dashboard';
		$data['subpage']='';
		$this->load->view('admin/adminheader',$data);
		$this->load->view('admin/dashboard');
		$this->load->view('admin/adminfooter');
	}
	
	public function library()
	{
		$data['page']='library';
		if($this->uri->segment(3)=='create'){
			$data['subpage']='Create Library';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/library/libcreate');
			$this->load->view('admin/library/libraryscripts');
		}
		else if($this->uri->segment(3)=='list'){
			$data['subpage']='List Library';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/library/liblist',$data);
			$this->load->view('admin/library/libraryscripts');
		}
		else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function subject()
	{
		$data['page']='subject';
		if($this->uri->segment(3)=='create'){
			$data['subpage']='Create subject';
			$loginid = $this->session->userdata('adminloginid');
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/subject/subcreate',$data);
			$this->load->view('admin/subject/subjectscripts');
		}
		else if($this->uri->segment(3)=='list'){
			$data['subpage']='List subject';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/subject/sublist',$data);
			$this->load->view('admin/subject/subjectscripts');
		}
		else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function book()
	{
		$data['page']='book';
		if($this->uri->segment(3)=='create'){
			$data['subpage']='Create book';
			$loginid = $this->session->userdata('adminloginid');
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/book/bookcreate',$data);
			$this->load->view('admin/book/bookscripts');
		}
		else if($this->uri->segment(3)=='list'){
			$data['subpage']='List book';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/book/booklist',$data);
			$this->load->view('admin/book/bookscripts');
		}
		else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function chapter()
	{
		$data['page']='book';
		if($this->uri->segment(3)!=''){
			$data['subpage']='List book';
			$data['chapterdt'] = $this->Admingetmodel->get_booksdt_by_id($this->uri->segment(3));
			$loginid = $this->session->userdata('adminloginid');
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/chapter/chapters',$data);
			$this->load->view('admin/chapter/chapterscripts');
		}else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function question()
	{
		$data['page']='book';
		if($this->uri->segment(3)=='view'){
			$data['subpage']='List book';
			$this->load->view('admin/adminheader',$data);
			if($this->uri->segment(4)!=''){
			    $data['quesdt'] = $this->Admingetmodel->get_quesdt_by_id($this->uri->segment(4));
			    $data['userrole'] = $this->Admingetmodel->get_user_role_by_id($data['quesdt'][0]->USER_ROLES);
			    $this->load->view('admin/question/view_ques',$data);
			    $this->load->view('admin/question/viewquesscript');
			}else{
			$data['chapterdt'] = $this->Admingetmodel->get_chaptersdt_by_id($this->uri->segment(3))['data'];
			    $this->load->view('admin/question/questions',$data);
			}
		}else if($this->uri->segment(3)=='edit'){
			$data['subpage']='List book';
			$this->load->view('admin/adminheader',$data);
			if($this->uri->segment(4)!=''){
			    $data['quesdt'] = $this->Admingetmodel->get_quesdt_by_id($this->uri->segment(4));
			    $data['all_sub'] = $this->Admingetmodel->get_subject_by_libid($data['quesdt'][0]->LIB_ID);
			    $this->load->view('admin/question/edit_ques',$data);
			    $this->load->view('admin/question/editquestionscripts');
			}else{
			$data['chapterdt'] = $this->Admingetmodel->get_chaptersdt_by_id($this->uri->segment(3))['data'];
			    $this->load->view('admin/question/questions',$data);
			    $this->load->view('admin/question/questionscripts');
			}
			
		}else if($this->uri->segment(3)=='create'){
			$data['subpage']='List book';
			$this->load->view('admin/adminheader',$data);
			if($this->uri->segment(4)!=''){
			    $data['chapterdt'] = $this->Admingetmodel->get_chaptersdt_by_id($this->uri->segment(4))['data'];
			    $this->load->view('admin/question/createquestion',$data);
			    $this->load->view('admin/question/questionscripts',$data);
			}else{
			    $data['chapterdt'] = $this->Admingetmodel->get_chaptersdt_by_id($this->uri->segment(3))['data'];
			    $this->load->view('admin/question/questions',$data);
			    $this->load->view('admin/question/questionscripts');
			}
			
		}else{
			$data['subpage']='List book';
			$data['chapterdt'] = $this->Admingetmodel->get_chaptersdt_by_id($this->uri->segment(3))['data'];
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/question/questions',$data);
			$this->load->view('admin/question/questionscripts');
			$this->load->view('admin/question/viewquesscript');
			$this->load->view('admin/question/editquestionscripts');
		}
		$this->load->view('admin/adminfooter');
	}
	
	public function ques_user()
	{
		$data['page']='question';
		
		if($this->uri->segment(3)=='question'){
            $data['subpage']='question';
			$data['alluser'] = $this->Admingetmodel->get_all_users();
			$data['subdt'] = $this->Admingetmodel->get_all_subject();
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/ques_user/ques_users',$data);
			$this->load->view('admin/ques_user/quesuserscripts');
			$this->load->view('admin/question/viewquesscript');
			$this->load->view('admin/question/editquestionscripts');

		}else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function test_series()
	{
		$data['page']='test series';
		if($this->uri->segment(3)=='create'){
			$data['subpage']='Create test series';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/test_series/testseriescreate');
			$this->load->view('admin/test_series/testseriesscripts');
		}
		else if($this->uri->segment(3)=='list'){
			$data['subpage']='List test series';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/test_series/testserieslist',$data);
			$this->load->view('admin/test_series/testseriesscripts');
		}
		else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function practice_test()
	{
		$data['page']='Test Set';
		if($this->uri->segment(3)=='create'){
			$data['subpage']='Create Test Set';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/practice_test/practice_testcreate');
			$this->load->view('admin/practice_test/practice_testscripts');
		}
		else if($this->uri->segment(3)=='list'){
			$data['subpage']='List Test Set';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/practice_test/practice_testlist',$data);
			$this->load->view('admin/practice_test/practice_testscripts');
		}
		else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function quiz_paper()
	{
		$data['page']='quiz paper';
		if($this->uri->segment(3)=='create'){
			$data['subpage']='Create quiz paper';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/quiz_paper/quiz_paper_create');
			$this->load->view('admin/quiz_paper/quiz_paper_scripts');
		}
		else if($this->uri->segment(3)=='list'){
			$data['subpage']='List quiz paper';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/quiz_paper/quiz_paper_list',$data);
			$this->load->view('admin/quiz_paper/quiz_paper_scripts');
		}
		else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function testpaper()
	{
		$data['page']='test series';
		if($this->uri->segment(3)!=''){
		    $data['subpage']='List test series';
		    $this->load->view('admin/adminheader',$data);
		    if($this->uri->segment(3)=='createtestpapers'){
		        if($this->uri->segment(4)!=''){
		            $data['tsdt'] = $this->Admingetmodel->get_testseriesdt_by_id($this->uri->segment(4));
		            $data['allsub'] = $this->Admingetmodel->get_all_subject();
		            $this->load->view('admin/test_paper_new/testpapernewscripts');
		            $this->load->view('admin/test_paper_new/subjecthtml',$data);
    			    
		        }else{
		            redirect('ratan/dashboard');
		        }
		    }else if($this->uri->segment(3)=='testpaperview'){
		        if($this->uri->segment(4)!=''){
		           $data['tsdt'] = $this->Admingetmodel->get_testseriesdt_by_id($this->uri->segment(5));
		            $data['tpdt'] = $this->Admingetmodel->get_testpaperdt_by_id($this->uri->segment(4));
		          //  $data['allsub'] = $this->Admingetmodel->get_all_subject();
		            $this->load->view('admin/test_paper_new/testpaperviewscripts');
		            $data['quesdt'] = $this->Admingetmodel->get_quesdt_by_id(implode(',',json_decode($data['tpdt'][0]->TP_QUESTIONS)));
		            $this->load->view('admin/test_paper_new/testpaperview',$data);
    			    
		        }else{
		            redirect('ratan/dashboard');
		        }
		    }else if($this->uri->segment(3)=='testpaperedit'){
		        if($this->uri->segment(4)!=''){
		           $data['tsdt'] = $this->Admingetmodel->get_testseriesdt_by_id($this->uri->segment(5));
		            $data['tpdt'] = $this->Admingetmodel->get_testpaperdt_by_id($this->uri->segment(4));
		            $data['allsub'] = $this->Admingetmodel->get_all_subject();
		            $this->load->view('admin/test_paper_new/testpapereditscript');
		            $data['addedques'] = implode(',',json_decode($data['tpdt'][0]->TP_QUESTIONS));
		          //  $data['quesdt'] = $this->Admingetmodel->get_quesdt_by_id(implode(',',json_decode($data['tpdt'][0]->TP_QUESTIONS)));
		            $this->load->view('admin/test_paper_new/testpaperedit',$data);
    			    
		        }else{
		            redirect('ratan/dashboard');
		        }
		    }else{
		        $data['tsdt'] = $this->Admingetmodel->get_testseriesdt_by_id($this->uri->segment(3));
    			$this->load->view('admin/test_paper/testpaperlist',$data);
    			$this->load->view('admin/test_paper/testpaperscripts');
		    }
			

		}else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	
	public function practicetestpaper()
	{
		$data['page']='Practice Test';
		if($this->uri->segment(3)!=''){
		    $data['subpage']='List Practice Test';
		    $this->load->view('admin/adminheader',$data);
		    if($this->uri->segment(3)=='create_practicetestpapers'){
		        if($this->uri->segment(4)!=''){
		            $data['tsdt'] = $this->Admingetmodel->get_practicetestdt_by_id($this->uri->segment(4));
		            $data['allsub'] = $this->Admingetmodel->get_all_subject();
		            $this->load->view('admin/practice_test_paper_new/testpapernewscripts');
		            $this->load->view('admin/practice_test_paper_new/subjecthtml',$data);
    			    
		        }else{
		            redirect('ratan/dashboard');
		        }
		    }else if($this->uri->segment(3)=='testpaperview'){
		        if($this->uri->segment(4)!=''){
		           	$data['tsdt'] = $this->Admingetmodel->get_practicetestdt_by_id($this->uri->segment(5));
		            $data['tpdt'] = $this->Admingetmodel->get_practicetestpaperdt_by_id($this->uri->segment(4));
		          //  $data['allsub'] = $this->Admingetmodel->get_all_subject();
		            $this->load->view('admin/practice_test_paper_new/testpaperviewscripts');
		            $data['quesdt'] = $this->Admingetmodel->get_quesdt_by_id(implode(',',json_decode($data['tpdt'][0]->TP_QUESTIONS)));
		            $this->load->view('admin/practice_test_paper_new/testpaperview',$data);
    			    
		        }else{
		            redirect('ratan/dashboard');
		        }
		    }else if($this->uri->segment(3)=='testpaperedit'){
		        if($this->uri->segment(4)!=''){
		           $data['tsdt'] = $this->Admingetmodel->get_practicetestdt_by_id($this->uri->segment(5));
		            $data['tpdt'] = $this->Admingetmodel->get_practicetestpaperdt_by_id($this->uri->segment(4));
		            $data['allsub'] = $this->Admingetmodel->get_all_subject();
		            $this->load->view('admin/practice_test_paper_new/testpapereditscript');
		            $data['addedques'] = implode(',',json_decode($data['tpdt'][0]->TP_QUESTIONS));
		          //  $data['quesdt'] = $this->Admingetmodel->get_quesdt_by_id(implode(',',json_decode($data['tpdt'][0]->TP_QUESTIONS)));
		            $this->load->view('admin/practice_test_paper_new/testpaperedit',$data);
    			    
		        }else{
		            redirect('ratan/dashboard');
		        }
		    }else{
		        $data['tsdt'] = $this->Admingetmodel->get_practicetestdt_by_id($this->uri->segment(3));
    			$this->load->view('admin/practice_test_paper/practice_testpaperlist',$data);
    			$this->load->view('admin/practice_test_paper/practice_testpaperscripts');
		    }
			

		}else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}

	public function member()
	{
		$data['page']='member';
		if($this->uri->segment(3)=='create'){
			$data['subpage']='Create member';
			$loginid = $this->session->userdata('adminloginid');
			$this->load->view('admin/adminheader',$data);
			$data['membertype'] = $this->Admingetmodel->get_all_member_type();
			$this->load->view('admin/member/membercreate',$data);
			$this->load->view('admin/member/memberscripts');
		}
		else if($this->uri->segment(3)=='list'){
			$data['subpage']='List member';
			$this->load->view('admin/adminheader',$data);
			$data['membertype'] = $this->Admingetmodel->get_all_member_type();
			$this->load->view('admin/member/memberlist',$data);
			$this->load->view('admin/member/memberscripts');
		}
		else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function student()
	{
		$data['page']='student';
		
		if($this->uri->segment(3)=='list'){
            $data['subpage']='list';
			$data['allts'] = $this->Admingetmodel->get_all_testseries(3);
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/student/list');
			$this->load->view('admin/student/studentscripts');
		}else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function performance()
	{
		$data['page']='student';
		$data['subpage']='list';
		if($this->uri->segment(3)!=''){
		    
		    
    			$attmptdt = $this->Admingetmodel->get_attempt_testpaper_dt($this->uri->segment(3));
    			$data['userdt'] = $this->Admingetmodel->get_admin_dt_model($this->uri->segment(3));
    			if(!empty($attmptdt)){
            	    $data['result'] = $this->Admingetmodel->get_attempt_testpaper_resultdt_andname($attmptdt);
            	}else{
            	    $data['result'] = "";
            	}
            	
    			$this->load->view('admin/adminheader',$data);
    			$this->load->view('admin/student/performance');
    			$this->load->view('admin/student/studentscripts');
		    
            
		}else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function coupon()
	{
		$data['page']='coupon';
		if($this->uri->segment(3)=='create'){
			$data['subpage']='Create coupon';
			$loginid = $this->session->userdata('adminloginid');
			$this->load->view('admin/adminheader',$data);
			$data['tsdt'] = $this->Admingetmodel->get_all_active_testseries(); 
			$data['prod_type'] = $this->Admingetmodel->get_all_prod_type(); 
			$this->load->view('admin/coupon/couponcreate',$data);
			$this->load->view('admin/coupon/couponscripts');
		}
		else if($this->uri->segment(3)=='list'){
			$data['subpage']='List coupon';
			$data['prod_type'] = $this->Admingetmodel->get_all_prod_type(); 
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/coupon/couponlist',$data);
			$this->load->view('admin/coupon/couponscripts');
		}
		else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
    public function message()
	{
		$data['page']='message';
		$data['alluser']=$this->Admingetmodel->get_user_by_userroleid(3);
		$data['alltemp']=$this->Admingetmodel->get_all_template(1);
		$data['tsdt']=$this->Admingetmodel->get_all_testseries();
        $data['subpage']='message';
		$this->load->view('admin/adminheader',$data);
		$this->load->view('admin/msg/message');
		$this->load->view('admin/msg/messagescripts');
		
		$this->load->view('admin/adminfooter');
	}
	public function mail()
	{
		$data['page']='mail';
		$data['alluser']=$this->Admingetmodel->get_user_by_userroleid(3);
		$data['alltemp']=$this->Admingetmodel->get_all_template(2);
		$data['tsdt']=$this->Admingetmodel->get_all_testseries();
        $data['subpage']='message';
		$this->load->view('admin/adminheader',$data);
		$this->load->view('admin/mail/mail');
		$this->load->view('admin/mail/mailscripts');
		
		$this->load->view('admin/adminfooter');
	}
	//A Start 11-04-2020
	public function course()
	{
		$data['page']='Course';
		if($this->uri->segment(3)=='create'){
			$data['subpage']='Create Course';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/course/coursecreate');
		}
		else if($this->uri->segment(3)=='list'){
			$data['subpage']='List Library';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/course/courselist',$data);
			$this->load->view('admin/course/courselistscript');
		}
		else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}

	public function slider()
	{
		$data['page']='slider';
		$data['prod_types'] = $this->Admingetmodel->get_all_prod_type();
		if($this->uri->segment(3)=='create'){
			$data['subpage']='Create slider';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/slider/slidercreate');
			$this->load->view('admin/slider/sliderscripts');
		}else if($this->uri->segment(3)=='list'){
			$data['subpage']='List Library';
			$this->load->view('admin/adminheader',$data);
			$this->load->view('admin/slider/sliderlist',$data);
			$this->load->view('admin/slider/sliderscripts');
		}else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
	public function sell()
	{
		$data['page']='sell';
		if($this->uri->segment(3)=='exam'){
			$data['subpage']='exam';
			$this->load->view('admin/adminheader',$data);
			if($this->uri->segment(4)=='addexams'){
				$this->load->view('admin/sell/exam/addexams');
			}else{
				$this->load->view('admin/sell/exam/listexams');
			}
			$this->load->view('admin/sell/exam/listexamscript');
		}else if($this->uri->segment(3)=='subject'){
			$data['subpage']='subject';
			$this->load->view('admin/adminheader',$data);
			if($this->uri->segment(4)=='addsubject'){
				$this->load->view('admin/sell/exam/addsubject');
			}else{
				$this->load->view('admin/sell/exam/listsubjects');
			}
			$this->load->view('admin/sell/exam/listsubjectsscript');
		}else if($this->uri->segment(3)=='book'){
			$data['subpage']='book';
			$$this->load->view('admin/adminheader',$data);
			if($this->uri->segment(4)=='addexams'){
				$this->load->view('admin/sell/book/addbook');
			}else{
				$this->load->view('admin/sell/book/listbooks');
			}
			$this->load->view('admin/sell/book/listbooksscript');
		}else{
			redirect('ratan/dashboard');
		}
		$this->load->view('admin/adminfooter');
	}
}
