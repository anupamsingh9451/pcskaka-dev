<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Sitemap
 */
class Sitemap extends CI_Controller
{
    /**
     * Main constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('common_helper');
    }
    /**
     * Main function for get links of each page | category | blog article
     */
    public function index()
    {
        $this->load->view('sitemap/sitemap');
    }
}