$('document').ready(function(){
  
	/* login user */
	$("#loginuser").on('submit', function (e) {
		e.preventDefault();
		var formdata = new FormData(this);
		$.ajax({
			type: 'POST',
			url: base_loc + 'userpostajax/userlogin',
			data: formdata,
			contentType: false,
			cache: false,
			processData: false,
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
			},
			beforeSend: function () {
				$('.loginuser').attr("disabled", "disabled");
				$('.loginuser').html("Please Wait...");
			},
			success: function (msg) {
				if(msg.message=="ok"){
					$('.mymsg').html("Login Successfull");
					$('.mymsg').removeClass('alert-warning');
					$('.mymsg').addClass('alert-success');
					$('.loginuser').attr("disabled", false);
					$('.loginuser').html("Sign me in");
					location.href=base_loc+'user/subject/list';
				}else{
				    if(msg.message=="Already Login"){
				        swal({
                            title: "Are you sure?",
                            text: "You are Loged in somewhere! Do you want to login?",
                            icon: "warning",
                            buttons: [
                                'No, cancel it!',
                                'Yes, I am sure!'
                            ],
                            dangerMode: true,
                        }).then(function(isConfirm) {
                            if (isConfirm) {
                               formdata.append('loginagain', '1');
                               $.ajax({
                        			type: 'POST',
                        			url: base_loc + 'userpostajax/userlogin',
                        			data: formdata,
                            		contentType: false,
                            		cache: false,
                            		processData: false,
                        			headers: {
                        				'Client-Service': clientserv,
                        				'Auth-Key': apikey,
                        			},
                        			success: function (msg) {
                        				if(msg.message=="ok"){
                        					$('.mymsg').html("Login Successfull");
                        					$('.mymsg').removeClass('alert-warning');
                        					$('.mymsg').addClass('alert-success');
                        					$('.loginuser').attr("disabled", false);
                        					$('.loginuser').html("Sign me in");
                        					location.href=base_loc+'user/subject/list';
                        				}else{
                        				    $('.mymsg').html(msg.message);
                        					$('.mymsg').addClass('alert-warning');
                        					$('.mymsg').removeClass('alert-success');
                        					$('.loginuser').attr("disabled", false);
                        					$('.loginuser').html("Sign me in");
                        				}
                        			},
                        			error: function (msg) {
                        				if (msg.responseJSON['status'] == 303) {
                        					location.href = base_loc;
                        				}
                        				if (msg.responseJSON['status'] == 401) {
                        					location.href = base_loc; 
                        				}
                        				if (msg.responseJSON['status'] == 400) {
                        					location.href = base_loc; 
                        				}
                        			}
                        		}); 
                            } else {
                                // swal("Cancelled", "Your Question is safe :)", "error");
                                $('.loginuser').attr("disabled", false);
					            $('.loginuser').html("Sign me in");
                            }
                        })
                        $(".swal-button--danger").css('background-color', '#00acac');
				    }else{
				        $('.mymsg').html(msg.message);
    					$('.mymsg').addClass('alert-warning');
    					$('.mymsg').removeClass('alert-success');
    					$('.loginuser').attr("disabled", false);
    					$('.loginuser').html("Sign me in");
				    }
					
				}
			},
			error: function (msg) {
				if (msg.responseJSON['status'] == 303) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 401) {
					location.href = base_loc; 
				}
				if (msg.responseJSON['status'] == 400) {
					location.href = base_loc; 
				}
			}
		});
	});
	/* for logout */
	$('.logoutuser').on('click', function () {
		$.ajax({
			type: 'POST',
			url: base_loc + 'userpostajax/userlogout',
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
			success: function (msg) {
				location.href = base_loc+'user';
			},
			error: function (msg) {
				if (msg.responseJSON['status'] == 303) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 401) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 400) {
					location.href = base_loc;
				}
			}
		});
	});
    /* for creating question user panel */
	$('body').on('submit','.create_quest', function (e) {
		e.preventDefault();
		$check = 0;
	    var obj_ques_explain = $(this).find('.question_explain').val();
	    var obj_options = $(this).find('.obj_options').val();
	    $i = 0;
	    $j = 0;
	    if(obj_options!='' && $.trim(obj_ques_explain)!=''){
	        $(this).find(".ques_option").each(function() {
                if($(this).val()!=''){
                    $j= $j+1;;
                }
            });
            $(this).find(".obj_answer").each(function() {
               if($(this).is(':checked')) {
                   $i = $i+1; 
               }
            });
	    }
        if($i>0 && $j==obj_options){   
            $check=1;
        }else{
            $check=0;
        }
        if($(this).find('#mysummernote').summernote('isEmpty')) {
            $check=0;
         }
        if($check==1){
    		var f=new FormData(this);
    		$.ajax({
    			type: 'POST',
    			url: base_loc + 'userpostajax/create_quest',
    			data:f,
    			contentType: false,
    			cache: false,
    			processData: false,
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    				'User-ID': loginid,
    				'Authorization': token,
    				'type': type
    			},
    			beforeSend: function () {
    				$('.questioncreate').attr("disabled", "disabled");
    				$('.questioncreate').html("Please Wait...");
    			},
    			success: function (msg) {
    				if(msg.data=="ok"){
    					GritterNotification("Success","QUESTION CREATED SUCCESSFULLY","my-sticky-class");
    					$('.questioncreate').attr("disabled",false);
    					$('.questioncreate').html("SAVE");
    				    $('#addquestion').modal('hide');
    				    var bookid = '';
    				    $(".table_tab").each(function() {
    				        if($(this).hasClass('active')){
    				            bookid = $(this).attr('attr-book-id');
    				        }
    				    });
    					$('.table_tab[attr-book-id="'+bookid+'"]').trigger('click');
    				}else{
    					GritterNotification("Error",msg.message,"my-sticky-class");
    					$('.questioncreate').attr("disabled",false);
    					$('.questioncreate').html("SAVE");	
    				}
    			},
    			error: function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc;
    				}
    			}
    		});
        }else{
		    GritterNotification("Error","Please Fill All Input","my-sticky-class");
		}
	});
    
    /* for Updating question user panel */
	$('body').on('submit','.questionedit', function (e) {
		e.preventDefault();
		$check = 0;
	    var obj_ques_explain = $(this).find('.obj_ques_explain').val();
	    var obj_options = $(this).find('.edit_obj_options').val();
	    var bookid = $(this).find('.bookid').val();
	    $i = 0;
	    $j = 0;
	    if(obj_options!='' && $.trim(obj_ques_explain)!=''){
	        $(this).find(".ques_option").each(function() {
                if($(this).val()!=''){
                    $j= $j+1;;
                }
            });
            $(this).find(".obj_answer").each(function() {
               if($(this).is(':checked')) {
                   $i = $i+1; 
               }
            });
	    }
	   
        if($i>0 && $j==obj_options){   
            $check=1;
        }else{
            $check=0;
        }
        if($(this).find('#mysummernote').summernote('isEmpty')) {
            $check=0;
         }
        if($check==1){
    		var f=new FormData(this);
    		$.ajax({
    			type: 'POST',
    			url: base_loc + 'userpostajax/update_quest',
    			data:f,
    			contentType: false,
    			cache: false,
    			processData: false,
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    				'User-ID': loginid,
    				'Authorization': token,
    				'type': type
    			},
    			beforeSend: function () {
    				$('.questioneditbtn').attr("disabled", "disabled");
    				$('.questioneditbtn').html("Please Wait...");
    			},
    			success: function (msg) {
    				if(msg.data=="ok"){
    					GritterNotification("Success","QUESTION UPDATED SUCCESSFULLY","my-sticky-class");
    					$('.questionedit')[0].reset();
    					$('.questioneditbtn').attr("disabled",false);
    					$('.questioneditbtn').html("SAVE");
    					$('.book-tab[attr-book-id="'+bookid+'"]').trigger('click');
    					console.log(bookid);
    					$('#editquestion').modal('hide');
    					
    				}else{
    					GritterNotification("Error",msg.message,"my-sticky-class");
    					$('.questioneditbtn').attr("disabled",false);
    					$('.questioneditbtn').html("SAVE");	
    				}
    			},
    			error: function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc;
    				}
    			}
    		});
        }else{
		    GritterNotification("Error","Please Fill All Input","my-sticky-class");
		}
	});
    

	
});