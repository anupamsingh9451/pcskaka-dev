$('document').ready(function(){
	App.init();
	InboxV2.init();
	DashboardV2.init();
	TableManageCombine.init();
	FormPlugins.init();
	FormWizardValidation.init();
	if ($("#bar-chart").length > 0) {
		ChartJs.init();
	}

	$('body').tooltip({
		selector: '.trashbtn,.addmoremodel,.toolstips'
	});
	/* get admin full details */

	$.ajax({
		type: 'GET',
		url: base_loc + 'usergetajax/get_user_dt',
		data:'id='+loginid,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
		    
			if(msg.status==200){
				$('.admin_name_upper').html(msg.data[0].USER_EMAIL);
				$('.admin_address_upper').html(msg.data[0].USER_ADDRESS);
			}else{
				location.href = base_loc;
			}
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc; 
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc; 
			}
		}
	});
	
	/* get library full details */
	
	
	

});
/*
function GritterNotification(titles,texts,classnm){
	$.gritter.add({
		title:titles,
		text:texts,
		//image:base_loc+"assets/themeassets/img/user/user-12.jpg",
		sticky:!0,
		time:"",
		class_name:classnm
	});
}
*/
function getlibrary(){
    $.ajax({
		type: 'GET',
		url: base_loc + 'admingetajax/getlibrary',
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
		    $('.library option').remove();
			var str='<option value="">--Select Library--</option>';
			if(msg.status==200){
				if(msg.data.length>0){
					$.each(msg.data, function(index, element) {
						str+='<option value="'+element.LIB_ID+'">'+element.LIB_NAME+'</option>';
					});
				}
				$('.library').append(str);
			}else{
				location.href = base_loc;
			}
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc; 
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc; 
			}
		}
	});
}

function readimage(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        $('.imagesrc').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}
function GritterNotification(titles,texts,classnm){
		if(titles=="Success"){
			var types='success';
			var btntext='Success';
			var colorcode='#469408';
		}else{
			var types='warning';
			var btntext='Warning';
			var colorcode='#f99b4a';
		}
		if(titles=="Success"){
		    swal({
    			title:titles,
    			text: texts,
    			icon:types,
    			buttons: {
    				confirm: {
    					text: 'ok',
    					value: !0,
    					visible: !0,
    					className: "btn btn-"+types,
    					closeModal: !0
    				}
    			}
            });  
		}else{
    		swal({
    			title:titles,
    			text: texts,
    			icon:types,
    			buttons: {
    				confirm: {
    					text: 'ok',
    					value: !0,
    					visible: !0,
    					className: "btn btn-"+types,
    					closeModal: !0
    				}
    			}
            });  
		}
		
		 
		
}
function expirydate(noofday){
	var dt = new Date();
	dt.setDate(dt.getDate() + noofday);
	var mnth=dt.getMonth()+1;
	var date=dt.getDate();
	
	if(mnth<10){
	   mnth='0'+mnth;
	}
	if(date<10){
	   date='0'+dt.getDate();
	}
	var expdt=dt.getFullYear()+"-"+mnth+"-"+date;
	
	return expdt;
}
function currentdate(){
	var dt = new Date();
	var mnth=dt.getMonth()+1;
	
	var date=dt.getDate();
	if(mnth<10){
	   mnth='0'+mnth;
	}
	if(date<10){
	   date='0'+dt.getDate();
	}
	var date=dt.getFullYear()+"-"+mnth+"-"+date;
	return date;
}
