$(document).ready(function() {
    Notification.init();

    function callnotification(status, message) {
        return $.gritter.add({
            title: status,
            text: message,
            class_name: "gritter-danger",
        })
    }
    var productcart = [];
    $('body').on('click', '.addtocart', function() {
        $(this).removeClass('addtocart').addClass('removefromcart');
        $(this).html('<i class="fa fa-minus"></i> REMOVE FROM CART');
        producttype = +$(this).attr('attr-prodtype');
        productid = $(this).attr('attr-prodid');
        details = [];
        details['product_type'] = producttype;
        details['product_id'] = productid;
        details['product_price'] = $(this).attr('attr-prodcost');
        details['discount_price'] = $(this).attr('attr-discountamt');;
        details['product_name'] = $(this).attr('attr-prodname');
        details['after_discount'] = $(this).attr('attr-totalamt');
        if (productcart.length == 0) {
            productcart.push(details);
        } else {
            filter = true;
            $.each(productcart, function(index, element) {
                if (element.product_type == producttype) {
                    if (element.product_id == productid) {
                        return filter = false;
                    }
                }
            });
            if (filter == true) {
                productcart.push(details);
            }
        }
        if (productcart.length > 0) {
            $('.buyallcartproduct').html('<li class="breadcrumb-item"><button class="btn btn-danger m-r-5 buy_cart_prod" href="#modal-prucahseproduct"  data-toggle="modal"><i class="fa fa-lock"></i> Buy Now </button></li>');
        }
    });
    $('body').on('click', '.removefromcart', function() {
        $(this).addClass('addtocart').removeClass('removefromcart');
        $(this).html('<i class="fa fa-plus"></i> ADD TO CART');
        producttype = +$(this).attr('attr-prodtype');
        productid = $(this).attr('attr-prodid');
        details = [];
        details['product_type'] = producttype;
        details['product_id'] = productid;
        if (productcart.length == 0) {
            productcart.push(details);
        } else {
            filter = true;
            count = 0;
            $.each(productcart, function(index, element) {
                if (element.product_type == producttype) {
                    if (element.product_id == productid) {
                        productcart[index] = '';
                    }
                }
                if (productcart[index] === "") {
                    count++;
                }
            });
        }
        if (count == productcart.length) {
            $('.buyallcartproduct').html('');
        }
    });
    $('body').on('click', '.buyallcartproduct', function() {
        html = "";
        total = 0;
        totalcharge = totaldiscount = totaltax = totalpay = 0;
        allproductid = "";
        allprodtype = [];
        $.each(productcart, function(index, element) {
            if (productcart[index] !== "") {
                total = parseFloat(element.product_price - element.discount_price).toFixed(2);
                subtotal = parseFloat(total);
                totalpay = parseFloat(subtotal) + parseFloat(totalpay);
                allproductid += element.product_id + ",";
                product_type_name = "";
                if(element.product_type == 0){
                    product_type_name = "Course";
                }else if(element.product_type == 1){
                    product_type_name = "Exams";
                }else if(element.product_type == 2){
                    product_type_name = "Subject";
                }else if(element.product_type == 3){
                    product_type_name = "Book";
                }else if(element.product_type == 4){
                    product_type_name = "Test Series";
                }
                html += "<tr>" +
                    "<td><span class='text-inverse word-break'>[" +  product_type_name + "] " + element.product_name + "</span></td>" +
                    "<td class='text-right' attr-productid='" + element.product_id + "' attr-producttype='" + element.product_type + "'  >₹" + element.product_price + "</td>" +
                    "<td class='text-right'>₹" + parseFloat(element.discount_price) + "</td>" +
                    "<td class='text-right'>₹" + subtotal + "</td>" +
                    "</tr>";
            }
        });
        tfoot = "";
        tfoot += "<tr><th colspan='3'>SUBTOTAL</th><td>₹" + totalpay + "</td></tr>";
        tfoot += "<tr>" +
            "<td colspan='3'>" +
            "<div class='row'> " +
            "<select name='couponcode' class='form-control couponcode col-md-3 col-6' placeholder='Apply Coupon'>" +
            "</select>" +
            "<div class='col-md-9 col-6 coupon-desc word-break'></div>" +
            "</div>" +
            "</td>" +
            "<td>₹0</td></tr>";
        $('.buycarttable tbody').html(html);
        $('.buycarttable tfoot').html(tfoot);
        allproductid = allproductid.replace(/,(?=\s*$)/, '');;
        $('.apply-tsid').val(allproductid);
        $('.subtotal , .totalpay').html('₹' + totalpay);
        getallcoupons();
    });
    $('body').on('change', '.couponcode', function() {
        optionvalue = $(this).val();
        description = "";
        totaldiscount = 0;
        $('tfoot tr:eq(1)').find("td:eq(1)").html('₹' + totaldiscount);
        $('tfoot tr:eq(1)').find("td:eq(0)").find('.coupon-desc').html(description);
        if ($(this).val() != "") {
            var cmode = $(this).find('option:selected').attr('attr_coupon_mode');
            var products = $(this).find('option:selected').attr('attr_products');
            products = products.split(',');
            var ptype = $(this).find('option:selected').attr('attr_prodtype');
            var prodtype = ptype;
            var cvalue = $(this).find('option:selected').attr('attr_cvalue');
            var description = $(this).find('option:selected').attr('attr_desc');
            $('.buycarttable > tbody  > tr').each(function(index, tr) {
                ptype = $(this).find("td:eq(1)").attr('attr-producttype');
                pid = $(this).find("td:eq(1)").attr('attr-productid');
                bprice = $(this).find("td:eq(1)").text().substring(1);
                subtotal = $(this).find("td:eq(3)").text().substring(1);
                if (prodtype.split(',').length == 1) {
                    if ($.inArray(pid, products) != -1) {
                        discount = 0;
                        if (cmode == 1) {
                            discount = cvalue;
                        } else {
                            discount = cvalue * subtotal / 100;
                        }
                        totaldiscount = (parseFloat(totaldiscount) + parseFloat(discount)).toFixed(2);
                        $(this).find("td:eq(1)").attr('attr-couponid', optionvalue);
                    } else {
                        $(this).find("td:eq(1)").attr('attr-couponid', '');
                    }
                }
                if (prodtype.split(',').length > 1) {
                    discount = 0;

                    if (cmode == 1) {
                        discount = cvalue;
                    } else {
                        discount = cvalue * subtotal / 100;
                    }
                    totaldiscount = (parseFloat(totaldiscount) + parseFloat(discount)).toFixed(2);
                    $(this).find("td:eq(1)").attr('attr-couponid', optionvalue);
                }
            });
        } else {
            $('.buycarttable > tbody  > tr').each(function(index, tr) {
                $(this).find("td:eq(1)").attr('attr-couponid', '');
            });
            //$(".buycarttable tbody tr td:eq(1)").removeAttr('attr-couponid');
        }
        totalprice = $('tfoot tr:eq(0)').find("td:eq(0)").text().substring(1);
        totalpay = (parseFloat(totalprice) - parseFloat(totaldiscount)).toFixed(2);
        $('tfoot tr:eq(1)').find("td:eq(1)").html('₹' + totaldiscount);
        $('tfoot tr:eq(1)').find("td:eq(0)").find('.coupon-desc').html(description);
        $('.subtotal , .totalpay').html('₹' + totalpay);
    });
    $('body').on('click', '.checkout', function() {
        allbuyproduct = [];
        $('.buycarttable tbody tr').each(function(i) {
            productype = $(this).find("td:eq(1)").attr('attr-producttype');
            productid = $(this).find("td:eq(1)").attr('attr-productid');
            productcouponid = $(this).find("td:eq(1)").attr('attr-couponid');
            allbuyproduct.push({ 'productid': productid, 'producttype': productype, 'coupnid': productcouponid });
        });
        $.ajax({
            type: 'GET',
            url: base_loc + 'studentgetajax/get_payumoney_details_products',
            data: { 'allproducts': allbuyproduct },
            async: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            success: function(msg) {
                console.log(msg);
                if (msg.status == 200 && msg.message == "ok") {
                    var data = msg.data;
                    launchBOLTS(data.key, data.txnid, data.hash, data.amount, data.fname, data.email, data.phone, data.pinfo, data.udf1, data.udf2, data.udf3, data.udf5);
                } else {
                    GritterNotification("Error", 'Payment Already Done!', "my-sticky-class");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
});

function launchBOLTS(keys, txnsid, hashes, amt, fname, emails, phones, pinfo, udf1, udf2, udf3, udf5) {
    bolt.launch({
        'key': keys,
        'txnid': txnsid,
        'hash': hashes,
        'amount': amt,
        'firstname': fname,
        'email': emails,
        'phone': phones,
        'productinfo': pinfo,
        'udf5': udf5,
        'udf1': udf1,
        'udf2': udf2,
        'udf3': udf3,
        'surl': $('#surl').val(),
        'furl': $('#furl').val(),
        'mode': 'dropout'
    }, {
        responseHandler: function(BOLT) {
            console.log(BOLT.response.txnStatus);
            if (BOLT.response.txnStatus != 'CANCEL') {
                //Salt is passd here for demo purpose only. For practical use keep salt at server side only.
                var fr = '<form action=\"' + $('#surl').val() + '\" method=\"post\">' +
                    '<input type=\"hidden\" name=\"key\" value=\"' + BOLT.response.key + '\" />' +
                    '<input type=\"hidden\" name=\"salt\" value=\"' + $('#salt').val() + '\" />' +
                    '<input type=\"hidden\" name=\"txnid\" value=\"' + BOLT.response.txnid + '\" />' +
                    '<input type=\"hidden\" name=\"amount\" value=\"' + BOLT.response.amount + '\" />' +
                    '<input type=\"hidden\" name=\"productinfo\" value=\"' + BOLT.response.productinfo + '\" />' +
                    '<input type=\"hidden\" name=\"firstname\" value=\"' + BOLT.response.firstname + '\" />' +
                    '<input type=\"hidden\" name=\"email\" value=\"' + BOLT.response.email + '\" />' +
                    '<input type=\"hidden\" name=\"udf5\" value=\"' + BOLT.response.udf5 + '\" />' +
                    '<input type=\"hidden\" name=\"udf1\" value=\"' + BOLT.response.udf1 + '\" />' +
                    '<input type=\"hidden\" name=\"udf2\" value=\"' + BOLT.response.udf2 + '\" />' +
                    '<input type=\"hidden\" name=\"udf3\" value=\"' + BOLT.response.udf3 + '\" />' +
                    '<input type=\"hidden\" name=\"mihpayid\" value=\"' + BOLT.response.mihpayid + '\" />' +
                    '<input type=\"hidden\" name=\"status\" value=\"' + BOLT.response.status + '\" />' +
                    '<input type=\"hidden\" name=\"hash\" value=\"' + BOLT.response.hash + '\" />' +
                    '</form>';
                var form = jQuery(fr);
                jQuery('body').append(form);
                form.submit();
            }
        },
        catchException: function(BOLT) {
            alert(BOLT.message);
        }
    });
}

function getallcoupons() {
    allbuyproduct = [];
    allproducttype = [];
    $('.buycarttable tbody tr').each(function(i) {
        productype = $(this).find("td:eq(1)").attr('attr-producttype');
        productid = $(this).find("td:eq(1)").attr('attr-productid');
        allbuyproduct.push({ 'productid': productid, 'producttype': productype });
    });
    $.ajax({
        type: 'GET',
        url: base_loc + 'Studentgetajax/getcouponcodes',
        data: { 'allproducts': allbuyproduct },
        headers: {
            'Client-Service': clientserv,
            'Auth-Key': apikey,
            'User-ID': loginid,
            'Authorization': token,
            'type': type
        },
        success: function(msg) {
            html = "";
            if (msg.data.length > 0) {
                html += '<option value="" >-- Apply Coupon --</option>';
                $.each(msg.data, function(index, element) {
                    html += '<option value="' + element.ID + '" attr_coupon_mode="' + element.MODE + '" attr_products="' + element.PROD_IDS + '" attr_prodtype="' + element.PROD_TYPE_ID + '" attr_cvalue="' + element.VALUE + '" attr_desc="' + (element.DESCRIPTION.replace(/'/g, '')).replace('"', '') + '">' + element.TITLE + '</option>';
                });
                $('.couponcode').html(html);
            } else {
                html += '<option value="" >-- No Coupon Found --</option>';
                $('.couponcode').html(html);
            }
        },
        error: function(msg) {
            if (msg.responseJSON['status'] == 303) {
                location.href = base_loc;
            }
            if (msg.responseJSON['status'] == 401) {
                location.href = base_loc;
            }
            if (msg.responseJSON['status'] == 400) {
                location.href = base_loc;
            }
        }
    });
}