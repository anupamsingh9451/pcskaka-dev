$('document').ready(function() {

    $.ajax({
        type: 'GET',
        url: base_loc + 'studentgetajax/get_student_dt',
        data: 'id=' + loginid,
        headers: {
            'Client-Service': clientserv,
            'Auth-Key': apikey,
            'User-ID': loginid,
            'Authorization': token,
            'type': type
        },
        success: function(msg) {

            if (msg.status == 200) {
                $('.name').html(msg.data[0].USER_FNAME + ' ' + msg.data[0].USER_LNAME);
                $('.student_name_upper').html(msg.data[0].USER_EMAIL);
                $('.student_address_upper').html(msg.data[0].USER_ADDRESS);
                $('.student_mobileno').html(msg.data[0].USER_CONTACT);
                $('.student_address_html').html(msg.data[0].USER_ADDRESS);
            } else {
                location.href = base_loc;
            }
        },
        error: function(msg) {
            if (msg.responseJSON['status'] == 303) {
                location.href = base_loc;
            }
            if (msg.responseJSON['status'] == 401) {
                location.href = base_loc;
            }
            if (msg.responseJSON['status'] == 400) {
                location.href = base_loc;
            }
        }
    });
});



function GritterNotification(titles, texts, classnm) {
    if (titles == "Success") {
        var types = 'success';
        var btntext = 'Success';
        var colorcode = '#469408';
    } else {
        var types = 'warning';
        var btntext = 'Warning';
        var colorcode = '#f99b4a';
    }
    swal({
        title: titles,
        text: texts,
        icon: types,
        buttons: {
            confirm: {
                text: 'ok',
                value: !0,
                visible: !0,
                className: "btn btn-" + types,
                closeModal: !0
            }
        }
    });


}