$('document').ready(function(){
  /* Registration Student */
	$("#student_reg").on('submit', function (e) {
		e.preventDefault();
		var name = $(this).find('.name').val();
		var email = $(this).find('.regemail').val();
		var emailcnfrm = $(this).find('.emailcnfrm').val();
		var regpassword = $(this).find('.regpassword').val();
		var cnfrmpassword = $(this).find('.cnfrmpassword').val();
		var mobile = $(this).find('.mobile').val();
		if(name!='' && email!='' && emailcnfrm!='' && regpassword!='' && cnfrmpassword!='' && mobile!=''){
    		$.ajax({
    			type: 'POST',
    			url: base_loc + 'studentpostajax/studentreg',
    			data:new FormData(this),
    			contentType: false,
    			cache: false,
    			processData: false,
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    			},
    			beforeSend: function () {
    				$('.btn-register').attr("disabled", "disabled");
    				$('.btn-register').html("Please Wait...");
    			},
    			success: function (msg) {
    			    $('.btn-register').attr("disabled", false);
    			    $('.btn-register').html("Create Account");
    				if(msg.status==200 && msg.message=='ok'){
                        $('.insertid').val(msg.data);
                        showOtpForm();
    				}else{
    					GritterNotification("Error",msg.message,"my-sticky-class");
    				}
    			},
    			error: function (msg) {
    			    $('.btn-register').attr("disabled", false);
    			    $('.btn-register').html("Create Account");
    				// if (msg.responseJSON['status'] == 303) {
    				// 	location.href = base_loc;
    				// }
    				// if (msg.responseJSON['status'] == 401) {
    				// 	location.href = base_loc; 
    				// }
    				// if (msg.responseJSON['status'] == 400) {
    				// 	location.href = base_loc; 
    				// }
    			}
    		});
	    }else{
	        
	        $('.btn-register').attr("disabled", false);
    		$('.btn-register').html("Create Account");
    		if(name==''){
    		    $("#student_reg").find('.name').addClass('redborder');
    		}
    		if(email==''){
    		    $("#student_reg").find('.regemail').addClass('redborder');
    		}
    		if(emailcnfrm==''){
    		    $("#student_reg").find('.emailcnfrm').addClass('redborder');
    		}
    		if(regpassword==''){
    		    $("#student_reg").find('.regpassword').addClass('redborder');
    		}
    		if(cnfrmpassword==''){
    		    $("#student_reg").find('.cnfrmpassword').addClass('redborder');
    		}
    		if(mobile==''){
    		    $("#student_reg").find('.mobile').addClass('redborder');
    		}
	    }
	});
	
	/* Submit OTP */
	$("#submit_otp").on('submit', function (e) {
		e.preventDefault();
		var otp = $(this).find('.otp').val();
		if(otp!=''){
		    var pass = $('#student_reg').find('.regpassword').val();
		    var formdata = new FormData(this);
		    formdata.append('password',pass);
    		$.ajax({
    			type: 'POST',
    			url: base_loc + 'studentpostajax/otpsubmit',
    			data:formdata,
    			contentType: false,
    			cache: false,
    			processData: false,
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    			},
    			beforeSend: function () {
    				$('.btn-otp').attr("disabled", "disabled");
    				$('.btn-otp').html("Please Wait...");
    			},
    			success: function (msg) {
    			    $('.btn-otp').attr("disabled", false);
    			    $('.btn-otp').html("Submit");
    				if(msg.status==200 && msg.message=='ok'){
    				    // location.href=base_loc+'student/dashboard';
    				    showLoginForm();
    				    GritterNotification("Success",'Registration Successfull',"my-sticky-class");
    				}else{
    					GritterNotification("Error",msg.message,"my-sticky-class");
    					$('.otpresendtmsg').css({'display':'none'});
    				}
    			},
    			error: function (msg) {
    			    $('.btn-otp').attr("disabled", false);
    			    $('.btn-otp').html("Submit");
    				
    			}
    		});
	    }else{
	        $(this).find('.otp_error').css({ "display": "block"});
	        $('.btn-otp').attr("disabled", false);
    	    $('.btn-otp').html("Submit");
	    }
	});
	
	/* Resend OTP */
	$("#resendOtp").on('click', function () {
		
		var insertid = $('#submit_otp').find('.insertid').val();
    		$.ajax({
    			type: 'POST',
    			url: base_loc + 'studentpostajax/resendotp',
    			data:{'userid':insertid},
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    			},
    			beforeSend: function () {
    				$('#resendOtp').attr("disabled", "disabled");
    				$('.btn-otp').attr("disabled", "disabled");
    				$('.btn-otpforgotpass').attr("disabled", "disabled");
    			},
    			success: function (msg) {
    			    $('.btn-otpforgotpass').attr("disabled", false);
    			    $('.btn-otp').attr("disabled", false);
    			    $('#resendOtp').attr("disabled", false);
    				if(msg.status==200 && msg.message=='ok'){
                        $('.emailsentmsg').css({'display':'none'});
                        $('.otpresendtmsg').css({'display':'block'});
    				}else{
    					GritterNotification("Error",msg.message,"my-sticky-class");
    				}
    			},
    			error: function (msg) {
    			    $('.btn-otp').attr("disabled", false);
    			    $('#resendOtp').attr("disabled", false);
    			    $('.btn-otpforgotpass').attr("disabled", false);
    			}
    		});
	   
	});
	
	/* login Student */
	$("#student_login").on('submit', function (e) {
		e.preventDefault();
		var email = $(this).find('.logemail').val();
		var password = $(this).find('.logpassword').val();
		if(email!='' && password!=''){
		     var formdata = new FormData(this);
    		$.ajax({
    			type: 'POST',
    			url: base_loc + 'studentpostajax/studentlogin',
    			data: formdata,
    			contentType: false,
    			cache: false,
    			processData: false,
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    			},
    			beforeSend: function () {
    				$('.btn-login').attr("disabled", "disabled");
    				$('.btn-login').html("Please Wait...");
    			},
    			success: function (msg) {
    				if(msg.message=="ok"){
    					$('.login_error').html('');
        				$('.login_error').css({ "display": "none"});
    					$('.btn-login').attr("disabled", false);
    					$('.btn-login').html("Sign me in");
    					location.href=base_loc+'student/dashboard';
    				}else{
    					if(msg.message=="Already Login"){
    					    $('.login_error').html('');
        					$('.login_error').css({ "display": "none"});
    				        swal({
                                title: "Are you sure?",
                                text: "You are Loged in somewhere! Do you want to login?",
                                icon: "warning",
                                buttons: [
                                    'No, cancel it!',
                                    'Yes, I am sure!'
                                ],
                                dangerMode: true,
                            }).then(function(isConfirm) {
                                if (isConfirm) {
                                   formdata.append('loginagain','1');
                                   $.ajax({
                            			type: 'POST',
                            		    url: base_loc + 'studentpostajax/studentlogin',
                            			data: formdata,
                            			contentType: false,
                                		cache: false,
                                		processData: false,
                            			headers: {
                            				'Client-Service': clientserv,
                            				'Auth-Key': apikey,
                            			},
                            			success: function (msg) {
                        			        $('.btn-login').attr("disabled", false);
                        					$('.btn-login').html("Sign me in");
                            				if(msg.message=="ok"){
                            					location.href=base_loc+'student/home';
                            				}else{
                            				    $('.login_error').html(msg.message);
                            					$('.login_error').css({ "display": "block"});
                            				}
                            			},
                            			error: function (msg) {
                            				if (msg.responseJSON['status'] == 303) {
                            					location.href = base_loc;
                            				}
                            				if (msg.responseJSON['status'] == 401) {
                            					location.href = base_loc; 
                            				}
                            				if (msg.responseJSON['status'] == 400) {
                            					location.href = base_loc; 
                            				}
                            			}
                            		}); 
                                } else {
                                    // swal("Cancelled", "Your Question is safe :)", "error");
                                    $('.btn-login').attr("disabled", false);
    					            $('.btn-login').html("Sign me in");
                                }
                            })
                            $(".swal-button--danger").css('background-color', '#00acac');
    				    }else{
    				        $('.login_error').html(msg.message);
        					$('.login_error').css({ "display": "block"});
        					$('.btn-login').attr("disabled", false);
        					$('.btn-login').html("Sign me in");
    				    }
    				}
    			},
    			error: function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
    		});
		}else{
            $('.login_error').html('Please Enter Email and Password First!');
    		$('.login_error').css({ "display": "block"});
    		$('.btn-login').attr("disabled", false);
    		$('.btn-login').html("Sign me in");
		}
	});
	
	/* Forgot password Student */
	$("#forgot_pass_form").on('submit', function (e) {
		e.preventDefault();
		var email = $(this).find('.forgotemail').val();
		if(isEmail(email) && email!=''){
    		$.ajax({
    			type: 'POST',
    			url: base_loc + 'studentpostajax/studentforgotpass',
    			data: new FormData(this),
    			contentType: false,
    			cache: false,
    			processData: false,
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    			},
    			beforeSend: function () {
    				$('.btn-forgotpass').attr("disabled", "disabled");
    				$('.btn-forgotpass').html("Please Wait...");
    			},
    			success: function (msg) {
    			    $('.mymsg').removeClass('alert-warning');
					$('.mymsg').addClass('alert-success');
					$('.btn-forgotpass').attr("disabled", false);
					$('.btn-forgotpass').html("Submit");
    				if(msg.status==200 && msg.message=='ok'){
    					$('.insertid').val(msg.data);
                        showOtpFormforgotpass();
    				}else{
    					$('.forgotpass_error').html(msg.message);
    					$('.forgotpass_error').css({ "display": "block"});
    					$('.btn-forgotpass').attr("disabled", false);
    					$('.btn-forgotpass').html("Submit");
    				}
    			},
    			error: function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc; 
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc; 
    				}
    			}
    		});
		}else{
            $('.forgotpass_error').html('Please Enter Valid Email!');
    		$('.forgotpass_error').css({ "display": "block"});
    		$('.btn-forgotpass').attr("disabled", false);
    		$('.btn-forgotpass').html("Submit");
		}
	});
	/* Submit OTP For Forgot Password */
	$("#submit_otp_forgotpass").on('submit', function (e) {
		e.preventDefault();
		var otp = $(this).find('.otp').val();
		if(otp!=''){
		    var formdata = new FormData(this);
    		$.ajax({
    			type: 'POST',
    			url: base_loc + 'studentpostajax/forgotpassotpsubmit',
    			data:formdata,
    			contentType: false,
    			cache: false,
    			processData: false,
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    			},
    			beforeSend: function () {
    				$('.btn-otpforgotpass').attr("disabled", "disabled");
    				$('.btn-otpforgotpass').html("Please Wait...");
    			},
    			success: function (msg) {
    			    $('.btn-otpforgotpass').attr("disabled", false);
    			    $('.btn-otpforgotpass').html("Submit");
    				if(msg.status==200 && msg.message=='ok'){
    				    $('.otp_error_forgotpass').css({'display':'none'});
    					$('.otp_error_forgotpass').html('');
    				    showForgotConfirmPassForm();
    				    console.log(msg.data);
    				}else{
    					$('.otpresendtmsg').css({'display':'none'});
    					$('.otp_error_forgotpass').css({'display':'block'});
    					$('.otp_error_forgotpass').html(msg.message);
    				}
    			},
    			error: function (msg) {
    			    $('.btn-otpforgotpass').attr("disabled", false);
    			    $('.btn-otpforgotpass').html("Submit");
    				
    			}
    		});
	    }else{
	        $(this).find('.otp_error_forgotpass').html('Please Enter OTP!');
	        $(this).find('.otp_error_forgotpass').css({ "display": "block"});
	        $('.btn-otpforgotpass').attr("disabled", false);
    	    $('.btn-otpforgotpass').html("Submit");
	    }
	});
	/* Submit Password For Forgot OTP */
	$("#forgot_confirmpass_form").on('submit', function (e) {
		e.preventDefault();
		var regpasswordforgot = $(this).find('.regpasswordforgot').val();
		var cnfrmpasswordforgot = $(this).find('.cnfrmpasswordforgot').val();
		if(cnfrmpasswordforgot!='' && regpasswordforgot!=''){
		    var formdata = new FormData(this);
    		$.ajax({
    			type: 'POST',
    			url: base_loc + 'studentpostajax/forgotpasswordsubmit',
    			data:formdata,
    			contentType: false,
    			cache: false,
    			processData: false,
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    			},
    			beforeSend: function () {
    				$('.btn-forgot-confirmpas').attr("disabled", "disabled");
    				$('.btn-forgot-confirmpas').html("Please Wait...");
    			},
    			success: function (msg) {
    			    $('.btn-forgot-confirmpas').attr("disabled", false);
    			    $('.btn-forgot-confirmpas').html("Submit");
    				if(msg.status==200 && msg.message=='ok'){
    				    showLoginForm();
    				    GritterNotification("Success",'Reset Password Successfull',"my-sticky-class");
    				}else{
    					GritterNotification("Error",msg.message,"my-sticky-class");
    				}
    			},
    			error: function (msg) {
    			    $('.btn-forgot-confirmpas').attr("disabled", false);
    			    $('.btn-forgot-confirmpas').html("Submit");
    				
    			}
    		});
	    }else{
	        $(this).find('.forgot_confirmpass_error').html('Please Enter Password!');
	        $(this).find('.forgot_confirmpass_error').css({ "display": "block"});
	        $('.btn-forgot-confirmpas').attr("disabled", false);
    		$('.btn-forgot-confirmpas').html("Submit");
	    }
	});
	/* for logout */
	$('.logoutstudent').on('click', function () {
		$.ajax({
			type: 'POST',
			url: base_loc + 'studentpostajax/logoutstudent',
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
			success: function (msg) {
				location.href = base_loc;
			},
			error: function (msg) {
				if (msg.responseJSON['status'] == 303) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 401) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 400) {
					location.href = base_loc;
				}
			}
		});
	});
	$('#editprofile').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: base_loc + 'studentpostajax/editprofile',
            data:new FormData(this),
            contentType: false,
			cache: false,
			processData: false,
            headers: {
                'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
            },
            beforeSend: function () {
                $('.edit_profile').attr("disabled", "disabled");
                $('.edit_profile').html("Please Wait...");
            },
            success: function (msg) { 
                if(msg.message=="ok" && msg.status==200){
                    GritterNotification("Success",'Profile Updated Successfully',"my-sticky-class");
                    location.reload();
                    $('.edit_profile').attr("disabled",false);
                    $('.edit_profile').html("SAVE");
                }
                else{
                    GritterNotification("Error",msg.message,"my-sticky-class"); 
                    $('.edit_profile').attr("disabled",false);
                    $('.edit_profile').html("SAVE");
                }
            },
            error: function (msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_url;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_url;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_url;
                }
            }
        });
    });
	
	$('#changepwd').on('submit', function (e) {

        e.preventDefault();
        var d=new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'studentpostajax/changepwd',
            data:d,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
            beforeSend: function () {
                $('.changepwd').attr("disabled", "disabled");
                $('.changepwd').html("Please Wait...");
            },
            success: function (msg) { 
                if(msg.data=="ok"){
                    GritterNotification("Success",msg.message,"my-sticky-class");
                    location.reload();
                    $('.changepwd').attr("disabled",false);
                    $('.changepwd').html("SAVE");
                }
                else{
                    GritterNotification("Error",msg.message,"my-sticky-class"); 
                    $('.changepwd').attr("disabled",false);
                    $('.changepwd').html("SAVE");
                }
            },
            error: function (msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_url;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_url;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_url;
                }
            }
        });
    
    });
	

});