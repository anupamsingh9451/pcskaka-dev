$('document').ready(function() {
    /* login Admins */
    $("#loginadmin").on('submit', function(e) {
        e.preventDefault();
        var formdata = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/adminlogin',
            data: formdata,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
            },
            beforeSend: function() {
                $('.loginadmin').attr("disabled", "disabled");
                $('.loginadmin').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.message == "ok") {
                    $('.mymsg').html("Login Successfull");
                    $('.mymsg').removeClass('alert-warning');
                    $('.mymsg').addClass('alert-success');
                    $('.loginadmin').attr("disabled", false);
                    $('.loginadmin').html("Sign me in");
                    location.href = base_loc + 'ratan/dashboard';
                } else {
                    if (msg.message == "Already Login") {
                        swal({
                            title: "Are you sure?",
                            text: "You are Loged in somewhere! Do you want to login?",
                            icon: "warning",
                            buttons: [
                                'No, cancel it!',
                                'Yes, I am sure!'
                            ],
                            dangerMode: true,
                        }).then(function(isConfirm) {
                            if (isConfirm) {
                                formdata.append('loginagain', '1');
                                $.ajax({
                                    type: 'POST',
                                    url: base_loc + 'adminpostajax/adminlogin',
                                    data: formdata,
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    headers: {
                                        'Client-Service': clientserv,
                                        'Auth-Key': apikey,
                                    },
                                    success: function(msg) {
                                        if (msg.message == "ok") {
                                            $('.mymsg').html("Login Successfull");
                                            $('.mymsg').removeClass('alert-warning');
                                            $('.mymsg').addClass('alert-success');
                                            $('.loginadmin').attr("disabled", false);
                                            $('.loginadmin').html("Sign me in");
                                            location.href = base_loc + 'ratan/dashboard';
                                        } else {
                                            $('.mymsg').html(msg.message);
                                            $('.mymsg').addClass('alert-warning');
                                            $('.mymsg').removeClass('alert-success');
                                            $('.loginuser').attr("disabled", false);
                                            $('.loginuser').html("Sign me in");
                                        }
                                    },
                                    error: function(msg) {
                                        if (msg.responseJSON['status'] == 303) {
                                            location.href = base_loc;
                                        }
                                        if (msg.responseJSON['status'] == 401) {
                                            location.href = base_loc;
                                        }
                                        if (msg.responseJSON['status'] == 400) {
                                            location.href = base_loc;
                                        }
                                    }
                                });
                            } else {
                                // swal("Cancelled", "Your Question is safe :)", "error");
                                $('.loginadmin').attr("disabled", false);
                                $('.loginadmin').html("Sign me in");
                            }
                        })
                        $(".swal-button--danger").css('background-color', '#00acac');
                    } else {
                        $('.mymsg').html(msg.message);
                        $('.mymsg').addClass('alert-warning');
                        $('.mymsg').removeClass('alert-success');
                        $('.loginadmin').attr("disabled", false);
                        $('.loginadmin').html("Sign me in");
                    }

                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* for logout */
    $('.logoutadmin').on('click', function() {
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/adminlogout',
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            success: function(msg) {
                location.href = base_loc + 'ratan';
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* for creating Library */
    $('#librarycreate').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        f.append('adminid', loginid);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/librarycreate',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.librarycreate').attr("disabled", "disabled");
                $('.librarycreate').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "LIBRARY CREATED SUCCESSFULLY", "my-sticky-class");
                    $('.librarycreate').attr("disabled", false);
                    $('.librarycreate').html("SAVE");
                    location.href = base_loc + 'ratan/library/list';
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.librarycreate').attr("disabled", false);
                    $('.librarycreate').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* for editing library */
    $('#editlibraryform').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/editlibrary',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.editlibraryform').attr("disabled", "disabled");
                $('.editlibraryform').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "LIBRARY EDITED SUCCESSFULLY", "my-sticky-class");
                    $('.editlibraryform').attr("disabled", false);
                    $('.editlibraryform').html("SAVE");
                    $('#editlibraryform').trigger('reset');
                    $('#editlibrary').modal('hide');
                    updatelistlibrary();
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.editareaform').attr("disabled", false);
                    $('.editareaform').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* for creating Subject */
    $('#subcreate').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/subjectcreate',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type,
            },
            beforeSend: function() {
                $('.subcreate').attr("disabled", "disabled");
                $('.subcreate').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "SUBJECT CREATED SUCCESSFULLY", "my-sticky-class");
                    $('#subcreate')[0].reset();
                    $('.subcreate').attr("disabled", false);
                    $('.subcreate').html("SAVE");
                    location.href = base_loc + 'ratan/subject/list';

                } else {
                    // $('#uploadImg').attr('src','');
                    //     		$('#uploadImg').addClass('hidden');
                    //     		$('.imglabel').html('Please upload a picture !');
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.subcreate').attr("disabled", false);
                    $('.subcreate').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* for editing library */
    $('#editsubjectform').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/editsubject',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.editsubjectform').attr("disabled", "disabled");
                $('.editsubjectform').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "SUBJECT EDITED SUCCESSFULLY", "my-sticky-class");
                    $('.editsubjectform').attr("disabled", false);
                    $('.editsubjectform').html("SAVE");
                    $('#editsubjectform').trigger('reset');
                    $('#editsubject').modal('hide');
                    updatelistsubject();
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.editsubjectform').attr("disabled", false);
                    $('.editsubjectform').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* for creating book */
    $('#bookcreate').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        f.append('adminid', loginid);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/bookcreate',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.bookcreate').attr("disabled", "disabled");
                $('.bookcreate').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "BOOK CREATED SUCCESSFULLY", "my-sticky-class");
                    $('#bookcreate')[0].reset();
                    $('.bookcreate').attr("disabled", false);
                    $('.bookcreate').html("SAVE");
                    $('#bookcreate').modal('hide');
                    location.href = base_loc + 'ratan/book/list';
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.bookcreate').attr("disabled", false);
                    $('.bookcreate').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* for updatint book */
    $('#editbookform').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        f.append('adminid', loginid);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/editbookform',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.editbookform').attr("disabled", "disabled");
                $('.editbookform').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "BOOK EDITED SUCCESSFULLY", "my-sticky-class");
                    $('#editbookform')[0].reset();
                    $('.editbookform').attr("disabled", false);
                    $('.editbookform').html("SAVE");
                    $('#editbook').modal('hide');

                    updatelistbooks();
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.editbookform').attr("disabled", false);
                    $('.editbookform').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* for creating chapter */
    $('#chaptercreate').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/chaptercreate',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.chaptercreate').attr("disabled", "disabled");
                $('.chaptercreate').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "CHAPTER CREATED SUCCESSFULLY", "my-sticky-class");
                    $('#chaptercreate')[0].reset();
                    $('.chaptercreate').attr("disabled", false);
                    $('.chaptercreate').html("SAVE");
                    $('#addchapter').modal('hide');
                    updatelistchapter()

                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.chaptercreate').attr("disabled", false);
                    $('.chaptercreate').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* for updatint chapter */
    $('#editchapterform').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/updatechapter',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.editchapterform').attr("disabled", "disabled");
                $('.editchapterform').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "CHAPTER EDITED SUCCESSFULLY", "my-sticky-class");
                    $('#editchapterform')[0].reset();
                    $('.editchapterform').attr("disabled", false);
                    $('.editchapterform').html("SAVE");
                    $('#editchapter').modal('hide');
                    updatelistchapter();
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.editchapterform').attr("disabled", false);
                    $('.editchapterform').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });

    /* for creating Question */
    $('#questioncreate').on('submit', function(e) {
        e.preventDefault();
        $check = 0;
        var ques_type = $(this).find('option:selected', '.ques_type').val();
        $chapter = $(this).find('.chapterid').val();
        console.log($chapter);
        if (ques_type == 1) {
            var subj_ques_explain = $(this).find('.subj_ques_explain').val();
            var subj_answer = $(this).find('.subj_answer').val();
            var subj_positive_mark = $(this).find('.subj_positive_mark').val();
            var subj_negative_mark = $(this).find('.subj_negative_mark').val();
            var subj_created_date = $(this).find('.subj_created_date').val();

            if (subj_answer != '' && subj_created_date != '' && $.trim(subj_ques_explain) != '') {
                $check = 1;
            }
        } else {
            var obj_ques_explain = $(this).find('.obj_ques_explain').val();
            var obj_options = $(this).find('.obj_options').val();

            //  var obj_options = $(this).find('.obj_options').val();
            //  var obj_positive_mark = $(this).find('.obj_positive_mark').val();
            //  var obj_negative_mark = $(this).find('.obj_negative_mark').val();
            var obj_created_date = $(this).find('.obj_created_date').val();

            $i = 0;
            $j = 0;
            if ($.trim(obj_ques_explain) != '' && obj_created_date != '') {
                $(".ques_option").each(function() {
                    if ($(this).val() != '') {
                        $j = $j + 1;
                    }
                });
                $(".obj_answer").each(function() {
                    if ($(this).is(':checked')) {
                        $i = $i + 1;
                    }
                });
            }

            if ($i > 0 && $j == obj_options) {
                $check = 1;
            } else {
                $check = 0;
            }
        }
        if ($('#mysummernote').summernote('isEmpty')) {
            $check = 0;
        }
        if ($check == 1) {
            var f = new FormData(this);
            $.ajax({
                type: 'POST',
                url: base_loc + 'adminpostajax/questioncreate',
                data: f,
                contentType: false,
                cache: false,
                processData: false,
                headers: {
                    'Client-Service': clientserv,
                    'Auth-Key': apikey,
                    'User-ID': loginid,
                    'Authorization': token,
                    'type': type
                },
                beforeSend: function() {
                    $('.questioncreate').attr("disabled", "disabled");
                    $('.questioncreate').html("Please Wait...");
                },
                success: function(msg) {
                    if (msg.data == "ok") {
                        GritterNotification("Success", "QUESTION CREATED SUCCESSFULLY", "my-sticky-class");
                        $('#questioncreate')[0].reset();
                        $('.questioncreate').attr("disabled", false);
                        $('.questioncreate').html("SAVE");
                        $('#addquestion').modal('hide');
                        location.href = base_loc + 'ratan/question/' + $chapter;

                    } else {
                        GritterNotification("Error", msg.message, "my-sticky-class");
                        $('.questioncreate').attr("disabled", false);
                        $('.questioncreate').html("SAVE");
                    }
                },
                error: function(msg) {
                    if (msg.responseJSON['status'] == 303) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 401) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 400) {
                        location.href = base_loc;
                    }
                }
            });
        } else {
            GritterNotification("Error", "Please Fill All Input", "my-sticky-class");
        }
    });



    /* for editing Question */
    $('body').on('submit', '#questionedit', function(e) {
        e.preventDefault();
        $check = 0;
        var ques_type = $(this).find('option:selected', '.ques_type').val();
        if (ques_type == 1) {
            var subj_answer = $(this).find('.subj_answer').val();
            var subj_positive_mark = $(this).find('.subj_positive_mark').val();
            var subj_negative_mark = $(this).find('.subj_negative_mark').val();
            //  var subj_created_date = $(this).find('.subj_created_date').val();
            var subj_ques_explain = $(this).find('.subj_ques_explain').val();
            if (subj_answer != '' && $.trim(subj_ques_explain) != '') {
                $check = 1;
            }
        } else {
            var obj_options = $(this).find('.obj_options').val();
            var obj_positive_mark = $(this).find('.obj_positive_mark').val();
            var obj_negative_mark = $(this).find('.obj_negative_mark').val();
            var obj_ques_explain = $(this).find('.obj_ques_explain').val();
            var edit_obj_options = $(this).find('.edit_obj_options').val();
            $i = 0;
            $j = 0;

            if ($.trim(obj_ques_explain) != '') {
                $(".obj_answer").each(function() {
                    if ($(this).is(':checked')) {
                        $i = $i + 1;
                    }
                });
                $(".ques_option").each(function() {
                    if ($(this).val() != '') {
                        $j = $j + 1;
                    }
                });
            }


            if ($i > 0 && $j == edit_obj_options) {
                $check = 1;
            } else {
                $check = 0;
            }
        }
        if ($('#mysummernote').summernote('isEmpty')) {
            $check = 0;
        }
        if ($check == 1) {
            var f = new FormData(this);
            $.ajax({
                type: 'POST',
                url: base_loc + 'adminpostajax/questionedit',
                data: f,
                contentType: false,
                cache: false,
                processData: false,
                headers: {
                    'Client-Service': clientserv,
                    'Auth-Key': apikey,
                    'User-ID': loginid,
                    'Authorization': token,
                    'type': type
                },
                beforeSend: function() {
                    $('.questionedit').attr("disabled", "disabled");
                    $('.questionedit').html("Please Wait...");
                },
                success: function(msg) {
                    if (msg.data == "ok") {
                        GritterNotification("Success", "QUESTION EDITED SUCCESSFULLY", "my-sticky-class");
                        $('.questionedit').attr("disabled", false);
                        $('.questionedit').html("SAVE");
                        $('#viewquestionmodal').modal('hide');
                        $('#editquestionmodal').modal('hide');

                        updatelistques();

                    } else {
                        GritterNotification("Error", msg.message, "my-sticky-class");
                        $('.questionedit').attr("disabled", false);
                        $('.questionedit').html("SAVE");
                    }
                },
                error: function(msg) {
                    if (msg.responseJSON['status'] == 303) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 401) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 400) {
                        location.href = base_loc;
                    }
                }
            });
        } else {
            GritterNotification("Error", "Please Fill All Input", "my-sticky-class");
        }
    });

    /* for creating Test series */
    $('#testseriescreate').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $check = 1;
        if ($('#mysummernote').summernote('isEmpty')) {
            $check = 0;
        }
        if ($check == 1) {
            $.ajax({
                type: 'POST',
                url: base_loc + 'adminpostajax/testseriescreate',
                data: f,
                contentType: false,
                cache: false,
                processData: false,
                headers: {
                    'Client-Service': clientserv,
                    'Auth-Key': apikey,
                    'User-ID': loginid,
                    'Authorization': token,
                    'type': type
                },
                beforeSend: function() {
                    $('.testseriescreate').attr("disabled", "disabled");
                    $('.testseriescreate').html("Please Wait...");
                },
                success: function(msg) {
                    if (msg.data == "ok") {
                        GritterNotification("Success", "TEST SERIES CREATED SUCCESSFULLY", "my-sticky-class");
                        $('#testseriescreate')[0].reset();
                        $('.testseriescreate').attr("disabled", false);
                        $('.testseriescreate').html("SAVE");
                    } else {
                        GritterNotification("Error", msg.message, "my-sticky-class");
                        $('.testseriescreate').attr("disabled", false);
                        $('.testseriescreate').html("SAVE");
                    }
                },
                error: function(msg) {
                    if (msg.responseJSON['status'] == 303) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 401) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 400) {
                        location.href = base_loc;
                    }
                }
            });
        } else {
            GritterNotification("Error", "Please Fill All Input", "my-sticky-class");
        }

    });

    /* for creating Test paper */
    $('#testpapercreate').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        var desc = $(this).find('.desc').val();
        if ($.trim(desc) != '') {
            $.ajax({
                type: 'POST',
                url: base_loc + 'adminpostajax/testpapercreate',
                data: f,
                contentType: false,
                cache: false,
                processData: false,
                headers: {
                    'Client-Service': clientserv,
                    'Auth-Key': apikey,
                    'User-ID': loginid,
                    'Authorization': token,
                    'type': type
                },
                beforeSend: function() {
                    $('.testpapercreate').attr("disabled", "disabled");
                    $('.testpapercreate').html("Please Wait...");
                },
                success: function(msg) {
                    if (msg.data == "ok") {
                        GritterNotification("Success", "TEST PAPER CREATED SUCCESSFULLY", "my-sticky-class");
                        $('#testpapercreate')[0].reset();
                        $('.testpapercreate').attr("disabled", false);
                        $('.testpapercreate').html("SAVE");
                        $('.questions').selectpicker('refresh');
                        $('.questions option[value="X"]').remove();
                        $('.countques').html('');
                        testpaperlist();
                        $('#addtetspaper').modal('hide');
                    } else {
                        GritterNotification("Error", msg.message, "my-sticky-class");
                        $('.testpapercreate').attr("disabled", false);
                        $('.testpapercreate').html("SAVE");
                    }
                },
                error: function(msg) {
                    if (msg.responseJSON['status'] == 303) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 401) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 400) {
                        location.href = base_loc;
                    }
                }
            });
        } else {
            GritterNotification("Error", "Please Fill All Input", "my-sticky-class");
        }
    });
    /* for editing library */
    $('#edittestseriesform').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $check = 1;
        if ($('#mysummernote').summernote('isEmpty')) {
            $check = 0;
        }
        if ($check == 1) {
            $.ajax({
                type: 'POST',
                url: base_loc + 'adminpostajax/updatetestseries',
                data: f,
                contentType: false,
                cache: false,
                processData: false,
                headers: {
                    'Client-Service': clientserv,
                    'Auth-Key': apikey,
                    'User-ID': loginid,
                    'Authorization': token,
                    'type': type
                },
                beforeSend: function() {
                    $('.edittestseriesform').attr("disabled", "disabled");
                    $('.edittestseriesform').html("Please Wait...");
                },
                success: function(msg) {
                    if (msg.data == "ok") {
                        GritterNotification("Success", "TEST SERIES UPDATED SUCCESSFULLY", "my-sticky-class");
                        $('.edittestseriesform').attr("disabled", false);
                        $('.edittestseriesform').html("Update");
                        $('#edittestseriesform').trigger('reset');
                        $('#edittestseries').modal('hide');
                        testserieslists();
                    } else {
                        GritterNotification("Error", msg.message, "my-sticky-class");
                        $('.edittestseriesform').attr("disabled", false);
                        $('.edittestseriesform').html("Update");
                    }
                },
                error: function(msg) {
                    if (msg.responseJSON['status'] == 303) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 401) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 400) {
                        location.href = base_loc;
                    }
                }
            });
        } else {
            GritterNotification("Error", "Please Fill All Input", "my-sticky-class");
        }
    });

    /* for creating Member */
    $('#membercreate').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        f.append('adminid', loginid);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/membercreate',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.membercreate').attr("disabled", "disabled");
                $('.membercreate').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "MEMBER CREATED SUCCESSFULLY", "my-sticky-class");
                    $('#membercreate')[0].reset();
                    $('.membercreate').attr("disabled", false);
                    $('.membercreate').html("SAVE");
                    location.href = base_loc + 'ratan/member/list';
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.membercreate').attr("disabled", false);
                    $('.membercreate').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });

    /* for updatint Member */
    $('#editmemberform').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        f.append('adminid', loginid);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/editmemberform',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.editbookform').attr("disabled", "disabled");
                $('.editbookform').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "MEMBER UPDATED SUCCESSFULLY", "my-sticky-class");
                    $('#editmemberform')[0].reset();
                    $('.editmemberform').attr("disabled", false);
                    $('.editmemberform').html("Update");
                    $('#editmember').modal('hide');
                    listmember();
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.editmemberform').attr("disabled", false);
                    $('.editmemberform').html("Update");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });


    /* for creating Coupon */
    $('#coupon_create').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/couponcreate',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.coupon_create').attr("disabled", "disabled");
                $('.coupon_create').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "COUPON CREATED SUCCESSFULLY", "my-sticky-class");
                    $('#coupon_create')[0].reset();
                    $('.coupon_create').attr("disabled", false);
                    $('.coupon_create').html("SAVE");
                    location.href = base_loc + 'ratan/coupon/list';
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.coupon_create').attr("disabled", false);
                    $('.coupon_create').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    //A 19-04-2020 Coupon Create
    $('#editcoupon').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/couponedit',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.editcoupon').attr("disabled", "disabled");
                $('.editcoupon').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "COUPON UPDATED SUCCESSFULLY", "my-sticky-class");
                    $('#editcoupon')[0].reset();
                    $('.editcoupon').attr("disabled", false);
                    $('.editcoupon').html("SAVE");
                    location.href = base_loc + 'ratan/coupon/list';
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.editcoupon').attr("disabled", false);
                    $('.editcoupon').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    //A end Coupon edit

    $('body').on('click', '.nfs_ques', function() {
        var quesid = $(this).attr('attr-quesid');
        var nfsact = $(this).attr('attr-nfsact');
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/nfs_ques',
            data: { "quesid": quesid, "loginid": loginid, "nfsact": nfsact },
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "QUESTION STATUS CHANGE SUCCESSFULLY", "my-sticky-class");
                    // 	location.reload()
                    $('.searchques').trigger('click');
                    $('#viewquestionmodal').modal('hide');
                    $('#editquestionmodal').modal('hide');
                    updatelistques();
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });

    $('body').on('submit', '#templateform', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/addtemplate',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.templateform').attr("disabled", "disabled");
                $('.templateform').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "TEMPLATE ADDED SUCCESSFULLY", "my-sticky-class");
                    $('#templateform')[0].reset();
                    $('.templateform').attr("disabled", false);
                    $('.templateform').html("Add Template");
                    getalltemplate(1, 0);
                    getalltemplate(2, 0);
                    getalltemplate(0, 1);
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.templateform').attr("disabled", false);
                    $('.templateform').html("Add Template");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });

    $('body').on('submit', '#sendmessage', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/sendmessage',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.sendmessage').attr("disabled", "disabled");
                $('.sendmessage').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "MESSAGE SENDED SUCCESSFULLY", "my-sticky-class");
                    $('.sendmessage').attr("disabled", false);
                    $('.sendmessage').html("Send");
                    $('.usermultipleselect').html('');
                    $(".usermultipleselect").select2();
                    $('.sendmessagetemp').html('');
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.sendmessage').attr("disabled", false);
                    $('.sendmessage').html("Send");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });

    $('body').on('submit', '#setscheduling', function(e) {
        e.preventDefault();
        if ($('.scheduletype').is(':checked')) {
            var check = 1;
        } else {
            var check = 0;
        }
        var f = new FormData(this);
        if (check == 1) {
            $.ajax({
                type: 'POST',
                url: base_loc + 'adminpostajax/setscheduling',
                data: f,
                contentType: false,
                cache: false,
                processData: false,
                headers: {
                    'Client-Service': clientserv,
                    'Auth-Key': apikey,
                    'User-ID': loginid,
                    'Authorization': token,
                    'type': type
                },
                beforeSend: function() {
                    $('.setscheduling').attr("disabled", "disabled");
                    $('.setscheduling').html("Please Wait...");
                },
                success: function(msg) {
                    if (msg.data == "ok") {
                        GritterNotification("Success", "MESSAGE SCHEDULING SET SUCCESSFULLY", "my-sticky-class");
                        $('#setscheduling')[0].reset();
                        $('.setscheduling').attr("disabled", false);
                        $('.setscheduling').html("Set Schedule");
                        $('.sendmessagetempsched').val('');
                        $('.usermultipleselectsched').html('');
                        $(".usermultipleselectsched").select2();
                        getallscheduling();
                    } else {
                        GritterNotification("Error", msg.message, "my-sticky-class");
                        $('.setscheduling').attr("disabled", false);
                        $('.setscheduling').html("Set Schedule");
                    }
                },
                error: function(msg) {
                    if (msg.responseJSON['status'] == 303) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 401) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 400) {
                        location.href = base_loc;
                    }
                }
            });
        } else {
            alert('Please select Scheduling Type!');
        }
    });

    $('body').on('submit', '#edittemplateform', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/updatetemplate',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.edittemplateform').attr("disabled", "disabled");
                $('.edittemplateform').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "TEMPLATE UPDATED SUCCESSFULLY", "my-sticky-class");
                    $('#edittemplateform')[0].reset();
                    $('.edittemplateform').attr("disabled", false);
                    $('.edittemplateform').html("Update Template");
                    $('#edittemplatemodal').modal("hide");
                    getalltemplate(1, 0);
                    getalltemplate(2, 0);
                    getalltemplate(0, 1);
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.templateform').attr("disabled", false);
                    $('.templateform').html("Update Template");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });

    $('body').on('submit', '#sendmail', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/sendmail',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.sendmail').attr("disabled", "disabled");
                $('.sendmail').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "MAIL SENDED SUCCESSFULLY", "my-sticky-class");
                    $('.sendmail').attr("disabled", false);
                    $('.sendmail').html("Send");
                    $('.usermultipleselect').html('');
                    $(".usermultipleselect").select2();
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.sendmail').attr("disabled", false);
                    $('.sendmail').html("Send");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });

    $('body').on('submit', '#editsetschedulingform', function(e) {
        e.preventDefault();
        if ($('.scheduletype').is(':checked')) {
            var check = 1;
        } else {
            var check = 0;
        }
        var f = new FormData(this);
        if (check == 1) {
            $.ajax({
                type: 'POST',
                url: base_loc + 'adminpostajax/update_scheduling',
                data: f,
                contentType: false,
                cache: false,
                processData: false,
                headers: {
                    'Client-Service': clientserv,
                    'Auth-Key': apikey,
                    'User-ID': loginid,
                    'Authorization': token,
                    'type': type
                },
                beforeSend: function() {
                    $('.editsetschedulingform').attr("disabled", "disabled");
                    $('.editsetschedulingform').html("Please Wait...");
                },
                success: function(msg) {
                    if (msg.data == "ok") {
                        GritterNotification("Success", "MESSAGE SCHEDULING SET SUCCESSFULLY", "my-sticky-class");
                        $('#editsetschedulingform')[0].reset();
                        $('.editsetschedulingform').attr("disabled", false);
                        $('.editsetschedulingform').html("Update Schedule");
                        $('.usermultipleschededit').html('');
                        $(".usermultipleschededit").select2();
                        $('#editsetscheduling').modal("hide");
                        getallscheduling();
                    } else {
                        GritterNotification("Error", msg.message, "my-sticky-class");
                        $('.editsetschedulingform').attr("disabled", false);
                        $('.editsetschedulingform').html("Update Schedule");
                    }
                },
                error: function(msg) {
                    if (msg.responseJSON['status'] == 303) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 401) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 400) {
                        location.href = base_loc;
                    }
                }
            });
        } else {
            alert('Please select Scheduling Type!');
        }
    });
    //A Start 07-04-2020 
    /* for creating Course */
    $('#coursecreate').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        f.append('adminid', loginid);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/coursecreate',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.coursecreate').attr("disabled", "disabled");
                $('.coursecreate').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "COURSE CREATED SUCCESSFULLY", "my-sticky-class");
                    $('.coursecreate').attr("disabled", false);
                    $('.coursecreate').html("SAVE");
                    location.href = base_loc + 'ratan/course/list';
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.coursecreate').attr("disabled", false);
                    $('.coursecreate').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* for editing Course */
    $('#editcourseform').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/editcourse',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.editcourseform').attr("disabled", "disabled");
                $('.editcourseform').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "COURSE EDITED SUCCESSFULLY", "my-sticky-class");
                    $('.editcourseform').attr("disabled", false);
                    $('.editcourseform').html("SAVE");
                    $('#editcourseform').trigger('reset');
                    $('#editcourseform').modal('hide');
                    courselist();
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.editcourseform').attr("disabled", false);
                    $('.editcourseform').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });


    $('#course_content_form').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/course_content',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.editcourseform').attr("disabled", "disabled");
                $('.editcourseform').html("Please Wait...");
            },
            success: function(msg) {
                console.log(msg);
                if (msg.data == "ok") {
                    GritterNotification("Success", "COURSE CONTENT ADDED SUCCESSFULLY", "my-sticky-class");
                    $('.editcourseform').attr("disabled", false);
                    $('.editcourseform').html("SAVE");
                    $('#editcourseform').trigger('reset');
                    $('#editcourseform').modal('hide');
                    courselist();
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.editcourseform').attr("disabled", false);
                    $('.editcourseform').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });

    //A Start 23-05-2020 
    /* for creating Slider */
    $('#slidercreate').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        f.append('adminid', loginid);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/slidercreate',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.slidercreate').attr("disabled", "disabled");
                $('.slidercreate').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "SLIDER CREATED SUCCESSFULLY", "my-sticky-class");
                    $('.slidercreate').attr("disabled", false);
                    $('.slidercreate').html("SAVE");
                    //location.href = base_loc + 'ratan/slider/list';
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.slidercreate').attr("disabled", false);
                    $('.slidercreate').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* for editing Slider */
    $('#editslider').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/editslider',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.editslider').attr("disabled", "disabled");
                $('.editslider').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "SLIDER EDITED SUCCESSFULLY", "my-sticky-class");
                    $('.editslider').attr("disabled", false);
                    $('.editslider').html("SAVE");
                    $('#editslider').trigger('reset');
                    $('.modal').modal('hide');
                    table_reload();
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.editslider').attr("disabled", false);
                    $('.editslider').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });

    /*Shubham 2020-06-13*/
    $('#practicetestcreate').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $check = 1;
        if ($('#mysummernote').summernote('isEmpty')) {
            $check = 0;
        }
        if ($check == 1) {
            $.ajax({
                type: 'POST',
                url: base_loc + 'adminpostajax/practicetestcreate',
                data: f,
                contentType: false,
                cache: false,
                processData: false,
                headers: {
                    'Client-Service': clientserv,
                    'Auth-Key': apikey,
                    'User-ID': loginid,
                    'Authorization': token,
                    'type': type
                },
                beforeSend: function() {
                    $('.practicetestcreate').attr("disabled", "disabled");
                    $('.practicetestcreate').html("Please Wait...");
                },
                success: function(msg) {
                    if (msg.data == "ok") {
                        GritterNotification("Success", "TEST SET CREATED SUCCESSFULLY", "my-sticky-class");
                        $('#practicetestcreate')[0].reset();
                        $('.practicetestcreate').attr("disabled", false);
                        $('.practicetestcreate').html("SAVE");
                        location.reload(true);
                    } else {
                        GritterNotification("Error", msg.message, "my-sticky-class");
                        $('.practicetestcreate').attr("disabled", false);
                        $('.practicetestcreate').html("SAVE");
                    }
                },
                error: function(msg) {
                    if (msg.responseJSON['status'] == 303) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 401) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 400) {
                        location.href = base_loc;
                    }
                }
            });
        } else {
            GritterNotification("Error", "Please Fill All Input", "my-sticky-class");
        }

    });

    $('#editpracticetestform').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $check = 1;
        if ($('#mysummernote').summernote('isEmpty')) {
            $check = 0;
        }
        if ($check == 1) {
            $.ajax({
                type: 'POST',
                url: base_loc + 'adminpostajax/updatepracticetest',
                data: f,
                contentType: false,
                cache: false,
                processData: false,
                headers: {
                    'Client-Service': clientserv,
                    'Auth-Key': apikey,
                    'User-ID': loginid,
                    'Authorization': token,
                    'type': type
                },
                beforeSend: function() {
                    $('.editpracticetestform').attr("disabled", "disabled");
                    $('.editpracticetestform').html("Please Wait...");
                },
                success: function(msg) {
                    if (msg.data == "ok") {
                        GritterNotification("Success", "TEST SET UPDATED SUCCESSFULLY", "my-sticky-class");
                        $('.editpracticetestform').attr("disabled", false);
                        $('.editpracticetestform').html("Update");
                        $('#editpracticetestform').trigger('reset');
                        $('#edittestseries').modal('hide');
                        /*testserieslists();*/
                        location.reload(true);
                    } else {
                        GritterNotification("Error", msg.message, "my-sticky-class");
                        $('.editpracticetestform').attr("disabled", false);
                        $('.editpracticetestform').html("Update");
                    }
                },
                error: function(msg) {
                    if (msg.responseJSON['status'] == 303) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 401) {
                        location.href = base_loc;
                    }
                    if (msg.responseJSON['status'] == 400) {
                        location.href = base_loc;
                    }
                }
            });
        } else {
            GritterNotification("Error", "Please Fill All Input", "my-sticky-class");
        }
    });
    /**/
    $('#editlibrarysaleform').on('submit', function(e) {
        e.preventDefault();
        var f = new FormData(this);
        $.ajax({
            type: 'POST',
            url: base_loc + 'adminpostajax/editlibrarycost',
            data: f,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            beforeSend: function() {
                $('.editlibrarysaleform').attr("disabled", "disabled");
                $('.editlibrarysaleform').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.data == "ok") {
                    GritterNotification("Success", "LIBRARY EDITED SUCCESSFULLY", "my-sticky-class");
                    $('.editlibrarysaleform').attr("disabled", false);
                    $('.editlibrarysaleform').html("SAVE");
                    $('#editlibrarysaleform').trigger('reset');
                    $('#editlibrarysale').modal('hide');
                    updatelistlibrary();
                } else {
                    GritterNotification("Error", msg.message, "my-sticky-class");
                    $('.editareaform').attr("disabled", false);
                    $('.editareaform').html("SAVE");
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
});