$('document').ready(function() {
    App.init();
    DashboardV2.init();
    TableManageCombine.init();
    FormPlugins.init();


    /* get admin full details */

    $.ajax({
        type: 'GET',
        url: base_loc + 'admingetajax/get_admin_dt',
        data: 'id=' + loginid,
        headers: {
            'Client-Service': clientserv,
            'Auth-Key': apikey,
            'User-ID': loginid,
            'Authorization': token,
            'type': type
        },
        success: function(msg) {

            if (msg.status == 200) {
                $('.admin_name_upper').html(msg.data[0].USER_EMAIL);
                $('.admin_address_upper').html(msg.data[0].USER_ADDRESS);
            } else {
                location.href = base_loc;
            }
        },
        error: function(msg) {
            if (msg.responseJSON['status'] == 303) {
                location.href = base_loc;
            }
            if (msg.responseJSON['status'] == 401) {
                location.href = base_loc;
            }
            if (msg.responseJSON['status'] == 400) {
                location.href = base_loc;
            }
        }
    });



    /* get library full details */
    $('body').on('change', '.library', function(e) {
        $.ajax({
            type: 'GET',
            data: 'subid=' + $(this).val(),
            url: base_loc + 'admingetajax/get_subject_by_libid',
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            success: function(msg) {
                $('.subject option').remove();
                var str = '<option value="">--Select Subject--</option>';
                if (msg.status == 200) {
                    if (msg.data.length > 0) {
                        $.each(msg.data, function(index, element) {
                            str += '<option value="' + element.SUB_ID + '">' + element.SUB_NAME + '</option>';
                        });
                    }
                    $('.subject').append(str);
                } else {
                    location.href = base_loc;
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* get library full details */
    $('body').on('change', '.subject', function(e) {
        $.ajax({
            type: 'GET',
            data: 'subid=' + $(this).val(),
            url: base_loc + 'admingetajax/get_book_by_subid',
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            success: function(msg) {
                $('.book option').remove();
                var str = '<option value="">--Select Book--</option>';
                if (msg.status == 200) {
                    if (msg.data.length > 0) {
                        $.each(msg.data, function(index, element) {
                            str += '<option value="' + element.BOOK_ID + '">' + element.BOOK_NAME + '</option>';
                        });
                    }
                    $('.book').append(str);
                } else {
                    location.href = base_loc;
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* getting country */
    if ($(".country").length > 0) {
        var cid = 0;
        if (!($('.countryedit').val() === undefined)) {
            cid = $('.countryedit').val();
        }

        $.ajax({
            type: 'GET',
            url: base_loc + 'lcogetajax/getcountry',
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            success: function(msg) {
                var str = '<option value="">Select Country</option>';
                if (msg.message == "ok") {
                    if (msg.data.length > 0) {
                        $.each(msg.data, function(index, element) {
                            if (cid == element.COUNTRY_NAME) {
                                str += '<option value="' + element.COUNTRY_NAME + '" selected>' + element.COUNTRY_NAME + '</option>';
                            } else {
                                str += '<option value="' + element.COUNTRY_NAME + '">' + element.COUNTRY_NAME + '</option>';
                            }
                        });
                    }
                    $('.country select').append(str);
                    $('.country').selectpicker('refresh');
                    $('.country option[selected]').trigger('change');
                } else {
                    location.href = base_loc;
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    }
    /* getting state */
    $('.country select').on('change', function() {
        var sid = 0;
        if (!($('.stateedit').val() === undefined)) {
            sid = $('.stateedit').val();
        }
        $('.city select option').remove();
        $('.city').selectpicker('refresh');
        $.ajax({
            type: 'GET',
            url: base_loc + 'lcogetajax/getstates',
            data: 'countryid=' + $(this).val(),
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            success: function(msg) {
                var str = '';
                $('.state select option').remove();
                if (msg.message == "ok") {
                    if (msg.data.length > 0) {
                        $.each(msg.data, function(index, element) {
                            if (sid == element.STATE_NAME) {
                                str += '<option value="' + element.STATE_NAME + '" selected>' + element.STATE_NAME + '</option>';
                            } else {
                                str += '<option value="' + element.STATE_NAME + '">' + element.STATE_NAME + '</option>';
                            }
                        });
                    }
                    $('.state select').append(str);
                    $('.state').selectpicker('refresh');
                    $('.state option[selected]').trigger('change');
                } else {
                    location.href = base_loc;
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
    /* getting city */
    $('.state select').on('change', function() {
        var ctid = 0;
        if (!($('.cityedit').val() === undefined)) {
            ctid = $('.cityedit').val();
        }
        $.ajax({
            type: 'GET',
            url: base_loc + 'lcogetajax/getcity',
            data: 'stateid=' + $(this).val(),
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
                'User-ID': loginid,
                'Authorization': token,
                'type': type
            },
            success: function(msg) {
                var str = '';
                $('.city select option').remove();
                if (msg.message == "ok") {
                    if (msg.data.length > 0) {
                        $.each(msg.data, function(index, element) {
                            if (ctid == element.CITY_ID) {
                                str += '<option value="' + element.CITY_ID + '" selected>' + element.CITY_NAME + '</option>';
                            } else {
                                str += '<option value="' + element.CITY_ID + '">' + element.CITY_NAME + '</option>';
                            }
                        });
                    }
                    $('.city select').append(str);
                    $('.city').selectpicker('refresh');
                } else {
                    location.href = base_loc;
                }
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });


    $('body').on('click', '.deactclick', function(e) {
        $('#stopbox').modal('show');
        $('#stopbox').find('.vcno').html($(this).attr('attr-vc-nm'));
        $('#stopbox').find('input[name=stbvcpairid]').val($(this).attr('attr-stbvc'));
        $('#stopbox').find('input[name=hrmid]').val($(this).attr('attr-hrmid'));
        $('#stopbox').find('input[name=firmid]').val($(this).attr('attr-firmid'));
    });
    $('body').on('click', '.actclick', function(e) {
        $('#startbox').modal('show');
        $('#startbox').find('.vcno').html($(this).attr('attr-vc-nm'));
        $('#startbox').find('input[name=stbvcpairid]').val($(this).attr('attr-stbvc'));
        $('#startbox').find('input[name=hrmid]').val($(this).attr('attr-hrmid'));
        $('#startbox').find('input[name=firmid]').val($(this).attr('attr-firmid'));
    });
    $('.check_percent').on('click', function() {
        if ($(this).prop("checked") == true) {
            $(this).parents('.parent_tax').find('.input_percent').attr('readonly', false);
        } else {
            $(this).parents('.parent_tax').find('.input_percent').attr('readonly', true);
            $(this).parents('.parent_tax').find('.input_percent').val('0');
        }
    });
    $('.fileinput-button').parent().find('input[type=file]').hide();
    $('.fileinput-button').on('click', function(e) {
        e.preventDefault();
        $(this).parent().find('input[type=file]').trigger('click');
    });

    $("body").on('change', 'input[type=file]', function() {
        readURL(this, $(this).parents('.image-section'));
    });
});


function readURL(input, loc) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            loc.find('img').attr({ 'src': e.target.result }).css({ 'max-height': '80px', 'max-width': '120px' });
        }
        reader.readAsDataURL(input.files[0]);
    }
}
/*
function GritterNotification(titles,texts,classnm){
	$.gritter.add({
		title:titles,
		text:texts,
		//image:base_loc+"assets/themeassets/img/user/user-12.jpg",
		sticky:!0,
		time:"",
		class_name:classnm
	});
}
*/
function getlibrary() {
    html = "";
    if ($('.courseid').val() != "" && $('.courseid').val() != "undefined" && $('.courseid').length > 0) {
        html += "courseid=" + $('.courseid').val();

    }
    $.ajax({
        type: 'GET',
        url: base_loc + 'admingetajax/getlibrary?' + html,
        headers: {
            'Client-Service': clientserv,
            'Auth-Key': apikey,
            'User-ID': loginid,
            'Authorization': token,
            'type': type
        },
        success: function(msg) {
            $('.library option').remove();
            var str = '<option value="">--Select Library--</option>';
            if (msg.status == 200) {
                if (msg.data.length > 0) {
                    $.each(msg.data, function(index, element) {
                        str += '<option value="' + element.LIB_ID + '">' + element.LIB_NAME + '</option>';
                    });
                }
                $('.library').append(str);
            } else {
                location.href = base_loc;
            }
        },
        error: function(msg) {
            if (msg.responseJSON['status'] == 303) {
                location.href = base_loc;
            }
            if (msg.responseJSON['status'] == 401) {
                location.href = base_loc;
            }
            if (msg.responseJSON['status'] == 400) {
                location.href = base_loc;
            }
        }
    });
}

function readimage(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.imagesrc').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function GritterNotification(titles, texts, classnm) {
    if (titles == "Success") {
        var types = 'success';
        var btntext = 'Success';
        var colorcode = '#469408';
    } else {
        var types = 'warning';
        var btntext = 'Warning';
        var colorcode = '#f99b4a';
    }
    if (titles == "Success") {
        swal({
            title: titles,
            text: texts,
            icon: types,
            buttons: {
                confirm: {
                    text: 'ok',
                    value: !0,
                    visible: !0,
                    className: "btn btn-" + types,
                    closeModal: !0
                }
            }
        });
    } else {
        swal({
            title: titles,
            text: texts,
            icon: types,
            buttons: {

                confirm: {
                    text: 'ok',
                    value: !0,
                    visible: !0,
                    className: "btn btn-" + types,
                    closeModal: !0
                }
            }
        });
    }



}

function expirydate(noofday) {
    var dt = new Date();
    dt.setDate(dt.getDate() + noofday);
    var mnth = dt.getMonth() + 1;
    var date = dt.getDate();

    if (mnth < 10) {
        mnth = '0' + mnth;
    }
    if (date < 10) {
        date = '0' + dt.getDate();
    }
    var expdt = dt.getFullYear() + "-" + mnth + "-" + date;

    return expdt;
}

function currentdate() {
    var dt = new Date();
    var mnth = dt.getMonth() + 1;

    var date = dt.getDate();
    if (mnth < 10) {
        mnth = '0' + mnth;
    }
    if (date < 10) {
        date = '0' + dt.getDate();
    }
    var date = dt.getFullYear() + "-" + mnth + "-" + date;
    return date;
}