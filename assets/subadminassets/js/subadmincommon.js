$('document').ready(function(){ 
	App.init();
	

	/* get admin full details */

	$.ajax({
		type: 'GET',
		url: base_loc + 'subadmingetajax/get_subadmin_dt',
		data:'id='+loginid,
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
		    console.log(msg);
			if(msg.status==200){
				$('.subadmin_name_upper').html(msg.data[0].USER_EMAIL);
				$('.subadmin_address_upper').html(msg.data[0].USER_ADDRESS);
			}else{
				location.href = base_loc;
			}
		},
		error: function (msg) {
// 			if (msg.responseJSON['status'] == 303) {
// 				location.href = base_loc;
// 			}
// 			if (msg.responseJSON['status'] == 401) {
// 				location.href = base_loc; 
// 			}
// 			if (msg.responseJSON['status'] == 400) {
// 				location.href = base_loc; 
// 			}
		}
	});
	
	
	
	/* get library full details */
	$('body').on('change','.library',function(e){
    	$.ajax({
    		type: 'GET',
    		data:'subid='+$(this).val(),
    		url: base_loc + 'subadmingetajax/get_subject_by_libid',
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    		    $('.subject option').remove();
    			var str='<option value="">--Select Subject--</option>';
    			if(msg.status==200){
    				if(msg.data.length>0){
    					$.each(msg.data, function(index, element) {
    						str+='<option value="'+element.SUB_ID+'">'+element.SUB_NAME+'</option>';
    					});
    				}
    				$('.subject').append(str);
    			}else{
    				location.href = base_loc;
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
	});
	/* get library full details */
	$('body').on('change','.subject',function(e){
    	$.ajax({
    		type: 'GET',
    		data:'subid='+$(this).val(),
    		url: base_loc + 'subadmingetajax/get_book_by_subid',
    		headers: {
    			'Client-Service': clientserv,
    			'Auth-Key': apikey,
    			'User-ID': loginid,
    			'Authorization': token,
    			'type': type
    		},
    		success: function (msg) {
    		    $('.book option').remove();
    			var str='<option value="">--Select Book--</option>';
    			if(msg.status==200){
    				if(msg.data.length>0){
    					$.each(msg.data, function(index, element) {
    						str+='<option value="'+element.BOOK_ID+'">'+element.BOOK_NAME+'</option>';
    					});
    				}
    				$('.book').append(str);
    			}else{
    				location.href = base_loc;
    			}
    		},
    		error: function (msg) {
    			if (msg.responseJSON['status'] == 303) {
    				location.href = base_loc;
    			}
    			if (msg.responseJSON['status'] == 401) {
    				location.href = base_loc; 
    			}
    			if (msg.responseJSON['status'] == 400) {
    				location.href = base_loc; 
    			}
    		}
    	});
	});

});
/*
function GritterNotification(titles,texts,classnm){
	$.gritter.add({
		title:titles,
		text:texts,
		//image:base_loc+"assets/themeassets/img/user/user-12.jpg",
		sticky:!0,
		time:"",
		class_name:classnm
	});
}
*/
function getlibrary(){
    $.ajax({
		type: 'GET',
		url: base_loc + 'subadmingetajax/getlibrary',
		headers: {
			'Client-Service': clientserv,
			'Auth-Key': apikey,
			'User-ID': loginid,
			'Authorization': token,
			'type': type
		},
		success: function (msg) {
		    $('.library option').remove();
			var str='<option value="">--Select Library--</option>';
			if(msg.status==200){
				if(msg.data.length>0){
					$.each(msg.data, function(index, element) {
						str+='<option value="'+element.LIB_ID+'">'+element.LIB_NAME+'</option>';
					});
				}
				$('.library').append(str);
			}else{
				location.href = base_loc;
			}
		},
		error: function (msg) {
			if (msg.responseJSON['status'] == 303) {
				location.href = base_loc;
			}
			if (msg.responseJSON['status'] == 401) {
				location.href = base_loc; 
			}
			if (msg.responseJSON['status'] == 400) {
				location.href = base_loc; 
			}
		}
	});
}

function readimage(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        $('.imagesrc').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}
function GritterNotification(titles,texts,classnm){
		if(titles=="Success"){
			var types='success';
			var btntext='Success';
			var colorcode='#469408';
		}else{
			var types='warning';
			var btntext='Warning';
			var colorcode='#f99b4a';
		}
		if(titles=="Success"){
		    swal({
    			title:titles,
    			text: texts,
    			icon:types,
    			buttons: {
    				confirm: {
    					text: 'ok',
    					value: !0,
    					visible: !0,
    					className: "btn btn-"+types,
    					closeModal: !0
    				}
    			}
            });  
		}else{
    		swal({
    			title:titles,
    			text: texts,
    			icon:types,
    			buttons: {
    				
    				confirm: {
    					text: 'ok',
    					value: !0,
    					visible: !0,
    					className: "btn btn-"+types,
    					closeModal: !0
    				}
    			}
            });  
		}
		
		 
		
}

