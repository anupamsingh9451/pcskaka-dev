$('document').ready(function(){
	/* login Sub Admins */
	$("#loginsubadmin").on('submit', function (e) {
		e.preventDefault();
		var formdata = new FormData(this);
		$.ajax({
			type: 'POST',
			url: base_loc + 'subadminpostajax/subadminlogin',
			data: formdata,
			contentType: false,
			cache: false,
			processData: false,
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey, 
			},
			beforeSend: function () {
				$('.loginsubadmin').attr("disabled", "disabled");
				$('.loginsubadmin').html("Please Wait...");
			},
			success: function (msg) {
				if(msg.message=="ok"){
					$('.mymsg').html("Login Successfull");
					$('.mymsg').removeClass('alert-warning');
					$('.mymsg').addClass('alert-success');
					$('.loginsubadmin').attr("disabled", false);
					$('.loginsubadmin').html("Sign me in");
					location.href=base_loc+'subadmin/ques_user/question';
				}else{
				    if(msg.message=="Already Login"){
				        swal({
                            title: "Are you sure?",
                            text: "You are Loged in somewhere! Do you want to login?",
                            icon: "warning",
                            buttons: [
                                'No, cancel it!',
                                'Yes, I am sure!'
                            ],
                            dangerMode: true,
                        }).then(function(isConfirm) {
                            if (isConfirm) {
                               formdata.append('loginagain','1');
                               $.ajax({
                        			type: 'POST',
                        			url: base_loc + 'subadminpostajax/subadminlogin',
                        			data: formdata,
                        			contentType: false,
                            		cache: false,
                            		processData: false,
                        			headers: {
                        				'Client-Service': clientserv,
                        				'Auth-Key': apikey,
                        			},
                        			success: function (msg) {
                        				if(msg.message=="ok"){
                        					$('.mymsg').html("Login Successfull");
                        					$('.mymsg').removeClass('alert-warning');
                        					$('.mymsg').addClass('alert-success');
                        					$('.loginsubadmin').attr("disabled", false);
                        					$('.loginsubadmin').html("Sign me in");
                        					location.href=base_loc+'subadmin/ques_user/question';
                        				}else{
                        				    $('.mymsg').html(msg.message);
                        					$('.mymsg').addClass('alert-warning');
                        					$('.mymsg').removeClass('alert-success');
                        					$('.loginsubadmin').attr("disabled", false);
                        					$('.loginsubadmin').html("Sign me in");
                        				}
                        			},
                        			error: function (msg) {
                        				if (msg.responseJSON['status'] == 303) {
                        					location.href = base_loc;
                        				}
                        				if (msg.responseJSON['status'] == 401) {
                        					location.href = base_loc; 
                        				}
                        				if (msg.responseJSON['status'] == 400) {
                        					location.href = base_loc; 
                        				}
                        			}
                        		}); 
                            } else {
                                // swal("Cancelled", "Your Question is safe :)", "error");
                                $('.loginsubadmin').attr("disabled", false);
					            $('.loginsubadmin').html("Sign me in");
                            }
                        })
                        $(".swal-button--danger").css('background-color', '#00acac');
				    }else{
				        $('.mymsg').html(msg.message);
    					$('.mymsg').addClass('alert-warning');
    					$('.mymsg').removeClass('alert-success');
    					$('.loginsubadmin').attr("disabled", false);
    					$('.loginsubadmin').html("Sign me in");
				    }
					
				}
			},
			error: function (msg) {
				if (msg.responseJSON['status'] == 303) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 401) {
					location.href = base_loc; 
				}
				if (msg.responseJSON['status'] == 400) {
					location.href = base_loc; 
				}
			}
		});
	});
	/* for logout */
	$('.logoutsubadmin').on('click', function () {
		$.ajax({
			type: 'POST',
			url: base_loc + 'subadminpostajax/subadminlogout',
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
			success: function (msg) {
				location.href = base_loc+'subadmin';
			},
			error: function (msg) {
				if (msg.responseJSON['status'] == 303) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 401) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 400) {
					location.href = base_loc;
				}
			}
		});
	});
	
	
	/* for creating Question */
	$('body').on('submit','#questioncreate', function (e) {
		e.preventDefault();
		$check = 0;
		var ques_type = $(this).find('option:selected','.ques_type').val();
		$chapter = $(this).find('.chapterid').val();
		console.log($chapter);
		if(ques_type==1){
		    var subj_ques_explain = $(this).find('.subj_ques_explain').val();
		    var subj_answer = $(this).find('.subj_answer').val();
		    var subj_positive_mark = $(this).find('.subj_positive_mark').val();
		    var subj_negative_mark = $(this).find('.subj_negative_mark').val();
		    var subj_created_date = $(this).find('.subj_created_date').val();

		    if(subj_answer!='' && subj_created_date!='' && $.trim(subj_ques_explain)!=''){
		        $check=1;
		    }
		}else{
		    var obj_ques_explain = $(this).find('.obj_ques_explain').val();
		    var obj_options = $(this).find('.obj_options').val();

		  //  var obj_options = $(this).find('.obj_options').val();
		  //  var obj_positive_mark = $(this).find('.obj_positive_mark').val();
		  //  var obj_negative_mark = $(this).find('.obj_negative_mark').val();
		    var obj_created_date = $(this).find('.obj_created_date').val();
		    
		    $i = 0;
		    $j = 0;
		    if($.trim(obj_ques_explain)!='' && obj_created_date!=''){
		        $(".ques_option").each(function() {
                    if($(this).val()!=''){
                        $j = $j+1; 
                    }
                });
                $(".obj_answer").each(function() {
                   if($(this).is(':checked')) {
                       $i = $i+1; 
                   }
                });
		    }
		    
            if($i>0 && $j==obj_options){   
                $check=1;
            }else{
                $check=0;
            }
		}
		if($('#mysummernote').summernote('isEmpty')) {
            $check=0;
         }
		if($check==1){
    		var f=new FormData(this);
    		$.ajax({
    			type: 'POST',
    			url: base_loc + 'subadminpostajax/questioncreate',
    			data:f,
    			contentType: false,
    			cache: false,
    			processData: false,
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    				'User-ID': loginid,
    				'Authorization': token,
    				'type': type
    			},
    			beforeSend: function () {
    				$('.questioncreate').attr("disabled", "disabled");
    				$('.questioncreate').html("Please Wait...");
    			},
    			success: function (msg) {
    				if(msg.data=="ok"){
    					GritterNotification("Success","QUESTION CREATED SUCCESSFULLY","my-sticky-class");
    					$('#questioncreate')[0].reset();
    					$('.questioncreate').attr("disabled",false);
    					$('.questioncreate').html("SAVE");
    					$('#addquestion').modal('hide');
    					$('#addquestionmodal').modal('hide');
    					updatelistques();
    				// 	location.href = base_loc+'subadmin/question/'+$chapter;
    					
    				}else{
    					GritterNotification("Error",msg.message,"my-sticky-class");
    					$('.questioncreate').attr("disabled",false);
    					$('.questioncreate').html("SAVE");	
    				}
    			},
    			error: function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc;
    				}
    			}
    		});
		}else{
		    GritterNotification("Error","Please Fill All Input","my-sticky-class");
		}
	});
	
	
	$('body').on('click','.questionedit', function (e) {
	    var mytype=$(this).attr('attr-type');
	    $('#questionedit').find('input[name=mytype]').val(mytype);
	    
	});
		/* for editing Question */
	$('body').on('submit','#questionedit', function (e) {
		e.preventDefault();
		$check = 0;
		
		var ques_type = $(this).find('option:selected','.ques_type').val();
		if(ques_type==1){
		    var subj_answer = $(this).find('.subj_answer').val();
		    var subj_positive_mark = $(this).find('.subj_positive_mark').val();
		    var subj_negative_mark = $(this).find('.subj_negative_mark').val();
		  //  var subj_created_date = $(this).find('.subj_created_date').val();
		     var subj_ques_explain = $(this).find('.subj_ques_explain').val();
		    if(subj_answer!='' && $.trim(subj_ques_explain)!=''){
		        $check=1;
		    }
		}else{
		    var obj_options = $(this).find('.obj_options').val();
		    var obj_positive_mark = $(this).find('.obj_positive_mark').val();
		    var obj_negative_mark = $(this).find('.obj_negative_mark').val();
		  var obj_ques_explain = $(this).find('.obj_ques_explain').val();
		  var edit_obj_options = $(this).find('.edit_obj_options').val();
		    $i = 0;
		    $j = 0;
		    
		    if($.trim(obj_ques_explain)!=''){
                $(".obj_answer").each(function() {
                   if($(this).is(':checked')) {
                       $i = $i+1; 
                   }
                });
                $(".ques_option").each(function() {
                    if($(this).val()!=''){
                        $j = $j+1;
                    }
                });
		    }

            if($i>0 && $j==edit_obj_options){
                $check=1;
            }else{
                $check=0;
            }
		}
		if($('#mysummernote').summernote('isEmpty')) {
            $check=0;
         }
		if($check==1){
    		var f=new FormData(this);
    		f.append('loginid',loginid);
    		$.ajax({
    			type: 'POST',
    			url: base_loc + 'subadminpostajax/questionedit',
    			data:f,
    			contentType: false,
    			cache: false,
    			processData: false,
    			headers: {
    				'Client-Service': clientserv,
    				'Auth-Key': apikey,
    				'User-ID': loginid,
    				'Authorization': token,
    				'type': type
    			},
    			beforeSend: function () {
    				$('.questionedit').attr("disabled", "disabled");
    				$('.questionedit').html("Please Wait...");
    			},
    			success: function (msg) {
    				if(msg.data=="ok"){
    					GritterNotification("Success","QUESTION EDITED SUCCESSFULLY","my-sticky-class");
    					$('.questionedit').attr("disabled",false);
    					$('.questionedit').html("SAVE");
    					$('#viewquestionmodal').modal('hide');
    					$('#editquestionmodal').modal('hide');
    					updatelistques();
    				}else{
    					GritterNotification("Error",msg.message,"my-sticky-class");
    					$('.questionedit').attr("disabled",false);
    					$('.questionedit').html("SAVE");	
    				}
    			},
    			error: function (msg) {
    				if (msg.responseJSON['status'] == 303) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 401) {
    					location.href = base_loc;
    				}
    				if (msg.responseJSON['status'] == 400) {
    					location.href = base_loc;
    				}
    			}
    		});
		}else{
		    GritterNotification("Error","Please Fill All Input","my-sticky-class");
		}
	});
	
/* for creating book */
	$('#bookcreate').on('submit', function (e) {
		e.preventDefault();
		var f=new FormData(this);
		f.append('adminid',loginid);
		$.ajax({
			type: 'POST',
			url: base_loc + 'subadminpostajax/bookcreate',
			data:f,
			contentType: false,
			cache: false,
			processData: false,
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
			beforeSend: function () {
				$('.bookcreate').attr("disabled", "disabled");
				$('.bookcreate').html("Please Wait...");
			},
			success: function (msg) {
				if(msg.data=="ok"){
					GritterNotification("Success","BOOK CREATED SUCCESSFULLY","my-sticky-class");
					$('#bookcreate')[0].reset();
					$('.bookcreate').attr("disabled",false);
					$('.bookcreate').html("SAVE");
					$('#bookcreate').modal('hide');
					location.href = base_loc+'subadmin/book/list';
				}else{
					GritterNotification("Error",msg.message,"my-sticky-class");
					$('.bookcreate').attr("disabled",false);
					$('.bookcreate').html("SAVE");	
				}
			},
			error: function (msg) {
				if (msg.responseJSON['status'] == 303) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 401) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 400) {
					location.href = base_loc;
				}
			}
		});
	});
	/* for updatint book */
	$('#editbookform').on('submit', function (e) {
		e.preventDefault();
		var f=new FormData(this);
		f.append('adminid',loginid);
		$.ajax({
			type: 'POST',
			url: base_loc + 'subadminpostajax/editbookform',
			data:f,
			contentType: false,
			cache: false,
			processData: false,
			headers: {
				'Client-Service': clientserv,
				'Auth-Key': apikey,
				'User-ID': loginid,
				'Authorization': token,
				'type': type
			},
			beforeSend: function () {
				$('.editbookform').attr("disabled", "disabled");
				$('.editbookform').html("Please Wait...");
			},
			success: function (msg) {
				if(msg.data=="ok"){
					GritterNotification("Success","BOOK EDITED SUCCESSFULLY","my-sticky-class");
					$('#editbookform')[0].reset();
					$('.editbookform').attr("disabled",false);
					$('.editbookform').html("SAVE");
					$('#editbook').modal('hide');
					
					updatelistbooks();
				}else{
					GritterNotification("Error",msg.message,"my-sticky-class");
					$('.editbookform').attr("disabled",false);
					$('.editbookform').html("SAVE");	
				}
			},
			error: function (msg) {
				if (msg.responseJSON['status'] == 303) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 401) {
					location.href = base_loc;
				}
				if (msg.responseJSON['status'] == 400) {
					location.href = base_loc;
				}
			}
		});
	});
});