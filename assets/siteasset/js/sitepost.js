$(document).ready(function(){
	$('#contactform').on('submit', function (e) {
        e.preventDefault();
		var f= new FormData(this);
    	$.ajax({
			type: 'POST',
			url: base_loc + 'site/sendcontactform',
			data:f,
			contentType: false,
			cache: false,
			processData: false,
			headers: {
				'Client-Service': clientserv,
    			'Auth-Key': apikey,
			},
			beforeSend: function () {
				$('.contact_form').attr("disabled", "disabled");
				$('.contact_form').html("Please Wait...");
			},
			success: function (msg) {
                show_message('Success','Enquiry Sent Successfully','success');
				$('.contact_form').attr("disabled",false);
				$('.contact_form').html("Send Message");
				$('#contactform').trigger('reset');
			},
			error: function (msg) {
				if (msg.responseJSON['status'] == 303) {
					location.href = base_url;
				}
				if (msg.responseJSON['status'] == 401) {
					location.href = base_url;
				}
				if (msg.responseJSON['status'] == 400) {
					location.href = base_url;
				}
			}
		});
	});
    function show_message(title,text,mode)
    {
        swal(title,text,mode);
    }
});