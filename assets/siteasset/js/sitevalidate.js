$('document').ready(function(){
	/* for input type number validation */
	$('body').on('keydown', '.input_num', function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl/cmd+A
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+C
			(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+X
			(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode == 45) || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	
});

function GritterNotification(titles,texts,classnm){
		if(titles=="Success"){
			var types='success';
			var btntext='Success';
			var colorcode='#469408';
		}else{
			var types='warning';
			var btntext='Warning';
			var colorcode='#f99b4a';
		}
		if(titles=="Success"){
		    swal({
    			title:titles,
    			text: texts,
    			icon:types,
    			buttons: {
    				confirm: {
    					text: 'ok',
    					value: !0,
    					visible: !0,
    					className: "btn btn-"+types,
    					closeModal: !0
    				}
    			}
            });  
		}else{
    		swal({
    			title:titles,
    			text: texts,
    			icon:types,
    			buttons: {
    				
    				confirm: {
    					text: 'ok',
    					value: !0,
    					visible: !0,
    					className: "btn btn-"+types,
    					closeModal: !0
    				}
    			}
            });  
		}
}